using System;
using System.Linq;

namespace cardGames.models
{

    public static class CardsListBuilder
    {
        /// <summary>
        /// Creates a full deck of cards without Jokers
        /// </summary>
        public static CardsList FullDeckNoJokers()
        {
            CardsList result = new CardsList();

            foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType)))
            {
                foreach (Card.RankType rank in Enum.GetValues(typeof(Card.RankType)))
                {
                    if (suit != Card.SuitType.Joker && rank != Card.RankType.JokerSmall && rank != Card.RankType.JokerBig)
                    {
                        result.Add(new Card(suit, rank));
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Creates a full single suit
        /// </summary>
        public static CardsList SingleSuit(Card.SuitType suit)
        {
            CardsList result = new CardsList();

            foreach (Card.RankType rank in Enum.GetValues(typeof(Card.RankType)))
            {
                if (rank != Card.RankType.JokerSmall && rank != Card.RankType.JokerBig)
                {
                    result.Add(new Card(suit, rank));
                }
            }

            return result;
        }


        /// <summary>
        /// Creates a full deck of cards *with Jokers*
        /// suit length: 15,12,13,12
        /// it's 52 cards deck not 54
        /// </summary>
        public static CardsList FullDeckWithJokers()
        {
            CardsList result = new CardsList();

            foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType)))
            {
                foreach (Card.RankType rank in Enum.GetValues(typeof(Card.RankType)))
                {
                    switch (suit)
                    {
                        case Card.SuitType.Joker:
                            break;
                        case Card.SuitType.Spades:
                            result.Add(new Card(suit, rank));
                            break;
                        case Card.SuitType.Hearts:
                            if (rank >= Card.RankType.Three && rank <= Card.RankType.Ace)
                                result.Add(new Card(suit, rank));
                            break;
                        case Card.SuitType.Clubs:
                            if (rank >= Card.RankType.Two && rank <= Card.RankType.Ace)
                                result.Add(new Card(suit, rank));
                            break;
                        case Card.SuitType.Diamonds:
                            if (rank >= Card.RankType.Three && rank <= Card.RankType.Ace)
                                result.Add(new Card(suit, rank));
                            break;
                    }

                }
            }
            return result;
        }


        /// <summary>
        /// 32 cards deck: 7,8,9,T,J,Q,K,A
        /// </summary>
        public static CardsList ShortDeck7ToAce()
        {
            CardsList result = new CardsList();
            result.AddRange(from Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType)) 
                where suit != Card.SuitType.Joker 
                from Card.RankType rank in Enum.GetValues(typeof(Card.RankType)) 
                where rank >= Card.RankType.Seven && rank <= Card.RankType.Ace select new Card(suit, rank));

            return result;
        }
    }

}