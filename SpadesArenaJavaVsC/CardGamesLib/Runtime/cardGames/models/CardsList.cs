﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace cardGames.models
{
	/// <summary>
	/// Represents a list of cards
	/// </summary>
	public class CardsList:List<Card>
	{
		private static Random rng = new Random();  

		public CardsList ()
		{
		}

		public CardsList (IEnumerable<Card> collection) : base (collection)
		{
		}

		 public CardsList(string cardsStr) 
        {
            if (cardsStr!=null)
                SetCards(cardsStr);
        }


         public CardsList(List<Card> cardsList)
         {
             if (cardsList == null) return;

             Clear();
             foreach (Card card in cardsList)
             {
                 Add(card);
             }
         }

         public void CopyCardsList(CardsList cardsListToCopy)
         {
             this.Clear();
             foreach (Card card in cardsListToCopy)
             {
                 this.Add(card);
             }
         }

        public void SetCards(params String[] cardStrs) {
			Clear ();
			for (int i = 0; i < cardStrs.Length; i++)
            {
                if (cardStrs[i].Length != 2)
                    throw (new Exception("cardStrs params string is not 2"));
				string suit = cardStrs [i][0].ToString().ToUpper();
				string rank = cardStrs [i][1].ToString();
				Add (new Card (suit + rank));
			}
		}

		public void SetCards(string cardsStr) {
            if (cardsStr.Length % 2 == 1 || cardsStr.Length == 1)
                throw (new Exception("cardStrs string does not consists of pairs of chars"));
            Clear();
			for (int i = 0; i < cardsStr.Length; i+=2) {
				string rank = cardsStr[i].ToString();
				string suit = cardsStr[i+1].ToString().ToUpper();
				Add(new Card(rank + suit));
			}
		}


		public CardsList CardsBySuits(params Card.SuitType[] suits) {
			CardsList result = new CardsList ();
			foreach (var suit in suits) {
				result.AddRange(this.Where (c => c.Suit == suit).ToList ());
			}

			result.SortHighToLowSuitFirst ();
			return result;
		}

        public CardsList CardsBySuits(ICollection<Card.SuitType> suits) {
            CardsList result = new CardsList();
            foreach (var suit in suits) {
                result.AddRange(this.Where(c => c.Suit == suit).ToList());
            }

            result.SortHighToLowSuitFirst();
            return result;
        }

        public bool HasRank(Card.RankType rank) {
			return Find (c => c.Rank == rank) != null;
		}

		public Card getCard(Card.RankType rank, Card.SuitType suit)
		{
			return Find(c => c.Rank == rank && c.Suit == suit);
		}


        public void SortHighToLowSuitFirst() {
			Sort ((C1, C2) => {
				int result = C2.Suit - C1.Suit;
				if (result == 0) {
					result = C2.Rank - C1.Rank;
				}

				return result;
			});
		}

		public void SortHighToLowRankFirst()
		{
			Sort((C1, C2) =>
			{
				int result = C2.Rank - C1.Rank;
				if (result == 0)
				{
					result = C2.Suit - C1.Suit;
				}

				return result;
			});
		}

        /// <summary>
        /// Sorts the cards list high to low rank first with spades cards at the beginning.
        /// Used for auto play, to prevent spades being thrown
        /// E.g: KS,4S,3S,KC,KD,QC..
        /// </summary>
        public void SortHighToLowRankFirstSpadesFirst()
        {
            SortHighToLowRankFirst();

            StringBuilder spadesString = new StringBuilder();
            for (int i = 0; i < Count; i++)
            {
                if (this[i].Suit == Card.SuitType.Spades)
                    spadesString.Append(this[i].ToString());
            }

            CardsList spades = new CardsList(spadesString.ToString());
            RemoveSomeCards(spades);
            InsertRange(0, spades);
        }


        // Fisher–Yates shuffle
        public void Shuffle() {
			int n = Count;  
			while (n > 1) {  
				n--;  
				int k = rng.Next(n + 1);  
				Card c = this[k];  
				this[k] = this[n];  
				this[n] = c;  
			}  
		}

		public int CountGreaterThan(Card.RankType rank) {
			return FindAll (c => c.Rank > rank).Count;
		}


		public int CountLowerThan(Card.RankType rank) {
			return FindAll (c => c.Rank < rank).Count;
		}

		public int CountSuit(Card.SuitType? suit) {
			return FindAll (c => c.Suit == suit).Count;
		}

		public bool HasSingleSuit(Card.SuitType suit) {
			return CountSuit (suit) == Count;
		}

        /// <summary>
        /// Assume that the list consists of a single suit.
        /// </summary>
        /// <param name="rank"></param>
        /// <returns> Returns the weakest card above rank, or null if no card. </returns>
        public Card GetWeakestAboveRank(Card.RankType rank) {
            CardsList cardsList = new CardsList(FindAll(c => c.Rank > rank));

            if(cardsList.Count == 0) {
                return null;
            }

            cardsList.SortHighToLowRankFirst();
            return cardsList[cardsList.Count - 1];
        }


	    /// <returns> Returns the weakest card, or null if no card. </returns>
	    public Card GetWeakest()
	    {
	        CardsList cardsList = new CardsList(FindAll(c => c.Rank >= Card.RankType.Two));

	        if (cardsList.Count == 0)
	        {
	            return null;
	        }

	        cardsList.SortHighToLowRankFirst();
	        return cardsList[cardsList.Count - 1];
	    }

        /// <returns> Returns the second weakest card, or the weakest if have only 1 card, or null if no card . </returns>
        public Card GetSecondWeakestCard()
	    {
	        CardsList cardsList = new CardsList(FindAll(c => c.Rank >= Card.RankType.Two));
	        if (cardsList.Count == 0) {return null;}
	        if (cardsList.Count == 1) {return cardsList[0];}
            cardsList.SortHighToLowRankFirst();
	        return cardsList[cardsList.Count - 2];
	    }

        /// <summary>
        /// Assume that the list consists of a single suit.
        /// </summary>
        /// <param name="rank"></param>
        /// <returns> Returns the strongest card below rank, or null if no card. </returns>
        public Card GetStrongestBelowRank(Card.RankType rank)
        {
            CardsList cardsList = new CardsList(FindAll(c => c.Rank < rank));

            if (cardsList.Count == 0)
            {
                return null;
            }

            cardsList.SortHighToLowRankFirst();
            return cardsList[0];
        }


	    /// <summary>
	    /// Assume that the list consists of a single suit.
	    /// </summary>
	    /// <param name="rank"></param>
	    /// <returns> Returns the strongest card below rank, or null if no card. </returns>
	    public CardsList GetAllCardsAboveRank(Card.RankType rank)
	    {
	        CardsList cardsList = new CardsList(FindAll(c => c.Rank > rank));

	        if (cardsList.Count == 0) {return null;}
            cardsList.SortHighToLowRankFirst();
	        return cardsList;
	    }

        /// Assume that the list consists of a single suit.
        public Card GetStrongestBelowRankBelowSecondHighest(Card.RankType rank, Card.SuitType suit, string deck)
        {
            CardsList cardsList;
            if (deck == "jokers")
            {
                cardsList = new CardsList(FindAll(c => c.Rank < rank && c.Rank < Card.RankType.JokerSmall));
            }
            else
            {
                cardsList = new CardsList(FindAll(c => c.Rank < rank && c.Rank < Card.RankType.King));
            }
            
            if (cardsList.Count == 0)
            {
                return null;
            }
            cardsList.SortHighToLowRankFirst();
            return cardsList[0];
        }

	    /// <returns> Returns the strongest card equal or below rank, or null if no card. </returns>
	    public Card GetStrongestBelowOrEqualRank(Card.RankType rank)
	    {
	        CardsList cardsList = new CardsList(FindAll(c => c.Rank <= rank));

	        if (cardsList.Count == 0)
	        {
	            return null;
	        }

	        cardsList.SortHighToLowRankFirst();
	        return cardsList[0];
	    }


        /// consists SEVERAL suits
        /// returns a card closest to 8, not the average of the deck
        public Card GetMediumCard()
        {
            if (this.Count == 0) { return null; }
            Card medium = this[0];
            int medDis = Math.Abs(medium.Rank - Card.RankType.Eight) ;

            foreach (Card c in this)
            {
                int x = Math.Abs(c.Rank - Card.RankType.Eight);
                if (x < medDis)
                {
                    medDis = x;
                    medium = c;
                }
            }
            return medium;
        }



        /// <summary>
        /// Removes num cards from the end of the list and returns them
        /// </summary>
        public CardsList RemoveCardsFromEnd(int num) {
            CardsList result = new CardsList();
            for (int i = 0; i < num; i++)
            {
                if (Count>0) {
                    result.Add(this[Count - 1]);
                    RemoveAt(Count-1);
                }
            }
            return result;
		}

        /// <summary>
        /// Removes num cards from the beginning of the list and returns them
        /// </summary>
        public CardsList RemoveCardsFromBeginning(int num)
        {
            CardsList result = new CardsList();
            for (int i = 0; i < num; i++)
            {
                if (Count > 0)
                {
                    result.Add(this[0]);
                    RemoveAt(0);
                }
            }
            return result;
        }

        /// <summary>
        /// Removes cards form this list that exist in the specified list
        /// </summary>
        public void RemoveSomeCards(CardsList cardsToRemove) {
            foreach (var card in cardsToRemove)
            {
                Remove(card);
            }
        }


        /// <summary>
        /// Moves a card to beginning of the list
        /// </summary>
        public void MoveCardToBeginning(Card card) {
            if (Remove(card)) {
                Insert(0,card);
            }
        }




		public int HonorsCount {
			get {
				return CountGreaterThan (Card.RankType.Ten);
			}
		}



		public static CardsList operator + (CardsList cl1, CardsList cl2) {
			cl1.AddRange (cl2);
			return cl1;
		}

        /// <summary>
        /// intersects two lists but keeping the order of the original one
        /// </summary>
        /// <param name="other"></param>
        public void Intersect(CardsList other)
        {
            CardsList intersected = new CardsList(this);

            foreach(Card card in this)
            {
                if (!other.Contains(card))
                    intersected.Remove(card);
            }

            this.CopyCardsList(intersected);
        }

        public override int GetHashCode()
        {
            if (this.Count == 0)
                return 0;

            int hash = 1;
            foreach (Card card in this)
            {
                hash *= card.GetHashCode();
            }
            while (hash > int.MaxValue)
            {
                hash = (int)Math.Sqrt(hash);
                hash *= 337;
            }

            return hash;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is List<Card>))
                return false;

            CardsList other = (CardsList)obj;

            return this.SequenceEqual(other);
        }

        public override string ToString ()
		{
			StringBuilder result = new StringBuilder();
			foreach(Card card in this)
			{
				result.Append(card.ToString ());
				//result.Append (',');
			}
			//if (result.Length > 0)
				//result.Remove (result.Length - 1, 1);
			return result.ToString ();
		}
	}
}
