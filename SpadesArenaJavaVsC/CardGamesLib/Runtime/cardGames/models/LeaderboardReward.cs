﻿namespace cardGames.models
{
	public class LeaderboardReward
	{
		private bool m_model_is_set = false;
		private int m_leaderboard_id = 0;
		private int m_rank=0;
		private int m_reward=0;
		private string m_leaderboardTitle="";
		private string m_color="";
		private string m_group="A1";

		public int Leaderboard_id {
			get {
				return m_leaderboard_id;
			}
			set {
				m_leaderboard_id = value;
			}
		}
	
		public int Rank {
			get {
				return m_rank;
			}
			set {
				m_rank = value;
			}
		}

		public int Reward {
			get {
				return m_reward;
			}
			set {
				m_reward = value;
			}
		}

		public string LeaderboardTitle {
			get {
				return m_leaderboardTitle;
			}
			set {
				m_leaderboardTitle = value;
			}
		}

		public bool ModelIsSet {
			get {
				return m_model_is_set;
			}
			set {
				m_model_is_set = value;
			}
		}

		public string Color {
			get {
				return m_color;
			}
			set {
				m_color = value;
			}
		}

		public string Group {
			get {
				return m_group;
			}
			set {
				m_group = value;
			}
		}
	}
}
