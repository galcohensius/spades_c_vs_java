﻿using System;

namespace cardGames.models
{
    /// <summary>
    /// Represents a single card
    /// </summary>
    public class Card
    {
        #region enums
        public enum SuitType
        {
            Clubs, Diamonds, Hearts, Spades, Joker
        }

        public enum RankType
        {
            Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace, JokerSmall, JokerBig
        }
        #endregion


        private SuitType suit;
        private RankType rank;

        private float strength;

        public Card()
        {

        }
        public Card(Card card)
        {
            this.Suit = card.Suit;
            this.Rank = card.Rank;
            this.Strength = card.Strength;
        }
        public Card(SuitType suit, RankType rank)
        {
            this.suit = suit;
            this.rank = rank;
        }

        public Card(String cardStr)
        {
            if (String.IsNullOrEmpty(cardStr) || cardStr.Length != 2)
                throw new Exception("card code string is null or length is not 2");

            RankType rankType;

            char rank = cardStr[0];

            switch (rank)
            {
                case '$':
                    rankType = RankType.JokerBig;
                    break;
                case '*':
                    rankType = RankType.JokerSmall;
                    break;
                case 'A':
                    rankType = RankType.Ace;
                    break;
                case 'K':
                    rankType = RankType.King;
                    break;
                case 'Q':
                    rankType = RankType.Queen;
                    break;
                case 'J':
                    rankType = RankType.Jack;
                    break;
                case '0':
                    rankType = RankType.Ten;
                    break;
                default:

                    short result;
                    if (Int16.TryParse(rank.ToString(), out result))
                    {
                        if (Enum.IsDefined(typeof(RankType), (int)result))
                            rankType = (RankType)result;
                        else
                            throw new Exception("No valid rank found for " + result);
                    }
                    else
                    {
                        throw new Exception("No valid rank found for " + result);
                    }
                    break;

            }

            this.rank = rankType;

            suit = ParseSuitChar(cardStr[1]);

        }

        public static SuitType ParseSuitChar(char suitChar)
        {
            switch (suitChar)
            {
                case 'C':
                    return SuitType.Clubs;
                case 'D':
                    return SuitType.Diamonds;
                case 'H':
                    return SuitType.Hearts;
                case 'S':
                    return SuitType.Spades;
                case 'J':
                    return SuitType.Joker;
            }
            throw new Exception("No suit found for " + suitChar);
        }

        public static bool operator ==(Card c1, Card c2)
        {

            if (ReferenceEquals(c1, null))
            {
                if (ReferenceEquals(c2, null))
                {
                    return true;
                }
                return false;
            }

            // Return true if the fields match:
            return c1.Equals(c2);
        }

        public static bool operator !=(Card c1, Card c2)
        {
            return !(c1 == c2);
        }


        public SuitType Suit
        {
            get
            {
                return suit;
            }
            set
            {
                suit = value;
            }
        }

        public RankType Rank
        {
            get
            {
                return rank;
            }
            set
            {
                rank = value;
            }
        }

        public float Strength
        {
            get
            {
                return strength;
            }
            set
            {
                strength = value;
            }
        }

        #region object methods

        public override string ToString()
        {
            return string.Format("{0}{1}", rank.SingleChar(), suit.SingleChar());
        }

        public override bool Equals(object obj)
        {
            // If parameter is null, return false.
            if (ReferenceEquals(obj, null))
            {
                return false;
            }
            // Optimization for a common success case.
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            // If run-time types are not exactly the same, return false.
            if (GetType() != obj.GetType())
                return false;

            // Return true if the fields match.
            Card c = (Card)obj;
            return (suit == c.suit) && (rank == c.rank);
        }

        public override int GetHashCode()
        {
            return (int)suit * 100 + (int)rank;
            //narrowed from 1000 to 100 for the CardsList equals operation
        }
        #endregion

    }

    public static class EnumExtensions
    {
        public static string SingleChar(this Card.RankType rank)
        {
            switch (rank)
            {
                case Card.RankType.Two:
                    return "2";
                case Card.RankType.Three:
                    return "3";
                case Card.RankType.Four:
                    return "4";
                case Card.RankType.Five:
                    return "5";
                case Card.RankType.Six:
                    return "6";
                case Card.RankType.Seven:
                    return "7";
                case Card.RankType.Eight:
                    return "8";
                case Card.RankType.Nine:
                    return "9";
                case Card.RankType.Ten:
                    return "0";
                case Card.RankType.Jack:
                    return "J";
                case Card.RankType.Queen:
                    return "Q";
                case Card.RankType.King:
                    return "K";
                case Card.RankType.Ace:
                    return "A";
                case Card.RankType.JokerSmall:
                    return "*";
                case Card.RankType.JokerBig:
                    return "$";
            }
            return null;
        }

        public static string SingleChar(this Card.SuitType suit)
        {
            switch (suit)
            {
                case Card.SuitType.Clubs:
                    return "C";
                case Card.SuitType.Hearts:
                    return "H";
                case Card.SuitType.Diamonds:
                    return "D";
                case Card.SuitType.Spades:
                    return "S";
                case Card.SuitType.Joker:
                    return "J";
            }
            return null;
        }



    }

}

