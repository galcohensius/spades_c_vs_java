﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace cardGames.models
{
    //AKA:                       regular, full, sleeping
    public enum PiggyState { Accumulating, Full, Cooldown }

    public class UserPiggyData
    {
        public int CoinsBalance { get; private set; }
        public int CoinsMultiplierEndDateInterval { get; private set; } // in seconds
        public DateTime CoinsMultiplierEndDate { get; private set; }
        public int CoinsMultiplierStartDateInterval { get; private set; }// in seconds
        public DateTime CoinsMultiplierStartDate { get; private set; }
        public float CoinsMultiplier { get; private set; }
        public float Cost { get; private set; }
        public List<List<int>> CapReminderList { get; private set; }
        private int currentCapReminder = 0;
        public int Cap { get; private set; }
        public string ExternalPackageID { get; private set; }
        public int EndDateInterval { get; private set; }// in seconds, also used as a flag if a timer is/was running (when greater than 0)
        public DateTime EndDate { get; private set; }   //to calculate the real end date, use this
        public int InitialCoins { get; private set; }
        public string PackageID { get; private set; }
        public string PackageMetaData { get; private set; }
        public int PiggyPlanID { get; private set; }
        public PiggyState PiggyState { get; private set; }
        public PiggyState PrevState { get; private set; } //needed for end game animation since the the update could occur before the animation check

        public Action<int> OnPiggyUpdated; //invokes with coins delta. invokes only at end match

        public UserPiggyData(int coinsBalance, int coinsMultiplierEndDateInterval, int coinsMultiplierStartDateInterval, float coinsMultiplier, 
            float cost, List<List<int>> capReminderList, int cap, int endDateInterval, string externalPackageID, int initialCoins, 
            string packageID, string packageMetaData, int piggyPlanID, int piggyState)
        {
            CoinsBalance = coinsBalance;
            CoinsMultiplierEndDateInterval = coinsMultiplierEndDateInterval;
            CoinsMultiplierStartDateInterval = coinsMultiplierStartDateInterval;
            CoinsMultiplier = coinsMultiplier;
            if (CoinsMultiplier > 1 && CoinsMultiplierEndDateInterval > 0)
            {
                CoinsMultiplierStartDate = DateTime.Now.AddSeconds(CoinsMultiplierStartDateInterval);
                CoinsMultiplierEndDate = DateTime.Now.AddSeconds(CoinsMultiplierEndDateInterval);
            }
            else
                CoinsMultiplierEndDate = CoinsMultiplierStartDate = DateTime.Now.AddSeconds(-1);
            Cost = cost;
            CapReminderList = capReminderList;
            Cap = cap;
            ExternalPackageID = externalPackageID;
            EndDateInterval = endDateInterval;
            EndDate = DateTime.Now.AddSeconds(endDateInterval);
            InitialCoins = initialCoins;
            PackageID = packageID;
            PackageMetaData = packageMetaData;
            PiggyPlanID = piggyPlanID;
            PiggyState = (PiggyState)piggyState;
            PrevState = PiggyState;
        }

        public void OverrideToCooldownStateWhileRefreshing()
        {
            CoinsBalance = 0;
            CoinsMultiplierEndDateInterval = 0;
            CoinsMultiplierStartDateInterval = 0;
            CoinsMultiplier = 0;
            Cost = 0;
            CapReminderList = null;
            currentCapReminder = -1;
            Cap = 0;
            EndDateInterval = 0;
            EndDate = DateTime.Now.AddSeconds(-1);
            InitialCoins = 0;
            PackageID = "dummy piggy";
            PackageMetaData = "";
            PiggyPlanID = -1;
            PiggyState = PiggyState.Cooldown;
            PrevState = PiggyState.Cooldown;
        }

        public bool UpdateAfterMatch(EndGamePiggyData endGamePiggyData)
        {
            bool piggyFilled = PiggyState == PiggyState.Accumulating && endGamePiggyData.PiggyState == PiggyState.Full;
            PiggyPlanID = endGamePiggyData.PiggyPlanID;
            CoinsBalance = endGamePiggyData.PiggyTotalCoins;
            PrevState = PiggyState;
            PiggyState = endGamePiggyData.PiggyState;
            OnPiggyUpdated?.Invoke(endGamePiggyData.PiggyCoinsDelta);
            return piggyFilled;
        }

        public bool IsBoostActive()
        {
            if (CoinsMultiplier > 1)
            {
                return CoinsMultiplierStartDate <= DateTime.Now && DateTime.Now < CoinsMultiplierEndDate;
            }
            return false;
        }

        //current implementation: no caps range (are present, not used)
        //binary decision based on if the player accumulated some percentage of money based on the lower limit of the highest cap
        public bool ShowCapacityReminder()
        {
            float relativeFilling = (float)(CoinsBalance - InitialCoins) / (Cap - InitialCoins);
            float threashold = (float)CapReminderList.Last()[0]/100;
            return relativeFilling >= threashold;
        }
    }
}


