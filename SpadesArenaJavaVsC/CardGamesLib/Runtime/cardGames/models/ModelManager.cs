﻿using cardGames.models.contests;
using common.controllers;
using common.models;
using System;
using System.Collections.Generic;

namespace cardGames.models
{
    /// <summary>
    /// Model Manager for card games.
    /// </summary>
    public abstract class ModelManager : MonoBehaviour
    {
        //hide this with derived class
        protected User m_user;
        protected List<World> m_worlds;

        public static Dictionary<string, int> unlockLevelMap = new Dictionary<string, int>(){

            {"s1",1},
            {"s2",1},
            {"s3",3},
            {"s4",3},
            {"s5",6},
            {"s6",6},

            {"s7",10},
            {"s8",10},
            {"s9",10},
            {"s10",10},
            {"s11",10},
            {"s12",10},

            {"s16",20},
            {"s17",20},
            {"s18",20},
            {"s19",20},
            {"s20",20},
            {"s21",20},

            {"s28",40},
            {"s29",40},
            {"s30",40},
            {"s31",40},
            {"s32",40},
            {"s33",40},

            {"s34",70},
            {"s35",70},
            {"s36",70},
            {"s37",70},
            {"s38",70},
            {"s39",70},

            {"s675",120},
            {"s676",140},
            {"s677",160},
            {"s678",120},
            {"s679",140},
            {"s680",160},

            {"a1",8},
            {"a4",8},

            {"a2",15},
            {"a5",15},

            {"a3",30},
            {"a6",30},

            {"a7",50},
            {"a9",50},

            {"a8",90},
            {"a10",90},

            {"a51",150},
            {"a52",150},
            {"a53",150},
            {"a54",150},
            {"a55",150},
            {"a56",150},
            {"a57",150},
            {"a58",150},
            {"a59",150},
            {"a60",150},
            {"a61",150},
            {"a62",150},
            {"a63",150},



        };

        private string m_initialWorldId;

        protected Table m_table;

        protected SocialModel m_social_model = new SocialModel();

        protected Cashier m_cashier;

        protected UserPiggyData m_piggy;

        /// <summary>
        /// generic dictionary for additional packages from Java server for Piggy, Quests and future features
        /// key : internal package ID, value : external package ID
        /// </summary>
        protected Dictionary<string, string> m_additional_packages = new Dictionary<string, string>();

        protected Mission m_mission;

        protected List<Bonus> bonuses;

        protected int m_current_world_id;
        protected BonusAwarded m_last_levelup_bonus_awarded;

        protected Action m_On_Mode_Changed;

        protected VersionData m_version_data;
        protected LeaderboardsModel m_leaderboards;// = new LeaderboardsModel ();
        protected LeaderboardReward m_leaderboardReward = new LeaderboardReward();

        protected Dictionary<string, int> m_winsInRow = new Dictionary<string, int>();

        protected ActiveMatch m_active_match;
        protected ArenaMatch m_arena_match = null;

        protected RoundData m_roundData;

        private List<VouchersGroup> m_vouchers = new List<VouchersGroup>();

        // Hold an array of inventory items for the user.
        // Inventory items are Avatar parts that a user can "buy"
        // Created as part of the avatars part sale POC (April 2019)
        protected ItemsInventory m_inventory;

        private ContestsList m_contestList = new ContestsList();

        MAvatarData m_avatarData = new MAvatarData();

        protected Dictionary<string, ActiveArena> m_active_arenas;

        public static ModelManager Instance;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;

            // init the model
           // m_avatarModel = MAvatarController.Instance.CreateRandomAvatarModel(MAvatarModel.GenderType.Male);
        }

        public void AddActiveArena(ActiveArena active_arena)
        {
            string temp_key = active_arena.GetArena().GetID();
            m_active_arenas[temp_key] = active_arena;
        }

        public void SetActiveArenas(Dictionary<string, ActiveArena> active_arena_dictionary)
        {
            m_active_arenas = active_arena_dictionary;
        }

        public ActiveArena GetActiveArena(string id)
        {
            ActiveArena temp_active_arena = null;
            m_active_arenas.TryGetValue(id, out temp_active_arena);
            return temp_active_arena;
        }


        /// <summary>
        /// implement this to case User to specific game user to gain access to its stats.
        /// </summary>
        /// <param name="user"></param>
        public abstract void SetUser(User user);

        public LeaderboardsModel Leaderboards
        {
            get
            {
                return m_leaderboards;
            }
            set
            {
                m_leaderboards = value;
            }
        }

        internal bool UpdatePiggy(EndGamePiggyData endGamePiggyData)
        {
            return m_piggy.UpdateAfterMatch(endGamePiggyData);
        }

        internal void SetPiggy(UserPiggyData userPiggyData)
        {
            m_piggy = userPiggyData;
        }


        public UserPiggyData GetPiggy()
        {
            return m_piggy;
        }

        /// <summary>
        /// add external package for mobile purchase
        /// </summary>
        /// <param name="key">internal package ID</param>
        /// <param name="external">external package ID</param>
        public void AddExternalPackage(string key, string external)
        {
            if (string.IsNullOrEmpty(key))
                throw new Exception("external package has invalid internal package ID");
            if (string.IsNullOrEmpty(external))
                throw new Exception("external package has invalid external package ID");
            if (m_additional_packages.ContainsKey(key))
            {
                if (m_additional_packages[key] != external)
                    LoggerController.Instance.LogWarningFormat("external package {0} overriden its value {1} with {2}. it may be an error", key, m_additional_packages[key], external);
            }
            m_additional_packages[key] = external;
        }

        public Dictionary<string, string> GetExternalPackages()
        {
            return m_additional_packages;
        }

        /// <summary>
        /// removes an entry from additional package by the internal ID. if none provide clear the whole dictionary
        /// </summary>
        /// <param name="packageID">the internal package</param>
        public void PurgeExternalPackage(string packageID = "")
        {
            if (packageID == "")
                m_additional_packages.Clear();
            else
            {
                if(m_additional_packages.Remove(packageID) == false)
                    LoggerController.Instance.LogWarningFormat("tried to remove external package {0} with no entry in the dictionary, it's probably an error", packageID);
            }
        }

        public void SetCashier(Cashier cashier)
        {
            m_cashier = cashier;
        }

        public Cashier GetCashier()
        {
            return m_cashier;
        }


        public User GetUser()
        {
            return m_user;
        }

        public World GetWorldById(string worldId)
        {
            return m_worlds.Find(w => w.GetID() == worldId);
        }

        public List<World> Worlds
        {
            get
            {
                return m_worlds;
            }
            set
            {
                m_worlds = value;
            }
        }


        public void SetTable(Table table)
        {
            m_table = table;
        }

        public Table GetTable()
        {
            return m_table;
        }


        public BonusAwarded Last_levelup_bonus_awarded
        {
            get
            {
                return m_last_levelup_bonus_awarded;
            }
            set
            {
                m_last_levelup_bonus_awarded = value;
            }
        }



        public Bonus FindBonus(Bonus.BonusTypes type, int sub_type = 0)
        {
            if (bonuses == null) return null;
            foreach (Bonus item in bonuses)
            {
                if (item.Type == type && item.SubType == sub_type)
                    return item;
            }
            return null;
        }


        public List<Bonus> Bonuses
        {
            get
            {
                return bonuses;
            }
            set
            {
                bonuses = value;
            }
        }

        public Action On_Mode_Changed
        {
            get
            {
                return m_On_Mode_Changed;
            }
            set
            {
                m_On_Mode_Changed = value;
            }
        }

        public SocialModel Social_model
        {
            get
            {
                return m_social_model;
            }
            set
            {
                m_social_model = value;
            }
        }

        public ActiveMatch GetActiveMatch()
        {
            return m_active_match;
        }

        public void SetActiveMatch(ActiveMatch match)
        {
            m_active_match = match;

        }

        public ArenaMatch GetArenaMatch()
        {
            return m_arena_match;
        }

        public VersionData Version_data
        {
            get
            {
                return m_version_data;
            }
            set
            {
                m_version_data = value;
            }
        }

        public LeaderboardReward LeaderboardReward
        {
            get
            {
                return m_leaderboardReward;
            }
            set
            {
                m_leaderboardReward = value;
            }
        }


        public Dictionary<string, int> WinsInRow
        {
            get
            {
                return m_winsInRow;
            }

            set
            {
                m_winsInRow = value;
            }
        }


        public virtual void SetMission(Mission mission)
        {
            m_mission = mission;
        }

        public virtual Mission GetMission()
        {
            return m_mission;
        }

       
        public RoundData RoundData
        {
            get
            {
                return m_roundData;
            }

            set
            {
                m_roundData = value;
            }
        }

       

        public void RemoveActiveArena(string id)
        {
            m_active_arenas.Remove(id);
        }

        public ItemsInventory Inventory { get => m_inventory; set => m_inventory = value; }

        public ContestsList ContestsLists
        {
            get => m_contestList;
        }

        public string InitialWorldId { get => m_initialWorldId; set => m_initialWorldId = value; }

        public List<VouchersGroup> Vouchers { get => m_vouchers; set => m_vouchers = value; }
        public MAvatarData AvatarData { get => m_avatarData;  }

        public abstract Arena GetArena(string arena_id);

        public abstract SingleMatch GetSingleMatch(string match_id);

    }
}