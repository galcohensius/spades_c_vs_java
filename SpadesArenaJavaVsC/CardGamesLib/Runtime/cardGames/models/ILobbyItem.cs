﻿
namespace cardGames.models
{
	public interface ILobbyItem
	{
        bool IsSingleMatch ();

        int GetLevel ();

        int GetUnlockLevel();

        World World {
            get;
        }
	}
}

