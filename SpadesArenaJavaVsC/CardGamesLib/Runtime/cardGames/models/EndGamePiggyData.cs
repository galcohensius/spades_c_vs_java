﻿namespace cardGames.models
{
    //the same response data for single matach and arena match
    public class EndGamePiggyData
    {
        public int PiggyCoinsDelta { get; private set; }
        public int PiggyPlanID { get; private set; }
        public PiggyState PiggyState { get; private set; }
        public int PiggyTotalCoins { get; private set; }

        public EndGamePiggyData(int piggyCoinsDelta, int piggyPlanID, int piggyState, int piggyTotalCoins)
        {
            PiggyCoinsDelta = piggyCoinsDelta;
            PiggyPlanID = piggyPlanID;
            PiggyState = (PiggyState)piggyState;
            PiggyTotalCoins = piggyTotalCoins;
        }

    }
}