﻿using common.controllers;
using common.models;

namespace cardGames.models
{
    /// <summary>
    /// should reflect the basic info of the user agnostic of game.
    /// to hold game specific info derive from this class e.g. SpadesUser
    /// </summary>
    public abstract class User : CommonUser
    {
        protected UserStats m_stats;

        protected MAvatarModel m_mavatarModel;

        public UserStats Stats { get => m_stats; set => m_stats = value; }

        public User() : base()
        { }

        public MAvatarModel GetMAvatar()
        {
            return m_mavatarModel;
        }

        public void SetMAvatar(MAvatarModel model)
        {
            if (m_mavatarModel != null)
                LoggerController.Instance.LogError("AvatarModel cannot be set twice for User");

            m_mavatarModel = model;
        }

    }
}