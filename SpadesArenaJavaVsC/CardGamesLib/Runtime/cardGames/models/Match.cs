﻿namespace cardGames.models
{
    /// <summary>
    /// This is the base class for all matches types (currently single and arena) for all games (spades, rummy).
    /// further specialization should be implemented as match type (e.g. SingleMatch, ArenaMatch) and then a concrete model 
    /// for a specific game with any additional data (in the class itself or with interfaces e.g. SpadesSingleMatch, SpadesArenaMatch)
    /// </summary>
    public abstract class Match
    {
        private string world_id;
        private int m_min_level;
        protected int m_unlocked_level;
        protected string m_id;
        protected float m_play_delay;
        protected int m_buy_in;

//        public World World { get; set; }
        public string Graphics_path { get; set; }


        public string World_id { get => world_id; set => world_id = value; }
        public int Min_level { get => m_min_level; set => m_min_level = value; }
        public float PlayDelay { get => m_play_delay; set => m_play_delay = value; }

        public virtual int Buy_in { get => m_buy_in; set => m_buy_in = value; }
       

        public string GetID()
        {
            return m_id;
        }

        public void SetID(string id)
        {
            m_id = id;
        }

        public int GetLevel()
        {
            return Min_level;
        }

        public int GetUnlockLevel()
        {
            return m_unlocked_level;
        }

        public void SetUnlockLevel(int level)
        {
            m_unlocked_level = level;
        }
    }
}