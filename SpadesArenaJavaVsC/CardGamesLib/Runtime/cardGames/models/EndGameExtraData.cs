﻿using cardGames.models.contests;
using common.models;

namespace cardGames.models
{
    public class EndGameExtraData
    {
        int m_coinsPayout;
        int m_xpPayout;
        int m_newCoinsBalance;
        int m_newXpBalance;
        int m_coinsForPointsPrize;
        LevelData m_levelData;
        int m_newLeaderboardId;
        Challenge m_missionUpdate;
        Cashier m_cashier;
        Contest m_contest;

        public EndGameExtraData(int coins_payout, int xp_payout, int new_coins_balance, int new_xp_balance,int coinsForPointsPrize, LevelData level_data, int new_leaderboard_id, Challenge challenge, Cashier cashier, Contest contest)
        {
            m_coinsPayout = coins_payout;
            m_xpPayout = xp_payout;
            m_newCoinsBalance = new_coins_balance;
            m_newXpBalance = new_xp_balance;
            m_coinsForPointsPrize = coinsForPointsPrize;
            m_levelData = level_data;
            m_newLeaderboardId = new_leaderboard_id;
            m_missionUpdate = challenge;
            m_cashier = cashier;
            m_contest = contest;

        }



        public int CoinsPayout
        {
            get
            {
                return m_coinsPayout;
            }
        }

        public int XpPayout
        {
            get
            {
                return m_xpPayout;
            }
        }

        public int NewCoinsBalance
        {
            get
            {
                return m_newCoinsBalance;
            }
        }

        public int NewXpBalance
        {
            get
            {
                return m_newXpBalance;
            }
        }

        public LevelData LevelData
        {

            get
            {
                return m_levelData;
            }
        }

        public int NewLeaderboardId
        {
            get
            {
                return m_newLeaderboardId;
            }
        }

        public Challenge MissionUpdate
        {
            get
            {
                return m_missionUpdate;
            }

        }

        public Cashier Cashier
        {
            get
            {
                return m_cashier;
            }
        }

        public int CoinsForPointsPrize { get => m_coinsForPointsPrize; set => m_coinsForPointsPrize = value; }
        public Contest Contest { get => m_contest; set => m_contest = value; }
    }
}