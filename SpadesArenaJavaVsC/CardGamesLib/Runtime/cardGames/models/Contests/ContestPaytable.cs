﻿using System.Collections.Generic;

namespace cardGames.models.contests
{
    /*
    * This class holds all the contest paytable rewards
    */
    public class ContestPaytable
    {
        #region Private Members
        private int id;
        private List<ContestPaytableColumn> m_payCols;
        #endregion

        //<summary>
        // Default constrator
        //</summary> 
        public ContestPaytable()
        {
            m_payCols = new List<ContestPaytableColumn>();
        }

        public List<ContestPaytableColumn> PayColumns { get { return m_payCols; } }

        public int Id { get => id; set => id = value; }
    }


    public class ContestPaytableColumn
    {
        #region Private Members
        int m_minPlayers;
        int m_maxPlayers;
        private List<ContestPaytableLine> m_payLines;
        #endregion

        public ContestPaytableColumn()
        {
            m_payLines = new List<ContestPaytableLine>();
        }

        public List<ContestPaytableLine> PayLines { get => m_payLines; }
        public int MinPlayers
        {
            get => m_minPlayers;
            set
            {
                m_minPlayers = value; if (value == 1) m_minPlayers = 0; // Imagine Oleg is sending 0
            }
        }
        public int MaxPlayers { get => m_maxPlayers; set => m_maxPlayers = value; }

        public ContestPaytableLine LastLine
        {
            get
            {
                return PayLines.FindLast(p => true);
            }
        }
    }
}

