﻿namespace cardGames.models.contests
{
    /*
     * Takes care of the Contest
    */
    public class ContestPlayer
    {
        #region Private Members

        private int m_id;

       // private AvatarModel m_avatar_data;
        private MAvatarModel m_avatar_data;

        private int m_rank;
        private int m_points;
        private int m_prize;
        private bool m_is_me;

        private RankMovement m_rank_movement = RankMovement.None;
        #endregion

        #region public Members
        public enum RankMovement
        {
            None,
            Up,
            Down
        };
        #endregion

        //<summary>
        // Gets all the main parameters and initalizes the class
        //</summary> 
        public ContestPlayer(int id, int rank, int points, MAvatarModel avatar_data, bool isMe)
        {
            // should be ranked last
            Rank_movement = RankMovement.None;
            m_id = id;
            m_rank = rank;
            m_points = points;
            //m_prize = prize;
            m_avatar_data = avatar_data;
            m_is_me = isMe;
        }

        public MAvatarModel AvatarData { get { return m_avatar_data; } }
        public int Id { get { return m_id; } set { m_id = value; } }
        public int Rank { get { return m_rank; } set { m_rank = value; } }
        public int Points { get { return m_points; } set { m_points = value; } }
        public int Prize { get { return m_prize; } set { m_prize = value; } }
        public bool IsMe { get { return m_is_me; } set { m_is_me = value; } }
        public RankMovement Rank_movement { get => m_rank_movement; set => m_rank_movement = value; }
    }
}
