﻿namespace cardGames.models.contests
{
    /*
     * This class is taking care of the paytable line object (holds the minimum, maximum and the prize for when the contest ends) 
     */
    public class ContestPaytableLine
    {
        #region Private Members
        private int m_minRank;
        private int m_maxRank;
        private float m_prize;
        private bool m_isLast;
        #endregion

        //<summary>
        // Gets and Sets all the min rank
        //</summary> 
        public int MinRank
        {
            get { return m_minRank; }
            set { m_minRank = value; }
        }

        //<summary>
        // Gets and Sets all the max rank
        //</summary> 
        public int MaxRank
        {
            get { return m_maxRank; }
            set { m_maxRank = value; }
        }

        //<summary>
        // Gets and Sets all the prize
        //</summary> 
        public float PrizePercentage
        {
            get { return m_prize; }
            set { m_prize = value; }
        }

        public bool IsLast { get => m_isLast; set => m_isLast = value; }
    }
}
