﻿using common.controllers;
using common.models;
using System;
using System.Collections.Generic;

namespace cardGames.models.contests
{
    /*
    * This class is taking care of the indevidual contest.
    * It holds all of it's relevant data.
    */
    public class Contest
    {
        #region Public Members
        public enum ContestStatus
        {
            Completed = 0,
            Ongoing,
            Future
        };

        public Action OnContestPlayersUpdated;
        public Action OnContestDynamicDataUpdated;
        #endregion

        #region Private Members
        private int m_id;
        private ContestStatus m_status;

        private int m_duration_sec; // endtime - starttime in seconds
        private DateTime m_time_to_switch_status; // ongoing > complete, future > ongoing

        private int m_grace_time;
        private int m_rank;
        private int m_total_players;

        private List<ContestPlayer> m_contest_players_mini_leaderboard_rank = new List<ContestPlayer>();
        private List<ContestPlayer> m_contest_players_mini_leaderboard_top3 = new List<ContestPlayer>();
        private List<ContestPlayer> m_contest_players;

        private ContestPaytable m_contest_paytable;
        private ContestJackpot m_contest_jackpot = new ContestJackpot();
        private List<int> m_paytable_Ids;
        private int m_endTime;
        private bool m_remind_me = true;
        private float m_winFactor;
        private float m_loseFactor; // if zero - win 1 game
        private TableFilterData m_tableFilterData;
        #endregion

        public void UpdateDynamicData(int curr_jackpot_value, int rank, int total_players)
        {
            m_rank = rank;
            m_total_players = total_players;

            Contest_jackpot.UpdateContestJackpot(curr_jackpot_value);

            if (OnContestDynamicDataUpdated != null)
                OnContestDynamicDataUpdated();
        }

        public void SetContestJackpot(int prev_value, int curr_value)
        {
            Contest_jackpot.InitContestJackpot(prev_value, curr_value);
        }

        public List<ContestPlayer> ContestPlayers { get { return Contest_players; } private set { Contest_players = value; } }

        public ContestStatus Status { get { return m_status; } set { m_status = value; } }
        public int Id { get { return m_id; } set { m_id = value; } }
        public int GraceTime { get { return m_grace_time; } set { m_grace_time = value; } }

        public List<ContestPlayer> Contest_players_mini_leaderboard_rank
        {
            get => m_contest_players_mini_leaderboard_rank;
            set { m_contest_players_mini_leaderboard_rank = value; }
        }
        public List<ContestPlayer> Contest_players_mini_leaderboard_top3 { get => m_contest_players_mini_leaderboard_top3; set => m_contest_players_mini_leaderboard_top3 = value; }
        public List<ContestPlayer> Contest_players
        {
            get => m_contest_players;
            set
            {
                m_contest_players = value;
                if (m_contest_players != null)
                    OnContestPlayersUpdated?.Invoke();
            }
        }
        public bool Is_calculating_results
        {
            get
            {
                return m_status == ContestStatus.Ongoing && m_time_to_switch_status < DateTime.Now;
            }
        }
        public ContestPaytable Contest_Paytable { get => m_contest_paytable; set => m_contest_paytable = value; }
        public ContestJackpot Contest_jackpot { get => m_contest_jackpot; set => m_contest_jackpot = value; }
        public int Rank { get => m_rank; set => m_rank = value; }
        public int Total_players { get => m_total_players; set => m_total_players = value; }
        public int Pool_prize { get => Contest_jackpot.Current_value; }
        public int Duration_sec { get => m_duration_sec; set => m_duration_sec = value; }
        public DateTime Time_to_switch_status { get => m_time_to_switch_status; set => m_time_to_switch_status = value; }
        public List<int> Paytable_Ids { get => m_paytable_Ids; set => m_paytable_Ids = value; }
        public int Coins_paytable_id { get => m_paytable_Ids[0]; }
        public float WinFactor { get => m_winFactor; set => m_winFactor = value; }
        public float LoseFactor { get => m_loseFactor; set => m_loseFactor = value; }

        public bool Remind_me { get => m_remind_me; set => m_remind_me = value; }

        public int EndTime { get => m_endTime; set => m_endTime = value; }
        public TableFilterData TableFilterData { get => m_tableFilterData; set => m_tableFilterData = value; }
    }

    public class ContestJackpot
    {
        int m_curr_value = -1;
        int m_prev_value = -1;
        int m_temp_value = -1;
        DateTime m_data_update_time;

        public int Current_value { get => m_curr_value; set => m_curr_value = value; }
        public int Temporary_value { get => m_temp_value; set => m_temp_value = value; }
        public int Prev_value { get => m_prev_value; set => m_prev_value = value; }
        public DateTime Data_update_time { get => m_data_update_time; set => m_data_update_time = value; }

        public void InitContestJackpot(int prev_value, int curr_value)
        {
            m_curr_value = curr_value;
            m_prev_value = prev_value;
            m_data_update_time = DateTime.Now;
        }

        public void UpdateContestJackpot(int new_curr_value)
        {
            m_prev_value = m_curr_value;
            m_curr_value = new_curr_value;
            m_data_update_time = DateTime.Now;
            LoggerController.Instance.Log(LoggerController.Module.Contest + "UpdateContestJackpot{ m_prev_value: " + m_prev_value + "m_curr_value: " + m_curr_value + "m_data_update_time: " + m_data_update_time + "}");
        }

        public void SetTemporaryValue(int tempvalue)
        {
            m_temp_value = tempvalue;
        }
    }

    public class ActiveContest
    {
        private int m_startGameRank;
        private int m_startGamePoints;
        private ContestPlayer m_current_player_in_contest;

        private Dictionary<int, int> m_full_player_ids_snapshot; // Used for the movement of the arrows (full leaderboard)
        private Dictionary<int, int> m_top3_player_ids_snapshot; // Used for the movement of the arrows (mini leaderboard)
        private Dictionary<int, int> m_rank_player_ids_snapshot; // Used for the movement of the arrows (mini leaderboard)

        public int PointsDelta()
        {
            if (Current_playar_in_contest != null)
                return Current_playar_in_contest.Points - StartGamePoints;
            return 0;
        }

        public int StartGameRank { get => m_startGameRank; set => m_startGameRank = value; }
        public int StartGamePoints { get => m_startGamePoints; set => m_startGamePoints = value; }
        public Dictionary<int, int> Full_player_ids_snapshot { get => m_full_player_ids_snapshot; set => m_full_player_ids_snapshot = value; }
        public Dictionary<int, int> Top3_player_ids_snapshot { get => m_top3_player_ids_snapshot; set => m_top3_player_ids_snapshot = value; }
        public Dictionary<int, int> Rank_player_ids_snapshot { get => m_rank_player_ids_snapshot; set => m_rank_player_ids_snapshot = value; }
        public ContestPlayer Current_playar_in_contest { get => m_current_player_in_contest; set => m_current_player_in_contest = value; }
    }

}
