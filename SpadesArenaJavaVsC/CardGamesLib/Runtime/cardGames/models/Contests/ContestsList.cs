﻿using System;
using System.Collections.Generic;
using System.Linq;

/* 
 * This class is taking care of all of the contests ;
 * ongoing, future and completed
 */
namespace cardGames.models.contests
{
    public class ContestsList
    {
        #region Private Members
        private List<Contest> m_completed_contests;
        private List<Contest> m_ongoing_contests;
        private List<Contest> m_future_contests;
        private List<Contest> m_all_contests;

        private Dictionary<int, ActiveContest> m_activeContests;
        #endregion

        public Action OnContestDataChanged;


        //<summary>
        // Gets all the main parameters and initalizes the class
        //</summary> 
        public ContestsList()
        {
            m_completed_contests = new List<Contest>();
            m_ongoing_contests = new List<Contest>();
            m_future_contests = new List<Contest>();
            m_all_contests = new List<Contest>();

            m_activeContests = new Dictionary<int, ActiveContest>();
        }


        public void AddContestToList(Contest contest)
        {
            switch (contest.Status)
            {
                case Contest.ContestStatus.Completed:
                    m_completed_contests.Add(contest);
                    break;
                case Contest.ContestStatus.Ongoing:
                    m_ongoing_contests.Add(contest);
                    break;
                case Contest.ContestStatus.Future:
                    m_future_contests.Add(contest);
                    break;
            }
            m_all_contests = m_ongoing_contests.Concat(m_future_contests).Concat(m_completed_contests).ToList();
        }

        public void FireContestsDataChangedEvent()
        {
            OnContestDataChanged?.Invoke();
        }

        public Contest GetContestByID(int id)
        {
            return AllContests.Find(c => c.Id == id);
        }

        public void Clear()
        {
            m_completed_contests.Clear();
            m_ongoing_contests.Clear();
            m_future_contests.Clear();
            m_all_contests.Clear();
        }

        public ActiveContest GetActiveContest(int id)
        {
            ActiveContest result = null;

            if (!m_activeContests.TryGetValue(id, out result))
            {
                result = new ActiveContest();
                m_activeContests[id] = result;
            }
           
            return result;
        }

        public void ClearActiveContests()
        {
            m_activeContests.Clear();
        }

        public List<Contest> OngoingContests { get { return m_ongoing_contests; }}
        public List<Contest> FutureContests { get { return m_future_contests; } }
        public List<Contest> CompletedContests { get { return m_completed_contests; }  }
        public List<Contest> AllContests { get { return m_all_contests; } }
       
    }
}