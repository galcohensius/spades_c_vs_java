﻿using common.ims.model;

namespace cardGames.models
{
    /// <summary>
    /// Base class of SpadesWorld to be implemented with extra data for Spades and Gin.
    /// </summary>
    public abstract class World
    {
        public enum WorldTypes
        {
            Regular,
            Promo,
            Virtual
        }


        string m_id;
        int m_index;
        string m_name;

        WorldTypes m_worldType;

        int m_curr_step_single_progress;
        int m_curr_single_step = 1;

        int m_arena_gem_curr_step;
        int m_arena_gem_total_steps;
        int m_arena_gems_won;

        IMSInteractionZone m_promo_iZone = null;

        public World()
        {
        }

        public abstract Arena GetArenaByID(string id);



        public string GetID()
        {
            return m_id;
        }

        public void SetId(string id)
        {
            m_id = id;
        }

        public string GetName()
        {
            return m_name;
        }

        public void SetName(string name)
        {
            m_name = name;
        }











        public int Index
        {
            get
            {
                return m_index;
            }
            set
            {
                m_index = value;
            }
        }



        public abstract int LowestUnlockLevel
        {
            get;
        }

        public int Curr_Single_step_progress
        {
            get
            {
                return m_curr_step_single_progress;
            }
            set
            {
                m_curr_step_single_progress = value;
            }
        }

        public int Curr_Single_step
        {
            get
            {
                return m_curr_single_step;
            }
            set
            {
                m_curr_single_step = value;
            }
        }


        public int Arena_Gem_Curr_Step
        {
            get
            {
                return m_arena_gem_curr_step;
            }
            set
            {
                m_arena_gem_curr_step = value;
            }
        }

        public int Arena_Gem_Total_Steps
        {
            get
            {
                return m_arena_gem_total_steps;
            }
            set
            {
                m_arena_gem_total_steps = value;
            }

        }

        public int Arena_Gems_Won
        {
            get
            {
                return m_arena_gems_won;
            }
            set
            {
                m_arena_gems_won = value;
            }
        }

        public WorldTypes WorldType { get => m_worldType; set => m_worldType = value; }
        public IMSInteractionZone Promo_iZone { get => m_promo_iZone; set => m_promo_iZone = value; }
    }
}