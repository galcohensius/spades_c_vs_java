﻿using System;

namespace cardGames.models
{
    /// <summary>
    /// abstraction for ActiveArena for cardGames layer.
    /// </summary>
    public abstract class ActiveArena
    {
        protected int m_curr_stage;
        protected Arena m_arena;
        protected bool m_arena_bought;

        public event Action<int> OnStageChange;
        public event Action OnBought;

        public ActiveArena()
        {
        }

        public ActiveArena(Arena arena)
        {
            m_arena = arena;
        }



        public void SetBought(bool value)
        {
            m_arena_bought = value;
            if (OnBought != null)
                OnBought();
        }

        public bool GetBought()
        {
            return m_arena_bought;
        }



        public void SetArena(Arena arena)
        {
            m_arena = arena;

        }

        public Arena GetArena()
        {
            return m_arena;
        }

        public int GetCurrStage()
        {
            return m_curr_stage;
        }

        public void SetCurrStage(int curr_stage)
        {
            m_curr_stage = curr_stage;
            if (OnStageChange != null)
                OnStageChange(m_curr_stage);
        }


        /// <summary>
        /// upcasts ActiveArena to a specific game Active Arena
        /// </summary>
        /// <typeparam name="T">type of Active Arena</typeparam>
        /// <typeparam name="U">type of SpadesSingleMatch</typeparam>
        /// <returns></returns>
        //public T ConvertToDerived<T>() where T : ActiveArena, new()
        //{
        //    T t = new T();
        //    t.m_curr_stage = GetCurrStage();
        //    t.m_arena = GetArena();
        //    t.m_arena_bought = GetBought(); 
        //    //if isn't working try the User way of implementation.
        //    t.OnStageChange += OnStageChange;
        //    t.OnBought += OnBought;

        //    return t;
        //}

        /// <summary>
        /// returns a new dictionary with converted values from ActiveArena to a derived SpadesActiveArena.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="worldsDict"></param>
        /// <returns></returns>
        //public static Dictionary<string, T> ConvertToDerived<T>(Dictionary<string, ActiveArena> activeArenas) where T : ActiveArena, new()
        //{
        //    Dictionary<string, T> convertedDict = new Dictionary<string, T>();
        //    foreach (var activeArena in activeArenas)
        //    {
        //        convertedDict.Add(activeArena.Key, activeArena.Value.ConvertToDerived<T>());
        //    }

        //    return convertedDict;
        //}
    }
}