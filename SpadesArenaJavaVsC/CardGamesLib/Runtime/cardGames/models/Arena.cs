﻿
using common.ims.model;
using System.Collections.Generic;

namespace cardGames.models
{
    /// <summary>
    /// Base class of Arena to be implemented with extra data for Spades and Gin
    /// </summary>
    public abstract class Arena: ILobbyItem
    {
        protected int m_buy_in;
        protected int m_payout_coins; //AKA reward - dupe by stage??!?!
        protected int m_level;
        protected string m_name;
        protected string m_id;
        protected int m_unlock_level;
        protected World m_world;
        
        protected int m_threshold;

        protected int m_curr_rebuy_num;
        protected List<ArenaMatch> m_arena_matches = new List<ArenaMatch>();

        int m_current_step;
        int m_current_stage;
        IMSGoodsList m_goodsList = new IMSGoodsList();

        public void SetAvatarItem(int inventoryItem)
        {
            m_goodsList.InventoryItems.Add(inventoryItem.ToString());
        }

        public List<string> GetAvatarItems()
        {
            return m_goodsList.InventoryItems;
        }

        public void SetThreshold(int threshold)
        {
            m_threshold = threshold;
        }

        public int GetThreshold()
        {
            return m_threshold;
        }

        public int GetBuyInCoins()
        {
            return m_buy_in;
        }

        public void SetBuyInCoins(int buyIn)
        {
            m_buy_in = buyIn;
        }

        public void SetBuyInMulti(int buyIn)
        {
            m_buy_in = buyIn;
        }

        public int GetPayOutCoins()
        {
            return m_payout_coins;
        }

        public void SetPayOutCoins(int payout)
        {
            m_payout_coins = payout;
        }

        public int GetLevel()
        {
            return m_level;
        }

        public void SetLevel(int level)
        {
            m_level = level;
        }

        public string GetName()
        {
            return m_name;
        }

        public void SetName(string name)
        {
            m_name = name;
        }

        public string GetID()
        {
            return m_id;
        }

        public void SetId(string id)
        {
            m_id = id;
        }

        public int GetUnlockLevel()
        {
            return m_unlock_level;
        }

        public void SetUnlockLevel(int level)
        {
            m_unlock_level = level;
        }

        public World World
        {
            get
            {
                return m_world;
            }
            set
            {
                m_world = value;
            }
        }

        public int Curr_rebuy_num
        {
            get
            {
                return m_curr_rebuy_num;
            }
            set
            {
                m_curr_rebuy_num = value;
            }
        }

        public ArenaMatch GetArenaMatch(int index)
        {
            return Arena_matches[index - 1];
        }

        public int GetNumStages()
        {
            return Arena_matches.Count;
        }

        public bool IsSingleMatch()
        {
            return false;
        }

        public List<ArenaMatch> Arena_matches { get => m_arena_matches; set => m_arena_matches = value; }
        public int Current_step { get => m_current_step; set => m_current_step = value; }
        public int Current_stage { get => m_current_stage; set => m_current_stage = value; }
        public IMSGoodsList GoodsList { get => m_goodsList; set => m_goodsList = value; }
    }
}