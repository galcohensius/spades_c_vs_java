﻿namespace cardGames.models
{
	public class VersionData
	{
		private int m_version;
		private bool m_force_update;

        private bool m_show_age_verification;

		public VersionData ()
		{
			m_version = 0;
			m_force_update = false;
		}

		public VersionData (int version,bool forceUpdate)
		{
			m_version = version;
			m_force_update = forceUpdate;

		}

		public int Version
		{
			get{ return m_version; }
			set{ m_version = value; }
		}

		public bool ForceUpdate {
			get{ return m_force_update; }
			set{ m_force_update = value; }
		}

        public bool ShowAgeVerification
        {
            get
            {
                return m_show_age_verification;
            }

            set
            {
                m_show_age_verification = value;
            }
        }

        public override string ToString()
        {
            return string.Format("[VersionData: version={0}, forceUpdate={1}]", Version, ForceUpdate);
        }
	}


}

