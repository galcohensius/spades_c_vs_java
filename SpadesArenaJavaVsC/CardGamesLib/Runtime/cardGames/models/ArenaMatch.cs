﻿using System.Collections.Generic;

namespace cardGames.models
{
    public abstract class ArenaMatch : Match
    {
        private Arena arena;
        private List<int> rebuys = new List<int>();
        private int stage_num;
        private int payout_coins; //AKA reward

        public virtual Arena Arena { get => arena ; set => arena = value; }
        public List<int> Rebuys { get => rebuys; set => rebuys = value; }
        public int Stage_num { get => stage_num; set => stage_num = value; }
        public int Payout_coins { get => payout_coins; set => payout_coins = value; }

        public int GetPayOutCoins()
        {
            return Payout_coins;
        }

        public override int Buy_in
        {
            get=> Arena.GetBuyInCoins();

            set { }
        }

        public void SetArena(Arena arena)
        {
            Arena = arena;
        }

        public Arena GetArena()
        {
            return Arena;
        }
    }
}