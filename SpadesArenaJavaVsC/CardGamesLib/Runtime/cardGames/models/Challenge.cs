﻿using cardGames.controllers;
using common.controllers;

namespace cardGames.models
{
    /// <summary>
    /// base class for all challenges. complete working implementation for reference can be found at spades.models.SpadesChallenge.
    /// not abstract to create concerte dummy instances to derive from
    /// </summary>
    public abstract class Challenge 
    {
        protected MissionController.ChallengeStatus m_status;

        protected int[] m_thresholds;
        protected int m_order;
        protected int m_bet;
        protected MissionController.BetOperator m_bet_operator;

        protected int m_prize;
        protected int m_swap_left;
        protected int m_progress;

        public Challenge()
        { }

        public Challenge(Challenge challenge)
        {
            m_status = challenge.m_status;

            m_thresholds = challenge.m_thresholds;

            m_order = challenge.m_order;

            m_prize = challenge.m_prize;

            m_progress = challenge.m_progress;

            m_swap_left = challenge.m_swap_left;

            m_bet = challenge.m_bet;
            m_bet_operator = challenge.m_bet_operator;
        }

        public Challenge(int order_id, int progress, int prize, MissionController.ChallengeStatus status)
        {
            Progress = progress;
            Order = order_id;
            m_prize = prize;
            m_status = status;

            LoggerController.Instance.Log("SpadesChallenge Update received: Progress: " + progress + " order: " + Order + " status: " + m_status);
        }

        public Challenge(MissionController.ChallengeStatus status, int[] thresholds, int order, int prize, int num_swapped, int bet, MissionController.BetOperator betOperator, int progress = 0)
        {
            m_status = status;

            //only 1 challenge can be active at same time
            //if completed then progress is 100
            //if swapped progress is 0 

            Progress = progress;


            m_thresholds = thresholds;

            Order = order;

            m_prize = prize;
            m_swap_left = num_swapped;

            m_bet = bet;
            m_bet_operator = betOperator;
        }

        public virtual void CopyStaticData(Challenge org_challenge)
        {
            m_thresholds = org_challenge.m_thresholds;
            m_swap_left = org_challenge.Swap_left;
            m_bet = org_challenge.m_bet;
            m_bet_operator = org_challenge.Bet_operator;
            m_prize = org_challenge.Prize;
        }

        public int[] Thresholds
        {
            get
            {
                return m_thresholds;
            }
        }

        public int Prize
        {
            get
            {
                return m_prize;
            }
        }

        public MissionController.ChallengeStatus Status
        {
            get
            {
                return m_status;
            }
            set
            {
                m_status = value;
            }
        }

        public int Order
        {
            get
            {
                return m_order;
            }

            set
            {
                m_order = value;
            }
        }

        public int Progress
        {
            get
            {
                return m_progress;
            }

            set
            {
                m_progress = value;
            }
        }

        public abstract int TotalProgress
        {
            get;
        }

        public int Bet
        {
            get
            {
                return m_bet;
            }
        }

        public MissionController.BetOperator Bet_operator
        {
            get
            {
                return m_bet_operator;
            }
        }

        public int Swap_left
        {
            get
            {
                return m_swap_left;
            }
        }
    }
}
