﻿namespace cardGames.models
{
    public abstract class SingleMatch : Match//, ILobbyItem
    {

        private int m_payout;
        private bool is_special;


        public int Payout { get => m_payout; set => m_payout = value; }
        public bool Is_special { get => is_special; set => is_special = value; }

        private bool m_active_in_challenge = false;


        public int GetPayOutCoins()
        {
            return Payout;
        }

        public bool IsSingleMatch()
        {
            return true;
        }

        public void SetPayOutCoins(int payout)
        {
            Payout = payout;
        }

        public bool Active_in_challenge { get => m_active_in_challenge; set => m_active_in_challenge = value; }
    }
}