﻿using common.controllers;
using System.Collections.Generic;

namespace cardGames.models
{
    /// <summary>
    /// Holds a list of item ids the user has / bought
    /// </summary>
    public class ItemsInventory 
    {
        private HashSet<string> m_itemIds = new HashSet<string>();


        public void AddItem(string id)
        {
            m_itemIds.Add(id);
            LoggerController.Instance.LogFormat("Item {0} added to user inventory", id);
        }

        public bool HasItem(string id)
        {
            return m_itemIds.Contains(id);
        }

        public HashSet<string> ItemIds { get => m_itemIds; }

    }

}