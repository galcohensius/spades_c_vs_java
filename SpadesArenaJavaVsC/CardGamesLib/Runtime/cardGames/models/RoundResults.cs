﻿namespace cardGames.models
{
    public abstract class RoundResults
    {
        protected int m_round_number;

        public void SetRoundNumber(int round_number)
        {
            m_round_number = round_number;
        }

        public int GetRoundNumber()
        {
            return m_round_number;
        }
    }
}