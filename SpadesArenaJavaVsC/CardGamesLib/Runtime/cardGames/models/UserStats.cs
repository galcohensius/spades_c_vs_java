﻿using System.Collections.Generic;

namespace cardGames.models
{
    /// <summary>
    /// each game need to implement this with its own unique features although most are common to all.
    /// NOTE: some stuff could accidently appear here when they are unique to a game or vice-versa. 
    /// feel free to fix as it was written after the completion of the common layer refactoring, during 
    /// the cardGames layer and before Gin full production.
    /// </summary>
    public abstract class UserStats
    {
        protected int saveTimeStamp;

        // Round related
        protected int roundPlayed;

        // Global
        protected int arenaPlayed;
        protected int coinsWon;

        protected int biggestCoinsWin;


        //	session base
        protected int sessionWins;
        protected int sessionLoses;
        protected int sessionWinsInRow;
        protected int sessionLosesInRow;

        protected List<int> m_FiveGamesState = new List<int>();

        public int ArenaPlayed
        {
            get
            {
                return this.arenaPlayed;
            }
            set
            {
                arenaPlayed = value;
            }
        }

        public int CoinsWon
        {
            get
            {
                return this.coinsWon;
            }
            set
            {
                coinsWon = value;
            }
        }



        public int BiggestCoinsWin
        {
            get
            {
                return biggestCoinsWin;
            }
            set
            {
                biggestCoinsWin = value;
            }
        }


        public int RoundPlayed
        {
            get
            {
                return roundPlayed;
            }
            set
            {
                roundPlayed = value;
            }
        }

        public int SessionWins
        {
            get
            {
                return sessionWins;
            }
            set
            {
                sessionWins = value;
            }
        }

        public int SessionLoses
        {
            get
            {
                return sessionLoses;
            }
            set
            {
                sessionLoses = value;
            }
        }

        public int SessionMatchedPlayed
        {
            get => sessionWins + sessionLoses;
        }

        public int SessionWinsInRow
        {
            get
            {
                return sessionWinsInRow;
            }
            set
            {
                sessionWinsInRow = value;
            }
        }

        public int SessionLosesInRow
        {
            get
            {
                return sessionLosesInRow;
            }
            set
            {
                sessionLosesInRow = value;
            }
        }

        public List<int> FiveGamesState
        {
            get
            {
                return m_FiveGamesState;
            }

            set
            {
                m_FiveGamesState = value;
            }
        }

        public int SaveTimeStamp
        {
            get
            {
                return saveTimeStamp;
            }

            set
            {
                saveTimeStamp = value;
            }
        }
    }
}