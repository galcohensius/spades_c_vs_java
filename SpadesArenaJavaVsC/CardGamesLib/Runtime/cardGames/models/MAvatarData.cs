﻿using cardGames.models;
using System.Collections.Generic;
using static common.utils.RandomExtensions;

namespace common.models
{
    public class MAvatarData
    {

        // F_M_1234

        Dictionary<string, MavatarItem> m_allAvatarData = new Dictionary<string, MavatarItem>();

        List<MavatarItem> m_Face_Male = new List<MavatarItem>();
        List<MavatarItem> m_Face_Female = new List<MavatarItem>();

        List<MavatarItem> m_Hair_Male = new List<MavatarItem>();
        List<MavatarItem> m_Hair_Female = new List<MavatarItem>();

        List<MavatarItem> m_Outfit_Male = new List<MavatarItem>();
        List<MavatarItem> m_Outfit_Female = new List<MavatarItem>();

        List<MavatarItem> m_Accesory_Male = new List<MavatarItem>();
        List<MavatarItem> m_Accesory_Female = new List<MavatarItem>();

        List<MavatarItem> m_Base_Male = new List<MavatarItem>();
        List<MavatarItem> m_Base_Female = new List<MavatarItem>();

        List<MavatarItem> m_Body_Male = new List<MavatarItem>();
        List<MavatarItem> m_Body_Female = new List<MavatarItem>();

        List<MavatarItem> m_Costume = new List<MavatarItem>();

        // This Dictionary of inventory items with inventory ids
        Dictionary<string, List<MavatarItem>> m_inventoryItems = new Dictionary<string, List<MavatarItem>>();

        private System.Random random = new System.Random();


        public void AddItemToModel(MavatarItem item)
        {
            MAvatarModel.ItemType itemType = item.ItemType;
            MAvatarModel.GenderType genderType = item.Gender;
            string dictID = item.ID;

            m_allAvatarData.Add(dictID, item);

            switch (itemType)
            {
                case MAvatarModel.ItemType.Face:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            m_Face_Male.Add(item);
                        else if (genderType == MAvatarModel.GenderType.Female)
                            m_Face_Female.Add(item);
                        else
                        {
                            m_Face_Male.Add(item);
                            m_Face_Female.Add(item);
                        }
                        break;
                    }
                case MAvatarModel.ItemType.Hair:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            m_Hair_Male.Add(item);
                        else if (genderType == MAvatarModel.GenderType.Female)
                            m_Hair_Female.Add(item);
                        else
                        {
                            m_Hair_Male.Add(item);
                            m_Hair_Female.Add(item);
                        }
                        break;
                    }
                case MAvatarModel.ItemType.Outfit:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            m_Outfit_Male.Add(item);
                        else if (genderType == MAvatarModel.GenderType.Female)
                            m_Outfit_Female.Add(item);
                        else
                        {
                            m_Outfit_Male.Add(item);
                            m_Outfit_Female.Add(item);
                        }
                        break;
                    }
                case MAvatarModel.ItemType.Accessory:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            m_Accesory_Male.Add(item);
                        else if (genderType == MAvatarModel.GenderType.Female)
                            m_Accesory_Female.Add(item);
                        else
                        {
                            m_Accesory_Male.Add(item);
                            m_Accesory_Female.Add(item);
                        }
                        break;
                    }
                case MAvatarModel.ItemType.Trophy:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            m_Base_Male.Add(item);
                        else if (genderType == MAvatarModel.GenderType.Female)
                            m_Base_Female.Add(item);
                        else
                        {
                            m_Base_Male.Add(item);
                            m_Base_Female.Add(item);
                        }
                        break;
                    }
                case MAvatarModel.ItemType.Body:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            m_Body_Male.Add(item);
                        else if (genderType == MAvatarModel.GenderType.Female)
                            m_Body_Female.Add(item);
                        else
                        {
                            m_Body_Male.Add(item);
                            m_Body_Female.Add(item);
                        }
                        break;
                    }
                case MAvatarModel.ItemType.Costume:
                    {
                        m_Costume.Add(item);
                        break;
                    }
            }

            if (!string.IsNullOrEmpty(item.InventoryID))
            {
                List<MavatarItem> items = null;

                if (!ModelManager.Instance.AvatarData.InventoryItems.TryGetValue(item.InventoryID, out items))
                {
                    items = new List<MavatarItem>();
                    ModelManager.Instance.AvatarData.InventoryItems[item.InventoryID] = items;
                }

                items.Add(item);

            }
        }

        public MavatarItem GetRandomItem(MAvatarModel.GenderType genderType, MAvatarModel.ItemType itemType, bool IsBot = true)
        {
            switch (itemType)
            {
                case MAvatarModel.ItemType.Face:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            return GetRandomItemFromList(m_Face_Male, IsBot);
                        else 
                            return GetRandomItemFromList(m_Face_Female, IsBot);
                        
                    }
                case MAvatarModel.ItemType.Hair:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            return GetRandomItemFromList(m_Hair_Male, IsBot);
                        else 
                            return GetRandomItemFromList(m_Hair_Female, IsBot);
                    }
                case MAvatarModel.ItemType.Outfit:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            return GetRandomItemFromList(m_Outfit_Male, IsBot);
                        else 
                            return GetRandomItemFromList(m_Outfit_Female, IsBot);
                    }
                case MAvatarModel.ItemType.Accessory:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            return GetRandomItemFromList(m_Accesory_Male, IsBot);
                        else 
                            return GetRandomItemFromList(m_Accesory_Female, IsBot);
                    }
                case MAvatarModel.ItemType.Trophy:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            return GetRandomItemFromList(m_Base_Male, IsBot);
                        else 
                            return GetRandomItemFromList(m_Base_Female, IsBot);
                    }
                case MAvatarModel.ItemType.Body:
                    {
                        if (genderType == MAvatarModel.GenderType.Male)
                            return GetRandomItemFromList(m_Body_Male, IsBot);
                        else 
                            return GetRandomItemFromList(m_Body_Female, IsBot);
                    }

                case MAvatarModel.ItemType.Costume:
                    {
                        return GetRandomItemFromList(m_Costume, IsBot);
                    }
            }

            return null;
        }

        private MavatarItem GetRandomItemFromList(List<MavatarItem> mavatarItems, bool IsBot)
        {
            int index = UnityEngine.Random.Range(0, mavatarItems.Count);

            if (mavatarItems.Count == 0)
                return new MavatarItem("");

            if (IsBot)
                return GetRandoBotItemFromList(mavatarItems);

            int currentLevel;
            // get random item from the ones I own
            if (ModelManager.Instance.GetUser() == null)
                currentLevel = 1;
            else
                currentLevel = ModelManager.Instance.GetUser().GetLevel();

            bool Owned;
            bool overLevel;
            int tried = mavatarItems.Count;

            do
            {
                if (string.IsNullOrEmpty(mavatarItems[index].InventoryID))
                {
                    Owned = true;
                }
                else
                {
                    Owned = ModelManager.Instance.Inventory?.HasItem(mavatarItems[index].InventoryID) ?? false;
                }

                overLevel = mavatarItems[index].MinLevel > currentLevel;

                if (!Owned || overLevel)
                {
                    tried--;
                    index++;
                    if (index == mavatarItems.Count)
                        index = 0;
                }
                else
                {
                    return mavatarItems[index];
                }
            }
            while (tried > 0);

            return null;
        }

        /// <summary>
        /// Logic is as follows:
        /// Each item has a percentage between 0-100
        /// 0 means it will never be selected
        /// 100 means it might be selected
        ///
        /// We shuffle the list, and then iterate while checking against a random number between 1-100
        /// If an item is found it is returned.
        /// </summary>
        private MavatarItem GetRandoBotItemFromList(List<MavatarItem> mavatarItems)
        {
            List<MavatarItem> shuffledList = new List<MavatarItem>(mavatarItems);
            random.Shuffle(shuffledList);

            int randNum = random.Next(100);

            foreach (MavatarItem item in shuffledList)
            {
                if (item.BotRandomPercentage > randNum)
                    return item;
            }

            return new MavatarItem("");
        }

        public MavatarItem GetItemByID(string prefabID)
        {
            MavatarItem mavatarItem = null;
            if (prefabID!=null) 
                m_allAvatarData.TryGetValue(prefabID, out mavatarItem);
            return mavatarItem;
        }

        public List<MavatarItem> Face_Male { get => m_Face_Male; set => m_Face_Male = value; }
        public List<MavatarItem> Face_Female { get => m_Face_Female; set => m_Face_Female = value; }
        public List<MavatarItem> Hair_Male { get => m_Hair_Male; set => m_Hair_Male = value; }
        public List<MavatarItem> Hair_Female { get => m_Hair_Female; set => m_Hair_Female = value; }
        public List<MavatarItem> Outfit_Male { get => m_Outfit_Male; set => m_Outfit_Male = value; }
        public List<MavatarItem> Outfit_Female { get => m_Outfit_Female; set => m_Outfit_Female = value; }
        public List<MavatarItem> Accesory_Male { get => m_Accesory_Male; set => m_Accesory_Male = value; }
        public List<MavatarItem> Accesory_Female { get => m_Accesory_Female; set => m_Accesory_Female = value; }
        public List<MavatarItem> Base_Male { get => m_Base_Male; set => m_Base_Male = value; }
        public List<MavatarItem> Base_Female { get => m_Base_Female; set => m_Base_Female = value; }
        public List<MavatarItem> Body_Male { get => m_Body_Male; set => m_Body_Male = value; }
        public List<MavatarItem> Body_Female { get => m_Body_Female; set => m_Body_Female = value; }
        public List<MavatarItem> Costume { get => m_Costume; set => m_Costume = value; }
        public Dictionary<string, MavatarItem> AllAvatarData { get => m_allAvatarData; }
        public Dictionary<string, List<MavatarItem>> InventoryItems { get => m_inventoryItems; }
    }
}

