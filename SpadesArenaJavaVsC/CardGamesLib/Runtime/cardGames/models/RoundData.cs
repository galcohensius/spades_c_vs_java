﻿namespace cardGames.models
{
    public abstract class RoundData
    {
        protected int gameTransId;
        protected int roundNum;
        protected string gameId;

        public int GameTransId
        {
            get
            {
                return gameTransId;
            }

            set
            {
                gameTransId = value;
            }
        }

        public int RoundNum
        {
            get
            {
                return roundNum;
            }

            set
            {
                roundNum = value;
            }
        }

        public string GameId
        {
            get
            {
                return gameId;
            }

            set
            {
                gameId = value;
            }
        }
    }

    public abstract class PlayerRoundData
    {
        protected int userId;

        public int UserId
        {
            get
            {
                return userId;
            }

            set
            {
                userId = value;
            }
        }
    }
}