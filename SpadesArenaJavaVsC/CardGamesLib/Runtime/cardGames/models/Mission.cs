﻿using cardGames.controllers;
using System;
using System.Collections.Generic;

namespace cardGames.models
{
    /// <summary>
    /// base class for missions. derived class only need to handle their games' challenges (by hiding) instead of
    /// using Challenge. see spades.models.SpadesMission for example.
    /// not abstract so concrete dummies could instantiated.
    /// </summary>
    public abstract class Mission
    {
        public Action ModelChanged;
        protected int m_active_challenge_index;
        protected List<Challenge> m_challenges = new List<Challenge>();

        protected DateTime m_end_date;

        protected int m_prize = 0;
        protected string m_mission_id = "";

        protected MissionController.MissionStatus m_status;

        /// <summary>
        /// don't instantiate with this CTor, used only to convert to a derived type
        /// </summary>
        public Mission()
        {

        }

        public int GetNumChallenges()
        {
            return m_challenges.Count;
        }

        public void AddChallenge(Challenge item)
        {
            m_challenges.Add(item);
        }

        public Mission(DateTime dateTime)
        {
            m_end_date = dateTime;
        }

        public void SetChallenge(int index, Challenge challenge)
        {
            m_challenges[index] = challenge;
        }

        public Challenge GetChallenge(int index)
        {
            if (m_challenges.Count - 1 < index)
                return null;

            return m_challenges[index];
        }

        public Challenge GetActiveChallenge()
        {
            if (m_challenges != null && m_challenges.Count - 1 >= Active_challenge_index)
                return m_challenges[Active_challenge_index];
            else
                return null;
        }

        public DateTime End_date
        {
            get
            {
                return m_end_date;
            }
        }


        public int Active_challenge_index
        {
            get
            {
                return m_active_challenge_index;
            }
            set
            {
                m_active_challenge_index = value;

                ModelChanged?.Invoke();
                
            }
        }

        public MissionController.MissionStatus Status
        {
            get
            {
                return m_status;
            }

            set
            {
                m_status = value;

                ModelChanged?.Invoke();
            }
        }

        public int Prize
        {
            get
            {
                return m_prize;
            }

            set
            {
                m_prize = value;
            }
        }

        public string Mission_id
        {
            get
            {
                return m_mission_id;
            }

            set
            {
                m_mission_id = value;
            }
        }
    }
}