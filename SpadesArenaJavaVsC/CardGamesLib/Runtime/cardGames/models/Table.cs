﻿namespace cardGames.models
{
    public abstract class Table
    {
        


        float time_left_on_timer = -1f;
        float total_time_on_timer = -1f;
        int dealer = -1;

       

        protected CardsList m_thrown_cards = new CardsList() { null, null, null, null };

        protected int m_curr_round = 0;

        protected int m_server_to_local_pos = 0;

        protected string m_game_id="";

        public abstract int ServerToLocalPos(int server_position);

        public abstract int LocalToServerPos(int local_position);

        public int GetRoundNumber()
        {
            return m_curr_round;
        }

        public void IncRoundNumber()
        {
            m_curr_round++;
        }

        public void SetRoundNumber(int round_number)
        {
            m_curr_round = round_number;
        }

        public abstract void SortPlayerCards();

        public int Server_to_local_pos
        {
            get
            {
                return m_server_to_local_pos;
            }
            set
            {
                m_server_to_local_pos = value;
            }
        }

        public string Game_id
        {
            get
            {
                return m_game_id;
            }
            set
            {
                m_game_id = value;
            }
        }

        public CardsList Thrown_cards
        {
            get
            {
                return this.m_thrown_cards;
            }
            set
            {
                m_thrown_cards = value;
            }
        }

        public float Time_left_on_timer
        {
            get
            {
                return time_left_on_timer;
            }

            set
            {
                time_left_on_timer = value;
            }
        }

        public int Dealer
        {
            get
            {
                return dealer;
            }

            set
            {
                dealer = value;
            }
        }

        public float Total_time_on_timer { get => total_time_on_timer; set => total_time_on_timer = value; }
    }
}