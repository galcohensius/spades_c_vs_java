﻿using System.Collections.Generic;

namespace cardGames.models
{
    //currently rummy only. may as well be refactored into spades as well.
    public class Page
    {
        public string Name { get; set; }
        //list of table types (single/sit n go, arena for now)
        public List<Match> Matches { get; set; } = new List<Match>();

        //rummy match/table typs:
        //sit & go (single match) - to me implemented
        //spin & go - future
        //points to cash - future
        //arena - to me implemented
        //jackpot - future

        public Page(string name = "", List<Match> matches = null)
        {
            Name = name;
            if (matches != null)
                Matches = matches;
        }

        public void AddMatch(Match match, int idx = -1)
        {
            if (idx == -1)
                Matches.Add(match);
            else
                Matches.Insert(idx, match);

        }

        int NumOfMatches
        {
            get => Matches.Count;
        }

        //holds visual data?
    }
}