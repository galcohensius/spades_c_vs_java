﻿using cardGames.models;
using common.comm;
using common.commands;
using common.models;

namespace cardGames.commands
{
    public abstract class CardGamesCommandsBuilder : CommandsBuilder
    {
        public static new CardGamesCommandsBuilder Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
                Instance = this;
        }

        protected override void AddAvatarNickname(CompositeParam cp, CommonUser user)
        {
            cp.AddValueParam((user as User).GetMAvatar().NickName);
        }

        /// <summary>
        /// send emoji/preset text and in the future free text
        /// </summary>
        /// <param name="id">message id</param>
        /// <param name="freeText">free text. ASSUMES THAT IF FREE TEXT IS SENT THE MESSAGE WILL NOT BE EMPTY.</param>
        /// <returns></returns>
        public virtual MMessage BuildChatCommand(string game_id, string chat_id, string message_text)
        {
            MMessage m = new MMessage();

            m.Code = CHAT_CODE;

            m.Params.Add(new SimpleParam(game_id));

            CompositeParam compositeParam = new CompositeParam();

            compositeParam.AddParam(new SimpleParam(chat_id));

            //if (string.IsNullOrEmpty(message_text) == false)
            //    compositeParam.AddParam(new SimpleParam(message_text));

            m.Params.Add(compositeParam);

            return m;

        }
    }
}