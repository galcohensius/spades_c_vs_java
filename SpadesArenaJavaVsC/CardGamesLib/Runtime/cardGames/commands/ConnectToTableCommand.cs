using cardGames.comm;
using common.comm;
using common.commands;
using System;



namespace cardGames.commands
{

    public class ConnectToTableCommand : Command
    {


        public enum ConnectToTableAnswers
        {
            OK,
            GameNotFound,
            AlreadyConnected
        }

        public override void Execute(MMessage message)
        {


            ConnectToTableAnswers answer_back = (ConnectToTableAnswers)Convert.ToInt32(message.Params[0].Value);
            

            if (answer_back == ConnectToTableAnswers.OK)
            {

            }
            else if (answer_back == ConnectToTableAnswers.GameNotFound)
            {
                //game not found
            }
            else if (answer_back == ConnectToTableAnswers.AlreadyConnected)
            {
                //already connected
                if (message.Params.Count == 3) //rummy only ATM
                {
                    GameServerManager.Instance.Last_table_id = message.Params[1].Value;
                    GameServerManager.Instance.Last_game_id = message.Params[2].Value;                  
                    HandleAlreadyInGame();
                }
            }

            commandsManager.EnableCommandsQueue(QueueType);

        }

        //TODO: MAKE CLASS ABSTRACT INSTEAD OF THIS HAM-FISTED SOLUTION WHEN FIXED IN SPADES
        //class is not abs and this method is not abstract to not break spades that currently doesn't handle this
        protected virtual void HandleAlreadyInGame()
        {

        }

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.Other;
    }
}