﻿using common.models;
using common;
using common.utils;
using System;
using cardGames.models;
using common.controllers.purchase;
using cardGames.views.popups;
using cardGames.controllers;
using System.Collections.Generic;
using common.views;

namespace cardGames.views
{
    public class CashierItemView : MonoBehaviour
    {
        bool m_contains_Voucher = false;

        string m_cashierVersion = "a";

        public void SetData(CashierItem cashier_item, CashierPopup.CoinsFormat format, bool show_was_line, Vector2 price_offset)
        {
            List<Transform> price_was_text = null;
            Button buy_button;
            Transform selectedPanel;
            CashierItem cashier_item_current = cashier_item;

            if (transform.FindDeepChild("VoucherPanel") != null)
            {
                GameObject coinPanel = transform.FindDeepChild("CoinsPanel").gameObject;
                GameObject voucherPanel = transform.FindDeepChild("VoucherPanel").gameObject;
                if (cashier_item.GoodsList.Vouchers.Count > 0)
                {
                    coinPanel.SetActive(false);
                    voucherPanel.SetActive(true);

                    selectedPanel = voucherPanel.transform;
                }
                else
                {
                    coinPanel.SetActive(true);
                    voucherPanel.SetActive(false);

                    selectedPanel = coinPanel.transform;
                }
            }
            else
            {
                selectedPanel = transform;
            }
            List<Transform> coins_text = transform.FindDeepChildren("CoinsText");
            List<Transform> price_text = transform.FindDeepChildren("PriceText");
            List<Transform> bonus_text = transform.FindDeepChildren("BonusText");

            Transform btn = transform.FindDeepChild("Button");
            if (btn != null)
            {
                // new version
                buy_button = btn.GetComponent<Button>();
            }
            else
            {
                // old version
                buy_button = transform.FindDeepChild("BG").GetComponent<Button>();
            }

            GameObject bonus_bg = transform.FindDeepChild("BonusBG").gameObject;

            if (show_was_line)
            {
                price_was_text = transform.FindDeepChildren("CoinsTextWas");

                if (cashier_item_current.Bonus != 0)
                {
                    for (int i = 0; i < coins_text.Count; i++)
                    {
                        Vector3 m_coins_text_pos = coins_text[i].gameObject.GetComponent<RectTransform>().localPosition;
                        coins_text[i].gameObject.GetComponent<RectTransform>().localPosition = new Vector3(m_coins_text_pos.x + price_offset.x, m_coins_text_pos.y + price_offset.y, 0);
                    }
                }
            }

            buy_button.gameObject.AddComponent<ButtonsOverView>();

            buy_button.onClick.AddListener(() =>
               {
                   CashierController.Instance.BuyPackage(cashier_item.Id, new PurchaseTrackingData
                   {
                       imsMetaData = ModelManager.Instance.GetCashier().ImsMetadata,
                       imsActionValue = cashier_item.ImsActionValue
                   });

               });
            for (int i = 0; i < coins_text.Count; i++)
                AssetBundleUtils.SetTextForTransform(coins_text[i], FormatUtils.FormatPrice(cashier_item_current.GoodsList.CoinsValue));

            switch (format)
            {
                case CashierPopup.CoinsFormat.Balance:
                    {
                        for (int i = 0; i < coins_text.Count; i++)
                            AssetBundleUtils.SetTextForTransform(coins_text[i], FormatUtils.FormatBalance(cashier_item_current.GoodsList.CoinsValue));
                        break;
                    }
                case CashierPopup.CoinsFormat.BuyIn:
                    {
                        for (int i = 0; i < coins_text.Count; i++)
                            AssetBundleUtils.SetTextForTransform(coins_text[i], FormatUtils.FormatBuyIn(cashier_item_current.GoodsList.CoinsValue));
                        break;
                    }
                case CashierPopup.CoinsFormat.Price:
                    {
                        for (int i = 0; i < coins_text.Count; i++)
                            AssetBundleUtils.SetTextForTransform(coins_text[i], FormatUtils.FormatPrice(cashier_item_current.GoodsList.CoinsValue));
                        break;
                    }
            }

            for (int i = 0; i < price_text.Count; i++)
                AssetBundleUtils.SetTextForTransform(price_text[i], "$" + cashier_item_current.Price);
            for (int i = 0; i < bonus_text.Count; i++)
                AssetBundleUtils.SetTextForTransform(bonus_text[i], cashier_item_current.Bonus + "%");

            Transform voucherItem = transform.FindDeepChild("VoucherItem");

            if (voucherItem != null)
            {
                Transform method = transform.FindDeepChild("MethodA");

                if (method != null)
                    m_cashierVersion = "a";

                 method = transform.FindDeepChild("MethodB");

                if (method != null)
                    m_cashierVersion = "b";

                if (m_cashierVersion == "a")
                {
                    // create from prefab
                    GameObject voucher_holder = transform.FindDeepChild("VoucherItem").gameObject;
                    Transform voucher_value = transform.FindDeepChild("VoucherValueText");
                    Transform voucher_quantity = transform.FindDeepChild("VoucherQuantityText");
                    if (cashier_item.GoodsList.Vouchers == null || cashier_item.GoodsList.Vouchers.Count == 0)
                    {
                        voucher_holder.SetActive(false);
                    }
                    else
                    {
                        voucher_holder.SetActive(true);
                        AssetBundleUtils.SetTextForTransform(voucher_value, FormatUtils.FormatBuyIn(cashier_item_current.GoodsList.Vouchers[0].VouchersValue));
                        AssetBundleUtils.SetTextForTransform(voucher_quantity, cashier_item_current.GoodsList.Vouchers[0].Count.ToString());
                        m_contains_Voucher = true;
                    }
                }
                else if (m_cashierVersion == "b")
                {

                    Transform dummyVoucher = voucherItem.FindDeepChild("DummyVoucher");
                    dummyVoucher.gameObject.SetActive(false);

                    Transform voucherPlaceholder = voucherItem.FindDeepChild("VoucherPlaceholder");
                    GameObject voucher_prefab = VoucherController.Instance.VoucherViewPrefab;
                    GameObject voucherGO = Instantiate(voucher_prefab, voucherPlaceholder);
                    voucherGO.transform.SetPositionAndRotation(voucherPlaceholder.position, voucherPlaceholder.rotation);

                    VouchersGroup voucherGroup = cashier_item.GoodsList.Vouchers[0];
                    voucherGO.GetComponent<VoucherView>().SetData(voucherGroup);

                    m_contains_Voucher = true;

                }
            }

            if (cashier_item_current.Bonus == 0)
            {
                bonus_bg.SetActive(false);

                //this means we dont have a bonus - but the prefab has the was option - so need to hide the was thing
                if (price_was_text != null)
                {
                    for (int i = 0; i < price_was_text.Count; i++)
                        price_was_text[i].gameObject.SetActive(false);
                }

            }
            else
            {
                bonus_bg.SetActive(true);

                //this means we have a bonus - now if the prefab itself also have a was container need to calculate 
                if (price_was_text != null)
                {
                    float ratio = (cashier_item_current.Bonus + 100) / 100f;

                    int was_value = Convert.ToInt32(cashier_item_current.GoodsList.CoinsValue / ratio);

                    switch (format)
                    {
                        case CashierPopup.CoinsFormat.Balance:
                            {
                                for (int i = 0; i < price_was_text.Count; i++)
                                    AssetBundleUtils.SetTextForTransform(price_was_text[i], "WAS " + FormatUtils.FormatBalance(was_value));
                                break;
                            }
                        case CashierPopup.CoinsFormat.BuyIn:
                            {
                                for (int i = 0; i < price_was_text.Count; i++)
                                    AssetBundleUtils.SetTextForTransform(price_was_text[i], "WAS " + FormatUtils.FormatBuyIn(was_value));
                                break;
                            }
                        case CashierPopup.CoinsFormat.Price:
                            {
                                for (int i = 0; i < price_was_text.Count; i++)
                                    AssetBundleUtils.SetTextForTransform(price_was_text[i], "WAS " + FormatUtils.FormatPrice(was_value));
                                break;
                            }
                    }

                    //now all that is left is to calculate and fix the length of the cross over line
                    float line_length = 0;

                    for (int i = 0; i < price_was_text.Count; i++)
                    {
                        if (price_was_text[i].GetComponent<Text>() != null)
                        {
                            line_length = price_was_text[i].GetComponent<Text>().preferredWidth - 160f;
                        }
                        else if (price_was_text[i].GetComponent<TMP_Text>() != null)
                        {
                            line_length = price_was_text[i].GetComponent<TMP_Text>().preferredWidth - 160f;
                        }
                        price_was_text[i].transform.GetChild(0).GetComponent<RectTransform>().offsetMax = new Vector2(line_length, price_was_text[i].transform.GetChild(0).GetComponent<RectTransform>().offsetMax.y);
                    }

                }

            }

        }

        public bool Contains_Voucher { get => m_contains_Voucher; set => m_contains_Voucher = value; }

    }
}