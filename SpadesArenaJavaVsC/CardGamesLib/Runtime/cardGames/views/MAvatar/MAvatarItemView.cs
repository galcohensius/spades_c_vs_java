﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class MAvatarItemView : MonoBehaviour
    {
        public void SetData(MavatarItem avatarItem, MAvatarModel.GenderType gender, bool onlyDestroy)
        {
            // removes previous items that were on
            foreach (Transform item in transform)
                Destroy(item.gameObject);

            if (onlyDestroy)
                return;

            GameObject go = MAvatarController.Instance.GetPrefabByID(avatarItem.ID, transform);
            if (go == null)
            {
                MAvatarController.Instance.GetDefaultPrefab(avatarItem.ItemType, gender, transform);
            }
        }
    }
}