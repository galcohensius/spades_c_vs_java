﻿using System.Collections.Generic;
using common.controllers;

namespace cardGames.views
{
    public class MAvatarLayerSetter : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> m_canvasObjectTargets;

        [SerializeField]
        private List<string> m_sortingLayers;

        [SerializeField]
        private string m_chosenLayer;

        [SerializeField]
        private int m_chosenOrderNumberInLayer = 1;

        [SerializeField]
        private GameObject m_faceObjectParent = null;

        public void GetCurrentAvailableLayers()
        {
            SortingLayer[] layers = SortingLayer.layers;
            m_sortingLayers.Clear();

            foreach (SortingLayer layer in layers)
            {
                m_sortingLayers.Add(layer.name);
            }
        }

        public void SetChosenLayer()
        {
            foreach (GameObject obj in CanvasObjectTargets)
            {
                if (obj != null)
                {
                    Canvas canvas = obj.GetComponent<Canvas>();

                    if (canvas != null)
                    {
                        canvas.sortingLayerName = ChosenLayer;
                    }
                    else
                    {
                        apPortrait apPortrait = obj.GetComponent<apPortrait>();

                        if (apPortrait != null)
                        {
                            apPortrait.SetSortingLayer(ChosenLayer);
                        }
                        else
                        {
                            ParticleSystemRenderer particleSystemRenderer = obj.GetComponent<ParticleSystemRenderer>();

                            if (particleSystemRenderer != null)
                            {
                                particleSystemRenderer.sortingLayerName = ChosenLayer;
                            }
                            else
                            {
                                LoggerController.Instance.LogError(string.Format("No Canvas Or ApPortrait Or ParticleSystemRenderer Component Found On Object - {0}", obj));
                            }
                        }
                    }
                }
            }
        }

        public void SetOrderInLayer()
        {
            foreach (GameObject obj in CanvasObjectTargets)
            {
                if (obj != null)
                {
                    Canvas canvas = obj.GetComponent<Canvas>();

                    if (canvas != null)
                    {
                        canvas.sortingOrder = ChosenOrderNumberInLayer;
                    }
                    else
                    {
                        apPortrait apPortrait = obj.GetComponent<apPortrait>();

                        if (apPortrait != null)
                        {
                            apPortrait.SetSortingOrder(ChosenOrderNumberInLayer);
                        }
                        else
                        {
                            ParticleSystemRenderer particleSystemRenderer = obj.GetComponent<ParticleSystemRenderer>();

                            if (particleSystemRenderer != null)
                            {
                                particleSystemRenderer.sortingOrder = ChosenOrderNumberInLayer;
                            }
                            else
                            {
                                LoggerController.Instance.LogError(string.Format("No Canvas Or ApPortrait Or ParticleSystemRenderer Component Found On Object - {0}", obj));
                            }
                        }
                    }
                }
            }
        }

        public void SetFaceLayer(string sortingLayer)
        {
            if (m_sortingLayers.Contains(sortingLayer))
            {
                apPortrait apPortrait = null;
                apPortrait = m_faceObjectParent.transform.GetChild(0).GetComponent<MAvatarFaceView>().ChosenApPortraitObject;

                if (apPortrait != null)
                {
                    apPortrait.SetSortingLayer(sortingLayer);
                }
            }
        }

        public void SetFaceOrder(int orderNumberInLayer)
        {
            apPortrait apPortrait = null;
            apPortrait = m_faceObjectParent.transform.GetChild(0).GetComponent<MAvatarFaceView>().ChosenApPortraitObject;

            if (apPortrait != null)
            {
                apPortrait.SetSortingOrder(orderNumberInLayer);
            }
        }

        public string ChosenLayer
        {
            get
            {
                return m_chosenLayer;
            }

            set
            {
                m_chosenLayer = value;
            }
        }

        public List<GameObject> CanvasObjectTargets
        {
            get
            {
                return m_canvasObjectTargets;
            }

            set
            {
                m_canvasObjectTargets = value;
            }
        }

        public int ChosenOrderNumberInLayer
        {
            get
            {
                return m_chosenOrderNumberInLayer;
            }

            set
            {
                m_chosenOrderNumberInLayer = value;
            }
        }
    }
}