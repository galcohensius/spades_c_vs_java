﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class MAvatarCostumeView : MonoBehaviour
    {
        [SerializeField] Transform m_costumeBust;

        public void SetData(MavatarItem avatarItem, bool onlyDestroy)
        {
            DestroyOldCostume();

            if (onlyDestroy)
                return;

            if (!string.IsNullOrEmpty(avatarItem.ID))
            {
                GameObject go = MAvatarController.Instance.GetPrefabByID(avatarItem.ID, transform);
                if (go != null)
                {
                    Transform bust = transform.FindDeepChild("BustHolder");
                    Vector3 localScale = bust.localScale;
                    Vector3 localPos = bust.localPosition;
                    // parent them to other different parents
                    bust.SetParent(m_costumeBust);
                    bust.localPosition = localPos;
                    bust.localScale = localScale;
                }
            }
        }

        private void DestroyOldCostume()
        {
            // removes previous items that were on
            foreach (Transform item in transform)
                Destroy(item.gameObject);

            // removes previous items that were on
            foreach (Transform item in m_costumeBust)
                Destroy(item.gameObject);
        }
    }
}