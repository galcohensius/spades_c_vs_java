﻿using System;
using System.Collections;
using System.Collections.Generic;
using cardGames.controllers;
using cardGames.models;
using common.facebook;
using common.utils;

namespace cardGames.views
{
    public class MAvatarView : MonoBehaviour
    {
        const int SHOW_MESSAGE_TIME = 3;
        const int NUM_CHARACTERS_LONG_MESSAGE = 14;

        float m_chatTimer;
        // this enables the bubble from the right direction
        // each player will select it's correct position in the inspector
        [SerializeField] MessageDirection m_messageDirection;
        public enum MessageDirection
        {
            North = 0,
            South,
            West,
            East
        }
        [SerializeField]
        private GameObject m_dynamicFacePrefab;

        [SerializeField]
        private GameObject m_faceParent;

        private Animator m_avatarAnimator;
        private static List<AvatarAnimationStateTrigger> m_ignoreSaveTriggerList;

        [SerializeField]
        protected MAvatarItemView m_trophyView;
        [SerializeField]
        protected MAvatarItemView m_outfitView;
        [SerializeField]
        protected MAvatarHairView m_hairView;
        [SerializeField]
        protected MAvatarItemView m_accessoryView;
        [SerializeField]
        protected MAvatarStaticFaceView m_staticFaceView;

        protected MAvatarFaceView m_dynamicFaceView = null;

        [SerializeField] bool m_isItem;

        [SerializeField] bool m_avatarCreateionPreview;

        [SerializeField]
        protected MAvatarBodyItemView m_bodyItemView;

        [SerializeField]
        protected MAvatarCostumeView m_costumeView;

        [SerializeField]
        protected GameObject m_facebookDisplayLarge;

        [SerializeField]
        protected Image m_facebookImageLarge;

        [SerializeField]
        private GameObject m_facebookDisplaySmall;

        [SerializeField]
        private Image m_facebookImageSmall;

        [SerializeField]
        private Sprite m_default_facebook_image;

        private bool m_isIdleStateActive = true;

        public MAvatarModel MAvatarModel { get; private set; }
        public bool UseDynamicFace { get => m_useDynamicFace; set => m_useDynamicFace = value; }
        public int BaseSortingOrder { get => m_baseSortingOrder; }

        private Action m_onFacebookImageLoaded = null;

        [SerializeField] bool m_useDynamicFace = false;

        [SerializeField] int m_baseSortingOrder;

        GameObject m_bubble = null;
        [SerializeField] Transform m_bubbleContainer;
        [SerializeField] GameObject m_bubbleBigPrefab;
        [SerializeField] GameObject m_bubbleSmallPrefab;
        [SerializeField] Transform m_northBubblePlaceholder;
        [SerializeField] Transform m_southBubblePlaceholder;
        [SerializeField] Transform m_eastBubblePlaceholder;
        [SerializeField] Transform m_westBubblePlaceholder;

        bool m_showingChat = false;
        private ArenaAvatarStartJumpEvent OnArenaAvatarStartJumpEvent = null;
        private ArenaAvatarDroppedEvent OnArenaAvatarDroppedEvent = null;
        private void Awake()
        {
            m_avatarAnimator = GetComponent<Animator>();
        }

        void OnDisable()
        {
            if (m_dynamicFaceView != null && m_dynamicFaceView.gameObject != null)
                Destroy(m_dynamicFaceView.gameObject);

            m_dynamicFaceView = null;
            RestartAnimator();
        }

        private void OnDestroy()
        {
            if (MAvatarModel != null)
                MAvatarModel.OnModelChanged -= UpdateViewFromModel;
        }

        private GameObject CreateDynamicFaceObject()
        {
            GameObject faceObj = Instantiate(m_dynamicFacePrefab, m_faceParent.transform);
            return faceObj;
        }

        public void SetAvatarModel(MAvatarModel avatarData, bool fullChange = false)
        {
            if (MAvatarModel != null)
                MAvatarModel.OnModelChanged -= UpdateViewFromModel;


            MAvatarModel = avatarData;

            UpdateViewFromModel(avatarData, fullChange);

            // Register on avatar model change events
            MAvatarModel.OnModelChanged += UpdateViewFromModel;
        }

        //TODO - return to private. made overridable for Rummy's FlatAvatarView
        protected virtual void UpdateViewFromModel(MAvatarModel model, bool fullChange)
        {
            // string a = PrefabUtils.GetGameObjectPath(gameObject);
            if (this == null)
                return;

            ResetSortingOrder();

            if (m_dynamicFaceView == null && m_useDynamicFace)
            {
                GameObject faceObj = CreateDynamicFaceObject();
                m_dynamicFaceView = faceObj.GetComponent<MAvatarFaceView>();
            }

            // Show the facebook display according to the mode
            m_facebookDisplayLarge.SetActive(false);
            m_facebookDisplaySmall.SetActive(false);
            if (model.Mode == MAvatarModel.MAvatarMode.Facebook)
            {
                m_facebookDisplayLarge.SetActive(true);
                SetFacebookImage(model, m_facebookImageLarge);
            }
            else if (model.Mode == MAvatarModel.MAvatarMode.FacebookAndAvatar)
            {
                m_facebookDisplaySmall.SetActive(true);
                SetFacebookImage(model, m_facebookImageSmall);
            }

            bool onlyDestroy = MAvatarModel.Mode == MAvatarModel.MAvatarMode.Facebook;
            bool isWearingCostume = !string.IsNullOrEmpty(model.Costume.ID);

            if (!m_isItem)
            {
                if (m_useDynamicFace)
                    m_dynamicFaceView.gameObject.SetActive(!(onlyDestroy || isWearingCostume));
                else
                    m_staticFaceView.gameObject.SetActive(!isWearingCostume);

                m_hairView.gameObject.SetActive(!isWearingCostume);
                m_accessoryView.gameObject.SetActive(!isWearingCostume);
                m_outfitView.gameObject.SetActive(!isWearingCostume);
                m_costumeView.gameObject.SetActive(isWearingCostume);

                // trophy should never be destroyed > its always shown (not in items)
                m_trophyView.SetData(model.Trophy, MAvatarModel.Gender, false);
            }

            if (m_useDynamicFace)
                m_dynamicFaceView.SetData(model, MAvatarModel.Gender);
            else
                m_staticFaceView.SetData(model, MAvatarModel.Gender, onlyDestroy);

            m_hairView.SetData(model.Hair, MAvatarModel.Gender, onlyDestroy || isWearingCostume);
            m_accessoryView.SetData(model.Accessory, MAvatarModel.Gender, onlyDestroy);
            m_outfitView.SetData(model.Outfit, MAvatarModel.Gender, onlyDestroy);

            if (onlyDestroy || isWearingCostume)
                m_bodyItemView.SetData(model, true);
            else
                m_bodyItemView.SetData(model, false);

            m_costumeView.SetData(model.Costume, onlyDestroy);

            if (m_avatarCreateionPreview)
                PrefabUtils.SetSortingOffset(transform, m_baseSortingOrder, true);

            else if (m_messageDirection == MessageDirection.South)
            {
                // without this, there will be a timing problem
                UnityMainThreadDispatcher.Instance.DelayedCall(0.1f, () =>
                {
                    PrefabUtils.SetExactSortingOffset(m_trophyView.transform, 30);
                    PrefabUtils.SetSortingOffset(transform, m_baseSortingOrder, false);
                });
            }

            ParticleUtil.RefreshShadersandMaterials(gameObject);
        }
        private void ResetSortingOrder()
        {
            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "Head", 35);
            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "Skull_Right", 35);
            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "Skull_Left", 35);
            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "FacebookDisplaySmall", 41);

            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "FaceParent", 37);
            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "Face", 37);
            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "Male Face", 37);

            PrefabUtils.ResetSortingOrderToOriginal(m_bodyItemView.transform, "Bust", 34);
            PrefabUtils.ResetSortingOrderToOriginal(m_accessoryView.transform, "Accessory", 38);

            PrefabUtils.ResetSortingOrderToOriginal(m_hairView.transform, "Hair", 37);
            PrefabUtils.ResetSortingOrderToOriginal(m_hairView.transform, "BackHairHolder", 33);
            PrefabUtils.ResetSortingOrderToOriginal(m_hairView.transform, "FrontHairHolder", 39);
        }
        public void OffsetSortingLayersToMinimum(int minimumLayer)
        {
            PrefabUtils.SetMinimumSortingOrder(transform, minimumLayer);
        }

        public void OffsetSortingLayersToMaximum(int maximumLayer)
        {
            PrefabUtils.SetMaximumSortingOrder(transform, maximumLayer);
        }

        //changed from private for FlatAvatar
        protected void SetFacebookImage(MAvatarModel avatarData, Image target)
        {
            target.sprite = m_default_facebook_image;

            if (avatarData.FB_ID != null)
            {
                FacebookController.Instance.GetUserPicture(avatarData.FB_ID, 200, 200, true, (Sprite fb_sprite) =>
                {
                    if (this != null && fb_sprite!=null)
                        target.sprite = fb_sprite;

                });
            }

        }

        static MAvatarView()
        {
            m_ignoreSaveTriggerList = new List<AvatarAnimationStateTrigger>();
            m_ignoreSaveTriggerList.Add(AvatarAnimationStateTrigger.EnterIdleState);
            m_ignoreSaveTriggerList.Add(AvatarAnimationStateTrigger.LogicState);
            m_ignoreSaveTriggerList.Add(AvatarAnimationStateTrigger.Idle);
            m_ignoreSaveTriggerList.Add(AvatarAnimationStateTrigger.StaticIdle);
        }

        public void ShowChat(string message, int overrideOrder = -1000)
        {
            HideChat();
            m_showingChat = true;

            switch (m_messageDirection)
            {
                case MessageDirection.North:
                    if (message.Length > NUM_CHARACTERS_LONG_MESSAGE)
                        m_bubble = Instantiate(m_bubbleBigPrefab, m_northBubblePlaceholder);
                    else
                        m_bubble = Instantiate(m_bubbleSmallPrefab, m_northBubblePlaceholder);
                    break;

                case MessageDirection.South:
                    if (message.Length > NUM_CHARACTERS_LONG_MESSAGE)
                        m_bubble = Instantiate(m_bubbleBigPrefab, m_southBubblePlaceholder);
                    else
                        m_bubble = Instantiate(m_bubbleSmallPrefab, m_southBubblePlaceholder);
                    break;

                case MessageDirection.East:
                    if (message.Length > NUM_CHARACTERS_LONG_MESSAGE)
                        m_bubble = Instantiate(m_bubbleBigPrefab, m_eastBubblePlaceholder);
                    else
                        m_bubble = Instantiate(m_bubbleSmallPrefab, m_eastBubblePlaceholder);

                    break;

                case MessageDirection.West:
                    if (message.Length > NUM_CHARACTERS_LONG_MESSAGE)
                        m_bubble = Instantiate(m_bubbleBigPrefab, m_westBubblePlaceholder);
                    else
                        m_bubble = Instantiate(m_bubbleSmallPrefab, m_westBubblePlaceholder);
                    break;
            }

            m_bubble.GetComponent<ChatBubbleView>().SetData(message, m_messageDirection, overrideOrder);
            m_bubble.gameObject.SetActive(true);
        }

        public void HideChat()
        {
            m_showingChat = false;
            m_chatTimer = SHOW_MESSAGE_TIME;

            if (m_bubble == null)
                return;

            Destroy(m_bubble);
            m_bubble = null;
        }

        private void Update()
        {
            if (!m_showingChat)
                return;

            m_chatTimer -= Time.deltaTime;

            if (m_chatTimer < 0)
            {
                HideChat();
            }
        }

        private IEnumerator SetTimeDelayedIdleState(float delayTime)
        {
            yield return new WaitForSeconds(delayTime);

            m_avatarAnimator.Play("IdleAnim");
        }

        public void PlayAnimation(AvatarAnimationStateTrigger eAnimationTrigger, float delay = 0)
        {
            if (m_isIdleStateActive)
            {
                // Valid Animation Request
                if (!m_ignoreSaveTriggerList.Contains(eAnimationTrigger))
                {
                    if (delay > 0)
                    {
                        StartCoroutine(PlayDelayedAnimation(eAnimationTrigger, delay));
                    }
                    else
                    {
                        SetAnimationTrigger(eAnimationTrigger);
                    }
                }
            }
        }

        private IEnumerator PlayDelayedAnimation(AvatarAnimationStateTrigger eAnimationTrigger, float delay)
        {
            yield return new WaitForSeconds(delay);

            SetAnimationTrigger(eAnimationTrigger);
        }

        private void SetAnimationTrigger(AvatarAnimationStateTrigger eAnimationTrigger)
        {
            if (eAnimationTrigger != AvatarAnimationStateTrigger.Idle)
            {
                m_isIdleStateActive = false;
            }

            m_avatarAnimator.SetTrigger(eAnimationTrigger.ToString());
            PlaySoundEffect(eAnimationTrigger);
        }

        private void PlaySoundEffect(AvatarAnimationStateTrigger eAnimationTrigger)
        {
            if (MAvatarModel != null)
            {
                bool isMale;

                if (MAvatarModel.Gender == MAvatarModel.GenderType.Male)
                {
                    isMale = true;
                }
                else
                {
                    isMale = false;
                }

                switch (eAnimationTrigger)
                {
                    case AvatarAnimationStateTrigger.Angry:
                        CardGamesSoundsController.Instance.AvatarAngry(isMale);
                        break;
                    case AvatarAnimationStateTrigger.Curses:
                        CardGamesSoundsController.Instance.AvatarCursing(isMale);
                        break;

                    case AvatarAnimationStateTrigger.Crying:
                        CardGamesSoundsController.Instance.AvatarSad(isMale);
                        break;

                    case AvatarAnimationStateTrigger.Love_1:
                        CardGamesSoundsController.Instance.AvatarHappy(isMale);
                        break;
                    case AvatarAnimationStateTrigger.Love_2:
                        CardGamesSoundsController.Instance.AvatarInLove(isMale);
                        break;

                    case AvatarAnimationStateTrigger.Sleepy:
                        CardGamesSoundsController.Instance.AvatarSleepy(isMale);
                        break;

                }
            }
        }

        public void TL_PlayFaceAnimation(MAvatarFaceView.AvatarFaceExpressions eExpression)
        {
            if (m_dynamicFaceView != null)
            {
                m_dynamicFaceView.PlayAnimation(eExpression);
            }
        }

        public void TL_SetIdleStateActive()
        {
            m_isIdleStateActive = true;
        }

        //TODO- make private again. made overridable to make Rummy's flat avatar workable again
        protected virtual void RestartAnimator()
        {
            if (!m_useDynamicFace)
                return;
            m_avatarAnimator.Rebind();
            m_avatarAnimator.Update(0f);
            m_avatarAnimator.enabled = true;
        }

        public void PlayTrickTakingAnim()
        {
            gameObject.GetComponent<Animator>().Play("Trick take");
        }

        public void OnAvatarRandomizeAnimationAtCenter()
        {
            //   AvatarCreationController.Instance.UpdateAvatarDataOnRandomizeFromCreationScreen();
        }


        public enum AvatarAnimationStateTrigger
        {
            EnterIdleState,
            Crying = 1,  // Table Chat Animations 1-5
            Angry = 2,
            Love_1 = 3, // Happy Anim
            Love_2 = 4, // Love Anim
            Sleepy = 5,
            Curses = 6,
            LogicState,
            Idle,
            Teleport,
            ArenaJumpNext,
            Drop,
            DropLeftJump,
            DropRightJump,
            ItemChange,
            StaticIdle,
            Shuffle,
            TrickTake,
            ArenaDrop,
            Direct,
            ResetDrop
        }

        public enum AnimationTransitionBehaviorState
        {
            Static,
            Dynamic,
            Semi_Dynamic
        }

        // copied from DynamicAvatar
        public void AddArenaAvatarStartJumpListener(UnityAction OnArenaAvatarStartJump)
        {
            if (OnArenaAvatarStartJumpEvent == null)
            {
                OnArenaAvatarStartJumpEvent = new ArenaAvatarStartJumpEvent();
            }

            OnArenaAvatarStartJumpEvent.AddListener(OnArenaAvatarStartJump);
        }

        public void AddArenaAvatarDroppedListener(UnityAction OnArenaAvatarDropped)
        {
            if (OnArenaAvatarDroppedEvent == null)
            {
                OnArenaAvatarDroppedEvent = new ArenaAvatarDroppedEvent();
            }

            OnArenaAvatarDroppedEvent.AddListener(OnArenaAvatarDropped);
        }

        public void RemoveAllArenaAvatarStartJumpListeners()
        {
            OnArenaAvatarStartJumpEvent.RemoveAllListeners();
        }

        public void RemoveAllArenaAvatarDroppedListeners()
        {
            OnArenaAvatarDroppedEvent.RemoveAllListeners();
        }

        public void InvokeArenaTweenJump()
        {
            OnArenaAvatarStartJumpEvent.Invoke();
        }

        public void InvokeArenaAvatarDropped()
        {
            OnArenaAvatarDroppedEvent.Invoke();
        }


        public class ArenaAvatarStartJumpEvent : UnityEvent
        {

        }

        public class ArenaAvatarDroppedEvent : UnityEvent
        {

        }
    }
}