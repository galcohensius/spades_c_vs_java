﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class MAvatarStaticFaceView : MonoBehaviour
    {
        public void SetData(MAvatarModel avatarModel, MAvatarModel.GenderType gender, bool onlyDestroy)
        {
            // removes previous items that were on
            foreach (Transform item in transform)
                Destroy(item.gameObject);

            if (onlyDestroy)
                return;

            GameObject prefab = MAvatarController.Instance.GetPrefabByID(avatarModel.Face.ID, transform);
            if (prefab == null)
            {
                prefab = MAvatarController.Instance.GetDefaultPrefab(MAvatarModel.ItemType.Face, gender, transform);
            }
            HandleFatImageSetting(avatarModel, prefab);
        }

        public void HandleFatImageSetting(MAvatarModel mavatarModel, GameObject prefab)
        {
            prefab.transform.GetChild(0).gameObject.SetActive(false);
            prefab.transform.GetChild(1).gameObject.SetActive(false);

            if (mavatarModel.HeadShape == MAvatarModel.HeadShapeType.Fat)
            {
                if (prefab.transform.GetChild(1).gameObject.GetComponent<Image>().sprite != null)
                    prefab.transform.GetChild(1).gameObject.SetActive(true);
                else
                    prefab.transform.GetChild(0).gameObject.SetActive(true);
            }
            else
            {
                prefab.transform.GetChild(0).gameObject.SetActive(true);
            }
        }

        private void SetSpecialFaceParams(Image targetImage, MAvatarModel.GenderType genderType, MAvatarModel.HeadShapeType headShapeType)
        {
            /*RectTransform imageRect = targetImage.gameObject.GetComponent<RectTransform>();

            if (headShapeType == MAvatarModel.HeadShapeType.Fat)
            {
                if (genderType == MAvatarModel.GenderType.Male)
                {
                    imageRect.localPosition = new Vector3(0, -19f, 0);
                    imageRect.localScale = new Vector3(1.02f, 0.97f, 1);
                }
                else
                    imageRect.localScale = new Vector3(1, 0.95f, 1);

            }
            else
            {
                if (genderType == MAvatarModel.GenderType.Male)
                {
                    imageRect.localPosition = new Vector3(0, -17f, 0);
                    imageRect.localScale = new Vector3(1f, 1f, 1);
                }
                else
                    imageRect.localScale = new Vector3(1f, 1f, 1);

            }*/
        }
    }
}