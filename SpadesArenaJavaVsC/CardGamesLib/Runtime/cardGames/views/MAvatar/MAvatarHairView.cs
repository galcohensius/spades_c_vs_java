﻿using cardGames.controllers;
using cardGames.models;
using System.Collections.Generic;

namespace cardGames.views
{
    public class MAvatarHairView : MonoBehaviour
    {
        const int FRONT_SORT = 39;
        const int BACK_SORT = 33;

        Canvas m_front_canvas;
        Canvas m_back_canvas;
        bool m_shouldChangeOverride = false;
        // when m_staticBackHair == null ; that means that it's the dynamic mavatar
        [SerializeField] Transform m_staticBackHair;

        public void SetData(MavatarItem avatarItem, MAvatarModel.GenderType gender, bool onlyDestroy)
        {
            // removes previous items that were on
            List<Transform> listToDestroy = transform.Cast<Transform>().ToList();
            foreach (var child in listToDestroy)
                Destroy(child.gameObject);

            if (m_staticBackHair != null)
            {
                List<Transform> listToDestroy2 = m_staticBackHair.Cast<Transform>().ToList();
                foreach (var child in listToDestroy2)
                    Destroy(child.gameObject);
            }

            if (onlyDestroy)
                return;

            GameObject prefab = MAvatarController.Instance.GetPrefabByID(avatarItem.ID, transform);
            if (prefab == null)
            {
                prefab = MAvatarController.Instance.GetDefaultPrefab(MAvatarModel.ItemType.Hair, gender, transform);
            }

            SetHairObjects(prefab);
        }

        private void SetHairObjects(GameObject prefab)
        {
            m_back_canvas = prefab.transform.GetChild(0).gameObject.AddComponent<Canvas>();
            m_front_canvas = prefab.transform.GetChild(1).gameObject.AddComponent<Canvas>();

            if (m_back_canvas != null)
                m_back_canvas.gameObject.SetActive(true);

            if (m_front_canvas != null)
                m_front_canvas.gameObject.SetActive(true);

            // when m_staticBackHair == null ; that means that it's the dynamic mavatar
            if (m_staticBackHair == null)
            {
                if (!gameObject.activeInHierarchy)
                    m_shouldChangeOverride = true;

                if (m_front_canvas != null)
                {
                    m_front_canvas.overrideSorting = true;
                    m_front_canvas.sortingOrder = FRONT_SORT;

                }
                if (m_back_canvas != null)
                {
                    m_back_canvas.overrideSorting = true;
                    m_back_canvas.sortingOrder = BACK_SORT;
                }
            }
            else
            {
                if (m_front_canvas != null)
                    m_front_canvas.overrideSorting = false;

                prefab.transform.GetChild(0).SetParent(m_staticBackHair);

                if (m_back_canvas != null)
                    m_back_canvas.overrideSorting = false;
            }
        }
        void Start()
        {
            MAvatarController.Instance.OnAvatarShownInHierarchy += HandleSetData;
        }
        private void OnDestroy()
        {
            MAvatarController.Instance.OnAvatarShownInHierarchy -= HandleSetData;
        }
        private void HandleSetData()
        {
            if (m_shouldChangeOverride && gameObject.activeInHierarchy)
            {
                m_shouldChangeOverride = false;
                if (m_front_canvas != null)
                {
                    m_front_canvas.overrideSorting = true;
                    m_front_canvas.sortingOrder = FRONT_SORT;
                }
                if (m_back_canvas != null)
                {
                    m_back_canvas.overrideSorting = true;
                    m_back_canvas.sortingOrder = BACK_SORT;
                }
            }
        }
    }
}