﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class MAvatarFaceView : MonoBehaviour
    {
        [SerializeField] private apPortrait m_apPortraitObjectMale;
        [SerializeField] private apOptTransform m_apOptTransformMale;
        [SerializeField] private apPortrait m_apPortraitObjectFemale;
        [SerializeField] private apOptTransform m_apOptTransformFemale;
        [SerializeField] Image m_mainImage;
        [SerializeField] Image m_secondaryImage;

        private apPortrait m_chosenApPortraitObject = null;
        private apOptTransform m_chosenApOptTransform;
        private MAvatarModel.GenderType m_currentChosenGender;
        private MAvatarModel.HeadShapeType m_currentHeadShape;

        Sprite m_main_sprite;
        Sprite m_fat_sprite;

        MAvatarModel m_mavatarModel;
        MAvatarModel.GenderType m_gender;
        bool m_shouldSetData = false;

        private void Awake()
        {
            ChosenApPortraitObject = m_apPortraitObjectMale;
            m_chosenApOptTransform = m_apOptTransformMale;
            MAvatarController.Instance.OnAvatarShownInHierarchy += HandleSetData;
        }
        private void OnDestroy()
        {
            MAvatarController.Instance.OnAvatarShownInHierarchy -= HandleSetData;
        }
        void HandleSetData()
        {
            if (m_shouldSetData && gameObject.activeInHierarchy)
            {
                m_shouldSetData = false;
                SetData(m_mavatarModel, m_gender);
            }
        }
        public void SetData(MAvatarModel mavatarModel, MAvatarModel.GenderType gender)
        {
            m_mavatarModel = mavatarModel;
            m_gender = gender;

            if (!gameObject.activeInHierarchy)
            {
                m_shouldSetData = true;
                return;
            }
            InitActiveStateFalse();

            GameObject prefab = MAvatarController.Instance.GetPrefabByID(mavatarModel.Face.ID, transform);
            if (prefab == null)
            {
                prefab = MAvatarController.Instance.GetDefaultPrefab(MAvatarModel.ItemType.Face, gender, transform);
            }

            LoadPrefabImageData(prefab);

            m_currentChosenGender = gender;
            m_currentHeadShape = mavatarModel.HeadShape;

            if (mavatarModel.Gender == MAvatarModel.GenderType.Male)
            {
                m_currentChosenGender = MAvatarModel.GenderType.Male;

                Texture2D texture;

                if (mavatarModel.HeadShape == MAvatarModel.HeadShapeType.Fat)
                    texture = BuildTexture(m_fat_sprite);
                else
                    texture = BuildTexture(m_main_sprite);

                OnTextureAssetFetchedMale(texture, m_mainImage, MAvatarModel.ItemType.Face);
            }
            else
            {
                Texture2D texture;

                m_currentChosenGender = MAvatarModel.GenderType.Female;

                if (mavatarModel.HeadShape == MAvatarModel.HeadShapeType.Fat)
                    texture = BuildTexture(m_fat_sprite);
                else
                    texture = BuildTexture(m_main_sprite);

                OnTextureAssetFetchedFemale(texture, m_secondaryImage, MAvatarModel.ItemType.Face);
            }
        }

        private Texture2D BuildTexture(Sprite sprite)
        {
            Texture2D croppedTexture = null;
            try
            {
                int width = (int)sprite.textureRect.width;
                int height = (int)sprite.textureRect.height;

                croppedTexture = new Texture2D(width, height, TextureFormat.ARGB32, false);
                Color[] pixels = new Color[width * height];
                pixels = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                        (int)sprite.textureRect.y,
                                                        (int)sprite.textureRect.width,
                                                        (int)sprite.textureRect.height);
                croppedTexture.SetPixels(pixels);
                croppedTexture.Apply();
            }
            catch (UnityException e)
            {
                // Use default face mavataritem here
                Debug.Log("sprite is not readable");
            }

            return croppedTexture;
        }

        public void LoadPrefabImageData(GameObject prefab)
        {
            GameObject main_image = prefab.transform.FindDeepChild("MainImage").gameObject;

            GameObject secondary_image = null;

            if (prefab.transform.FindDeepChild("SecondaryImage") != null)
                secondary_image = prefab.transform.FindDeepChild("SecondaryImage").gameObject;

            m_main_sprite = main_image.GetComponent<Image>().sprite;

            if (secondary_image != null && secondary_image.GetComponent<Image>().sprite != null)
                m_fat_sprite = secondary_image.GetComponent<Image>().sprite;
            else
                m_fat_sprite = m_main_sprite;

            Destroy(prefab);
        }

        private void InitActiveStateFalse()
        {
            m_apPortraitObjectMale.gameObject.SetActive(false);
            m_apPortraitObjectFemale.gameObject.SetActive(false);
        }

        private void OnTextureAssetFetchedMale(Texture2D loadedAsset, Image targetImage, MAvatarModel.ItemType assetType)
        {
            ChosenApPortraitObject = m_apPortraitObjectMale;
            m_chosenApOptTransform = m_apOptTransformMale;

            SetTexture(loadedAsset, targetImage, assetType);
        }

        private void OnTextureAssetFetchedFemale(Texture2D loadedAsset, Image targetImage, MAvatarModel.ItemType assetType)
        {
            ChosenApPortraitObject = m_apPortraitObjectFemale;
            m_chosenApOptTransform = m_apOptTransformFemale;

            SetTexture(loadedAsset, targetImage, assetType);
        }

        private void SetTexture(Texture2D loadedAsset, Image targetImage, MAvatarModel.ItemType assetType)
        {
            if (loadedAsset != null)
            {
                ChosenApPortraitObject.gameObject.SetActive(true);
                ChosenApPortraitObject.Initialize();
                ChosenApPortraitObject.SetMeshImage(m_chosenApOptTransform, loadedAsset);
            }
            SetSpecialFaceParams(targetImage);
        }

        private void SetSpecialFaceParams(Image targetImage)
        {
            /* if (m_currentHeadShape == MAvatarModel.HeadShapeType.Fat)
             {
                 if (m_currentChosenGender == MAvatarModel.GenderType.Male)
                 {
                     SetFaceParams(targetImage, new Vector3(1.02f, 0.97f, 1), new Vector3(0, -19f, 0));
                 }
                 else
                 {
                     SetFaceParams(targetImage, new Vector3(1, 0.95f, 1), new Vector3(), shouldUsePosition: false);
                 }
             }
             else
             {
                 if (m_currentChosenGender == MAvatarModel.GenderType.Male)
                 {
                     SetFaceParams(targetImage, new Vector3(1, 1, 1), new Vector3(0, -17f, 0));
                 }
                 else
                 {
                     SetFaceParams(targetImage, new Vector3(1, 1, 1), new Vector3(), shouldUsePosition: false);
                 }
             }*/
        }

        private void SetFaceParams(Image targetImage, Vector3 scaleVector, Vector3 positionVector, bool shouldUseScale = true, bool shouldUsePosition = true)
        {
            if (shouldUseScale)
                ChosenApPortraitObject.gameObject.transform.localScale = scaleVector;

            if (shouldUsePosition)
                ChosenApPortraitObject.gameObject.transform.localPosition = positionVector;
        }

        public void PlayAnimation(AvatarFaceExpressions eExpression)
        {
            if (m_chosenApPortraitObject != null && m_chosenApPortraitObject.gameObject.activeInHierarchy)
            {
                switch (eExpression)
                {
                    case AvatarFaceExpressions.M_Blink:

                        if (m_currentChosenGender == MAvatarModel.GenderType.Male)
                            ChosenApPortraitObject.Play("Male blink");
                        else
                            ChosenApPortraitObject.Play("Fem Blink");

                        break;

                    case AvatarFaceExpressions.M_Smile:

                        if (m_currentChosenGender == MAvatarModel.GenderType.Male)
                            ChosenApPortraitObject.Play("Male smile");
                        else
                            ChosenApPortraitObject.Play("Fem Smile");

                        break;

                    case AvatarFaceExpressions.M_Angry:

                        if (m_currentChosenGender == MAvatarModel.GenderType.Male)
                            ChosenApPortraitObject.Play("Male Angry");
                        else
                            ChosenApPortraitObject.Play("Fem Angry");

                        break;

                    case AvatarFaceExpressions.M_Sleepy:

                        if (m_currentChosenGender == MAvatarModel.GenderType.Male)
                            ChosenApPortraitObject.Play("Male sleepy");
                        else
                            ChosenApPortraitObject.Play("Fem sleepy");

                        break;

                    default:
                        break;
                }
            }
        }

        public enum AvatarFaceExpressions
        {
            M_Blink,
            M_Smile,
            M_Angry,
            M_Sleepy,
        }

        public apPortrait ChosenApPortraitObject
        {
            get
            {
                return m_chosenApPortraitObject;
            }

            private set
            {
                m_chosenApPortraitObject = value;
            }
        }
    }
}