﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class MAvatarBodyItemView : MonoBehaviour
    {
        [SerializeField] Image m_skullRight;
        [SerializeField] Image m_baseRight;
        [SerializeField] Image m_skullLeft;
        [SerializeField] Image m_baseLeft;

        public void SetData(MAvatarModel model, bool onlyDestroy)
        {
            m_skullRight.gameObject.SetActive(!onlyDestroy);
            m_skullLeft.gameObject.SetActive(!onlyDestroy);
            m_baseRight.gameObject.SetActive(!onlyDestroy);
            m_baseLeft.gameObject.SetActive(!onlyDestroy);

            if (onlyDestroy)
                return;

            string skull_id = model.Gender + "_" + model.HeadShape + "_" + model.SkinColor;
            string bust_id = model.Gender + "_" + model.SkinColor;

            m_skullRight.sprite = MAvatarController.Instance.GetStaticSprite(true, skull_id);
            m_skullLeft.sprite = MAvatarController.Instance.GetStaticSprite(true, skull_id);

            m_baseRight.sprite = MAvatarController.Instance.GetStaticSprite(false, bust_id);
            m_baseLeft.sprite = MAvatarController.Instance.GetStaticSprite(false, bust_id);
        }
    }
}