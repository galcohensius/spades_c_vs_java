﻿using System;
using System.Collections.Generic;
using common.controllers;
using cardGames.views.overlays;

namespace cardGames.views
{
    public delegate void OverlayClosedDelegate();
    public delegate void OverlayAnimationFinished();

    public abstract class OverlaysManager : MonoBehaviour
    {

        public event Action<bool> OnOverlayShown;

        protected CanvasGroup m_canvas_group;

        List<OverlayBase> m_overlays = new List<OverlayBase>();

        protected GameObject m_cashier = null;
        protected Canvas m_overlay_canvas = null;

        [SerializeField] protected GameObject m_onboarding_overlay = null;
        [SerializeField] protected GameObject m_comet_overlay = null;
        [SerializeField] protected GameObject m_generic_BG = null;

        public static OverlaysManager Instance;

        protected virtual void Awake()
        {
            m_canvas_group = GetComponent<CanvasGroup>();
            Instance = this;
        }

        public void ShowOverlay(GameObject overlay_object, bool first, OverlayClosedDelegate close_delegate = null)
        {
            m_canvas_group.blocksRaycasts = true;
            overlay_object.transform.SetParent(transform,false); // Second param must be false to maintain scale and anchoring

            RectTransform overlay_object_rect = overlay_object.GetComponent<RectTransform>();

            overlay_object_rect.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            overlay_object_rect.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

            RectTransform RT = overlay_object_rect.GetComponent<RectTransform>();
            RT.offsetMin = new Vector2(RT.offsetMin.x, 0);
            RT.offsetMax = new Vector2(RT.offsetMax.x, 0);


            if (first)
                overlay_object.transform.SetAsFirstSibling();
            else
                overlay_object.transform.SetAsLastSibling();

            OverlayBase m_overlay_base = overlay_object.GetComponent<OverlayBase>();

            LoggerController.Instance.Log("OVERLAY --- Showing Overlay: " + overlay_object.name);

            if (OnOverlayShown != null)
                OnOverlayShown(true);

            m_overlays.Add(m_overlay_base);

            m_overlay_base.Show(this, close_delegate);

        }

        // this is ever used ??
        public void ShowOverlay(GameObject overlay_object, int order_index, OverlayClosedDelegate close_delegate = null)
        {
            m_canvas_group.blocksRaycasts = true;
            overlay_object.transform.SetParent(transform);
            RectTransform overlay_object_rect = overlay_object.GetComponent<RectTransform>();

            overlay_object_rect.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
            overlay_object_rect.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);

            RectTransform RT = overlay_object_rect.GetComponent<RectTransform>();
            RT.offsetMin = new Vector2(RT.offsetMin.x, 0);
            RT.offsetMax = new Vector2(RT.offsetMax.x, 0);

            overlay_object.transform.SetSiblingIndex(order_index);

            OverlayBase m_overlay_base = overlay_object.GetComponent<OverlayBase>();

            LoggerController.Instance.Log("OVERLAY --- Showing Overlay: " + overlay_object.name);

            if (OnOverlayShown != null)
                OnOverlayShown(true);

            m_overlays.Add(m_overlay_base);

            m_overlay_base.Show(this, close_delegate);

        }


        public void HideOverlay(OverlayBase overlay_base)
        {
            LoggerController.Instance.Log("OVERLAY --- Hiding Overlay: " + overlay_base.gameObject.name);

            overlay_base.Close();
            m_overlays.Remove(overlay_base);

            if (m_overlays.Count == 0)
                m_canvas_group.blocksRaycasts = false;

            if (OnOverlayShown != null && m_overlays.Count == 0)
                OnOverlayShown(false);

            FixCoinBoxDepth(false);
        }

        public void HideAllOverlays()
        {
            foreach (OverlayBase overlay_base in m_overlays)
            {
                overlay_base.Close();

            }
            m_canvas_group.blocksRaycasts = false;
            m_overlays.Clear();
            if (OnOverlayShown != null)
                OnOverlayShown(false);

            FixCoinBoxDepth(false);
        }

        public abstract void FixCoinBoxDepth(bool show);

        public CometOverlay ShowCometOverlay(OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_comet_overlay);

            FixCoinBoxDepth(true);

            ShowOverlay(overlay, true, close_delegate);
          
            CometOverlay cometOverlay = overlay.GetComponent<CometOverlay>();

            cometOverlay.Comets_flight_done2 = CometFlightDone;

            return cometOverlay;

        }

        public abstract void FlyStandardCoinsComet(GameObject startPoint, int amount);

        public void CometFlightDone()
        {
            FixCoinBoxDepth(false);
        }

        /// <summary>
        /// Use this to TEMPORARILY allow UI events (like clicking on objects or dragging) to pass through the overlay layer to recepients canvases behind.
        /// Showing another overlay will reset this. To use on a different overlay call this method again.
        /// </summary>
        public void PassRayCasting()
        {
            m_canvas_group.blocksRaycasts = false;
        }

        /// <summary>
        /// Use this to block UI events as overlay layer normally does. Reverses the PassRayCasting() effect.
        /// </summary>
        public void BlockRayCasting()
        {
            m_canvas_group.blocksRaycasts = true;
        }

        public bool IsOverlayShown
        {
            get
            {
                return m_overlays.Count > 0;
            }
        }

        public GameObject Cashier
        {
            get
            {
                return m_cashier;
            }
        }

        public Canvas Overlay_canvas
        {
            get
            {
                return m_overlay_canvas;
            }
        }

        public GameObject Onboarding_overlay
        {
            get
            {
                return m_onboarding_overlay;
            }
            set
            {
                m_onboarding_overlay = value;
            }
        }

        public GameObject GetOverlayBG_Object => m_generic_BG;

        /// <summary>
        /// emergency reference to open overlay. should not be common use. better query with IsOverlayShown and/or IsOverlayOfTypeShown instead
        /// </summary>
        public OverlayBase GetShownOverlay { get => m_overlays[0]; }

        /// <summary>
        /// Is a specific overlay currently shown. doesn't care if said overlay is on the stack
        /// </summary>
        /// <typeparam name="T">a type of popup</typeparam>
        /// <returns></returns>
        public bool IsOverlayOfTypeShown<T>() where T : OverlayBase
        {
            if (!IsOverlayShown)
                return false;

            return GetShownOverlay is T;
        }
    }
}