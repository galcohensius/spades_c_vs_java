﻿using System;
using cardGames.ims;
using cardGames.models;
using common.ims;
using common.ims.model;
using common.ims.views;


namespace cardGames.views
{

    public class PromoRoomItemView : MonoBehaviour
    {
        [SerializeField] IMSBannerView_v2 m_IMSPromoRoomBanner;
        IMSInteractionZone m_iZone;

        public Action<string, Sprite> OnInjectionCompleted;


        public void SetWorld(World world, Action<string, Sprite> onPromoRoomInjectionCompleted)
        {
            m_IMSPromoRoomBanner.OnInjectionCompleted += OnBannerInjectionCompleted;
            m_IMSPromoRoomBanner.Location = world.Promo_iZone.Location;

            m_iZone = world.Promo_iZone;

            OnInjectionCompleted = onPromoRoomInjectionCompleted;
        }


        private void OnBannerInjectionCompleted(IMSBannerInjector injector)
        {
            CardGamesIMSPromoRoomInjector promoRoomInjector = injector as CardGamesIMSPromoRoomInjector;
            if (promoRoomInjector != null)
            {
                OnInjectionCompleted?.Invoke(m_iZone.Location, promoRoomInjector.HeaderImageSprite);
            }
        }

    }
}
