﻿using cardGames.controllers;
using common.facebook;
using common.facebook.models;
using common.utils;

namespace cardGames.views
{
    public abstract class FBGiftItemView : MonoBehaviour
    {
        [SerializeField] TMP_Text m_name = null;
        [SerializeField] TMP_Text m_prize_text = null;

        [SerializeField] Image m_image = null;
        [SerializeField] TMP_Text m_message = null;
        [SerializeField] TMP_Text m_create_time = null;
        [SerializeField] TMP_Text m_data = null;

        FBRequestData m_gift_data = null;

        [SerializeField] GameObject m_coin_icon = null;

        [SerializeField] GameObject m_spades_holder = null;
        [SerializeField] GameObject m_collect_text = null;
        [SerializeField] protected Button m_button = null;

        public void SetData(FBRequestData fb_gift_data, int prize)
        {

            m_gift_data = fb_gift_data;
            m_name.text = fb_gift_data.Sender_name;
            TMPBidiHelper.MakeRTL(m_name);
            m_message.text = fb_gift_data.Message;
            m_create_time.text = fb_gift_data.Create_time.ToString();
            m_data.text = fb_gift_data.Data;

            m_prize_text.text = FormatUtils.FormatPrice(prize);

            if (fb_gift_data.Sender_id != null) //this means the image link is in the param called id - and needs to be retrived 
            {
                FacebookController.Instance.GetUserPicture(fb_gift_data.Sender_id, 300, 300, true, (Sprite user_image) => {

                    if (m_image != null)
                    {
                        m_image.sprite = user_image;
                        m_image.color = Color.white;
                    }


                });
            }

        }

        public void BT_Clicked()
        {
            if (!SocialController.Instance.Gift_being_claimed)
            {
                m_spades_holder.SetActive(true);
                m_collect_text.SetActive(false);
                m_button.interactable = false;

                HandleButtonClick();

                SocialController.Instance.CollectGiftBonus(m_gift_data, m_coin_icon, (bool success) =>
                {
                    if (!success)
                    {

                    }

                });
                SocialController.Instance.Gift_being_claimed = true;
            }
        }

        protected abstract void HandleButtonClick();


        public TMP_Text Prize_text
        {
            get
            {
                return m_prize_text;
            }
        }

        public GameObject Coin_icon
        {
            get
            {
                return m_coin_icon;
            }
        }


    }
}