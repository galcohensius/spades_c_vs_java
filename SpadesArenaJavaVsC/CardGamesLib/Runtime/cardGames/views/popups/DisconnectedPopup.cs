using common.views;
using System;

namespace cardGames.views.popups
{

    public class DisconnectedPopup : PopupBase
    {

        [SerializeField] TMP_Text m_body_text;
        [SerializeField] GameObject m_disconnected_icon;
        [SerializeField] GameObject m_trying_connect_icon;
        [SerializeField] GameObject m_reconnect_button;
        Action m_button_action = null;
        

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        public void CloseDisconnectPopup()
        {

        }

        public void ShowDisconnection(Action button_action)
        {
            m_body_text.text = "Lost connection to server.\nTrying to reconnect...";

            iTween.ScaleTo(m_disconnected_icon, iTween.Hash("x", 1.15, "y", 1.15, "easeType", iTween.EaseType.easeInCubic, "time", .5, "looptype", "pingPong"));

            gameObject.SetActive(true);
            m_button_action = button_action;
            m_reconnect_button.SetActive(false);
            m_trying_connect_icon.SetActive(false);
            m_disconnected_icon.SetActive(true);

        }

        public void ShowReconnectButton()
        {

            m_disconnected_icon.SetActive(false);
            m_reconnect_button.SetActive(true);

            m_body_text.text = "Cannot reconnect to server.\nPlease check your internet connection and try again.";
        }

        public void Button_Reconnect_Clicked()
        {
            if (m_button_action != null)
                m_button_action();

            ShowDisconnection(m_button_action);
        }

        public void HidePopup()
        {
            // this was MonoBehaviour before, now it's an interface
            //if (m_manager)
            m_manager?.HidePopup();
        }

    }
}