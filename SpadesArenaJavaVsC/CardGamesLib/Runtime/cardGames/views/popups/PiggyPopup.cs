﻿using cardGames.controllers;
using cardGames.models;
using cardGames.piggy.controllers;
using common.controllers;
using common.utils;
using common.views;
using common.views.popups;
using System;
using System.Collections;
using System.Text;

namespace cardGames.views.popups
{
    public class PiggyPopup : PopupBase
    {
        [SerializeField]
        private Button closeButton;

        [Header("offer screen")]
        [SerializeField]
        private GameObject offerScreen;

        [SerializeField]
        private TextMeshProUGUI timerText;


        [SerializeField]
        private GameObject lightsBoardCooldown;
        [SerializeField]
        private GameObject lightsBoardActive;
        [SerializeField]
        private TextMeshProUGUI title;
        [SerializeField]
        private TextMeshProUGUI coinsSaved;
        [SerializeField]
        private TextMeshProUGUI message;
        [Space]
        [SerializeField]
        private GameObject lightBoardBoost;
        [SerializeField]
        private TextMeshProUGUI bonusAmount;
        [SerializeField]
        private TextMeshProUGUI savedText;
        [SerializeField]
        private TextMeshProUGUI extraCoins;
        [SerializeField]
        private TextMeshProUGUI offerTimer;
        private IEnumerator boostTimer;
        [Space]
        [SerializeField]
        private GameObject breakButtonPanel;
        [SerializeField]
        private Button breakButton;
        private TextMeshProUGUI breakButtonText;
        [SerializeField]
        private Button infoButton;

        [Space]
        [Header("lights frame")]
        [SerializeField]
        private CanvasGroup frameLights1;
        [SerializeField]
        private CanvasGroup frameLights2;
        [SerializeField]
        private float frameCycleTime;
        private Sequence frameLightsSeq;

        [Space]
        [Space]
        [Header("purchase screen")]
        [SerializeField]
        private GameObject purchaseScreen;
        [SerializeField]
        private GameObject coinsTrailOrigin;
        [SerializeField]
        private TextMeshProUGUI coinsAddedText;

        private UserPiggyData piggyData;
        private IEnumerator timer;
        private bool hasActiveTimer;
        private Animator animator;
        private int coinsAdded;
        private int newBalanace;

        protected override void Awake()
        {
            base.Awake();

            animator = GetComponent<Animator>();

            piggyData = ModelManager.Instance.GetPiggy();

            breakButton.onClick.AddListener(OnPurchaseClicked);
            breakButtonText = breakButton.GetComponentInChildren<TextMeshProUGUI>();

            infoButton.onClick.AddListener(ShowInfo);

            hasActiveTimer = piggyData.EndDateInterval > 0;

            PiggyController.Instance.PurchaseSuccessful += PlayBreakAnimation;
            PiggyController.Instance.OnPiggyUpdated += ReloadNewPiggy;
            PiggyController.Instance.OnPiggyRefreshing += ClosePiggyPopupAndInfo;

            purchaseScreen.SetActive(false);

            CardGamesSoundsController.Instance.AdjustMusicAndSFX();
            CardGamesSoundsController.Instance.PlayBGMusic();
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);
            Refresh();
        }

        private void Refresh()
        {         
            SetTimer();
            SetTexts();
            SetPiggyGraphic();
            CycleLights();
            CardGamesSoundsController.Instance.StartPiggyMusicLoop(piggyData.PiggyState == PiggyState.Cooldown);
        }

        private void ReloadNewPiggy(UserPiggyData userPiggyData, bool freshPiggy = true)
        {
            //get fresh piggy
            piggyData = userPiggyData;
            CardGamesSoundsController.Instance.StopPiggyMusic();
            Refresh();
            animator.enabled = false;
            animator.enabled = true;
            switch (piggyData.PiggyState)
            {
                case PiggyState.Cooldown:
                    animator.Play("PIGGY_Sleeping_Idle");
                    break;
                default:
                    LoggerController.Instance.LogError("piggy timer end doesn't result in piggy sleeping is probably a serious error");
                    break;
            }
        }

        private void SetTimer()
        {
            if (piggyData.EndDateInterval <= 0 || piggyData.PiggyState == PiggyState.Cooldown)
            {
                GameObject timer = timerText.transform.parent.gameObject;
                timer.SetActive(false);
                return;
            }

            TimeSpan ts = new TimeSpan(piggyData.EndDate.Ticks - DateTime.Now.Ticks);
            timerText.transform.parent.gameObject.SetActive(true);

            timer = DateUtils.RunTimer48Format(ts, timerText, "ends in: ", OnTimerDepleted);
            StartCoroutine(timer);
        }

        private void OnTimerDepleted()
        {
            piggyData = ModelManager.Instance.GetPiggy();
            Refresh();
        }

        private void SetTexts()
        {
            if (piggyData.PiggyState == PiggyState.Cooldown)
            {
                lightsBoardCooldown.SetActive(true);
                lightsBoardActive.SetActive(false);
                lightBoardBoost.SetActive(false);
                breakButtonPanel.SetActive(false);
                return;
            }

            lightsBoardCooldown.SetActive(false);

            //boost period text trumps reminders and full
            if (piggyData.CoinsMultiplier > 1 && piggyData.CoinsMultiplierStartDate <= DateTime.Now && DateTime.Now < piggyData.CoinsMultiplierEndDate)
            {
                lightsBoardActive.SetActive(false);
                lightBoardBoost.SetActive(true);

                bonusAmount.text = string.Format("+{0}%", (piggyData.CoinsMultiplier - 1) * 100);

                StringBuilder sb = new StringBuilder("you saved").AppendLine();
                sb.AppendLine("<color=#F4EBED><s><size=80>" + FormatUtils.FormatBalance(piggyData.CoinsBalance) + "</s>");
                sb.AppendLine("<size=50 >break now and get");
                savedText.text = sb.ToString();

                extraCoins.text = FormatUtils.FormatBalanceWithCoinSprite(Convert.ToInt64(piggyData.CoinsMultiplier * piggyData.CoinsBalance));

                TimeSpan ts = new TimeSpan(piggyData.CoinsMultiplierEndDate.Ticks - DateTime.Now.Ticks);
                if (ts.TotalDays <= 2)
                {
                    boostTimer = DateUtils.RunTimer48Format(ts, offerTimer, "offer ends in: <color=#FBBE05>", m_manager.HidePopup);
                    StartCoroutine(boostTimer);
                }
                else
                    offerTimer.gameObject.SetActive(false);

            }
            else
            {   //normal pig, no boost
                lightsBoardActive.SetActive(true);
                lightBoardBoost.SetActive(false);
                //if full don't show reminders texts
                if (piggyData.PiggyState == PiggyState.Full)
                {
                    title.text = "your piggy is full";
                    message.text = "<size=45>don't let the extra coins\ngo to waste";
                }
                else //only if accumulating, check for reminders first
                {
                    //                                      when accumulating:
                    //texts priority: sleep > boost > full > time > capcity > regular
                    if (PiggyController.Instance.ShowTimeReminder(piggyData))
                    {
                        title.text = "don't lose your savings";
                        message.text = "<size=45>last chance to\n<size=55>break your piggy";
                    }
                    else if (piggyData.ShowCapacityReminder())
                    {
                        title.text = "you've already saved";
                        message.text = "<size=45>it's a great time to\n<size=55>break your piggy";
                    }
                    else
                    {
                        title.text = "break piggy to get";
                        message.text = "<size=50>coins you've saved";
                    }
                }
                coinsSaved.text = "<sprite=1>" + FormatUtils.FormatBalance(piggyData.CoinsBalance);
            }
            breakButtonText.text = "break for $" + piggyData.Cost;
            breakButtonPanel.GetComponentInChildren<Animator>().enabled = true;
        }

        private void SetPiggyGraphic()
        {
            switch (piggyData.PiggyState)
            {
                case PiggyState.Accumulating:
                    animator.SetTrigger("is accumulating");
                    break;
                case PiggyState.Full:
                    animator.SetTrigger("is full");
                    break;
                case PiggyState.Cooldown:
                    animator.SetTrigger("is sleeping");
                    break;
                default:
                    throw new Exception("unhandled piggy state");
            }
            animator.enabled = true;

            
        }

        private void CycleLights()
        {
            if (frameLightsSeq == null)
            {
                frameLights1.alpha = 1;
                frameLights2.alpha = 0;

                frameLightsSeq = DOTween.Sequence();

                frameLightsSeq.Insert(0, frameLights1.DOFade(0, frameCycleTime));
                frameLightsSeq.Insert(0, frameLights2.DOFade(1, frameCycleTime));
                frameLightsSeq.SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
                frameLightsSeq.Play();
            }
            else
            {
                if (!frameLightsSeq.IsPlaying())
                    frameLightsSeq.Play();
            }
        }

        private void ShowInfo()
        {
            CardGamesPopupManager.Instance.ShowPiggyInfoPopup();
        }

        //attempt to purchase
        private void OnPurchaseClicked()
        {
            PiggyController.Instance.PurchasePiggy();
            CardGamesSoundsController.Instance.PlayPiggyPurchaseButtonSFX();
            breakButton.interactable = false;
        }

        //purchase completed successfully
        private void PlayBreakAnimation(int newBalance)
        {
            closeButton.gameObject.SetActive(false);
            animator.SetTrigger("break");
            int prevBalance = ModelManager.Instance.GetUser().GetCoins();
            coinsAdded = newBalance - prevBalance;
            coinsAddedText.text = FormatUtils.FormatBalanceWithCoinSprite(coinsAdded);
            ModelManager.Instance.GetUser().SetCoins(newBalance);
            CardGamesSoundsController.Instance.PlayPiggyBreakSequence();
        }

        public void PlaySignSwooshInSFX() //animation event
        {
            CardGamesSoundsController.Instance.PlayPiggySignSwooshIn();
        }

        public void FlyCoinsComet() //animation event
        {
            var m_coins_comet = OverlaysManager.Instance.ShowCometOverlay();

            m_coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, 5, coinsTrailOrigin, CardGamesPopupManager.Instance.Lobby_flight_target
               , 0f, 0.15f, 1.5f, CardGamesPopupManager.Instance.Lobby_coins_balance, coinsAdded, CardGamesPopupManager.Instance.Lobby_coins_animator, 3, 1.0f, 0.5f
               , ()=> 
               {
                   PiggyController.Instance.SetDummySleepingPiggy(); 
                   m_coins_comet.Close();
                   closeButton.gameObject.SetActive(true);
               }, new Vector2(0.56f, 0.765f));
        }

        public override void Close()
        {
            base.Close();
            StopLoopingMusic();
        }

        public void StopLoopingMusic()
        {
            CardGamesSoundsController.Instance.StopPiggyMusic();
        }

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }

        public override float GetBGDarkness()
        {
            return 1;
        }

        private void ClosePiggyPopupAndInfo()
        {
            if (m_manager.IsPopupOfTypeShown<SimplePopup>())
                m_manager.HidePopup();
            CloseButtonClicked();
        }

        private void OnDestroy()
        {
            frameLightsSeq.Kill();
            PiggyController.Instance.PurchaseSuccessful -= PlayBreakAnimation;
            PiggyController.Instance.OnPiggyUpdated -= ReloadNewPiggy;
            PiggyController.Instance.OnPiggyRefreshing -= ClosePiggyPopupAndInfo;
        }
    }
}