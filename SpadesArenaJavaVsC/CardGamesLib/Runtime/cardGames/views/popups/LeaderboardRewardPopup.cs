﻿using cardGames.comm;
using cardGames.models;
using common.controllers;
using common.utils;
using common.views;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.views.popups
{
    public abstract class LeaderboardRewardPopup : PopupBase
    {
        [SerializeField] TMP_Text m_title = null;
        [SerializeField] TMP_Text m_rank = null;
        [SerializeField] protected TMP_Text m_reward = null;
        [SerializeField] protected Button m_collectButton = null;
        [SerializeField] GameObject m_spadesHolder = null;
        [SerializeField] ParticleSystem m_effect = null;

        private Dictionary<string, int> m_groupToImageMap = new Dictionary<string, int>() {
        { "A1",0 },
        { "A2",1 },
        { "A3",2 },
        { "B1",3 },
        { "B2",4 },
        { "B3",5 },
        { "C1",6 },
        { "C2",7 },
        { "C3",8 },
        { "D1",9 },
        { "D2",10 },
        { "D3",11 }
    };


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            User user = ModelManager.Instance.GetUser();

            m_rank.text = "#" + ModelManager.Instance.LeaderboardReward.Rank;
            m_reward.text = "<sprite=1>" + FormatUtils.FormatBalance(ModelManager.Instance.LeaderboardReward.Reward);

            string group = ModelManager.Instance.LeaderboardReward.Group;

            m_title.text = "<sprite=" + m_groupToImageMap[group] + ">" + ModelManager.Instance.LeaderboardReward.LeaderboardTitle + " LEADERBOARD ";

            m_collectButton.enabled = true;

            m_effect.Stop();

            StartCoroutine(delayedParticteEffectStart());
        }

        IEnumerator delayedParticteEffectStart()
        {
            yield return new WaitForSeconds(0.5f);
            m_effect.Play();
        }

        public void OnCollectClick()
        {
            m_collectButton.interactable = false;
            HandleCollectClick();

            m_collectButton.transform.GetChild(0).gameObject.SetActive(false);
            m_spadesHolder.SetActive(true);

            CardGamesCommManager.Instance.ClaimLeaderboardReward(ModelManager.Instance.LeaderboardReward.Leaderboard_id, onLeaderboardClaimAwardComplete);
        }

        protected abstract void HandleCollectClick();


        private void onLeaderboardClaimAwardComplete(bool success, int coinsWon, int newBalance)
        {
            if (success)
            {
                HandleLeaderboardClaimAwardSuccess(success, coinsWon, newBalance);
            }
            else
            {
                m_manager.HidePopup();
            }

        }

        protected abstract void HandleLeaderboardClaimAwardSuccess(bool success, int coinsWon, int newBalance);

        protected void TrailCompleted()
        {
            m_manager.HidePopup();
        }

        public override Vector3 GetPosition()
        {
            return new Vector3(0, -20);
        }

        public override void OnBackButtonClicked()
        {
            OnCollectClick();
        }
    }

}