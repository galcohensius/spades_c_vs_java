using System.Collections.Generic;
using cardGames.controllers;
using cardGames.models;
using common;
using common.controllers;
using common.models;
using common.utils;
using common.views;

namespace cardGames.views.popups
{

    public class RateUsPopup : PopupBase
    {

        [SerializeField] List<Image> m_stars = new List<Image>();
        [SerializeField] Sprite m_selected_star = null;
        [SerializeField] Sprite m_NOT_selected_star = null;

        [SerializeField] GameObject m_rate_question = null;
        [SerializeField] GameObject m_rate_store = null;
        [SerializeField] GameObject m_rate_support = null;

        [SerializeField] TMP_Text m_question_text = null;
        [SerializeField] TMP_Text m_store_text = null;
        [SerializeField] TMP_Text m_top_title_text = null;
        [SerializeField] TMP_Text m_support_coins_text = null;
        [SerializeField] TMP_Text m_store_coins_text = null;

        [SerializeField] GameObject m_close_button = null;


        string[] m_question_star_texts = new string[]{
            "not at all",
            "not so much",
            "it's ok",
            "i like it",
            "it's great"
        };

        [SerializeField] Button m_submit_button = null;

        int m_selected_index = 0;


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            m_submit_button.interactable = false;
            m_submit_button.GetComponent<ButtonsOverView>().Disabled = true;
            ViewUtils.EnableDisableButtonText(m_submit_button);

            CardGamesPopupManager.Instance.UserSeenRateUsPopup = true;

        }

        public override void Start()
        {
            base.Start();

            int bonusReward = 0;
            Bonus bonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.RateUs);
            if (bonus != null)
                bonusReward = bonus.CoinsSingle;

            m_support_coins_text.text = "<sprite=1>" + bonusReward;
            m_store_coins_text.text = "<sprite=1>" + bonusReward;

#if UNITY_ANDROID
			m_store_text.text = "Please rate us on Google Play\n& COLLECT";
#elif UNITY_IOS
            m_store_text.text = "Please rate us on the App Store\n& COLLECT";
#else
			m_store_text.text = "Please rate us on Facebook\n& COLLECT";
#endif
        }

        public void BT_QuestionRateClicked()
        {
            var data = new Dictionary<string, object> { { "rate_value", m_selected_index + 1 } };

            if (m_selected_index > 4)
            {
                m_top_title_text.text = "Thanks For Your Feedback!";
                m_rate_store.SetActive(true);


                m_close_button.SetActive(true);

            }
            else
            {

                m_top_title_text.text = "How Can We Improve?";
                m_rate_support.SetActive(true);


                m_close_button.SetActive(true);
            }


            m_rate_question.SetActive(false);

        }

        public override void CloseButtonClicked()
        {

            BT_LaterClicked();
        }

        public void BT_StoreRateClicked()
        {
            CardGamesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.RateUs, PopupManager.AddMode.ShowAndRemove);
            RateClicked();
            m_manager.HidePopup();

        }

        public void BT_SupportOKClicked()
        {
            CardGamesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.RateUs, PopupManager.AddMode.ShowAndRemove);
            WebExternalController.Instance.OpenSupportPage();
            m_manager.HidePopup();
        }

        public void BT_LaterClicked()
        {
            m_manager.HidePopup();
        }

        public void BT_RateStarClicked(int star_index)
        {
            for (int i = 0; i < m_stars.Count; i++)
            {
                m_stars[i].sprite = m_NOT_selected_star;
                m_stars[i].SetNativeSize();
            }


            for (int i = 0; i < star_index; i++)
            {
                m_stars[i].sprite = m_selected_star;
                m_stars[i].SetNativeSize();
            }

            m_selected_index = star_index;

            m_question_text.text = m_question_star_texts[m_selected_index - 1].ToUpper();

            m_submit_button.interactable = true;
            ViewUtils.EnableDisableButtonText(m_submit_button);
            m_submit_button.GetComponent<ButtonsOverView>().Disabled = false;

        }

        private void RateClicked()
        {

#if (UNITY_WEBGL || UNITY_EDITOR)

            WebExternalController.Instance.OpenUrl("https://www.facebook.com/");

#else
				VersionController.Instance.OpenAppStore ();
#endif

            m_rate_store.SetActive(false);

        }


    }
}
