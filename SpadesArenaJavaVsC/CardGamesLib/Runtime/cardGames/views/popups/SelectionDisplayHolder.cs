﻿using common.models;
using System;
using common.utils;
using cardGames.models;
using common.controllers;
using common.ims;

namespace cardGames.views.popups
{
    public class SelectionDisplayHolder : MonoBehaviour
    {

        private const string title_template = "GET {0} FREE COINS!";
        private Bonus m_bonus_model = null;
        [SerializeField] TMP_Text m_get_coins_field = null;
        [SerializeField] GameObject m_IMSView = null;

        private string bonusAmount = "";
        public void Init()
        {
            m_bonus_model = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Invite, 0);

            if (m_bonus_model != null)
            {
                m_get_coins_field.text = String.Format(title_template, FormatUtils.FormatBalance(m_bonus_model.CoinsSingle));
                bonusAmount = FormatUtils.FormatPrice(m_bonus_model.CoinsSingle);
            }
            else
            {
                LoggerController.Instance.Log("SelectionDisplayHolder -> No Bonus Model");
            }

            // Special case - there is no IMS data at all - show invite text
            if (!IMSController.Instance.HasEvents)
            {
                ShowInviteText();
            }
        }

        public void ShowInviteText()
        {
            //gameObject.SetActive(true);
            Transform GetCoinsField = transform.FindDeepChild("GetCoinsField");
            GetCoinsField.gameObject.SetActive(true);

            Transform PlayerDisplayField = transform.FindDeepChild("PlayerDisplayField");
            PlayerDisplayField.gameObject.SetActive(true);
        }

        public void SetBonusAmount()
        {
            // Since the invite banner might refresh itself, we need to wait another frame so the old is destroyed
            InvokingUtils.InvokeActionWithDelay(true, 1, () =>
            {
                Transform coinspanel = transform.FindDeepChild("CoinsPanel");

                if (coinspanel != null)
                {
                    coinspanel.gameObject.SetActive(true);
                    //gameObject.SetActive(true);
                    Transform CoinsAmount = transform.FindDeepChild("CoinsAmount");
                    TMP_Text tmpCoinsAmount = CoinsAmount.GetComponent<TMP_Text>();
                    if (tmpCoinsAmount != null)
                        tmpCoinsAmount.text = "<sprite=1>" + bonusAmount;
                }

            }, this);

        }

    }
}