﻿using common.utils;
using common.views.popups;
using cardGames.models;
using System;
using System.Collections;
using common.ims;

namespace cardGames.views.popups
{
    public class IMSOpenTablePopup : IMSPopup
    {
        [SerializeField] protected GameObject m_hat1_prefab;
        [SerializeField] protected GameObject m_hat2_prefab;
        [SerializeField] protected GameObject m_hat3_prefab;

        #region Private Members
        TMP_Text m_rewardAmountText;
        Image m_icon;
        #endregion

        private IEnumerator RunTimer(TimeSpan ts, TMP_Text timer)
        {
            float startTime = Time.realtimeSinceStartup;

            while (ts.TotalSeconds >= 0)
            {
                timer.text = DateUtils.TimespanToDateMission(ts);
                yield return new WaitForSecondsRealtime(1f);
                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }
        }

        IEnumerator CountToMissionExpire(Mission mission)
        {
            TimeSpan time_to_wait = mission.End_date - DateTime.Now;

            yield return new WaitForSecondsRealtime((float)time_to_wait.TotalSeconds);

            // once the mission expired, close the popup
            CloseButtonClicked();
        }

        public void ButtonClicked(int index)
        {
            IMSController.Instance.ExecuteAction(m_iZone, index);
        }
    }
}
