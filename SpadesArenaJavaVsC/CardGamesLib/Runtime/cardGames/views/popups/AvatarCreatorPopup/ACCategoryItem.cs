﻿using cardGames.controllers;

namespace cardGames.views
{
    // This class is taking care of the preview avatar section
    // updates the look of the avatar when there are changes
    public class ACCategoryItem : MonoBehaviour
    {
        #region Serialized Members
        [SerializeField] GameObject m_notifContainer; // holds the background and the text of the notification
        [SerializeField] TMP_Text m_notification;

        [SerializeField] Image m_itemImage;
        [SerializeField] Sprite m_spriteSelected;
        [SerializeField] Sprite m_spriteRegular;
        [SerializeField] Image m_icon;
        [SerializeField] Sprite m_iconSelected;
        [SerializeField] Sprite m_iconRegular;
        [SerializeField] TMP_Text m_title;
        Color32 m_textColorRegular = new Color32(168, 174, 178, 255);
        Color32 m_textColorSelected = new Color32(255, 255, 255, 255);
        #endregion

        AvatarCreationPopup m_avatarCreator;

        public void SetData(AvatarCreationPopup avatarCreator, int notifications)
        {
            m_avatarCreator = avatarCreator;
            if (notifications == 0)
            {
                m_notifContainer.SetActive(false);
            }
            else
            {
                m_notification.text = notifications.ToString();
                m_notifContainer.SetActive(true);
            }
        }

        private void Start()
        {
            MAvatarController.Instance.OnCategoryItemSelected += ResetCategoryItem;
        }

        private void OnDestroy()
        {
            MAvatarController.Instance.OnCategoryItemSelected -= ResetCategoryItem;
        }

        void ResetCategoryItem()
        {
            m_itemImage.sprite = m_spriteRegular;
            m_icon.sprite = m_iconRegular;
            m_title.color = m_textColorRegular;
        }

        public void Button_SelectItem(int categoryType)
        {
            if (m_avatarCreator == null)
                return;

            m_avatarCreator.Button_Select_Category(categoryType);
            m_title.color = m_textColorSelected;
            m_itemImage.sprite = m_spriteSelected;
            m_icon.sprite = m_iconSelected;
        }
    }
}
