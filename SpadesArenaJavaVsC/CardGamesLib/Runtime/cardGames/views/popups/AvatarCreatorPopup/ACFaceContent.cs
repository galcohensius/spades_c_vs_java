﻿namespace cardGames.views
{
    public class ACFaceContent : MonoBehaviour
    {
        #region Serialized Members
        [SerializeField] ACCategorySection m_categorySection;
        #endregion

        public void SetData()
        {
            m_categorySection.SetSeparator("Face");
            m_categorySection.SetData(ACCategorySection.SectionType.FaceSection, models.MAvatarModel.ItemType.Face);
        }
    }
}
