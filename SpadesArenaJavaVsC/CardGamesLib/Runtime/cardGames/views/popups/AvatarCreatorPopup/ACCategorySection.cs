﻿using cardGames.controllers;
using cardGames.models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace cardGames.views
{
    public class ACCategorySection : MonoBehaviour
    {
        #region Const Members
        const int YIELD_NULL_VALUE = 4;
        protected const int ITEMS_IN_ROW = 4; // the number of items in each row in the view
        const int ITEMS_TO_SCROLL = 12; // max number of items in view at the same time
        const int NUM_OF_SHAPES = 3;
        #endregion

        #region Serialized Members
        [SerializeField] protected GridLayoutGroup m_gridLayoutGroup;
        [SerializeField] protected TMP_Text m_separator;

        [SerializeField] ACFaceItem m_faceItemPrefab;
        [SerializeField] ACFaceItem m_faceItemEmptyPrefab;
        [SerializeField] ACManikinItem m_manikinEmptyItemPrefab;

        [SerializeField] ACManikinItem m_manikinTopItemPrefab;
        [SerializeField] ACManikinItem m_manikinBottomItemPrefab;
        [SerializeField] ACManikinItem m_manikinFullItemPrefab;
        [SerializeField] List<ACGenderItem> m_genderItems = new List<ACGenderItem>();
        [SerializeField] List<ACSkinToneItem> m_skinToneItems = new List<ACSkinToneItem>();

        #endregion

        #region Enum Members
        public enum SectionType
        {
            None = 0,
            SkinToneSection,
            GenderSection,
            FaceSection,
            ShapeSection,
            ManikinOwnedSection,
            ManikinLockedSection
        };
        private SectionType m_sectionType;
        #endregion

        #region Private Members
        VerticalLayoutGroup m_verticalLayoutGroup;
        MAvatarModel.ItemType m_itemType;

        List<ACFaceItem> m_faceItems = new List<ACFaceItem>();
        List<ACFaceItem> m_shapeItems = new List<ACFaceItem>();
        List<GameObject> m_shapesGo = new List<GameObject>();

        List<ACManikinItem> m_manikinItems = new List<ACManikinItem>();

        List<ACManikinItem> m_manikinItemsTOP = new List<ACManikinItem>();
        List<ACManikinItem> m_manikinItemsBOTTOM = new List<ACManikinItem>();
        List<ACManikinItem> m_manikinItemsFULL = new List<ACManikinItem>();


        int faceItemsCount;
        int shapeItemsCount;
        int manikinItemsCount;

        bool m_finishedInstantiating = false;
        bool m_show = true;
        #endregion

        #region Protected Members
        protected bool m_isSeparatorCollapsable = false;
        #endregion

        #region Public Members
        public Action<bool> OnDoneLoading;
        private bool ItemsShowing;
        #endregion


        private void Awake()
        {
            m_verticalLayoutGroup = GetComponent<VerticalLayoutGroup>();
            MAvatarController.Instance.TempAvatarModel.OnModelChanged += UpdateIndication;
        }

        private void OnDestroy()
        {
            MAvatarController.Instance.TempAvatarModel.OnModelChanged -= UpdateIndication;
        }

        public void SetData(SectionType sectionType, MAvatarModel.ItemType itemType)
        {
            gameObject.SetActive(true);

            m_itemType = itemType;
            m_sectionType = sectionType;
            switch (sectionType)
            {
                case SectionType.GenderSection:
                    GenderInit();
                    break;

                case SectionType.ShapeSection:
                    StartCoroutine(CreateShape());
                    m_isSeparatorCollapsable = false;
                    break;

                case SectionType.ManikinOwnedSection:
                case SectionType.ManikinLockedSection:
                    CreateManikins(itemType, sectionType);
                    m_isSeparatorCollapsable = true;
                    break;

                case SectionType.SkinToneSection:
                    m_isSeparatorCollapsable = false;
                    SkinToneInit();
                    break;
            }

            // Grid and arrow should be shown on init
            if (m_isSeparatorCollapsable)
            {
                m_gridLayoutGroup.gameObject.SetActive(true);
            }

            UpdateCheckMark();

        }

        public void UpdateIndication(MAvatarModel model, bool fullChange)
        {
            if (fullChange)
            {
                if (gameObject.activeInHierarchy)
                {
                    if (m_sectionType == SectionType.ShapeSection)
                    {
                        StartCoroutine(CreateShape());
                        m_isSeparatorCollapsable = false;
                    }
                    if (m_sectionType == SectionType.FaceSection)
                    {
                       // CreateFaces();
                    }
                }
            }
            else
            {
                if (m_sectionType == SectionType.ShapeSection)
                {
                    for (int i = 0; i < NUM_OF_SHAPES; i++)
                    {
                        m_shapeItems.Add(m_shapesGo[i].GetComponent<ACFaceItem>());
                        m_shapeItems[i].SetShapeData(m_shapesGo[i].transform, (MAvatarModel.HeadShapeType)i);
                    }
                }
            }

            UpdateCheckMark();

        }

        private void UpdateCheckMark()
        {
            foreach (Transform item in m_gridLayoutGroup.transform)
            {
                item.GetComponent<ACItem>().SetActiveIndication(false);

                if (item.GetComponent<ACGenderItem>() != null)
                {
                    ACGenderItem gender = item.GetComponent<ACGenderItem>();
                    gender.SetActiveIndication(false);
                    if (MAvatarController.Instance.TempAvatarModel.Gender == gender.Gender())
                        gender.SetActiveIndication(true);
                }
                else if (item.GetComponent<ACSkinToneItem>() != null)
                {
                    ACSkinToneItem skinColor = item.GetComponent<ACSkinToneItem>();
                    skinColor.SetActiveIndication(false);
                    if (MAvatarController.Instance.TempAvatarModel.SkinColor == skinColor.SkinColorType)
                        skinColor.SetActiveIndication(true);
                }
                else if (item.GetComponent<ACManikinItem>() != null)
                {
                    ACManikinItem manikin = item.GetComponent<ACManikinItem>();
                    manikin.SetActiveIndication(false);
                    if (manikin.Item == null)
                        continue;
                    switch (m_itemType)
                    {
                        case MAvatarModel.ItemType.Featured:
                            if (
                                (MAvatarController.Instance.TempAvatarModel.Hair.ID == manikin.Item.ID ||
                                (MAvatarController.Instance.TempAvatarModel.Accessory.ID == manikin.Item.ID) ||
                                (MAvatarController.Instance.TempAvatarModel.Costume.ID == manikin.Item.ID) ||
                                (MAvatarController.Instance.TempAvatarModel.Trophy.ID == manikin.Item.ID) ||
                                (MAvatarController.Instance.TempAvatarModel.Outfit.ID == manikin.Item.ID)))
                                manikin.SetActiveIndication(true);
                            break;
                        case MAvatarModel.ItemType.Hair:
                            if (MAvatarController.Instance.TempAvatarModel.Hair.ID == manikin.Item.ID)
                                manikin.SetActiveIndication(true);
                            break;

                        case MAvatarModel.ItemType.Accessory:
                            if (MAvatarController.Instance.TempAvatarModel.Accessory.ID == manikin.Item.ID)
                                manikin.SetActiveIndication(true);
                            break;

                        case MAvatarModel.ItemType.Costume:
                            if (MAvatarController.Instance.TempAvatarModel.Costume.ID == manikin.Item.ID)
                                manikin.SetActiveIndication(true);
                            break;

                        case MAvatarModel.ItemType.Outfit:
                            if (MAvatarController.Instance.TempAvatarModel.Outfit.ID == manikin.Item.ID)
                                manikin.SetActiveIndication(true);
                            break;

                        case MAvatarModel.ItemType.Trophy:
                            if (MAvatarController.Instance.TempAvatarModel.Trophy.ID == manikin.Item.ID)
                                manikin.SetActiveIndication(true);
                            break;
                    }

                }
                else if (item.GetComponent<ACFaceItem>() != null)
                {
                    ACFaceItem face = item.GetComponent<ACFaceItem>();
                    face.SetActiveIndication(false);


                    if (m_sectionType == SectionType.ShapeSection)
                    {
                        if (face.Item != null && MAvatarController.Instance.TempAvatarModel.HeadShape == face.MavatarModel.HeadShape)
                            face.SetActiveIndication(true);
                    }
                    else
                    {
                        if (face.Item != null && MAvatarController.Instance.TempAvatarModel.Face.ID == face.Item.ID)
                            face.SetActiveIndication(true);
                    }
                }
            }
        }

        public void SetFeaturedData()
        {
            List<MavatarItem> accesory, hair, outfit, trophy, costume;

            if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
            {
                accesory = ModelManager.Instance.AvatarData.Accesory_Male.Where(item => item.Featured == true).ToList();
                hair = ModelManager.Instance.AvatarData.Hair_Male.Where(item => item.Featured == true).ToList();
                outfit = ModelManager.Instance.AvatarData.Outfit_Male.Where(item => item.Featured == true).ToList();
                trophy = ModelManager.Instance.AvatarData.Base_Male.Where(item => item.Featured == true).ToList();
            }
            else
            {
                accesory = ModelManager.Instance.AvatarData.Accesory_Female.Where(item => item.Featured == true).ToList();
                hair = ModelManager.Instance.AvatarData.Hair_Female.Where(item => item.Featured == true).ToList();
                outfit = ModelManager.Instance.AvatarData.Outfit_Female.Where(item => item.Featured == true).ToList();
                trophy = ModelManager.Instance.AvatarData.Base_Female.Where(item => item.Featured == true).ToList();
            }
            costume = ModelManager.Instance.AvatarData.Costume.Where(item => item.Featured == true).ToList();

            StartCoroutine(CreateFeaturedItems(accesory, hair, outfit, trophy, costume));
        }

        private void SkinToneInit()
        {
            m_skinToneItems[0].SetData(MAvatarModel.SkinColorType.Light);
            m_skinToneItems[1].SetData(MAvatarModel.SkinColorType.Medium);
            m_skinToneItems[2].SetData(MAvatarModel.SkinColorType.Dark);
            m_skinToneItems[3].SetData(MAvatarModel.SkinColorType.White);
            int skinIndex = (int)MAvatarController.Instance.TempAvatarModel.SkinColor;
            m_skinToneItems[skinIndex].Button_Select_Item();

        }

        private void GenderInit()
        {
            if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                m_genderItems[0].Button_Select_Item();
            else
                m_genderItems[1].Button_Select_Item();
        }

        private MavatarItem GetCorrectManikinItem(int index, MAvatarModel.ItemType itemType)
        {
            switch (itemType)
            {
                case MAvatarModel.ItemType.Accessory:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Accesory_Male[index];
                    return ModelManager.Instance.AvatarData.Accesory_Female[index];

                case MAvatarModel.ItemType.Outfit:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Outfit_Male[index];
                    return ModelManager.Instance.AvatarData.Outfit_Female[index];

                case MAvatarModel.ItemType.Hair:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Hair_Male[index];
                    return ModelManager.Instance.AvatarData.Hair_Female[index];

                case MAvatarModel.ItemType.Trophy:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Base_Male[index];
                    return ModelManager.Instance.AvatarData.Base_Female[index];

                case MAvatarModel.ItemType.Face:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Face_Male[index];
                    return ModelManager.Instance.AvatarData.Face_Female[index];

                case MAvatarModel.ItemType.Costume:
                    return ModelManager.Instance.AvatarData.Costume[index];
            }
            return null;
        }

        private void CreateManikins(MAvatarModel.ItemType itemType, SectionType sectionType)
        {
            int numOfItems = HandleNumOfItems(itemType);

            bool includeEmpty = false;

            // default manikin should be the first one
            if (sectionType != SectionType.ManikinLockedSection)
            {
                if (itemType == MAvatarModel.ItemType.Accessory || itemType == MAvatarModel.ItemType.Trophy
                    || itemType == MAvatarModel.ItemType.Costume)
                {
                    includeEmpty = true;
                }
            }

            StartCoroutine(CreateManikinItems(includeEmpty, numOfItems, itemType, sectionType));
        }

        IEnumerator CreateFeaturedItems(List<MavatarItem> accesory, List<MavatarItem> hair, List<MavatarItem> outfit, List<MavatarItem> trophy, List<MavatarItem> costume)
        {
            int index = 0;
            ItemsShowing = false;
            GameObject go = new GameObject();
            int currentLevel = ModelManager.Instance.GetUser().GetLevel();

            // sorting the array with the comparer
            accesory.Sort(new NewAndLevelComparer());
            hair.Sort(new NewAndLevelComparer());
            outfit.Sort(new NewAndLevelComparer());
            trophy.Sort(new NewAndLevelComparer());
            costume.Sort(new NewAndLevelComparer());

            for (int i = 0; i < m_manikinItems.Count; i++)
            {
                m_manikinItems[i].gameObject.SetActive(false);
            }
            m_manikinItems.Clear();

            m_manikinItemsTOP.ForEach(obj => obj.gameObject.SetActive(false));
            m_manikinItemsBOTTOM.ForEach(obj => obj.gameObject.SetActive(false));
            m_manikinItemsFULL.ForEach(obj => obj.gameObject.SetActive(false));
            
            int topCount = 0;
            int botCount = 0;
            int fullCount = 0;

            foreach (MavatarItem item in accesory)
            {
                if (item.MinLevel > currentLevel || (!string.IsNullOrEmpty(item.InventoryID) && !ModelManager.Instance.Inventory.HasItem(item.InventoryID)))
                    continue;

                if (topCount >= m_manikinItemsTOP.Count)
                {
                    go = Instantiate(m_manikinTopItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                    m_manikinItemsTOP.Add(go.GetComponent<ACManikinItem>());

                }
                else
                {
                    go = m_manikinItemsTOP[topCount].gameObject;
                    go.SetActive(true);
                }
                topCount++;

                go.GetComponent<ACManikinItem>().SetData(item, SectionType.ManikinOwnedSection);
                if (MAvatarController.Instance.TempAvatarModel.Accessory.ID == item.ID)
                    go.GetComponent<ACManikinItem>().SetActiveIndication(true);
                index++;
                HandleScrolling(index);

                if (index % YIELD_NULL_VALUE == 0)
                    yield return null;
            }

            foreach (MavatarItem item in hair)
            {
                if (item.MinLevel > currentLevel || (!string.IsNullOrEmpty(item.InventoryID) && !ModelManager.Instance.Inventory.HasItem(item.InventoryID)))
                    continue;

            
                if (topCount >= m_manikinItemsTOP.Count)
                {
                    go = Instantiate(m_manikinTopItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                    m_manikinItemsTOP.Add(go.GetComponent<ACManikinItem>());
                }
                else
                {
                    go = m_manikinItemsTOP[topCount].gameObject;
                    go.SetActive(true);
                }
                topCount++;



                go.GetComponent<ACManikinItem>().SetData(item, SectionType.ManikinOwnedSection);
                if (MAvatarController.Instance.TempAvatarModel.Hair.ID == item.ID)
                    go.GetComponent<ACManikinItem>().SetActiveIndication(true);
                index++;
                HandleScrolling(index);

                if (index % YIELD_NULL_VALUE == 0)
                    yield return null;
            }

            foreach (MavatarItem item in outfit)
            {
                if (item.MinLevel > currentLevel || (!string.IsNullOrEmpty(item.InventoryID) && !ModelManager.Instance.Inventory.HasItem(item.InventoryID)))
                    continue;

                if (botCount >= m_manikinItemsBOTTOM.Count)
                {
                    go = Instantiate(m_manikinBottomItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                    m_manikinItemsBOTTOM.Add(go.GetComponent<ACManikinItem>());

                }
                else
                {
                    go = m_manikinItemsBOTTOM[botCount].gameObject;
                    go.SetActive(true);
                }
                botCount++;

                go.GetComponent<ACManikinItem>().SetData(item, SectionType.ManikinOwnedSection);
                if (MAvatarController.Instance.TempAvatarModel.Outfit.ID == item.ID)
                    go.GetComponent<ACManikinItem>().SetActiveIndication(true);
                index++;
                HandleScrolling(index);

                if (index % YIELD_NULL_VALUE == 0)
                    yield return null;
            }

            foreach (MavatarItem item in trophy)
            {
                if (item.MinLevel > currentLevel || (!string.IsNullOrEmpty(item.InventoryID) && !ModelManager.Instance.Inventory.HasItem(item.InventoryID)))
                    continue;

                if (botCount >= m_manikinItemsBOTTOM.Count)
                {
                    go = Instantiate(m_manikinBottomItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                    m_manikinItemsBOTTOM.Add(go.GetComponent<ACManikinItem>());

                }
                else
                {
                    go = m_manikinItemsBOTTOM[botCount].gameObject;
                    go.SetActive(true);
                }
                botCount++;

                go.GetComponent<ACManikinItem>().SetData(item, SectionType.ManikinOwnedSection);
                if (MAvatarController.Instance.TempAvatarModel.Trophy.ID == item.ID)
                    go.GetComponent<ACManikinItem>().SetActiveIndication(true);

                if (index % YIELD_NULL_VALUE == 0)
                    index++;
                HandleScrolling(index);

                yield return null;
            }

            foreach (MavatarItem item in costume)
            {
                if (item.MinLevel > currentLevel || (!string.IsNullOrEmpty(item.InventoryID) && !ModelManager.Instance.Inventory.HasItem(item.InventoryID)))
                    continue;

                if (fullCount >= m_manikinItemsFULL.Count)
                {
                    go = Instantiate(m_manikinFullItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                    m_manikinItemsFULL.Add(go.GetComponent<ACManikinItem>());

                }
                else
                {
                    go = m_manikinItemsFULL[fullCount].gameObject;
                    go.SetActive(true);
                }
                fullCount++;

                go.GetComponent<ACManikinItem>().SetData(item, SectionType.ManikinOwnedSection);
                if (MAvatarController.Instance.TempAvatarModel.Costume.ID == item.ID)
                    go.GetComponent<ACManikinItem>().SetActiveIndication(true);
                index++;
                HandleScrolling(index);

                if (index % YIELD_NULL_VALUE == 0)
                    yield return null;
            }

            
            ItemsShowing = index > 0;
            OnDoneLoading(ItemsShowing);

            int itemsInRow = index % ITEMS_IN_ROW;
            if (itemsInRow == 0)
                itemsInRow = ITEMS_IN_ROW;

            // make empty items to fill the row
            for (int i = 0; i < ITEMS_IN_ROW - itemsInRow; i++)
            {
                //     go = Instantiate(m_manikinEmptyItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                //      m_manikinItems.Add(go.GetComponent<ACManikinItem>());
            }
        }

        IEnumerator CreateManikinItems(bool includeEmpty, int numOfItems, MAvatarModel.ItemType itemType, SectionType sectionType)
        {
            ItemsShowing = false;

            List<MavatarItem> items = new List<MavatarItem>();
            // creating an empty item
            if (includeEmpty)
            {
                MavatarItem emptyItem = new MavatarItem("");
                emptyItem.ItemType = itemType;
                items.Add(emptyItem);
            }
            m_finishedInstantiating = false;

            // creating the rest of the items
            for (int i = 0; i < numOfItems; i++)
            {
                MavatarItem avatarItem = GetCorrectManikinItem(i, itemType);
                items.Add(avatarItem);
            }


            for (int i = 0; i < m_manikinItems.Count; i++)
            {
                m_manikinItems[i].gameObject.SetActive(false);
            }

            for (int i = 0; i < m_faceItems.Count; i++)
            {
                m_faceItems[i].gameObject.SetActive(false);
            }

            m_manikinItems.Clear();
             

            // sorting the array with the comparer
            items.Sort(new NewAndLevelComparer());
            int index = 0;
            int manikingLockedCounter = 0;

            int botCount = 0;
            int topCount = 0;
            int fullCount = 0;
            int faceCount = 0;

            for (int k = 0; k < items.Count; k++)
            {
                // don't show item if it was not bought
                if (!string.IsNullOrEmpty(items[k].InventoryID) && !ModelManager.Instance.Inventory.HasItem(items[k].InventoryID))
                    continue;

                if (sectionType == SectionType.ManikinOwnedSection)
                {
                    var iminlvl = items[k].MinLevel;

                    if (items[k].MinLevel > ModelManager.Instance.GetUser().GetLevel())
                    {
                        continue;
                    }
                }
                else
                {
                    if (items[k].MinLevel <= ModelManager.Instance.GetUser().GetLevel())
                    {
                        continue;
                    }
                    manikingLockedCounter++;
                }

                GameObject go = null;

                if (itemType == MAvatarModel.ItemType.Face)
                {

                    if (faceCount >= m_faceItems.Count)
                    {
                        go = Instantiate(m_faceItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                        m_faceItems.Add(go.GetComponent<ACFaceItem>());
                    }
                    else
                    {
                        go = m_faceItems[faceCount].gameObject;
                        go.SetActive(true);
                    }
                    faceCount++;
                 }
                else if (itemType == MAvatarModel.ItemType.Accessory || itemType == MAvatarModel.ItemType.Hair)
                {
                    if (topCount >= m_manikinItemsTOP.Count)
                    {
                        go = Instantiate(m_manikinTopItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                        m_manikinItemsTOP.Add(go.GetComponent<ACManikinItem>());
                    }
                    else
                    {
                        go = m_manikinItemsTOP[topCount].gameObject;
                        go.SetActive(true);
                    }
                    topCount++;

                }
                else if (itemType == MAvatarModel.ItemType.Trophy || itemType == MAvatarModel.ItemType.Outfit)
                {
                    if (botCount >= m_manikinItemsBOTTOM.Count)
                    {
                        go = Instantiate(m_manikinBottomItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                        m_manikinItemsBOTTOM.Add(go.GetComponent<ACManikinItem>());
                    }
                    else
                    {
                        go = m_manikinItemsBOTTOM[botCount].gameObject;
                        go.SetActive(true);
                    }
                    botCount++;

                }
                else if (itemType == MAvatarModel.ItemType.Costume)
                {
                    if (fullCount >= m_manikinItemsFULL.Count)
                    {
                        go = Instantiate(m_manikinFullItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                        m_manikinItemsFULL.Add(go.GetComponent<ACManikinItem>());
                    }
                    else
                    {
                        go = m_manikinItemsFULL[fullCount].gameObject;
                        go.SetActive(true);
                    }
                    fullCount++;

                }

                if ( itemType != MAvatarModel.ItemType.Face)
                    m_manikinItems.Add(go.GetComponent<ACManikinItem>());

                // TODO 
                switch (items[k].ItemType)
                {
                    case MAvatarModel.ItemType.Face:
                        m_faceItems[faceCount - 1].SetFaceData(items[k]);
                        if (m_faceItems[faceCount - 1].Item.ID == MAvatarController.Instance.TempAvatarModel.Face.ID)
                            m_faceItems[faceCount - 1].SetActiveIndication(true);

                        break;
                    case MAvatarModel.ItemType.Accessory:
                    case MAvatarModel.ItemType.Hair:
                        m_manikinItemsTOP[topCount - 1].SetData(items[k], sectionType);
                        HandleManikinActiveIndicator(go.GetComponent<ACManikinItem>() );
                        break;
                    case MAvatarModel.ItemType.Trophy:
                    case MAvatarModel.ItemType.Outfit:
                        m_manikinItemsBOTTOM[botCount - 1].SetData(items[k], sectionType);
                        HandleManikinActiveIndicator(go.GetComponent<ACManikinItem>());

                        break;
                    case MAvatarModel.ItemType.Costume:
                        m_manikinItemsFULL[fullCount - 1].SetData(items[k], sectionType);
                        HandleManikinActiveIndicator(go.GetComponent<ACManikinItem>());
                        break;
                    default:
                        continue;
                        break;
                }

                //if (items[i].ItemType != MAvatarModel.ItemType.Face)
               //     HandleManikinActiveIndicator(m_manikinItems[index]);

                HandleScrolling(index);
                index++;

                if (index % YIELD_NULL_VALUE == 0)
                    yield return null;
            }

            // don't show locked sections if it has no items

            if (sectionType == SectionType.ManikinLockedSection)
                if (manikingLockedCounter == 0)
                    gameObject.SetActive(false);


            int itemsInRow = index % ITEMS_IN_ROW;
            if (itemsInRow == 0)
                itemsInRow = ITEMS_IN_ROW;


            ItemsShowing = index > 0 + (includeEmpty ? 1 : 0);
            OnDoneLoading(ItemsShowing);


            if (ItemsShowing)
            {
                // make empty items to fill the row
                for (int i = 0; i < ITEMS_IN_ROW - itemsInRow; i++)
                {
                    GameObject go = Instantiate(m_manikinEmptyItemPrefab.gameObject, m_gridLayoutGroup.gameObject.transform);
                    go.GetComponent<ACManikinItem>().SetCorrectBG(sectionType);
                    m_manikinItems.Add(go.GetComponent<ACManikinItem>());
                }

            }
            else if ( includeEmpty)
            {
                //If you only have empty icon, remove it
                m_manikinItems[0].gameObject.SetActive(false);
            }


            m_finishedInstantiating = true;
        }

        private void HandleManikinActiveIndicator(ACManikinItem manikingItem)
        {
            switch (manikingItem.Item.ItemType)
            {
                case MAvatarModel.ItemType.Hair:
                    if (manikingItem.Item.ID == MAvatarController.Instance.TempAvatarModel.Hair.ID)
                        manikingItem.SetActiveIndication(true);
                    break;

                case MAvatarModel.ItemType.Accessory:
                    if (manikingItem.Item.ID == MAvatarController.Instance.TempAvatarModel.Accessory.ID)
                        manikingItem.SetActiveIndication(true);
                    break;

                case MAvatarModel.ItemType.Trophy:
                    if (manikingItem.Item.ID == MAvatarController.Instance.TempAvatarModel.Trophy.ID)
                        manikingItem.SetActiveIndication(true);
                    break;

                case MAvatarModel.ItemType.Outfit:
                    if (manikingItem.Item.ID == MAvatarController.Instance.TempAvatarModel.Outfit.ID)
                        manikingItem.SetActiveIndication(true);
                    break;

                case MAvatarModel.ItemType.Costume:
                    if (manikingItem.Item.ID == MAvatarController.Instance.TempAvatarModel.Costume.ID)
                        manikingItem.SetActiveIndication(true);
                    break;
            }
        }


        IEnumerator CreateShape()
        {
            // This handles the destruction correctly:
            List<Transform> listToDestroy = m_gridLayoutGroup.transform.Cast<Transform>().ToList();
            foreach (var child in listToDestroy)
                child.gameObject.SetActive(false);

            m_shapeItems.Clear();
            //m_shapesGo.Clear()

            ;
            for (int i = 0; i < NUM_OF_SHAPES; i++)
            {
                yield return null;

                if (i >= m_shapesGo.Count)
                    m_shapesGo.Add(Instantiate(m_faceItemPrefab.gameObject, m_gridLayoutGroup.transform));
                else
                    m_shapesGo[i].SetActive(true);

                m_shapeItems.Add(m_shapesGo[i].GetComponent<ACFaceItem>());
                m_shapeItems[i].SetShapeData(m_shapesGo[i].transform, (MAvatarModel.HeadShapeType)i);
            }
            // 4 - empty
            m_shapesGo.Add(Instantiate(m_faceItemEmptyPrefab.gameObject, m_gridLayoutGroup.gameObject.transform));
            m_shapeItems.Add(m_shapesGo[NUM_OF_SHAPES].GetComponent<ACFaceItem>());
        }

         /// <summary>
        /// Get Shape or Face item avatar
        /// </summary>
        private MavatarItem GetCorrectFaceItem(int index)
        {
            if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                return ModelManager.Instance.AvatarData.Face_Male[index];

            return ModelManager.Instance.AvatarData.Face_Female[index];
        }

        public int HandleNumOfItems(MAvatarModel.ItemType itemType)
        {
            switch (itemType)
            {
                case MAvatarModel.ItemType.Face:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Face_Male.Count;
                    return ModelManager.Instance.AvatarData.Face_Female.Count;

                case MAvatarModel.ItemType.Body:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Body_Male.Count;
                    return ModelManager.Instance.AvatarData.Body_Female.Count;

                case MAvatarModel.ItemType.Outfit:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Outfit_Male.Count;
                    return ModelManager.Instance.AvatarData.Outfit_Female.Count;

                case MAvatarModel.ItemType.Accessory:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Accesory_Male.Count;
                    return ModelManager.Instance.AvatarData.Accesory_Female.Count;

                case MAvatarModel.ItemType.Hair:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Hair_Male.Count;
                    return ModelManager.Instance.AvatarData.Hair_Female.Count;

                case MAvatarModel.ItemType.Trophy:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Base_Male.Count;
                    return ModelManager.Instance.AvatarData.Base_Female.Count;

                case MAvatarModel.ItemType.Costume:
                    if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                        return ModelManager.Instance.AvatarData.Costume.Count;
                    return ModelManager.Instance.AvatarData.Costume.Count;
            }

            return 0;
        }

        // **********************************************************************
        // alignment top + able to scroll
        // this makes the content scrollable and aligned to the top of the screen
        // **********************************************************************
        private void HandleScrolling(int index)
        {
            if (index >= ITEMS_TO_SCROLL)
            {
                m_verticalLayoutGroup.childForceExpandHeight = true;
                m_verticalLayoutGroup.childControlHeight = true;
            }
            else
            {
                m_verticalLayoutGroup.childControlHeight = true;
                m_verticalLayoutGroup.childForceExpandHeight = false;
            }
        }

        // This function isn't in use any more
        public void Button_Collapse_Expand()
        {
            if (!m_finishedInstantiating)
                return;

            // This handles he scroller when the separator was hit
            if (m_isSeparatorCollapsable)
            {
                m_verticalLayoutGroup.childForceExpandHeight = true;
                m_verticalLayoutGroup.childControlHeight = false;
            }
            else
            {
                m_verticalLayoutGroup.childForceExpandHeight = true;
                m_verticalLayoutGroup.childControlHeight = true;
            }

            if (!m_isSeparatorCollapsable)
                return;

            m_show = !m_show;
            m_gridLayoutGroup.gameObject.SetActive(m_show);



            /* if (m_show)
             {
                 m_arrowDown.SetActive(true);
                 m_arrowUp.SetActive(false);
                 return;
             }
             m_arrowDown.SetActive(false);
             m_arrowUp.SetActive(true);
             */

        }

        public void SetSeparator(string separatorName)
        {
            m_separator.text = separatorName;
        }
    }

    // This sorts the items by:
    // first by the "new" badge
    // then by the min level
    // then by the store order
    // then by the id
    public class NewAndLevelComparer : IComparer<MavatarItem>
    {
        public int Compare(MavatarItem a, MavatarItem b)
        {
            if (a == null)
                return -1;

            if (b == null)
                return 1;

            // new
            bool aContainsInventoryId = MAvatarController.Instance.GetNewAvatarItemIdsForCategory(a.ItemType).Contains(a.InventoryID);
            bool bContainsInventoryId = MAvatarController.Instance.GetNewAvatarItemIdsForCategory(b.ItemType).Contains(b.InventoryID);

            if (aContainsInventoryId && !bContainsInventoryId)
                return 1;

            else if (!aContainsInventoryId && bContainsInventoryId)
                return -1;

            // level
            int levelComparison = Math.Sign((a.MinLevel).CompareTo(b.MinLevel));
            if (levelComparison != 0)
                return levelComparison;

            // store order
            int storeOrderComparison = Math.Sign((a.StoreOrder).CompareTo(b.StoreOrder));

            if (storeOrderComparison != 0)
                return storeOrderComparison;

            // id
            if (a.ID == null)
                return -1;

            else if (b.ID == null)
                return 1;

            return Math.Sign((a.ID).CompareTo(b.ID));
        }
    }
}