﻿using cardGames.controllers;
using cardGames.models;
using common.utils;
using static cardGames.views.ACCategorySection;

namespace cardGames.views
{
    public class ACManikinItem : ACItem
    {
        #region Enum Members
        enum ManikinPosition
        {
            Full = 0,
            Top,
            Middle,
            Bottom
        }
        // will know the current appearance of the mask on the manikin
        ManikinPosition m_manikinPosition;
        #endregion

        #region Serialize Members
        [SerializeField] GameObject m_positionPlaceholder;

        [SerializeField] Sprite m_ownedBg;
        [SerializeField] Sprite m_lockedBg;
        [SerializeField] Image m_bg;

        [SerializeField] Image m_head;
        [SerializeField] Image m_body;

        [SerializeField] Sprite m_spriteBodyManOwned;
        [SerializeField] Sprite m_spriteBodyManLocked;

        [SerializeField] Sprite m_spriteBodyWomanOwned;
        [SerializeField] Sprite m_spriteBodyWomanLocked;

        [SerializeField] Sprite m_spriteHeadManOwned;
        [SerializeField] Sprite m_spriteHeadManLocked;

        [SerializeField] Sprite m_spriteHeadWomanOwned;
        [SerializeField] Sprite m_spriteHeadWomanLocked;

        [SerializeField] GameObject m_noneIndication;

        [SerializeField] MAvatarHairView m_hairView;
        [SerializeField] MAvatarItemView m_itemBaseViewTop;
        [SerializeField] MAvatarItemView m_itemOutfitViewBottom;
        [SerializeField] MAvatarItemView m_itemTrophyViewBottom;
        [SerializeField] MAvatarCostumeView m_itemCostumeViewFull;
        #endregion

        private void Clear()
        {
            m_noneIndication?.SetActive(false);
            m_positionPlaceholder?.SetActive(true);
            if (m_head != null)
                m_head.sprite = null;
            if (m_body != null)
            m_body.sprite = null;


            m_itemOutfitViewBottom?.SetData(null, MAvatarModel.GenderType.Male, true);
            m_itemTrophyViewBottom?.SetData(null, MAvatarModel.GenderType.Male, true);
            m_itemBaseViewTop?.SetData(null, MAvatarModel.GenderType.Male, true);
            m_hairView?.SetData(null, MAvatarModel.GenderType.Male, true);
            m_itemCostumeViewFull?.SetData(null, true);
        }


        public void SetData(MavatarItem mavatarItem, SectionType sectionType)
        {
            Clear();

            base.SetData(mavatarItem);

            if (string.IsNullOrEmpty(mavatarItem.ID))
            {
                m_noneIndication.SetActive(true);
                m_positionPlaceholder.SetActive(false);
                return;
            }

            switch (m_item.ItemType)
            {
                case MAvatarModel.ItemType.Outfit:
                    HandleManikinSprites(ManikinPosition.Bottom, sectionType);
                    m_itemOutfitViewBottom.SetData(mavatarItem, mavatarItem.Gender, false);
                    break;

                case MAvatarModel.ItemType.Hair:
                    HandleManikinSprites(ManikinPosition.Top, sectionType);
                    m_hairView.SetData(mavatarItem, mavatarItem.Gender, false);
                    break;

                case MAvatarModel.ItemType.Accessory:
                    HandleManikinSprites(ManikinPosition.Top, sectionType);
                    m_itemBaseViewTop.SetData(mavatarItem, mavatarItem.Gender, false);
                    break;

                case MAvatarModel.ItemType.Trophy:
                    HandleManikinSprites(ManikinPosition.Bottom, sectionType);
                    m_itemTrophyViewBottom.SetData(mavatarItem, mavatarItem.Gender, false);
                    break;

                case MAvatarModel.ItemType.Costume:
                    HandleManikinSprites(ManikinPosition.Full, sectionType);
                    m_itemCostumeViewFull.SetData(mavatarItem, false);
                    break;
            }

            PrefabUtils.DisableAnimations(transform);
        }
        public void SetCorrectBG(SectionType sectionType)
        {
           // for face and shape
            m_bg.sprite = m_ownedBg;

            if (sectionType == SectionType.ManikinOwnedSection)
                m_bg.sprite = m_ownedBg;

            else if (sectionType == SectionType.ManikinLockedSection)
                m_bg.sprite = m_lockedBg;
        }
        private void HandleManikinSprites(ManikinPosition manikinPosition, SectionType sectionType)
        {
            if (manikinPosition == ManikinPosition.Full)
                return;
            
            SetCorrectBG(sectionType);

            if (sectionType == SectionType.ManikinOwnedSection)
            {
                if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                {
                    m_head.sprite = m_spriteHeadManOwned;
                    m_body.sprite = m_spriteBodyManOwned;
                }
                else
                {
                    m_head.sprite = m_spriteHeadWomanOwned;
                    m_body.sprite = m_spriteBodyWomanOwned;
                }
            }
            else
            {
                if (MAvatarController.Instance.TempAvatarModel.Gender == MAvatarModel.GenderType.Male)
                {
                    m_head.sprite = m_spriteHeadManLocked;
                    m_body.sprite = m_spriteBodyManLocked;
                }
                else
                {
                    m_head.sprite = m_spriteHeadWomanLocked;
                    m_body.sprite = m_spriteBodyWomanLocked;
                }
            }
        }

        public void Button_Select_Item()
        {
            if (m_item != null && m_item.MinLevel > ModelManager.Instance.GetUser().GetLevel())
                return;

            MAvatarController.Instance.SelectItem(m_item);
        }
    }
}
