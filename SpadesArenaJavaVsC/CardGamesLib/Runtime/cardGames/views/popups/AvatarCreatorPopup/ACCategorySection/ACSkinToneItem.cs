﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class ACSkinToneItem : ACItem
    {
        MAvatarModel.SkinColorType m_skinColorType;

        public void SetData(MAvatarModel.SkinColorType skinColorType)
        {
            m_skinColorType = skinColorType;
        }

        public void Button_Select_Item()
        {
            MAvatarController.Instance.SelectSkinTone(m_skinColorType);
        }

        public MAvatarModel.SkinColorType SkinColorType { get => m_skinColorType; }
    }
}