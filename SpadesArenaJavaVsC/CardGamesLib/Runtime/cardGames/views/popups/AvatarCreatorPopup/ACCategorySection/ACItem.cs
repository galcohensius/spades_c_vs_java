﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class ACItem : MonoBehaviour
    {
        #region Serialized Members
        [SerializeField] protected GameObject m_animatedIcon;
        [SerializeField] protected TMP_Text m_minLevel;
        [SerializeField] protected GameObject m_minLevelContainer;
        [SerializeField] protected GameObject m_notification;
        [SerializeField] protected GameObject m_activeIndication;
        #endregion

        #region Protected Members
        protected bool m_isEmpty = true;
        protected MavatarItem m_item;
        #endregion

        public virtual void SetData(MavatarItem item)
        {
            m_item = item;
            m_isEmpty = false;

            if (item.ID == null)
            {
                m_minLevelContainer.gameObject.SetActive(false);
                return;
            }
            
            item.Gender = MAvatarController.Instance.TempAvatarModel.Gender;

            if (item.Animated)
                m_animatedIcon.SetActive(true);
            
            m_minLevel.text = "LVL " + item.MinLevel;

            // should shouw the min level container or not
            m_minLevelContainer.gameObject.SetActive(ModelManager.Instance.GetUser().GetLevel() < item.MinLevel);

            // not efficient to check for every item
            // with / without prefix
            if (MAvatarController.Instance.GetNewAvatarItemIdsForCategory(item.ItemType).Contains(item.ID))
                m_notification.SetActive(true);
            else
                m_notification.SetActive(false);
        }

        public void SetActiveIndication(bool show)
        {
            m_activeIndication.SetActive(show);
        }

        public MavatarItem Item { get => m_item; set => m_item = value; }
    }
}