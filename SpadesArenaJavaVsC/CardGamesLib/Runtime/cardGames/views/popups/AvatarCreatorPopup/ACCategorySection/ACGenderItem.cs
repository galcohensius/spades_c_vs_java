﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class ACGenderItem : ACItem
    {
        #region Protected Members
        [SerializeField] private bool m_isMale;
        #endregion

        public void Button_Select_Item()
        {
            MAvatarController.Instance.SelectGender(m_isMale);
        }
        public MAvatarModel.GenderType Gender()
        {
            if (m_isMale)
                return MAvatarModel.GenderType.Male;
            else
                return MAvatarModel.GenderType.Female;
        }
    }
}
