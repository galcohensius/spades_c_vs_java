﻿using cardGames.controllers;
using cardGames.models;
using common.utils;

namespace cardGames.views
{
    public class ACFaceItem : ACItem
    {
        MAvatarModel m_mavatarModel;
        bool m_isShape;

        [SerializeField] MAvatarView m_mavatarView;

        public void SetFaceData(MavatarItem avatarItem)
        {
            if (!transform.gameObject.activeInHierarchy)
                return;

            base.SetData(avatarItem);
            m_isShape = false;

            m_mavatarModel = new MAvatarModel(); // Temporary model for this view
            // in face category, everything is unlocked
            MAvatarController.Instance.CopyMavatarModel(MAvatarController.Instance.TempAvatarModel, m_mavatarModel);
            m_mavatarModel.Face = new MavatarItem(avatarItem);
            m_mavatarModel.Accessory.ID = "";
            m_mavatarModel.Trophy.ID = "";
            m_mavatarModel.Costume.ID = "";
            m_mavatarModel.Mode = MAvatarModel.MAvatarMode.Avatar;

            m_mavatarView.SetAvatarModel(m_mavatarModel);

            PrefabUtils.DisableAnimations(transform);
        }

        public void SetShapeData(Transform container, MAvatarModel.HeadShapeType headShape)
        {
            if (!transform.gameObject.activeInHierarchy)
                return;

            base.SetData(MAvatarController.Instance.TempAvatarModel.Face);
            m_isShape = true;

            m_mavatarModel = new MAvatarModel(); // Temporary model for this view
            // in shape category, everything is unlocked
            MAvatarController.Instance.CopyMavatarModel(MAvatarController.Instance.TempAvatarModel, m_mavatarModel);
            m_mavatarModel.Face = new MavatarItem(MAvatarController.Instance.TempAvatarModel.Face);
            m_mavatarModel.HeadShape = headShape;
            m_mavatarModel.Accessory.ID = "";
            m_mavatarModel.Trophy.ID = "";
            m_mavatarModel.Costume.ID = "";
            m_mavatarModel.Mode = MAvatarModel.MAvatarMode.Avatar;

            m_mavatarView.SetAvatarModel(m_mavatarModel);

            // show shape indication
            if (MAvatarController.Instance.TempAvatarModel.HeadShape == headShape)
                m_activeIndication.SetActive(true);
            else
                m_activeIndication.SetActive(false);

            PrefabUtils.DisableAnimations(transform);
        }

        public void Button_Select_Item()
        {
            if (m_item != null && m_item.MinLevel > ModelManager.Instance.GetUser().GetLevel())
                return;

            if (m_isEmpty)
                return;

            if (m_isShape)
                MAvatarController.Instance.SelectItem(m_mavatarModel.Face, ACCategorySection.SectionType.ShapeSection, m_mavatarModel.HeadShape);
            else
                MAvatarController.Instance.SelectItem(m_item);
            //MAvatarController.Instance.SelectItem(m_mavatarModel.Face, ACCategorySection.SectionType.FaceSection, m_mavatarModel.HeadShape);
        }
        public MAvatarModel MavatarModel { get => m_mavatarModel; }
    }
}
