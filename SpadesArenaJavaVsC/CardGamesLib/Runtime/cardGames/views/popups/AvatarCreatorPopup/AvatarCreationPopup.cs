﻿
using cardGames.controllers;
using cardGames.models;
using common.facebook;
using common.views;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.views
{
    public class AvatarCreationPopup : PopupBase
    {
        #region Serialized Members
        [SerializeField] ACContent m_mainContent;
        [SerializeField] ACMyAvatar m_myAvatar;
        [SerializeField] ACCategory m_category;
        #endregion

        #region Private Members
        MAvatarController m_avatarController;
        #endregion

        public void SetData()
        {
            m_avatarController = MAvatarController.Instance;

            m_category.SetData(this);
            m_myAvatar.SetData();
            m_mainContent.SetData(this, MAvatarModel.ItemType.Body);
            CardGamesStateController.Instance.SaveLastLevelInAvatarCreatorPopup();
        }

        public void Button_Select_Category(int itemType)
        {
            MAvatarController.Instance.OnCategoryItemSelected?.Invoke();
            StartCoroutine(SelectCategory((MAvatarModel.ItemType)itemType));
        }

        IEnumerator SelectCategory(MAvatarModel.ItemType itemType)
        {
            yield return null;
            m_mainContent.SelectCategoryToShow((MAvatarModel.ItemType)itemType);
        }

        public void Button_ChangeMode(int mode)
        {
            if (m_avatarController.TempAvatarModel.Mode == (MAvatarModel.MAvatarMode)mode)
            {
                m_myAvatar.ActionMenu(false);
                return;
            }
            MAvatarModel.MAvatarMode mavatarMode = (MAvatarModel.MAvatarMode)mode;
            switch (mavatarMode)
            {
                // Avatar
                case MAvatarModel.MAvatarMode.Avatar:
                    m_avatarController.TempAvatarModel.Mode = MAvatarModel.MAvatarMode.Avatar;
                    break;

                // Facebook
                case MAvatarModel.MAvatarMode.Facebook:
                    if (FacebookController.Instance.IsLoggedIn())
                    {
                        m_avatarController.TempAvatarModel.Mode = MAvatarModel.MAvatarMode.Facebook;
                        m_avatarController.TempAvatarModel.FB_ID = FacebookController.Instance.UserFacebookData.User_id;
                    }
                    break;

                // Facebook & Avatar
                case MAvatarModel.MAvatarMode.FacebookAndAvatar:
                    if (FacebookController.Instance.IsLoggedIn())
                    {
                        m_avatarController.TempAvatarModel.Mode = MAvatarModel.MAvatarMode.FacebookAndAvatar;
                        m_avatarController.TempAvatarModel.FB_ID = FacebookController.Instance.UserFacebookData.User_id;
                    }
                    break;
            }
            m_avatarController.TempAvatarModel.FireOnModelChangedEvent();
        }

        public void HandleWatchedCategory(MAvatarModel.ItemType selectedItem)
        {
            HashSet<string> items = new HashSet<string>();

            // Save only when the category was viewed !
            HashSet<string> newPerCategory = MAvatarController.Instance.GetNewAvatarItemIdsForCategory(selectedItem);
            CardGamesStateController.Instance.RemoveNewAvatarItemIds(newPerCategory, selectedItem);

            m_category.SetData(this);
        }

        public void Button_Click_Close()
        {
            if (!MAvatarController.Instance.IsEqual(MAvatarController.Instance.TempAvatarModel, ModelManager.Instance.GetUser().GetMAvatar()))
            {
                // Show are you sure if not after buy and the origin is a click
                CardGamesPopupManager.Instance.ShowQuestionOverPopups(null,
                   "Save changes before closing?", "Save", "No, thanks",
                   () =>
                   {
                       // apply
                       CardGamesPopupManager.Instance.HideQuestionOverPopups();
                       // this is here because the any portrait is hiding to slow
                       m_myAvatar.gameObject.SetActive(false);
                       Button_Apply_Avatar();
                   }, () =>
                   {
                       // no, thanks
                       CardGamesPopupManager.Instance.HideQuestionOverPopups();
                       // this is here because the any portrait is hiding to slow
                       m_myAvatar.gameObject.SetActive(false);
                       m_manager.HidePopup();
                   });
            }
            else
            {
                m_myAvatar.gameObject.SetActive(false);
                m_manager.HidePopup();
            }
        }

        public void Button_Apply_Avatar()
        {
            m_avatarController.CopyMavatarModel(m_avatarController.TempAvatarModel, ModelManager.Instance.GetUser().GetMAvatar());
            // should update the server
            MAvatarController.Instance.UpdateAvatarInfo();
            ModelManager.Instance.GetUser().GetMAvatar().FireOnModelChangedEvent();

            m_myAvatar.gameObject.SetActive(false);
            m_manager.HidePopup();
        }
    }
}
