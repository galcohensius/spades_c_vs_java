﻿using cardGames.models;

namespace cardGames.views
{
    public class ACCategoryContent : MonoBehaviour
    {

        #region Serialized Members
        [SerializeField] ACCategorySection m_categorySectionOwned;
        [SerializeField] ACCategorySection m_categorySectionLocked;
        [SerializeField] GameObject emptyCategoryText;
        #endregion
        int owned = -1;
        int locked = -1;

        public void SetData(MAvatarModel.ItemType item)
        {
            m_categorySectionOwned.OnDoneLoading = UpdateAfterLoadOwned;
            m_categorySectionLocked.OnDoneLoading = UpdateAfterLoadLocked;

            owned = -1;
            locked = -1;
            emptyCategoryText.SetActive(false);

            m_categorySectionOwned.SetData(ACCategorySection.SectionType.ManikinOwnedSection, item);
            m_categorySectionLocked.SetData(ACCategorySection.SectionType.ManikinLockedSection, item);
        }

        private void UpdateAfterLoadOwned(bool ItemsShowing)
        {
            owned = ItemsShowing ? 1 : 0;

            if (locked != -1)
                UpdateAfterAll();

        }

        private void UpdateAfterLoadLocked(bool ItemsShowing)
        {
            locked = ItemsShowing ? 1 : 0;

            if (owned != -1)
                UpdateAfterAll();
        }

        private void UpdateAfterAll()
        {
            if (owned == 1 || locked == 1)
                emptyCategoryText.SetActive(false);
            else
                emptyCategoryText.SetActive(true);
        }
    }
}
