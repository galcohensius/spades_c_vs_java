﻿using cardGames.controllers;
using cardGames.models;
using common.controllers;
using common.facebook;
using common.utils;

namespace cardGames.views
{
    // This class is taking care of the preview avatar section
    // updates the look of the avatar when there are changes
    public class ACMyAvatar : MonoBehaviour
    {
        #region Const Members
        const int MENU_OPEN_HEIGHT = 265;
        const int MENU_CLOSE_HEIGHT = 61;
        #endregion

        #region Serialized Members
        [SerializeField] MAvatarView m_mavatarView;
        [SerializeField] TMP_Text m_nickname;
        [SerializeField] GameObject m_menuOpen;
        [SerializeField] RectTransform m_menuBackground;
        [SerializeField] GameObject m_insideMenu;
        [SerializeField] GameObject m_arrowOpen;
        [SerializeField] GameObject m_loader;

        [SerializeField] GameObject m_nameEditBlackScreen;
        [SerializeField] NameEditToolTipView m_nameEditTooltip;

        [SerializeField] TMP_Text m_player_name;
        [SerializeField] Button m_menuButton;
        #endregion

        bool m_isMenuOpened = false;

        private void UpdateMenuSize(int value)
        {
            m_menuBackground.sizeDelta = new Vector2(m_menuBackground.sizeDelta.x, value);
        }

        public void SetData()
        {
            if (ModelManager.Instance.GetUser().GetMAvatar() == null)
                MAvatarController.Instance.TempAvatarModel = MAvatarController.Instance.CreateRandomAvatarModel(MAvatarModel.GenderType.Male);
            else
                MAvatarController.Instance.CopyMavatarModel(ModelManager.Instance.GetUser().GetMAvatar(), MAvatarController.Instance.TempAvatarModel);

            m_player_name.text = MAvatarController.Instance.TempAvatarModel.NickName + "<sprite name=rename>";
            m_mavatarView.SetAvatarModel(MAvatarController.Instance.TempAvatarModel);
            m_loader.SetActive(false);
        }

        public void ActionMenu(bool open)
        {
            if (!FacebookController.Instance.IsLoggedIn())
            {
                PopupManager.Instance.ShowQuestionOverPopups("",
                        "You are not connected to Facebook. Do you want to connect now?", "Connect", "Cancel", () =>
                        {
                            PopupManager.Instance.HideQuestionOverPopups();
                            CardGamesPopupManager.Instance.ShowSettingsPopup(common.controllers.PopupManager.AddMode.ShowAndRemove);
                        }, () =>
                        {
                            PopupManager.Instance.HideQuestionOverPopups();
                        });
                return;
            }
            m_isMenuOpened = open;

            if (m_isMenuOpened)
            {
                iTween.ValueTo(gameObject, iTween.Hash("from", MENU_CLOSE_HEIGHT, "to", MENU_OPEN_HEIGHT, "time", .1f, "onupdate", "UpdateMenuSize", "delay", 0f));
                UnityMainThreadDispatcher.Instance.DelayedCall(0.1f, () =>
                {
                    m_menuOpen.SetActive(true);
                    m_insideMenu.SetActive(true);
                    m_arrowOpen.SetActive(false);
                });
            }
            else
            {
                iTween.ValueTo(gameObject, iTween.Hash("from", MENU_OPEN_HEIGHT, "to", MENU_CLOSE_HEIGHT, "time", .1f, "onupdate", "UpdateMenuSize", "delay", 0f));
                m_insideMenu.SetActive(false);
                UnityMainThreadDispatcher.Instance.DelayedCall(0.1f, () =>
                {
                    m_menuOpen.SetActive(false);
                    m_arrowOpen.SetActive(true);
                });
            }
        }


        // randomize the only wanted items (not gender/face/shape/skintone)
        public void Button_Random_Avatar()
        {
            m_mavatarView.gameObject.GetComponent<Animator>().SetTrigger("Teleport");
            UnityMainThreadDispatcher.Instance.DelayedCall(0.5f, () =>
            {
                MAvatarModel random = MAvatarController.Instance.CreateRandomAvatarModel(MAvatarController.Instance.TempAvatarModel.Gender);
                random.NickName = MAvatarController.Instance.TempAvatarModel.NickName;

                if (MAvatarController.Instance.TempAvatarModel.Mode == MAvatarModel.MAvatarMode.FacebookAndAvatar)
                {
                    random.Mode = MAvatarModel.MAvatarMode.FacebookAndAvatar;
                    random.FB_ID = MAvatarController.Instance.TempAvatarModel.FB_ID;
                }

                MAvatarController.Instance.CopyMavatarModel(random, MAvatarController.Instance.TempAvatarModel);
                m_mavatarView.SetAvatarModel(MAvatarController.Instance.TempAvatarModel);
                MAvatarController.Instance.TempAvatarModel.FireOnModelChangedEvent(true);
            });
        }

        // resets avatar to the ModelManager mavatar model
        public void Button_Undo()
        {
            if (ModelManager.Instance.GetUser().GetMAvatar() == null)
                return;

            // check if there's available undo
            if (MAvatarController.Instance.IsEqual(ModelManager.Instance.GetUser().GetMAvatar(), MAvatarController.Instance.TempAvatarModel))
                return;

            MAvatarController.Instance.CopyMavatarModel(ModelManager.Instance.GetUser().GetMAvatar(), MAvatarController.Instance.TempAvatarModel);
            m_player_name.text = MAvatarController.Instance.TempAvatarModel.NickName + "<sprite name=rename>";

            MAvatarController.Instance.TempAvatarModel.FireOnModelChangedEvent(true);
        }

        public void OpenNameEditConsole()
        {
            m_nameEditBlackScreen.SetActive(true);
            m_nameEditTooltip.Init(MAvatarController.Instance.TempAvatarModel.NickName, SetPlayerName);
        }

        public void Btn_NameEditBlackScreenClick()
        {
            m_nameEditBlackScreen.SetActive(false);
            m_nameEditTooltip.gameObject.SetActive(false);
        }

        public void SetPlayerName(string name, bool wasAccepted)
        {
            m_nameEditBlackScreen.SetActive(false);

            if (wasAccepted)
            {
                m_player_name.text = GetNameWithEditableSuffix(name);
                m_player_name.gameObject.SetActive(true);
                TMPBidiHelper.MakeRTL(m_player_name);

                MAvatarController.Instance.TempAvatarModel.NickName = name;
                MAvatarController.Instance.TempAvatarModel.FireOnModelChangedEvent();
            }
        }


        private string GetNameWithEditableSuffix(string name)
        {
            string editableImageSuffix = "";

            // if (m_isEditable)
            // {
            editableImageSuffix = "<sprite name=rename>";
            // }

            return name + editableImageSuffix;
        }

        public MAvatarView MavatarView { get => m_mavatarView; set => m_mavatarView = value; }
    }
}
