﻿using cardGames.controllers;
using cardGames.models;
using common.controllers;
using System;

namespace cardGames.views
{
    public class ACCategory : MonoBehaviour
    {
        [SerializeField] Serialized_KeyAvatarTypeValueCategory_Dictionary m_categoryItems;

        public void SetData(AvatarCreationPopup avatarCreation)
        {
            MAvatarController.Instance.UserLeveledUp(CardGamesStateController.Instance.GetLastLevelInAvatarCreatorPopup(), ModelManager.Instance.GetUser().GetLevel());

            foreach (var item in m_categoryItems)
                item.Value.SetData(avatarCreation, MAvatarController.Instance.GetNewAvatarItemIdsForCategory(item.Key).Count);
        }

        [Serializable]
        public class Serialized_KeyAvatarTypeValueCategory_Dictionary : SerializableDictionary<MAvatarModel.ItemType, ACCategoryItem>
        {
        }
    }
}
