﻿using common.ims;
using common.ims.views;

namespace cardGames.views
{
    // This class is taking care of the preview avatar section
    // updates the look of the avatar when there are changes
    public class ACFeaturedContent : MonoBehaviour
    {
        #region Serialized Members
        const int BANNER_HEIGHT = 240; //jpeg
        const int BANNER_BOTTOM_SPACE = 365;//jpeg

        const int BUNDLE_HEIGHT = 133;
        const int BUNDLE_BOTTOM_SPACE = 68;
        const int BUNDLE_SPACE = 121;

        const int NO_BUNDLE_OR_BANNER = -10;
        #endregion

        #region Serialized Members
        [SerializeField] ACCategorySection m_categorySection;
        [SerializeField] IMSBannerView m_banner;
        [SerializeField] GameObject emptyCategoryText;
        #endregion

        #region Private Members
        bool m_isSpecial;
        #endregion
        bool flag;
        bool m_initialized = false;

        public void SetData()
        {
            if (m_initialized)
                return;

            m_initialized = true;
            m_categorySection.OnDoneLoading = UpdateAfterLoad;
            emptyCategoryText.SetActive(false);

            m_categorySection.SetSeparator("Featured");
            m_categorySection.SetFeaturedData();
            GetComponent<VerticalLayoutGroup>().padding.top = NO_BUNDLE_OR_BANNER;

            if (IMSController.Instance.BannersByLocations.ContainsKey(m_banner.Location))
            {
                m_banner.gameObject.SetActive(false);
                if (!m_banner.Model.IsAssetBundle)
                {
                    flag = true;
                }
                else
                {
                    UnityMainThreadDispatcher.Instance.DelayedCall(1f, () =>
                    {
                        m_banner.gameObject.SetActive(true);
                        GetComponent<VerticalLayoutGroup>().padding.top = BUNDLE_HEIGHT;
                        GetComponent<VerticalLayoutGroup>().spacing = BUNDLE_HEIGHT;
                        m_banner.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(-4.5f, -4.5f, 0);
                        RefreshLayout();
                    });
                }
            }
            else
            {
                m_banner.gameObject.SetActive(false);
            }
            RefreshLayout();
        }
        public void LateUpdate()
        {
            if (flag)
            {
                flag = !flag;
                GetComponent<VerticalLayoutGroup>().padding.top = NO_BUNDLE_OR_BANNER;
                GetComponent<VerticalLayoutGroup>().padding.bottom = BUNDLE_BOTTOM_SPACE;
                GetComponent<VerticalLayoutGroup>().spacing = BUNDLE_SPACE;
                RefreshLayout();
                UnityMainThreadDispatcher.Instance.DelayedCall(.1f, () =>
                {
                    m_banner.gameObject.SetActive(true);
                    GetComponent<VerticalLayoutGroup>().padding.top = BUNDLE_HEIGHT;
                    RefreshLayout();
                    m_banner.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition = new Vector3(-426, 110, 0);
                });
            }
        }

        private void RefreshLayout()
        {
            GetComponent<VerticalLayoutGroup>().enabled = false;
            GetComponent<VerticalLayoutGroup>().enabled = true;
        }

        private void UpdateAfterLoad(bool ItemsShowing)
        {
            if (ItemsShowing)
                emptyCategoryText.SetActive(false);
            else
                emptyCategoryText.SetActive(true);
        }
    }
}
