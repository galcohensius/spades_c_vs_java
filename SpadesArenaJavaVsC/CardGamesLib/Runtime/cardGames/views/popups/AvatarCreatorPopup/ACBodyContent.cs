﻿using cardGames.models;

namespace cardGames.views
{
    public class ACBodyContent : MonoBehaviour
    {
        #region Serialized Members
        [SerializeField] ACCategorySection m_shapeSection;
        [SerializeField] ACCategorySection m_genderSection;
        [SerializeField] ACCategorySection m_skinToneSection;
        #endregion

        public void SetData()
        {
            m_genderSection.SetData(ACCategorySection.SectionType.GenderSection, MAvatarModel.ItemType.Body);
            m_skinToneSection.SetData(ACCategorySection.SectionType.SkinToneSection, MAvatarModel.ItemType.Body);
            m_shapeSection.SetSeparator("Shapes");
            m_shapeSection.SetData(ACCategorySection.SectionType.ShapeSection, MAvatarModel.ItemType.Body);
        }
    }
}
