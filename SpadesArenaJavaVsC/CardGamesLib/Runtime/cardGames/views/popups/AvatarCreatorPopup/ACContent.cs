﻿using cardGames.models;
using System.Collections.Generic;

namespace cardGames.views
{
    public class ACContent : MonoBehaviour
    {
        #region Serialized Members
        [SerializeField] ACFeaturedContent m_featuredContentPrefab;
        [SerializeField] ACBodyContent m_bodyContentPrefab;
        [SerializeField] ACFaceContent m_FaceContentPrefab;
        [SerializeField] ACCategoryContent m_categoryContentPrefab;
        [SerializeField] GameObject m_loader;
        [SerializeField] Transform m_content;
        #endregion

        AvatarCreationPopup m_avatarCreation;
        #region enum Members
        MAvatarModel.ItemType m_selectedItem = MAvatarModel.ItemType.None;
        #endregion

        private List<ACBodyContent> bodyPool = new List<ACBodyContent>();
        private List<ACFeaturedContent> featurePool = new List<ACFeaturedContent>();
        private List<ACFaceContent> facePool = new List<ACFaceContent>();
        private List<ACCategoryContent> categoryPool = new List<ACCategoryContent>();

        private int bodyCount;
        private int featureCount;
        private int faceCount;
        private int categoryCount;

        private MAvatarModel.ItemType currentCategory;

        public void SetData(AvatarCreationPopup avatarCreation, MAvatarModel.ItemType selectedItem)
        {
            m_avatarCreation = avatarCreation;
            SelectCategoryToShow(selectedItem);

            currentCategory = selectedItem; 
        }

        public void SelectCategoryToShow(MAvatarModel.ItemType selectedItem)
        {

            if (selectedItem == currentCategory)
                return;

            currentCategory = selectedItem;

            bodyCount = 0;
            featureCount = 0;
            faceCount = 0;
            categoryCount = 0;

            m_avatarCreation.GetComponent<AvatarCreationPopup>().HandleWatchedCategory(m_selectedItem);

            // destory exisitng content
            foreach (Transform item in m_content)
                item.gameObject.SetActive(false);
               // Destroy(item.gameObject.SetActive);

            m_selectedItem = selectedItem;

            GameObject go;
            // init the selected channel content
            switch (m_selectedItem)
            {
                case MAvatarModel.ItemType.Featured:

                    featureCount++;

                    if (featureCount > featurePool.Count)
                    {
                        go = Instantiate(m_featuredContentPrefab.gameObject, m_content);
                        go.GetComponent<ACFeaturedContent>().SetData();
                        featurePool.Add(go.GetComponent<ACFeaturedContent>());
                    }
                    else
                    {
                        featurePool[featureCount - 1].gameObject.SetActive(true);
                        featurePool[featureCount - 1].SetData();
                    }
                    break;

                case MAvatarModel.ItemType.Body:

                    bodyCount++;
                    if (bodyCount > bodyPool.Count)
                    {
                        go = Instantiate(m_bodyContentPrefab.gameObject, m_content);
                        go.GetComponent<ACBodyContent>().SetData();
                        bodyPool.Add(go.GetComponent<ACBodyContent>());
                    }
                    else
                    {
                        bodyPool[bodyCount - 1].gameObject.SetActive(true);
                        bodyPool[bodyCount - 1].SetData();
                    }
                    break;

                case MAvatarModel.ItemType.Face:
                case MAvatarModel.ItemType.Hair:
                case MAvatarModel.ItemType.Outfit:
                case MAvatarModel.ItemType.Accessory:
                case MAvatarModel.ItemType.Trophy:
                case MAvatarModel.ItemType.Costume:

                    categoryCount++;
                    if (categoryCount > categoryPool.Count)
                    {
                        go = Instantiate(m_categoryContentPrefab.gameObject, m_content);
                        go.GetComponent<ACCategoryContent>().SetData(m_selectedItem);
                        categoryPool.Add(go.GetComponent<ACCategoryContent>());
                    }
                    else
                    {
                        categoryPool[categoryCount - 1].gameObject.SetActive(true);
                        categoryPool[categoryCount - 1].SetData(m_selectedItem);
                    }

                    break;
            }
            m_loader.SetActive(false);
        }
    }
}
