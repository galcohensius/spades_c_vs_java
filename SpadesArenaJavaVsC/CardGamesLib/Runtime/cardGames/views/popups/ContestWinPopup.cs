﻿using System.Collections.Generic;
using common.views;
using common.controllers;
using common.utils;
using common.models;
using cardGames.models;
using cardGames.controllers;
using cardGames.views.overlays;

namespace cardGames.views.popups
{
    public class ContestWinPopup : PopupBase
    {

        [SerializeField] GameObject m_flight_source = null;

        [SerializeField] List<Material> m_materials;

        [SerializeField] List<Sprite> m_trophies;
        [SerializeField] List<Sprite> m_big_wings;
        [SerializeField] List<Sprite> m_wings;

        [SerializeField] Image m_trophy;
        [SerializeField] Image m_big_wing1;
        [SerializeField] Image m_big_wing2;

        [SerializeField] List<Image> m_wings_objects;

        [SerializeField] TMP_Text m_rank_1_3;
        [SerializeField] TMP_Text m_rank_ITM;

        [SerializeField] TMP_Text m_contest_win_1_3;

        [SerializeField] Button m_claim_button;

        Animator m_anim = null;
        int m_new_balance = 0;
        int m_curr_contest_coins_awarded = 0;

        bool m_total_claims_done = false;

        [SerializeField] TMP_Text m_prize_text = null;

        protected override void Awake()
        {
            base.Awake();
            m_anim = GetComponent<Animator>();
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            StartClaimProcess();

        }

        private void StartClaimProcess()
        {
            if (ContestsController.Instance.To_claim_contests.Count > 0)
            {
                ContestsController.ClaimContestData claimContestData = ContestsController.Instance.To_claim_contests[0];
                if (claimContestData.Contest_coins_prize > 0)
                    SetGraphicsAndPlayAnim(claimContestData.Contest_rank, claimContestData.Contest_coins_prize);
                else
                    PopupManager.Instance.HidePopup();
            }
            else
            {
                PopupManager.Instance.ShowMessagePopup("This bonus can no longer be collected.");
                PopupManager.Instance.HidePopup();

            }
        }

        public void Trigger_Play_Sound(string soundName)
        {
            switch (soundName)
            {
                case "BigWin":
                    CardGamesSoundsController.Instance.Trigger_BigWing();
                    break;

                case "SmallWin":
                    CardGamesSoundsController.Instance.Trigger_SmallWin();
                    break;
            }
        }

        /*private void ClaimContest(ContestsController.ClaimContestData contest_to_claim)
        {
            CommManager.Instance.ClaimContestReward(contest_to_claim.Contest_id, BonusClaimed);
        }


        private void BonusClaimed(bool success, int new_balance, BonusAwarded bonusAwarded)
        {
            
            if (ContestsController.Instance.To_claim_contests.Count > 0)
                ContestsController.Instance.To_claim_contests.RemoveAt(0);

            Claim_button.gameObject.SetActive(false);

            if (success)
            {
                PlayCoinsFlightForHourlyBonus(bonusAwarded.CoinsAwarded);
                New_balance = new_balance;
            }
            else
            {
                PopupManager.Instance.ShowMessagePopup("This reward can no longer be collected.");
                ContestsController.Instance.To_claim_contests.Clear();
                m_manager.HidePopup();
            }
        }*/

        private void SetGraphicsAndPlayAnim(int rank, int coins_prize)
        {
            //need to check my rank - and based on the asnwer - set all the graphics params and pick the right animation

            if (rank > 0 && rank < 4)
            {
                m_trophy.sprite = m_trophies[rank - 1];
                m_big_wing1.sprite = m_big_wings[rank - 1];
                m_big_wing2.sprite = m_big_wings[rank - 1];

                foreach (Image item in m_wings_objects)
                {
                    item.sprite = m_wings[rank - 1];
                }
                m_rank_1_3.text = GetPrefix(rank) + " Place";

                m_rank_1_3.fontSharedMaterial = m_materials[rank - 1];
                m_contest_win_1_3.fontSharedMaterial = m_materials[rank - 1];

                m_anim.SetTrigger("Win_1_3");

            }
            else
            {
                m_trophy.sprite = m_trophies[3];
                m_big_wing1.sprite = m_big_wings[3];
                m_big_wing2.sprite = m_big_wings[3];

                m_rank_ITM.text = GetPrefix(rank) + " Place";

                m_anim.SetTrigger("Win_ITM");
            }

            m_prize_text.text = FormatUtils.FormatBalance(coins_prize);

        }

        private string GetPrefix(int place)
        {
            if (place == 1)
                return "1st";
            else if (place == 2)
                return "2nd";
            else if (place == 3)
                return "3rd";
            else
                return place + "th";
        }

        public void TIMELINE_Anim_Done_Show_Trail()
        {
            m_claim_button.gameObject.SetActive(true);
            m_claim_button.interactable = true;
            m_claim_button.gameObject.transform.GetChild(0).gameObject.SetActive(true);
            m_claim_button.gameObject.transform.GetChild(1).gameObject.SetActive(false);
            //PlayCoinsFlightForHourlyBonus(m_curr_contest_coins_awarded);
        }

        public void BT_ClickToContinueClicked()
        {
            m_claim_button.interactable = false;
            m_claim_button.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            m_claim_button.gameObject.transform.GetChild(1).gameObject.SetActive(true);


            if (ContestsController.Instance.To_claim_contests.Count > 0)
                ContestsController.Instance.ClaimContest(OnClaimCompleted);
            else
                PopupManager.Instance.HidePopup();
        }

        private void OnClaimCompleted(bool success, int new_balance, BonusAwarded bonusAwarded)
        {
            m_claim_button.gameObject.SetActive(false);

            if (success)
            {
                PlayCoinsFlightForHourlyBonus(bonusAwarded.GoodsList.CoinsValue);
                m_new_balance = new_balance;
            }
            else
            {
                PopupManager.Instance.HidePopup();
                PopupManager.Instance.ShowMessagePopup("This reward can no longer be collected.");
            }
        }


        public void PlayCoinsFlightForHourlyBonus(int bonus_amount)
        {
            CometOverlay comet = OverlaysManager.Instance.ShowCometOverlay();


            int num_coins_to_show = Mathf.Min(5, bonus_amount);

            comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_flight_source, CardGamesPopupManager.Instance.Lobby_flight_target
                , 0f, 0.15f, 1.5f, CardGamesPopupManager.Instance.Lobby_coins_balance, bonus_amount, CardGamesPopupManager.Instance.Lobby_coins_animator, 3, 1.0f, 0.5f
                , TrailCompleted, new Vector2(-0.2f, 0f), new Vector2(0.8f, 0.4f));
        }

        private void TrailCompleted()
        {
            ModelManager.Instance.GetUser().SetCoins(m_new_balance);

            if (ContestsController.Instance.To_claim_contests.Count > 0)
            {
                //rewind the animation to start
                m_anim.SetTrigger("Exit");
                //keep claiming
                StartClaimProcess();

            }
            else
            {
                //done claiming all missions
                //m_total_claims_done = true;
                PopupManager.Instance.HidePopup();
            }

        }

    }


}
