﻿using cardGames.controllers;
using cardGames.models;
using cardGames.views.social;
using common.controllers;
using common.facebook;
using common.facebook.models;
using common.facebook.views;
using common.models;
using common.utils;
using common.views;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.views.popups
{

    public abstract class SocialPopup : PopupBase
    {

        public enum SocialTab
        {
            Invite,
            Send,
            App_requests
        }

        const int FRIEND_ITEMS_IN_GROUP = 2;
        const int MAX_ITEMS_TO_SHOW = 50;

        const bool OPEN_STATE = false; //popup will open with everything selected if the state is True.

        [SerializeField] Transform m_app_friends_holder = null;
        [SerializeField] Transform m_app_request_holder = null;

        [SerializeField] GameObject m_invite_object = null;
        [SerializeField] GameObject m_app_friends_object = null;
        [SerializeField] GameObject m_app_requests_object = null;

        //this refers to the tab buttons
        [SerializeField] GameObject m_invite_button = null;
        [SerializeField] GameObject m_send_gift_button = null;
        [SerializeField] GameObject m_receive_gift_button = null;


        GameObject m_active_tab = null;
        GameObject m_active_button = null;

        [SerializeField] GameObject m_navigation_buttons = null;
        [SerializeField] GameObject m_connect_button = null;

        Action m_collect_all_done_callback;

        //this is a link to the send gift button in the send screen
        [SerializeField] Button m_send_gifts_button = null;

        private SocialTab m_selected_tab;

        [SerializeField] TMP_Text m_facebookConnectDescriptionText = null;
        [SerializeField] protected FacebookButton m_facebookConnectBtn;

        private const string FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG = "Get <color=#ffff0f>{0}</color> free coins!";

        

        public void Init(SocialTab tab)
        {

            SetTabActive(m_invite_object, false);
            SetTabActive(m_app_friends_object, false);
            SetTabActive(m_app_requests_object, false);

            m_selected_tab = tab;

            m_navigation_buttons.SetActive(true);
            m_invite_object.GetComponent<InviteSocialView>().Init();

            if (!FacebookController.Instance.IsLoggedIn())
            {
                ShowConnectHideRest();
                InviteClicked();
            }
            else
            {
                m_connect_button.SetActive(false);

                m_app_friends_object.GetComponent<FBListFilterView>().InitMyFilter("Send Gifts", SocialController.Instance.SendGiftButtonClicked, BuildUserAppFriendstList, SelectAllAppFriends, "Great!\nYou've sent gifts to all your friends!", false);
                m_app_requests_object.GetComponent<FBListFilterView>().InitMyFilter("Collect All", CollectAllClicked, null, null, "You have no gifts to collect.\nInvite friends to collect more gifts!", true);

                BuildUserAppFriendstList("");
                BuildAppRequestList();

                SelectAllAppFriends(OPEN_STATE, "");
                m_app_friends_object.GetComponent<FBListFilterView>().BT_SelectAllClicked(OPEN_STATE);

                if (tab == SocialTab.Invite)
                    InviteClicked();
                else if (tab == SocialTab.Send)
                    SendClicked();
                else if (tab == SocialTab.App_requests)
                    GiftClicked();
            }
        }

        private void CollectAllClicked(Action collect_all_done)
        {
            m_collect_all_done_callback = collect_all_done;

            if (!SocialController.Instance.Gift_being_claimed)
            {
                SocialController.Instance.Gift_being_claimed = true;
                SocialController.Instance.CollectAllGiftsClicked((bool success, int coins_awarded) =>
                {
                    FlyCoinsComet(coins_awarded, m_app_requests_object.GetComponent<FBListFilterView>().Button_caption.gameObject);
                });
            }
            else
            {
                //if another button of collect is already collecting we immedietally turn off the spades holder of a collectAll button
                if (collect_all_done != null)
                    collect_all_done();
            }

        }

        private void SelectAllAppFriends(bool select_all, string curr_filter)
        {
            for (int i = 0; i < ModelManager.Instance.Social_model.AppFriends.Count; i++)
            {
                ModelManager.Instance.Social_model.AppFriends[i].Selected = select_all;
            }


            RefreshItemsSelection(m_app_friends_holder, false);
        }

        private void ShowConnectHideRest()
        {
            m_connect_button.SetActive(true);
            m_app_friends_object.SetActive(false);
            m_app_requests_object.SetActive(false);

            SetFacebookConnectTextMessage();
        }

        private void SetFacebookConnectTextMessage()
        {
            FacebookBonus bonus = (FacebookBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.FacebookConnect);

            m_facebookConnectDescriptionText.gameObject.SetActive(false);
            m_facebookConnectBtn.SetMode(FacebookButton.ButtonMode.Connect);

            if (bonus != null)
            {
                if (!bonus.IsEntitled)
                {
                    m_facebookConnectDescriptionText.gameObject.SetActive(true);
                    m_facebookConnectDescriptionText.text = string.Format(FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG, FormatUtils.FormatBalance(bonus.CoinsSingle));
                    m_facebookConnectBtn.SetMode(FacebookButton.ButtonMode.Connect, true);
                }
            }
        }

        public void BuildUserAppFriendstList(string text = "")
        {
            UnityMainThreadDispatcher.Instance.DelayedCall(2, () =>
            {
                if (this != null)
                    BuildDataItems(text, ModelManager.Instance.Social_model.AppFriends, m_app_friends_holder);
            });
        }


        public void BuildAppRequestList()
        {

            Bonus gift_bonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Gift);

            int collect_gift_coins = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Gift).CoinsSingle;

            if (m_app_request_holder != null)
            {
                foreach (Transform C in m_app_request_holder)
                {
                    Destroy(C.gameObject);
                }
            }

            //needs to start building all the lists inside the lists
            List<FBRequestData> m_filtered_data = new List<FBRequestData>();

            m_filtered_data = ModelManager.Instance.Social_model.App_Requests;
            if (m_filtered_data != null)
            {
                int num_friends = m_filtered_data.Count;

                for (int i = 0; i < num_friends; i++)
                {
                    GameObject G = Instantiate(SocialController.Instance.FB_Gift_Item_Object);

                    G.GetComponent<FBGiftItemView>().SetData(m_filtered_data[i], collect_gift_coins);

                    G.transform.SetParent(m_app_request_holder);
                    G.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
                    RectTransform RT = G.GetComponent<RectTransform>();
                    RT.localPosition = new Vector3(RT.localPosition.x, RT.localPosition.y, 0);
                }

                FBListFilterView fb_list_filter_view = m_app_requests_object.GetComponent<FBListFilterView>();

                if (m_filtered_data.Count == 0)
                {
                    fb_list_filter_view.ShowEmptyMessage(true);
                    fb_list_filter_view.ShowHideActionButton(false);
                }
                else
                {
                    fb_list_filter_view.ShowHideActionButton(true);
                    fb_list_filter_view.ShowEmptyMessage(false);
                }

                if (m_selected_tab==SocialTab.App_requests)
                {
                    bool showCollectAll = LocalDataController.Instance.GetSettingAsBool("ShowCollectAllButton", true);
                    fb_list_filter_view.ShowHideActionButton(showCollectAll);
                }
            }


        }

        private void BuildDataItems(string curr_filter, List<FacebookData> source, Transform holder, bool isInviteList = false)
        {

            foreach (Transform C in holder)
            {
                Destroy(C.gameObject);
            }

            //needs to start building all the lists inside the lists
            List<FacebookData> m_filtered_data = new List<FacebookData>();

            m_filtered_data = Filter(curr_filter, source);

            int num_friends = m_filtered_data.Count;

            int num_groups = 0;

            if (num_friends > 0)
            {
                if (num_friends < FRIEND_ITEMS_IN_GROUP)
                    num_groups = 1;
                else
                    num_groups = num_friends / FRIEND_ITEMS_IN_GROUP + num_friends % 2;
            }

            int counter = 0;

            for (int i = 0; i < num_groups; i++)
            {
                GameObject G = Instantiate(SocialController.Instance.FB_Friend_Group_Item_Object);

                FBFriendItemGroupView IG = G.GetComponent<FBFriendItemGroupView>();

                for (int j = 0; j < FRIEND_ITEMS_IN_GROUP; j++)
                {
                    if (counter < num_friends)
                    {
                        IG.Friend_items[j].GetComponent<FBFriendItemView>().SetDataView(m_filtered_data[counter]);
                        counter++;
                    }
                    else
                    {
                        IG.Friend_items[j].gameObject.SetActive(false);//clear the other items of the same group as they have no data
                    }
                }

                G.transform.SetParent(holder);
                G.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
                RectTransform RT = G.GetComponent<RectTransform>();
                RT.localPosition = new Vector3(RT.localPosition.x, RT.localPosition.y, 0);
            }

            FBListFilterView fb_list_filter_view = m_app_friends_object.GetComponent<FBListFilterView>();

            if (ModelManager.Instance.Social_model.AppFriends.Count == 0)
            {
                fb_list_filter_view.ShowEmptyMessage(true);
                fb_list_filter_view.ShowHideActionButton(false);
            }
            else
            {
                fb_list_filter_view.ShowHideActionButton(true);
                fb_list_filter_view.ShowEmptyMessage(false);
            }


        }

        private void RefreshItemsSelection(Transform holder, bool invite_friends)
        {

            foreach (Transform item in holder)
            {
                item.gameObject.GetComponent<FBFriendItemGroupView>().ChangeSelectionInidcationToChilds();
            }
        }
        private List<FacebookData> Filter(string text, List<FacebookData> source)
        {
            List<FacebookData> filtered = new List<FacebookData>();

            for (int i = 0; i < source.Count; i++)
            {
                if (CheckFiltered(text, source[i], filtered))
                    filtered.Add(source[i]);
            }


            return filtered;
        }




        private bool CheckFiltered(string text, FacebookData source, List<FacebookData> filtered)
        {
            if (filtered.Count < MAX_ITEMS_TO_SHOW)
            {
                return source.FullName.ToLower().Contains(text.ToLower());
            }

            source.Selected = false;

            return false;
        }

        private void SelectTab(GameObject tab, GameObject button)
        {
            if (m_active_tab != null)
            {
                SetTabActive(m_active_tab, false);

                m_active_button.GetComponent<Image>().sprite = CardGamesPopupManager.Instance.Tab_Off_sprite;
                m_active_button.GetComponent<Image>().SetNativeSize();
                m_active_button.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().fontMaterial = CardGamesPopupManager.Instance.Tab_Off_Material;
            }

            CardGamesSoundsController.Instance.TabsSFX();

            m_active_button = button;
            m_active_tab = tab;

            button.GetComponent<Image>().sprite = CardGamesPopupManager.Instance.Tab_On_sprite;
            m_active_button.GetComponent<Image>().SetNativeSize();

            m_active_button.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().fontMaterial = CardGamesPopupManager.Instance.Tab_On_Material;

            SetTabActive(m_active_tab, true);
        }

        private void SetTabActive(GameObject tabObject, bool active)
        {
            CanvasGroup oldCanvasGroup = tabObject.GetComponent<CanvasGroup>();
            oldCanvasGroup.alpha = active ? 1 : 0;
            oldCanvasGroup.interactable = active;
            oldCanvasGroup.blocksRaycasts = active;
        }

        //show FB dialog for choosing friends
        public void InviteFriendsToPlay()
        {
            SocialController.Instance.InviteButtonClicked();
        }

        public void InviteClicked()
        {
            SelectTab(m_invite_object, m_invite_button);

            m_connect_button.SetActive(false);

        }

        public void SendClicked()
        {
            SelectTab(m_app_friends_object, m_send_gift_button);

            if (!FacebookController.Instance.IsLoggedIn())
                ShowConnectHideRest();
            else
            {
                m_connect_button.SetActive(false);
            }

        }

        public void GiftClicked()
        {
            SelectTab(m_app_requests_object, m_receive_gift_button);

            if (!FacebookController.Instance.IsLoggedIn())
                ShowConnectHideRest();
            else
            {
                m_connect_button.SetActive(false);
            }

        }

        public void BT_InviteClicked()
        {
            FacebookController.Instance.Login((logged_success) =>
            {
                if (logged_success)
                    Init(SocialTab.Invite);
            });
        }

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }
        //public override Vector3 GetPosition()
        //{
        //	return new Vector3 (0, -40f, 0);
        //} 

        //implementation could be even simplified but that needs re-doing OverlaysManager/SpadesOverlayManager, ManagerView and LobbyTopController.
        //not sure it worth the time but should be considered for future rummy implementation
        public abstract void FlyCoinsComet(int prize_value, GameObject start_game_object);

        protected void CometsDoneFlying()
        {
            if (this != null)
                StartCoroutine(PostCometsFlight());

        }

        public void BT_ConnectToFBClicked()
        {
            m_facebookConnectBtn.SetMode(FacebookButton.ButtonMode.Waiting);
            HandleClickConnectToFacebook();
        }

        protected abstract void HandleClickConnectToFacebook();

        IEnumerator PostCometsFlight()
        {
            SocialController.Instance.Gift_being_claimed = false;
            SocialController.Instance.PostClaimGifts();

            yield return null;
        }

        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }


        public override void Close()
        {
            SocialController.Instance.Gift_being_claimed = false;

            m_invite_object.GetComponent<InviteSocialView>().UnregisterFromEvents();

            SocialController.Instance.PostClaimGifts();

            base.Close();
        }




        public Action Collect_all_done_callback
        {
            get
            {
                return m_collect_all_done_callback;
            }
        }

        public Button Send_gifts_button
        {
            get
            {
                return m_send_gifts_button;
            }

            set
            {
                m_send_gifts_button = value;
            }
        }

        public GameObject App_friends_object
        {
            get
            {
                return m_app_friends_object;
            }
        }
    }
}
