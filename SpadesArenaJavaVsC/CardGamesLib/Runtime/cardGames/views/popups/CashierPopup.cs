﻿using common.controllers;
using common.models;
using common.utils;
using cardGames.models;
using common.views;
using common;
using cardGames.controllers;
using common.ims;
using common.mes;

namespace cardGames.views.popups
{

    public class CashierPopup : PopupBase
    {
        public enum CoinsFormat
        {
            Price,
            BuyIn,
            Balance
        }

        Button m_close_button;
        Button m_redeem_button;

        CoinsFormat m_coins_format;
        bool m_show_was = false;
        Vector2 m_price_offset = new Vector2();

        bool m_is_in_asset_bundle = false;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);
            PlayBGMusic(true);
        }

        public void RedeemButtonClicked()
        {
            m_manager.HidePopup();
            //SpadesPopupManager.Instance.ShowRedeemCodePopup(PopupManager.AddMode.ShowAndKeep);
        }

        public override void CloseButtonClicked()
        {
            if (m_is_in_asset_bundle)
            {
                Destroy(gameObject);
            }
            else
            {
                m_manager.HidePopup();

                IMSController.Instance.TriggerEvent(IMSController.EVENT_CLOSE_CASHIER);

                MESBase.Instance?.Execute(MESBase.TRIGGER_CLOSE_CASHIER);

                PlayBGMusic();
            }

        }

        private void ParseConfigJson(string incoming_data)
        {
            JSONNode jsonNode = JSON.Parse(incoming_data);
            string m_coins_format_str = jsonNode["coins_format"];

            m_coins_format = CoinsFormat.Balance;

            switch (m_coins_format_str)
            {
                case "Price":
                    {
                        m_coins_format = CoinsFormat.Price;
                        break;
                    }
                case "BuyIn":
                    {
                        m_coins_format = CoinsFormat.BuyIn;
                        break;
                    }
                case "Balance":
                    {
                        m_coins_format = CoinsFormat.Balance;
                        break;
                    }

            }

            m_show_was = jsonNode["show_was_line"].AsInt == 1;
            JSONArray offset = jsonNode["price_offset"].AsArray;
            m_price_offset.x = (float)offset[0];
            m_price_offset.y = (float)offset[1];
        }



        public void InitAndLink(Cashier cashier, bool is_in_asset_bundle = false)
        {
            m_is_in_asset_bundle = is_in_asset_bundle;

            m_close_button = transform.FindDeepChild("CloseButton").GetComponent<Button>();
            m_close_button.gameObject.AddComponent<ButtonsOverView>();
            Transform redeem_button_root = transform.FindDeepChild("RedeemButton");

            if (redeem_button_root != null)
            {
                if (redeem_button_root.gameObject.activeSelf)
                {
                    m_redeem_button = redeem_button_root.transform.FindDeepChild("ButtonImage").GetComponent<Button>();

                    redeem_button_root.gameObject.AddComponent<ButtonsOverView>();

                    m_redeem_button.onClick.AddListener(() =>
                    {
                        RedeemButtonClicked();
                    });

                }
            }

            Transform configTransform = transform.FindDeepChild("Config");
            if (configTransform != null)
            {
                string config = string.Empty;
                if (configTransform.gameObject.GetComponent<Text>() != null)
                    config = configTransform.gameObject.GetComponent<Text>().text;

                else if (configTransform.gameObject.GetComponent<TMP_Text>() != null)
                    config = configTransform.gameObject.GetComponent<TMP_Text>().text;

                ParseConfigJson(config);

            }


            //these 2 varibles are for hiding the offer we dont want to show in the case of more offers than items to show
            int num_items = transform.FindDeepChild("ItemsHolder").transform.childCount;
            bool jumped_over = false;
            AssetBundleUtils.SetTextForTransform(transform.FindDeepChild("LevelGroupText"), "LEVELS " + GetLevelText());

            if (Is_in_asset_bundle)
                return;

            //change the music output to the correct mixer
            ChangeMusicOutputToCorrectMixer();

            //read actual data - only when in the client scene and not in asset bundle 
            int i = 1;
            foreach (CashierItem cItem in cashier.Cashier_packages)
            {
                // ICollection<CashierItem> items_of_group = cashier.Cashier_packages[group_id].Values;


                // foreach (CashierItem item in items_of_group)
                // {
                //we are getting 6 offers - but current cashier supports only 5 - so when we get to the 2nd offer - we jump over it and show the next one
                if (num_items == 5 && i == 5 && !jumped_over)
                {
                    jumped_over = true;
                }
                else
                {
                    GameObject cashierItem = transform.FindDeepChild("CashierItem" + i).gameObject;
                    cashierItem.AddComponent<CashierItemView>();
                    cashierItem.GetComponent<CashierItemView>().SetData(cItem, m_coins_format, m_show_was, m_price_offset);

                    Transform m_voucher_message = transform.FindDeepChild("tc");
                    if (cashierItem.GetComponent<CashierItemView>().Contains_Voucher)
                    {
                        if (m_voucher_message != null)
                            m_voucher_message.gameObject.SetActive(true);
                    }
                    else
                    {
                        if (m_voucher_message != null)
                            m_voucher_message.gameObject.SetActive(false);
                    }
                    i++;
                }

                //}
            }
        }

        protected virtual void ChangeMusicOutputToCorrectMixer()
        {
            if (transform.FindDeepChild("Sounds") != null)
            {
                transform.FindDeepChild("BGSound").gameObject.GetComponent<AudioSource>().volume = CardGamesStateController.Instance.GetMusicSetting();
                transform.FindDeepChild("SFX1").gameObject.GetComponent<AudioSource>().volume = CardGamesStateController.Instance.GetSFXSetting();
                transform.FindDeepChild("SFX2").gameObject.GetComponent<AudioSource>().volume = CardGamesStateController.Instance.GetSFXSetting();
            }
        }

        protected virtual void PlayBGMusic(bool stop = false)
        {
            SoundsController.Instance.PlayBGMusic(stop);
        }

        private string GetLevelText()
        {
            int curr_level = ModelManager.Instance.GetUser().GetLevel();

            if (curr_level >= 180)
            {
                return "180+";
            }
            else if (curr_level >= 140)
            {
                return "140-179";
            }
            else if (curr_level >= 100)
            {
                return "100-139";
            }
            else if (curr_level >= 60)
            {
                return "60-99";
            }
            else if (curr_level >= 20)
            {
                return "20-59";
            }
            else
            {
                return "1-19";
            }

        }

        public bool Is_in_asset_bundle { get => m_is_in_asset_bundle; set => m_is_in_asset_bundle = value; }
    }
}