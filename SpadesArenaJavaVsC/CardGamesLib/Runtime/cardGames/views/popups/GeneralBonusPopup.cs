﻿using cardGames.controllers;
using cardGames.models;
using common.controllers;
using common.models;
using common.utils;
using common.views;
using cardGames.views.overlays;
using System.Collections;
using System.Collections.Generic;
using common.ims;
using common.ims.model;

namespace cardGames.views.popups
{
    public class GeneralBonusPopup : PopupBase
    {
        [SerializeField] GameObject m_avatar_item_panel = null;
        [SerializeField] GameObject m_vouchers_panel = null;
        [SerializeField] GameObject m_coins_panel = null;

        [SerializeField] TMP_Text m_reward_coins = null;
        [SerializeField] TMP_Text m_voucherQuantity = null;
        [SerializeField] TMP_Text m_voucherValue = null;

        [SerializeField] GameObject m_button_text = null;
        [SerializeField] GameObject m_working_animation_indicator = null;
        [SerializeField] Button m_collect_button = null;

        [SerializeField] GameObject m_coins_emmiter = null;

        [SerializeField] GameObject m_footer;

        [SerializeField] ACManikinItem m_manikinItem;

        float m_xp_icon_width = 42f;

        [SerializeField] TMP_Text m_descriptionText = null;

        GeneralBonus m_bonus = null;
        private BonusAwarded m_bonusAwarded;
        private string m_token;
        private bool m_autoClose;

        private IMSInteractionZone m_originIZone;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            CardGamesSoundsController.Instance.PlayBonusReceived();
        }


        public void Init(Bonus.BonusTypes bonus_type, BonusAwarded bonusAwarded, bool autoClose,IMSInteractionZone originIZone)
        {
            m_autoClose = autoClose;
            m_originIZone = originIZone;

            if (bonusAwarded != null)
            {
                // Awarded bonus takes precedence over type
                HandleVouchers(bonusAwarded);
                HandleAvatar(bonusAwarded);
                HandleCoins(bonusAwarded.GoodsList.Coins);
            }
            else
            {
                // Find the bonus by type - currently only coins!
                m_avatar_item_panel.SetActive(false);
                m_vouchers_panel.SetActive(false);

                Bonus bonus = ModelManager.Instance.FindBonus(bonus_type);
                m_bonus = (GeneralBonus)bonus;

                if (m_bonus != null)
                {
                    m_token = m_bonus.Token;

                    HandleCoins(new List<int> { bonus.CoinsSingle });

                    if (m_bonus.Type == Bonus.BonusTypes.FacebookConnect)
                    {
                        m_descriptionText.text = "Thank you for connecting to Facebook!";
                    }
                    else if (m_bonus.Type == Bonus.BonusTypes.RewardedVideoBonus)
                    {
                        m_descriptionText.text = "Free coins for watching the Video!";
                    }
                    else
                    {
                        m_descriptionText.text = "You get free coins!";
                    }

                }
            }



            float pos_x2 = m_button_text.gameObject.GetComponent<RectTransform>().localPosition.x - m_reward_coins.preferredWidth / 2 + m_xp_icon_width;
            m_coins_emmiter.GetComponent<RectTransform>().localPosition = new Vector3(pos_x2, m_coins_emmiter.GetComponent<RectTransform>().localPosition.y, 0);

            if (m_autoClose)
            {
                m_footer.gameObject.SetActive(false);


                StartCoroutine(DelayedCall(2.5f, bonusAwarded));
            }
        }

        IEnumerator DelayedCall(float delay, BonusAwarded awarded_bonus)
        {
            yield return new WaitForSeconds(delay);
            OnBonusClaimed(true, awarded_bonus);
        }



        private void HandleVouchers(BonusAwarded awarded_bonus)
        {
            if (awarded_bonus.GoodsList.Vouchers.Count > 0)
            {
                m_voucherQuantity.text = "x" + FormatUtils.FormatBalance(awarded_bonus.GoodsList.Vouchers[0].Count);
                m_voucherValue.text = FormatUtils.FormatBuyIn(awarded_bonus.GoodsList.Vouchers[0].VouchersValue);
                return;
            }
            m_vouchers_panel.SetActive(false);
        }

        private void HandleCoins(List<int> coins)
        {
            if (coins.Count > 0)
            {
                int value = coins[0];

                if (!m_vouchers_panel.activeSelf && !m_avatar_item_panel.activeSelf)
                    m_reward_coins.text = FormatUtils.FormatBalance(value);
                else
                    m_reward_coins.text = FormatUtils.FormatPrice(value);
                return;
            }

            m_coins_panel.SetActive(false);
        }

        private void HandleAvatar(BonusAwarded awarded_bonus)
        {
            if (awarded_bonus.GoodsList.InventoryItems.Count > 0)
            {
                m_avatar_item_panel.SetActive(true);

              /*  MavatarItem item = new MavatarItem(awarded_bonus.GoodsList.InventoryItems[0].ToString());
                List<MavatarItem> items = new List<MavatarItem>();
                items.Add(item);
                List<GameObject> goItems = MAvatarController.Instance.GetListOfItemsByInventoryId(items, m_avatar_item_panel);
                if (goItems != null)
                    m_manikinItem.SetData(item);*/
            }
            else
            {
                m_avatar_item_panel.SetActive(false);
            }
        }

        public void BT_Collect_Clicked()
        {
            m_button_text.SetActive(false);
            m_working_animation_indicator.SetActive(true);
            m_collect_button.interactable = false;
            HandleButtonClicked();

            string imsMetadata = m_originIZone?.Metadata ?? null;

            if (m_bonus != null)
            {
                BonusController.Instance.ClaimBonus(m_bonus.Type, 0, 0, null, 0, OnBonusClaimed, m_token,imsMetadata: imsMetadata);
            }
            else if (m_bonusAwarded != null)
            {
                OnBonusClaimed(true, m_bonusAwarded);
            }
        }


        public void OnBonusClaimed(bool success, BonusAwarded awarded_bonus)
        {
            if (success)
            {
                IMSController.Instance.TriggerEvent(IMSController.EVENT_BONUS_AWARD,true);

                if (awarded_bonus.GoodsList.Coins.Count > 0)
                    HandleSuccessfulBonusClaimed(awarded_bonus);
                else
                    m_manager.HidePopup();
            }
            else
            {
                LoggerController.Instance.Log("General Bonus Claim Error");
                m_manager.HidePopup();
            }

        }


        protected void TrailCompleted()
        {
            m_manager.HidePopup();

        }

        protected void HandleButtonClicked()
        {
            ViewUtils.EnableDisableButtonText(m_collect_button);
        }

        protected void HandleSuccessfulBonusClaimed(BonusAwarded awarded_bonus)
        {

            CometOverlay coins_comet = OverlaysManager.Instance.ShowCometOverlay();

            coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, 5, m_coins_emmiter,
                    CardGamesPopupManager.Instance.Lobby_flight_target, 0f, 0.15f, .75f,
                    CardGamesPopupManager.Instance.Lobby_coins_balance, awarded_bonus.GoodsList.CoinsValue,
                    CardGamesPopupManager.Instance.Lobby_coins_animator, 2, 0.3f, 0.3f,
                    TrailCompleted, new Vector2(0.4f, 0.4f));

        }
    }
}