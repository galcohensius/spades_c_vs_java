
using cardGames.controllers;
using common.controllers;
using common.views;
using static common.models.CommonUser;

namespace cardGames.views.popups
{

    public class BanUserPopup : PopupBase {

        [SerializeField] GameObject m_close_btn = null;
		[SerializeField] TMP_Text m_title_text = null;
        [SerializeField] TMP_Text m_desc_text = null;

        AccountStatus m_accountStatus;


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            switch(m_accountStatus)
            {
                case AccountStatus.BanFromLogin:
                    m_title_text.text = "Failed to connect";
                    m_desc_text.text = "Your account has been suspended due to a violation of our terms of service.\nFor more information please contact Support.";
                    m_close_btn.SetActive(false);
                    break;
                case AccountStatus.BanFromPurchase:
                    m_title_text.text = "Purchase Error";
                    m_desc_text.text = "Your account is prohibited from conducting any further purchases.\nFor more information please contact Support.";
                    break;
                case AccountStatus.WarnBeforeBan:
                    m_title_text.text = "IMPORTANT NOTICE!";
                    m_desc_text.text = "Our system detected a refund request from your account. Since this is an uncommon event, your future transactions will be monitored.\nYour experience is important to us, therefore we kindly request that you contact our support team to clarify the matter.";
                    break;
            }

        }

        public void ContactSupportClick()
        {
            WebExternalController.Instance.OpenSupportPage();
        }

        public override void CloseButtonClicked()
		{
            if (m_close_btn.activeSelf)
			    m_manager.HidePopup ();
		}


        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }
        
        public AccountStatus AccountStatus { get => m_accountStatus; set => m_accountStatus = value; }
    }
}
