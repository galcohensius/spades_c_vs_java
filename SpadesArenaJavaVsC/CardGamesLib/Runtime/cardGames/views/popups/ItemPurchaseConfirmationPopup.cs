
using common.views;
using System;
using common.controllers;
using common.utils;

namespace cardGames.views.popups
{

	public class ItemPurchaseConfirmationPopup : PopupBase {

        [SerializeField] TMP_Text 			m_desc_text=null;
        [SerializeField] Button m_accept_btn = null;
        [SerializeField] GameObject m_footer = null;
        bool m_autoAccept;

        public Action OnAccept;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            if (m_autoAccept)
            {
                // Hide the button start a timer and change the text
                m_desc_text.text = "Processing purchase, please wait...";

                m_footer.SetActive(false);
                (gameObject.transform as RectTransform).sizeDelta = new Vector2(1050, 400);

                InvokingUtils.InvokeActionWithDelay(false,3,() =>
                 {
                     if (this != null)
                     {
                         OnAcceptClick();
                     }
                 },this);
            }
        }

        public override void CloseButtonClicked()
		{
			m_manager.HidePopup ();
		}

		public void OnAcceptClick()
		{
            m_manager.HidePopup();
            OnAccept?.Invoke();
        }


        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }
        
        public bool AutoAccept { get => m_autoAccept; set => m_autoAccept = value; }
    }
}
