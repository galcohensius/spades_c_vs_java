﻿using cardGames.controllers;
using cardGames.models;
using common.controllers;
using common.controllers.purchase;
using common.ims;
using common.models;
using common.utils;
using common.views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace cardGames.views.popups
{
    public class InsufficientFundsPopup : PopupBase
    {
        [SerializeField] protected TMP_Text m_missing_amount_text = null;
        [SerializeField] protected TMP_Text m_coin_amount_text = null;
        [SerializeField] protected TMP_Text m_money_amount_text = null;
        [SerializeField] protected TMP_Text m_bonus_amount_text = null;
        [SerializeField] protected GameObject m_bonus_container = null;

        protected CashierItem m_cashier_item = null;
        protected PurchaseTrackingData m_purchaseTrackingData;

        PopupClosedDelegate m_cancel_selected = null;


        public void CreateMessageAndButton(int delta, PopupClosedDelegate close_delegate, PopupClosedDelegate cancel_selected_delegate)
        {
            //i assume all the offers are arranged by their coin amount - from lowest to highest

            m_cancel_selected = cancel_selected_delegate;

            m_close_delegate = close_delegate;

            bool found_item = false;
            m_cashier_item = null;


            Cashier cashier = ModelManager.Instance.GetCashier();

            // find the package id that matches the delta needed
            ICollection<CashierItem> items_of_group = cashier.Cashier_packages;

            foreach (CashierItem item in items_of_group)
            {
                if (!found_item)
                {
                    if (item.GoodsList.CoinsValue > delta)
                    {
                        found_item = true;
                        m_cashier_item = item;
                    }
                }

            }

            if (m_cashier_item == null)
            {
                // Show the biggest offer available
                m_cashier_item = items_of_group.Last();
            }


            //checking if there is a optimum cashier from elad - if so - make sure its big enough (coins wise compared to delta) - and if so return that - if not - return what is already chosen

            int optimum_group = ModelManager.Instance.GetCashier().Optimum_group;

            if (optimum_group != 0)
            {
                CashierItem optimum_item = ModelManager.Instance.GetCashier().Cashier_packages[optimum_group];
                if (optimum_item.GoodsList.CoinsValue >= delta)
                    m_cashier_item = optimum_item;

            }

            FillTexts(delta);

            // Create the tracking data
            m_purchaseTrackingData = new PurchaseTrackingData()
            {
                imsMetaData = ModelManager.Instance.GetCashier().ImsMetadata,
                imsActionValue = m_cashier_item.ImsActionValue,
                clientMetaData = IMSController.CLIENT_EVENT_INSUFFICIENT_FUNDS
            };

        }

        virtual protected void FillTexts(int delta)
        {
            m_missing_amount_text.text = "You need at least <color=yellow><sprite=1>" + FormatUtils.FormatBalance(delta) + "</color> more to play.";
            m_coin_amount_text.text = String.Format(m_coin_amount_text.text, FormatUtils.FormatBalance(m_cashier_item.GoodsList.CoinsValue));
            m_money_amount_text.text = String.Format(m_money_amount_text.text, m_cashier_item.Price);

            if (m_cashier_item.Bonus == 0)
            {
                m_bonus_container.SetActive(false);
            }
            else
            {
                m_bonus_amount_text.text = String.Format(m_bonus_amount_text.text, m_cashier_item.Bonus);
            }
        }

        public override void CloseButtonClicked()
        {
            //special case for cancel - one that will not be triggered like close - this is for cancelled special offer buy and be able to go back to the offer for rebuy in arena lobby
            m_cancel_selected?.Invoke();

            m_manager.HidePopup();
        }

        public void CashierClicked()
        {
            CashierController.Instance.BuyPackage(m_cashier_item.Id, m_purchaseTrackingData);
        }
    }
}