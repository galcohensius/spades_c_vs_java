﻿using cardGames.controllers;
using cardGames.models;
using common.utils;
using common.views;
using System;

namespace cardGames.views.popups
{
    public class NewVersionPopup : PopupBase
    {

        [SerializeField] TMP_Text m_body_text = null;

        [SerializeField] GameObject m_UpdateButton = null;
        [SerializeField] GameObject m_CancelButton = null;
        Action m_on_no_new_version = null;


        VersionData m_versionData;

        public void buildPopup(VersionData versionData, int curr_version_number, Action on_no_new_version)
        {

            m_on_no_new_version = on_no_new_version;
            m_versionData = versionData;

            m_body_text.text = string.Format("A new version is available!");

            if (versionData.ForceUpdate)
            {
                m_body_text.text += "\n\nThis update is required in order to continue playing.";
                m_CancelButton.SetActive(false);
                m_UpdateButton.GetComponent<RectTransform>().localPosition = new Vector3(0, m_UpdateButton.GetComponent<RectTransform>().localPosition.y, 0);
            }
            else
            {
                m_body_text.text += "\n\nPlease update in order to continue playing.";
            }

            m_body_text.text += string.Format("\n\n<size=36>Latest version: {0}</size>", FormatUtils.VersionCodeToVersion(versionData.Version));

        }

        public override void OnBackButtonClicked()
        {
            if (!m_versionData.ForceUpdate)
                BT_cancelClicked();
        }

        public void BT_updateClicked()
        {
            //m_manager.HidePopup ();
            VersionController.Instance.OpenAppStore();
        }

        public void BT_cancelClicked()
        {
            if (m_on_no_new_version != null)
                m_on_no_new_version();

            m_manager.HidePopup();
        }
    }
}