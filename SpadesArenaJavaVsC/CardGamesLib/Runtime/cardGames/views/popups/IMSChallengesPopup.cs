﻿using common.utils;
using common.ims.model;
using cardGames.controllers;
using common.views.popups;
using cardGames.models;
using System;
using System.Collections;
using common;
using common.ims;
using common.comm;

namespace cardGames.views.popups
{
    public class IMSChallengesPopup : IMSPopup
    {

        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            IMSPODynamicContent dynamicContentPO = CommManager.Instance.GetModelParser().ParsePODynamicContent(m_iZone.DynamicContent.Content[0]);

            Mission mission = ModelManager.Instance.GetMission();

            // handle no mission or no active challenge
            if (mission == null || mission.GetActiveChallenge() == null)
            {
                CloseButtonClicked();
                return;
            }

            // Title
            Transform titleGO = transform.FindDeepChild("Title");
            if (titleGO != null)
            {
                TMP_Text title = titleGO.GetComponent<TMP_Text>();
                title.text = MissionController.Instance.GetTitleFromChallenge(mission.GetActiveChallenge());
            }


            // Description
            Transform descriptionGO = transform.FindDeepChild("Description");
            if (descriptionGO != null)
            {
                TMP_Text desc = descriptionGO.GetComponent<TMP_Text>();
                desc.text = MissionController.Instance.GetDescriptionFromChallenge(mission.GetActiveChallenge());
            }

            // Icon
            //Transform iconGO = transform.FindDeepChild("Icon");
            //if (iconGO != null)
            //{
            //    Sprite icon = MissionController.Instance.GetMissionIcon(mission.GetActiveChallenge());

            //}

            // handeling the timer
            Transform endsIn = transform.FindDeepChild("EndsIn");
            if (endsIn != null)
            {
                TMP_Text timer = endsIn.GetComponent<TMP_Text>();
                timer.text = DateUtils.TimespanToDateMission(TimeSpan.FromTicks(mission.End_date.Ticks));

                // run the timer
                TimeSpan delta = mission.End_date - DateTime.Now;

                StartCoroutine(RunTimer(delta, timer));
                StartCoroutine(CountToMissionExpire(mission));
            }


            // handeling the reward
            Transform rewardGO = transform.FindDeepChild("Reward");
            if (rewardGO != null)
            {
                TMP_Text reward = rewardGO.GetComponent<TMP_Text>();
                reward.text = "<sprite=1>" + FormatUtils.FormatBalance(mission.GetActiveChallenge().Prize);
            }


            int progress = mission.GetActiveChallenge().Progress;
            int totalProgress = mission.GetActiveChallenge().TotalProgress;

            // handeling the progress bar
            Transform totalChallenges = transform.FindDeepChild("TotalChallenges");
            if (totalChallenges != null)
            {
                TMP_Text totalChallengesTxt = totalChallenges.GetComponent<TMP_Text>();

                totalChallengesTxt.text = mission.GetNumChallenges().ToString();
            }

            Transform progressTrns = transform.FindDeepChild("ProgressText");
            if (progressTrns != null)
            {
                TMP_Text progressText = progressTrns.GetComponent<TMP_Text>();
                progressText.text = FormatUtils.FormatBalance(progress) + "/" + FormatUtils.FormatBalance(totalProgress);

                progressTrns = transform.FindDeepChild("FullProgress");
                if (progressTrns != null)
                {
                    float width = progressTrns.GetComponent<RectTransform>().rect.width;
                    Transform fillProgress = transform.FindDeepChild("FillProgress");
                    if (fillProgress != null)
                        fillProgress.GetComponent<RectTransform>().sizeDelta = new Vector2(width * ((float)progress / (float)totalProgress), fillProgress.GetComponent<RectTransform>().rect.height);
                    //   m_progressBar_image.fillAmount = (float)progress / (float)total;
                }
            }


            Transform btn = transform.FindDeepChild("actionBtn");
            if (btn != null)
            {
                btn.GetComponent<Button>().onClick.AddListener(() => { ButtonClicked(); });
                btn.gameObject.AddComponent<ButtonsOverView>();
            }

        }

        private IEnumerator RunTimer(TimeSpan ts, TMP_Text timer)
        {
            float startTime = Time.realtimeSinceStartup;

            while (ts.TotalSeconds >= 0)
            {
                timer.text = DateUtils.TimespanToDateMission(ts);
                yield return new WaitForSecondsRealtime(1f);
                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }
        }

        IEnumerator CountToMissionExpire(Mission mission)
        {
            TimeSpan time_to_wait = mission.End_date - DateTime.Now;

            yield return new WaitForSecondsRealtime((float)time_to_wait.TotalSeconds);

            // once the mission expired, close the popup
            CloseButtonClicked();
        }

        public void ButtonClicked()
        {
            IMSController.Instance.ExecuteAction(m_iZone);
        }

       
    }
}