﻿using common.views;
using common.models;
using common.controllers;
using cardGames.controllers;

namespace cardGames.views.popups
{

    public abstract class RedeemCodePopup : PopupBase
    {
        private const int MIN_CODE_LENGTH = 5;
        private int redeemFails = 0;


        private const string TOO_MANY_FAILED_ATTEMPTS = "Too many failed attempts,\nplease try again later.";
        private const string INVALID_CODE = "Invalid code, please try again.";

        [SerializeField] TMP_Text m_status_text = null;
        [SerializeField] TMP_InputField m_code_text = null;
        [SerializeField] protected Button m_redeem_btn = null;
        [SerializeField] GameObject m_redeem_button_text = null;
        [SerializeField] GameObject m_spades_holder = null;

        int fails = 0;

        public void Init()
        {
            m_code_text.text = ""; //reset code view

            m_redeem_btn.interactable = false;
            HandleEnableRedeemButton();

            m_spades_holder.SetActive(false);
            m_redeem_button_text.SetActive(true);

            m_status_text.text = "";

            if (redeemFails > 3)
            {
                m_code_text.text = ""; //reset code view
                m_status_text.text = TOO_MANY_FAILED_ATTEMPTS;
                (m_code_text.placeholder as TMP_Text).text = "LOCKED!";
                m_code_text.enabled = false;
            }
        }

        protected abstract void HandleEnableRedeemButton();

        //Invoked when the value of the text field changed.
        public void OnTextChanged(string value)
        {
            //set button active only if 5 chars code entered
            m_redeem_btn.interactable = m_code_text.text.Length >= MIN_CODE_LENGTH;
            HandleEnableRedeemButton();
        }

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }

        public void OnRedeemClick()
        {
            m_redeem_btn.interactable = false;
            HandleEnableRedeemButton();
            m_spades_holder.SetActive(true);
            m_redeem_button_text.SetActive(false);

            m_status_text.text = "Validating code";
            string bonusToken = m_code_text.text;

            BonusController.Instance.ClaimBonus(type: Bonus.BonusTypes.GeneralBonus,
                                                token: bonusToken,
                                                claimedDelegate: OnClaimComplete,
                                                showErrorMessage: false,
                                                isRedeem: true);

        }

        private void OnClaimComplete(bool success, BonusAwarded bonusAwarded)
        {
            m_spades_holder.SetActive(false);
            m_redeem_button_text.SetActive(true);

            if (success)
            {
                PopupManager.Instance.HidePopup();
                CardGamesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.GeneralBonus, PopupManager.AddMode.ShowAndRemove, null, true, bonusAwarded);
            }
            else
            {
                redeemFails++;

                if (redeemFails <= 3)
                {
                    m_status_text.text = INVALID_CODE;
                    m_code_text.text = ""; //reset code view
                }
                else
                {
                    m_code_text.text = ""; //reset code view
                    m_status_text.text = TOO_MANY_FAILED_ATTEMPTS;
                    (m_code_text.placeholder as TMP_Text).text = "LOCKED!";
                    m_code_text.enabled = false;
                }
            }

        }

        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }
    }
}
