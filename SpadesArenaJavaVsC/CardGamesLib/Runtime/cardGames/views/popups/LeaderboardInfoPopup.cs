﻿using common.views;
using System.Collections.Generic;
using cardGames.models;
using cardGames.controllers;
using common.controllers;

namespace cardGames.views.popups
{
    public class LeaderboardInfoPopup : PopupBase
    {
        [SerializeField] Material m_selected_tab_material = null;
        [SerializeField] Material m_UNselected_tab_material = null;


        [SerializeField] Sprite m_selected_tab_image = null;
        [SerializeField] Sprite m_regular_tab_image = null;

        [SerializeField] GameObject m_infoContent = null;
        [SerializeField] GameObject m_rulesContent = null;
        [SerializeField] GameObject m_divisionsContent = null;

        [SerializeField] Image m_infoTabBg = null;
        [SerializeField] Image m_rulesTabBg = null;
        [SerializeField] Image m_divisionsTabBg = null;

        [SerializeField] LeaderboardInfoDivisionView m_bronzeDivision = null;
        [SerializeField] LeaderboardInfoDivisionView m_silverDivision = null;
        [SerializeField] LeaderboardInfoDivisionView m_goldDivision = null;
        [SerializeField] LeaderboardInfoDivisionView m_platinumDivision = null;

        public void Init()
        {
            ShowTab("info");
        }


        public void OnInfoClick()
        {
            ShowTab("info");
        }

        public void OnRulesClick()
        {
            ShowTab("rules");
        }

        public void OnDivisionsClick()
        {
            ShowTab("divisions");
        }

        public override void CloseButtonClicked()
        {
            PopupManager.Instance.HidePopup();
        }

        private void ShowTab(string tabId)
        {
            m_infoTabBg.gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<TMP_Text>().fontMaterial = m_UNselected_tab_material;
            m_rulesTabBg.gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<TMP_Text>().fontMaterial = m_UNselected_tab_material;
            m_divisionsTabBg.gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<TMP_Text>().fontMaterial = m_UNselected_tab_material;



            switch (tabId)
            {
                case "info":
                    m_infoTabBg.sprite = m_selected_tab_image;
                    m_rulesTabBg.sprite = m_regular_tab_image;
                    m_divisionsTabBg.sprite = m_regular_tab_image;

                    m_infoContent.SetActive(true);
                    m_rulesContent.SetActive(false);
                    m_divisionsContent.SetActive(false);
                    SetAllImagesNativeSize();



                    m_infoTabBg.gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<TMP_Text>().fontMaterial = m_selected_tab_material;

                    break;
                case "rules":
                    m_infoTabBg.sprite = m_regular_tab_image;
                    m_rulesTabBg.sprite = m_selected_tab_image;
                    m_divisionsTabBg.sprite = m_regular_tab_image;

                    m_infoContent.SetActive(false);
                    m_rulesContent.SetActive(true);
                    m_divisionsContent.SetActive(false);
                    SetAllImagesNativeSize();


                    m_rulesTabBg.gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<TMP_Text>().fontMaterial = m_selected_tab_material;


                    break;
                case "divisions":
                    m_infoTabBg.sprite = m_regular_tab_image;
                    m_rulesTabBg.sprite = m_regular_tab_image;
                    m_divisionsTabBg.sprite = m_selected_tab_image;

                    m_infoContent.SetActive(false);
                    m_rulesContent.SetActive(false);
                    m_divisionsContent.SetActive(true);
                    SetAllImagesNativeSize();


                    m_divisionsTabBg.gameObject.transform.GetChild(0).transform.GetChild(0).gameObject.GetComponent<TMP_Text>().fontMaterial = m_selected_tab_material;

                    m_bronzeDivision.ClearRanks();
                    m_silverDivision.ClearRanks();
                    m_goldDivision.ClearRanks();
                    m_platinumDivision.ClearRanks();

                    foreach (KeyValuePair<string, string> infoData in ModelManager.Instance.Leaderboards.InfoTable)
                    {
                        switch (ModelManager.Instance.Leaderboards.GetColorFromGroup(infoData.Key))
                        {
                            case "BRONZE":
                                m_bronzeDivision.AddRank(infoData.Value);
                                break;
                            case "SILVER":
                                m_silverDivision.AddRank(infoData.Value);
                                break;
                            case "GOLD":
                                m_goldDivision.AddRank(infoData.Value);
                                break;
                            case "PLATINUM":
                                m_platinumDivision.AddRank(infoData.Value);
                                break;
                        }
                    }
                    break;
            }

            CardGamesSoundsController.Instance.TabsSFX();
        }

        private void SetAllImagesNativeSize()
        {
            m_infoTabBg.SetNativeSize();
            m_rulesTabBg.SetNativeSize();
            m_divisionsTabBg.SetNativeSize();
        }
    }

}