﻿namespace cardGames.views.popups
{
    public class LeaderboardInfoDivisionView : MonoBehaviour
    {
        [SerializeField] TMP_Text m_title = null;
        [SerializeField] Image m_shield = null;
        [SerializeField] TMP_Text[] m_levels = null;

        public void AddRank(string levels)
        {
            foreach (TMP_Text text in m_levels)
            {
                if (text.text == "SOON")
                {
                    text.text = "LVL " + levels;
                    break;
                }
            }
        }

        public void ClearRanks()
        {
            foreach (TMP_Text text in m_levels)
            {
                text.text = "SOON";
            }
        }
    }
}
