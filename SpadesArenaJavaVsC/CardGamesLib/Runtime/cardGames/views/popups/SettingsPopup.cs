﻿using System.Collections.Generic;
using cardGames.controllers;
using cardGames.models;
using common.apple;
using common.apple.views;
using common.controllers;
using common.facebook;
using common.facebook.views;
using common.models;
using common.utils;
using common.views;
using UnityEngine.UI;

namespace cardGames.views.popups
{

    public abstract class SettingsPopup : PopupBase
    {
        [SerializeField] protected List<Toggle> m_toggles = new List<Toggle>();
        [SerializeField] protected RectTransform m_BG_image = null;

        [Header("Settings controls")]
        [SerializeField] protected Slider m_sfx_slider = null;
        private SliderValueChangingEventPropagator sfxSliderDrag;
        [SerializeField] protected Slider m_music_slider = null;
        private SliderValueChangingEventPropagator musicSliderDrag;
        [SerializeField] protected Toggle m_notifications_toggle = null;
        [SerializeField] protected Toggle m_multi_toggle = null;
        [SerializeField] protected FacebookButton m_facebook_button = null;
        [SerializeField] protected AppleButton m_apple_button = null;

        [Header("Rows")]
        [SerializeField] protected GameObject m_game_center_row = null;
        [SerializeField] protected GameObject m_notification_row = null;
        [SerializeField] protected GameObject m_apple_row = null;

        [Header("Facebook")]
        [SerializeField] protected TMP_Text m_facebook_text = null;

        [Header("Apple")]
        [SerializeField] protected TMP_Text m_apple_text = null;

        [Header("Game center")]
        [SerializeField] protected Button m_game_center_button = null;
        [SerializeField] protected TMP_Text m_game_center_button_text = null;
        [SerializeField] protected GameObject m_game_center_loading_indication = null;
        [SerializeField] protected TMP_Text m_game_center_player_text = null;

        [SerializeField] protected GameObject m_qaEnvSelectionPanelView;

        bool m_disable_fb_connect_button = false;
        float m_sfx_old;
        float m_music_old;


        private const string FACEBOOK_CONNECT_TEXT_MSG = "Connect with Facebook";
        private const string FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG = "Connect with Facebook\n& get <color=#e9c063>{0}</color> free coins!";

        private const string APPLE_CONNECT_TEXT_MSG = "Connect with Apple Id";
        private const string APPLE_CONNECTED_TEXT_MSG = "Signed in with Apple";


        protected bool m_loginProcessActive = false; // A flag indicating that any login (facebook / apple / ?) is active

        public override void Start()
        {
            base.Start();

            if (m_sfx_slider != null)
            {
                sfxSliderDrag = m_sfx_slider.GetComponent<SliderValueChangingEventPropagator>();
                if (sfxSliderDrag != null)
                {
                    sfxSliderDrag.OnSliderPointerUp += PlaySliderValueChanged;
                }
            }
        }

        public override void Show(PopupManager manager)
        {

            base.Show(manager);
            ManagePicAndName();
            SetButtons();

#if UNITY_WEBGL
			m_notification_row.SetActive (false);
#endif

            if (m_disable_fb_connect_button)
            {
                m_facebook_button.Interactable = false;

                // Also disable Google play games if visible
                if (m_game_center_button != null)
                {
                    m_game_center_button.interactable = false;
                    ViewUtils.EnableDisableButtonText(m_game_center_button);
                }

                m_apple_button.Interactable = false;
            }


#if QA_MODE 
            m_qaEnvSelectionPanelView?.SetActive(true);
#else
            m_qaEnvSelectionPanelView?.SetActive(false);
#endif

        }



        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }

        protected void ManagePicAndName()
        {
            if (FacebookController.Instance.IsLoggedIn())
            {
                User user = ModelManager.Instance.GetUser();

                string user_id = FacebookController.Instance.UserFacebookData.User_id;


#if UNITY_WEBGL
				//m_facebook_button.GetComponent<RectTransform> ().localScale = new Vector3 (0.3f, 0.3f, 0.3f);
                //m_facebook_button.GetComponent<RectTransform> ().localPosition = new Vector3 (0f, -295f, 0f);
				m_facebook_button.gameObject.SetActive (false);

#endif
            }

            UpdateGameCenterButton();
            UpdateAppleSignIn();
        }

        private void SignInWithApple()
        {
            AppleController.Instance.SignInWithApple(SignInWithAppleFinished, true);
        }

        private void SignInWithAppleFinished(AppleData appleData)
        {
            if (appleData==null)
            {
                PopupManager.Instance.ShowMessagePopup("Unfortunately, Sign in with apple is not available on this device.");
                return;
            }

            if (appleData.SignedIn == true)
                m_apple_button.SetMode(AppleButton.ButtonMode.Connected);
            else
                m_apple_button.SetMode(AppleButton.ButtonMode.Waiting);

            UpdateAppleSignIn();
            ReLoginAfterAppleSignIn(appleData);

            m_loginProcessActive = false;
        }

        protected abstract void ReLoginAfterAppleSignIn(AppleData appleData);

        private void UpdateAppleSignIn()
        {

#if (!(UNITY_IOS || UNITY_TVOS))
            m_apple_row.SetActive(false);
#else
            m_apple_row.SetActive(true);

            if (AppleController.Instance.AppleData != null &&
                AppleController.Instance.AppleData.SignedIn)
            {
                m_apple_button.gameObject.GetComponent<Button>().interactable = false;
                m_apple_button.gameObject.GetComponent<Button>().gameObject.SetActive(false);

                m_apple_text.text = APPLE_CONNECTED_TEXT_MSG;
                string appleUsername = AppleController.Instance.AppleData.FirstName;
                if (!string.IsNullOrEmpty(appleUsername))
                    m_apple_text.text += ": "+ appleUsername;
            }
            else
            {
                m_apple_button.gameObject.GetComponent<Button>().gameObject.SetActive(true);
                m_apple_button.gameObject.GetComponent<Button>().interactable = true;
                m_apple_text.text = APPLE_CONNECT_TEXT_MSG;
            }
#endif
        }

        private void UpdateGameCenterButton()
        {
#if UNITY_ANDROID && !AMAZON_APP_STORE && false

            m_BG_image.sizeDelta = new Vector2(1200, 1070);
            m_game_center_row.SetActive(true);

            if (GameCentersManager.Instance.IsLoggedIn())
            {
                m_game_center_player_text.text = Social.localUser.userName;
                m_game_center_button_text.text = "Sign Out";
            }
            else
            {
                m_game_center_player_text.text = "Google Play Games";
                m_game_center_button_text.text = "Sign In";
            }

#else
            //m_BG_image.sizeDelta = new Vector2(1200, 820);  // NOT USED FOR NOW - IF NEEDED NEED TO EXTRACT THE SIZES
            m_game_center_row.SetActive(false);
#endif


        }

        public void Button_GameCenterClicked()
        {
            if (GameCentersManager.Instance.IsLoggedIn())
            {
                GameCentersManager.Instance.Logout();
                m_game_center_button_text.text = "Sign In";
                m_game_center_player_text.text = "Google Play Games";
            }
            else
            {
                m_game_center_loading_indication.SetActive(true);
                m_game_center_button_text.gameObject.SetActive(false);

                LoggerController.Instance.Log("SETTINGS POPUP Trying to login to Game Center...");


                GameCentersManager.Instance.Login((success) =>
                {
                    if (this == null)
                        return;

                    //need to remove the flag to not prompt to login - so future game login would result also in google game center login
                    CardGamesStateController.Instance.SetDontShowGameCenterLogin(true);

                    if (m_game_center_button_text.gameObject != null)
                    {
                        m_game_center_loading_indication.SetActive(false);
                        m_game_center_button_text.gameObject.SetActive(true);

                        if (success)
                        {
                            m_game_center_player_text.text = Social.localUser.userName;
                            m_game_center_button_text.text = "Sign Out";
                        }
                    }

                });
            }
        }

        protected void UpdateFBButton()
        {
            if (FacebookController.Instance.IsLoggedIn())
            {
                m_facebook_button.SetMode(FacebookButton.ButtonMode.SignOut);
                m_facebook_text.text = FormatUtils.FormatFacebookName(FacebookController.Instance.UserFacebookData);
            }
            else
            {
                m_facebook_button.SetMode(FacebookButton.ButtonMode.Connect);
                SetFacebookConnectTextMessage();
            }
        }

        private void SetFacebookConnectTextMessage()
        {
            FacebookBonus bonus = (FacebookBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.FacebookConnect);

            m_facebook_text.text = FACEBOOK_CONNECT_TEXT_MSG;

            if (bonus != null)
            {
                if (!bonus.IsEntitled)
                {
                    m_facebook_text.text = string.Format(FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG, FormatUtils.FormatBalance(bonus.CoinsSingle));
                    m_facebook_button.SetMode(FacebookButton.ButtonMode.Connect, true);
                }
            }
        }

        protected void SetButtons()
        {
            m_sfx_slider.value = StateController.Instance.GetSFXSetting();
            m_music_slider.value = StateController.Instance.GetMusicSetting();
            SoundsController.Instance.ChannelAdjustment(false, m_sfx_slider.value);
            SoundsController.Instance.ChannelAdjustment(true, m_music_slider.value);

            m_sfx_old = m_sfx_slider.value;
            m_music_old = m_music_slider.value;

            m_notifications_toggle.isOn = CardGamesStateController.Instance.GetNotificationSetting();

            bool temp = StateController.Instance.GetFakeMultiSetting();

            UpdateFBButton();
        }

        public void UpdateMusicSlider()
        {
            SoundsController.Instance.ChannelAdjustment(true, m_music_slider.value);
        }

        public void UpdateSFXSlider()
        {
            SoundsController.Instance.ChannelAdjustment(false, m_sfx_slider.value);
        }

        public void OnMuteSFXButton()
        {
            m_sfx_old = 0;
            StateController.Instance.SaveSFX(m_sfx_old);
        }

        public void OnMuteMusicButton()
        {
            m_music_old = 0;
            StateController.Instance.SaveMusic(m_music_old);
        }



        public void OnPointerUp()
        {

            if (m_sfx_slider.value != m_sfx_old)
            {
                m_sfx_old = m_sfx_slider.value;
                StateController.Instance.SaveSFX(m_sfx_old);
                CardGamesSoundsController.Instance.CollectibleUnlocked();
                Debug.Log("OnPointerUp! SFX saved: " + m_sfx_old);
            }

            if (m_music_slider.value != m_music_old)
            {
                m_music_old = m_music_slider.value;
                StateController.Instance.SaveMusic(m_music_old);
                Debug.Log("OnPointerUp! Music saved: " + m_music_old);
                SoundsController.Instance.PlayBGMusic();
            }
        }

        public virtual void NotificationsButtonClicked(bool isOn)
        {
            PlayToggleSound();

            if (!isOn)
                NotificationController.Instance.CancelPendingNotifications();

            StateController.Instance.SaveNotifications(isOn);
        }

        public virtual void QAServerButtonClicked(bool isOn)
        {
            //ManagerView.Instance.LocalFakeMutli = isOn;

            //StateController.Instance.SaveFakeMulti(isOn);
        }

        public virtual void RealMultiButtonClicked(bool isOn)
        {

        }

        public abstract void FacebookButtonClicked();

        public void AppleButtonClicked()
        {
            if (m_loginProcessActive) return;

            m_loginProcessActive = true;

            m_apple_button.SetMode(AppleButton.ButtonMode.Waiting);
            SignInWithApple();
        }

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }


        public bool Disable_fb_connect_button
        {
            set
            {
                m_disable_fb_connect_button = value;
            }
        }

        public void PlayToggleSound()
        {
            CardGamesSoundsController.Instance.ButtonToggle();
        }

        public void PlaySliderSetToMin()
        {
            CardGamesSoundsController.Instance.ButtonSetSliderClickedMin();
        }

        public void PlaySliderSetToMax()
        {
            CardGamesSoundsController.Instance.ButtonSetSliderClickedMax();
        }

        public void PlaySliderValueChanged()
        {
            //wait for the new value to be stored and applied before playing the sound with the new volume
            InvokingUtils.InvokeActionWithDelay(true, 1, CardGamesSoundsController.Instance.SliderValueChangedSFX, this);
        }


        public abstract void LoginAfterQAEnvChanged();
    }
}
