﻿using cardGames.controllers;
using common.ims.model;
using common.models;
using common.utils;
using common.views;
using cardGames.views.overlays;
using System;
using System.Collections.Generic;
using common.controllers;
using cardGames.models;

namespace cardGames.views.popups
{
    public class PurchasePopup : PopupBase
    {
        CometOverlay m_coins_comet;
        GameObject m_comet_trails_prefab;
        [SerializeField] GameObject m_start_coin_indication = null;
        [SerializeField] TMP_Text m_coins_text = null;
        [SerializeField] Button m_ok_button = null;

        [SerializeField] GameObject m_coinsPanel;
        [SerializeField] GameObject m_avatarPanel;
        [SerializeField] GameObject m_vouchersPanel;
        [SerializeField] TMP_Text m_voucherQuantity = null;
        [SerializeField] TMP_Text m_voucherValue = null;

        [SerializeField] int m_coinsSpriteIndex = 8; // THIS IS HERE SINCE IN RUMMY THE SPRITE INDEX IS DIFFERENT!

        ACManikinItem m_manikinItem;
        [SerializeField] ACManikinItem m_manikinTopItemPrefab;
        [SerializeField] ACManikinItem m_manikinMiddleItemPrefab;
        [SerializeField] ACManikinItem m_manikinBottomItemPrefab;
        [SerializeField] ACManikinItem m_manikinFullItemPrefab;
        Canvas m_canvas;

        int m_curr_coins_balance;
        int m_coins_value;


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            CardGamesSoundsController.Instance.PlayPurchaseSuccess();
        }

        public bool InitPurchase(int newCoinsBalance, IMSGoodsList goodsList, GameObject comet_trails_prefab, GameObject coins_box_target_object)
        {
            // "newCoinsBalance" not in use

            m_comet_trails_prefab = comet_trails_prefab;

            if (goodsList.Coins.Count > 0)
                m_coins_value = goodsList.CoinsValue;

            if (m_coins_value == 0)
                return false; // This is to prevent a duplicate purchase bug

            //m_balance_coins_text = LobbyTopController.Instance.Coins_text;

            ShowVouchers(goodsList.Vouchers);
            ShowInventoryItems(goodsList.InventoryItems);
            ShowCoins(); // Must be last


            m_ok_button.gameObject.SetActive(false);

            return true;

        }

        private void ShowCoins()
        {
            if (m_vouchersPanel == null)
                return;
            if (!m_vouchersPanel.activeSelf && !m_avatarPanel.activeSelf)
                m_coins_text.text = FormatUtils.FormatBalance(m_coins_value);
            else
                m_coins_text.text = FormatUtils.FormatPrice(m_coins_value);

        }

        private void ShowInventoryItems(List<string> inventoryItems)
        {
            m_avatarPanel.SetActive(false);

            if (m_avatarPanel == null)
                return;

            if (inventoryItems.Count == 0)
                return;

            m_avatarPanel.SetActive(true);

            List<MavatarItem> items = ModelManager.Instance.AvatarData.InventoryItems[inventoryItems[0]];
  
            List<GameObject> goItems = MAvatarController.Instance.GetListOfItemsByInventoryId(items, m_avatarPanel);
            if (goItems != null)
            {
                if (items[0].ItemType == MAvatarModel.ItemType.Accessory || items[0].ItemType == MAvatarModel.ItemType.Hair)
                    m_manikinItem = Instantiate(m_manikinTopItemPrefab.gameObject, m_avatarPanel.transform).GetComponent<ACManikinItem>();
                else
                    m_manikinItem = Instantiate(m_manikinBottomItemPrefab.gameObject, m_avatarPanel.transform).GetComponent<ACManikinItem>();

                // handeling only one avatar item (the first)
                m_manikinItem.SetData(items[0], ACCategorySection.SectionType.ManikinOwnedSection);
            }
        }
        private void ShowVouchers(List<VouchersGroup> vouchers)
        {
            if (vouchers?.Count > 0)
            {
                GetComponent<RectTransform>().sizeDelta = new Vector2(GetComponent<RectTransform>().rect.width, 785);
                // Currently a single voucher is supported
                m_voucherValue.text = FormatUtils.FormatBuyIn(vouchers[0].VouchersValue);
                m_voucherQuantity.text = "x" + FormatUtils.FormatBuyIn(vouchers[0].Count);
                if (m_vouchersPanel != null)
                    m_vouchersPanel.SetActive(true);
            }
            else
            {
                if (m_vouchersPanel != null)
                    m_vouchersPanel.SetActive(false);
            }
        }


        protected override void OnEnterAnimDone()
        {

            //PopupManager.Instance.GetComponent<CanvasGroup> ().blocksRaycasts = true;
            GenerateCoinsComet(m_coins_value, m_curr_coins_balance, m_start_coin_indication, 0f, () =>
            {

                m_ok_button?.gameObject.SetActive(true);

            });
        }

        public void SetDepth(int depth)
        {
            m_canvas = GetComponent<Canvas>();
            m_canvas.sortingOrder = depth;
        }

        private void GenerateCoinsComet(int prize_coins, int initial_coins, GameObject origin_object, float initial_delay = 0.5f, Action comets_done = null)
        {
            m_coins_comet = OverlaysManager.Instance.ShowCometOverlay();

            int num_coins_to_show = Mathf.Min(5, prize_coins);

            m_coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, origin_object, CardGamesPopupManager.Instance.Lobby_flight_target
               , 0f, 0.15f, 1.5f, CardGamesPopupManager.Instance.Lobby_coins_balance, prize_coins, CardGamesPopupManager.Instance.Lobby_coins_animator, 3, 1.0f, 0.5f
               , comets_done, new Vector2(0.2f, 0f), new Vector2(0.8f, 0.4f));
        }

        public void BT_button_clicked()
        {
            m_manager.HidePopup();
        }

    }
}