﻿using common.utils;
using System;


namespace cardGames.views.popups
{
    public class AltInsufficientFundsPopup : InsufficientFundsPopup
    {
        //slightly different implementation for Rummy. original implementation is still a concrete object in use in Spades.
        protected override void FillTexts(int delta)
        {
            m_missing_amount_text.text = "You need at least <sprite=0><font=\"Uni\" material=\"Uni_Gold 2\">" + FormatUtils.FormatBalance(delta) + "</font> more to play!";
            m_coin_amount_text.text = "<sprite=0>" + FormatUtils.FormatBalance(m_cashier_item.GoodsList.CoinsValue);
            //m_money_amount_text.text = "For " + m_cashier_item.Price;
            m_money_amount_text.text = String.Format(m_money_amount_text.text, m_cashier_item.Price);

            if (m_cashier_item.Bonus == 0)
            {
                m_bonus_container.SetActive(false);
            }
            else
            {
                m_bonus_amount_text.text = String.Format(m_bonus_amount_text.text, m_cashier_item.Bonus);
            }
        }
    }
}