using common.controllers;
using common.utils;
using common.views;
using System;

namespace cardGames.views.popups
{
    public class ArenaBuyWarningPopup : PopupBase
    {
        [SerializeField] TMP_Text m_text = null;

        private Action m_quit;
        private Action m_buy;

        public void SetData(int rebuy,Action onQuit,Action onBuy)
        {
            m_quit = onQuit;
            m_buy = onBuy;

            m_text.text = String.Format(m_text.text, FormatUtils.FormatBuyIn(rebuy));
        }
        public void OnQuitClick()
        {
            m_quit();
            PopupManager.Instance.HidePopup();
        }

        public void OnBuyClick()
        {
            m_buy();
            PopupManager.Instance.HidePopup();
        }
    }

}
