using common.models;
using common.utils;
using cardGames.models;

namespace cardGames.views
{

    public class LeaderboardItem : MonoBehaviour
    {

        [SerializeField] TMP_Text m_rank = null;
        [SerializeField] TMP_Text m_score = null;
        [SerializeField] TMP_Text m_playerName = null;

        //take this out of spades.views and move LeaderboardItem either really to common.views or cardGames.views
        [SerializeField] MAvatarView m_mavatarView = null;
        [SerializeField] GameObject m_avatarSilhouetteImage = null;

        [SerializeField] GameObject m_me_row_object = null;

        [SerializeField] GameObject m_place_icon_holder = null;
        [SerializeField] Image m_rank_up = null;
        [SerializeField] Image m_rank_down = null;
        [SerializeField] GameObject m_place_1_icon = null;
        [SerializeField] GameObject m_place_2_icon = null;
        [SerializeField] GameObject m_place_3_icon = null;

        [SerializeField] GameObject m_current = null;
        [SerializeField] GameObject m_prev = null;

        [SerializeField] Image m_bg_lighting_current = null;
        [SerializeField] Image m_bg_lighting_previous = null;

        [SerializeField] Sprite m_bg_me_current = null;
        [SerializeField] Sprite m_bg_me_previous = null;

        [SerializeField] ParticleSystem m_effect_current = null;
        [SerializeField] ParticleSystem m_effect_previous = null;


        private long m_userId;
        private LeaderboardItemModel m_itmModel;
        private bool m_isMe = false;
        private LeaderboardsModel.LeaderboardType m_leaderboardType;
        private GameObject prefab_to_delete;

        public void Init(LeaderboardItemModel itmModel, LeaderboardsModel.LeaderboardType type, bool isMe = false)
        {
            GameObject m_place_1_inst;
            GameObject m_place_2_inst;
            GameObject m_place_3_inst;

            m_itmModel = itmModel;
            m_userId = itmModel.UserId;
            m_isMe = isMe;
            m_leaderboardType = type;
            if (isMe)
            {
                if (m_leaderboardType == LeaderboardsModel.LeaderboardType.GlobalCurrent)
                {
                    m_me_row_object.GetComponent<Image>().sprite = m_bg_me_current;
                    m_current.SetActive(true);
                    m_prev.SetActive(false);
                    m_effect_current.GetComponent<Renderer>().sortingOrder = 36;
                }
                else
                {
                    m_me_row_object.GetComponent<Image>().sprite = m_bg_me_previous;
                    m_current.SetActive(false);
                    m_prev.SetActive(true);
                    m_effect_previous.GetComponent<Renderer>().sortingOrder = 36;
                }
            }
            else
            {
                m_me_row_object.SetActive(false);
                m_current.SetActive(false);
                m_prev.SetActive(false);
            }



            m_rank.text = itmModel.Rank.ToString();
            m_score.text = FormatUtils.FormatBalance(itmModel.Score);

            m_playerName.text = itmModel.MAvatarModel.NickName;

            TMPBidiHelper.MakeRTL(m_playerName);

            if (itmModel.IsMe)
            {
                ToggleAvatarSilhouetteActiveState(false);
                m_mavatarView.SetAvatarModel(ModelManager.Instance.GetUser().GetMAvatar());
                PrefabUtils.DisableAnimations(m_mavatarView.transform);

                m_rank.color = Color.white;
            }
            else
            {
                if (itmModel.ShouldDisplaySilhouette)
                {
                    ToggleAvatarSilhouetteActiveState(true);
                }
                else
                {
                    ToggleAvatarSilhouetteActiveState(false);
                    m_mavatarView.SetAvatarModel(itmModel.MAvatarModel);
                    PrefabUtils.DisableAnimations(m_mavatarView.transform);

                }
            }

            m_place_icon_holder.SetActive(true);

            if (m_leaderboardType == LeaderboardsModel.LeaderboardType.GlobalCurrent)
            {
                if (itmModel.RankState == LeaderboardModel.RankState.UP)
                {
                    m_rank_up.gameObject.SetActive(true);
                    m_rank_down.gameObject.SetActive(false);

                }
                else if (itmModel.RankState == LeaderboardModel.RankState.DOWN && itmModel.Rank > 1)
                {
                    m_rank_up.gameObject.SetActive(false);
                    m_rank_down.gameObject.SetActive(true);
                }
                else
                {
                    m_rank_up.gameObject.SetActive(false);
                    m_rank_down.gameObject.SetActive(false);
                }

                if (m_bg_lighting_current != null)
                    m_bg_lighting_current.enabled = true;

                if (m_bg_lighting_previous != null)
                    m_bg_lighting_previous.enabled = false;
            }
            else
            {
                m_rank_up.gameObject.SetActive(false);
                m_rank_down.gameObject.SetActive(false);

                if (m_bg_lighting_current != null)
                    m_bg_lighting_current.enabled = false;

                if (m_bg_lighting_previous != null)
                    m_bg_lighting_previous.enabled = true;
            }

            if (prefab_to_delete != null)
            {
                Destroy(prefab_to_delete);
                prefab_to_delete = null;
            }
            switch (itmModel.Rank)
            {
                case 1:
                    m_place_1_inst = (GameObject)Instantiate(m_place_1_icon);
                    m_place_1_inst.transform.SetParent(m_place_icon_holder.transform);
                    m_place_1_inst.transform.localPosition = new Vector3();
                    m_place_1_inst.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
                    m_rank.enabled = false;
                    prefab_to_delete = m_place_1_inst;
                    break;

                case 2:
                    m_place_2_inst = (GameObject)Instantiate(m_place_2_icon);
                    m_place_2_inst.transform.SetParent(m_place_icon_holder.transform);
                    m_place_2_inst.transform.localPosition = new Vector3();
                    m_place_2_inst.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
                    m_rank.enabled = false;
                    prefab_to_delete = m_place_2_inst;
                    break;

                case 3:
                    m_place_3_inst = (GameObject)Instantiate(m_place_3_icon);
                    m_place_3_inst.transform.SetParent(m_place_icon_holder.transform);
                    m_place_3_inst.transform.localPosition = new Vector3();
                    m_place_3_inst.transform.localScale = new Vector3(1.1f, 1.1f, 1f);
                    m_rank.enabled = false;
                    prefab_to_delete = m_place_3_inst;
                    break;

                default:
                    m_place_icon_holder.SetActive(false);
                    m_rank.enabled = true;
                    break;
            }

            m_me_row_object.GetComponent<Button>().onClick.RemoveAllListeners();


        }

        private void ToggleAvatarSilhouetteActiveState(bool isSilhouetteActive)
        {
            m_mavatarView.gameObject.SetActive(!isSilhouetteActive);
            m_avatarSilhouetteImage.SetActive(isSilhouetteActive);
        }

        public void setMeRowBg(bool isCurrent)
        {
            if (isCurrent)
            {
                m_me_row_object.GetComponent<Image>().sprite = m_bg_me_current;
                m_current.SetActive(true);
                m_prev.SetActive(false);
            }
            else
            {
                m_me_row_object.GetComponent<Image>().sprite = m_bg_me_previous;
                m_current.SetActive(false);
                m_prev.SetActive(true);
            }

            m_me_row_object.SetActive(true);

        }
    }
}
