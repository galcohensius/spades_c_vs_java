﻿using System;

namespace cardGames.views
{
    public class TextBubbleView : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text m_SpeechBubbleText = null;

        [SerializeField]
        private Transform m_bubbleTransform = null;

        private Action m_onChatBubbleClosed = null;
        private Action m_onChatBubbleOpened = null;

        private Vector2 m_lastBubbleSize = Vector2.zero;

        public void AddChatBubbleCloseListener(Action onChatBubbleClosed)
        {
            m_onChatBubbleClosed = onChatBubbleClosed;
        }

        public void RemoveChatBubbleCloseListener()
        {
            m_onChatBubbleClosed = null;
        }

        public void AddChatBubbleOpenListener(Action onChatBubbleOpened)
        {
            m_onChatBubbleOpened = onChatBubbleOpened;
        }

        public void RemoveChatBubbleOpenListener()
        {
            m_onChatBubbleOpened = null;
        }

        public void InvokeChatBubbleClosed()
        {
            ResetBubbleSize();

            if (m_onChatBubbleClosed != null)
            {
                m_onChatBubbleClosed.Invoke();
            }
        }

        public void InvokeChatBubbleOpened()
        {
            if (m_onChatBubbleOpened != null)
            {
                m_onChatBubbleOpened.Invoke();
            }
        }

        public void SetMessageData(string messageText, Vector2 messageSize)
        {
            m_lastBubbleSize = m_bubbleTransform.GetComponent<RectTransform>().sizeDelta;
            m_bubbleTransform.GetComponent<RectTransform>().sizeDelta = messageSize;
            SetMessageText(messageText);
        }

        public void SetMessageText(string messageText)
        {
            m_SpeechBubbleText.text = messageText;
        }

        private void ResetBubbleSize()
        {
            if (!m_lastBubbleSize.Equals(Vector2.zero))
            {
                m_bubbleTransform.GetComponent<RectTransform>().sizeDelta = m_lastBubbleSize;
            }
        }
    }
}