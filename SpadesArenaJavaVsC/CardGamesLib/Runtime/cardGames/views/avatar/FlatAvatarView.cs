﻿using cardGames.controllers;
using cardGames.models;

namespace cardGames.views
{
    public class FlatAvatarView : MAvatarView
    {
        [SerializeField]
        private GameObject m_avatarDisplay;
        [SerializeField]
        private GameObject m_fbDisplay;
        [SerializeField]
        private Image m_avatarImage;

        protected override void UpdateViewFromModel(MAvatarModel avatarData, bool fullChange = false)
        {
            if (avatarData.Mode == MAvatarModel.MAvatarMode.AvatarFlat || avatarData.Mode == MAvatarModel.MAvatarMode.Avatar || avatarData.Mode == MAvatarModel.MAvatarMode.Guest)
            {
                m_avatarImage.sprite = FlatAvatarController.Instance.GetAvatar(avatarData);
                m_avatarDisplay.SetActive(true);
                m_fbDisplay.SetActive(false);

            }
            else if (avatarData.Mode == MAvatarModel.MAvatarMode.Facebook || avatarData.Mode == MAvatarModel.MAvatarMode.FacebookAndAvatar)
            {
                m_avatarDisplay.SetActive(false);
                m_fbDisplay.SetActive(true);

                SetFacebookImage(avatarData, m_facebookImageLarge);
                m_facebookDisplayLarge.SetActive(true);
            }
        }

        protected override void RestartAnimator()
        {
            return;
        }
    }
}