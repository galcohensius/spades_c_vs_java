﻿using common.controllers;
using System.Collections.Generic;

namespace cardGames.views
{
    public abstract class LeftControlPanelView : MonoBehaviour
    {
        [SerializeField]
        protected GameObject m_bodyPanel;

        [SerializeField]
        protected List<GameObject> m_bodyPanelButtonsList = null;

        [SerializeField]
        protected GameObject m_inventoryPanel;

        [SerializeField]
        protected List<GameObject> m_inventoryPanelButtonsList = null;

        [SerializeField]
        protected GameObject m_storePanel;

        [SerializeField]
        protected List<GameObject> m_storePanelButtonsList = null;

        [SerializeField]
        protected List<GameObject> m_tabButtonsList = null;

        [SerializeField]
        protected Sprite m_leftControlPanelButtonOnSprite;

        [SerializeField]
        protected Sprite m_leftControlPanelButtonOffSprite;

        

        protected LeftPanelType m_currentOpenLeftPanelType;
        protected int m_activeLeftControlPanelButtonIndex;

        void OnEnable()
        {
            //SetBodyPanel();
        }

        protected abstract void InitTabButtons(int activeTabButtonIndex);

        public void SetBodyPanel()
        {
            SetLeftPanelByType(LeftPanelType.Body);
            InitTabButtons(0);
            InitLeftControlPanelButtons(4);
        }

        public void SetInventoryPanel()
        {
            SetLeftPanelByType(LeftPanelType.Inventory);
            InitTabButtons(1);
            InitLeftControlPanelButtons(0);
        }

        public void SetStorePanel()
        {
            SetLeftPanelByType(LeftPanelType.Store);
            InitTabButtons(2);
        }

        protected void SetLeftPanelByType(LeftPanelType leftPanelType)
        {
            m_currentOpenLeftPanelType = leftPanelType;

            InitPanelsActiveStateFalse();

            switch (leftPanelType)
            {
                case LeftPanelType.Body:
                    m_bodyPanel.SetActive(true);
                    break;

                case LeftPanelType.Inventory:
                    m_inventoryPanel.SetActive(true);
                    break;

                case LeftPanelType.Store:
                    m_storePanel.SetActive(true);
                    break;
            }
        }

        protected void InitPanelsActiveStateFalse()
        {
            m_bodyPanel.SetActive(false);
            m_inventoryPanel.SetActive(false);
            m_storePanel.SetActive(false);
        }

        public void SetActiveButton(int activeLeftControlPanelButtonIndex)
        {
            if (IsPanelsScrolling)
                return;

            InitLeftControlPanelButtons(activeLeftControlPanelButtonIndex);
        }

        protected void InitLeftControlPanelButtons(int activeLeftControlPanelButtonIndex)
        {
            m_activeLeftControlPanelButtonIndex = activeLeftControlPanelButtonIndex;

            switch (CurrentOpenLeftPanelType)
            {
                case LeftPanelType.Body:
                    InitButtonsForPanel(activeLeftControlPanelButtonIndex, m_bodyPanelButtonsList);
                    break;

                case LeftPanelType.Inventory:
                    InitButtonsForPanel(activeLeftControlPanelButtonIndex, m_inventoryPanelButtonsList);
                    break;

                case LeftPanelType.Store:
                    InitButtonsForPanel(activeLeftControlPanelButtonIndex, m_storePanelButtonsList);
                    break;
            }
        }

        protected void InitButtonsForPanel(int activeLeftControlPanelButtonIndex, List<GameObject> currentPanelButtonsList)
        {
            foreach (GameObject button in currentPanelButtonsList)
            {
                button.GetComponent<Image>().sprite = m_leftControlPanelButtonOffSprite;
            }

            if (currentPanelButtonsList[activeLeftControlPanelButtonIndex] != null)
            {
                currentPanelButtonsList[activeLeftControlPanelButtonIndex].GetComponent<Image>().sprite = m_leftControlPanelButtonOnSprite;
            }
            else
            {
                LoggerController.Instance.LogError("Missing LeftControlPanel Button / Link Inspector");
            }
        }

        public LeftPanelType CurrentOpenLeftPanelType
        {
            get
            {
                return m_currentOpenLeftPanelType;
            }
        }

        public int ActiveLeftControlPanelButtonIndex
        {
            get
            {
                return m_activeLeftControlPanelButtonIndex;
            }
        }


        public bool IsPanelsScrolling { get; set; }
    }


    public enum LeftPanelType
    {
        Body,
        Inventory,
        Store
    }
}