﻿namespace cardGames.views
{
    // This is the new class for chat bubble view
    public class ChatBubbleView : MonoBehaviour
    {
        [SerializeField] TMP_Text m_message;
        [SerializeField] RectTransform m_bubbleImage;

        public void SetData(string message, MAvatarView.MessageDirection messageDirection, int overrideOrder = -1000)
        {
            m_message.text = message;
            switch (messageDirection)
            {
                case MAvatarView.MessageDirection.North:
                    m_bubbleImage.Rotate(new Vector3(-180, 0, 0));
                    break;

                case MAvatarView.MessageDirection.South:
                    m_bubbleImage.Rotate(new Vector3(0, 0, 0));
                    break;

                case MAvatarView.MessageDirection.East:
                    m_bubbleImage.Rotate(new Vector3(0, 180, 0));
                    break;

                case MAvatarView.MessageDirection.West:
                    m_bubbleImage.Rotate(new Vector3(0, 0, 0));
                    break;
            }

            if (overrideOrder != -1000)
            {
                Canvas overrideCanvas = this.gameObject.GetComponent<Canvas>();
                overrideCanvas.overrideSorting = true;
                overrideCanvas.sortingOrder = overrideOrder;
            }
        }

        public TMP_Text Message { get => m_message; set => m_message = value; }
        public RectTransform Bubble { get => m_bubbleImage; set => m_bubbleImage = value; }
    }
}
