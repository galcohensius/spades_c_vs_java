﻿namespace cardGames.views
{
    public class PromoView : MonoBehaviour
    {

        [SerializeField] Image m_bg_image;
        [SerializeField] Text m_text;


        public void Init()
        {
            m_bg_image = transform.FindDeepChild("Image").GetComponent<Image>();
            m_text = transform.FindDeepChild("Text").GetComponent<Text>();
        }

        public Image Bg_image
        {
            get
            {
                return m_bg_image;
            }
        }

        public Text Text
        {
            get
            {
                return m_text;
            }
        }
    }
}
