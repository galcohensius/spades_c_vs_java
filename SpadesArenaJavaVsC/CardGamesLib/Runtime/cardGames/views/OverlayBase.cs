﻿using common.controllers;

namespace cardGames.views
{

    public class OverlayBase : MonoBehaviour
    {

        protected OverlaysManager m_manager;

        OverlayClosedDelegate m_close_delegate;

        public virtual void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            AndroidBackButtonListener.Instance.BackButtonClicked += OnBackButtonClicked;
            m_manager = manager;
            m_close_delegate = close_delegate;

        }

        private void OnDisable()
        {
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
        }

        public virtual void OnBackButtonClicked()
        {
            // Do nothing by default
        }

        public virtual void Close()
        {
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
            if (m_close_delegate != null)
                m_close_delegate();

            Destroy(this.gameObject);
        }

    }
}