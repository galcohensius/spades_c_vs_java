﻿using System;
using common.utils;

namespace cardGames.views
{
    public class NameEditToolTipView : MonoBehaviour
    {
        [SerializeField]
        private InputField m_inputText = null;

        [SerializeField]
        private TMP_Text m_errorText = null;

        [SerializeField]
        private Button m_acceptButton = null;

        private string m_last_name_selected;

        //public delegate void OnAcceptClicked();
        //private event OnAcceptClicked OnAcceptClickedEvent;

        private Action<string, bool> OnChoiceClicked = null;

        public void Init(string name, Action<string, bool> onChoiceClicked)
        {
            gameObject.SetActive(true);
            m_errorText.gameObject.SetActive(false);

            OnChoiceClicked = onChoiceClicked;
            m_last_name_selected = name;
            m_inputText.text = m_last_name_selected;
            //TMPBidiHelper.MakeRTL(m_inputText);
        }

        public string GetName()
        {
            return m_last_name_selected;
        }

        public void NameTextBoxValueChanged(string text)
        {
            if (ValidateNameLength())
            {
                //TMPBidiHelper.MakeRTL(m_inputText);
            }
        }

        public void NameTextBoxEndEdit(string text)
        {
            if (ValidateNameLength())
            {
                m_inputText.DeactivateInputField();
            }
        }

        public void AcceptClicked()
        {
            m_last_name_selected = m_inputText.text.Trim();
            OnChoiceClicked(m_last_name_selected, true);
            gameObject.SetActive(false);
        }

        public void CancelClicked()
        {
            OnChoiceClicked(m_last_name_selected, false);
            gameObject.SetActive(false);
        }

        public void OnTextBoxSelect()
        {
            ValidateNameLength();
        }

        public void EditPenClicked()
        {
            m_inputText.ActivateInputField();
        }

        private bool ValidateNameLength()
        {
            string name = m_inputText.text.Trim();

            if (name.Length < 3 || name.Length > 12)
            {

                // Show error
                m_errorText.gameObject.SetActive(true);
                m_acceptButton.interactable = false;
                ViewUtils.EnableDisableButtonText(m_acceptButton);

                return false;
            }
            else
            {
                // Hide error
                m_errorText.gameObject.SetActive(false);
                m_acceptButton.interactable = true;
                ViewUtils.EnableDisableButtonText(m_acceptButton);
                return true;
            }
        }
    }
}