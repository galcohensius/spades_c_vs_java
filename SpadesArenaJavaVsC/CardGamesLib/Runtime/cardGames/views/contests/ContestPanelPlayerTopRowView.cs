﻿using cardGames.models.contests;
using common.utils;

namespace cardGames.views
{
    public class ContestPanelPlayerTopRowView : MonoBehaviour
    {
        bool is_nickname_shown = false;
        [SerializeField] GameObject m_points_trophy;
        [SerializeField] TMP_Text m_nickname;
        [SerializeField] TMP_Text m_rank;
        [SerializeField] TMP_Text m_points;
        [SerializeField] GameObject m_gold_trophy;
        [SerializeField] GameObject m_silver_trophy;
        [SerializeField] GameObject m_bronze_trophy;
        [SerializeField] MAvatarView m_mavatar_view;
        [SerializeField] Image m_me_row;
        [SerializeField] Image m_bg;

        private enum Place { first, second, third, fourth, fifth, sixth };
        [SerializeField] Place m_place;

        public void SetData(ContestPlayer player)
        {
            if (player.IsMe)
                m_me_row.gameObject.SetActive(true);
            else
                m_me_row.gameObject.SetActive(false);

            m_nickname.text = player.AvatarData.NickName;
            TMPBidiHelper.MakeRTL(m_nickname);

            string movement;
            if (player.Rank_movement == ContestPlayer.RankMovement.Down)
                movement = "<sprite=1>";
            else if (player.Rank_movement == ContestPlayer.RankMovement.Up)
                movement = "<sprite=0>";
            else
                movement = "";

            m_rank.text = player.Rank + movement;

            m_points.text = FormatUtils.FormatBalance(player.Points);
            m_mavatar_view.SetAvatarModel(player.AvatarData);
            PrefabUtils.DisableAnimations(m_mavatar_view.transform);

            switch (m_place)
            {
                case Place.first:
                    m_gold_trophy.SetActive(true);
                    break;
                case Place.second:
                    m_silver_trophy.SetActive(true);
                    break;
                case Place.third:
                    m_bronze_trophy.SetActive(true);
                    break;
            }

            CancelInvoke("ReplaceNicknameAndPoints");
            is_nickname_shown = false; // Reset the flag so all players will switch the same
            InvokeRepeating("ReplaceNicknameAndPoints", 2.0f, 3f);
        }

        private void ReplaceNicknameAndPoints()
        {
            is_nickname_shown = !is_nickname_shown;
            if (is_nickname_shown)
            {
                m_points_trophy.SetActive(false);
                m_points.gameObject.SetActive(false);
                m_nickname.gameObject.SetActive(true);
            }
            else
            {
                m_points_trophy.SetActive(true);
                m_points.gameObject.SetActive(true);
                m_nickname.gameObject.SetActive(false);
            }

        }
    }
}
