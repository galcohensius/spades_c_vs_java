﻿using cardGames.controllers;
using cardGames.models;
using cardGames.models.contests;
using cardGames.views;
using common.controllers;
using System.Collections;
using System.Collections.Generic;

public class ContestPanelView : MonoBehaviour
{
    #region SerializeField Members
    [SerializeField] ContestPanelIntroPageView m_intro_page;
    [SerializeField] ContestPanelInfoPageView m_info_page;
    [SerializeField] ContestPanelPaytableView m_paytable_page;
    [SerializeField] ContestPanelTop3PageView m_top3_page;
    [SerializeField] ContestPanelRankView m_rank_page;
    [SerializeField] ContestJackPotView m_pool_prize;
    [SerializeField] ContestTimerView m_contestTimerView;
    [SerializeField] ContestPanelOpenButtonView cpobv;
    [SerializeField] ContestPanelTooltipPointsView m_contest_panel_tooltip_points_view;
    [SerializeField] ContestPanelOpenButtonView m_contest_panel_open_button_view;

    [SerializeField] private GameObject m_open_button;
    [SerializeField] Transform m_in_screen_place_holder;
    [SerializeField] Transform m_in_screen_for_button_place_holder;
    [SerializeField] Transform m_hide_place_holder;

    [SerializeField] GameObject m_player_with_tooltip_in_game;
    [SerializeField] GameObject m_contest_tooltip;
    #endregion

    #region Public Members
    public enum Pages
    {
        Intro,
        Info,
        Paytable,
        Rank,
        Top3,
        Entering
    };
    public enum SoundName
    {
        Bounce,
        GrowingNumbers,
        SnapToPosition,
        SmallTrail,
        SmallWin,
        BigWin,
        Enter,
        BounceX3,
        ShootUp,
        SwitchPlaces,
        Slide,
        TooltipAppear,
        Button_2
    };
    #endregion

    #region Private Members
    private Animator m_anim;
    private Contest m_contest;
    private ActiveContest m_activeContest;
    private Pages m_pages;
    private SoundName m_soundName;
    private bool m_is_panel_shown;
    private bool m_is_tooltip_panel_ever_opened = false;
    private bool m_duringGame;
    private bool m_entered = false;
    private bool m_entering_the_contest = false;
    private bool m_started_with_2_players;
    #endregion

    private void Awake()
    {
        m_anim = GetComponent<Animator>();
    }

    // "Player Tooltip"
    public void Trigger_ResetTooltips()
    {
        m_player_with_tooltip_in_game.gameObject.SetActive(false);
        m_contest_tooltip.gameObject.SetActive(true);
    }

    public void Trigger_PlayerWithTooltipOn()
    {
        m_player_with_tooltip_in_game.gameObject.SetActive(true);
        m_contest_tooltip.gameObject.SetActive(false);
    }

    public void Trigger_Tooltip()
    {
        m_contest_tooltip.gameObject.SetActive(true);
    }

    // "Player Tooltip close"
    public void Trigger_ResetTooltipsClose()
    {
        m_player_with_tooltip_in_game.gameObject.SetActive(true);
        m_contest_tooltip.gameObject.SetActive(true);
    }

    public void Trigger_TooltipClose()
    {
        m_contest_tooltip.gameObject.SetActive(false);
    }

    public void Trigger_InGameAndTooltipClose()
    {
        m_player_with_tooltip_in_game.gameObject.SetActive(false);
        m_contest_tooltip.gameObject.SetActive(true);
    }

    public void SetData(Contest contest, bool duringGame)
    {
        m_contest = contest;

        if (contest == null)
            return;

        m_anim.Play("Reset_Animations");

        m_activeContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);

        m_is_tooltip_panel_ever_opened = duringGame;
        m_duringGame = duringGame;

        InitButtons();

        m_contestTimerView.SetData(m_contest);
        m_pool_prize.SetData(m_contest);

        m_info_page.SetData(m_contest);
        m_intro_page.SetData(m_contest);
        m_paytable_page.SetData(m_contest);
        m_top3_page.SetData(m_contest);
        m_rank_page.SetData(m_contest, m_duringGame);
        m_contest_panel_open_button_view.SetData(m_contest);
    }



    private void InitButtons()
    {

        m_intro_page.SetButtons(InfoClicked, OpenPaytableClicked);
        // if not in contest yet
        if (m_contest.Contest_players_mini_leaderboard_rank.Count == 0)
        {
            m_paytable_page.SetButtons(IntroClicked);
            m_info_page.SetButtons(IntroClicked);
        }
        else
        {
            if (m_activeContest.Current_playar_in_contest != null)
            {

                if (m_activeContest.Current_playar_in_contest.Rank <= 3)
                {
                    m_paytable_page.SetButtons(TOP3Clicked);
                    m_info_page.SetButtons(TOP3Clicked);
                    m_top3_page.SetButtons(InfoClicked, OpenPaytableClicked);
                }
                else
                {
                    m_paytable_page.SetButtons(RankClicked);
                    m_info_page.SetButtons(RankClicked);
                    m_top3_page.SetButtons(RankClicked);
                }
            }
        }

        m_rank_page.SetButtons(TOP3Clicked, InfoClicked, OpenPaytableClicked);
    }

    // This function is being called from the inspector
    public void Trigger_Play_Sound(string soundName)
    {
        switch (soundName)
        {
            case "BigWin":
                CardGamesSoundsController.Instance.Trigger_BigWing();
                break;
            case "Enter":
                CardGamesSoundsController.Instance.Trigger_Enter();
                break;
            case "Bounce":
                CardGamesSoundsController.Instance.Trigger_Bounce();
                break;
            case "BounceX3":
                CardGamesSoundsController.Instance.Trigger_BounceX3();
                break;
            case "NumberGrowing":
                CardGamesSoundsController.Instance.Trigger_NumberGrowing();
                break;
            case "ShootUp":
                CardGamesSoundsController.Instance.Trigger_ShootUp();
                break;
            case "Slide":
                CardGamesSoundsController.Instance.Trigger_Slide();
                break;
            case "SmallTrail":
                CardGamesSoundsController.Instance.Trigger_SmallTrail();
                break;
            case "SmallWin":
                CardGamesSoundsController.Instance.Trigger_SmallWin();
                break;
            case "SnapToPosition":
                CardGamesSoundsController.Instance.Trigger_SnapToPosition();
                break;
            case "SwitchPlaces":
                CardGamesSoundsController.Instance.Trigger_SwitchPlaces();
                break;
            case "TooltipAppear":
                CardGamesSoundsController.Instance.Trigger_TooltipAppear();
                break;
            case "Button_2":
                CardGamesSoundsController.Instance.ButtonToggle();
                break;
        }
    }

    // this should be in use instead of the string comparison ^^
    public void Trigger_Play_Sound(SoundName soundName)
    {
        switch (soundName)
        {
            case SoundName.BigWin:
                CardGamesSoundsController.Instance.Trigger_BigWing();
                break;
            case SoundName.Enter:
                CardGamesSoundsController.Instance.Trigger_Enter();
                break;
            case SoundName.Bounce:
                CardGamesSoundsController.Instance.Trigger_Bounce();
                break;
            case SoundName.BounceX3:
                CardGamesSoundsController.Instance.Trigger_BounceX3();
                break;
            case SoundName.GrowingNumbers:
                CardGamesSoundsController.Instance.Trigger_NumberGrowing();
                break;
            case SoundName.ShootUp:
                CardGamesSoundsController.Instance.Trigger_ShootUp();
                break;
            case SoundName.Slide:
                CardGamesSoundsController.Instance.Trigger_Slide();
                break;
            case SoundName.SmallTrail:
                CardGamesSoundsController.Instance.Trigger_SmallTrail();
                break;
            case SoundName.SmallWin:
                CardGamesSoundsController.Instance.Trigger_SmallWin();
                break;
            case SoundName.SnapToPosition:
                CardGamesSoundsController.Instance.Trigger_SnapToPosition();
                break;
            case SoundName.SwitchPlaces:
                CardGamesSoundsController.Instance.Trigger_SwitchPlaces();
                break;
            case SoundName.TooltipAppear:
                CardGamesSoundsController.Instance.Trigger_TooltipAppear();
                break;
            case SoundName.Button_2:
                CardGamesSoundsController.Instance.ButtonToggle();
                break;
        }
    }




    private void ShowPage(Pages page)
    {
        HideAllPages();
        m_pages = page;

        switch (page)
        {
            case Pages.Intro:
                m_intro_page.gameObject.SetActive(true);
                m_anim.Play("Before_First_Game");
                break;

            case Pages.Entering:
                m_intro_page.gameObject.SetActive(true);
                m_rank_page.gameObject.SetActive(true);
                break;

            case Pages.Paytable:
                m_paytable_page.gameObject.SetActive(true);
                break;

            case Pages.Top3:
                m_top3_page.gameObject.SetActive(true);
                break;

            case Pages.Rank:
                m_rank_page.gameObject.SetActive(true);
                break;

            case Pages.Info:
                m_info_page.gameObject.SetActive(true);
                break;
        }
    }
    /// <summary>
    /// Enables the contest panel
    /// </summary>
    public void BT_ShowContestPanel()
    {
        SetOpenPanel(true);
    }

    public void BT_HideContestPanel()
    {
        SetOpenPanel(false);
    }

    public void SetOpenPanel(bool visible)
    {
        if (m_contest == null)
            return;
        m_rank_page.ResetAnimations();

        m_is_panel_shown = visible;
        if (visible)
        {
            iTween.MoveTo(gameObject, iTween.Hash("x", m_in_screen_place_holder.position.x, "islocal", false,
                "easeType", iTween.EaseType.easeOutQuad, "time", .3f, "oncomplete", "OnPanelEnterAnimComplete", "Oncompletetarget", this.gameObject));
            iTween.MoveTo(m_open_button, iTween.Hash("x", m_hide_place_holder.position.x, "islocal", false, "easeType", iTween.EaseType.easeInQuad, "time", .3f));
            SelectPageToShow();
        }
        else
        {
            iTween.MoveTo(gameObject, iTween.Hash("x", m_hide_place_holder.position.x, "islocal", false, "easeType", iTween.EaseType.easeInQuad, "time", .3f));
            iTween.MoveTo(m_open_button, iTween.Hash("x", m_in_screen_for_button_place_holder.position.x, "islocal", false, "easeType", iTween.EaseType.easeOutQuad, "time", .3f));
        }

    }

    private void OnPanelEnterAnimComplete()
    {
        ShowTooltip();
    }

    public void StopTweensAndExitPanel()
    {

        gameObject.GetComponent<RectTransform>().position = new Vector3(m_hide_place_holder.position.x, gameObject.GetComponent<RectTransform>().position.y, gameObject.GetComponent<RectTransform>().position.z);
        m_open_button.GetComponent<RectTransform>().position = new Vector3(m_in_screen_for_button_place_holder.position.x, m_open_button.GetComponent<RectTransform>().position.y, m_open_button.GetComponent<RectTransform>().position.z);
        m_is_panel_shown = false;
    }

    private void SelectPageToShow()
    {
        m_contest_panel_tooltip_points_view.ResetTooltips();
        m_anim.enabled = true;

        //not in the contest yet
        if (m_contest.Contest_players_mini_leaderboard_rank.Count == 0)
        {
            if (m_duringGame)
                ShowPage(Pages.Intro);
            else
                ShowPage(Pages.Rank);
        }
        // in the contest
        else
        {
            //in end game
            if (!m_duringGame)
            {
                if (m_activeContest.StartGameRank == 0)
                {
                    if (!m_entered)
                    {
                        m_entered = true;
                        ShowPage(Pages.Entering);
                    }
                    else
                        ShowPage(Pages.Rank);
                }
                else
                    ShowPage(Pages.Rank);
            }
            else
            {
                // show rank in all cases except when the panel is opening in the game
                if (m_activeContest.Current_playar_in_contest.Rank <= 3)
                    ShowPage(Pages.Top3);
                else
                {
                    ShowPage(Pages.Rank);
                    // Disabled the animator since we need to hide the third row of the minileaderboard by code
                    // The animator has a keyframe that prevent this from happening if enabled
                    m_anim.enabled = false;
                }
            }
        }
    }



    public IEnumerator OpenAndClosePanel()
    {
        SetOpenPanel(true);
        yield return new WaitForSecondsRealtime(4);
        SetOpenPanel(false);
    }

    private void ShowTooltip()
    {
        //  m_rank_page.ResetTooltip();
        if (m_is_tooltip_panel_ever_opened)
        {
            m_rank_page.Button_Tooltip_Clicked();
            m_is_tooltip_panel_ever_opened = false;
        }
    }

    public void StartEndGameAnimation()
    {
        if (m_contest == null)
            return;

        if (m_activeContest.PointsDelta() == 0)
            return;


        m_rank_page.In_Game_Tooltip.gameObject.SetActive(false);
        List<ContestPlayer> mini_leaderboard_rank = m_contest.Contest_players_mini_leaderboard_rank;
        int rank = m_activeContest.Current_playar_in_contest.Rank;

        // play all animations
        // check if in contest
        if (mini_leaderboard_rank.Count == 0)
            return;

        SetOpenPanel(true);

        // not first time in contest
        // *********************
        if (m_activeContest.StartGameRank != 0)
        {
            // maybe shouldn't be handles here
            ShowPage(Pages.Rank);
            if (mini_leaderboard_rank.Count == 3)
            {
                Handle3Players();
                return;
            }

            if (mini_leaderboard_rank.Count == 2)
            {
                Handle2Players();
                return;
            }

            if (mini_leaderboard_rank.Count == 1)
            {
                Handle1Player();
                return;
            }
        }

        // first time in contest
        // *********************
        // maybe shouldn't be handles here
        ShowPage(Pages.Entering);
        m_entering_the_contest = true;

        if (mini_leaderboard_rank.Count == 1)
        {
            HandleAnimations("Entering_The_Contest_Rank1_empty");
            return;
        }

        if (mini_leaderboard_rank.Count == 2)
        {
            if (rank == 1)
                HandleAnimations("Entering_The_Contest_Rank1_outof2");
            else
                HandleAnimations("Entering_The_Contest_Rank2_outof2");
            return;
        }

        if (mini_leaderboard_rank.Count == 3)
        {
            // first place in contest
            if (rank == 1)
                HandleAnimations("Entering_The_Contest_Rank1_outof3");
            // somewhere in the middle of the contest
            else
                HandleAnimations("Entering_The_Contest");
            return;
        }
    }

    private void Handle1Player()
    {
        HandleAnimations("Contest_Placement_Rank1_empty");
    }

    private void Handle2Players()
    {
        int startRank = m_activeContest.StartGameRank;
        int rank = m_activeContest.Current_playar_in_contest.Rank;

        if (rank == 1)
        {
            HandleAnimations("Contest_Placement_Rank1_outof2");
            return;
        }
        // This is also the situation when you're the last player in the leaderboard
        HandleAnimations("Contest_Placement_Rank2_outof2");
    }

    private void Handle3Players()
    {
        int startRank = m_activeContest.StartGameRank;
        int rank = m_activeContest.Current_playar_in_contest.Rank;

        // player didn't change his rank
        if (startRank == rank)
        {
            if (rank == 1)
            {
                HandleAnimations("Contest_Placement_Nomove_Rank1");
                return;
            }
            else
            {
                HandleAnimations("Contest_Placement_Nomove");
                return;
            }
        }

        // playerd improved his rank
        if (startRank > rank)
        {
            if (rank == 1)
            {
                if (m_started_with_2_players)
                {
                    HandleAnimations("Contest_Placement_Rank1_2to3");
                    return;
                }
                if (startRank == 2)
                {
                    // from second place to first
                    HandleAnimations("Contest_Placement_Small_up");
                    return;
                }
                // three players were at the begining
                HandleAnimations("Contest_Placement_Rank1");
                return;
            }

            // two players are playing
            // ***********************
            // play big animation
            // run numbers until the current match score
            if (m_started_with_2_players)
            {
                HandleAnimations("Contest_Placement_2to3");
                return;
            }

            // player raised by one rank , but not to first place
            if (startRank - 1 == rank)
            {
                // play small animation
                if (m_started_with_2_players)
                {
                    HandleAnimations("Contest_Placement_2to3");
                    return;
                }
                else
                {
                    m_rank_page.SetFakeData(m_contest);
                    HandleAnimations("Contest_Placement_Small_up_normal");
                    return;
                }
            }
            // started with three players in the ranks
            HandleAnimations("Contest_Placement");
            return;
        }

        // player went to a lower rank (bigger rank)
        if (startRank < rank)
        {
            // the player went down in rank
            HandleAnimations("None");
            m_rank_page.HandleAllButtons();
            m_rank_page.SetData(m_contest, true);
            return;
        }

        Debug.LogError("No animation was handled !");
    }

    private void HandleAnimations(string animationName)
    {
        //m_rank_page.SetData(m_contest);  REMOVE THIS?
        m_contest_panel_tooltip_points_view.SetData(m_contest);
        StartCoroutine(PlayAnimation(animationName));
        LoggerController.Instance.Log(LoggerController.Module.Contest, "animationName: " + animationName);
        StartCoroutine(m_contest_panel_tooltip_points_view.TooltipAnimationsOnEndGame());
        m_contest_panel_tooltip_points_view.SetTooltipData();
    }

    IEnumerator PlayAnimation(string animationName)
    {
        m_anim.Play("Reset_Animations");
        m_rank_page.ResetAnimations();
        yield return null;
        m_anim.Play(animationName);
    }

    // This is being called from the timeline as a trigger
    public void Trigger_SetData()
    {
        m_rank_page.SetPlayersData();
    }

    private void HideAllPages()
    {
        m_paytable_page.gameObject.SetActive(false);
        m_info_page.gameObject.SetActive(false);
        m_intro_page.gameObject.SetActive(false);
        m_rank_page.gameObject.SetActive(false);
        m_top3_page.gameObject.SetActive(false);
    }


    public void HideIntroPage()
    {
        m_intro_page.gameObject.SetActive(false);
    }

    public void ResetPanel()
    {
        HideAllPages();
    }

    public void BackInInfoClicked()
    {
        LoggerController.Instance.Log(LoggerController.Module.Contest, "BACK WAS CLICKED");
        ShowPage(Pages.Intro);
    }

    public void InfoClicked()
    {
        LoggerController.Instance.Log(LoggerController.Module.Contest, "INFO WAS CLICKED");
        ShowPage(Pages.Info);
    }

    public void IntroClicked()
    {
        LoggerController.Instance.Log(LoggerController.Module.Contest, "INTRO WAS CLICKED");
        ShowPage(Pages.Intro);
    }

    public void OpenPaytableClicked()
    {
        LoggerController.Instance.Log(LoggerController.Module.Contest, "Open Paytable CLICKED");
        ShowPage(Pages.Paytable);
    }

    public void TOP3Clicked()
    {
        LoggerController.Instance.Log(LoggerController.Module.Contest, "TOP3 CLICKED");
        ShowPage(Pages.Top3);
    }

    public void RankClicked()
    {
        LoggerController.Instance.Log(LoggerController.Module.Contest, "RANK CLICKED");
        ShowPage(Pages.Rank);
    }

    public bool Started_with_2_players { get => m_started_with_2_players; set => m_started_with_2_players = value; }
    public Animator Anim { get => m_anim; set => m_anim = value; }
}
