﻿using cardGames.controllers;
using cardGames.models.contests;
using common.utils;
using System;
using System.Collections.Generic;

namespace cardGames.views
{
    public class PaytableView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_actual_prize;
        [SerializeField] private GameObject m_full_paytable;
        [SerializeField] private GameObject m_paytable_button;
        [SerializeField] private GameObject m_arrow;
        [SerializeField] private bool m_is_expaned = false;
        [SerializeField] private Transform m_lines_container;
        [SerializeField] private GameObject m_mask_and_image;
        [SerializeField] private GameObject m_shadows;
        [SerializeField] private Scrollbar m_scrollbar;
        [SerializeField] private Image m_handle;
        [SerializeField] private Transform m_place_slider_without_scrollbar;
        [SerializeField] private GameObject m_grid_with_elements;
        [SerializeField] private GameObject m_paytable_line_1_prefab;
        [SerializeField] private GameObject m_paytable_line_2_prefab;
        [SerializeField] private GameObject m_paytable_line_3_prefab;
        [SerializeField] private GameObject m_paytable_line_4_prefab;
        private List<PaytableLineRowView> m_paytable_line_row_view;

        List<GameObject> m_go = new List<GameObject>();
        #endregion

        private void ResetPaytable()
        {
            for (int i = 0; i < m_go.Count; i++)
            {
                Destroy(m_go[i]);
            }
        }
        /// <summary>
        /// duplicated function. should be used only once
        /// </summary>
        public void SetData(Contest contest)
        {
            if (m_is_expaned)
            {
                BT_Expand_or_shrink_paytable();
            }
            int leftOver = contest.Contest_jackpot.Temporary_value % 10;
           // m_actual_prize.text = "<sprite=1> " + FormatUtils.FormatBalance(Convert.ToInt32(contest.Contest_jackpot.Temporary_value) - leftOver);
            m_actual_prize.text = "<sprite=1> " + FormatUtils.FormatBalance(Convert.ToInt32(contest.Contest_jackpot.Current_value));

            if (contest.Contest_Paytable == null)
                return;

            ResetPaytable();

            ContestPaytableColumn paytableColumn = ContestsController.Instance.FindActivePaytable(contest);

            foreach (ContestPaytableLine payline in paytableColumn.PayLines)
            {
                GameObject go = null;


                int min_rank = payline.MinRank;
                if (min_rank >= 1 && min_rank <= 3)
                {
                    switch (min_rank)
                    {
                        case 1:
                            go = Instantiate(m_paytable_line_1_prefab, m_lines_container);
                            break;
                        case 2:
                            go = Instantiate(m_paytable_line_2_prefab, m_lines_container);
                            break;
                        case 3:
                            go = Instantiate(m_paytable_line_3_prefab, m_lines_container);
                            break;
                    }
                }
                else
                    go = Instantiate(m_paytable_line_4_prefab, m_lines_container);

                go.GetComponent<PaytableLineRowView>().SetData(payline, contest);
                m_go.Add(go);
            }
        }

        public void SetJackPotData(Contest contest)
        {
            if (contest == null)
                return;

            int leftOver = contest.Contest_jackpot.Temporary_value % 10;
            int round;
            if (leftOver < 5)
                round = -leftOver;
            else
                round = 10 - leftOver;
            m_actual_prize.text = "<sprite=1> " + FormatUtils.FormatBalance(Convert.ToInt32(contest.Contest_jackpot.Current_value));

           // m_actual_prize.text = "<sprite=1> " + FormatUtils.FormatBalance(Convert.ToInt32(contest.Contest_jackpot.Temporary_value) + round);
        }

        public void BT_Expand_or_shrink_paytable()
        {
            m_is_expaned = !m_is_expaned;
            //m_scrollbar.SetActive(m_is_expaned);
            m_scrollbar.GetComponent<Image>().enabled = m_is_expaned;
            m_handle.enabled = m_is_expaned;

            m_mask_and_image.GetComponent<Mask>().enabled = m_is_expaned;
            m_mask_and_image.GetComponent<Image>().enabled = m_is_expaned;
            m_shadows.SetActive(m_is_expaned);
            m_full_paytable.SetActive(m_is_expaned);


            if (m_lines_container.transform.childCount < 11)
            {
                m_lines_container.transform.position = m_place_slider_without_scrollbar.position;

            }

            m_arrow.transform.Rotate(180, 0, 0, Space.Self);
        }
    }
}