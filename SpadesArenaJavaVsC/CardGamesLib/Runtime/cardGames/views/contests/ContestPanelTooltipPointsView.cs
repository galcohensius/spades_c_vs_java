﻿using cardGames.controllers;
using cardGames.models;
using cardGames.models.contests;
using common.utils;
using System;
using System.Collections;

namespace cardGames.views
{
    public class ContestPanelTooltipPointsView : MonoBehaviour
    {
        #region Const Members
        private const float COUNTER_ANIM_TIME = 1.4f;
        private const float GROWING_NUMBERS_PLAYTIME = 5f;
        #endregion

        #region SerializeField Members
        [SerializeField] GameObject m_tooltip_growing_numbers_container;
        [SerializeField] GameObject m_tooltip_2_container;
        [SerializeField] GameObject m_tooltip_4_container;
        [SerializeField] GameObject m_bubble;

        [SerializeField] TMP_Text m_tooltip;
        [SerializeField] private TMP_Text m_tooltip_2_rank;
        [SerializeField] private TMP_Text m_tooltip_2_prize;
        [SerializeField] private TMP_Text m_tooltip_4_rank;
        [SerializeField] private Animator m_anim;
        #endregion

        #region Private Members
        private Contest m_contest;
        private ActiveContest m_activeContest;
        #endregion


        public void SetData(Contest contest)
        {
            m_contest = contest;
            m_activeContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
        }

        public void SetTooltipData()
        {
            m_tooltip.text = FormatUtils.FormatBalance(m_activeContest.PointsDelta());
            ContestPaytableLine contestPaytableLine = ContestsController.Instance.FindNextRankAndPrize(m_contest);

            if (contestPaytableLine != null)
            {
                m_tooltip_2_rank.text = "RANK " + contestPaytableLine.MaxRank + "\nQUALIFIES FOR";
                m_tooltip_2_prize.text = "<sprite=1>" + FormatUtils.FormatBuyIn((int)ContestsController.Instance.CalculatePrize(m_contest, contestPaytableLine)) + "!";
            }

            // tooltip 2 and 4 shouldn't show the intro page
            m_tooltip_4_rank.text = m_activeContest.Current_playar_in_contest.Rank.ToString();
        }

        public void ResetTooltips()
        {
            m_tooltip_growing_numbers_container.SetActive(false);
            m_tooltip_2_container.SetActive(false);
            m_tooltip_4_container.SetActive(false);
            m_bubble.SetActive(false);
        }

        // Handeling the tooltip animations on end game
        // first part: growing numbers animation to show the amount of points the player got
        // second part: what's the next prize qualification for a bigger prize
        // third part: players new rank
        public IEnumerator TooltipAnimationsOnEndGame()
        {
            ResetTooltips();

            int pointsInc = (int)(m_activeContest.PointsDelta() / (60 * COUNTER_ANIM_TIME));
            double current_points = 0;

            // if the player got points in this game
            if (m_activeContest.PointsDelta() > 0)
            {
                m_tooltip_growing_numbers_container.SetActive(true);
                m_tooltip_2_container.SetActive(false);
                m_tooltip_4_container.SetActive(false);
                m_bubble.SetActive(true);

                while (current_points <= m_activeContest.PointsDelta())
                {
                    current_points += pointsInc;
                    m_tooltip.text = FormatUtils.FormatBalance((int)Math.Min(current_points, m_activeContest.PointsDelta()));
                    // wait for one frame
                    yield return null;
                }
            }
        
                ContestPaytableLine contestPaytableLine = ContestsController.Instance.FindNextRankAndPrize(m_contest);
                // if the player isn't in first place
                if (contestPaytableLine != null)
                {
                    yield return new WaitForSecondsRealtime(GROWING_NUMBERS_PLAYTIME);

                    // tooltip 2 and 4 shouldn't show the intro page
                    ContestPanelController.Instance.Contest_panel_view.HideIntroPage();

                    m_tooltip_growing_numbers_container.SetActive(false);
                    m_tooltip_2_container.SetActive(true);
                    m_tooltip_4_container.SetActive(false);
                    m_bubble.SetActive(true);

                    m_tooltip_2_rank.text = "RANK " + contestPaytableLine.MaxRank + "\nQUALIFIES FOR";
                    m_tooltip_2_prize.text = "<sprite=1>" + FormatUtils.FormatPrice((int)ContestsController.Instance.CalculatePrize(m_contest, contestPaytableLine)) + "!";
                }

                // start and end game rank isn't same
                if (m_activeContest.Current_playar_in_contest.Rank != m_activeContest.StartGameRank)
                {
                    // show tooltip 4
                    yield return new WaitForSecondsRealtime(GROWING_NUMBERS_PLAYTIME);

                    m_tooltip_2_container.SetActive(false);
                    m_tooltip_growing_numbers_container.SetActive(false);
                    m_tooltip_4_container.SetActive(true);
                    m_bubble.SetActive(true);

                    // tooltip 2 and 4 shouldn't show the intro page
                    ContestPanelController.Instance.Contest_panel_view.HideIntroPage();
                    m_tooltip_4_rank.text = m_activeContest.Current_playar_in_contest.Rank.ToString();
                }
            }
        
    }
}
