﻿using cardGames.controllers;
using cardGames.models;
using cardGames.models.contests;
using common;
using common.utils;

namespace cardGames.views
{
    public class ContestPanelPlayerView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_rank;
        [SerializeField] private TMP_Text m_points;
        [SerializeField] private TMP_Text m_nickname;
        [SerializeField] private GameObject m_points_trophy;
        [SerializeField] private MAvatarView m_mavatar_view;
        private bool m_is_me;
        private int m_current_rank;
        private bool is_nickname_shown;
        #endregion

        public void SetData(Contest contest, int order)
        {
            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
            if (contest.Contest_players_mini_leaderboard_rank.Count > 0)
            {
                ContestPlayer contestPlayer = contest.Contest_players_mini_leaderboard_rank[order];
                m_mavatar_view.SetAvatarModel(contestPlayer.AvatarData);
                PrefabUtils.DisableAnimations(m_mavatar_view.transform);

                m_points.text = FormatUtils.FormatBalance(contestPlayer.Points);
                m_current_rank = contestPlayer.Rank;
                m_nickname.text = m_mavatar_view.MAvatarModel.NickName;
                TMPBidiHelper.MakeRTL(m_nickname);

                string movement;
                if (contestPlayer.Rank_movement == ContestPlayer.RankMovement.Down)
                    movement = "<sprite=1>";
                else if (contestPlayer.Rank_movement == ContestPlayer.RankMovement.Up)
                    movement = "<sprite=0>";
                else
                    movement = "";
                m_rank.text = m_current_rank + movement;

                if (!ContestPanelController.Instance.During_game || aContest.Current_playar_in_contest.Rank == 1)
                {
                    GetComponent<ButtonsOverView>().enabled = false;
                }
                else
                {
                    GetComponent<ButtonsOverView>().enabled = true;
                }
                CancelInvoke("ReplaceNicknameAndPoints");
                is_nickname_shown = false; // Reset the flag so all players will switch the same
                InvokeRepeating("ReplaceNicknameAndPoints", 2.0f, 3f);

            }
        }

        public void SetFakeDate(Contest contest, int order)
        {
            SetData(contest, order);

            ContestPlayer contestPlayer = contest.Contest_players_mini_leaderboard_rank[order];
            switch (order)
            {
                case 0:
                    m_rank.text = contest.Contest_players_mini_leaderboard_rank[order].Rank.ToString();
                    break;
                case 1:
                    m_rank.text = (contest.Contest_players_mini_leaderboard_rank[order].Rank + 1).ToString();
                    break;
                case 2:
                    m_rank.text = (contest.Contest_players_mini_leaderboard_rank[order].Rank - 1).ToString();
                    break;

            }

        }

        private void ReplaceNicknameAndPoints()
        {
            is_nickname_shown = !is_nickname_shown;
            if (is_nickname_shown)
            {
                m_points_trophy.SetActive(false);
                m_points.gameObject.SetActive(false);
                m_nickname.gameObject.SetActive(true);
            }
            else
            {
                m_points_trophy.SetActive(true);
                m_points.gameObject.SetActive(true);
                m_nickname.gameObject.SetActive(false);
            }
        }
    }
}
