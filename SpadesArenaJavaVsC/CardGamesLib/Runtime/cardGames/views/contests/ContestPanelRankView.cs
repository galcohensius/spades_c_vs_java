﻿using cardGames.controllers;
using cardGames.models;
using cardGames.models.contests;
using common.controllers;
using System;
using System.Collections;

namespace cardGames.views
{
    public class ContestPanelRankView : MonoBehaviour
    {
        #region Private Members
        private bool m_is_tooltip_mode = false;
        Action m_top3_button_clicked;
        Action m_info_button_clicked;
        Action m_paytable_button_clicked;
        private Contest m_contest;
        private ActiveContest m_activeContest;
        #endregion

        #region SerializeField Members
        [SerializeField] private GameObject m_top3;
        [SerializeField] private GameObject m_top3_button;
        [SerializeField] private GameObject m_paytable_button;
        [SerializeField] private GameObject m_info_button;
        [SerializeField] private ContestPanelTooltipView m_in_game_tooltip;
        [SerializeField] private ContestPanelTooltipPointsView m_end_game_tooltip;
        [SerializeField] private ContestPanelPlayerView m_player_box_prior;
        [SerializeField] private ContestPanelPlayerView m_player_box_me;
        [SerializeField] private ContestPanelPlayerView m_player_box_following;
        [SerializeField] Animator m_anim;
        [SerializeField] private ContestPanelPlayerView m_duplicated_me_row_for_tooltip;

        [SerializeField] GameObject m_contestReel;
        [SerializeField] GameObject m_ReelSparks;

        Coroutine m_autoClose;
        Action m_on_end_game;

        public ContestPanelTooltipView In_Game_Tooltip { get => m_in_game_tooltip; set => m_in_game_tooltip = value; }
        #endregion

        private void Start()
        {
            m_in_game_tooltip.gameObject.SetActive(false);
        }

        public void SetData(Contest contest, bool shouldDataBeSet)
        {
            m_contest = contest;
            m_activeContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
            if (ContestPanelController.Instance.During_game)
                m_end_game_tooltip.gameObject.SetActive(false);
            else
                m_end_game_tooltip.gameObject.SetActive(true);

            HandleAllButtons();

            // At end game, set player data will be called by a trigger from the timeline
            if (m_autoClose != null)
                StopCoroutine(m_autoClose);

            if (shouldDataBeSet)
            {
                SetPlayersData();
                //m_autoClose = StartCoroutine(CloseTooltipAutomatically());
            }

            ResetAnimations();
        }

        public void SetFakeData(Contest contest)
        {
            Debug.Log("SetFakeData");
            if (m_contest.Contest_players_mini_leaderboard_rank.Count == 3)
            {
                m_player_box_following.SetFakeDate(m_contest, 0);
                m_player_box_me.SetFakeDate(m_contest, 1);
                m_player_box_prior.SetFakeDate(m_contest, 2);
            }
        }

        public void SetPlayersData()
        {
            if (m_activeContest.Current_playar_in_contest != null && m_contest.Contest_players_mini_leaderboard_rank.Count == 3)
            {
                m_player_box_prior.gameObject.SetActive(true);
                m_player_box_me.gameObject.SetActive(true);
                m_player_box_following.gameObject.SetActive(true);

                if (m_activeContest.Current_playar_in_contest.Rank == 1)
                {
                    m_player_box_prior.SetData(m_contest, 2);
                    m_player_box_following.SetData(m_contest, 1);
                    m_player_box_me.SetData(m_contest, 0);

                    m_duplicated_me_row_for_tooltip.SetData(m_contest, 0);
                }
                else
                {
                    m_player_box_prior.SetData(m_contest, 2);
                    m_player_box_me.SetData(m_contest, 1);
                    m_player_box_following.SetData(m_contest, 0);

                    m_duplicated_me_row_for_tooltip.SetData(m_contest, 1);
                    m_in_game_tooltip.SetData(m_contest);
                }

                m_end_game_tooltip.SetData(m_contest);
            }
            else if (m_contest.Contest_players_mini_leaderboard_rank.Count == 2)
            {
                if (m_activeContest.Current_playar_in_contest != null && m_activeContest.Current_playar_in_contest.Rank == 1)
                {
                    m_player_box_me.SetData(m_contest, 0);
                    m_player_box_following.SetData(m_contest, 1);
                    m_player_box_prior.gameObject.SetActive(false);
                    m_duplicated_me_row_for_tooltip.SetData(m_contest, 0);
                }
                else
                {
                    m_player_box_prior.gameObject.SetActive(false);
                    m_player_box_following.SetData(m_contest, 0);
                    m_player_box_me.SetData(m_contest, 1);
                    m_duplicated_me_row_for_tooltip.SetData(m_contest, 1);
                    m_in_game_tooltip.SetData(m_contest);
                }

                m_end_game_tooltip.SetData(m_contest);
            }
            else if (m_contest.Contest_players_mini_leaderboard_rank.Count == 1)
            {
                m_player_box_prior.gameObject.SetActive(false);
                m_player_box_following.gameObject.SetActive(false);
                m_player_box_me.SetData(m_contest, 0);

                m_duplicated_me_row_for_tooltip.SetData(m_contest, 0);
                m_end_game_tooltip.SetData(m_contest);
            }
            else if (m_contest.Contest_players_mini_leaderboard_rank.Count == 0)
            {
                m_player_box_prior.gameObject.SetActive(false);
                m_player_box_following.gameObject.SetActive(false);
                m_player_box_me.gameObject.SetActive(false);
                m_end_game_tooltip.SetData(m_contest);
            }

        }
        public void SetButtons(Action top3_button_clicked, Action info_button_clicked, Action paytable_button_clicked)
        {
            m_top3_button_clicked = top3_button_clicked;
            m_info_button_clicked = info_button_clicked;
            m_paytable_button_clicked = paytable_button_clicked;
        }

        public void ResetAnimations()
        {
            m_contestReel.SetActive(false);
            m_ReelSparks.SetActive(false);
        }

        public void ResetTooltip()
        {
            m_is_tooltip_mode = false;
            if (m_contest.Contest_players_mini_leaderboard_rank.Count == 2)
            {
                m_anim.Play("Player_Tooltip_Close_Last_Place");
                LoggerController.Instance.Log(LoggerController.Module.Contest + "playing animation named: Player_Tooltip_Close_Last_Place");
                return;
            }
            else
            {
                m_anim.Play("Player Tooltip close");
                LoggerController.Instance.Log(LoggerController.Module.Contest + "Player Tooltip close");
                return;
            }
        }

        public void Button_Tooltip_Clicked()
        {
            if (m_activeContest.Current_playar_in_contest == null || m_activeContest.Current_playar_in_contest.Rank == 1 || !gameObject.activeSelf)
            {
                m_in_game_tooltip.gameObject.SetActive(false);
                LoggerController.Instance.Log(LoggerController.Module.Contest + "m_in_game_tooltip.gameObject.SetActive(false);");
                return;
            }

            m_end_game_tooltip.gameObject.SetActive(false);
            LoggerController.Instance.Log(LoggerController.Module.Contest + "m_end_game_tooltip.gameObject.SetActive(false);");


            // end game
            if (!ContestPanelController.Instance.During_game)
            {
                m_end_game_tooltip.gameObject.SetActive(true);
                LoggerController.Instance.Log(LoggerController.Module.Contest + "m_end_game_tooltip.gameObject.SetActive(true);");

                return;
            }
            m_in_game_tooltip.gameObject.SetActive(false);
            LoggerController.Instance.Log(LoggerController.Module.Contest + "m_in_game_tooltip.gameObject.SetActive(false);");

            m_anim.enabled = true;
            if (m_is_tooltip_mode)
            {
                m_is_tooltip_mode = false;
                if (m_contest.Contest_players_mini_leaderboard_rank.Count == 2)
                {
                    m_anim.Play("Player_Tooltip_Close_Last_Place");
                    LoggerController.Instance.Log(LoggerController.Module.Contest + "Player_Tooltip_Close_Last_Place (2)");
                    return;
                }
                else
                {
                    m_anim.Play("Player Tooltip close");
                    LoggerController.Instance.Log(LoggerController.Module.Contest + "Player Tooltip close (2)");

                    return;
                }
            }
            if (m_is_tooltip_mode)
                return;

            m_is_tooltip_mode = true;
            if (m_contest.Contest_players_mini_leaderboard_rank.Count == 2)
            {
                m_anim.Play("Player_Tooltip_Last_Place");
                LoggerController.Instance.Log(LoggerController.Module.Contest + "Player_Tooltip_Last_Place (2)");

            }
            else
            {
                m_anim.Play("Player Tooltip");
                LoggerController.Instance.Log(LoggerController.Module.Contest + "Player Tooltip (2)");

            }

            if (m_autoClose != null)
                StopCoroutine(m_autoClose);

            m_autoClose = StartCoroutine(CloseTooltipAutomatically());
        }

        private IEnumerator CloseTooltipAutomatically()
        {
            // increased the time from 2.5f to 3f
            yield return new WaitForSecondsRealtime(3f);
            if (m_contest.Contest_players_mini_leaderboard_rank.Count == 2)
            {
                LoggerController.Instance.Log(LoggerController.Module.Contest + "funcitonName: CloseTooltipAutomatically, animationName: Player_Tooltip_Close_Last_Place");
                m_anim.Play("Player_Tooltip_Close_Last_Place");
            }
            else
            {
                LoggerController.Instance.Log(LoggerController.Module.Contest + "funcitonName: CloseTooltipAutomatically, animationName: Player Tooltip close");
                m_anim.Play("Player Tooltip close");
            }
            m_is_tooltip_mode = false;
            yield return new WaitForSecondsRealtime(1f);
            m_anim.enabled = false;
        }

        public void HandleAllButtons()
        {
            if (ContestPanelController.Instance.During_game)
            {
                m_top3_button.gameObject.SetActive(true);
                m_paytable_button.gameObject.SetActive(true);
                m_info_button.gameObject.SetActive(true);
            }
            else
            {
                m_paytable_button.gameObject.SetActive(false);
                m_info_button.gameObject.SetActive(false);
                m_top3_button.gameObject.SetActive(false);
            }
        }

        public void HandleTop3Buttons()
        {
            if (m_contest != null && m_activeContest.Current_playar_in_contest != null)
            {
                if (m_activeContest.Current_playar_in_contest.Rank <= 3)
                    m_top3_button.gameObject.SetActive(false);
                else
                    m_top3_button.gameObject.SetActive(true);
            }
        }

        public void Button_Top3_Clicked()
        {
            ResetTooltip();
            if (m_top3_button_clicked != null)
                m_top3_button_clicked();
        }

        public void Button_Info_Clicked()
        {
            ResetTooltip();
            if (m_info_button_clicked != null)
                m_info_button_clicked();
        }

        public void Button_Paytable_Clicked()
        {
            ResetTooltip();
            if (m_paytable_button_clicked != null)
                m_paytable_button_clicked();
        }
    }
}