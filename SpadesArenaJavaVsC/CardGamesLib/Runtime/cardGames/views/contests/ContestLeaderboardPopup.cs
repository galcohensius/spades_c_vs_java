﻿using common.views;
using common.controllers;
using cardGames.models;
using cardGames.models.contests;
using System.Collections.Generic;
using common.utils;
using cardGames.controllers;

namespace cardGames.views
{
    public class ContestLeaderboardPopup : PopupBase
    {
        #region const Members
        Contest m_contest;
        const float THRESHOLD_SEC = 5;
        public const float ITEM_WIDTH = 60f;
        #endregion

        #region SerializeField Members
        [SerializeField] VerticalLayoutGroup m_verticalLayoutGroup;
        [SerializeField] GameObject m_contest_row_leaderboard_prefab;
        [SerializeField] GameObject m_me_row_prefab;
        [SerializeField] GameObject me_row = null;
        [SerializeField] PaytableView m_paytable_view;
        [SerializeField] GameObject m_waiting_indication;
        [SerializeField] ContestTimerView m_contestTimerView;
        [SerializeField] TMP_Text m_bet_range_ends_in;
        [SerializeField] GameObject items_container = null;
        [SerializeField] ScrollRect m_scroll_rect = null;
        [SerializeField] Scrollbar m_scroll_bar;
        [SerializeField] GameObject m_titles;
        [SerializeField] GameObject m_title_no_leaderboard;
        #endregion

        #region Private Members
        private List<ContestRowLeaderboardView> m_rankingRows;
        private GameObject selfRowGridRef = null;
        private bool isMeRow = false;
        Coroutine m_timer_coroutine = null;
        private float m_treshhold_for_displaying_data = 0f;
        #endregion

        #region Public Members
        public Contest Contest { get => m_contest; set => m_contest = value; }
        #endregion

        public override void Start()
        {
            base.Start();

            ModelManager.Instance.ContestsLists.OnContestDataChanged += ClosePopup;
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            m_contestTimerView.SetData(m_contest);
            if (m_contest.TableFilterData.MinBuyIn == m_contest.TableFilterData.MaxBuyIn)
                m_bet_range_ends_in.text = "BET: " + FormatUtils.FormatBuyIn(m_contest.TableFilterData.MaxBuyIn) + " | " + "ENDS IN: ";
            else
                m_bet_range_ends_in.text = "BET: " + FormatUtils.FormatBuyIn(m_contest.TableFilterData.MinBuyIn) + "-" + FormatUtils.FormatBuyIn(m_contest.TableFilterData.MaxBuyIn) + " | " + "ENDS IN: ";
            // m_bet_range_ends_in.preferredWidth
            // m_bet_range_ends_in.bounds = m_contestTimerView.Timer_text.preferredWidth;
            // m_bet_range_ends_in.GetComponent<RectTransform>().localPosition = new Vector3(m_bet_range_ends_in.transform.position.x + m_contestTimerView.Timer_text.preferredWidth, m_bet_range_ends_in.transform.position.y, m_bet_range_ends_in.transform.position.z);

            m_paytable_view.SetJackPotData(m_contest);
            if (m_contest.Contest_players != null)
            {
                ShowLeaderboardRows();
            }
            else
            {
                ShowAndHideWaitingIndication(true);
                m_contest.OnContestPlayersUpdated += ShowLeaderboardRows;

            }
        }

        private void ShowLeaderboardRows()
        {
            if (m_verticalLayoutGroup == null)
                return;

            selfRowGridRef = null;
            int index = 0;
            GameObject go = null;

            m_rankingRows = new List<ContestRowLeaderboardView>();
            foreach (ContestPlayer contestPlayer in m_contest.Contest_players)
            {
                if (contestPlayer.Id == ModelManager.Instance.GetUser().GetID())
                {
                    go = Instantiate(m_me_row_prefab, m_verticalLayoutGroup.transform);
                    me_row.GetComponent<ContestRowLeaderboardView>().SetData(m_contest, contestPlayer);
                    isMeRow = true;
                    selfRowGridRef = go;
                }
                else
                {
                    if (m_verticalLayoutGroup != null)
                        go = Instantiate(m_contest_row_leaderboard_prefab, m_verticalLayoutGroup.transform);
                }
                if (go != null)
                {
                    go.GetComponent<ContestRowLeaderboardView>().SetData(m_contest, contestPlayer);
                    m_rankingRows.Add(go.GetComponent<ContestRowLeaderboardView>());
                }
                index++;
            }
            // no data
            if (m_contest.ContestPlayers.Count == 0)
            {
                //m_title_no_leaderboard = Instantiate(m_title_no_leaderboard_prefab, m_verticalLayoutGroup.transform);
                if (m_title_no_leaderboard != null)
                {
                    m_title_no_leaderboard.SetActive(true);
                    m_titles.SetActive(false);
                }
            }

            ShowAndHideWaitingIndication(false);

            m_paytable_view.SetData(m_contest);
        }

        private void ShowAndHideWaitingIndication(bool show)
        {
            // for cases that the user closes the windows right after it opened (didn't init all the data)
            if (items_container == null || m_waiting_indication == null)
                return;

            items_container.SetActive(!show);
            m_waiting_indication.SetActive(show);
        }

        public override void CloseButtonClicked()
        {
            ClosePopup();
        }

        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }

        private void ClosePopup()
        {
            ModelManager.Instance.ContestsLists.OnContestDataChanged -= ClosePopup;
            CardGamesPopupManager.Instance.ShowContestsPopup(PopupManager.AddMode.ShowAndRemove);

        }

        void OnEnable()
        {
            m_scroll_rect.onValueChanged.AddListener(onScroll);
        }

        public void onScroll(Vector2 vec)
        {


            if (selfRowGridRef == null || items_container.transform.childCount <= 6)
            {
                me_row.SetActive(false);
                return;
            }

            float combined_pos = items_container.transform.localPosition.y + selfRowGridRef.transform.localPosition.y;

            //LoggerController.Instance.Log("CombinedPos: " + combined_pos);

            float Min = -630f;
            float Max = 0f;


            if (combined_pos > Min && combined_pos <= Max)
            {
                me_row.SetActive(false);
            }
            else if (isMeRow && combined_pos < Min)
            {
                // Me row at the bottom
                me_row.SetActive(true);
                me_row.transform.localPosition = new Vector3(me_row.transform.localPosition.x, -393f, 0);
            }
            else if (isMeRow && combined_pos > Max)
            {
                // Me row at the top
                me_row.SetActive(true);
                me_row.transform.localPosition = new Vector3(me_row.transform.localPosition.x, 232f, 0);
            }
        }

        public override void Close()
        {
            base.Close();
            if (m_contest!=null)
                m_contest.OnContestPlayersUpdated = ShowLeaderboardRows;
        }

        protected override void PlayOpenAnimation()
        {
            UpdatePopupAlpha(1);
        }

        //protected override void PlayCloseAnimation()
        //{
        //    OnExitAnimDone();
        //}
    }
}


