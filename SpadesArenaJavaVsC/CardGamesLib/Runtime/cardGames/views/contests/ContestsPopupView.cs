﻿using cardGames.models;
using cardGames.models.contests;
using common.controllers;
using common.utils;
using common.views;
using System.Collections.Generic;

namespace cardGames.views
{
    public class ContestsPopupView : PopupBase
    {
        public static ContestsPopupView Instance;

        #region const Members
        const int NUM_OF_GROUP_TYPES = 3;
        #endregion


        #region SerializeField Members
        [Header("General")]
        [SerializeField] GameObject m_contets;
        [SerializeField] GameObject m_leaderboard;
        [SerializeField] GameObject m_info;

        [Header("Contests")]
        [SerializeField] VerticalLayoutGroup m_verticalLayoutGroup;
        [SerializeField] ContestsGroupView m_contest_type_prefab;
        [SerializeField] Transform m_grid_with_elements;
        List<ContestsGroupView> m_cgt;
        List<GameObject> m_go;
        List<GameObject> m_goRows;
        #endregion

        [SerializeField]
        private Scrollbar m_scrollBar;

        // Leaderboard:
        #region const Members
        Contest m_contest;
        #endregion

        [Header("LeaderBoard")]

        #region SerializeField Members
        [SerializeField] VerticalLayoutGroup m_verticalLayoutGroup2;
        [SerializeField] GameObject m_contest_row_leaderboard_prefab;
        [SerializeField] GameObject m_me_row_prefab;
        [SerializeField] GameObject me_row = null;
        [SerializeField] PaytableView m_paytable_view;
        [SerializeField] GameObject m_waiting_indication;
        [SerializeField] ContestTimerView m_contestTimerView;
        [SerializeField] TMP_Text m_bet_range_ends_in;
        [SerializeField] GameObject items_container = null;
        [SerializeField] ScrollRect m_scroll_rect = null;
        [SerializeField] Scrollbar m_scroll_bar;
        [SerializeField] GameObject m_titles;
        [SerializeField] GameObject m_title_no_leaderboard;
        #endregion

        #region Private Members
        private List<ContestRowLeaderboardView> m_rankingRows;
        private GameObject selfRowGridRef = null;
        private bool isMeRow = false;
        Coroutine m_timer_coroutine = null;
        private float m_treshhold_for_displaying_data = 0f;
        #endregion

        #region Public Members
        public Contest Contest { get => m_contest; set => m_contest = value; }
        #endregion

        public enum ShowPage
        {
            Contests, Leaderboard, Info
        };

        ShowPage m_showPage = ShowPage.Contests;

        private void Awake()
        {
            Instance = this;
            m_cgt = new List<ContestsGroupView>();
            m_go = new List<GameObject>();
            m_goRows = new List<GameObject>();
        }

        public override void Start()
        {
            base.Start();
            ModelManager.Instance.ContestsLists.OnContestDataChanged += RefreshPopup;
        }

        public void OnDestroy()
        {
            ModelManager.Instance.ContestsLists.OnContestDataChanged -= RefreshPopup;
        }

        public void SetPageToShow(ShowPage page, Contest contest = null)
        {
            if (contest != null)
                m_contest = contest;

            m_showPage = page;

            // reset all game objects
            m_contets.SetActive(false);
            m_leaderboard.SetActive(false);
            m_info.SetActive(false);

            switch (page)
            {
                case ShowPage.Contests:
                    m_contets.SetActive(true);
                    LoadAllContestsToPopup();

                    break;

                case ShowPage.Leaderboard:
                    m_leaderboard.SetActive(true);
                    SetAndShowLeaderboard();
                    break;

                case ShowPage.Info:
                    m_info.SetActive(true);
                    break;
            }
        }

        void OnEnable()
        {
            m_scrollBar.onValueChanged.AddListener(delegate { ValueChangeCheck(); });
            m_scroll_rect.onValueChanged.AddListener(onScroll);
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);
            ContestsPopupView.Instance.SetPageToShow(ContestsPopupView.ShowPage.Contests);

            m_verticalLayoutGroup.gameObject.SetActive(true);

            UnityMainThreadDispatcher.Instance.DelayedCall(0.1f, () =>
            {
                m_scrollBar.value = 1; // Always start with scrollbar at the top position
            });
        }

        private void SetAndShowLeaderboard()
        {
            // leaderboard:
            m_paytable_view.SetData(m_contest);
            m_contestTimerView.SetData(m_contest);
            if (m_contest.TableFilterData.MinBuyIn == m_contest.TableFilterData.MaxBuyIn)
                m_bet_range_ends_in.text = "TABLE: " + FormatUtils.FormatBuyIn(m_contest.TableFilterData.MaxBuyIn);
            else
                m_bet_range_ends_in.text = "TABLES: " + FormatUtils.FormatBuyIn(m_contest.TableFilterData.MinBuyIn) + "-" + FormatUtils.FormatBuyIn(m_contest.TableFilterData.MaxBuyIn);

            m_paytable_view.SetJackPotData(m_contest);
            ClearRows();

            if (m_contest.Contest_players != null)
            {
                ShowLeaderboardRows();
            }
            else
            {
                ShowAndHideWaitingIndication(true);
                m_contest.OnContestPlayersUpdated += ShowLeaderboardRows;
            }
        }

        private void ClearContests()
        {
            for (int i = 0; i < m_verticalLayoutGroup.transform.childCount; i++)
                Destroy(m_verticalLayoutGroup.transform.GetChild(i).gameObject);

            if (m_cgt.Count > 0)
                m_cgt.Clear();
        }

        public void LoadAllContestsToPopup()
        {
            ClearContests();
            for (int i = 0; i < NUM_OF_GROUP_TYPES; i++)
            {
                ContestsGroupView contest_type = m_contest_type_prefab;
                GameObject go = Instantiate(contest_type.gameObject, m_grid_with_elements);
                m_go.Add(go);
                m_cgt.Add(go.GetComponent<ContestsGroupView>());
                m_cgt[i].SetData((Contest.ContestStatus)i);
            }
        }

        // Invoked when the value of the slider changes.
        public void ValueChangeCheck()
        {
            if (m_cgt.Count == 0 || !m_scrollBar.gameObject.activeSelf)
                return;
            if (m_cgt[0].Contest_rows != null)
                AvailableOff(m_cgt[0].Contest_rows);
            if (m_cgt[1].Contest_rows != null)
                AvailableOff(m_cgt[1].Contest_rows);
            if (m_cgt[2].Contest_rows != null)
                AvailableOff(m_cgt[2].Contest_rows);
        }

        // call the available function off
        private void AvailableOff(List<ContestRowView> result)
        {
            foreach (ContestRowView crv in result)
                crv.Contest_table.HideAvailable();
        }

        private void RefreshPopup()
        {
            SetPageToShow(ShowPage.Contests);
        }


        public override void CloseButtonClicked()
        {
            ClosePopup();
        }

        public override void OnBackButtonClicked()
        {
            ClosePopup();
        }

        public void OnBackButtonClickedToContestsPage()
        {
            SetPageToShow(ShowPage.Contests);
        }

        public void Button_InfoClicked()
        {
            SetPageToShow(ShowPage.Info);
        }

        private void ClosePopup()
        {
            m_manager.HidePopup();
        }

        public void BUTTON_Click_Close()
        {
            m_contestTimerView.StopTimerCoroutime();
            ClearRows();

            if (m_contest != null)
                m_contest.OnContestPlayersUpdated = ShowLeaderboardRows;

            ModelManager.Instance.ContestsLists.OnContestDataChanged -= RefreshPopup;
            ModelManager.Instance.ContestsLists.OnContestDataChanged -= ClosePopup;
            m_manager.HidePopup();
        }



        // ********************* LeaderBoard *********************

        private void ClearRows()
        {
            // is this fixing the clearRows null reference bug?
            if (m_verticalLayoutGroup2 == null)
                return;

            for (int i = 1; i < m_verticalLayoutGroup2.transform.childCount; i++)
            {
                if (m_verticalLayoutGroup2.gameObject.name != m_title_no_leaderboard.name)
                    if (m_verticalLayoutGroup2.transform.GetChild(i) != null)
                        Destroy(m_verticalLayoutGroup2.transform.GetChild(i).gameObject);
            }
            m_title_no_leaderboard.SetActive(false);
            me_row.SetActive(false);
            isMeRow = false;
        }

        private void ShowLeaderboardRows()
        {
            if (m_verticalLayoutGroup2 == null)
                return;

            selfRowGridRef = null;
            int index = 0;
            GameObject goRow = null;

            ClearRows();
            m_rankingRows = new List<ContestRowLeaderboardView>();
            me_row.SetActive(false);

            foreach (ContestPlayer contestPlayer in m_contest.Contest_players)
            {
                if (contestPlayer.Id == ModelManager.Instance.GetUser().GetID())
                {
                    goRow = Instantiate(m_me_row_prefab, m_verticalLayoutGroup2.transform);
                    m_goRows.Add(goRow);
                    me_row.GetComponent<ContestRowLeaderboardView>().SetData(m_contest, contestPlayer);
                    if (contestPlayer.Rank > 6)
                        me_row.SetActive(true);
                    isMeRow = true;
                    selfRowGridRef = goRow;
                }
                else
                {
                    if (m_verticalLayoutGroup2 != null)
                    {
                        goRow = Instantiate(m_contest_row_leaderboard_prefab, m_verticalLayoutGroup2.transform);
                        m_goRows.Add(goRow);
                    }
                }
                if (goRow != null)
                {
                    goRow.GetComponent<ContestRowLeaderboardView>().SetData(m_contest, contestPlayer);
                    m_rankingRows.Add(goRow.GetComponent<ContestRowLeaderboardView>());
                }
                index++;
            }
            // no data
            if (m_contest.ContestPlayers.Count == 0)
            {
                //m_title_no_leaderboard = Instantiate(m_title_no_leaderboard_prefab, m_verticalLayoutGroup.transform);
                if (m_title_no_leaderboard != null)
                {
                    m_title_no_leaderboard.SetActive(true);
                    m_titles.SetActive(false);
                }
            }

            ShowAndHideWaitingIndication(false);

        }

        private void ShowAndHideWaitingIndication(bool show)
        {
            // for cases that the user closes the windows right after it opened (didn't init all the data)
            if (items_container == null || m_waiting_indication == null)
                return;

            items_container.SetActive(!show);
            m_waiting_indication.SetActive(show);
        }

        public void onScroll(Vector2 vec)
        {
            if (selfRowGridRef == null || items_container.transform.childCount <= 6)
            {
                me_row.SetActive(false);
                return;
            }

            float combined_pos = items_container.transform.localPosition.y + selfRowGridRef.transform.localPosition.y;

            float Min = -630f;
            float Max = 0f;

            if (combined_pos > Min && combined_pos <= Max)
            {
                me_row.SetActive(false);
            }
            else if (isMeRow && combined_pos < Min)
            {
                // Me row at the bottom
                me_row.SetActive(true);
                me_row.transform.localPosition = new Vector3(me_row.transform.localPosition.x, -393f, 0);
            }
            else if (isMeRow && combined_pos > Max)
            {
                // Me row at the top
                me_row.SetActive(true);
                me_row.transform.localPosition = new Vector3(me_row.transform.localPosition.x, 232f, 0);
            }
        }

        protected override void PlayOpenAnimation()
        {
            UpdatePopupAlpha(1);
        }
    }
}


