﻿using cardGames.models;
using cardGames.models.contests;
using common.controllers;
using System;
using System.Collections.Generic;

namespace cardGames.views
{
    public class ContestPanelTop3PageView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] private GameObject m_top3;
        [SerializeField] List<ContestPanelPlayerTopRowView> player_place;
        [SerializeField] GameObject m_player_in_top3;
        [SerializeField] GameObject m_player_isnt_in_top3;
        [SerializeField] GameObject m_top3_title;
        #endregion

        #region Private Members
        private bool m_is_tooltip_mode;
        Action m_info_button_clicked;
        Action m_paytable_button_clicked;
        Action m_ranking_button_clicked;
        #endregion

        public void SetData(Contest contest)
        {

            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);

            // get player with rank 3 data
            List<ContestPlayer> mini_leader_board_players_top3 = contest.Contest_players_mini_leaderboard_top3;
            if (contest.Contest_players_mini_leaderboard_rank.Count > 0)
            {
                if (aContest.Current_playar_in_contest != null && aContest.Current_playar_in_contest.Rank <= 3)
                {
                    m_player_in_top3.SetActive(true);
                    m_player_isnt_in_top3.SetActive(false);
                    m_top3_title.SetActive(false);
                }
                else
                {
                    m_player_in_top3.SetActive(false);
                    m_player_isnt_in_top3.SetActive(true);
                    m_top3_title.SetActive(true);
                }
            }

            for (int i = 0; i < 3; i++)
                player_place[i].gameObject.SetActive(false);

            for (int i = 0; i < mini_leader_board_players_top3.Count; i++)
            {
                player_place[i].gameObject.SetActive(true);
                player_place[i].SetData(mini_leader_board_players_top3[i]);
            }

        }

        public void SetButtons(Action info_button_clicked, Action paytable_button_clicked)
        {
            m_info_button_clicked = info_button_clicked;
            m_paytable_button_clicked = paytable_button_clicked;
        }

        public void SetButtons(Action ranking_button_clicked)
        {
            m_ranking_button_clicked = ranking_button_clicked;
        }

        public void Button_Ranking_Clicked()
        {
            LoggerController.Instance.Log(LoggerController.Module.Contest,"Button_Ranking_Clicked (in ranking)");
            if (m_ranking_button_clicked != null)
                m_ranking_button_clicked();
        }

        public void Button_Info_Clicked()
        {
            LoggerController.Instance.Log(LoggerController.Module.Contest,"Button_Info_Clicked (in ranking)");
            if (m_info_button_clicked != null)
                m_info_button_clicked();
        }

        public void Button_Paytable_Clicked()
        {
            LoggerController.Instance.Log(LoggerController.Module.Contest,"Button_Paytable_Clicked (in ranking)");
            if (m_paytable_button_clicked != null)
                m_paytable_button_clicked();
        }
    }
}