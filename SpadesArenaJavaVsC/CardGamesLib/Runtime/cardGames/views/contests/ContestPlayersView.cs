﻿using cardGames.models.contests;

namespace cardGames.views
{
    public class ContestPlayersView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_players_text;
        #endregion

        public void SetData(Contest contest)
        {
            if (contest.Status == Contest.ContestStatus.Future)
                m_players_text.text = "";
            else
                m_players_text.text = contest.Total_players.ToString();
        }
    }
}
