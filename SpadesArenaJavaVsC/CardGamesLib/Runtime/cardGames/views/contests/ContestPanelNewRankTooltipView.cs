﻿using cardGames.models;
using cardGames.models.contests;

namespace cardGames.views
{
    public class ContestPanelNewRankTooltipView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] private TMP_Text m_rank;
        #endregion


        public void SetData(Contest contest)
        {
            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
            if (aContest.Current_playar_in_contest == null)
                return;

            // is this the old or new rank ?
            m_rank.text = aContest.Current_playar_in_contest.Rank.ToString();
        }
    }
}
