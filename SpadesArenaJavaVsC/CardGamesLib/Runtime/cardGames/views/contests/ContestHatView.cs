﻿using cardGames.models.contests;
using System.Collections;


namespace cardGames.views.contests
{
    public abstract class ContestHatView : MonoBehaviour
    {
        const float ALTERNTATE_INTERVAL = 5f;
        const float ALTERNTATE_SWITCH = .5f;

        [SerializeField] ContestJackPotView contestJackPotView;
        [SerializeField] ContestRankView contestRankView;
        [SerializeField] ContestTimerView contestTimerView;

        CanvasGroup m_hats_canvas;
        Animator m_animator;
        CanvasGroup m_rank_canvas;
        CanvasGroup m_timer_canvas;

        [SerializeField] TMP_Text m_title_text;
        Contest m_contest;
        Coroutine m_alternate_texts;
        [SerializeField] protected Transform m_container;

        public virtual void Awake()
        {
            m_rank_canvas = contestRankView.gameObject.GetComponent<CanvasGroup>();
            m_timer_canvas = contestTimerView.gameObject.GetComponent<CanvasGroup>();
            m_animator = gameObject.GetComponent<Animator>();
            m_hats_canvas = gameObject.GetComponent<CanvasGroup>();
        }

        public void InitAndLink(Contest contest)
        {
            if (m_hats_canvas == null)
                return;

            m_hats_canvas.alpha = 0;

            m_hats_canvas.alpha = 0;
            m_contest = contest;

            m_title_text.text = "Contest #" + m_contest.Id;
            contestJackPotView.SetData(m_contest);
            contestTimerView.SetData(m_contest);
            m_contest.OnContestDynamicDataUpdated += ContestDynamicDataUpdated;
            if (m_contest.Rank != 0)
            {
                contestRankView.SetData(m_contest.Rank);
                StartCoroutine(AlternatePrize_Timer());
                PlayAnim(true);
            }
            else
            {
                m_timer_canvas.gameObject.SetActive(true);
                m_rank_canvas.gameObject.SetActive(false);
            }
        }

        public void PlayAnim(bool play_in)
        {
            if (play_in)
                m_animator.SetTrigger("AnimIn");
            else
                m_animator.SetTrigger("AnimOut");
        }

        private void ContestDynamicDataUpdated()
        {
            contestRankView.SetData(m_contest.Rank);
        }

        private void OnDisable()
        {
            if (m_contest != null)
                m_contest.OnContestDynamicDataUpdated -= ContestDynamicDataUpdated;
        }

        IEnumerator AlternatePrize_Timer()
        {

            RectTransform rank_rect = m_rank_canvas.gameObject.GetComponent<RectTransform>();
            RectTransform timer_rect = m_timer_canvas.gameObject.GetComponent<RectTransform>();

            GameObject rank_group = rank_rect.gameObject;
            GameObject timer_group = timer_rect.gameObject;

            m_timer_canvas.alpha = 0f;
            m_rank_canvas.alpha = 0f;

            m_rank_canvas.alpha = 1f;
            /*   rank_rect.localPosition = new Vector3(0f, rank_rect.localPosition.y, 0);

               timer_rect.localPosition = new Vector3(0, timer_rect.localPosition.y, 0);*/

            bool group_active = true;

            while (group_active)
            {
                yield return new WaitForSeconds(ALTERNTATE_INTERVAL);

                //timer_rect.localPosition = new Vector3(0, timer_rect.localPosition.y, 0);

                //iTween.MoveTo(rank_group, iTween.Hash("x", -20f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(rank_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdatePrizeAlpha", "Onupdatetarget", this.gameObject));


                //iTween.MoveTo(timer_group, iTween.Hash("x", 0f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(timer_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateTimerAlpha", "Onupdatetarget", this.gameObject));

                yield return new WaitForSeconds(ALTERNTATE_SWITCH + ALTERNTATE_INTERVAL);


                //   rank_rect.localPosition = new Vector3(0, rank_rect.localPosition.y, 0);
                //iTween.MoveTo(rank_group, iTween.Hash("x", 0f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(rank_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdatePrizeAlpha", "Onupdatetarget", this.gameObject));


                //iTween.MoveTo(timer_group, iTween.Hash("x", -20f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(timer_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateTimerAlpha", "Onupdatetarget", this.gameObject));

                yield return new WaitForSeconds(ALTERNTATE_SWITCH);
            }

        }

        public void UpdatePrizeAlpha(float value)
        {
            m_rank_canvas.alpha = value;
        }

        public void UpdateTimerAlpha(float value)
        {
            m_timer_canvas.alpha = value;
        }

    }
}