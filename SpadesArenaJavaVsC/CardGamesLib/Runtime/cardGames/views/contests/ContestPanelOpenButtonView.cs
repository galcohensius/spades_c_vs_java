﻿using cardGames.models.contests;

/// <summary>
/// This class is taking care of the appearance of the contest open button
/// shows rank if in contest, otherwise just show the title
/// </summary>
namespace cardGames.views
{
public class ContestPanelOpenButtonView : MonoBehaviour
{
    #region SerializeField Members
    [SerializeField] GameObject m_trophy;
    [SerializeField] GameObject m_contest_logo;
    [SerializeField] TMP_Text m_rank;
    [SerializeField] GameObject m_rank_bg;
    #endregion

    #region Private Members
    private string m_movement;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        m_trophy.SetActive(true);
        m_contest_logo.SetActive(true);
        m_rank_bg.gameObject.SetActive(false);
    }

        public void SetData(Contest contest)
        {
            if (contest.Contest_players_mini_leaderboard_rank.Count == 0)
            {
                m_contest_logo.SetActive(true);
                m_rank_bg.gameObject.SetActive(false);
                return;
            }

            foreach (ContestPlayer contestPlayer in contest.Contest_players_mini_leaderboard_rank)
            {
                if (!contestPlayer.IsMe)
                    continue;

                m_contest_logo.SetActive(false);
                m_rank_bg.gameObject.SetActive(true);

                if (contestPlayer.Rank_movement == ContestPlayer.RankMovement.Down)
                    m_movement = "<sprite=1>";
                else if (contestPlayer.Rank_movement == ContestPlayer.RankMovement.Up)
                    m_movement = "<sprite=0>";
                else
                    m_movement = "";

                m_rank.text = contestPlayer.Rank + m_movement;
                return;
            }
        }
    }
}
