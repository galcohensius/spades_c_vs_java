﻿using cardGames.models;
using System.Text;

namespace cardGames.utils
{

    public static class CardGamesFormatUtils
    {
        public static string FormatHand(CardsList cards, string delimiter = ",")
        {
            CardsList cl = new CardsList(cards);
            cl.SortHighToLowSuitFirst();

            StringBuilder result = new StringBuilder();
            foreach (Card card in cl)
            {
                result.Append(card.ToString());
                if (delimiter != null)
                    result.Append(',');
            }
            if (result.Length > 0 && delimiter != null)
                result.Remove(result.Length - 1, 1);
            return result.ToString();
        }

    }
}