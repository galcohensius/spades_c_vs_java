using cardGames.controllers;
using cardGames.models.contests;
using common.controllers;
using common.utils;
using System;
using System.Collections;

namespace cardGames.views
{
    public class ContestJackPotView : MonoBehaviour
    {
        #region Private Members
        [SerializeField]
        private TMP_Text m_jackpot_text;
        Contest m_contest;
        Coroutine m_timer_routine = null;
        bool m_initialized = false;
        #endregion

        public void SetData(Contest contest)
        {

            m_jackpot_text.text = "<sprite=1> " + FormatUtils.FormatJackpot(contest.Contest_jackpot.Current_value);

            if (contest.Status == Contest.ContestStatus.Ongoing)
            {
                m_contest = contest;

                contest.OnContestDynamicDataUpdated -= JackpotDataUpdate;
                contest.OnContestDynamicDataUpdated += JackpotDataUpdate;


                if (contest.Contest_jackpot.Prev_value >= 0)
                {
                    //means there was already a value assigned to this in the past
                    JackpotDataUpdate();
                }
            }
            m_initialized = true;
        }


        private void JackpotDataUpdate()
        {
            if (m_contest == null)
                return;

            int target_value = m_contest.Contest_jackpot.Current_value;

            DateTime update_time_stamp = m_contest.Contest_jackpot.Data_update_time;

            int m_calculated_start_value = m_contest.Contest_jackpot.Prev_value;

            int delta_value = target_value - m_calculated_start_value;

            //the moving number needs to reach the curr_value - DATA_REFRESH_INTERVAL seconds after the update_time
            TimeSpan interval_timeSpan = new TimeSpan(ContestsController.DATA_REFRESH_INTERVAL * 10000000);
            TimeSpan time_left_timeSpan = (update_time_stamp + interval_timeSpan) - DateTime.Now;

            float time_left_seconds = (float)time_left_timeSpan.TotalSeconds;

            if (time_left_seconds < 0)
            {
                //this means the last update was more than interval ago
            }
            else
            {
                int modified_start_value = (int)(m_calculated_start_value + delta_value * (ContestsController.DATA_REFRESH_INTERVAL - time_left_seconds) / ContestsController.DATA_REFRESH_INTERVAL);

                LoggerController.Instance.LogFormat(LoggerController.Module.Contest, "Jackpot view for contest {0}: Start Value: {1}, Target value: {2}, Time left: {3}", m_contest.Id, m_calculated_start_value, target_value, time_left_seconds);

                //m_calculated_start_value = modified_start_value;
                delta_value = target_value - modified_start_value;


                if (m_timer_routine != null)
                    StopCoroutine(m_timer_routine);


                if (delta_value > 0 && gameObject.activeInHierarchy)
                    m_timer_routine = StartCoroutine(ShowMovingNumbers(delta_value, time_left_seconds, modified_start_value));
            }

        }


        IEnumerator ShowMovingNumbers(int delta_value, float delta_time, int calculated_start_value)
        {
            float updateInterval = 0.03f; // sec

            double deltaInterval = delta_value * updateInterval / delta_time;

            double jackpotValue = calculated_start_value;
            int endValue = calculated_start_value + delta_value;


            while (jackpotValue <= endValue)
            {
                jackpotValue += deltaInterval;

                jackpotValue = Math.Min(jackpotValue, endValue);

                m_jackpot_text.text = "<sprite=1> " + FormatUtils.FormatJackpot(jackpotValue);

                yield return new WaitForSeconds(updateInterval);
            }
            LoggerController.Instance.Log(LoggerController.Module.Contest, "End Rountine Time: " + Time.time);
        }

        private void OnDisable()
        {
            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (m_contest != null)
                m_contest.OnContestDynamicDataUpdated -= JackpotDataUpdate;
        }

        private void OnEnable()
        {
            // if (m_initialized)
            //    JackpotDataUpdate();
            /*
            if (m_contest != null)
                m_contest.OnContestDynamicDataUpdated += JackpotDataUpdate;

            if(m_contest !=null)
                JackpotDataUpdate();*/
        }

    }
}
