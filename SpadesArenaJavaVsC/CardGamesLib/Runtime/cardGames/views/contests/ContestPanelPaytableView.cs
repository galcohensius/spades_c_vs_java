using cardGames.controllers;
using cardGames.models.contests;
using common.controllers;
using System;
using System.Collections.Generic;

namespace cardGames.views
{
    public class ContestPanelPaytableView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private GameObject m_full_paytable;
        [SerializeField] private GameObject m_paytable_button;
        [SerializeField] private GameObject m_arrow;
        [SerializeField] private bool m_is_expaned = false;
        [SerializeField] private Transform m_lines_container;
        [SerializeField] private GameObject m_mask_and_image;
        [SerializeField] private GameObject m_shadows;
        [SerializeField] private Scrollbar m_scrollbar;
        private Contest m_contest;
        [SerializeField] private GameObject m_paytable_line_1_prefab;
        [SerializeField] private GameObject m_paytable_line_2_prefab;
        [SerializeField] private GameObject m_paytable_line_3_prefab;
        [SerializeField] private GameObject m_paytable_line_4_prefab;
        [SerializeField] private GameObject m_paytable_line_empty_prefab;
        private List<PaytableLineRowView> m_paytable_line_row_view;
        Action m_button_clicked;
        #endregion

        /// <summary>
        /// duplicated function. should be used only once
        /// </summary>
        /// <param name="contest_id"></param>
        public void SetData(Contest contest)
        {
            m_contest = contest;

            if (m_contest.Contest_Paytable == null)
                return;

            ContestPaytableColumn cpc = ContestsController.Instance.FindActivePaytable(m_contest);

            // Destroys all the children of the parent
            foreach (Transform child in m_lines_container)
                Destroy(child.gameObject);

            int counter = 0;
            foreach (ContestPaytableLine payline in cpc.PayLines)
            {
                GameObject go = null;
                int min_rank = payline.MinRank;
                if (min_rank >= 1 && min_rank <= 3)
                {
                    switch (min_rank)
                    {
                        case 1:
                            go = Instantiate(m_paytable_line_1_prefab, m_lines_container);
                            break;
                        case 2:
                            go = Instantiate(m_paytable_line_2_prefab, m_lines_container);
                            break;
                        case 3:
                            go = Instantiate(m_paytable_line_3_prefab, m_lines_container);
                            break;
                    }
                }
                else
                    go = Instantiate(m_paytable_line_4_prefab, m_lines_container);

                go.GetComponent<PaytableLineRowView>().SetData(payline, contest);
                counter++;
            }

            if (counter < 7)
            {
                m_mask_and_image.GetComponent<ScrollRect>().enabled = false;
                m_scrollbar.gameObject.SetActive(false);
            }
            else
            {
                m_mask_and_image.GetComponent<ScrollRect>().enabled = true;
                m_scrollbar.gameObject.SetActive(true);
            }

            while (counter < 7)
            {
                GameObject go = null;
                go = Instantiate(m_paytable_line_empty_prefab, m_lines_container);
                counter++;
            }
        }

        public void BT_Expand_or_shrink_paytable()
        {
            m_is_expaned = !m_is_expaned;
            m_mask_and_image.GetComponent<Mask>().enabled = m_is_expaned;
            m_mask_and_image.GetComponent<Image>().enabled = m_is_expaned;
            m_shadows.SetActive(m_is_expaned);
            m_full_paytable.SetActive(m_is_expaned);
            m_arrow.transform.Rotate(180, 0, 0, Space.Self);
        }

        public void SetButtons(Action button_clicked)
        {
            m_button_clicked = button_clicked;
        }

        public void Button_Clicked()
        {
            LoggerController.Instance.Log(LoggerController.Module.Contest, "Button clicked (in paytable)");
            if (m_button_clicked != null)
                m_button_clicked();
        }
    }
}
