﻿using cardGames.models;
using cardGames.models.contests;
using common.utils;

namespace cardGames.views
{
    public class PlayerLeaderboardView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_player;
        [SerializeField] private MAvatarView m_mavatar;
        #endregion

        // should get the right avatar model of every user in that contest
        public void SetData(ContestPlayer contestPlayer)
        {
             MAvatarModel avatarModel = contestPlayer.AvatarData;
             m_mavatar.SetAvatarModel(avatarModel);

            m_player.text = avatarModel.NickName;
            TMPBidiHelper.MakeRTL(m_player);
        }
    }
}