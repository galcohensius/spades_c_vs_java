﻿using cardGames.controllers;
using cardGames.models.contests;

namespace cardGames.views
{
    public class ContestPanelTooltipView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] GameObject m_tooltip_container;
        [SerializeField] TMP_Text m_tooltip;
        #endregion

        private void Awake()
        {
            this.gameObject.SetActive(false);
        }

        public void SetData(Contest contest)
        {
            ContestPaytableLine contestPaytableLine = ContestsController.Instance.FindNextRankAndPrize(contest);
            if (contestPaytableLine == null)
                return;

            if (contestPaytableLine.IsLast)
                m_tooltip.text = "Reach rank " + contestPaytableLine.MaxRank + " to qualify for a coin prize.";
            else
                m_tooltip.text = "Reach rank " + contestPaytableLine.MaxRank + " for a bigger prize.";
        }
    }
}