﻿using cardGames.models.contests;

namespace cardGames.views
{
    public class ContestPlayerRankView : MonoBehaviour
    {
        #region Private Members
        bool m_color_flag = false;
        #endregion

        #region SerializeField Members
        [SerializeField] private TMP_Text m_rank;
        #endregion

        public void SetData(Contest contest)
        {
            m_color_flag = false;
            if (contest.Status == Contest.ContestStatus.Completed)
                m_color_flag = true;

            if (contest.Status == Contest.ContestStatus.Future)
                m_rank.text = "";
            else if (contest.Rank == 0)
            {
                m_color_flag = true;
                m_rank.text = "---";
            }
            else
                m_rank.text = contest.Rank.ToString();

            Color color;
            if (m_color_flag)
            {
                if (ColorUtility.TryParseHtmlString("#999999", out color))
                {
                    m_rank.color = color;
                }
            }
            else
            {
                if (ColorUtility.TryParseHtmlString("#ffffff", out color))
                {
                    m_rank.color = color;
                }
            }
        }
    }
}
