﻿using cardGames.models.contests;
using common.controllers;
using System;

namespace cardGames.views
{
    public class ContestPanelInfoPageView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_text;
        [SerializeField] private TMP_Text m_wins;
        [SerializeField] private TMP_Text m_lose;
        Action m_button_clicked;
        #endregion

        public void SetData(Contest contest)
        {
            m_wins.text = "<size=30>x</size>" + contest.WinFactor;
            m_lose.text = "<size=30>x</size>" + contest.LoseFactor;
        }

        public void SetButtons(Action button_clicked)
        {
            m_button_clicked = button_clicked;
        }

        public void Button_Clicked()
        {
            LoggerController.Instance.Log(LoggerController.Module.Contest, "Button clicked (in intro)");
            if (m_button_clicked != null)
                m_button_clicked();
        }
    }
}


