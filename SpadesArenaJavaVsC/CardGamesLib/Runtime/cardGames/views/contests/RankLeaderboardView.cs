﻿using cardGames.models.contests;
using common.controllers;

namespace cardGames.views
{
    public class RankLeaderboardView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_rank;
        [SerializeField] private TMP_Text m_arrow;
        [SerializeField] private Image m_trophy1;
        [SerializeField] private Image m_trophy2;
        [SerializeField] private Image m_trophy3;
        #endregion

        public void SetData(ContestPlayer contestPlayer)
        {
            try
            {
                if (contestPlayer.Rank_movement == ContestPlayer.RankMovement.Up)
                {
                    // player Increased it's rank
                    m_arrow.text = " <sprite=0>";
                }
                else if (contestPlayer.Rank_movement == ContestPlayer.RankMovement.Down)
                {
                    // player Dicreased it's rank
                    m_arrow.text = " <sprite=1>";
                }
                else
                {
                    // player stayed in the same rank
                    m_arrow.text = "";
                }
                m_rank.text = contestPlayer.Rank.ToString();
                m_trophy1.gameObject.SetActive(false);
                m_trophy2.gameObject.SetActive(false);
                m_trophy3.gameObject.SetActive(false);

                if (contestPlayer.Rank == 1)
                {
                    m_trophy1.gameObject.SetActive(true);
                    Color color;
                    if (ColorUtility.TryParseHtmlString("#f1c763", out color))
                    { m_rank.color = color; }
                }
                else if (contestPlayer.Rank == 2)
                {
                    m_trophy2.gameObject.SetActive(true);
                    Color color;
                    if (ColorUtility.TryParseHtmlString("#dbe2e9", out color))
                    { m_rank.color = color; }
                }
                else if (contestPlayer.Rank == 3)
                {
                    m_trophy3.gameObject.SetActive(true);
                    Color color;
                    if (ColorUtility.TryParseHtmlString("#d69871", out color))
                    { m_rank.color = color; }
                }
                else
                {
                    Color color;
                    if (ColorUtility.TryParseHtmlString("#999999", out color))
                    { m_rank.color = color; }
                }
            }
            catch
            {
                LoggerController.Instance.Log("no such place in list");
            }
        }
    }
}