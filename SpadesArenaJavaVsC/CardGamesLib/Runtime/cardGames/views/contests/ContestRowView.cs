﻿using cardGames.controllers;
using cardGames.models.contests;

namespace cardGames.views
{
    public class ContestRowView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private ContestTableView m_contest_table;
        [SerializeField] private ContestJackPotView m_contest_jackpot;
        [SerializeField] private ContestPlayersView m_contest_players;
        [SerializeField] private ContestPlayerRankView m_contest_player_rank;
        [SerializeField] private ContestTimerView m_contest_timer;
        [SerializeField] GameObject m_contest_row_leaderboard_prefab;
        [SerializeField] Toggle m_remind_me;
        #endregion

        #region Private Members
        private Contest m_contest;
        #endregion

        public void SetData(Contest contest)
        {
            m_contest = contest;
            m_contest_table.SetData(contest);
            m_contest_jackpot.SetData(contest);
            m_contest_players.SetData(contest);
            m_contest_player_rank.SetData(contest);
            m_contest_timer.SetData(contest);

            // relevant only for future contests
            m_remind_me.isOn = contest.Remind_me;

#if UNITY_WEBGL
            // Don't show remind me on WebGL
            m_remind_me.gameObject.SetActive(false);
#endif
        }

        public void RefreshData()
        {
            SetData(m_contest);
        }

        public void BT_RankingClicked()
        {
            int leftOver = m_contest.Contest_jackpot.Current_value % 10;
            int round;
            if (leftOver < 5)
                round = -leftOver;
            else
                round = 10 - leftOver;

            m_contest.Contest_jackpot.SetTemporaryValue(m_contest.Contest_jackpot.Current_value + round);
            ContestsController.Instance.ShowContestLeaderboardPopup(m_contest);
        }

        public void BT_RemindMe()
        {
            if (m_contest != null)
                ContestsController.Instance.SwitchRemindMeMode(m_contest, m_remind_me.isOn);
        }

        public ContestTableView Contest_table { get => m_contest_table; }

    }
}