﻿using cardGames.models.contests;
using common.utils;
using common.views.popups;

namespace cardGames.views
{
    public class ContestTableView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] private TMP_Text m_jackpot_text;
        [SerializeField] protected InfoBubbleContainer m_infoBubbleContainer;
        #endregion

        protected virtual void Start()
        {
        }

        public virtual void SetData(Contest contest)
        {
            if (contest.TableFilterData.MinBuyIn == contest.TableFilterData.MaxBuyIn)
                m_jackpot_text.text = FormatUtils.FormatBuyIn(contest.TableFilterData.MinBuyIn);
            else
                m_jackpot_text.text = FormatUtils.FormatBuyIn(contest.TableFilterData.MinBuyIn) + " - " + FormatUtils.FormatBuyIn(contest.TableFilterData.MaxBuyIn);
        }

        public virtual void HideAvailable()
        {
            m_infoBubbleContainer.Hide();
        }

        public TMP_Text Jackpot_text { get => m_jackpot_text; set => m_jackpot_text = value; }
    }
}
