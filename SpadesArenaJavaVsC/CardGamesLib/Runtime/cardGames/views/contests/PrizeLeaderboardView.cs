﻿using cardGames.controllers;
using cardGames.models.contests;
using common.controllers;
using common.utils;

namespace cardGames.views
{
    public class PrizeLeaderboardView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_prize;
        #endregion

        public void SetData(Contest contest, ContestPlayer contestPlayer)
        {
            try
            {
                contestPlayer.Prize = (int)ContestsController.Instance.FindPrizeMatch(contest, contestPlayer);
                int leftOver = contestPlayer.Prize % 10;
                int round;
                if (leftOver < 5)
                    round = -leftOver;
                else
                    round = 10 - leftOver;

                if (contestPlayer.Prize > 0)
                    m_prize.text = "<sprite=1> " + FormatUtils.FormatBalance(contestPlayer.Prize + round);
                else
                    m_prize.text = "";

            }
            catch
            {
                LoggerController.Instance.Log("(PrizeLeaderboardView) no such place in list");
            }
        }
    }
}