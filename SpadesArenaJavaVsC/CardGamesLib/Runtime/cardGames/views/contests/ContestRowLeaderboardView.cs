﻿using cardGames.models.contests;

namespace cardGames.views
{
    public class ContestRowLeaderboardView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private RankLeaderboardView m_rank;
        [SerializeField] private PlayerLeaderboardView m_player;
        [SerializeField] private PointsLeaderboardView m_points;
        [SerializeField] private PrizeLeaderboardView m_prize;

        #endregion

        public void SetData(Contest contest, ContestPlayer contestPlayer)
        {
            m_rank.SetData(contestPlayer);
            m_player.SetData(contestPlayer);
            m_points.SetData(contestPlayer);
            m_prize.SetData(contest, contestPlayer);
        }
    }
}