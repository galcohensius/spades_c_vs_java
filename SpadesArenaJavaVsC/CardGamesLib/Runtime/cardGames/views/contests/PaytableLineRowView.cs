﻿using cardGames.controllers;
using cardGames.models.contests;
using common.utils;

namespace cardGames.views
{
    public class PaytableLineRowView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_rank_range;
        [SerializeField] private TMP_Text m_winning_prize;
        #endregion

        public void SetData(ContestPaytableLine contestPaytableLine, Contest contest)
        {
            m_winning_prize.text = "<sprite=1> " + FormatUtils.FormatBuyIn(RoundPrize((int)ContestsController.Instance.CalculatePrize(contest, contestPaytableLine)));

            if (contestPaytableLine.MinRank < 4)
            {
                Color color;
                switch (contestPaytableLine.MinRank)
                {
                    case 1:
                        if (ColorUtility.TryParseHtmlString("#F1C763", out color))
                        {
                            m_rank_range.color = color;
                        }
                        break;

                    case 2:
                        if (ColorUtility.TryParseHtmlString("#DBE2E9", out color))
                        {
                            m_rank_range.color = color;
                        }
                        break;

                    case 3:
                        if (ColorUtility.TryParseHtmlString("#D69871", out color))
                        {
                            m_rank_range.color = color;
                        }
                        break;

                }
                m_rank_range.text = contestPaytableLine.MinRank.ToString();
            }
            else
            {
                if (contestPaytableLine.MinRank != contestPaytableLine.MaxRank)
                    m_rank_range.text = contestPaytableLine.MinRank.ToString() + "-" + contestPaytableLine.MaxRank.ToString();
                else
                    m_rank_range.text = contestPaytableLine.MinRank.ToString();
            }
        }

        private int RoundPrize(int prize)
        {
            if (prize == 0)
                return 0;

            int leftOvers = prize % 10;

            if (leftOvers < 5)
                return prize - leftOvers;

            return prize + (10 - leftOvers);
        }
    }
}
