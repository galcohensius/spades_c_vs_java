﻿using cardGames.models.contests;
using common.utils;
using System.Collections;

namespace cardGames.views
{
    public class ContestTimerView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] private TMP_Text m_timer_text;
        [SerializeField] private TMP_Text m_length_text;

        [SerializeField] private bool m_color_flag;
        [SerializeField] GameObject m_calculating_mask;
        #endregion

        #region Private Members
        Contest m_contest;
        Coroutine m_timer_coroutine;
        #endregion


        public void SetData(Contest contest)
        {
            m_contest = contest;
            m_color_flag = false;

            // Start the timer coroutine
            if (m_timer_coroutine != null)
                StopCoroutine(m_timer_coroutine);
            if (m_contest.Status == Contest.ContestStatus.Completed)
            {
                m_calculating_mask.SetActive(false);
                m_timer_text.text = "---";
                m_color_flag = true;
            }
            else
            {
                if (m_timer_coroutine != null)
                    StopCoroutine(m_timer_coroutine);

                if (gameObject.activeInHierarchy)
                    m_timer_coroutine = StartCoroutine(TimerCoroutine());
            }

            int duration = m_contest.Duration_sec;
            string durationString = DateUtils.FormatDuration(duration);
            m_length_text.text = "DURATION: " + durationString;

            Color color;
            if (m_color_flag)
            {
                if (ColorUtility.TryParseHtmlString("#999999", out color))
                {
                    m_timer_text.color = color;
                }
            }
            else
            {
                if (ColorUtility.TryParseHtmlString("#ffffff", out color))
                {
                    m_timer_text.color = color;
                }
            }
        }

        public void StopTimerCoroutime()
        {
            if (m_timer_coroutine != null)
                StopCoroutine(TimerCoroutine());
        }

        private IEnumerator TimerCoroutine()
        {
            m_calculating_mask.SetActive(false);
            m_timer_text.gameObject.SetActive(true);


            while (true)
            {
                long interval = DateUtils.TotalSeconds(m_contest.Time_to_switch_status);

                if (interval <= 0)
                    break;

                m_timer_text.text = "T" + DateUtils.ConvertSecondsToDate(interval);


                yield return new WaitForSecondsRealtime(1);

            }


            // show calculating results for ongoing contests
            if (m_contest.Status == Contest.ContestStatus.Ongoing)
            {
                m_calculating_mask.SetActive(true);
                m_timer_text.gameObject.SetActive(false);
                if (m_color_flag)
                {
                    Color color;
                    if (ColorUtility.TryParseHtmlString("#999999", out color))
                    {
                        m_length_text.color = color;
                        m_timer_text.color = color;
                    }
                }
            }
        }

        public TMP_Text Timer_text { get => m_timer_text; set => m_timer_text = value; }
    }
}
