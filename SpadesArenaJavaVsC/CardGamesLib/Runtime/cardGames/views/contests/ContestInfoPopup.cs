﻿using cardGames.views;

namespace common.views.popups
{
    public class ContestInfoPopup : MonoBehaviour
    {
        //Canvas m_canvas;
        [SerializeField] TMP_Text m_button_text;
        int m_page = 0;
        [SerializeField] GameObject group1;
        [SerializeField] GameObject group2;

      /*  public void SetDepth(int depth)
        {
            m_canvas = GetComponent<Canvas>();
            m_canvas.sortingOrder = depth;
        }*/

        public void Button_Clicked()
        {
            if (m_page == 0)
            {
                m_page = 1;

                group1.SetActive(false);
                group2.SetActive(true);
            }
            else
            {
                m_page = 0;

                group1.SetActive(true);
                group2.SetActive(false);
            }
            
            SetButtonData();

        }

        private void SetButtonData()
        {
            if(m_page == 0)
                m_button_text.text ="Next";
            else
                m_button_text.text = "Previous";
        }

        public void OnBackButtonClickedToContestsPage()
        {
            ContestsPopupView.Instance.SetPageToShow(ContestsPopupView.ShowPage.Contests);
        }

        public void Button_ClosePopup()
        {
            ContestsPopupView.Instance.Close();
        }
    }
}