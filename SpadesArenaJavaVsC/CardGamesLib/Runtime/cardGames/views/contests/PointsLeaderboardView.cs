﻿using cardGames.models.contests;
using common.controllers;
using common.utils;

namespace cardGames.views
{
    public class PointsLeaderboardView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_points;
        #endregion

        public void SetData(ContestPlayer contestPlayer)
        {
            try
            {
                m_points.text = FormatUtils.FormatBalance(contestPlayer.Points);
            }
            catch
            {
                LoggerController.Instance.Log("no such place in list");
            }
        }
    }
}
