﻿using cardGames.controllers;
using cardGames.models.contests;
using System.Collections.Generic;

namespace cardGames.views
{
    public class ContestsGroupView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] private TMP_Text m_group_name;
        [SerializeField] GameObject m_row_completed_prefab;
        [SerializeField] GameObject m_row_ongoing_prefab;
        [SerializeField] GameObject m_row_future_prefab;
        [SerializeField] GameObject m_arrow_down;
        [SerializeField] GameObject m_arrow_right;

        [SerializeField] private TMP_Text m_title_prize_pool;
        [SerializeField] private TMP_Text m_title_players;
        [SerializeField] private TMP_Text m_title_rank;
        [SerializeField] private TMP_Text m_title_time;
        [SerializeField] private GameObject m_title_no_contests_prefab;
        #endregion

        #region Private Members
        private GameObject m_title_no_contests;
        private List<ContestRowView> m_contest_rows;
        private bool m_is_expanded = true;
        private Contest.ContestStatus m_contestStatus;
        #endregion
        #region Public Members
        public List<ContestRowView> Contest_rows { get => m_contest_rows; }
        #endregion

        public void SetData(Contest.ContestStatus contestStatus)
        {
            m_contestStatus = contestStatus;
            List<Contest> contest_list = ContestsController.Instance.GetContestListByStatus(contestStatus);
            if (contest_list.Count == 0)
            {
                if (contestStatus != Contest.ContestStatus.Future)
                {
                    Destroy(gameObject);
                    return;
                }
                else
                {
                    m_title_no_contests = Instantiate(m_title_no_contests_prefab, this.transform.parent);
                    m_title_no_contests.SetActive(true);
                }
            }
            else
            {
                m_title_players.gameObject.SetActive(true);
                m_title_rank.gameObject.SetActive(true);
                m_title_prize_pool.gameObject.SetActive(true);
                m_title_time.gameObject.SetActive(true);
            }
            m_contest_rows = new List<ContestRowView>();

            CreateRows();

            SetGroupName(contestStatus);

            if (contestStatus == Contest.ContestStatus.Completed)
                btn_ExpandOrShrink();

        }

        private void SetGroupName(Contest.ContestStatus contestStatus)
        {
            m_title_players.text = "players";
            m_title_rank.text = "my rank";
            m_title_prize_pool.text = "PRIZE POOL";
            switch (contestStatus)
            {
                case Contest.ContestStatus.Completed:
                    m_group_name.text = "COMPLETED CONTESTS";
                    m_title_time.text = "TIME";
                    break;

                case Contest.ContestStatus.Ongoing:
                    m_group_name.text = "LIVE CONTESTS";
                    m_title_time.text = "TIME";
                    break;

                case Contest.ContestStatus.Future:
                    m_group_name.text = "UPCOMING CONTESTS";
                    m_title_time.text = "STARTS IN";
                    m_title_players.text = "";
                    m_title_rank.text = "";
                    break;
            }
        }

        private void ClearRows()
        {
            for (int i = 0; i < transform.parent.childCount; i++)
            {
             //   Destroy(transform.parent.GetChild(i).gameObject);
            }
        }

        private void CreateRows()
        {
            List<Contest> contest_list = ContestsController.Instance.GetContestListByStatus(m_contestStatus);
            m_contest_rows.Clear();
            ClearRows();
            foreach (Contest contest in contest_list)
            {
                GameObject contestGO;

                // The prefab for completed and ongoing is the same
                if (m_contestStatus == Contest.ContestStatus.Future)
                    contestGO = Instantiate(m_row_future_prefab, this.transform.parent);
                else if (m_contestStatus == Contest.ContestStatus.Completed)
                    contestGO = Instantiate(m_row_completed_prefab, this.transform.parent);
                else
                    contestGO = Instantiate(m_row_ongoing_prefab, this.transform.parent);

                ContestRowView contestRow = contestGO.GetComponent<ContestRowView>();
                m_contest_rows.Add(contestRow);
                contestRow.SetData(contest);
            }
        }

        public void btn_ExpandOrShrink()
        {
            m_is_expanded = !m_is_expanded;

            if (m_title_no_contests == null)
            {
                //showing / not showing the titles
                m_title_players.gameObject.SetActive(m_is_expanded);
                m_title_rank.gameObject.SetActive(m_is_expanded);
                m_title_prize_pool.gameObject.SetActive(m_is_expanded);
                m_title_time.gameObject.SetActive(m_is_expanded);
            }
            else
            {
                m_title_players.gameObject.SetActive(false);
                m_title_rank.gameObject.SetActive(false);
                m_title_prize_pool.gameObject.SetActive(false);
                m_title_time.gameObject.SetActive(false);
                m_title_no_contests.SetActive(m_is_expanded);
            }

            if (m_is_expanded)
            {
                m_group_name.color = Color.white;
                m_arrow_down.SetActive(m_is_expanded);
                m_arrow_right.SetActive(!m_is_expanded);
            }
            else
            {
                Color color;
                if (ColorUtility.TryParseHtmlString("#999999", out color))
                { m_group_name.color = color; }

                m_arrow_down.SetActive(m_is_expanded);
                m_arrow_right.SetActive(!m_is_expanded);
            }

            foreach (ContestRowView row in m_contest_rows)
            {
                row.gameObject.SetActive(m_is_expanded);
                if (m_contestStatus != Contest.ContestStatus.Completed && m_is_expanded)
                {
                    row.RefreshData();
                }
            }
        }

    }
}