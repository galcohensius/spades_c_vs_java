﻿namespace cardGames.views
{
    public class ContestRankView : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField] TMP_Text m_rank_text;

        int m_prev_rank = 0;

        public void SetData(int rank)
        {
            if (rank < m_prev_rank)
            {
                // green arrow
                m_rank_text.text = "RANK: " + rank.ToString() + "<sprite=0>";
            }
            else if (rank > m_prev_rank && rank != 1)
            {
                // red arrow
                m_rank_text.text = "RANK: " + rank.ToString() + "<sprite=1>";
            }
            else
            {
                // no arrow / no change
                m_rank_text.text = "RANK: " + rank.ToString();
            }

            m_prev_rank = rank;
        }
    }
}