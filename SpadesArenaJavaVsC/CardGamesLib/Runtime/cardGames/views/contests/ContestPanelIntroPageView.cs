﻿using cardGames.models;
using cardGames.models.contests;
using System;

namespace cardGames.views
{
    public class ContestPanelIntroPageView : MonoBehaviour
    {
        #region Private Members
        [SerializeField] private TMP_Text m_text;
        [SerializeField] private MAvatarView m_avatar;

        Action m_info_button_clicked;
        Action m_paytable_button_clicked;
        #endregion

        public void SetData(Contest contest)
        {
            m_avatar.SetAvatarModel(ModelManager.Instance.GetUser().GetMAvatar());
            if (contest.LoseFactor == 0)
                m_text.text = "win 1 game to enter";
            else
                m_text.text = "play 1 game to enter";
        }

        public void SetButtons(Action info_button_clicked, Action paytable_button_clicked)
        {
            m_info_button_clicked = info_button_clicked;
            m_paytable_button_clicked = paytable_button_clicked;
        }

        public void Info_Button_Clicked()
        {
            if (m_info_button_clicked != null)
                m_info_button_clicked();
        }

        public void Paytable_Button_Clicked()
        {
            if (m_paytable_button_clicked != null)
                m_paytable_button_clicked();
        }
    }
}


