﻿using cardGames.controllers;
using common.views;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.views.overlays
{
    public abstract class CometOverlay : OverlayBase
    {
        const float COMET_TRAIL_DELAY = 0.5f;
        int m_num_comets = 0;
        int m_comet_reach_end_counter = 0;
        Action m_comets_flight_done = null;
        Action m_comets_flight_done2 = null;

        public Action Comets_flight_done2 { get => m_comets_flight_done2; set => m_comets_flight_done2 = value; }

        /// <summary>
        /// Flies the comets.
        /// Path points - are the mid points on the flight path - the start and the end are transfered to the function as game objects.
        /// the middle points are calculated relative to the screen size were (-1,-1) is the bottom left corner and (1,1) is top right
        /// you can add as many in between points as you like.
        /// </summary>
        public void FlyComets(GameObject comet_prefab, int num_objects, GameObject start_object, GameObject target_object, float initial_delay,
            float between_items_delay, float flight_time, BalanceCoinTextbox text_object = null, int amount = 0, Animator animation_controller = null,
            float comet_scale_factor = 1, float comet_scale_timing = 0, float comet_scale_delay = 0, Action CometsFlightDone = null, params Vector2[] path_points)
        {
            m_comets_flight_done = CometsFlightDone;

            Vector3 target_pos = target_object.transform.position;

            Vector3 start_pos = start_object.transform.position;

            List<Vector3> path = new List<Vector3>();

            path.Add(start_pos);

            Vector2 size = GetLandscapeSize();
            float scale = GetLandscapeScale();

            if (path_points != null)
            {
                foreach (Vector2 point in path_points)
                {
                    //path.Add (new Vector3 (start_pos.x + (target_pos.x - start_pos.x) * point.x, start_pos.y + (target_pos.y - start_pos.y) * point.y, 90));
                    path.Add(new Vector3(scale * point.x * size.x / 2, scale * point.y * size.y / 2, 90));

                }
            }

            path.Add(target_pos);

            StartCoroutine(StartAnim(comet_prefab, num_objects, path, initial_delay, between_items_delay, flight_time, text_object, amount,
                animation_controller, comet_scale_factor, comet_scale_timing, comet_scale_delay));

        }

        protected abstract Vector2 GetLandscapeSize();
        protected abstract float GetLandscapeScale();

        /// <summary>
        /// Flies the comets without any scale
        /// </summary>
        public void FlyComets(GameObject comet_prefab, int num_objects, GameObject start_object, GameObject target_object, float initial_delay,
            float between_items_delay, float flight_time, BalanceCoinTextbox text_object = null, int amount = 0, Animator animation_controller = null, Action CometsFlightDone = null,
            params Vector2[] path_points)
        {


            FlyComets(comet_prefab, num_objects, start_object, target_object, initial_delay,
                between_items_delay, flight_time, text_object, amount, animation_controller, 1, 0, 0, CometsFlightDone,
                path_points);

        }

        private IEnumerator StartAnim(GameObject comet_prefab, int num_objects, List<Vector3> path, float initial_delay, float delay,
            float flight_time, BalanceCoinTextbox text_object, int amount = 0, Animator animation_controller = null,
            float comet_scale_factor = 1, float comet_scale_timing = 0, float comet_scale_delay = 0)
        {

            

            m_num_comets = num_objects;

            yield return new WaitForSeconds(initial_delay);
            CardGamesSoundsController.Instance.Coins_Trail();

            if (text_object != null)
            {
                float total_time = (num_objects - 1) * delay;
                StartCoroutine(UpdateText(text_object, amount, total_time, flight_time));
            }

            for (int i = 0; i < num_objects; i++)
            {

                GameObject comet = Instantiate(comet_prefab);
                comet.transform.SetParent(this.transform);
                comet.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                comet.GetComponent<RectTransform>().position = path[0];

                Hashtable someVals = new Hashtable();
                someVals.Add("anim", animation_controller);
                someVals.Add("object", comet);


                iTween.MoveTo(comet, iTween.Hash("path", path.ToArray(), "islocal", false, "easeType", "easeInQuad", "time", flight_time, "onComplete", "CometReachedEnd", "onCompleteTarget", this.gameObject, "oncompleteparams", someVals));

                if (comet_scale_factor != 1)
                {
                    GameObject comet_coin = comet.transform.Find("Comet").gameObject;

                    iTween.ScaleTo(comet_coin, iTween.Hash("x", comet_scale_factor, "y", comet_scale_factor, "time", comet_scale_timing / 2, "easeType", iTween.EaseType.linear, "delay", comet_scale_delay));
                    iTween.ScaleTo(comet_coin, iTween.Hash("x", 1, "y", 1, "time", comet_scale_timing / 2, "delay", comet_scale_timing / 2 + comet_scale_delay));
                }
                yield return new WaitForSeconds(delay);
            }
        }

        private IEnumerator UpdateText(BalanceCoinTextbox text_object, int amount, float total_time, float initial_delay)
        {
            yield return new WaitForSeconds(initial_delay);

            //float start_time = Time.realtimeSinceStartup;
            float update_rate = 0.1f;

            int iterations = Convert.ToInt32(total_time / update_rate);

            iterations = Mathf.Max(1, iterations);

            int amount_per_iteration = amount / iterations;

            int remainder = amount - amount_per_iteration * iterations;

            text_object.Amount += remainder;

            for (int i = 0; i < iterations; i++)
            {
                text_object.Amount += amount_per_iteration;
                yield return new WaitForSeconds(update_rate);
            }
            //TODO: Hanlde pause in web GL - using real time since.... .
        }

        private void CometReachedEnd(object values)
        {

            Hashtable hash = (Hashtable)values;

            Animator anim;
            GameObject comet;

            comet = (GameObject)hash["object"];
            anim = (Animator)hash["anim"];

            if (anim != null)
            {
                CardGamesSoundsController.Instance.Coins_Balance();
                anim.SetTrigger("play");
            }
            comet.transform.GetChild(0).gameObject.SetActive(false);
            comet.transform.GetChild(1).GetComponent<ParticleSystem>().enableEmission = false;

            Destroy(comet);

            m_comet_reach_end_counter++;

            //Debug.Log ("Comet Overlay - comet reached end " +  m_comet_reach_end_counter);

            if (m_comet_reach_end_counter == m_num_comets)
                StartCoroutine(FullHideComet());

        }

        IEnumerator FullHideComet()
        {
            //Debug.Log ("Comet Overlay - Full hide comet called");

            yield return new WaitForSeconds(COMET_TRAIL_DELAY);
            OverlaysManager.Instance.HideOverlay(this);

            if (m_comets_flight_done != null)
                m_comets_flight_done();

            if (m_comets_flight_done2 != null)
                m_comets_flight_done2();

        }

    }
}
