﻿using System.Collections.Generic;

namespace cardGames.views.social
{

	public class FBFriendItemGroupView : MonoBehaviour {

		[SerializeField]List<FBFriendItemView> 			m_friend_items = new List<FBFriendItemView> ();



		public List<FBFriendItemView> Friend_items {
			get {
				return m_friend_items;
			}
			set {
				m_friend_items = value;
			}
		}

		public void ChangeSelectionInidcationToChilds()
		{
			foreach (FBFriendItemView item in m_friend_items)
			{
				if(item!=null && item.gameObject.activeSelf)
					item.ChangeSelectionIndication ();
			}
		}

	}
}