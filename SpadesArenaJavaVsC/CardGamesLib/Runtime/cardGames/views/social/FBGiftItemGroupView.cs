﻿using System.Collections.Generic;

namespace cardGames.views.social
{

    public class FBGiftItemGroupView : MonoBehaviour {

		[SerializeField]List<FBGiftItemView> 			m_friend_items = new List<FBGiftItemView> ();



		public List<FBGiftItemView> Friend_items {
			get {
				return m_friend_items;
			}
			set {
				m_friend_items = value;
			}
		}
	}
}