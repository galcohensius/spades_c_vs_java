﻿using System;
using common.facebook.models;
using cardGames.models;
using cardGames.controllers;

namespace cardGames.views.social
{

    public class FBListFilterView : MonoBehaviour
    {

        Action<string> m_filter_func = null;
        [SerializeField] GameObject m_filter_object = null;
        [SerializeField] TMP_Text m_button_caption = null;
        Action<Action> m_button_action = null;
        Action<bool, string> m_select_all_function = null;

        [SerializeField] TMP_Text m_empty_message = null;
        [SerializeField] GameObject m_empty_message_holder = null;
        [SerializeField] GameObject m_select_all_check_object = null;

        [SerializeField] GameObject m_invite_on_empty_button_object = null;

        [SerializeField] GameObject m_button_spades_holder = null;

        string m_curr_filter_text = "";


        public void InitMyFilter(string button_caption, Action<Action> button_action = null, Action<string> Filter_Func = null, Action<bool, string> SelectAll_Function = null, string empty_message = null, bool show_invite_button = false)
        {
            if (Filter_Func != null)
                m_filter_func = Filter_Func;
            else
            {
                m_filter_object.SetActive(false);
                m_button_caption.transform.parent.parent.GetComponent<RectTransform>().localPosition = new Vector3(0, m_button_caption.transform.parent.parent.GetComponent<RectTransform>().localPosition.y, 0);
            }

            m_select_all_function = SelectAll_Function;

            // NOTE: THIS IS A PATCH - EUGENE SHOULD FIX THAT IN THE SOCIAL 
            if (m_button_caption != null)
            {
                if (button_caption != null || button_caption == "")
                    m_button_caption.text = button_caption;
                else
                    m_button_caption.transform.parent.gameObject.SetActive(false);

                m_empty_message.text = empty_message;

                m_button_action = button_action;

                if (SelectAll_Function == null)
                    m_select_all_check_object.SetActive(false);

                m_invite_on_empty_button_object.SetActive(show_invite_button);
            }

        }

        public void ShowHideActionButton(bool show)
        {
            m_button_caption.transform.parent.gameObject.SetActive(show);
        }

        public void ShowEmptyMessage(bool show)
        {
            m_empty_message_holder.SetActive(show);

            if (m_select_all_function == null)
            {
                m_filter_object.SetActive(false);
                m_select_all_check_object.SetActive(false);
            }
            else
            {
                m_filter_object.SetActive(!show);
                m_select_all_check_object.SetActive(!show);
            }

        }

        public void NameTextBoxValueChanged(string text)//accessed from inupt box (like a button)
        {
            m_filter_func(text);
            m_curr_filter_text = text;
        }

        public void BT_Invite_Clicked()
        {
            SocialController.Instance.Social_popup.InviteClicked();
        }

        public void BT_Clicked()
        {
            if (m_button_action != null)
            {
                m_button_spades_holder.SetActive(true);
                m_button_caption.gameObject.SetActive(false);

                m_button_action(() =>
                {
                    //button can go back to normal state from spades holder 
                    m_button_spades_holder.SetActive(false);
                    m_button_caption.gameObject.SetActive(true);
                });
            }
        }

        public void ChangeSelectAllIndication(bool selected)
        {
            m_select_all_check_object.GetComponent<Toggle>().isOn = selected;
        }

        public void BT_SelectAllClicked(bool selected)
        {
            m_select_all_check_object.GetComponent<Toggle>().isOn = selected;
            if (selected)
            {
                m_select_all_function(true, m_curr_filter_text);
            }
            else
            {
                bool all_selected = true;
                foreach (FacebookData item in ModelManager.Instance.Social_model.AppFriends)
                {
                    if (!item.Selected)
                    {
                        all_selected = false;
                        continue;
                    }

                }

                if (all_selected && m_select_all_function != null)
                    m_select_all_function(false, m_curr_filter_text);
            }
        }

        public TMP_Text Button_caption
        {
            get
            {
                return m_button_caption;
            }
        }
    }
}