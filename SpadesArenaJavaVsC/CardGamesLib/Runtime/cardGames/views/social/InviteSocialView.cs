﻿using cardGames.controllers;
using cardGames.models;
using cardGames.views.popups;
using common.controllers.social.inviter;

namespace cardGames.views.social
{
    public class InviteSocialView : MonoBehaviour
    {

        [SerializeField] SelectionDisplayHolder m_selection_display = null;
        [SerializeField] InviterBase[] m_inviters = null;

        public void Init()
        {
            m_selection_display.Init();

            foreach (InviterBase inviter in m_inviters)
            {
                inviter.Init(ModelManager.Instance.GetUser().GetID(), SocialController.Instance.InviteTitle, SocialController.Instance.InviteBodyAndLink);
                inviter.InviterClick -= ShowWaitingOnClick;
                inviter.InviterClick += ShowWaitingOnClick;
            }
        }

        public void UnregisterFromEvents()
        {
            foreach (InviterBase inviter in m_inviters)
            {
                inviter.InviterClick -= ShowWaitingOnClick;
            }
        }

        private void ShowWaitingOnClick()
        {
            CardGamesPopupManager.Instance.ShowWaitingIndication(true, 3f);
        }

    }
}
