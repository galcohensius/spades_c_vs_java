using cardGames.controllers;
using common.facebook;
using common.facebook.models;
using common.utils;
using System;
using System.Collections;

namespace cardGames.views.social
{

    public class FBFriendItemView : MonoBehaviour {

		[SerializeField]TMP_Text	m_name=null;

		[SerializeField]Image		m_image=null;
		[SerializeField]Image		m_selection_image=null;

		FacebookData				m_fb_data=null;
		private Action<FacebookData> m_on_item_change = null;

		public void SetDataView(FacebookData fb_data,Action<FacebookData> OnItemSelectionChange = null)
		{
			m_fb_data = fb_data;
			m_name.text = fb_data.FullName;
            TMPBidiHelper.MakeRTL(m_name);

			if (fb_data.Image_url == null)	//this means the image link is in the param called id - and needs to be retrived 
			{
				FacebookController.Instance.GetUserPicture (fb_data.User_id, 300, 300,false,(Sprite user_image) => {
					if(m_image!=null)
					{
						m_image.sprite = user_image;
						m_image.color = Color.white;
					}

				});
			}
			else
			{
				FacebookController.Instance.GetInviteFriendPicture (fb_data.User_id ,fb_data.Image_url, (Sprite user_image) => {

					if(m_image!=null)
					{
						m_image.sprite = user_image;
						m_image.color = Color.white;
					}

				});
			}

			m_on_item_change = OnItemSelectionChange;
			ChangeSelectionIndication ();
		}

		public IEnumerator RetrieveImageFromUrl(Image target, string url)
		{

			Debug.Log ("Retrieving image from url: " + url);

			Texture2D tex;
			tex = new Texture2D(50, 50, TextureFormat.DXT1, false);
			WWW www = new WWW(url);
			yield return www;
			www.LoadImageIntoTexture(tex);

			Debug.Log ("Image retrieved from url: " + url);
			target.sprite = Sprite.Create (www.texture, new Rect (0, 0, 50, 50), new Vector2 (0.5f, 0.5f));

		}

		public void BT_Clicked()
		{
			m_fb_data.Selected=!m_fb_data.Selected;
            SocialController.Instance.CheckSelectAllIndication();
            ChangeSelectionIndication ();
		}

		public void ChangeSelectionIndication()
		{
            if (m_fb_data.Selected)
			{
				m_selection_image.sprite = SocialController.Instance.Social_selected;
				m_fb_data.Selected = true;
			}
			else
			{
				m_selection_image.sprite = SocialController.Instance.Social_un_selected;
				m_fb_data.Selected = false;
			}
            

            if (m_on_item_change != null)
				m_on_item_change (m_fb_data);
			
			m_selection_image.SetNativeSize ();

            //this is to disable\enable the send gift buttons when nothing is selected
            SocialController.Instance.UpdateSendGiftButton();
        }

		public FacebookData Fb_data {
			get {
				return m_fb_data;
			}
		}
	}
}
