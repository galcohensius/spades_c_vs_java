﻿using common.utils;
using System;

namespace cardGames.views
{
    public class ArenaStageCardView : MonoBehaviour
    {


        [SerializeField] Image m_bg = null;
        [SerializeField] TMP_Text m_stage_number = null;
        [SerializeField] TMP_Text m_prize_amount = null;
        [SerializeField] GameObject m_highlight = null;

        Sprite m_complete_sprite = null;
        Action m_flip_complete = null;
        float m_org_z_angle = 0;
        Vector3 m_org_position = new Vector3();
        float m_flip_angle = 0;

        public void SetData(int stage_num, float flip_angle, Color color, int prize = 0, Sprite prize_sprite = null, bool final_stage = false, Sprite final_sprite = null)
        {
            m_stage_number.text = stage_num.ToString();
            m_stage_number.color = color;
            m_flip_angle = flip_angle;

            if (final_stage)
            {
                m_bg.sprite = final_sprite;
                m_stage_number.color = new Color(153f / 255f, 101f / 255f, 11f / 255f);
            }
            else
                m_bg.sprite = prize_sprite;

            if (prize > 0)
            {
                m_prize_amount.text = FormatUtils.FormatPrice(prize);
                m_prize_amount.gameObject.SetActive(true);
            }

        }

        public void SetComplete(Sprite sprite, float flipped = 1)
        {
            m_stage_number.gameObject.SetActive(false);
            m_prize_amount.gameObject.SetActive(false);
            m_bg.sprite = sprite;

            m_bg.GetComponent<RectTransform>().localScale = new Vector3(flipped, 1f, 1f);
        }

        public void HighLight(bool show)
        {
            m_highlight.SetActive(show);

            RectTransform rect_transform = GetComponent<RectTransform>();

            m_org_position = rect_transform.localPosition;

            if (show)
                iTween.MoveTo(gameObject, iTween.Hash("position", rect_transform.localPosition + rect_transform.up.normalized * 70f, "islocal", true, "easeType", "easeInQuad", "time", .2));
        }



        public void FlipToComplete(Sprite complete_sprite, Action flip_complete)
        {
            m_complete_sprite = complete_sprite;
            m_flip_complete = flip_complete;
            m_org_z_angle = GetComponent<RectTransform>().localEulerAngles.z;

            iTween.RotateTo(gameObject, iTween.Hash("name", "RotateCard", "y", m_flip_angle, "z", 0, "easeType", "linear", "time", .25, "onComplete", "Rotate2", "onCompleteTarget", this.gameObject));

        }

        void Rotate2()
        {
            SetComplete(m_complete_sprite, -1f);
            iTween.RotateTo(gameObject, iTween.Hash("name", "RotateCard", "y", 180, "z", -m_org_z_angle, "easeType", "linear", "time", .25, "onComplete", "PostRotate", "onCompleteTarget", this.gameObject));
        }

        void PostRotate()
        {
            m_highlight.SetActive(false);
            iTween.MoveTo(gameObject, iTween.Hash("position", m_org_position, "islocal", true, "easeType", "easeInQuad", "time", .2));

            if (m_flip_complete != null)
                m_flip_complete();

        }
    }
}
