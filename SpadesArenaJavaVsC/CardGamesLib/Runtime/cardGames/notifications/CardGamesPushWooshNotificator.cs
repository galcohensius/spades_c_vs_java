﻿using cardGames.comm;
using cardGames.controllers;
using cardGames.models;
using common.controllers;
using common.notification;
using System.Collections;

namespace cardGames.notifications
{

    public class CardGamesPushWooshNotificator : PushWooshNotificator
	{
		

		public static CardGamesPushWooshNotificator Instance;

		private void Awake()
        {
            Instance = this;
        }

		public void SetUserTags(User user)
		{
			// Tags
			Pushwoosh.Instance.SetUserId(user.GetID().ToString());

            Pushwoosh.Instance.SetStringTag("Name", user.GetMAvatar().NickName ?? "");
			LoggerController.Instance.Log("CardGamesPushWooshNotificator: Tag 'Name': " + user.GetMAvatar().NickName);

			Pushwoosh.Instance.SetIntTag("uvs_level", user.Uvs_level);
			LoggerController.Instance.Log("CardGamesPushWooshNotificator: Tag 'uvs_level': " + user.Uvs_level);

			Pushwoosh.Instance.SetIntTag("activity_level", user.Activity_level);
			LoggerController.Instance.Log("CardGamesPushWooshNotificator: Tag 'activity_level': " + user.Activity_level);

			Pushwoosh.Instance.SetIntTag("player_level", user.GetLevel());
			LoggerController.Instance.Log("CardGamesPushWooshNotificator: Tag 'player_level': " + user.GetLevel());

		}

		public void SavePushToken()
		{
#if !UNITY_WEBGL && !UNITY_EDITOR
			StartCoroutine(SavePushTokenCoroutine());
#endif
		}

		private IEnumerator SavePushTokenCoroutine()
		{

            int checkCount = 0;

			while (checkCount < 30)
			{
				string token = Pushwoosh.Instance.PushToken;

				if (!string.IsNullOrEmpty(token) && token!= "Unsupported platform")
				{

					LoggerController.Instance.Log("Got pushwoosh token: " + token);

					// Check if this token is new, if so, send to server
					if (CardGamesStateController.Instance.GetPushToken() != token)
					{
						CardGamesCommManager.Instance.SavePushToken(token, CardGamesStateController.Instance.GetDeviceId(), (bool success) =>
						{
                            // Save the push token after server registration
                            CardGamesStateController.Instance.SetPushToken(token);
						});
					}
					break;
				}

				checkCount++;

				yield return new WaitForSeconds(1);

			}
		}

  

	}

}
