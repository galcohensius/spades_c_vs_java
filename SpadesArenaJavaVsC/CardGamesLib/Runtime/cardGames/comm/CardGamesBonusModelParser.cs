﻿using System.Collections.Generic;
using common.models;
using cardGames.controllers;
using cardGames.models;
using System;

namespace cardGames.comm
{
    /// <summary>
    /// Model parser for bonus related objects
    /// </summary>
    public abstract class CardGamesBonusModelParser
    {
        public Bonus ParseBonus(JSONNode jsonNode, List<World> worlds)
        {
            Bonus result = null;

            Bonus.BonusTypes type = (Bonus.BonusTypes)jsonNode["bTe"].AsInt;

            int sub_type = jsonNode["bSTe"].AsInt;

            switch (type)
            {
                case Bonus.BonusTypes.Hourly:
                    result = ParseHourlyBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.Daily:
                    result = ParseDailyBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.Gift:
                    result = ParseGiftBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.Leaderboard:
                    result = ParseLeaderboardBonus(jsonNode, sub_type);
                    break;
                case Bonus.BonusTypes.GeneralBonus:
                    result = ParseGeneralBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.Invite:
                    result = ParseInviteBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.RateUs:
                    result = ParseRateUsBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.FacebookConnect:
                    result = ParseFacebookBonus(jsonNode);
                    break;
                case Bonus.BonusTypes.RewardedVideoBonus:
                case Bonus.BonusTypes.RewardedHourlyVideoBonus:
                case Bonus.BonusTypes.RewardedSlotVideoBonus:
                case Bonus.BonusTypes.RewardedVideoLevelUp:
                case Bonus.BonusTypes.RewardedVideoLoseInARow:
                    result = ParseRewardedVideoBonus(jsonNode, type);
                    break;
                default:
                    return null;
            }

            // Common bonus data
            JSONArray coinsArr = jsonNode["cGs"].AsArray;

            if (coinsArr != null)
            {
                foreach (JSONNode coin in coinsArr)
                {
                    result.Coins.Add(coin.AsInt);
                }
            }

            result.IsEntitled = jsonNode["iEd"].AsInt == 1;
            return result;
        }

        private Bonus ParseHourlyBonus(JSONNode jsonNode)
        {
            HourlyBonus result = new HourlyBonus();

            result.Interval = jsonNode["nSIl"].AsInt;

            return result;
        }

        private Bonus ParseDailyBonus(JSONNode jsonNode)
        {
            DailyBonus result = new DailyBonus();

            result.Interval = jsonNode["nSIl"].AsInt;
            result.Steps = jsonNode["nSe"].AsInt;
            result.NextStep = jsonNode["nSp"].AsInt;

            JSONArray payTable = jsonNode["cGs"].AsArray;
            foreach (JSONNode table_item in payTable)
            {
                result.Pay_table.Add(table_item.AsInt);
            }

            /*
			JSONArray extraConf = jsonNode ["eCf"].AsArray;
			foreach (JSONNode conf in extraConf) {
				result.ConsecutiveDaysFactor.Add (conf.AsFloat);
			}*/

            JSONNode extra_conf = jsonNode["eCf"];

            JSONArray day_bonus = extra_conf["cDBs"].AsArray;
            foreach (JSONNode conf in day_bonus)
            {
                result.ConsecutiveDaysFactor.Add(conf.AsInt);
            }

            JSONArray friend_bonus = extra_conf["fBs"].AsArray;
            foreach (JSONNode conf in friend_bonus)
            {
                result.FriendsFactor.Add(conf.AsInt);
            }

            return result;
        }

        private Bonus ParseGiftBonus(JSONNode jsonNode)
        {
            GiftBonus result = new GiftBonus();

            return result;
        }

        private Bonus ParseCollectibleBonus(JSONNode jsonNode, Bonus.BonusTypes type, int sub_type)
        {
            CollectibleBonus result = new CollectibleBonus(type, sub_type);

            return result;
        }

        private Bonus ParseLeaderboardBonus(JSONNode jsonNode, int sub_type)
        {
            LeaderboardBonus result = new LeaderboardBonus(sub_type);

            JSONNode extra_conf = jsonNode["eCf"];

            JSONArray userRanks = extra_conf["uRk"].AsArray;

            foreach (JSONNode userRank in userRanks)
            {
                result.Places.Add(userRank.Value);
                result.LowestRewardPlace = parseLowestPlace(userRank);
            }

            return result;
        }

        private int parseLowestPlace(string place)
        {
            string tmp = place;

            tmp = tmp.Replace("\"", "");
            string[] arr = tmp.Split('-');

            if (arr.Length == 2)
            {
                return Convert.ToInt32(arr[1]);
            }
            else
            {
                return 0;
            }
        }

        private Bonus ParseGeneralBonus(JSONNode jsonNode)
        {
            GeneralBonus generalBonus = new GeneralBonus();
            generalBonus.Coins.Clear();
            generalBonus.Token = jsonNode["bTn"];
            generalBonus.Coins.Add(jsonNode["cGs"].AsInt);
            return generalBonus;
        }

        private Bonus ParseInviteBonus(JSONNode jsonNode)
        {
            InviteBonus result = new InviteBonus();
            JSONArray invitersArr = jsonNode["eUD"].AsArray;

            for (int i = 0; i < invitersArr.Count; i++)
            {
                User user = CreateUser();
                user.SetID(invitersArr[i]["uky"].AsInt);

                JSONNode avatarStr = invitersArr[i]["avr"];

                MAvatarModel avatarModel =  MAvatarController.Instance.DeserializeAvatarData(avatarStr);
                user.SetMAvatar(avatarModel);

                result.Inviters.Add(user);
            }

            return result;
        }

        protected abstract User CreateUser();

        private Bonus ParseRateUsBonus(JSONNode jsonNode)
        {

            RateUsBonus result = new RateUsBonus();

            JSONArray payTable = jsonNode["cGs"].AsArray;
            foreach (JSONNode table_item in payTable)
            {
                result.Coins.Add(table_item.AsInt);
            }

            return result;
        }

        private Bonus ParseFacebookBonus(JSONNode jsonNode)
        {
            FacebookBonus result = new FacebookBonus();

            return result;
        }

        private Bonus ParseRewardedVideoBonus(JSONNode jsonNode, Bonus.BonusTypes bonusType)
        {
            RewardedVideoBonus result = new RewardedVideoBonus(bonusType);

            return result;
        }

    }
}