﻿using cardGames.commands;
using common.comm;
using common.commands;
using common.controllers;
using common.utils;
using System;
using System.Collections;

namespace cardGames.comm
{
    public abstract class GameServerManager : MonoBehaviour
    {
        protected Connector m_connector;
        protected CommandsManager m_commands_manager;
        [SerializeField] protected string m_host = null;
        [SerializeField] protected string m_hostQA1Mode = null;
        [SerializeField] protected string m_hostQA2Mode = null;
        [SerializeField] protected string m_hostDevMode = null;
        [Tooltip("alternative port for development. DEV MODE only.")]
        [SerializeField] bool m_useDevPort = true;
        [Tooltip("Dev only port - toggle on to use")]
        [SerializeField] int m_dev_port = 3106;
        [Tooltip("QA, Dev and Prod port. Editor and builds")]
        [SerializeField] protected int m_port = 0;
        [SerializeField] protected int m_portWebSocket = 0;

        public bool showRawMessage = false;

        int m_echo_sent_time = 0;

        const float SLOW_THRESH = 3f;
        const float DISCONNECT_THRESH = 6f;
        public const int NUM_CONNECT_ATTEMPS = 3;
        public const float ATTEMPS_TIMEOUT = 1f;


        protected int m_connect_attempts = 0;
        protected float m_last_communication = 0;

        protected bool m_connected = false;
        protected MMessage echoCommand;
        protected Coroutine m_check_connection_coroutine;

        public Action SlowConnection_Event;
        public Action<bool> Socket_Disconnected_Event; // the bool param - true = trying to reconnect automatically - false = cannot reconncet
        public Action Socket_Reconnected_Event;



        public static GameServerManager Instance;


        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        void Start()
        {            
            m_commands_manager = new CommandsManager();
            RegisterCommands();

            echoCommand = CommandsBuilder.Instance.BuildEchoCommand("");

            CreateConnectorsByEnvIndex(StateController.Instance.GetQAEnvironment());
            StateController.Instance.OnQAEnvChanged += CreateConnectorsByEnvIndex;

        }

        private void CreateConnectorsByEnvIndex(int envIndex)
        {
            string host = m_host;
#if QA_MODE
            switch (envIndex)
            {
                case 2:
                    host = m_hostQA2Mode;
                    break;
                default:
                    host = m_hostQA1Mode;
                    break;
            }
#elif DEV_MODE
            host = m_hostDevMode;
#endif


#if UNITY_WEBGL && !UNITY_EDITOR
            m_connector = new WSConnector(host,m_portWebSocket,m_commands_manager);
            GameObject delegateGO = new GameObject("GameServerManagerDelegate");
            DontDestroyOnLoad(delegateGO);
            WSConnector.WSDelegate ws_delegate = delegateGO.AddComponent<WSConnector.WSDelegate>();
            ws_delegate.Connector = (WSConnector)m_connector;
#else
#if DEV_MODE
            if (m_useDevPort)
                m_connector = new SocketConnector(host, m_dev_port, m_commands_manager);
            else
                m_connector = new SocketConnector(host, m_port, m_commands_manager);
#else
            m_connector = new SocketConnectorV2(host, m_port, m_commands_manager);
#endif
#endif

            m_connector.Client_connected += () =>
            {
                m_connected = true;
                m_check_connection_coroutine = StartCoroutine(CheckConnection());
            };

            m_connector.showRawMessage = showRawMessage;
        }

        private void Update()
        {
            m_connector?.OnUpdate();
        }

        private IEnumerator CheckConnection()
        {
            Debug.Log("CheckConnection, m_echo_sent_time=" + m_echo_sent_time + "---------------------------------------------------------------------");
            do
            {
                if (m_echo_sent_time == 0)
                {
                    m_echo_sent_time = (int)Time.realtimeSinceStartup;
                    Send(echoCommand);
                }
                else
                {
                    float delta_time = Time.realtimeSinceStartup - m_echo_sent_time;
                    Debug.Log("Delta time: " + delta_time);
                    if (delta_time >= DISCONNECT_THRESH)
                    {
                        LoggerController.Instance.Log(LoggerController.Module.Comm, "Disconnect after " + DISCONNECT_THRESH + " seconds passed since last echo");
                        m_echo_sent_time = 0;
                        if (Socket_Disconnected_Event != null)
                            Socket_Disconnected_Event?.Invoke(true);
                        if (m_check_connection_coroutine != null)
                            StopCoroutine(m_check_connection_coroutine);
                        Disconnect();
                        TryReconnecting();
                        break;
                    }
                    else if (delta_time >= SLOW_THRESH)
                    {
                        if (SlowConnection_Event != null)
                            SlowConnection_Event();
                    }
                }
                yield return new WaitForSecondsRealtime(1f);
            } while (true);
        }


        private void ConnectionCheckAnswerReceived()
        {
            //answer back from the Server
            m_echo_sent_time = 0;
        }


        protected virtual void RegisterCommands()
        {
            Command connect_to_table_command = new ConnectToTableCommand();
            m_commands_manager.RegisterCommand(CommandsBuilder.CONNECT_TO_TABLE_CODE, connect_to_table_command);
            Command echo_command = new EchoCommand(ConnectionCheckAnswerReceived);
            m_commands_manager.RegisterCommand(CommandsBuilder.ECHO_CODE, echo_command);
            Command error_command = new ErrorCommand(OnErrorReceived);
            m_commands_manager.RegisterCommand(CommandsBuilder.INTERNAL_SERVER_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.MESSAGE_FORMAT_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.BAD_PARAMS_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.UNKNOWN_COMMAND_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.NOT_LOGGED_IN_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.BAD_FLOW_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.SERVER_SHUTDOWN_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.BANNED_USER_ERROR, error_command);
            m_commands_manager.RegisterCommand(CommandsBuilder.S2S_ERROR, error_command);
        }


        public virtual void Connect(Action<bool> ConnectCallback = null)
        {
            LoggerController.Instance.Log(LoggerController.Module.Comm,"GameServerManager connecting...");
#if UNITY_WEBGL
            if (m_connected)
#endif
                Disconnect(); // Make sure we are not connected with an old socket
            m_connector.Connect((success) =>
            {
                if (success)
                {
                    m_connect_attempts = 0;
                    RegisterToBackgroundEvents();

                    LoggerController.Instance.Log(LoggerController.Module.Comm,"GameServerManager connected successfully");
                }

                ConnectCallback?.Invoke(success);
            });
        }


        public virtual void Disconnect(bool keepBackgroundEvents = false)
        {
            if (!keepBackgroundEvents)
                UnregisterFromBackgroundEvents();

            if (m_check_connection_coroutine != null)
            {
                StopCoroutine(m_check_connection_coroutine);
                m_check_connection_coroutine = null;
                m_echo_sent_time = 0;
            }
#if UNITY_WEBGL
            if (m_connected)
#endif
                m_connector.Disconnect();
            m_connected = false;
            // Clear the commands queue
            m_commands_manager.ClearCommandsQueues();
        }


        public virtual void Send(MMessage message)
        {
            if (message.Code != CommandsBuilder.ECHO_CODE)
                m_last_communication = Time.realtimeSinceStartup;
            m_connector.Send(message);
        }


        private void OnErrorReceived(string errorCode)
        {
            LoggerController.Instance.LogFormat("Error received from server: {0}", errorCode);
            // Show error popup and do something
            PopupManager.Instance.ShowMessagePopup("Error from server: " + errorCode);
        }


        public void TryReconnecting()
        {
            m_connect_attempts++;
            LoggerController.Instance.Log(LoggerController.Module.Comm, "Game Server Manager trying to Connect. Attempt " + m_connect_attempts);
            Connect((bool success) =>
            {
                if (success)
                {
                    //we reconnected succesfully
                    Socket_Reconnected_Event?.Invoke();
                }
                else
                {
                    if (m_connect_attempts >= NUM_CONNECT_ATTEMPS)
                    {
                        Socket_Disconnected_Event?.Invoke(false);
                        m_connect_attempts = 0;
                    }
                    else
                    {
                        InvokingUtils.InvokeActionWithDelay(false, ATTEMPS_TIMEOUT, TryReconnecting, this);
                    }
                }
            });
        }


        private void RegisterToBackgroundEvents()
        {
            UnregisterFromBackgroundEvents();
            BackgroundController.Instance.OnBackFromBackground += OnBackFromBackground;
            BackgroundController.Instance.OnGoingToBackground += OnGoingToBackground;
        }

        private void UnregisterFromBackgroundEvents()
        {
            BackgroundController.Instance.OnBackFromBackground -= OnBackFromBackground;
            BackgroundController.Instance.OnGoingToBackground -= OnGoingToBackground;
        }


        private void OnGoingToBackground()
        {
            LoggerController.Instance.Log(LoggerController.Module.Comm, "GameServerManager going to background - Disconnecting socket");
            Disconnect(true);
        }

        private void OnBackFromBackground(bool sessionTimeout, TimeSpan backgroundTime)
        {
            if (!sessionTimeout)
            {
                LoggerController.Instance.Log(LoggerController.Module.Comm, "GameServerManager back from background - trying to reconnect socket");
                UnityMainThreadDispatcher.Instance.DelayedCall(1, TryReconnecting);
                //TryReconnecting();
            }
        }


        public bool IsConnected { get => m_connected; }
        
        public string Last_game_id { get; set; }
        public string Last_table_id { get; set; }

        public CommandsManager Commands_manager { get => m_commands_manager; set => m_commands_manager = value; }
    }
}
