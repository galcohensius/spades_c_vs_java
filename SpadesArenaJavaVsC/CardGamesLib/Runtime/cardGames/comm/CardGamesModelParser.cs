using cardGames.controllers;
using cardGames.models;
using cardGames.models.contests;
using common.comm;
using common.controllers;
using common.models;
using System;
using System.Collections.Generic;
using static cardGames.controllers.ContestsController;

namespace cardGames.comm
{
    public abstract class CardGamesModelParser : ModelParser
    {

        #region Piggy

        public UserPiggyData ParseUserPiggyDate(JSONNode jSONNode)
        {
            List<List<int>> capReminderList = new List<List<int>>();
            JSONArray capsArr = jSONNode["cRLt"].AsArray;
            if (capsArr != null)
            {
                foreach (JSONNode node in capsArr)
                {
                    List<int> cap = new List<int>();
                    JSONArray range = node.AsArray;
                    foreach (JSONNode limit in range)
                    {
                        cap.Add(limit.AsInt);
                    }
                    capReminderList.Add(cap);
                }
            }

            return new UserPiggyData(jSONNode["cBe"].AsInt, jSONNode["cMEDe"].AsInt, jSONNode["cMSDe"].AsInt, jSONNode["cMr"].AsFloat,
                jSONNode["cOt"].AsFloat, capReminderList, jSONNode["cap"].AsInt, jSONNode["eDe"].AsInt, jSONNode["ePId"].Value, 
                jSONNode["iCs"].AsInt,jSONNode["pId"].Value, jSONNode["pMDa"].Value, jSONNode["pPId"].AsInt, jSONNode["pSe"].AsInt);
        }

        //same data for single match and arena match
        public EndGamePiggyData ParseEndGamePiggyData(JSONNode jSONNode)
        {
            return new EndGamePiggyData(jSONNode["pCDa"].AsInt, jSONNode["pPId"].AsInt, jSONNode["pSe"].AsInt, jSONNode["pTCs"].AsInt);
        }

        #endregion

        

        public abstract User ParseUser(JSONNode jsonNode, List<World> worlds);

        protected void ParseUserCommonData(User user, JSONNode jsonNode, List<World> worlds)
        {
            int id = jsonNode["uky"].AsInt;
            if (id == 0)
                id = jsonNode["uId"].AsInt;
            user.SetID(id);

            user.SetCoins(jsonNode["cBe"].AsInt);


            user.SetLevel(jsonNode["lel"].AsInt, jsonNode["xTd"].AsInt, jsonNode["xp"].AsInt);

            user.SetCountry(jsonNode["coy"]);

            user.SetMatchHistoryFlag(jsonNode["sGh"].AsInt == 1);

            user.User_type = (CommonUser.UserType)jsonNode["uTe"].AsInt;

            //collectible dynamic data
            ParseUserRewards(jsonNode["uRs"], worlds);

            JSONNode avatarData = jsonNode["avr"];
            MAvatarModel model = MAvatarController.Instance.DeserializeAvatarData(avatarData);

            user.SetMAvatar(model);

            user.Uvs_level = jsonNode["uvs"].AsInt;
            user.Activity_level = jsonNode["aLl"].AsInt;
            user.Days_from_reg = jsonNode["rDs"].AsInt;
            user.Account_Status = (CommonUser.AccountStatus)jsonNode["aSs"].AsInt;
        }

        // This parse is called from the login and wil reward the player per arena
        public void ParseArenaPlayerRewardProgress(JSONArray rewardsNode)
        {
            foreach (JSONNode item in rewardsNode)
            {
                int id = item["aId"].AsInt; // arena ID
                int currentProgress = item["cPs"].AsInt; // Current Progress
                int totalAwarded = item["tAd"].AsInt; // Total Award
                int inventoryID = item["aIID"].AsInt; // Inventory ID

                foreach (World world in ModelManager.Instance.Worlds)
                {
                    // find the world with the wanted arena ID
                    if (world.GetArenaByID(id.ToString()) != null)
                    {
                        Arena arena = world.GetArenaByID(id.ToString());
                        // ? arena.Current_stage = 0; // make sure that
                        arena.Current_step = currentProgress; ;
                        // ? arena.Total_steps = 0;
                        arena.GoodsList.Coins.Add(totalAwarded);
                        arena.GoodsList.InventoryItems.Add(inventoryID.ToString());
                        // ? ModelManager.Instance.Inventory.AddItem(inventoryID.ToString());
                    }
                }
            }
        }

        public void ParseUserRewards(JSONNode rewardsNode, List<World> worlds)
        {

            JSONArray world_trophy_dynamic = rewardsNode["T"].AsArray;

            foreach (JSONNode node in world_trophy_dynamic)
            {

                string worldId = node["wId"];

                int total_collecctibles_awarded = node["tCAd"].AsInt;

                World world = worlds.Find(w => w.GetID() == worldId);
                world.Arena_Gem_Curr_Step = node["cSp"].AsInt;
                world.Arena_Gems_Won = total_collecctibles_awarded;
            }
        }

        public abstract World ParseWorld(JSONNode jsonNode, JSONNode collectiblesNode, JSONNode trophyNode);

        protected void ParseWorldCommonData(World world, JSONNode jsonNode, JSONNode collectiblesNode, JSONNode trophyNode)
        {

            world.SetId(jsonNode["wId"]);
            world.SetName(jsonNode["wNe"]);

            //total steps for trophy
            JSONNode world_trophy_node = jsonNode["wTs"];
            world.Arena_Gem_Total_Steps = world_trophy_node["thd"].AsInt;

        }

        public virtual Arena ParseArena(JSONNode jsonNode, World world)
        {
            Arena result = CreateArena();
            result.SetId(jsonNode["aId"]);
            result.SetName(jsonNode["aNe"]);
            result.SetLevel(jsonNode["mLl"].AsInt);

            int mc = jsonNode["bICs"].AsInt;


            result.SetBuyInMulti(mc);

            result.Curr_rebuy_num = jsonNode["uNrs"].AsInt;

            result.World = world;

            return result;
        }

        protected abstract Arena CreateArena();

        public Challenge ParseMissionUpdate(JSONNode jsonNode)
        {
            MissionController.ChallengeStatus ch_status = MissionController.ChallengeStatus.Locked;
            string status_str = jsonNode["sts"].Value;

            switch (status_str)
            {
                case "T":
                    ch_status = MissionController.ChallengeStatus.Init;
                    break;
                case "I":
                    ch_status = MissionController.ChallengeStatus.InProgress;
                    break;
                case "C":
                    ch_status = MissionController.ChallengeStatus.Completed;
                    break;
                case "S":
                    ch_status = MissionController.ChallengeStatus.Swapped;
                    break;

            }

            Challenge result = CreateChallenge(jsonNode["oId"].AsInt, jsonNode["uCPs"].AsInt, jsonNode["pre"].AsInt, ch_status);

            ModelManager.Instance.GetMission().Status = MissionController.MissionStatus.InProgress;
            if (jsonNode["mSs"].Value == "C")
            {
                ModelManager.Instance.GetMission().Status = MissionController.MissionStatus.Completed;
                MissionController.Instance.AddMissionToClaimMissionList(Convert.ToInt32(ModelManager.Instance.GetMission().Mission_id), ModelManager.Instance.GetMission().Prize.ToString());
            }

            return result;
        }

        protected abstract Challenge CreateChallenge(int order_id, int progress, int prize, MissionController.ChallengeStatus status);


        public Mission ParseMission(JSONNode jsonNode)
        {
            LoggerController.Instance.Log("SpadesMission Data: " + jsonNode.ToString());

            if (jsonNode == null || jsonNode.Count == 0)
                return null;

            //long num_sec = (long)jsonNode["eDe"].AsInt;
            long end_date_interval = (long)jsonNode["eDIl"].AsInt;

            //TimeSpan timeDiff = TimeSpan.FromSeconds(DateUtils.TotalSeconds((int)num_sec));

            TimeSpan timeDiff = TimeSpan.FromSeconds(end_date_interval);

            DateTime end_date = new DateTime(timeDiff.Ticks + DateTime.Now.Ticks);


            //int curr_challenge_index = jsonNode["cCId"].AsInt;
            string mission_status_string = jsonNode["mSs"].Value;

            //SpadesStateController.Instance.SetMissionId("5");

            MissionController.MissionStatus mission_status = MissionController.MissionStatus.Completed;

            if (mission_status_string == "C")
                mission_status = MissionController.MissionStatus.Completed;


            if (mission_status_string == "I")
                mission_status = MissionController.MissionStatus.InProgress;

            Mission result = CreateMission(end_date);

            result.Status = mission_status;

            result.Prize = jsonNode["pre"].AsInt;

            result.Mission_id = jsonNode["mId"].Value;

            JSONArray challengesNode = jsonNode["chs"].AsArray;

            foreach (JSONNode ch_node in challengesNode)
            {
                result.AddChallenge(ParseChallengePackage(ch_node));
            }


            return result;
        }

        protected abstract Mission CreateMission(DateTime dateTime);

        public virtual Challenge ParseChallengePackage(JSONNode jsonNode)
        {
            MissionController.ChallengeStatus ch_status = MissionController.ChallengeStatus.Locked;

            string status_str = jsonNode["cSs"].Value;

            switch (status_str)
            {
                case "L":
                    ch_status = MissionController.ChallengeStatus.Locked;
                    break;
                case "I":
                    ch_status = MissionController.ChallengeStatus.InProgress;
                    break;
                case "C":
                    ch_status = MissionController.ChallengeStatus.Completed;
                    break;
            }

            JSONArray thresholds = jsonNode["thd"].AsArray;

            int[] ch_thresholds = new int[thresholds.Count];
            int ch_counter = 0;
            foreach (JSONNode threshold in thresholds)
            {
                ch_thresholds[ch_counter] = threshold.AsInt;
                ch_counter++;
            }

            int bet = jsonNode["bet"].AsInt;

            string temp_bet_operator = jsonNode["bOr"].Value;

            cardGames.controllers.MissionController.BetOperator betOperator = MissionController.BetOperator.Exact;

            if (temp_bet_operator == "M")
            {
                betOperator = MissionController.BetOperator.Max;
            }
            else if (temp_bet_operator == "N")
            {
                betOperator = MissionController.BetOperator.Min;
            }

            int ch_order = jsonNode["oId"].AsInt;

            int prize = jsonNode["pre"].AsInt;

            int num_swapped = jsonNode["nSLt"].AsInt;

            int progress = jsonNode["uCPs"].AsInt;

            Challenge result = CreateChallenge(ch_status, ch_thresholds, ch_order, prize, num_swapped, bet, betOperator, progress);

            return result;
        }


        protected abstract Challenge CreateChallenge(MissionController.ChallengeStatus status, int[] thresholds, int order, int prize, int num_swapped, int bet, MissionController.BetOperator betOperator, int progress);

        public ActiveArena ParseActiveArena(JSONNode jsonNode, Arena arena)
        {
            ActiveArena result = CreateActiveArena(arena);

            result.SetBought(true);
            result.SetCurrStage(jsonNode["aSId"].AsInt);

            return result;
        }

        protected abstract ActiveArena CreateActiveArena(Arena arena);





        public ContestPaytable ParsePaytable(JSONNode node)
        {
            ContestPaytable result = new ContestPaytable();

            JSONObject jsonObj = node.AsObject;


            result.Id = jsonObj["pOId"];

            // Iterate the places and create a list of place ranges
            List<string> places = new List<string>();
            JSONArray placesJson = jsonObj["place"].AsArray;
            foreach (JSONNode place in placesJson)
            {
                places.Add(place.Value);
            }

            // Iterate the num players and build the columns
            JSONObject numPlayersJson = jsonObj["num_pl"].AsObject;
            foreach (KeyValuePair<string, JSONNode> pair in numPlayersJson)
            {
                string playerNumRange = pair.Key;
                JSONArray prizes = pair.Value.AsArray;

                ContestPaytableColumn col = new ContestPaytableColumn();
                (col.MinPlayers, col.MaxPlayers) = GetIntRangeFromString(playerNumRange);
                result.PayColumns.Add(col);

                // Iterate prizes
                for (int i = 0; i < prizes.Count; i++)
                {
                    ContestPaytableLine line = new ContestPaytableLine();
                    (line.MinRank, line.MaxRank) = GetIntRangeFromString(places[i]);
                    line.PrizePercentage = prizes[i].AsFloat;

                    col.PayLines.Add(line);
                }
                col.PayLines.FindLast(l => true).IsLast = true;

            }
            return result;
        }

        /// <summary>
        /// Gets a string with a range (14-29), and splits it into two numbers
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        private (int, int) GetIntRangeFromString(string range)
        {
            string[] tokens = range.Replace(" ", "").Split('-');
            if (tokens.Length == 1)
            {
                if (Int32.TryParse(tokens[0], out int val))
                    return (val, val);
            }
            else
            {
                int min = Int32.Parse(tokens[0]);
                int max = Int32.Parse(tokens[1]);
                return (min, max);
            }
            return (-1, -1);

        }
        public ClaimContestData ParseAward(JSONNode node)
        {
            int contestId = node["ctId"].AsInt;
            int rank = node["rak"].AsInt;
            int prize_won = node["pre"].AsInt;
            int score = node["sce"].AsInt;
            return new ClaimContestData(contestId, prize_won, rank);
        }

        public List<ClaimContestData> ParseAwards(JSONNode node)
        {
            JSONArray arrayOfAwards = node.AsArray;

            List<ClaimContestData> awards = new List<ClaimContestData>();
            foreach (JSONNode award in arrayOfAwards)
                awards.Add(ParseAward(award));

            return awards;
        }

        public List<ContestPlayer> ParseContestPlayers(JSONNode node, string key)
        {
            JSONArray arr = node[key].AsArray;
            List<ContestPlayer> cpList = new List<ContestPlayer>();
            if (arr != null)
            {
                foreach (JSONNode playerJSON in arr)
                {
                    ContestPlayer contestPlayer = ParseContestPlayer(playerJSON);
                    cpList.Add(contestPlayer);
                }
            }
            return cpList;
        }

        public ContestPlayer ParseContestPlayer(JSONNode node)
        {
            int userId = node["uky"].AsInt;
            int rank = node["rak"].AsInt;

            MAvatarModel avatar = MAvatarController.Instance.DeserializeAvatarData(node["avr"]);

            int points = node["pts"].AsInt;
            return new ContestPlayer(userId, rank, points, avatar, ModelManager.Instance.GetUser().GetID() == userId);
        }

        public List<ContestDynamicData> ParseDynamicDataList(JSONNode node)
        {
            List<ContestDynamicData> dynamicContestList = new List<ContestDynamicData>();

            JSONArray arr = node["data"].AsArray;

            foreach (JSONNode dynDataJson in arr)
            {
                int contestId = dynDataJson["ctId"].AsInt;
                int pool_prize = dynDataJson["pPl"].AsInt;
                int rank = dynDataJson["rak"].AsInt;
                int totalPlayers = dynDataJson["tPs"].AsInt;

                dynamicContestList.Add(new ContestDynamicData(contestId, pool_prize, rank, totalPlayers));
            }
            return dynamicContestList;
        }

        public virtual Contest ParseContest(JSONNode node, int index, Contest.ContestStatus contestStatus)
        {
            JSONNode jNode = node[index];
            Contest contest = new Contest();

            contest.Status = contestStatus;

            contest.Id = node["ctId"];

            // paytable ids set up for contest
            JSONArray listOfIds = node["pOTs"].AsArray;
            List<int> paytable_ids = new List<int>();
            foreach (JSONNode item in listOfIds)
                paytable_ids.Add(item.AsInt);
            contest.Paytable_Ids = paytable_ids;

            // data for the timers
            contest.Duration_sec = node["dRn"];
            int timeToSwitch = node["tTs"].AsInt;
            contest.Time_to_switch_status = DateTime.Now.AddSeconds(timeToSwitch);

            contest.WinFactor = node["wFr"].AsFloat;
            contest.LoseFactor = node["lFr"].AsFloat;
            contest.EndTime = node["eAt"].AsInt;

            contest.GraceTime = node["gTe"];
            contest.TableFilterData = CreateTableFilterdata();
            contest.TableFilterData.MinBuyIn = node["bet"]["sTt"].AsInt;
            contest.TableFilterData.MaxBuyIn = node["bet"]["eNd"].AsInt;
            contest.Rank = node["rak"].AsInt;
            contest.Total_players = node["tPs"].AsInt;

            int pool_prize = node["tPPl"].AsInt;
            int prev_pool_prize = node["pTPPl"].AsInt;
            ContestJackpot cj = new ContestJackpot();
            if (contestStatus == Contest.ContestStatus.Ongoing)
                cj.InitContestJackpot(prev_pool_prize, pool_prize);
            else
                cj.InitContestJackpot(pool_prize, pool_prize);

            contest.Contest_jackpot = cj;
            contest.TableFilterData.SpecialOnly = node["iSl"] == 1;

            return contest;
        }


        public void ParseContestsLists(ContestsList contestsList, JSONNode jSONNode)
        {
            contestsList.Clear();

            JSONArray contest_array = jSONNode["fUe"].AsArray;
            ParseContestsList(contestsList, contest_array, Contest.ContestStatus.Future);

            contest_array = jSONNode["oGg"].AsArray;
            ParseContestsList(contestsList, contest_array, Contest.ContestStatus.Ongoing);

            contest_array = jSONNode["cOd"].AsArray;
            ParseContestsList(contestsList, contest_array, Contest.ContestStatus.Completed);

            contestsList.FireContestsDataChangedEvent();

        }

        private void ParseContestsList(ContestsList contestsList, JSONArray contest_array, Contest.ContestStatus contestStatus)
        {
            int i = 0;
            foreach (JSONNode ch_node in contest_array)
            {
                Contest contest = ParseContest(ch_node, i, contestStatus);

                contestsList.AddContestToList(contest);

                i++;
            }
        }





    }

    public class ContestDynamicData
    {
        int m_contest_id;
        int m_curr_jackpot_value;
        int m_rank;
        int m_total_players;

        public ContestDynamicData(int m_contest_id, int m_curr_jackpot_value, int m_rank, int m_total_players)
        {
            this.m_contest_id = m_contest_id;
            this.m_curr_jackpot_value = m_curr_jackpot_value;
            this.m_rank = m_rank;
            this.m_total_players = m_total_players;
        }

        public int Contest_id { get => m_contest_id; set => m_contest_id = value; }
        public int Curr_jackpot_value { get => m_curr_jackpot_value; set => m_curr_jackpot_value = value; }
        public int Rank { get => m_rank; set => m_rank = value; }
        public int Total_players { get => m_total_players; set => m_total_players = value; }
    }

}