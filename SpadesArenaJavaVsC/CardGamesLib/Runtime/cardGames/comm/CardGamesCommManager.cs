using common.controllers.purchase;
using System.Collections;
using System.Collections.Generic;
using common;
using common.comm;
using common.experiments;
using common.facebook.models;
using common.ims;
using common.models;
using System;
using cardGames.models;
using cardGames.controllers;
using common.mes;
using common.utils;
using cardGames.models.contests;
using common.controllers;
using static cardGames.controllers.ContestsController;
using common.ims.model;
using static cardGames.models.World;
using common.apple;

namespace cardGames.comm
{
    public abstract class CardGamesCommManager : CommManager
    {
        // Session timeout in seconds. The server timeout is 60 minutes.
        public const float SESSION_TIMEOUT_SEC = 60 * 60;

        public event Action SessionTimeOut;

        [SerializeField] string fbCanvasUrl = null;
        [SerializeField] string fbCanvasUrlQAMode = null;
        [SerializeField] string fbCanvasUrlDevMode = null;

        [SerializeField] public bool m_contestsFakeData;


        public delegate void LoginCompleted(bool success, User user, List<World> worlds, Dictionary<string, ActiveArena> activeArenas,
            Cashier cashier, Mission mission, List<MissionController.ClaimMissionData> mission_to_claim, long next_mission_date,
            List<ContestsController.ClaimContestData> contests_to_claim, List<VouchersGroup> vouchers, bool newUser,
            UserStats stats, List<Bonus> bonuses, VersionData versionData, ItemsInventory inventory);

        public delegate void AddInvitedFriendCompleted(bool success, JSONNode responseData);

        public delegate void EndRoundCompleted(Challenge challenge_update, Contest contest);

        public delegate void EndMatchArenaCompleted(bool success, int coinsPayout, int xpPayout, int newCoinsBalance, int newDiamondsBalance, int newXpBalance, LevelData levelData, int nextArenaStage, int currGemStep, int totalGemsWon, int newLeaderboardId, float minBotFactor, float maxBotFactor, Cashier cashier);

        public delegate void ArenaBuyInCompleted(bool success, int curStage, int newDiamondsBalance);

        public delegate void BonusClaimCompleted(bool success, int newCoinsBalance, int newDiamondsBalance, BonusAwarded bonusAwarded);

        public delegate void BonusIMSClaimCompleted(bool success, int newCoinsBalance, BonusAwarded bonusAwarded);

        public delegate void LeaderboardAwardClaimCompleted(bool success, int coinsWon, int newCoinsBalance);

        public delegate void MissionClaimCompleted(bool success, int newCoinsBalance, BonusAwarded bonusAwarded);

        public delegate void ContestClaimCompleted(bool success, int newCoinsBalance, BonusAwarded bonusAwarded);

        public delegate void CommandCompleted(bool success);

        public delegate void OtherUserStatsCompleted(bool success, UserStats user_stats, int xp, int level, int total_level_xp, int coins_balance);

        public delegate void EnterMaintenance();

        public delegate void GetChallengesCompleted(bool success, long next_mission_date, Mission mission);

        public delegate void BuyItemCompleted(bool success, string itemId, int newCoinsBalance);

        public delegate void GetTableDataCompleted(bool success, SingleMatch matchData);

        public delegate void GetSendDataCompleted(bool success);

        public delegate void GetContestsComplete(bool success);
        public delegate void GetContestLeaderboardComplete(bool success, int contest_id, List<ContestPlayer> contestPlayers);
        public delegate void GetContestMiniLeaderboardComplete(bool success, int contest_id, List<ContestPlayer> contestPlayersTop3, List<ContestPlayer> contestPlayersRank);
        public delegate void GetContestAwardsComplete(bool success, List<ClaimContestData> claimContestData);
        public delegate void GetContestDynamicDataComplete(bool success, List<ContestDynamicData> contestDynamicData);
        public delegate void GetContestPaytableComplete(bool success, ContestPaytable contestPaytable, JSONNode paytableJson);


        protected LoginCompleted m_login_completed;
        protected GetChallengesCompleted m_get_mission_completed;
        protected EndMatchArenaCompleted m_end_match_arena_completed;
        protected ArenaBuyInCompleted m_arena_buyin_completed;
        protected BonusClaimCompleted m_bonus_claim_completed;
        protected BonusIMSClaimCompleted m_bonus_ims_claim_completed;
        protected CommandCompleted m_command_completed;
        protected OtherUserStatsCompleted m_other_user_stats_completed;
        protected AddInvitedFriendCompleted m_add_invited_friend_completed;
        protected CommandCompleted m_save_push_token_completed;
        protected LeaderboardAwardClaimCompleted m_leaderboard_award_claim_completed;
        protected MissionClaimCompleted m_mission_claim_compelte;
        protected ContestClaimCompleted m_contest_claim_compelte;
        protected CommandCompleted m_leaderboards_completed;
        protected EnterMaintenance m_enterMaintenance;
        protected BuyItemCompleted m_buy_item_completed;
        protected EndRoundCompleted m_end_round_completed;
        protected GetTableDataCompleted m_get_table_data_completed;
        protected GetSendDataCompleted m_get_send_data_completed;
        protected GetContestsComplete m_get_contests_complete;
        protected GetContestLeaderboardComplete m_get_contest_leaderboard_complete;
        protected GetContestMiniLeaderboardComplete m_get_contest_mini_leaderboard_complete;
        protected GetContestAwardsComplete m_get_contest_awards_complete;
        protected GetContestDynamicDataComplete m_get_contest_dynamic_data_complete;
        protected GetContestPaytableComplete m_get_contest_paytable_data_complete;


        public static new CardGamesCommManager Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;

                // Add error ignore command
                m_client.ErrorIgnoreCommands.Add("getContestsDynamicData");
                m_client.ErrorIgnoreCommands.Add("getContests");
                m_client.ErrorIgnoreCommands.Add("clientExceptionLogger");

                m_client.LogIgnoreCommands.Add("clientExceptionLogger");
            }

            m_contestsFakeData = false;
        }


        protected virtual void Start()
        {



#if UNITY_WEBGL || UNITY_EDITOR
            StartCoroutine(CheckSessionTimeout());
#endif
        }

        #region factory methods
        protected abstract CardGamesBonusModelParser GetBonusModelParser();
        #endregion

        /// <summary>
        /// Login or registers a user.
        /// Called when the app starts.
        /// </summary>       
        public virtual void Login(string deviceId, int platform, int appVersion, FacebookData facebook_data, AppleData appleData, JSONObject trackingInfo,
                           string bonusToken, string fbMessengerId, LoginCompleted completeDelegate, EnterMaintenance maintenanceDelegate)
        {

            m_login_completed = completeDelegate;
            m_enterMaintenance = maintenanceDelegate;

            JSONNode requestData = new JSONObject();
            requestData["dId"] = deviceId;//"generated-B96RQIzoguHJ0jCTLgh6HXkX0B";//;
            requestData["plm"].AsInt = platform;
            requestData["aVn"].AsInt = appVersion;

            //facebook related
            if (facebook_data != null)
            {
                requestData["eUy"] = facebook_data.User_id;

                if (facebook_data.First_name != null)
                    requestData["fNe"] = facebook_data.First_name;

                if (facebook_data.Last_name != null)
                    requestData["lNe"] = facebook_data.Last_name;

                if (facebook_data.Middle_name != null)
                    requestData["mNe"] = facebook_data.Middle_name;

                if (facebook_data.Email != null)
                    requestData["eml"] = facebook_data.Email;

                if (facebook_data.Gender != null)
                    requestData["ger"] = facebook_data.Gender;

            }

            if (appleData != null && appleData.SignedIn)
            {
                requestData["eUy2"] = appleData.UserId;
            }

            // Tracking info
            if (trackingInfo != null)
            {
                requestData["tIo"] = trackingInfo;
            }

            if (bonusToken != null && bonusToken != "")
            {
                requestData["bTn"] = bonusToken;
            }

            if (fbMessengerId != null && fbMessengerId != "")
            {
                requestData["mId"] = fbMessengerId;
            }

            m_client.SendRequest("login", requestData, onLoginResponse, false);
            m_client.Login_token = RestClient.INVALID_LOGIN_TOKEN;
        }


        protected virtual void onLoginResponse(bool success, JSONNode responseData)
        {
            if (success)
            {

                //maintenance
                JSONNode maintenance = responseData["mae"];
                if (maintenance != null && m_enterMaintenance != null)
                {
                    m_enterMaintenance();
                    return;
                }

                // Set the login token for future requests
                string loginToken = responseData["lTn"];
                m_client.Login_token = loginToken;




                // Lobby data
                List<World> worlds = new List<World>();
                JSONArray lobbyData = responseData["lDa"].AsArray;


                JSONNode collectiblesNode = responseData["uCs"];
                JSONNode trophyNode = responseData["uTs"];

                // PromoRoom Changes
                // Get PROMO ROOM information at this point from the server

                int index = 0;
                foreach (JSONNode worldNode in lobbyData)
                {

                    World world = (GetModelParser() as CardGamesModelParser).ParseWorld(worldNode, collectiblesNode, trophyNode);
                    world.Index = index++;

                    if (world.Index < 6)
                        world.WorldType = WorldTypes.Regular;
                    else
                        world.WorldType = WorldTypes.Virtual;

                    worlds.Add(world);

                }

                // Initial world id
                ModelManager.Instance.InitialWorldId = responseData["iWd"];

                Mission mission = GetCardGamesModelParser().ParseMission(responseData["msn"]);


                //missions to claim list

                JSONArray missions_to_claim = responseData["mAd"].AsArray;

                List<MissionController.ClaimMissionData> missions_to_claim_list = new List<MissionController.ClaimMissionData>();

                foreach (JSONNode mission_to_claim in missions_to_claim)
                {
                    missions_to_claim_list.Add(new MissionController.ClaimMissionData(mission_to_claim["mId"].AsInt, mission_to_claim["pre"].Value));
                }



                //next mission date

                long next_mission_date = -1;//means nothing is set there
                long next_mission_interval = 0;

                if (responseData["nMDe"] != null)
                {
                    next_mission_date = (long)responseData["nMDe"].AsInt;

                    next_mission_interval = (long)responseData["nMDIl"].AsInt;

                    TimeSpan timeDiff = TimeSpan.FromSeconds(next_mission_interval);

                    next_mission_date = Convert.ToInt64(timeDiff.Ticks + DateTime.Now.Ticks);
                }


                // Cashier data - IMS (have higher priority)
                Cashier cashier = GetCardGamesModelParser().ParseCashier(responseData["ims"]["bPe"]);

                // Cashier data - MES
                if (cashier == null)
                    cashier = GetCardGamesModelParser().ParseCashierMES(responseData["pSr"]);



                // User data
                User user = GetCardGamesModelParser().ParseUser(responseData, worlds);

                // arena player rewards progress
                JSONArray arenaPlayerRewardsProgress = responseData["aPRPs"].AsArray;
                GetCardGamesModelParser().ParseArenaPlayerRewardProgress(arenaPlayerRewardsProgress);

                // Active Arenas
                Dictionary<String, ActiveArena> activeArenas = new Dictionary<string, ActiveArena>();

                JSONArray activeArenasData = responseData["pAs"].AsArray;

                foreach (JSONNode activeArenaNode in activeArenasData)
                {
                    Arena arena = null;
                    String arenaId = activeArenaNode["aId"];

                    // Loop over worlds and find the arena by its id
                    foreach (World _world in worlds)
                    {
                        arena = _world.GetArenaByID(arenaId);
                        if (arena != null)
                            break;
                    }

                    ActiveArena activeArena = GetCardGamesModelParser().ParseActiveArena(activeArenaNode, arena);
                    activeArenas.Add(arenaId, activeArena);
                }

                // New user
                bool newUser = responseData["iNw"].AsInt == 1;

                // User stats
                JSONNode userStatsJson = responseData["uSs"];
                UserStats stats = UserStatsController.Instance.ParseUserStats(userStatsJson);


                // Bonuses data
                List<Bonus> bonuses = new List<Bonus>();
                JSONArray bonusesData = responseData["bDa"].AsArray;
                foreach (JSONNode bonusData in bonusesData)
                {
                    Bonus bonus = GetBonusModelParser().ParseBonus(bonusData, worlds);
                    if (bonus != null)
                    {
                        bonuses.Add(bonus);
                    }
                }

                //achievement data
                HandleAchievements(responseData);

                //version control
                JSONNode version_control = responseData["vCl"];
                VersionData versionData = new VersionData();

                if (version_control != "{}" && version_control != null)
                {
                    int version = version_control["nVn"].AsInt;
                    bool mandatory = Convert.ToBoolean(version_control["iMy"].AsInt);

                    versionData = new VersionData(version, mandatory);
                }

                // Age verification flag
                versionData.ShowAgeVerification = true;
                if (responseData["age"] != null)
                {
                    versionData.ShowAgeVerification = false;
                }

                //leaderboard reward
                ModelManager.Instance.LeaderboardReward.ModelIsSet = false;
                JSONNode leaderboardRewardData = responseData["lAd"];
                if (leaderboardRewardData != null && leaderboardRewardData["lId"] != null)
                {
                    ModelManager.Instance.LeaderboardReward.Leaderboard_id = leaderboardRewardData["lId"].AsInt;
                    ModelManager.Instance.LeaderboardReward.Group = leaderboardRewardData["cGId"];
                    if (ModelManager.Instance.Leaderboards == null)
                    {
                        ModelManager.Instance.Leaderboards = new LeaderboardsModel(user);
                    }
                    ModelManager.Instance.LeaderboardReward.LeaderboardTitle = ModelManager.Instance.Leaderboards.GetTitleFromGroup(leaderboardRewardData["cGId"]);
                    ModelManager.Instance.LeaderboardReward.Color = ModelManager.Instance.Leaderboards.GetColorFromGroup(leaderboardRewardData["cGId"]);
                    ModelManager.Instance.LeaderboardReward.Rank = leaderboardRewardData["rak"].AsInt;
                    ModelManager.Instance.LeaderboardReward.Reward = leaderboardRewardData["cGs"].AsInt;
                    ModelManager.Instance.LeaderboardReward.ModelIsSet = true;
                }



                // A/B Experiments
                JSONNode ab = responseData["ab"];
                if (ab != null)
                {
                    ExperimentsManager.Instance.LoadExperiments(ab);
                }

                IMSController.Instance.ClearEvents(); // Clear everything on login

                JSONNode mesEvents = responseData["evs"];
                LoggerController.Instance.Log(LoggerController.Module.Comm, "MES JSON: " + mesEvents.ToString());
                MESBase.Instance.ParseMESJson(mesEvents, true);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log(LoggerController.Module.Comm, "IMS JSON: " + imsEvents.ToString());
                IMSController.Instance.ParseEvents(imsEvents, false);

                // Inventory
                LoggerController.Instance.Log(LoggerController.Module.Comm, "Initializing user items inventory");
                ItemsInventory inventory = new ItemsInventory();
                JSONArray inventoryData = responseData["iny"].AsArray;
                foreach (JSONNode inventoryItem in inventoryData)
                {
                    inventory.AddItem(inventoryItem.Value);
                }

                // Missions
                MissionController.Instance.NewMissionArrived(mission);

                // Contests
                JSONNode contestsNode;
#if UNITY_EDITOR
                if (m_contestsFakeData)
                {
                    contestsNode = JSON.Parse(ContestsController.Instance.Full_contests_data.ToString());
                    if (contestsNode != null)
                        GetCardGamesModelParser().ParseContestsLists(ModelManager.Instance.ContestsLists, contestsNode["cOts"]);
                }
                else
                {
                    contestsNode = responseData["cOts"];
                    LoggerController.Instance.Log(LoggerController.Module.Comm, "Login contest data: " + contestsNode);
                    if (contestsNode != null)
                        GetCardGamesModelParser().ParseContestsLists(ModelManager.Instance.ContestsLists, contestsNode);

                }
#else
                contestsNode = responseData["cOts"];
                LoggerController.Instance.Log(LoggerController.Module.Comm, "Login contest data: " + contestsNode);
                if (contestsNode != null)
                    GetCardGamesModelParser().ParseContestsLists(ModelManager.Instance.ContestsLists, contestsNode);
#endif
                //contests to claim list
                JSONArray contests_to_claim = null;
#if UNITY_EDITOR
                if (m_contestsFakeData)
                {
                 /*   contests_to_claim = JSON.Parse(ContestsController.Instance.Dummy_Award_contests_data.ToString()).AsArray;
                    ContestsController.Instance.SetFakeAwards();
                    OnGetContestAwardsResponse(true, JSON.Parse(ContestsController.Instance.Dummy_Award_contests_data.ToString()));
                    if (contests_to_claim == null)
                        GetCardGamesModelParser().ParseAwards(ContestsController.Instance.Dummy_Award_contests_data.ToString());
*/
                }
                else
                {
                    contests_to_claim = responseData["cAd"].AsArray;
                }
#else
                contests_to_claim = responseData["cAd"].AsArray;
#endif


                List<ContestsController.ClaimContestData> contests_to_claim_list = new List<ContestsController.ClaimContestData>();
#if UNITY_EDITOR
                if (m_contestsFakeData)
                {
                       //contests_to_claim_list.Add(new ClaimContestData(1, 100, 11));
                       //contests_to_claim_list.Add(new ClaimContestData(2, 400, 5));
                }
                else
                {
                    foreach (JSONNode contest in contests_to_claim)
                    {
                        //extra measure not to add defective contest claims
                        if (contest["pre"].AsInt > 0)
                            contests_to_claim_list.Add(new ContestsController.ClaimContestData(contest["ctId"].AsInt, contest["pre"].AsInt, contest["rak"].AsInt));
                    }
                }
#else
                foreach (JSONNode contest in contests_to_claim)
                {
                    //extra measure not to add defective contest claims
                    if (contest["pre"].AsInt > 0)
                        contests_to_claim_list.Add(new ContestsController.ClaimContestData(contest["ctId"].AsInt, contest["pre"].AsInt, contest["rak"].AsInt));
                }
#endif
                //Vouchers
                List<VouchersGroup> vouchers = GetCardGamesModelParser().ParseVouchersList(responseData["vLt"].AsArray);

                m_login_completed(true, user, worlds, activeArenas, cashier, mission, missions_to_claim_list, next_mission_date, contests_to_claim_list, vouchers,
                    newUser, stats, bonuses, versionData, inventory);
            }
            else
            {
                m_login_completed(false, null, null, null, null, null, null, -1, null, null, false, null, null, null, null);
            }

        }

        protected abstract void HandleAchievements(JSONNode responseData);

        /// <summary>
        /// Updates the user avatar data
        /// </summary>
        public void UpdateUserAvatar(MAvatarModel avatar, string nickName = null, ResponseDelegate onResponse = null)
        {
            JSONNode requestData = new JSONObject();

            if (avatar != null)
            {
                JSONNode avatarJsonObject = MAvatarController.Instance.SerializeAvatarData(avatar);
                requestData["avr"] = avatarJsonObject;
            }

            if (nickName != null)
                requestData["nie"] = nickName;

            m_client.SendRequest("updateUserInfo", requestData, onResponse);
        }

        /// <summary>
        /// Updates the user avatar data
        /// </summary>
        public void UpdateUserNickname(String nickName, ResponseDelegate onResponse)
        {
            JSONNode requestData = new JSONObject();

            requestData["nie"] = nickName;

            m_client.SendRequest("updateUserInfo", requestData, onResponse);

        }



        public void GetContests(bool getOnGoing, bool getFuture, bool getCompleted, GetContestsComplete onComplete)
        {
            m_get_contests_complete = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["fUe"].AsInt = getOnGoing ? 1 : 0;
            requestData["oGg"].AsInt = getFuture ? 1 : 0;
            requestData["cOd"].AsInt = getCompleted ? 1 : 0;

#if UNITY_EDITOR
            if (m_contestsFakeData)
            {
                //  calling fake data
                OnGetContestsResponse(true, JSON.Parse(ContestsController.Instance.Full_contests_data.ToString()));
            }
            else
            {
                // calling the server
                m_client.SendRequest("getContests", requestData, OnGetContestsResponse);
            }
#else
            m_client.SendRequest("getContests", requestData, OnGetContestsResponse);
#endif
        }

        private void OnGetContestsResponse(bool success, JSONNode responseData)
        {
            if (!success)
                return;


            GetCardGamesModelParser().ParseContestsLists(ModelManager.Instance.ContestsLists, responseData["cOts"]);


            m_get_contests_complete(success);
        }


        public void GetContestsPaytable(int paytable_id, GetContestPaytableComplete onComplete)
        {
            m_get_contest_paytable_data_complete = onComplete;
            JSONNode requestData = new JSONObject();

            JSONArray paytableIdsArr = new JSONArray();
            paytableIdsArr.Add(paytable_id);

            requestData["pOTs"] = paytableIdsArr;

#if UNITY_EDITOR
            if (m_contestsFakeData)
            {
                // calling fake data
                JSONNode fakeData = JSON.Parse(ContestsController.Instance.Paytable_data.ToString());
                OnGetContestsPaytableResponse(true, fakeData);
            }
            else
            {
                m_client.SendRequest("getContestsPayTable", requestData, OnGetContestsPaytableResponse);
            }
#else
            m_client.SendRequest("getContestsPayTable", requestData, OnGetContestsPaytableResponse);
#endif
        }

        /// <summary>
        /// should get here only when reaching the server for paytable data
        /// </summary>
        /// <param name="success"></param>
        /// <param name="responseData"></param>
        private void OnGetContestsPaytableResponse(bool success, JSONNode responseData)
        {
            if (!success)
                return;

            JSONNode paytableJson = responseData["pOTs"][0];

            // save data in the model
            ContestPaytable contestPaytable = GetCardGamesModelParser().ParsePaytable(paytableJson);

            m_get_contest_paytable_data_complete(success, contestPaytable, paytableJson);
        }

        public void GetContestsLeaderboard(int contest_id, GetContestLeaderboardComplete onComplete)
        {
            m_get_contest_leaderboard_complete = onComplete;
            JSONNode requestData = new JSONObject();
            requestData["ctId"] = contest_id;
#if UNITY_EDITOR
            if (m_contestsFakeData)
            {
                // calling fake data
                OnGetContestsLeaderboardResponse(true, JSON.Parse(ContestsController.Instance.Leaderboard_contests_data.ToString()));
            }
            else
            {
                // calling the server
                m_client.SendRequest("getContestsLeaderBoard", requestData, OnGetContestsLeaderboardResponse);
            }
#else
            m_client.SendRequest("getContestsLeaderBoard", requestData, OnGetContestsLeaderboardResponse);
#endif
        }

        private void OnGetContestsLeaderboardResponse(bool success, JSONNode responseData)
        {
            if (!success)
                return;

            List<ContestPlayer> contestPlayers = GetCardGamesModelParser().ParseContestPlayers(responseData, "pLs");
            m_get_contest_leaderboard_complete(success, responseData["ctId"].AsInt, contestPlayers);
        }


        public void GetContestsMiniLeaderboard(int contest_id, GetContestMiniLeaderboardComplete onComplete)
        {
            m_get_contest_mini_leaderboard_complete = onComplete;
            JSONNode requestData = new JSONObject();
            requestData["ctId"] = contest_id;
            // Below and Above
            requestData["rNm"] = 1;
#if UNITY_EDITOR
            if (m_contestsFakeData)
            {
                // calling fake data
                OnGetContestsMiniLeaderboardResponse(true, JSON.Parse(ContestsController.Instance.Mini_leaderboard_contests_data.ToString()));
            }
            else
            {
                // calling the server
                m_client.SendRequest("getContestsMiniLeaderBoard", requestData, OnGetContestsMiniLeaderboardResponse);
            }
#else
            m_client.SendRequest("getContestsMiniLeaderBoard", requestData, OnGetContestsMiniLeaderboardResponse);
#endif
        }

        private void OnGetContestsMiniLeaderboardResponse(bool success, JSONNode responseData)
        {
            if (!success)
                return;

            List<ContestPlayer> contestlistRank = GetCardGamesModelParser().ParseContestPlayers(responseData, "pLs");
            List<ContestPlayer> contestlistTop3 = GetCardGamesModelParser().ParseContestPlayers(responseData, "tOp3");

            m_get_contest_mini_leaderboard_complete(success, responseData["ctId"].AsInt, contestlistRank, contestlistTop3);
        }

        public void GetContestAwards(GetContestAwardsComplete onComplete)
        {
            m_get_contest_awards_complete = onComplete;
            JSONNode requestData = new JSONObject();
            // calling fake data
#if UNITY_EDITOR
            if (m_contestsFakeData)
            {
                   OnGetContestAwardsResponse(true, JSON.Parse(ContestsController.Instance.Dummy_Award_contests_data.ToString()));
            }
            // calling the server
            else
            {
                m_client.SendRequest("getContestsAwards", requestData, OnGetContestAwardsResponse);
            }
#else
            m_client.SendRequest("getContestsAwards", requestData, OnGetContestAwardsResponse);
#endif
        }

        private void OnGetContestAwardsResponse(bool success, JSONNode responseData)
        {
            if (!success)
                return;

            List<ClaimContestData> claimContestData = GetCardGamesModelParser().ParseAwards(responseData["cAd"]);
            m_get_contest_awards_complete(success, claimContestData);
            foreach (ClaimContestData CCD in claimContestData)
            {
                if (CCD.Contest_rank == 1)
                    TrackingManager.Instance.AppsFlyerTrackOneTimeEvent("ContestWinner");
                if (CCD.Contest_coins_prize > 0)
                    TrackingManager.Instance.AppsFlyerTrackOneTimeEvent("ContestInTheMoney");
            }
        }

        public void GetContestsDynamicData(List<Contest> ongoingContests, GetContestDynamicDataComplete onComplete)
        {
            m_get_contest_dynamic_data_complete = onComplete;
            JSONNode requestData = new JSONObject();

            JSONArray contestsIds = new JSONArray();
            foreach (Contest contest in ongoingContests)
                contestsIds.Add(contest.Id);

            requestData["ctIds"] = contestsIds;
            requestData["pPl"] = 1;
            requestData["rak"] = 1;
            requestData["tPs"] = 1;
#if UNITY_EDITOR
            if (m_contestsFakeData)
            {
                // calling fake data
                OnGetContestsDynamicDataResponse(true, JSON.Parse(ContestsController.Instance.Dynamic_contests_data.ToString()));
            }
            else
            {
                // calling the server
                m_client.SendRequest("getContestsDynamicData", requestData, OnGetContestsDynamicDataResponse);
            }
#else
            m_client.SendRequest("getContestsDynamicData", requestData, OnGetContestsDynamicDataResponse);
#endif
        }

        private void OnGetContestsDynamicDataResponse(bool success, JSONNode responseData)
        {
            List<ContestDynamicData> dynamicContest = GetCardGamesModelParser().ParseDynamicDataList(responseData);
            m_get_contest_dynamic_data_complete(success, dynamicContest);
        }


        /// <summary>
        /// Command for starting a new single match.
        /// </summary>
        public void GetChallengesMission(bool swap, GetChallengesCompleted onComplete)
        {
            string command_name = "getChallenges";

            if (swap)
                command_name = "swapChallenge";

            m_get_mission_completed = onComplete;

            JSONNode requestData = new JSONObject();

            m_client.SendRequest(command_name, requestData, OnGetChallengesResponse);
        }

        private void OnGetChallengesResponse(bool success, JSONNode responseData)
        {

            Mission mission = GetCardGamesModelParser().ParseMission(responseData["msn"]);


            //next mission date

            long next_mission_date = -1;//means nothing is set there
            long next_mission_interval = 0;

            if (responseData["nMDe"] != null)
            {
                long next_mission_date2 = (long)responseData["nMDe"].AsInt;

                next_mission_interval = (long)responseData["nMDIl"].AsInt;

                TimeSpan timeDiff = TimeSpan.FromSeconds(next_mission_interval);

                DateTime end_date = new DateTime(timeDiff.Ticks + DateTime.UtcNow.Ticks);

                next_mission_date = DateUtils.DateToUnixTimestamp(end_date);

            }

            m_get_mission_completed(success, next_mission_date, mission);
            MissionController.Instance.NewMissionArrived(mission);

        }


        /// <summary>
        /// Starts the reconnect.
        /// </summary>
        public void StartReconnect(CommandCompleted onComplete)
        {
            m_command_completed = onComplete;
            JSONNode requestData = new JSONObject();
            m_client.SendRequest("reconnect", requestData, OnReconnectResponse);
        }

        private void OnReconnectResponse(bool success, JSONNode responseData)
        {
            m_command_completed(success);
        }

        /// <summary>
        /// Commnad for buy-in to Arena.
        /// </summary>
        public void ArenaBuyIn(int rebuy, string arenaId, ArenaBuyInCompleted onComplete)
        {
            m_arena_buyin_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["aId"] = arenaId;
            requestData["iRy"] = rebuy;


            m_client.SendRequest("buyInArena", requestData, OnArenaBuyInResponse);
        }

        private void OnArenaBuyInResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                int curStage = responseData["aSId"].AsInt;
                int newCoinsBalance = responseData["cBe"].AsInt;

                m_arena_buyin_completed(true, curStage, newCoinsBalance);
            }
            else
            {
                m_arena_buyin_completed(false, 0, 0);
            }
        }

        public void AddInvitedFriend(int askUserId, AddInvitedFriendCompleted onComplete)
        {
            m_add_invited_friend_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["frd"] = askUserId;

            m_client.SendRequest("addInvitedFriend", requestData, OnAddInvitedFriend);
        }

        private void OnAddInvitedFriend(bool success, JSONNode responseData)
        {
            if (!success)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm, "Add Invited friend error received:\n" + responseData);

            }

            m_add_invited_friend_completed(success, responseData);
        }

        public IEnumerator CheckSessionTimeout()
        {
            while (true)
            {
                float curr_time = Time.realtimeSinceStartup;

                // Check how much time has passed since last communication to the server
                if (curr_time > Rest_Client.Last_communication + SESSION_TIMEOUT_SEC)
                {
                    if (SessionTimeOut != null)
                    {
                        SessionTimeOut();
                        break;
                    }
                }
                yield return new WaitForSecondsRealtime(60);
            }
        }





        public string FbCanvasUrl
        {
            get
            {
#if DEV_MODE
                return fbCanvasUrlDevMode;
#elif QA_MODE
                return fbCanvasUrlQAMode;
#else
				return fbCanvasUrl;
#endif
            }
        }


        public RestClient Rest_Client
        {
            get
            {
                return m_client;
            }
        }




        /// <summary>
        /// same command can return both bot or user data- you need to only send one parameter - either level to get a bot of that level data - or the user details based on a ukey sent - 
        /// instead of adding a flag - a -1 value in the level means we want the ukey 
        /// </summary>
        public void RequestUserStats(int req_level, int ukey, OtherUserStatsCompleted OnComplete)
        {
            m_other_user_stats_completed = OnComplete;

            JSONNode requestData = new JSONObject();

            if (req_level != -1)
                requestData["lel"] = req_level;
            else
                requestData["uky"] = ukey;

            m_client.SendRequest("getOpponentStats", requestData, onUserStatsBack);

        }

        private void onUserStatsBack(bool success, JSONNode responseData)
        {
            if (success)
            {

                JSONNode user_stas_json = responseData["uSs"];
                int xp = responseData["xp"].AsInt;
                int level = responseData["lel"].AsInt;
                int total_level_xp = responseData["xTd"].AsInt;
                int coins_balance = responseData["cBe"].AsInt;

                UserStats user_stats = CreateUserStats();

                if (user_stas_json.Value != "null") //this means the stats isnt empty
                    user_stats = UserStatsController.Instance.ParseUserStats(user_stas_json);
                else
                {
                    user_stats.ArenaPlayed = 0;
                    user_stats.CoinsWon = 0;
                    user_stats.RoundPlayed = 0;
                    HandleEmptyUserStats(user_stats);
                }

                m_other_user_stats_completed(true, user_stats, xp, level, total_level_xp, coins_balance);
            }
            else
            {

                m_other_user_stats_completed(false, null, 0, 0, 0, 0);
            }
        }

        protected abstract UserStats CreateUserStats();

        protected abstract void HandleEmptyUserStats(UserStats user_stats);

        public void SavePushToken(string pushToken, string deviceId, CommandCompleted onComplete)
        {
            m_save_push_token_completed = onComplete;

            JSONNode requestData = new JSONObject();

            requestData["pTId"] = pushToken;
            requestData["dId"] = deviceId;

            m_client.SendRequest("savePushToken", requestData, OnSavePushTokenComplete);

        }

        private void OnSavePushTokenComplete(bool success, JSONNode responseData)
        {
            if (success)
            {
                m_save_push_token_completed(true);
            }
            else
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm, "Save push token error received:\n" + responseData);

                m_save_push_token_completed(false);
            }
        }

        public void GetLeaderboards(CommandCompleted callback)
        {
            m_leaderboards_completed = callback;

            JSONNode requestData = new JSONObject();
            m_client.SendRequest("getLeaderboards", requestData, OnGetLeaderboardsResponse, false);

        }

        private void OnGetLeaderboardsResponse(bool success, JSONNode responseData)
        {
            if (!success)
            {
                m_leaderboards_completed(false);
                return;
            }

            LeaderboardMeta leaderboardMeta = null;
            JSONNode leaderboard_json = responseData["led"];
            JSONNode infoTable = responseData["lLGp"];
            JSONNode currentLeaderboardData = leaderboard_json["c"];
            JSONNode previousLeaderboardData = leaderboard_json["p"];

            if (currentLeaderboardData["lId"] != null)
            {
                JSONNode ranks_json = currentLeaderboardData["pDn"];
                ModelManager.Instance.Leaderboards.CurrentLeaderboardId = currentLeaderboardData["lId"].AsInt;
                leaderboardMeta = new LeaderboardMeta(ModelManager.Instance.Leaderboards.CurrentLeaderboardId,
                    currentLeaderboardData["mLl"].AsInt,
                    currentLeaderboardData["xLl"].AsInt,
                    ranks_json["uRk"].AsArray,
                    ranks_json["prz"].AsArray,
                    currentLeaderboardData["eDe"].AsInt,
                    currentLeaderboardData["cGId"]
                );

                LeaderboardModel ldrbrdMdl = ModelManager.Instance.Leaderboards.GetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalCurrent);
                if (ldrbrdMdl == null)
                    ldrbrdMdl = new LeaderboardModel();

                ldrbrdMdl.LeaderboardMetaData = leaderboardMeta;
                ModelManager.Instance.Leaderboards.SetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalCurrent, ldrbrdMdl);
            }

            if (previousLeaderboardData["lId"] != null)
            {
                JSONNode ranks_json = previousLeaderboardData["pDn"];
                leaderboardMeta = new LeaderboardMeta(previousLeaderboardData["lId"].AsInt,
                    previousLeaderboardData["mLl"].AsInt,
                    previousLeaderboardData["xLl"].AsInt,
                    ranks_json["uRk"].AsArray,
                    ranks_json["prz"].AsArray,
                    previousLeaderboardData["eDe"].AsInt,
                    currentLeaderboardData["cGId"]
                );

                LeaderboardModel ldrbrdMdl = ModelManager.Instance.Leaderboards.GetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalPrevious);
                if (ldrbrdMdl == null)
                    ldrbrdMdl = new LeaderboardModel();

                ldrbrdMdl.LeaderboardMetaData = leaderboardMeta;
                ModelManager.Instance.Leaderboards.SetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalPrevious, ldrbrdMdl);

            }
            else
            {
                //no prev leaderboard
            }

            ModelManager.Instance.Leaderboards.InfoTable.Clear();
            for (int i = 0; i < infoTable.Count; i++)
            {
                JSONNode infoTableItem = infoTable.AsArray[i];
                ModelManager.Instance.Leaderboards.InfoTable.Add(infoTableItem["cGId"], infoTableItem["mLl"] + " - " + infoTableItem["xLl"]);
            }
            m_leaderboards_completed(true);
        }

        public void ClaimLeaderboardReward(int leaderboardId, LeaderboardAwardClaimCompleted onComplete)
        {
            m_leaderboard_award_claim_completed = onComplete;
            JSONNode requestData = new JSONObject();
            requestData["lId"] = leaderboardId;
            m_client.SendRequest("claimLeaderboardPrize", requestData, onClaimLeaderboardReward, true);
        }

        private void onClaimLeaderboardReward(bool success, JSONNode responseData)
        {
            if (success)
            {
                JSONNode bonusAward = responseData["bAd"];
                m_leaderboard_award_claim_completed(success, bonusAward["cGs"], responseData["cBe"]);
            }
            else
            {
                m_leaderboard_award_claim_completed(false, 0, 0);
            }
        }

        public void ClaimMissionReward(int missionID, MissionClaimCompleted onComplete)
        {
            m_mission_claim_compelte = onComplete;
            JSONNode requestData = new JSONObject();
            requestData["mId"] = missionID;
            m_client.SendRequest("claimMissionPrize", requestData, onClaimMissionReward, true);
        }

        private void onClaimMissionReward(bool success, JSONNode responseData)
        {
            if (success)
            {
                BonusAwarded bonusAwarded = BonusModelParser.ParseBonusAwarded(responseData["bAd"]);
                m_mission_claim_compelte(success, responseData["cBe"], bonusAwarded);
            }
            else
            {
                m_mission_claim_compelte(false, 0, null);
            }
        }

        public void ClaimContestReward(int contestID, ContestClaimCompleted onComplete)
        {
            m_contest_claim_compelte = onComplete;
            JSONNode requestData = new JSONObject();
            requestData["ctId"] = contestID;
            m_client.SendRequest("claimContestPrize", requestData, onClaimContestReward, true);
        }

        private void onClaimContestReward(bool success, JSONNode responseData)
        {
            if (success)
            {
                BonusAwarded bonusAwarded = BonusModelParser.ParseBonusAwarded(responseData["bAd"]);
                m_contest_claim_compelte(success, responseData["cBe"], bonusAwarded);
            }
            else
            {
                m_contest_claim_compelte(false, 0, null);
            }
        }

        public void BuyItem(string itemData, string imsMetaData, BuyItemCompleted onCompleted)
        {
            m_buy_item_completed = onCompleted;

            JSONNode requestData = new JSONObject
            {
                ["iDa"] = itemData,
                ["mDa"] = imsMetaData
            };

            m_client.SendRequest("buyItem", requestData, OnBuyItemResponse, true);
        }

        private void OnBuyItemResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                string itemId = responseData["iId"].Value;
                int newCoinsBalance = responseData["cBe"].AsInt;
                m_buy_item_completed(true, itemId, newCoinsBalance);
            }
            else
            {
                m_buy_item_completed(false, "0", 0);
            }

        }

        public abstract void EndRound(string tableId, RoundData rData, EndRoundCompleted onComplete);


        public void ClaimIMSBonus(IMSActionValue actionValue, string imsMetaData, BonusIMSClaimCompleted onComplete)
        {
            m_bonus_ims_claim_completed = onComplete;

            JSONNode requestData = new JSONObject
            {
                ["aVe"] = new IMSModelParser().SerializeActionValue(actionValue),
                ["mDa"] = imsMetaData
            };

            m_client.SendRequest("claimDynamicBonus", requestData, OnClaimIMSBonusResponse, true);
        }



        public void OnClaimIMSBonusResponse(bool success, JSONNode responseData)
        {
            if (success)
            {

                IMSGoodsList iMSGoodsList = GetCardGamesModelParser().ParseGoodList(responseData["rGs"].AsArray);
                int newCoinsBalance = responseData["cBe"].AsInt;

                BonusAwarded bonusAwarded = new BonusAwarded();
                bonusAwarded.GoodsList = iMSGoodsList;

                // Cashier data and IMS data
                GetModelParser().ParseCashier(responseData["ims"]["bPe"]);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log("IMS JSON CLAIM BONUS: " + imsEvents.ToString());
                IMSController.Instance.ParseEvents(imsEvents);

                // Must update the purchaser since new packages might have been received
                PurchaseController.Instance.UpdatePurchaser();

                m_bonus_ims_claim_completed(true, newCoinsBalance, bonusAwarded);
            }
            else
            {
                m_bonus_ims_claim_completed(false, 0, null);
            }
        }

        public void ClaimBonus(Bonus.BonusTypes type, int sub_type, string token, int num_of_friends, List<string> senderIds, int leaderboardRank, bool isRedeem,
            string imsMetadata,BonusClaimCompleted onComplete)
        {
            m_bonus_claim_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["bTe"].AsInt = (int)type;
            requestData["bSTe"].AsInt = sub_type;
            requestData["nFs"].AsInt = num_of_friends;
            if (token != null)
                requestData["bTn"] = token;

            JSONObject eUD = new JSONObject();
            eUD["uRe"].AsInt = leaderboardRank;
            requestData["eUD"] = eUD;

            if (senderIds != null && senderIds.Count > 0)
            {
                requestData["sId"] = new JSONArray();
                foreach (string senderId in senderIds)
                {
                    requestData["sId"].AsArray.Add(senderId);
                }
            }

            // Redeem code flag
            requestData["iRm"] = isRedeem ? 1 : 0;

            // Optional IMS metadata
            if (imsMetadata!=null)
                requestData["mDa"] = imsMetadata;

            m_client.SendRequest("claimBonus", requestData, OnClaimBonusResponse, true);
        }

        private void OnClaimBonusResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                int newCoinsBalance = responseData["cBe"].AsInt;

                BonusAwarded bonusAwarded = BonusModelParser.ParseBonusAwarded(responseData["bAd"]);


                // Cashier data and IMS data
                GetModelParser().ParseCashier(responseData["ims"]["bPe"]);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log("IMS JSON CLAIM BONUS: " + imsEvents.ToString());
                IMSController.Instance.ParseEvents(imsEvents);

                // Must update the purchaser since new packages might have been received
                PurchaseController.Instance.UpdatePurchaser();

                m_bonus_claim_completed(true, newCoinsBalance, 0, bonusAwarded);
            }
            else
            {
                m_bonus_claim_completed(false, 0, 0, null);
            }
        }

        public void GetTableData(string tableId, GetTableDataCompleted onCompleted)
        {
            m_get_table_data_completed = onCompleted;

            JSONNode requestData = new JSONObject
            {
                ["gTId"] = tableId
            };

            m_client.SendRequest("getTable", requestData, OnGetTableResponse);
        }

        protected abstract void OnGetTableResponse(bool success, JSONNode responseData);

        public void SendLoggingData(string transactionID, string data)
        {

            JSONNode requestData = new JSONObject
            {
                ["tId"] = transactionID,
                ["lDa"] = data
            };

            m_client.SendRequest("clientExceptionLogger", requestData, null);
        }

        //public abstract CardGamesModelParser GetModelParser();

        public abstract override ModelParser GetModelParser();

        public CardGamesModelParser GetCardGamesModelParser()
        {
            return GetModelParser() as CardGamesModelParser;
        }

    }
}