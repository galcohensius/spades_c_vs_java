﻿using cardGames.controllers;
using cardGames.models;
using cardGames.piggy.controllers;
using cardGames.views.popups;
using common.controllers;
using common.controllers.purchase;
using common.ims;
using common.ims.model;
using common.mes;
using common.models;
using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_WEBGL
using System.Runtime.InteropServices;
#endif
using static common.controllers.PopupManager;

namespace cardGames.ims
{
    public abstract class CardGamesIMSController : IMSController
    {
        // Banner locations
        public const string BANNER_LOBBY_LEFT = "L1";
        public const string BANNER_HEADER_BUY = "H1";
        public const string BANNER_HEADER_RIGHT_BUTTON = "H2";
        public const string BANNER_HEADER_LEFT_BUTTON = "H3";
        public const string BANNER_HEADER_END_GAME_OFFER = "H4";
        public const string BANNER_FOOTER_SPECIAL_ROOM = "F1";
        public const string BANNER_END_GAME_UPPER = "E1";
        public const string BANNER_END_GAME_SIDE = "E2";


        [SerializeField] bool m_dontShowPopupsOnEditor;

        bool m_shouldClearRotatingBanners;

        public new static CardGamesIMSController Instance;

        protected override void Awake()
        {
            base.Awake();
            Instance = this as CardGamesIMSController;
        }

        private void Start()
        {
            ListenToIMSEventOnLogin();
        }

        protected abstract void ListenToIMSEventOnLogin();

        public override void TriggerEvent(string eventId, bool deleteAfterTriggering = false)
        {
            m_shouldClearRotatingBanners = true;

            base.TriggerEvent(eventId, deleteAfterTriggering);

        }

        protected override void ExecuteDecision(IMSDecision decision)
        {
            switch (decision.Id)
            {
                case MESBase.DECISION_SHOW_POPUPS:
#if UNITY_EDITOR
                    if (m_dontShowPopupsOnEditor) break;
#endif
                    foreach (string izId in decision.InteractionZoneIds)
                    {
                        IMSInteractionZone iZone = interactionZones[izId];
                        iZone.OpeningEvent = decision.ImsEvent;
                        ShowPopup(iZone, AddMode.DontShowIfPopupShown);
                    }
                    break;
                case MESBase.DECISION_SHOW_BANNERS:
                    ShowBanners(decision);
                    break;
                case MESBase.DECISION_OPEN_INVITE:
                    ShowSocialPopup(AddMode.DontShowIfPopupShown,SocialPopup.SocialTab.Invite);
                    break;
                case MESBase.DECISION_OPEN_CASHIER:
                    CardGamesPopupManager.Instance.ShowCashierIMSPopup(AddMode.DontShowIfPopupShown);
                    break;
                case MESBase.DECISION_PIGGY:
                    // Since piggy data is received after login, there might be a situation where the model is not set yet
                    // if this is on a login event
                    if (ModelManager.Instance.GetPiggy()!=null && ModelManager.Instance.GetPiggy().PiggyState != PiggyState.Cooldown)
                        CardGamesPopupManager.Instance.ShowPiggyOfferPopup();
                    break;
                case MESBase.DECISION_PIGGY_CONDITIONAL:
                    // Since piggy data is received after login, there might be a situation where the model is not set yet
                    // if this is on a login event
                    UserPiggyData piggy = ModelManager.Instance.GetPiggy();
                    if (piggy != null)
                    {
                        if (piggy.PiggyState != PiggyState.Cooldown)
                        {
                            if (PiggyController.Instance.ShowTimeReminder(piggy) || piggy.ShowCapacityReminder())
                                CardGamesPopupManager.Instance.ShowPiggyOfferPopup();
                        }
                        else
                            LoggerController.Instance.LogWarning("DECISION_PIGGY_CONDITIONAL arrived even though the piggy on cooldown. potential error");
                    }
                    else
                        LoggerController.Instance.LogError("DECISION_PIGGY_CONDITIONAL arrived even though the user doesn't have a piggy. definitely an error!");
                    break;
                case MESBase.DECISION_OPEN_INBOX:
                    ShowSocialPopup(AddMode.DontShowIfPopupShown, SocialPopup.SocialTab.App_requests);
                    break;
                case MESBase.DECISION_OPEN_SEND_GIFT:
                    ShowSocialPopup(AddMode.DontShowIfPopupShown, SocialPopup.SocialTab.Send);
                    break;
                case MESBase.DECISION_SHOW_PROFILE:
                    ShowProfile();
                    break;
                case MESBase.DECISION_SHOW_AVATAR:
                    CardGamesPopupManager.Instance.ShowMavatarCreationPopup(AddMode.ShowAndRemove);
                    break;
                case MESBase.DECISION_SHOW_ACHHIVMENT:
                    break;
                case MESBase.DECISION_SHOW_SETTING:
                    CardGamesPopupManager.Instance.ShowSettingsPopup(AddMode.DontShowIfPopupShown);
                    break;
                case MESBase.DECISION_SHOW_TROPHY:
                    ShowTrophy(AddMode.DontShowIfPopupShown);
                    break;
                case MESBase.DECISION_SHOW_CHAT_BOT:
                    break;
                case MESBase.DECISION_SHOW_RATE_US:
                    CardGamesPopupManager.Instance.ShowRateQuestionPopup();
                    break;
                case MESBase.DECISION_SHOW_REDEEM_BONUS:
                    ShowRedeemBonus();
                    break;
            }
        }

        private void ShowPopup(IMSInteractionZone iZone, AddMode addMode)
        {
            // Special case - the popup has an action of ACTION_OPEN_APP_STORE and no close button
            // In this case we destroy all the popups and show only this one.
            if (iZone.Action.Id == MESBase.ACTION_OPEN_APP_STORE && iZone.CloseBtnType == "N")
            {
                PopupManager.Instance.RemoveAllPopups();
            }

            // Special case for rewarded video action
            if (iZone.Action.Id == MESBase.ACTION_SHOW_REWARDED_VIDEO ||
                iZone.Action.Id == MESBase.ACTION_SHOW_DAILY_REWARDED_VIDEO ||
                iZone.Action.Id == MESBase.ACTION_SHOW_HOURLY_REWARDED_VIDEO ||
                iZone.Action.Id == MESBase.ACTION_SHOW_LEVELUP_REWARDED_VIDEO)
            {
                VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(ModelManager.Instance.GetUser()); // Init here again if needed
                if (VideoAdController.Instance.IsRewardedVideoCapped())
                    return; // Only return if video is capped
            }

            // Show popup by template
            if (iZone.TemplateId == IMS_OPEN_TABLE_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowIMSOpenTablePopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_RAFFLE_PO_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowRafflePOPopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_BONUS_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowBonusIMSPopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_CHALLENGE_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowIMSChallengePopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_PO_TEMPLATE)
            {
                // Make sure the balance is updated when the popup is closed.
                // Can be used here since there is no "received" popup
                //CardGamesPopupManager.Instance.ShowIMSPOPopup(addMode, iZone, () => UpdateCoinsText());

                // There is a purchase received popup after PO so I don't understand the above comment (Ran 5/5/2020)
                // No need to invoke UpdateCoinsText() manually

                CardGamesPopupManager.Instance.ShowIMSPOPopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_MGAP_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowIMSMGAPPopup(iZone);
            }
            else if (iZone.TemplateId == IMS_CASHIER_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowCashierIMSPopup(addMode);
            }
            else if (iZone.TemplateId == IMS_BONUS_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowBonusIMSPopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_OPEN_CASHIER_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowIMSOpenCashierPopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_CHALLENGE_TEMPLATE)
            {
                CardGamesPopupManager.Instance.ShowIMSChallengePopup(addMode, iZone);
            }
            else if (iZone.TemplateId == IMS_PO_TEMPLATE && iZone.IsAssetBundle)
            {
                CardGamesPopupManager.Instance.ShowIMSPOPopup(addMode, iZone);
            }
            else
            {
                if (iZone.IsAssetBundle)
                    CardGamesPopupManager.Instance.ShowIMSBundlePopup(addMode, iZone);
                else
                    CardGamesPopupManager.Instance.ShowIMSTemplatePopup(addMode, iZone);
            }
        }

        //these are either not implemented in rummy or implemeted differently from spades
        //TODO: when the unimplemented ones get done, move them to CardGames and remove the abstraction (ongoing)
        protected abstract void UpdateCoinsText();
        protected abstract void ShowSocialPopup(AddMode addMode,SocialPopup.SocialTab? startingTab);
        protected abstract void ShowProfile();
        protected abstract void ShowRedeemBonus();
        protected abstract void ShowTrophy(AddMode addMode);

        private void ShowBanners(IMSDecision decision)
        {
            foreach (string izId in decision.InteractionZoneIds)
            {
                IMSInteractionZone iZone = interactionZones[izId];

                switch (iZone.Location)
                {
                    case BANNER_LOBBY_LEFT:
                        break;

                    case BANNER_HEADER_BUY:
                    case BANNER_HEADER_RIGHT_BUTTON:
                    case BANNER_HEADER_LEFT_BUTTON:
                    case BANNER_HEADER_END_GAME_OFFER:
                        SetLobbyIMSBanner(iZone);
                        break;
                    case BANNER_FOOTER_SPECIAL_ROOM:
                        break;
                }
            }
        }

        protected abstract void SetLobbyIMSBanner(IMSInteractionZone iZone);

        public override void ExecuteAction(IMSInteractionZone iZone, int actionParamIndex = 0)
        {
            switch (iZone.Action.Id)
            {
                case MESBase.ACTION_CLOSE_CONTINUE:
                    PopupManager.Instance.HidePopup();
                    break;
                case MESBase.ACTION_OPEN_POPUP:
                    // Get the popup iZone
                    string popupIZoneId = iZone.ChildIds[0];
                    IMSInteractionZone popupIZone = interactionZones[popupIZoneId];

                    // Set the origin meta data - the metadata of the iZone which opens the popup (banner etc.)
                    popupIZone.OriginMetadata = iZone.Metadata;

                    ShowPopup(popupIZone, AddMode.ShowAndRemove);

                    break;
                case MESBase.ACTION_DIRECT_BUY:
                    ExecuteDirectBuyAction(iZone, actionParamIndex);
                    break;
                case ACTION_BUY_ITEM:
                    ExecuteBuyItemAction(iZone);
                    break;
                case MESBase.ACTION_OPEN_APP_STORE:
                    // If the close button is not N, close the popup when clicking since it is an optional update
                    if (iZone.CloseBtnType != "N")
                        PopupManager.Instance.HidePopup();
                    VersionController.Instance.OpenAppStore();
                    break;

                case MESBase.ACTION_OPEN_CASHIER:
                    CardGamesPopupManager.Instance.ShowCashierIMSPopup(PopupManager.AddMode.ShowAndRemove);
                    break;

                case MESBase.ACTION_PIGGY:
                    if (ModelManager.Instance.GetPiggy()!=null && ModelManager.Instance.GetPiggy().PiggyState != PiggyState.Cooldown)
                        CardGamesPopupManager.Instance.ShowPiggyOfferPopup();
                    break;

                case MESBase.ACTION_PIGGY_INFO:
                    CardGamesPopupManager.Instance.ShowPiggyInfoPopup();
                    break;

                case MESBase.ACTION_OPEN_FB_INVITE_FRIENDS:
                    ShowSocialPopup(AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
                    break;
                case MESBase.ACTION_OPEN_NATIVE_INVITE_FRIENDS:
                    SocialController.Instance.ShowNativeInviteWithDefaults();
                    break;
                case MESBase.ACTION_GOTO_ROOM:

                    int room_id = 0; //Convert.ToInt32(material.actionParam.Value);
                    GoToRoom(room_id);
                    break;
                case MESBase.ACTION_OPEN_SEND_GIFT:
                    ShowSocialPopup(AddMode.ShowAndRemove, SocialPopup.SocialTab.Send);
                    break;
                case MESBase.ACTION_OPEN_LEADERBOARD:
                    PopupManager.Instance.HidePopup();
                    ShowLeaderboard();
                    break;
                case MESBase.ACTION_OPEN_CONTEST:
                    CardGamesPopupManager.Instance.ShowContestsPopup(PopupManager.AddMode.ShowAndRemove);
                    break;
                case MESBase.ACTION_OPEN_CONTEST_INFO:
                    CardGamesPopupManager.Instance.ShowContestInfo(PopupManager.AddMode.ShowAndRemove);
                    break;
                case MESBase.ACTION_OPEN_RATEUS:
                    CardGamesPopupManager.Instance.ShowRateQuestionPopup();
                    break;
                case MESBase.ACTION_SHOW_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoBonus, iZone);
                    break;
                case MESBase.ACTION_SHOW_DAILY_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedSlotVideoBonus, iZone);
                    break;
                case MESBase.ACTION_SHOW_HOURLY_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedHourlyVideoBonus, iZone);
                    break;
                case MESBase.ACTION_SHOW_LEVELUP_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoLevelUp, iZone);
                    break;
                case MESBase.ACTION_OPEN_EXTERNAL_URL:

                    string url = Uri.UnescapeDataString(iZone.Action.Values[0].Value);
#if UNITY_WEBGL && !UNITY_EDITOR
                    openBrowserWindow (url);
#else
                    Application.OpenURL(url);
#endif
                    // Close the popup if exists
                    PopupManager.Instance.HidePopup();
                    break;
                case MESBase.ACTION_OPEN_DAILYBONUS:
                    break;
                case MESBase.ACTION_FB_CONNECT:
                    StartCoroutine(DelayedFBConnect());
                    break;
                case MESBase.ACTION_OPEN_INVITE:
                    ShowSocialPopup(AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
                    break;
                case MESBase.ACTION_OPEN_TABLE:
                    HandleOpenTable(iZone.DynamicContent.Content[actionParamIndex]);
                    break;
                case MESBase.ACTION_CLAIM_BONUS:
                case MESBase.ACTION_CLAIM_DYNAMIC_BONUS:
                    BonusController.Instance.ClaimIMSBonus(iZone, (bool success, BonusAwarded bonusAwarded) =>
                    {
                        if (success)
                            CardGamesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.GeneralBonus, PopupManager.AddMode.ShowAndRemove, null, true, bonusAwarded);
                    });
                    break;
                case MESBase.ACTION_SHOW_PROFILE:
                    ShowProfile();
                    break;
                case MESBase.ACTION_SHOW_AVATAR:
                    CardGamesPopupManager.Instance.ShowMavatarCreationPopup(PopupManager.AddMode.ShowAndRemove);
                    break;
                case MESBase.ACTION_SHOW_ACHIEVEMENT:
                    break;
                case MESBase.ACTION_OPEN_ARENA:
                    string arena_id = iZone.Action.Values[actionParamIndex].Value;
                    Arena arena = ModelManager.Instance.GetArena(arena_id);
                    HandleOpenArena(arena);
                    break;
                case MESBase.ACTION_SHOW_SETTING:
                    CardGamesPopupManager.Instance.ShowSettingsPopup(AddMode.ShowAndRemove);
                    break;
                case MESBase.ACTION_SHOW_TROPHY:
                    CardGamesPopupManager.Instance.ShowTrophyRoomPopup(PopupManager.AddMode.ShowAndRemove);
                    break;
                case MESBase.ACTION_OPEN_REDEEM_BONUS:
                    ShowRedeemBonus();
                    break;
                case MESBase.ACTION_SHOW_MISSION_POPUP:
                    MissionController.Instance.MissionButtonClicked(false);
                    break;
                case MESBase.ACTION_OPEN_SIDE_GAMES:
                    {
                        //used to close the win lose view if the mini game is triggered from the end game event
                        MiniGameInitiated();
                        PopupManager.Instance.RemoveAllPopups();
                        PopupManager.Instance.HidePopup();

                        SideGamesController.Instance.StartMiniGame(SideGamesController.SideGames.Slot3);
                        break;
                    }
                case MESBase.ACTION_GOTO_PROMO_ROOM:
                    string roomNum = iZone.Action.Values[actionParamIndex].Value;
                    if (roomNum == "1")
                    {
                        int pos = ModelManager.Instance.Worlds.FindIndex(w => w.WorldType == World.WorldTypes.Promo);
                        GoToRoom(pos + 1);
                    }
                    else if (roomNum == "2")
                    {
                        int pos = ModelManager.Instance.Worlds.FindLastIndex(w => w.WorldType == World.WorldTypes.Promo);
                        GoToRoom(pos + 1);
                    }
                    break;

            }
            if (!iZone.IsBanner || LocalDataController.Instance.GetSettingAsBool("BannersImpressionTracking", false))
                FireClickTracking(iZone, actionParamIndex);
        }

        protected abstract void GoToRoom(int room);
        protected abstract void MiniGameInitiated();
        public abstract void HandleOpenTable(JSONNode node);
        protected abstract void ShowLeaderboard();
        protected abstract void HandleOpenArena(Arena arena);

        public override void ExecuteDirectBuyAction(IMSInteractionZone iZone, int actionParamIndex, bool showPurchasePopup = true)
        {
            List<IMSActionValue> actionVals = iZone.Action.Values;

            IMSActionValue actionValue = actionVals[actionParamIndex];
            // in banners show and keep
            // in popup show and remove
            AddMode addMode;
            if (iZone.IsBanner)
                addMode = AddMode.ShowAndKeep;
            else
                addMode = AddMode.ShowAndRemove;

            CashierController.Instance.BuyPackage(actionValue.Value, new PurchaseTrackingData
            {
                imsMetaData = iZone.Metadata,
                imsOriginMetaData = iZone.OriginMetadata,
                imsActionValue = actionValue,
                popupIndex = iZone.PopupIndex
            }, showPurchasePopup, addMode);
        }

        private void ExecuteBuyItemAction(IMSInteractionZone iZone)
        {
            IMSActionValue imsActionValue = iZone.Action.Values[0];

            // get the cost
            int cost = iZone.DynamicContent.Content[0]["cot"].AsInt;

            HandleBuyUserInventoryItem(imsActionValue.Value, iZone.Metadata, cost);
        }

        protected abstract void HandleBuyUserInventoryItem(string imsActionValue, string iZoneMetadata, int cost);



        protected override string GetImpressionsTrackingURL()
        {
            return LocalDataController.Instance.GetSetting("IMS.TrackingUrl.Impressions");
        }

        protected override string GetClicksTrackingURL()
        {
            return LocalDataController.Instance.GetSetting("IMS.TrackingUrl.Clicks");
        }

        /// <summary>
        /// Delays the FB connect by some fraction of a second. 
        /// This is used to make sure the popup have time to disappear before we go out to Facebook's UI.
        /// </summary>
        private IEnumerator DelayedFBConnect()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            LobbyFBLogin();
        }

        protected abstract void LobbyFBLogin();

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void openBrowserWindow(string url);
#endif

    }
}