﻿using System.Collections.Generic;
using common.ims.model;
using common.views;
using common.ims.views;
using common.ims;

namespace cardGames.ims
{
    public abstract class CardGamesIMSPromoRoomInjector : IMSBannerInjector
    {
        private Sprite headerImageSprite;


        protected override void BannerInject()
        {
            InstantiatePrefab();
        }

        private void InstantiatePrefab()
        {

        }

        protected void BannerClick()
        {
            IMSController.Instance.ExecuteAction(m_iZone);
        }

        public override void InjectGraphics(IMSBannerView_v2 bannerView, GameObject loaded_asset, IMSInteractionZone iZone)
        {
            m_loaded_asset = loaded_asset;
            m_iZone = iZone;
            m_bannerView = bannerView;

            Transform transform_BG = m_loaded_asset.transform.FindDeepChild("ActionButton");
            //For promo rooms without Action Button
            if (transform_BG == null)
                transform_BG = m_loaded_asset.transform;

            // Use pointer down to support external URL actions
            transform_BG.gameObject.AddComponent<PointerDownHandler>();
            transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
            transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
            {
                ExecuteAction();
            });


            transform_BG.gameObject.gameObject.AddComponent<PointerUpHandler>();
            transform_BG.gameObject.gameObject.GetComponent<PointerUpHandler>().OnUp.RemoveAllListeners();
            transform_BG.gameObject.gameObject.GetComponent<PointerUpHandler>().OnUp.AddListener(() =>
            {
                EndSwipe();
            });
        }

        protected override void HandleClick()
        {
            Transform transform_BG = m_loaded_asset.transform.FindDeepChild("ActionButton");
            //For promo rooms without Action Button
            if (transform_BG == null)
                transform_BG = m_loaded_asset.transform;

            if (transform_BG != null)
            {
                // Use pointer down to support external URL actions
                transform_BG.gameObject.AddComponent<PointerDownHandler>();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                {
                    StartSwipe();
                });

                transform_BG.gameObject.AddComponent<PointerUpHandler>();
                transform_BG.gameObject.GetComponent<PointerUpHandler>().OnUp.RemoveAllListeners();
                transform_BG.gameObject.GetComponent<PointerUpHandler>().OnUp.AddListener(() =>
                {
                    EndSwipe();
                });
            }
        }

        private void HandleNestedBanners()
        {
            List<Transform> nestedBanners = new List<Transform>();
            bool foundBanners = m_loaded_asset.transform.FindDeepChildsContains("banner_", nestedBanners);
            if (foundBanners)
            {
                foreach (Transform banner in nestedBanners)
                {
                    banner.GetComponent<RectTransform>().localScale = new Vector3(scaleFactorValue, scaleFactorValue, 1);
                    if (banner.GetComponent<Image>() != null)
                        banner.GetComponent<Image>().enabled = false;
                    GameObject IMSBannerV2_prefab = MonoBehaviour.Instantiate(m_IMSBannerV2_prefab, banner);
                    IMSBannerView_v2 bannerView = IMSBannerV2_prefab.GetComponent<IMSBannerView_v2>();
                    bannerView.Location = banner.name.Replace("banner_", "");
                }
            }
        }

        private void HandlePromoRoomHeader()
        {
            Transform header = m_loaded_asset.transform.FindDeepChild("Header");
            if (header != null)
            {
                header.gameObject.SetActive(false);
                Image headerImage = header.GetComponent<Image>();
                if (headerImage != null)
                {
                    headerImageSprite = headerImage.sprite;
                }
            }


            /*
            Image roomNameSprite = header.GetComponent<Image>();
            Transform RoomName_Image = LobbyController.Instance.Lobby_view.transform.Find("RoomName_Image");
            if (RoomName_Image != null)
            {
                Image nameImage = RoomName_Image.GetComponent<Image>();
                if (nameImage != null && newNameImage != null)
                    nameImage.sprite = newNameImage;
            }
            */
        }

        public override void Inject(IMSBannerView_v2 bannerView, GameObject loaded_asset, IMSInteractionZone iZone, GameObject IMSBannerV2_prefab)
        {
            base.Inject(bannerView, loaded_asset, iZone, IMSBannerV2_prefab);
            HandleNestedBanners();
            HandlePromoRoomHeader();
        }

        public Sprite HeaderImageSprite { get => headerImageSprite; }

        #region game-specific lobby and step-swipe implementation

        protected abstract void StartSwipe();
        protected abstract void ExecuteAction();
        protected abstract void EndSwipe();

        #endregion
    }
}