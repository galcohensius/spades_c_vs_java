﻿using cardGames.comm;
using cardGames.models;
using cardGames.views.popups;
using cardGames.views.social;
using common.controllers;
using common.facebook;
using common.facebook.models;
using common.models;
using common.utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.controllers
{
    public class SocialController : MonoBehaviour
    {

#if DEV_MODE
		
		public const int REQUEST_EXPIRE_MINUTES = 2;
		// 2 min
		public const int GIFT_SEND_INTERVAL_SEC = 2 * 60;
		// 2 min
		

#else
        public const int REQUEST_EXPIRE_MINUTES = 24 * 60;
        // 24 hours
        public const int GIFT_SEND_INTERVAL_SEC = 24 * 60 * 60;
        // 24 hours
#endif

        public const int APP_REQUEST_REFRESH_SEC = 30;

        
        string m_inviteTitle;
        string m_inviteBody;
        string m_inviteLink;

        [Header("Visuals")]
        [SerializeField] Sprite m_social_selected = null;
        [SerializeField] Sprite m_social_un_selected = null;

        public static SocialController Instance;

        [SerializeField] GameObject m_FB_Friend_Group_Item_Object = null;
        [SerializeField] GameObject m_FB_Friend_Item_Object = null;

        [SerializeField] GameObject m_FB_Gift_Item_Object = null;

        protected SocialPopup m_social_popup = null;

        protected JSONObject m_sent_gifts_Json;

        FBRequestData m_last_claim_gift_fb_req_data = null;
        Coroutine m_check_app_requests_routine = null;

        bool m_gift_being_claimed = false;
        List<FBRequestData> m_temp_requests_list = new List<FBRequestData>();


        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_inviteTitle = ldc.GetSetting("InviteTitle","");
                m_inviteBody = ldc.GetSetting("InviteBody","");
                m_inviteLink = ldc.GetSetting("InviteLink","");
            };
        }

        public void ShowSocialPopup(PopupManager.AddMode add_mode, SocialPopup.SocialTab? starting_tab = null)
        {
            m_temp_requests_list.Clear();

            if (FacebookController.Instance.IsLoggedIn() && !FacebookController.Instance.CheckPermission(FacebookController.Permissions.USER_FRIENDS))
            {
                FacebookController.Instance.GrantPermission(FacebookController.Permissions.USER_FRIENDS, (bool granted) => {
                    if (granted)
                    {
                        RequestFacebookSocialData();
                        ShowSocialPopup(add_mode, starting_tab);
                    }
                });

                return;
            }

            if (starting_tab == null)
            {
                SocialPopup.SocialTab active_tab = SocialPopup.SocialTab.Invite;

                if (ModelManager.Instance.Social_model.App_Requests.Count > 0)
                    active_tab = SocialPopup.SocialTab.App_requests;

                m_social_popup = CardGamesPopupManager.Instance.ShowSocialPopup(add_mode, active_tab);
            }
            else
            {
                m_social_popup = CardGamesPopupManager.Instance.ShowSocialPopup(add_mode, (SocialPopup.SocialTab)starting_tab);
            }

            m_social_popup.Send_gifts_button.interactable = false;

            HandleSendGiftButtonOnShowPopup();
        }

       

        public void RequestFacebookSocialData()
        {
            //make a new social model object

            m_sent_gifts_Json = HandleSendGiftsJson();

            FacebookController.Instance.GetUserAppFriends(UserAppFriendCallback);
            FacebookController.Instance.GetUserInviteFriends(UserInviteFriendCallback);
        }

       

        public void StartAppRequestCheck()
        {
            StopAppRequestCheck();
            m_check_app_requests_routine = StartCoroutine(GetUserAppRequests());
        }

        public void StopAppRequestCheck()
        {
            if (m_check_app_requests_routine != null)
                StopCoroutine(m_check_app_requests_routine);
        }

        private IEnumerator GetUserAppRequests()
        {
            bool done = false;

            //Debug.Log ("GetUserAppRequest");

            while (!done)
            {
                // Debug.Log ("Social Controller requesting App requests");
                FacebookController.Instance.GetUserAppRequests(UserAppRequestCallback);

                yield return new WaitForSecondsRealtime(APP_REQUEST_REFRESH_SEC);
            }

        }

        public void CheckSelectAllIndication()
        {
            bool all_selected = true;
            foreach (FacebookData item in ModelManager.Instance.Social_model.AppFriends)
            {
                if (!item.Selected)
                {
                    all_selected = false;
                    continue;
                }

            }

            m_social_popup.App_friends_object.GetComponent<FBListFilterView>().ChangeSelectAllIndication(all_selected);
        }

        public void UpdateSendGiftButton()
        {
            bool enable_button = false;
            foreach (FacebookData item in ModelManager.Instance.Social_model.AppFriends)
            {
                if (item.Selected)
                    enable_button = true;
            }


            if (m_social_popup != null)
            {
                m_social_popup.Send_gifts_button.interactable = enable_button;
                HandleSendGiftButtonOnShowPopup();
            }

        }

        public void UserAppRequestCallback(List<FBRequestData> app_requests)
        {
            Dictionary<string, FBRequestData> data = new Dictionary<string, FBRequestData>();
            FBRequestData inviterReq = null;


            for (int j = 0; j < app_requests.Count; j++)
            {
                if (app_requests[j].Data != null && (string)app_requests[j].Data["type"] == "I" && ModelManager.Instance.GetUser() != null)
                { //TODO: fix bug here
                    inviterReq = app_requests[j];

                    if (j != 0)
                    {
                        FacebookController.Instance.DeleteAppRequest(app_requests[j].RequestId, null);
                    }

                    app_requests.RemoveAt(j);
                }
            }

            for (int i = app_requests.Count - 1; i >= 0; i--)
            {

                if (app_requests[i].Data != null && (string)app_requests[i].Data["type"] == "G" && app_requests[i].Sender_id != null)
                {
                    if (data.ContainsKey(app_requests[i].Sender_id))
                    {
                        //need to see who has a newer time stamp
                        LoggerController.Instance.Log("Second gift from same Sender: " + app_requests[i].Sender_id);

                        FBRequestData fb_req_data;
                        data.TryGetValue(app_requests[i].Sender_id, out fb_req_data);//curr value in the dictionary

                        if (fb_req_data.Create_time < app_requests[i].Create_time)
                        {
                            data.Remove(app_requests[i].Sender_id);
                            data.Add(app_requests[i].Sender_id, app_requests[i]);
                            LoggerController.Instance.Log("Consolidating gifts, from: " + app_requests[i].Sender_id);
                        }
                        else
                        {
                            FacebookController.Instance.DeleteAppRequest(app_requests[i].RequestId, null);
                            app_requests.RemoveAt(i);
                        }

                    }
                    else
                    {

                        TimeSpan delta = DateTime.Now - app_requests[i].Create_time;

                        if (delta.TotalMinutes <= REQUEST_EXPIRE_MINUTES)
                        {
                            data.Add(app_requests[i].Sender_id, app_requests[i]);
                        }
                        else
                        {
                            LoggerController.Instance.Log("Removing expired gift, from sender: " + app_requests[i].Sender_id);
                            FacebookController.Instance.DeleteAppRequest(app_requests[i].RequestId, null);
                            app_requests.RemoveAt(i);
                        }
                    }
                }
            }

            if (inviterReq != null)
            {
                CardGamesCommManager.Instance.AddInvitedFriend((int)inviterReq.Data["userId"], (bool success, JSONNode response) => {
                    if (success)
                    {
                        FacebookController.Instance.DeleteAppRequest(inviterReq.RequestId, null);
                    }
                    else
                    {
                        //handle error
                    }
                });
            }

            ModelManager.Instance.Social_model.RemoveAllAppRequests();

            ModelManager.Instance.Social_model.AddAppRequest(data.Values);


            if (m_social_popup != null)
                m_social_popup.BuildAppRequestList();

        }


        public void UserAppFriendCallback(List<FacebookData> friends)
        {
            List<FacebookData> filtered_friends = new List<FacebookData>();

            foreach (FacebookData friend in friends)
            {
                if (CanSendGift(friend.User_id))
                {
                    filtered_friends.Add(friend);
                }
            }

            HandleSaveSendGiftsJson();

            ModelManager.Instance.Social_model.AppFriends = filtered_friends;

            if (m_social_popup != null)
                m_social_popup.BuildUserAppFriendstList("");
        }

      

        private bool CanSendGift(string friend_id)
        {
            if (m_sent_gifts_Json.HasChild(friend_id))
            {
                int last_gift_sent_time = m_sent_gifts_Json[friend_id].AsInt;

                if (DateUtils.TotalSeconds(DateTime.Now) - last_gift_sent_time > GIFT_SEND_INTERVAL_SEC)
                {
                    m_sent_gifts_Json.Remove(friend_id);
                    return true;
                }
            }
            else
            {
                return true;
            }

            return false;
        }

        public void UserInviteFriendCallback(List<FacebookData> friends)
        {
            ModelManager.Instance.Social_model.InvitableFriends = friends;
        }



        public void InviteButtonClicked()
        {
            LoggerController.Instance.Log("Invite Button Clicked");

            FacebookController.Instance.SendInviteFriends(ModelManager.Instance.GetUser().GetID(),m_inviteBody, (List<string> results) => {

                LoggerController.Instance.Log("Received results from app request : " + results.Count);
                if (results.Count > 0)
                {

                    for (int i = ModelManager.Instance.Social_model.InvitableFriends.Count - 1; i >= 0; i--)
                    {
                        if (ModelManager.Instance.Social_model.InvitableFriends[i].Selected)
                            ModelManager.Instance.Social_model.InvitableFriends.RemoveAt(i);
                    }
                }

            });


        }

        public void SendGiftButtonClicked(Action send_gifts_callback)
        {

            LoggerController.Instance.Log("send gift Button Clicked");

            List<string> selected_users = new List<string>();

            for (int i = 0; i < ModelManager.Instance.Social_model.AppFriends.Count; i++)
            {
                if (ModelManager.Instance.Social_model.AppFriends[i].Selected)
                    selected_users.Add(ModelManager.Instance.Social_model.AppFriends[i].User_id);
            }

            if (selected_users.Count > 0)
            {
                FacebookController.Instance.SendGiftToFriends(FacebookController.Instance.UserFacebookData?.First_name, selected_users, (int results_count) => {

                    LoggerController.Instance.Log("Received results from app request for gifts : " + results_count);
                    if (results_count > 0)
                    {
                        List<FacebookData> app_friends = ModelManager.Instance.Social_model.AppFriends;

                        for (int i = app_friends.Count - 1; i >= 0; i--)
                        {
                            if (app_friends[i].Selected)
                            {

                                m_sent_gifts_Json[app_friends[i].User_id].AsInt = (int)DateUtils.TotalSeconds(DateTime.Now);
                                app_friends.RemoveAt(i);

                            }
                        }
                        HandleSaveSendGiftsJson();
                        m_social_popup.BuildUserAppFriendstList("");

                    }

                });
            }

            //turn off the send gifts spades holder
            if (send_gifts_callback != null)
                send_gifts_callback();
        }

        public void CollectAllGiftsClicked(Action<bool, int> claim_done)
        {

            List<string> sendersIds = new List<string>();

            foreach (FBRequestData item in ModelManager.Instance.Social_model.App_Requests)
            {
                sendersIds.Add(item.Sender_id);

                FacebookController.Instance.DeleteAppRequest(item.RequestId, null);
            }

            //store the model request data in temp location so we can manage the send gifts back after sending the gifts is done
            m_temp_requests_list.Clear();

            foreach (FBRequestData item in ModelManager.Instance.Social_model.App_Requests)
            {
                m_temp_requests_list.Add(item);
            }

            //delete from internal model
            ModelManager.Instance.Social_model.RemoveAllAppRequests();

            BonusController.Instance.ClaimBonus(Bonus.BonusTypes.Gift, 0, sendersIds.Count, sendersIds, 0, (bool success, BonusAwarded bonusAwarded) => {
                claim_done(success, bonusAwarded.GoodsList.CoinsValue);
            });

        }

        public void CollectGiftBonus(FBRequestData fb_req_data, GameObject coin_icon, Action<bool> claim_done)
        {
            m_last_claim_gift_fb_req_data = fb_req_data;

            FacebookController.Instance.DeleteAppRequest(fb_req_data.RequestId, null);

            //store the model request data in temp location so we can manage the send gifts back after sending the gifts is done
            m_temp_requests_list.Clear();

            m_temp_requests_list.Add(fb_req_data);

            //delete from internal model
            ModelManager.Instance.Social_model.RemoveAppRequest(fb_req_data);

            List<String> senderIds = new List<string>();
            senderIds.Add(fb_req_data.Sender_id);

            BonusController.Instance.ClaimBonus(Bonus.BonusTypes.Gift, 0, 1, senderIds, 0, (bool success, BonusAwarded bonusAwarded) => {
                if (success)
                    m_social_popup.FlyCoinsComet(bonusAwarded.GoodsList.CoinsValue, coin_icon);

            });

        }

        public void PostClaimGifts()
        {

            List<string> sender_ids = new List<string>();

            foreach (FBRequestData item in m_temp_requests_list)
            {
                if (CanSendGift(item.Sender_id))
                    sender_ids.Add(item.Sender_id);
            }

            m_social_popup.BuildAppRequestList();

            if (sender_ids.Count > 0)
            {
                FacebookController.Instance.SendGiftToFriends(FacebookController.Instance.UserFacebookData?.First_name, sender_ids, (int result_count) => {
                    if (result_count > 0)
                    {
                        foreach (var id in sender_ids)
                        {
                            m_sent_gifts_Json[id].AsInt = (int)DateUtils.TotalSeconds(DateTime.Now);
                        }

                        FacebookController.Instance.GetUserAppFriends(UserAppFriendCallback);

                    }

                    HandleSaveSendGiftsJson();

                });

            }

            //this makes the button of collect all go back to normal state from spades_holder state
            if (m_social_popup.Collect_all_done_callback != null)
                m_social_popup.Collect_all_done_callback();

        }

        public void DeleteInviteItem(string item_key)
        {
            int index = ModelManager.Instance.Social_model.InvitableFriends.FindIndex((FacebookData obj) => obj.User_id == item_key);

            if (index != -1)
                ModelManager.Instance.Social_model.InvitableFriends.RemoveAt(index);

        }

        private void HandleSaveSendGiftsJson()
        {
            CardGamesStateController.Instance.SaveSendGiftsJson(m_sent_gifts_Json);
        }

        private void HandleSendGiftButtonOnShowPopup()
        {
            ViewUtils.EnableDisableButtonText(m_social_popup.Send_gifts_button);
        }

        private JSONObject HandleSendGiftsJson()
        {
            return (JSONObject)JSONNode.Parse(CardGamesStateController.Instance.LoadSendGiftsJson());
        }


        public void ShowNativeInviteWithDefaults()
        {
            string title = m_inviteTitle;
            string body = String.Format(m_inviteBody, "Native", ModelManager.Instance.GetUser().GetID());

#if UNITY_ANDROID
            NativeShareManager.ShowNativeAndroidShare(body, title);

#elif UNITY_IOS

            NativeShareManager.ShowNativeIosShare(body, title);

#endif
        }
       


        public GameObject FB_Friend_Item_Object
        {
            get
            {
                return m_FB_Friend_Item_Object;
            }
        }

        public GameObject FB_Friend_Group_Item_Object
        {
            get
            {
                return m_FB_Friend_Group_Item_Object;
            }
        }

        public GameObject FB_Gift_Item_Object
        {
            get
            {
                return m_FB_Gift_Item_Object;
            }
        }

        public Sprite Social_selected
        {
            get
            {
                return m_social_selected;
            }
        }

        public Sprite Social_un_selected
        {
            get
            {
                return m_social_un_selected;
            }
        }

        public SocialPopup Social_popup
        {
            get
            {
                return m_social_popup;
            }
        }

        public bool Gift_being_claimed
        {
            get
            {
                return m_gift_being_claimed;
            }
            set
            {
                m_gift_being_claimed = value;
            }
        }

        public string InviteTitle { get => m_inviteTitle;}
        public string InviteBody { get => m_inviteBody; }
        public string InviteBodyAndLink { get => m_inviteBody+" "+m_inviteLink; }
        public string InviteLink { get => m_inviteLink; }
    }
}
