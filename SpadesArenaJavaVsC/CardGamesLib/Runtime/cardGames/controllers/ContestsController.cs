using System.Collections.Generic;
using cardGames.models.contests;
using cardGames.comm;
using cardGames.models;
using System.Collections;
using System;
using common.utils;
using common.controllers;
using common.models;
using cardGames.views;

namespace cardGames.controllers
{
    /* 
     * This class is taking care of the Contests Controller
     */
    public class ContestsController : MonoBehaviour
    {
        #region Const Members
        public const int DATA_REFRESH_INTERVAL = 60; //sec
        private const int LEADERBOARD_REFRESH_INTERVAL = 30; // should be something like 30 sec

        const string SCHEDULE_NOTIF = "Contest_Schedule_Notifications";
        const string IN_THE_MONEY_SCHEDULE_NOTIF = "Contest_Schedule_Notifications";

#if QA_MODE || DEV_MODE
        public const int ONE_HOUR = 180;
        public const int TWELVE_HOURS = 1200;

#else
        public const int ONE_HOUR = 3600;
        public const int TWELVE_HOURS = 3600*12;

#endif
        #endregion

        #region SerializeField Members
        // Fake data
        [SerializeField] TextAsset m_full_contests_data;
        [SerializeField] TextAsset m_leaderboard_contests_data;
        [SerializeField] TextAsset m_mini_leaderboard_contests_data;
        [SerializeField] TextAsset m_mini_leaderboard_contests_data_end;
        [SerializeField] TextAsset m_dynamic_contests_data;
        [SerializeField] TextAsset m_paytable_data;
        #endregion

        #region Private Members
        Coroutine m_dynamic_data_coroutine;
        Coroutine m_get_contests_coroutine;
        Coroutine m_get_contests_earliest_coroutine;
        Coroutine m_get_contests_awards_coroutine;
        CardGamesModelParser parser;
        DateTime m_time_of_last_pulling;
        Dictionary<int, DateTime> m_time_of_last_pulling_per_contest = new Dictionary<int, DateTime>();
        Action m_SetViewData;
        Action m_OnContestInfoShown;
        public Action On_ClaimDataAvailble;
        #endregion

        #region Static Members
        public static ContestsController Instance;
        #endregion

        //for claim
        private List<ClaimContestData> m_to_claim_contests = new List<ClaimContestData>();
        CardGamesCommManager.ContestClaimCompleted m_onContestCompleted;
        private int min_contests_level = 20;

        //for contest lobby hats
        [SerializeField] List<GameObject> m_contest_lobby_hats;
        public List<GameObject> Contest_lobby_hats { get => m_contest_lobby_hats; set => m_contest_lobby_hats = value; }

        public TextAsset Dummy_Award_contests_data;

        public virtual void Awake()
        {
            Instance = this;

            m_time_of_last_pulling = DateTime.Now.AddYears(-100); // add really far time

        }

        private void Start()
        {
            parser = CardGamesCommManager.Instance.GetModelParser() as CardGamesModelParser;

            // Set contests min level
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                min_contests_level = ldc.GetSettingAsInt("MinContestsLevel", 20);
            };
        }

        public void OnLogin()
        {
            ModelManager.Instance.ContestsLists.ClearActiveContests();

            OnContestsDataArrived(true);

            RequestContestDynamicData();
        }

        public void HandleLevelUp()
        {
            LoggerController.Instance.Log(LoggerController.Module.Contest, "Level Up was made > getting ready for loading the updated contests");
            CardGamesCommManager.Instance.GetContests(true, true, true, OnContestsDataArrived);
        }

        public void SwitchRemindMeMode(Contest contest, bool isOn)
        {
            contest.Remind_me = isOn;
            if (contest.Remind_me)
            {
                CardGamesStateController.Instance.SetContestRemindMe(contest.Id, true);

                // schedule local notification
                NotificationController.Instance.ScheduleContestNotification(contest, contest.Time_to_switch_status - DateTime.Now);
            }
            else
            {
                CardGamesStateController.Instance.SetContestRemindMe(contest.Id, false);

                NotificationController.Instance.CancelContestNotification();
            }
        }

        public List<Contest> GetContestListByStatus(Contest.ContestStatus contestStatus)
        {
            List<Contest> result = new List<Contest>();

            switch (contestStatus)
            {
                case Contest.ContestStatus.Completed:
                    result = ModelManager.Instance.ContestsLists.CompletedContests;
                    break;

                case Contest.ContestStatus.Ongoing:
                    result = ModelManager.Instance.ContestsLists.OngoingContests;
                    break;

                case Contest.ContestStatus.Future:
                    result = ModelManager.Instance.ContestsLists.FutureContests;
                    break;
            }
            return result;
        }

        public void ShowContestsPopup()
        {
            if (!UserOverContestMinLevel())
            {
                PopupManager.Instance.ShowMessagePopup("CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + min_contests_level + "!", PopupManager.AddMode.ShowAndRemove, true);
                return;
            }

            if (m_to_claim_contests.Count > 0)
            {
                CardGamesPopupManager.Instance.ShowContestsWinPopup(m_to_claim_contests, PopupManager.AddMode.DontShowIfPopupShown, null);
                return;
            }

            if (ModelManager.Instance.ContestsLists != null)
                CardGamesPopupManager.Instance.ShowContestsPopup(common.controllers.PopupManager.AddMode.ShowAndRemove);
        }

        public void GetContestPaytableComplete(bool success, ContestPaytable contestPaytable, JSONNode paytableJson)
        {
            if (!success)
                return;

            // save cached data
            LocalDataController.Instance.SaveJSONFile(paytableJson, "/paytables/paytableData_" + contestPaytable.Id + ".json");

            // Find the related contests and set the paytable
            foreach (Contest contest in ModelManager.Instance.ContestsLists.AllContests)
            {
                if (contest.Coins_paytable_id == contestPaytable.Id)
                    contest.Contest_Paytable = contestPaytable;
            }
        }

        public void ShowContestLeaderboardPopup(Contest contest)
        {
            if (contest == null)
                return;

            if (!m_time_of_last_pulling_per_contest.ContainsKey(contest.Id))
            {
                m_time_of_last_pulling_per_contest[contest.Id] = DateTime.Now.AddYears(-100);// add really far time
            }

            // Time based caching mechanism 
            if (contest.Contest_players == null || (DateTime.Now - m_time_of_last_pulling_per_contest[contest.Id]).TotalSeconds > LEADERBOARD_REFRESH_INTERVAL)
            {
                contest.Contest_players = null;
                m_time_of_last_pulling_per_contest[contest.Id] = DateTime.Now;
                CardGamesCommManager.Instance.GetContestsLeaderboard(contest.Id, GetContestLeaderboardComplete);
            }

            ContestsPopupView.Instance.SetPageToShow(ContestsPopupView.ShowPage.Leaderboard, contest);
        }

        /// <summary>
        /// Calculating the prize with the percentage from the total number of players
        /// </summary>
        /// <param name="win_percentage"></param>
        /// <param name="totalPlayers"></param>
        /// <returns></returns>
        public float CalculatePrize(Contest contest, ContestPaytableLine contestPaytableLine)
        {
            return (contest.Contest_jackpot.Current_value * contestPaytableLine.PrizePercentage / 100);
        }

        public ContestPaytableLine FindNextRankAndPrize(Contest contest)
        {
            ActiveContest activeContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
            ContestPaytableColumn cpc = FindActivePaytable(contest);

            ContestPlayer contestPlayer = activeContest.Current_playar_in_contest;
            if (contestPlayer == null)
            {
                // Player is not in the contest yet, the prize line is the last
                return cpc.LastLine;
            }

            if (contestPlayer.Rank <= cpc.PayLines[0].MaxRank)
            {
                // Player is at the top most line
                return null;
            }

            for (int i = 1; i < cpc.PayLines.Count; i++)
            {
                if (contestPlayer.Rank <= cpc.PayLines[i].MaxRank)
                    return cpc.PayLines[i - 1];
            }

            return cpc.LastLine;
        }

        public ContestPaytableColumn FindActivePaytable(Contest contest)
        {
            foreach (ContestPaytableColumn contestPaytableColumn in contest.Contest_Paytable.PayColumns)
            {
                if (contest.Total_players >= contestPaytableColumn.MinPlayers && contest.Total_players <= contestPaytableColumn.MaxPlayers)
                {
                    return contestPaytableColumn;
                }
            }
            throw new Exception("No paytable column found, the paytable is fucked up! Total players: " + contest.Total_players);
        }

        /// <summary>
        /// Looking in the relevant paytable and finds the correct prize for each contest player
        /// </summary>
        /// <param name="contest"></param>
        /// <param name="contestPlayer"></param>
        /// <returns></returns>
        public float FindPrizeMatch(Contest contest, ContestPlayer contestPlayer)
        {
            foreach (ContestPaytableColumn contestPaytableColumn in contest.Contest_Paytable.PayColumns)
            {
                if (contest.Total_players >= contestPaytableColumn.MinPlayers && contest.Total_players <= contestPaytableColumn.MaxPlayers)
                {
                    foreach (ContestPaytableLine contestPaytableLine in contestPaytableColumn.PayLines)
                    {
                        if (contestPlayer.Rank >= contestPaytableLine.MinRank && contestPlayer.Rank <= contestPaytableLine.MaxRank)
                            return CalculatePrize(contest, contestPaytableLine);
                    }
                }
            }

            return 0f;
        }

        /// <summary>
        /// Set playerpref for if the player is in the money or not
        /// </summary>
        /// <param name="contest"></param>
        /// <param name="contestPlayer"></param>
        /// <returns></returns>
        public bool IsContestPlayerInTheMoney(Contest contest, ContestPlayer contestPlayer)
        {
            ContestPaytableColumn cpc = FindActivePaytable(contest);
            int lowestRankForPrize = cpc.LastLine.MaxRank;
            return contestPlayer.Rank <= lowestRankForPrize;
        }

        public void HandleNotificationInTheMoney(Contest contest)
        {
            // if this contest was already scheduled, don't cancel and reschedule it.
            // schedule local notification if remind me is turned on
            TimeSpan timeToEnd = contest.Time_to_switch_status - DateTime.Now;

            if (timeToEnd.TotalSeconds <= ONE_HOUR) // Do not send if there is less than an hour
                return;

            // Shcedule 1 hour notification
            NotificationController.Instance.ScheduleContestNotificationInTheMoney(contest, timeToEnd - TimeSpan.FromSeconds(ONE_HOUR), true);

            if (timeToEnd.TotalSeconds > TWELVE_HOURS)
                // Shcedule 12 hour notification
                NotificationController.Instance.ScheduleContestNotificationInTheMoney(contest, timeToEnd - TimeSpan.FromSeconds(TWELVE_HOURS), false);
        }

        private void GetContestLeaderboardComplete(bool success, int contest_id, List<ContestPlayer> contestPlayers)
        {
            Contest contest = ModelManager.Instance.ContestsLists.GetContestByID(contest_id);
            if (contest == null)
                return;

            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest_id);

            if (success)
            {
                aContest.Full_player_ids_snapshot = SetPlayersRanksMovement(aContest.Full_player_ids_snapshot, contestPlayers, true);

                contest.Contest_players = contestPlayers;
            }
        }

        public Dictionary<int, int> SetPlayersRanksMovement(Dictionary<int, int> snapshot, List<ContestPlayer> contestPlayers, bool newIsUp = false)
        {
            Dictionary<int, int> newSnapshot = new Dictionary<int, int>();
            foreach (ContestPlayer player in contestPlayers)
            {
                if (snapshot != null)
                {
                    if (snapshot.ContainsKey(player.Id))
                    {

                        if (player.Rank > snapshot[player.Id])
                        {
                            player.Rank_movement = ContestPlayer.RankMovement.Down;
                        }
                        else if (player.Rank < snapshot[player.Id])
                        {
                            player.Rank_movement = ContestPlayer.RankMovement.Up;
                        }
                    }
                    else if (newIsUp)
                    {
                        player.Rank_movement = ContestPlayer.RankMovement.Up;
                    }
                }

                // Update the new snapshot
                newSnapshot.Add(player.Id, player.Rank);
            }

            return newSnapshot;
        }

        public void ClaimContest(CardGamesCommManager.ContestClaimCompleted onClaimCompleted)
        {
            m_onContestCompleted = onClaimCompleted;
#if UNITY_EDITOR
            if (CardGamesCommManager.Instance.m_contestsFakeData)
            {
                BonusAwarded bonusAwarded = new BonusAwarded();
                bonusAwarded.GoodsList.Coins.Add(350);
                BonusClaimed(true, ModelManager.Instance.GetUser().GetCoins() + 350, bonusAwarded);
            }
            else
            {
                CardGamesCommManager.Instance.ClaimContestReward(m_to_claim_contests[0].Contest_id, BonusClaimed);
            }
#else
            CardGamesCommManager.Instance.ClaimContestReward(m_to_claim_contests[0].Contest_id, BonusClaimed);
#endif
        }

        private void BonusClaimed(bool success, int new_balance, BonusAwarded bonusAwarded)
        {
            if (m_to_claim_contests.Count > 0)
                m_to_claim_contests.RemoveAt(0);

            if (!success)
                m_to_claim_contests.Clear();

            m_onContestCompleted?.Invoke(success, new_balance, bonusAwarded);
        }

        public void OnContestsDataArrived(bool success)
        {
            if (!success)
                return;

            ContestsList contestsList = ModelManager.Instance.ContestsLists;

            // Find the earliest status time
            DateTime earliestStatusTime = DateTime.Now.AddMinutes(15); // Init with a delay if there are no contests we'll check again in 15 minutes

            Contest earliestGraceTimeContest = null;

            foreach (Contest contest in contestsList.OngoingContests)
            {
                if (contest.Time_to_switch_status.AddSeconds(contest.GraceTime) < earliestStatusTime)
                {
                    earliestStatusTime = contest.Time_to_switch_status.AddSeconds(contest.GraceTime + 2); // Add 2 second so we will not fall in the 1 second delay between contests
                    earliestGraceTimeContest = contest;
                }
            }

            if (earliestGraceTimeContest != null)
                m_get_contests_earliest_coroutine = StartCoroutine(GraceTimeCountDown(earliestGraceTimeContest));

            CardGamesStateController.Instance.RemovedOldContestRemindMe(contestsList.FutureContests);
            foreach (Contest contest in contestsList.FutureContests)
            {
                // comparing the difference between two contests - need to add 10 seconds since there is 1 second delay between contests
                // for example: one contest ends in 00:10 seconds, another starts in 00:12 seconds
                // Than the refresh will be in 00:20 seconds
                if (contest.Time_to_switch_status < earliestStatusTime.AddSeconds(10))
                    earliestStatusTime = contest.Time_to_switch_status.AddSeconds(2); // Add 2 second so we will not fall in the 1 second delay between contests
                // check the remind me which is store in the player prefs
                contest.Remind_me = CardGamesStateController.Instance.IsContestRemindMe(contest.Id);

                // if this contest was already scheduled, don't cancel and reschedule it.
                if (!contest.Remind_me)
                {
                    CardGamesStateController.Instance.SetContestRemindMe(contest.Id, false);
                    // cancel the notification anyway
                    NotificationController.Instance.CancelContestNotification();
                    // schedule local notification
                    if (contest.Remind_me)
                        NotificationController.Instance.ScheduleContestNotification(contest, contest.Time_to_switch_status - DateTime.Now);
                }
            }

            // Start an update coroutine for the found time
            // Stop previous coroutine
            if (m_get_contests_coroutine != null)
                StopCoroutine(m_get_contests_coroutine);

            // Start the next contests update coroutine according to the earliest status change time
            long interval = DateUtils.TotalSeconds(earliestStatusTime);
            if (interval >= 0)
            {
                // In order to prevent all clients requesting the getContests command at the same time, add a
                // random interval
                int randomFactor = UnityEngine.Random.Range(1, 6);

                m_get_contests_coroutine = StartCoroutine(UpdateContestsData(interval + randomFactor));
            }

            StartDynamicDataRequest();

            // paytable
            LoadAndSavePaytable(contestsList.AllContests);
        }

        private IEnumerator GraceTimeCountDown(Contest contest)
        {
            TimeSpan timeToWait = (contest.Time_to_switch_status.AddSeconds(contest.GraceTime)) - DateTime.Now;

            yield return new WaitForSecondsRealtime((float)timeToWait.TotalSeconds);

            ActiveContest activeContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
            ContestPlayer contestPlayer = activeContest.Current_playar_in_contest;
            if (contestPlayer != null)
            {
                if (IsContestPlayerInTheMoney(contest, contestPlayer))
                    CardGamesCommManager.Instance.GetContestAwards(GetContestAwardsComplete);
                    // in the money...
            }
        }

        public void SetFakeAwards()
        {
            CardGamesCommManager.Instance.GetContestAwards(GetContestAwardsComplete);
        }

        private void GetContestAwardsComplete(bool success, List<ClaimContestData> claimContestData)
        {
            foreach (ClaimContestData ccd in claimContestData)
            {
                //extra measure to ensure contest prize isnt 0
                if (ccd.Contest_coins_prize > 0)
                {
                    // Check if the fucker is not already listed, by looking at the contest id
                    if (m_to_claim_contests.Find(c => c.Contest_id == ccd.Contest_id) == null)
                        m_to_claim_contests.Add(ccd);
                }
            }
            On_ClaimDataAvailble?.Invoke();
        }
        public void LoadAndSavePaytable(List<Contest> contestList)
        {
            HashSet<int> contestIdsToLoad = new HashSet<int>();
            foreach (Contest contest in contestList)
            {
                foreach (int paytableId in contest.Paytable_Ids)
                {
                    JSONNode paytable_data = LocalDataController.Instance.LoadJSONFile("paytables/paytableData_" + paytableId + ".json");

                    if (paytable_data == null)
                        contestIdsToLoad.Add(paytableId);
                    else
                        contest.Contest_Paytable = parser.ParsePaytable(paytable_data);
                }
            }
            // Load missing paytables
            foreach (int paytableId in contestIdsToLoad)
                CardGamesCommManager.Instance.GetContestsPaytable(paytableId, GetContestPaytableComplete);
        }

        private IEnumerator UpdateContestsData(long intervalSec)
        {
            yield return new WaitForSecondsRealtime(intervalSec);
            CardGamesCommManager.Instance.GetContests(true, true, true, OnContestsDataArrived);
        }

        private void StartDynamicDataRequest()
        {
            if (m_dynamic_data_coroutine != null)
                StopCoroutine(m_dynamic_data_coroutine);

            m_dynamic_data_coroutine = StartCoroutine(RequestContestDynamicData());
        }


 
        /// <summary>
        /// Finds all contests for a range of buy ins. Doesn't return calculating results contests
        /// </summary>
        public List<Contest> GetOnGoingContestsInRange(int min, int max)
        {
            List<Contest> temp = new List<Contest>();

            ContestsList contestsList = ModelManager.Instance.ContestsLists;

            foreach (Contest contest in contestsList.OngoingContests)
            {
                if (contest.TableFilterData.MaxBuyIn >= min && contest.TableFilterData.MinBuyIn <= max)
                    temp.Add(contest);
            }

            return temp;
        }

       


        private IEnumerator RequestContestDynamicData()
        {
            yield return new WaitForSeconds(DATA_REFRESH_INTERVAL);

            CardGamesCommManager.Instance.GetContestsDynamicData(ModelManager.Instance.ContestsLists.OngoingContests, GetContestDynamicDataComplete);
        }

        private void GetContestDynamicDataComplete(bool success, List<ContestDynamicData> dynamicData)
        {
            if (success)
            {
                foreach (ContestDynamicData contest_data in dynamicData)
                {
                    Contest contest = ModelManager.Instance.ContestsLists.GetContestByID(contest_data.Contest_id);
                    if (contest != null)
                        contest.UpdateDynamicData(contest_data.Curr_jackpot_value, contest_data.Rank, contest_data.Total_players);
                }
            }

            StartDynamicDataRequest();
        }

        private bool UserOverContestMinLevel()
        {
            return ModelManager.Instance.GetUser().GetLevel() >= Min_contests_level;
        }

        public void SetContestsToClaim(List<ClaimContestData> claimContestDatas)
        {
            m_to_claim_contests = claimContestDatas;
            On_ClaimDataAvailble?.Invoke();
        }

        public class ClaimContestData
        {
            int m_contest_id;
            int m_contest_coins_prize;
            int m_contest_rank;

            public ClaimContestData(int contest_id, int contest_coins_prize, int contest_rank)
            {
                m_contest_id = contest_id;
                m_contest_coins_prize = contest_coins_prize;
                m_contest_rank = contest_rank;
            }

            public int Contest_id { get => m_contest_id; }
            public int Contest_coins_prize { get => m_contest_coins_prize; }
            public int Contest_rank { get => m_contest_rank; }
        }

        public void FireContestInfoShownEvent()
        {
            m_OnContestInfoShown?.Invoke();
        }

        public TextAsset Full_contests_data { get => m_full_contests_data; }
        public TextAsset Leaderboard_contests_data { get => m_leaderboard_contests_data; }
        public TextAsset Mini_leaderboard_contests_data { get => m_mini_leaderboard_contests_data; }
        public TextAsset Mini_leaderboard_contests_data_end { get => m_mini_leaderboard_contests_data_end; }
        public TextAsset Dynamic_contests_data { get => m_dynamic_contests_data; }
        public TextAsset Paytable_data { get => m_paytable_data; }
        public List<ClaimContestData> To_claim_contests { get => m_to_claim_contests; set => m_to_claim_contests = value; }
        public Action OnContestInfoShown { get => m_OnContestInfoShown; set => m_OnContestInfoShown = value; }
        public int Min_contests_level { get => min_contests_level; }
    }
}
