using cardGames.comm;
using cardGames.models;
using common.controllers;
using common.controllers.purchase;
using common.ims.model;
using common.mes;
using common.models;
using System;
using System.Collections;

namespace cardGames.controllers
{

    public class CashierController : MonoBehaviour, ICashierController
    {

        [SerializeField] GameObject m_default_cashier = null;

        bool m_purchase_locked = false;

        string m_last_loaded_cashier_path = "";

        public event Action<string, IMSGoodsList> PurchaseCompleted;
        public event Action<string> PurchaseCanceled;

        public static CashierController Instance;

        private Coroutine m_lockReleaseCoroutine;

        

        private void Awake()
        {
            Instance = this;
        }



        public void BuyPackage(string packageId, PurchaseTrackingData purchaseTrackingData = default, bool showPurchasePopup = true, PopupManager.AddMode purchasePopupAddMode = PopupManager.AddMode.ShowAndRemove)
        {

            User user = ModelManager.Instance.GetUser();

            // Check user ban from purchase
            if (user.Account_Status == CommonUser.AccountStatus.BanFromPurchase)
            {
                CardGamesPopupManager.Instance.ShowBanUserPopup(PopupManager.AddMode.ShowAndRemove, CommonUser.AccountStatus.BanFromPurchase);
                return;
            }

            if (!m_purchase_locked)
            {
                m_purchase_locked = true;

                // Start a delayed call for releaseing the lock after 30 seconds if not released
                m_lockReleaseCoroutine = StartCoroutine(LockRelease());

                int curr_coins_balance = ModelManager.Instance.GetUser().GetCoins();

                string loginToken = CardGamesCommManager.Instance.LoginToken;

                PopupManager.Instance.ShowWaitingIndication(true, 20);

                PurchaseController.Instance.MakePurchase(packageId, loginToken, purchaseTrackingData, (bool success, bool canceled, bool latePayment,
                    int newCoinsBalance, IMSGoodsList imsGoodsList, Cashier new_cashier) =>
                {

                    //there is a problem with the callback - so i am calling the release of the lock from here - after an answer came - there will be a popup so cashier will be closed
                    m_purchase_locked = false;

                    //close the waiting popup
                    PopupManager.Instance.ShowWaitingIndication(false);


                    if (success)
                    {
                        // Store the last goodvirtz - might be used on chained purchased
                        PurchaseController.Instance.LastGoodsList.Add(imsGoodsList);

                        //update the cashier data
                        if (new_cashier != null)
                        {
                            ModelManager.Instance.SetCashier(new_cashier);
                        }

                        LoggerController.Instance.LogFormat("Purchase of item {0} successful", packageId);


                        if (showPurchasePopup)
                            PopupManager.Instance.ShowPurchasePopup(purchasePopupAddMode, newCoinsBalance, imsGoodsList, PurchaseAnswerPopupClosed);

                        // Must be set after the popup - TOOD: remove this limitation
                        user.SetCoins(newCoinsBalance);

                        VoucherController.Instance?.NewVouchersReceived(imsGoodsList.Vouchers);

                        // update avater item
                        foreach (string item in imsGoodsList.InventoryItems)
                            ModelManager.Instance.Inventory.AddItem(item);
                        MAvatarController.Instance.NewInventoryItemsArrived(imsGoodsList.InventoryItems);

                        // Check if first deposit
                        if (user.User_type == User.UserType.NonPaying)
                        {
                            // First deposit
                            user.User_type = User.UserType.Paying;

                            // Appsflyer first deposit event
                            TrackingManager.Instance?.AppsFlyerTrackEvent(TrackingManager.EventName.FirstDeposit);

                        }

                        //firing the purchase complete event for special PO and invite banner to listen to
                        PurchaseCompleted?.Invoke(packageId, imsGoodsList);

                        //calls the MES Controller to update the banners - 
                        MESBase.Instance?.Execute(MESBase.TRIGGER_LOGIN, true);

                    }
                    else
                    {
                        if (latePayment)
                        {
                            PopupManager.Instance.ShowMessagePopup("Your purchase was initiated, coins balance will be updated when the transaction completes", PopupManager.AddMode.ShowAndKeep, true, PurchaseAnswerPopupClosed);

                        }
                        else if (canceled)
                        {

                            PopupManager.Instance.ShowMessagePopup("Your purchase has not been processed", PopupManager.AddMode.ShowAndKeep, true, PurchaseAnswerPopupClosed);

                        }
                        else
                        {

                            PopupManager.Instance.ShowMessagePopup("Your payment processing was unsuccessful", PopupManager.AddMode.ShowAndKeep, true, PurchaseAnswerPopupClosed, "TRY AGAIN");

                        }
                        //firing the purchase complete event for special PO and invite banner to listen to
                        if (PurchaseCanceled != null)
                            PurchaseCanceled(packageId);

                    }

                });
            }
        }

        private void PurchaseAnswerPopupClosed()
        {
            m_purchase_locked = false;
            if (m_lockReleaseCoroutine != null)
                StopCoroutine(m_lockReleaseCoroutine);
        }

        private IEnumerator LockRelease()
        {
            yield return new WaitForSecondsRealtime(15);
            m_purchase_locked = false;
        }

        public GameObject DefaultCashierPrefab { get => m_default_cashier; }

    }
}