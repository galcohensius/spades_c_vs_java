﻿using cardGames.comm;
using cardGames.models;
using common;

namespace cardGames.controllers
{

    public class CommManagerJava : MonoBehaviour
    {
        public static CommManagerJava Instance;

        CardGamesModelParser m_cardGamesModelParser;
        // Password for message signing
        private byte[] Dp = new byte[] { 76, 101, 40, 93, 95, 56, 87, 119, 113, 56, 87, 81, 53, 65, 81, 94 };

        RestClient m_client;

        //getQuests
        //quests/getQuests

        protected void Start()
        {
            m_cardGamesModelParser = CardGamesCommManager.Instance.GetModelParser() as CardGamesModelParser;
        }

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this;

                m_client = GetComponent<RestClient>();
                m_client.Dp = Dp;
            }
        }

        public string LoginToken
        {
            get
            {
                return m_client.Login_token;
            }

            set
            {
                m_client.Login_token = value;
            }

        }

        public CardGamesModelParser GetModelParser()
        {
            return m_cardGamesModelParser;
        }

        public RestClient Client { get => m_client; set => m_client = value; }

        #region Quests

        public delegate void GetQuestsCompleted(bool success, JSONNode responseData);
        protected GetQuestsCompleted m_get_quests_completed;

        public delegate void GetQuestUpdateCompleted(bool success, JSONNode responseData);
        protected GetQuestUpdateCompleted m_get_quest_update_completed;


        public void GetQuests(string loginToekn, int userId, GetQuestsCompleted getquestsCompleted)
        {
            m_get_quests_completed = getquestsCompleted;

            JSONNode requestData = new JSONObject();
            requestData["lTn"] = loginToekn;
            requestData["uId"] = userId;
           
            m_client.SendRequest("getQuests", requestData, OnGetQuestsResponse, true,"quests/");
        }

        private void OnGetQuestsResponse(bool success, JSONNode responseData)
        {
            m_get_quests_completed(success, responseData);
        }

        public void GetEndGameQuestUpdate(string loginToekn, int userId,bool isWinner,int tableId, GetQuestUpdateCompleted getquestUpdateCompleted)
        {
            m_get_quest_update_completed = getquestUpdateCompleted;

            JSONNode requestData = new JSONObject();
            requestData["uId"] = userId;
            requestData["lTn"] = loginToekn;
            requestData["tId"] = tableId;
            requestData["iWr"] = isWinner?"true":"false";


            m_client.SendRequest("endGame", requestData, OnGetQuestUpdateResponse, true, "quests/");
        }

        private void OnGetQuestUpdateResponse(bool success, JSONNode responseData)
        {
            m_get_quest_update_completed(success, responseData);
        }

        #endregion

        #region Piggy
        //GET PIGGY
        public delegate void GetPiggyCompleted(bool success, JSONNode responseData);
        protected GetPiggyCompleted m_get_piggy_completed;

        public void GetPiggy(GetPiggyCompleted getPiggyCompleted)
        {
            m_get_piggy_completed = getPiggyCompleted;

            JSONNode requestData = new JSONObject();
            requestData["lTn"] = CardGamesCommManager.Instance.LoginToken;
            requestData["uId"] = ModelManager.Instance.GetUser().GetID();

            m_client.SendRequest("getPiggy", requestData, OnGetPiggyResponse, true, "piggy/");
        }

        public void GetPiggy(string loginToken, int userId, GetPiggyCompleted getPiggyCompleted)
        {
            m_get_piggy_completed = getPiggyCompleted;

            JSONNode requestData = new JSONObject();
            requestData["lTn"] = loginToken;
            requestData["uId"] = userId;

            m_client.SendRequest("getPiggy", requestData, OnGetPiggyResponse, true, "piggy/");
        }

        private void OnGetPiggyResponse(bool success, JSONNode responseData)
        {
            m_get_piggy_completed(success, responseData);
        }


        //SINGLE MATCH
        public delegate void GetEndGameSingleMatchPiggyCompleted(bool success, JSONNode responseData);
        protected GetEndGameSingleMatchPiggyCompleted m_get_piggy_end_game_single_match_update_completed;

        public void GetEndGameSingleMatchPiggyUpdate(int c4pContribution, int hiloContribution, GetEndGameSingleMatchPiggyCompleted getPiggyEndGameSingleMatchUpdateCompleted)
        {
            m_get_piggy_end_game_single_match_update_completed = getPiggyEndGameSingleMatchUpdateCompleted;

            JSONNode requestData = new JSONObject();

            if (c4pContribution > 0)
                requestData["cCn"] = c4pContribution;
            if (hiloContribution > 0)
                requestData["hLCn"] = hiloContribution;
            requestData["lTn"] = CardGamesCommManager.Instance.LoginToken;
            requestData["tId"] = GameServerManager.Instance.Last_table_id;
            requestData["uId"] = ModelManager.Instance.GetUser().GetID();

            m_client.SendRequest("endGameSingle", requestData, OnGetEndGameSingleMatchPiggyUpdateResponse, true, "piggy/");
        }

        public void GetEndGameSingleMatchPiggyUpdate(string loginToken, string tableID, int userID, int c4pContribution, int hiloContribution, GetEndGameSingleMatchPiggyCompleted getPiggyEndGameSingleMatchUpdateCompleted)
        {
            m_get_piggy_end_game_single_match_update_completed = getPiggyEndGameSingleMatchUpdateCompleted;

            JSONNode requestData = new JSONObject();

            if (c4pContribution > 0)
                requestData["cCn"] = c4pContribution;
            if (hiloContribution > 0)
                requestData["hLCn"] = hiloContribution;
            requestData["lTn"] = loginToken;
            requestData["tId"] = tableID;
            requestData["uId"] = userID;

            m_client.SendRequest("endGameSingle", requestData, OnGetEndGameSingleMatchPiggyUpdateResponse, true, "piggy/");
        }

        private void OnGetEndGameSingleMatchPiggyUpdateResponse(bool success, JSONNode responseData)
        {
            m_get_piggy_end_game_single_match_update_completed(success, responseData);
        }



        //ARENA
        public delegate void GetPiggyArenaEndGameCompleted(bool success, JSONNode responseData);
        protected GetPiggyArenaEndGameCompleted m_get_piggy_arena_end_game_completed;

        public void GetArenaEndGamePiggyUpdate(int xpDelta, GetPiggyArenaEndGameCompleted getPiggyArenaEndGameUpdateCompleted)
        {
            m_get_piggy_arena_end_game_completed = getPiggyArenaEndGameUpdateCompleted;

            JSONNode requestData = new JSONObject();

            requestData["aId"] = ModelManager.Instance.GetArenaMatch().GetArena().GetID();
            requestData["lTn"] = CardGamesCommManager.Instance.LoginToken;
            //TODO: may not actually be Current_stage. TBD
            requestData["sId"] = ModelManager.Instance.GetArenaMatch().GetArena().Current_stage;
            requestData["uId"] = ModelManager.Instance.GetUser().GetID();
            requestData["xDa"] = xpDelta;

            m_client.SendRequest("endGameArena", requestData, OnGetPiggyArenaEndGameUpdateResponse, true, "piggy/");
        }

        public void GetArenaEndGamePiggyUpdate(int arenaID, string loginToken, int xpDelta, int stageID, int userID, GetPiggyArenaEndGameCompleted getPiggyArenaEndGameUpdateCompleted)
        {
            m_get_piggy_arena_end_game_completed = getPiggyArenaEndGameUpdateCompleted;

            JSONNode requestData = new JSONObject();
            requestData["aId"] = arenaID;
            requestData["lTn"] = loginToken;
            requestData["sId"] = stageID;
            requestData["uId"] = userID;
            requestData["xDa"] = xpDelta;

            m_client.SendRequest("endGameArena", requestData, OnGetPiggyArenaEndGameUpdateResponse, true, "piggy/");
        }

        private void OnGetPiggyArenaEndGameUpdateResponse(bool success, JSONNode responseData)
        {
            m_get_piggy_arena_end_game_completed(success, responseData);
        }

        #endregion
    }
}