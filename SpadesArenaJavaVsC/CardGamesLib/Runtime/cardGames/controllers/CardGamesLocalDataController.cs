using cardGames.models;
using common.controllers;
using System.Collections.Generic;

namespace cardGames.controllers
{
    public class CardGamesLocalDataController : LocalDataController
    {
        int FB_START_INDEX = 3500;

        [SerializeField]
        private TextAsset m_avatarBotsJson = null;

        [SerializeField]
        private TextAsset m_AIChatResponseOptionSpansJsonFile = null;

        List<BotListItem> m_botsList = new List<BotListItem>();

        public static new CardGamesLocalDataController Instance;

        System.Random rand = new System.Random();

        override protected void Awake()
        {
            base.Awake();
            if (Instance == null)
                Instance = this;
        }

        override protected void Start()
        {
            base.Start();

            LoggerController.Instance.Log("LocalDataController reading bots json");
            if (m_avatarBotsJson != null)
            {
                JSONNode root = JSON.Parse(m_avatarBotsJson.ToString());
                JSONArray bots_array = root.AsArray;

                foreach (JSONNode node in bots_array)
                {
                    // TODO: check if the parsing works correctly
                    BotListItem botListItem = new BotListItem();
                    botListItem.Nickname = node["avr"]["n"].Value;
                    botListItem.FbID = node["avr"]["fID"].Value;
                    botListItem.gender = (MAvatarModel.GenderType)node["avr"]["g"].AsInt;

                    m_botsList.Add(botListItem);
                }
            }
        }

        public List<BotListItem> FetchRandomBots(int count, bool fbConnectedOnly)
        {
            List<BotListItem> results = new List<BotListItem>();

            int startIndex = 0;
            if (fbConnectedOnly)
                startIndex = FB_START_INDEX;

            for (int i = 0; i < count; i++)
            {
                // int index = UnityEngine.Random.Range(startIndex, m_botsList.Count - 1000); // The last 1000 FB images are not loading..
                int index = UnityEngine.Random.Range(startIndex, m_botsList.Count);

                results.Add(m_botsList[index]);
                m_botsList.RemoveAt(index); // Remove to prevent duplicates
            }

            // Return what we removed before
            foreach (var item in results)
            {
                m_botsList.Add(item);
            }
            return results;
        }

        public TextAsset AIChatResponseOptionSpansJsonFile
        {
            get
            {
                return m_AIChatResponseOptionSpansJsonFile;
            }
        }

        public List<BotListItem> BotsList { get => m_botsList; }
    }

    public class BotListItem
    {
        public string Nickname { get; set; }
        public MAvatarModel.GenderType gender { get; set; }
        public string FbID { get; set; }
    }
}
