using cardGames.models;
using cardGames.views;
using cardGames.views.popups;
using common.controllers;
using common.ims;
using common.ims.model;
using common.models;
using common.views;
using common.views.popups;
using System;
using System.Collections;
using System.Collections.Generic;
using static common.models.CommonUser;

namespace cardGames.controllers
{
    public abstract class CardGamesPopupManager : PopupManager
    {
        [Space]
        //may be changed in future for rummy with table scene design change.
        [SerializeField] protected bool isLobbyScene = true;
        public bool IsLobbyScene { get => isLobbyScene; }

        [Space]
        [Header("spades sprites & materials")]
        [SerializeField] protected Sprite m_tab_On_sprite = null;
        [SerializeField] protected Sprite m_tab_Off_sprite = null;

        [SerializeField] protected Material m_tab_On_Material = null;
        [SerializeField] protected Material m_tab_Off_Material = null;

        [Space]
        [Header("general popups")]
        QuestionPopup m_questionPopup;
        bool m_isQuestionPopupOn;
        [SerializeField] protected GameObject m_question = null;
        [SerializeField] protected GameObject m_c4pQuit = null;
        [SerializeField] protected GameObject m_waiting_indication = null;
        [SerializeField] protected GameObject m_cheat_popup = null;
        [SerializeField] protected GameObject m_new_version_popup = null;
        [SerializeField] protected GameObject m_disconnected_popup = null;
        [SerializeField] protected GameObject m_rate_us_question_popup = null;
        [SerializeField] protected GameObject m_maintenance_popup = null;
        [SerializeField] protected GameObject m_error_message = null;
        [SerializeField] protected GameObject m_promotion_popup = null;
        [SerializeField] protected GameObject m_ban_user_popup = null;

        [Space]
        [Header("settings and social")]
        [SerializeField] protected GameObject m_settings_popup = null;
        [SerializeField] protected GameObject m_profile = null;
        [SerializeField] protected GameObject m_social_popup = null;

        [Space]
        [Header("purchasing and bonuses")]
        [SerializeField] protected GameObject m_purchase_poopup = null;
        [SerializeField] protected GameObject m_general_bonus_popup = null;
        [SerializeField] protected GameObject m_invite_bonus_popup = null;
        [SerializeField] protected GameObject m_redeem_code_popup = null;
        [SerializeField] protected GameObject m_free_coins_popup = null;
        [SerializeField] protected GameObject m_insufficient_funds_popup = null;
        [SerializeField] protected GameObject m_item_purchase_confirmation_popup = null;

        [Space]
        [Header("Piggy")]
        [SerializeField] protected GameObject m_piggy_unlock_popup = null; //overlay popup - always run in the popup queue, not the overlay queue
        [SerializeField] protected GameObject m_piggy_offer = null;
        [SerializeField] protected GameObject m_piggy_info = null;

        [Space]
        [Header("leaderboards, missions, arenas & pre-match")]
        [SerializeField] protected GameObject m_mission_popup = null;
        [SerializeField] protected GameObject m_searching_for_players = null;
        [SerializeField] protected GameObject m_leaderboard_reward = null;
        [SerializeField] protected GameObject m_leaderboard_info_popup = null;
        [SerializeField] protected GameObject m_arena_buy_warning = null;
        [SerializeField] protected GameObject m_trophy_room_popup = null;

        [Space]
        [Header("contests")]
        [SerializeField] protected GameObject m_contests_popup = null;
        [SerializeField] protected GameObject m_contest_claim_popup = null;

        [Space]
        [Header("IMS popups")]
        [SerializeField] protected GameObject m_ims_bundle_popup = null;
        [SerializeField] protected GameObject m_ims_template_popup = null;
        [SerializeField] protected GameObject m_ims_bonus_popup = null;
        [SerializeField] protected GameObject m_ims_rafflepo_popup = null;
        [SerializeField] protected GameObject m_ims_mgap_popup = null;
        [SerializeField] protected GameObject m_ims_po_popup = null;
        [SerializeField] protected GameObject m_ims_challenge_popup = null;
        [SerializeField] protected GameObject m_ims_openTable_popup = null;
        [SerializeField] protected GameObject m_ims_openCashier_popup = null;
        [SerializeField] protected GameObject m_ims_cashier_popup = null;

        [SerializeField] GameObject m_mavatarCreationPopup;

        //these fields are used for all the flight coins related 
        [Space]
        [Header("comets elements")]
        [Tooltip("different scene 'volumes' need different comet sizes")]
        [SerializeField] protected GameObject m_coins_trails_prefab = null;
        [Tooltip("override in different scenes if necessarry")]
        [SerializeField] protected GameObject m_XP_trails_prefab = null;
        [SerializeField] protected GameObject m_lobby_coins_box = null;
        protected BalanceCoinTextbox m_lobby_coins_balance = null;
        protected Animator m_lobby_coins_animator = null;
        protected GameObject m_lobby_flight_target;

        GameObject m_questionOverPopupsInstance;


        public static new CardGamesPopupManager Instance;

        public bool UserSeenRateUsPopup = false;

        protected override void Awake()
        {
            base.Awake();
            //override Instance with each scene load
            Instance = this;
            if (m_lobby_coins_box != null)
            {
                Transform coins_box = m_lobby_coins_box.transform.FindDeepChild("CoinsText");
                if (coins_box != null)
                {
                    m_lobby_coins_balance = coins_box.GetComponent<BalanceCoinTextbox>();
                    m_lobby_coins_animator = m_lobby_coins_box.GetComponent<Animator>();
                    m_lobby_flight_target = m_lobby_coins_box.transform.FindDeepChild("CoinsIconDummy").gameObject;
                }
                else
                    Debug.LogWarning("lobby coins box not found but is expected to be here. null exceptions may occure.");
            }
        }

        public void SetCoinBoxOnTop(bool on_top)
        {
            m_lobby_coins_box.GetComponent<Canvas>().overrideSorting = on_top;
        }

        public override void ShowWaitingIndication(bool show, float autoHideDelay = 0)
        {
            m_waiting_indication.SetActive(show);

            if (autoHideDelay > 0)
            {
                StartCoroutine(HideWaitingIndication(autoHideDelay));
            }
        }

        protected IEnumerator HideWaitingIndication(float delayInSec)
        {
            yield return new WaitForSecondsRealtime(delayInSec);
            ShowWaitingIndication(false);
        }

        public void ShowCheatPopup()
        {
            GameObject popup = (GameObject)Instantiate(m_cheat_popup);
            AddPopup(popup, null);
        }

        public void ShowMissionPopup(AddMode add_mode, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_mission_popup);

            AddPopup(popup, add_mode, close_delegate);

        }

        public void ShowMaintenancePopup()
        {
            GameObject popup = (GameObject)Instantiate(m_maintenance_popup);
            AddPopup(popup, AddMode.ShowAndRemove);
        }

        public void ShowRateQuestionPopup(bool force_show = false, PopupClosedDelegate close_delegate = null)
        {

            if (force_show || ModelManager.Instance.FindBonus(Bonus.BonusTypes.RateUs) != null)
            {
                // Check that the user has not already seen the rate us popup for this session
                if (UserSeenRateUsPopup)
                    return;

                GameObject popup = (GameObject)Instantiate(m_rate_us_question_popup);
                AddPopup(popup, AddMode.ShowAndRemove, close_delegate);
            }
            else
            {
                LoggerController.Instance.Log("You are not entitled to rate us bonus so its not shown");
            }

        }

        public AvatarCreationPopup ShowMavatarCreationPopup(AddMode add_mode, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = Instantiate(m_mavatarCreationPopup);

            AvatarCreationPopup mavatarPopup = popup.GetComponent<AvatarCreationPopup>();

            mavatarPopup.SetData();
            AddPopup(popup, add_mode, close_delegate);

            return mavatarPopup;
        }

        public override IMSCashierPopup ShowCashierIMSPopup(AddMode addMode)
        {
            // For cashier popup - the close event is always an IMS trigger
            PopupClosedDelegate closedDelegate = () =>
            {
                IMSController.Instance.TriggerEvent(IMSController.EVENT_CLOSE_CASHIER);
            };

            GameObject popup = (GameObject)Instantiate(m_ims_cashier_popup);
            IMSCashierPopup ims_cashier_popup = popup.GetComponent<IMSCashierPopup>();
            ims_cashier_popup.InitPopup(CashierController.Instance, ModelManager.Instance.GetCashier(), ModelManager.Instance.GetUser());
            AddPopup(popup, addMode, closedDelegate);
            return ims_cashier_popup;
        }

        public IMSBonusPopup ShowBonusIMSPopup(AddMode addMode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null, string button_text = null, int ontop = 0)
        {
            GameObject popup = (GameObject)Instantiate(m_ims_bonus_popup);
            IMSBonusPopup ims_bonus_popup = popup.GetComponent<IMSBonusPopup>();
            ims_bonus_popup.InitPopup(iZone);
            AddPopup(popup, addMode, close_delegate);
            return ims_bonus_popup;
        }

        public ContestsPopupView ShowContestsPopup(AddMode add_mode, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_contests_popup);
            ContestsPopupView contestsPopup = popup.GetComponent<ContestsPopupView>();

            AddPopup(popup, add_mode, close_delegate);

            return contestsPopup;

        }

        public ContestWinPopup ShowContestsWinPopup(List<ContestsController.ClaimContestData> to_claim_contests, AddMode addMode, PopupClosedDelegate popupClosedDelegate)
        {
            GameObject popup = Instantiate(m_contest_claim_popup);
            AddPopup(popup, addMode, popupClosedDelegate);
            return popup.GetComponent<ContestWinPopup>();
        }



        public void ShowContestInfo(AddMode add_mode, PopupClosedDelegate close_delegate = null)
        {
            ContestsPopupView contestsPopup = ShowContestsPopup(add_mode, close_delegate);
            contestsPopup.SetPageToShow(ContestsPopupView.ShowPage.Info);
        }


        public override void ShowQuestionPopup(AddMode addMode, string title, string question, string yes_title, string no_title, Action yes_action, Action no_action = null)
        {
            GameObject popup = (GameObject)Instantiate(m_question);
            QuestionPopup questionPopup = popup.GetComponent<QuestionPopup>();

            questionPopup.SetTexts(title, question, yes_title, no_title);
            questionPopup.Yes_action = yes_action;
            questionPopup.No_action = no_action;

            AddPopup(popup, addMode);
        }

        public override void ShowQuestionOverPopups(string title, string question, string yes_title, string no_title, Action yes_action, Action no_action = null)
        {
            m_questionOverPopupsInstance = (GameObject)Instantiate(m_question, this.transform);

            // Overrride with highest sorting layer
            Canvas canvas = m_questionOverPopupsInstance.GetComponent<Canvas>();
            if (canvas != null)
            {
                m_questionOverPopupsInstance.GetComponent<Canvas>().overrideSorting = true;
                m_questionOverPopupsInstance.GetComponent<Canvas>().sortingOrder = 32767;
            }


            QuestionPopup questionPopup = m_questionOverPopupsInstance.GetComponent<QuestionPopup>();

            m_isQuestionPopupOn = true;

            questionPopup.SetTexts(title, question, yes_title, no_title);
            questionPopup.IsOverPopups = true;
            questionPopup.Yes_action = yes_action;
            questionPopup.No_action = no_action;
        }

        public override void HideQuestionOverPopups()
        {
            m_isQuestionPopupOn = false;

            if (m_questionOverPopupsInstance != null)
                DestroyImmediate(m_questionOverPopupsInstance);
        }


        public LoadingAssetPopup ShowLoadingAssetPopup(string image_url, Action popup_shown, PopupClosedDelegate close_delegate = null, Action cancel_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_loading_asset_popup);
            LoadingAssetPopup loading_assets_popup = popup.GetComponent<LoadingAssetPopup>();
            loading_assets_popup.OnPopupShown = popup_shown;
            loading_assets_popup.Cancel_delegate = cancel_delegate;

            loading_assets_popup.Image_url = image_url;

            AddPopup(popup, AddMode.ShowAndRemove, close_delegate);

            return loading_assets_popup;
        }


        public DisconnectedPopup ShowDisconnectedPopup(Action button_action, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_disconnected_popup);
            DisconnectedPopup disconnected_popup = popup.GetComponent<DisconnectedPopup>();

            AddPopup(popup, AddMode.ShowAndRemove, close_delegate);
            disconnected_popup.ShowDisconnection(button_action);

            return disconnected_popup;
        }

        public SocialPopup ShowSocialPopup(AddMode add_mode, SocialPopup.SocialTab open_tab)
        {
            GameObject popup = (GameObject)Instantiate(m_social_popup);

            SocialPopup social_popup = popup.GetComponent<SocialPopup>();
            social_popup.Init(open_tab);
            AddPopup(popup, add_mode, null);

            return social_popup;
        }

        public void ShowinsufficientFundsPopup(int delta, AddMode add_mode, PopupClosedDelegate close_delegate = null, PopupClosedDelegate popup_cancelled = null)
        {
            GameObject popup = (GameObject)Instantiate(m_insufficient_funds_popup);
            popup.GetComponent<InsufficientFundsPopup>().CreateMessageAndButton(delta, close_delegate, popup_cancelled);
            AddPopup(popup, add_mode, close_delegate);
        }

        public override void ShowPurchasePopup(AddMode add_mode, int new_balance, IMSGoodsList goodsList, PopupClosedDelegate close_delegate = null)
        {
            // TODO: "new_balance" isn't in use. should be deleted (from spades and rummy)
            // if purchase contains MGAP, show the purchase success for a few seconds

            // TODO: Think of a more generic way for chained purchases (RAN 25/2/2020) 
            if (goodsList.MGAP)
            {
               // With MGAP, there is no purchase success popup
                
            }
            else
            {
                // show purchase popup 
                GameObject popup = (GameObject)Instantiate(m_purchase_poopup);

                if (popup.GetComponent<PurchasePopup>().InitPurchase(0, goodsList, Coins_trails_prefab, Lobby_coins_box))
                {
                    AddPopup(popup, add_mode, close_delegate);
                }
                else
                {
                    DestroyImmediate(popup);
                }
            }
        }

        public void ShowTrophyRoomPopup(AddMode add_mode, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_trophy_room_popup);

            AddPopup(popup, add_mode, close_delegate);
        }

        public void ShowErrorMessagePopup(string error, string stack_trace, PopupClosedDelegate close_delegate = null)
        {
            m_error_message.GetComponent<ErrorMessagePopup>().Error_message = error;
            m_error_message.GetComponent<ErrorMessagePopup>().Stack_error_message = stack_trace;
            m_error_message.SetActive(true);
        }

        public void ShowNewVersionPopup(VersionData versionData, int curr_version_number, Action on_no_new_version)
        {
            GameObject popup = (GameObject)Instantiate(m_new_version_popup);
            popup.GetComponent<NewVersionPopup>().buildPopup(versionData, curr_version_number, on_no_new_version);

            AddPopup(popup, null);
        }

        public void ShowInviteBonusPopup(PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_invite_bonus_popup);
            AddPopup(popup, AddMode.DontShowIfPopupShown, close_delegate);
        }

        public void ShowFreeCoinsPopup(AddMode addMode, PopupClosedDelegate closeDelegate = null)
        {
            if (m_free_coins_popup != null)
            {
                GameObject popup = (GameObject)Instantiate(m_free_coins_popup);
                AddPopup(popup, addMode, closeDelegate);
            }
        }

        public void ShowItemConfirmationPopup(AddMode addMode, bool autoAccept, Action acceptAction, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_item_purchase_confirmation_popup);
            ItemPurchaseConfirmationPopup itemConfirmationPopup = popup.GetComponent<ItemPurchaseConfirmationPopup>();
            itemConfirmationPopup.AutoAccept = autoAccept;
            itemConfirmationPopup.OnAccept = acceptAction;

            AddPopup(popup, addMode, close_delegate);
        }

        public void ShowPiggyUnlockedPopup(AddMode add_mode, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_piggy_unlock_popup);
            AddPopup(popup, add_mode, close_delegate);
        }

        public void ShowPiggyOfferPopup(PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_piggy_offer);
            AddPopup(popup, AddMode.ShowAndRemove, close_delegate);
        }

        public void ShowPiggyInfoPopup()
        {
            GameObject popup = (GameObject)Instantiate(m_piggy_info);
            AddPopup(popup, AddMode.ShowAndKeep);
        }

        public void ShowGeneralBonusPopup(Bonus.BonusTypes bonus_type, AddMode addMode, PopupClosedDelegate close_delegate = null, bool autoClose = false, BonusAwarded bonusAwarded = null, IMSInteractionZone originIZone = null)
        {
            GameObject popup = (GameObject)Instantiate(m_general_bonus_popup);
            popup.GetComponent<GeneralBonusPopup>().Init(bonus_type, bonusAwarded, autoClose, originIZone);

            AddPopup(popup, addMode, close_delegate);
        }

        public void ShowRedeemCodePopup(AddMode addMode = AddMode.ShowAndRemove, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_redeem_code_popup);
            popup.GetComponent<RedeemCodePopup>().Init();

            AddPopup(popup, addMode, close_delegate);
        }

        public IMSPopup ShowIMSBundlePopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_ims_bundle_popup);
            IMSPopup iMSPopup = popup.GetComponent<IMSPopup>();

            iMSPopup.InitPopup(iZone);
            AddPopup(popup, add_mode, close_delegate);

            return iMSPopup;
        }

        public IMSTemplatePopup ShowIMSTemplatePopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_ims_template_popup);
            IMSTemplatePopup iMSPopup = popup.GetComponent<IMSTemplatePopup>();

            iMSPopup.InitPopup(iZone);
            AddPopup(popup, add_mode, close_delegate);

            return iMSPopup;
        }
        
        public RafflePOPopup ShowRafflePOPopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            Canvas m_popup_canvas = GetComponent<Canvas>();

            GameObject popup = (GameObject)Instantiate(m_ims_rafflepo_popup);

            RafflePOPopup rafflePOPopup = popup.GetComponent<RafflePOPopup>();
            rafflePOPopup.InitPopup(iZone, CashierController.Instance);
            rafflePOPopup.OnPrizeAwarded += (amount) =>
            {
                OverlaysManager.Instance.FlyStandardCoinsComet(rafflePOPopup.CometTriggerStartPoint, amount);
            };

            AddPopup(popup, add_mode, close_delegate);

            return rafflePOPopup;
        }
        public IMSMGAPPopup ShowIMSMGAPPopup(IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            IMSMGAPPopup mgapPopup = null;

            Canvas m_popup_canvas = GetComponent<Canvas>();
            GameObject popup = Instantiate(m_ims_mgap_popup);

            mgapPopup = popup.GetComponent<IMSMGAPPopup>();
            mgapPopup.InitPopup(iZone, CashierController.Instance);

            // must be "show and remove" to be the first in line
            AddPopup(popup, AddMode.ShowAndRemove, close_delegate);

            return mgapPopup;
        }

        //explicit spades and rummy versions are overriden and available at their respective places
        public IMSOpenTablePopup ShowIMSOpenTablePopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            Canvas m_popup_canvas = GetComponent<Canvas>();

            GameObject popup = (GameObject)Instantiate(m_ims_openTable_popup);

            IMSOpenTablePopup openTablePopup = popup.GetComponent<IMSOpenTablePopup>();
            openTablePopup.Close_delegate = close_delegate;
            openTablePopup.InitPopup(iZone);

            AddPopup(popup, add_mode);

            return openTablePopup;
        }

        public IMSChallengesPopup ShowIMSChallengePopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            Canvas m_popup_canvas = GetComponent<Canvas>();

            GameObject popup = (GameObject)Instantiate(m_ims_challenge_popup);

            Mission mission = ModelManager.Instance.GetMission();
            // handle no mission or no active challenge
            if (mission == null || mission.GetActiveChallenge() == null)
            {
                return null;
            }
            IMSChallengesPopup imspo_popup = popup.GetComponent<IMSChallengesPopup>();
            imspo_popup.InitPopup(iZone);

            AddPopup(popup, add_mode, close_delegate);

            return imspo_popup;
        }

        public IMSOpenCashierPopup ShowIMSOpenCashierPopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            Canvas m_popup_canvas = GetComponent<Canvas>();

            GameObject popup = (GameObject)Instantiate(m_ims_openCashier_popup);

            IMSOpenCashierPopup imspo_popup = popup.GetComponent<IMSOpenCashierPopup>();
            imspo_popup.InitPopup(iZone);
            imspo_popup.InitPopup(ModelManager.Instance.GetCashier());

            AddPopup(popup, add_mode, close_delegate);

            return imspo_popup;
        }

        public IMSPOPopup ShowIMSPOPopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            Canvas m_popup_canvas = GetComponent<Canvas>();

            GameObject popup = (GameObject)Instantiate(m_ims_po_popup);

            IMSPOPopup imspo_popup = popup.GetComponent<IMSPOPopup>();
            imspo_popup.InitPopup(iZone);

            AddPopup(popup, add_mode, close_delegate);

            return imspo_popup;
        }

        public LeaderboardRewardPopup ShowLeaderboardRewardPopup()
        {

            ModelManager.Instance.LeaderboardReward.ModelIsSet = false;

            GameObject popup = (GameObject)Instantiate(m_leaderboard_reward);
            AddPopup(popup, AddMode.DontShowIfPopupShown);
            return popup.GetComponent<LeaderboardRewardPopup>();
        }

        public LeaderboardInfoPopup ShowLeaderboardInfoPopup()
        {
            GameObject popup = (GameObject)Instantiate(m_leaderboard_info_popup);
            AddPopup(popup, AddMode.ShowAndRemove);
            return popup.GetComponent<LeaderboardInfoPopup>();
        }

        public ArenaBuyWarningPopup ShowArenaBuyWarning()
        {
            GameObject popup = (GameObject)Instantiate(m_arena_buy_warning);
            AddPopup(popup, AddMode.DontShowIfPopupShown);
            return popup.GetComponent<ArenaBuyWarningPopup>();
        }

        public BanUserPopup ShowBanUserPopup(AddMode addMode, AccountStatus accountStatus)
        {
            GameObject popup = (GameObject)Instantiate(m_ban_user_popup);
            BanUserPopup banUserPopup = popup.GetComponent<BanUserPopup>();
            banUserPopup.AccountStatus = accountStatus;

            AddPopup(popup, addMode);

            return banUserPopup;
        }

        public override void SetCashierPopupPrefab(GameObject cashier_gameobject)
        {
            m_cashier_popup = cashier_gameobject;
        }

        public virtual void ShowSettingsPopup(AddMode add_mode, bool disable_FB_button = false)
        {
        }

        public Sprite Tab_On_sprite
        {
            get
            {
                return m_tab_On_sprite;
            }
        }

        public Sprite Tab_Off_sprite
        {
            get
            {
                return m_tab_Off_sprite;
            }
        }

        public GameObject Question
        {
            get
            {
                return m_question;
            }
        }

        public GameObject Searching_for_players
        {
            get
            {
                return m_searching_for_players;
            }
        }

        public GameObject Settings_popup
        {
            get
            {
                return m_settings_popup;
            }
        }

        public GameObject Profile
        {
            get
            {
                return m_profile;
            }
        }

        public GameObject New_Version_popup
        {
            get
            {
                return m_new_version_popup;
            }
        }

        public Material Tab_On_Material
        {
            get
            {
                return m_tab_On_Material;
            }
        }

        public Material Tab_Off_Material
        {
            get
            {
                return m_tab_Off_Material;
            }

        }

        public GameObject XP_trails_prefab { get => m_XP_trails_prefab; }
        public GameObject Lobby_coins_box { get => m_lobby_coins_box; }

        public GameObject Coins_trails_prefab { get => m_coins_trails_prefab; }
        public Animator Lobby_coins_animator { get => m_lobby_coins_animator; set => m_lobby_coins_animator = value; }
        public BalanceCoinTextbox Lobby_coins_balance { get => m_lobby_coins_balance; set => m_lobby_coins_balance = value; }

        public GameObject Lobby_flight_target
        {
            get { return m_lobby_flight_target; }
            set { m_lobby_flight_target = value; }
        }

        public bool IsQuestionPopupOn { get => m_isQuestionPopupOn; }
    }
}