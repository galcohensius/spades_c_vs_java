﻿using cardGames.comm;
using cardGames.models;
using cardGames.views;
using common.controllers;
using common.utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.controllers
{
    /// <summary>
    /// should be abstract but a concrete instance is required for ModelManager
    /// </summary>
    public abstract class MissionController : MonoBehaviour
    {
        public enum MissionStatus
        {
            InProgress,
            Completed,
            Expired
        }

        public enum BetOperator
        {
            Max,
            Min,
            Exact
        }

        public enum ChallengeStatus
        {
            Init,
            InProgress,
            Completed,
            Swapped,
            Locked
        }

        public Color MISSION_LOCKED_COLOR = new Color(95f / 255f, 95f / 255f, 95f / 255f);

        [SerializeField] protected Sprite[] m_active_mission_images = null;

        [SerializeField] protected Sprite m_locked_bg;
        [SerializeField] protected Sprite m_completed_bg;

        protected int m_min_mission_level = 7;

        protected long m_next_mission_date = -1;

        protected List<ClaimMissionData> m_to_claim_mission = new List<ClaimMissionData>();

        protected bool m_curr_game_contributing_to_challenge = false;

        protected PluralFormatProvider pluralFormatProvider = new PluralFormatProvider();

        protected Challenge m_curr_challenge_before_update = null;

        protected Challenge m_curr_challenge_post_update = null;

        //Don't forget to copy-paste this segment to each derived class
        public static MissionController Instance;

        Coroutine m_mission_expire_routine;


        protected virtual void Awake()
        {
            Instance = this;
        }

        protected virtual void Start()
        {
            // Set mission min level
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_min_mission_level = ldc.GetSettingAsInt("MinMissionsLevel", 7);
            };
        }

        public void AddMissionToClaimMissionList(int mission_id, string mission_prize)
        {
            if (To_claim_mission != null)
            {
                bool mission_already_in_list = false;

                foreach (ClaimMissionData item in To_claim_mission)
                {
                    if (item.Mission_id == mission_id)
                        mission_already_in_list = true;
                }

                if (mission_already_in_list == false)
                {
                    ClaimMissionData claimMissionData = new ClaimMissionData(mission_id, mission_prize);
                    To_claim_mission.Add(claimMissionData);
                }
            }
            else
            {
                ClaimMissionData claimMissionData = new ClaimMissionData(mission_id, mission_prize);
                To_claim_mission.Add(claimMissionData);
            }
        }

        public virtual void NewMissionArrived(Mission mission)
        {
            if (mission == null)
                return;

            //first need to look for any status I - in progress - if its found - anything before it is a C and anything after it is an L

            //if you cannot find any I - it means all are C

            int I_index = -1;
            int num_challenges = mission.GetNumChallenges();

            for (int i = 0; i < num_challenges; i++)
            {
                //can only have 1 I index
                if (mission.GetChallenge(i).Status == MissionController.ChallengeStatus.InProgress)
                    I_index = i;

            }

            if (I_index != -1)
            {
                //found and InProgress mission -- this means any mission before it must have been a C - so no need to do anything to them - conver the ones after it to L
                for (int i = I_index + 1; i < num_challenges; i++)
                {
                    mission.GetChallenge(i).Status = MissionController.ChallengeStatus.Locked;

                }

                mission.Active_challenge_index = I_index;
            }
            else
            {
                //all missions are completed
            }

            //starts a countdown to mission expire
            if (m_mission_expire_routine != null)
                StopCoroutine(m_mission_expire_routine);

            m_mission_expire_routine = StartCoroutine(CountToMissionExpire(mission));

        }

        IEnumerator CountToMissionExpire(Mission mission)
        {
            TimeSpan time_to_wait = mission.End_date - DateTime.Now;

            yield return new WaitForSecondsRealtime((float)time_to_wait.TotalSeconds);

            //Mission Expired

            if (mission.Status != MissionStatus.Completed)
                mission.Status = MissionStatus.Expired;

            LoggerController.Instance.Log("Mission Timer Expired");

            UpdateMissionLobbyIcons();

        }

        public virtual Sprite GetActiveMissionIcon()
        {
            if (ModelManager.Instance.GetMission() != null)
            {
                return GetActiveMissionIconFromModelManager();
            }
            else
                return m_active_mission_images[0];
        }

        public abstract Sprite GetActiveMissionIconFromModelManager();

        public List<ClaimMissionData> To_claim_mission
        {
            get
            {
                return m_to_claim_mission;
            }

            set
            {
                m_to_claim_mission = value;
            }
        }

        public long Next_mission_date
        {
            get
            {
                return m_next_mission_date;
            }

            set
            {
                m_next_mission_date = value;
            }
        }

        public int Min_mission_level
        {
            get
            {
                return m_min_mission_level;
            }
        }

        public bool Show_mission_button_in_game
        {
            get
            {
                return m_curr_game_contributing_to_challenge;
            }

            set
            {
                m_curr_game_contributing_to_challenge = value;
            }
        }

        public Challenge Curr_challenge_before_update
        {
            get
            {
                return m_curr_challenge_before_update;
            }

            set
            {
                m_curr_challenge_before_update = value;
            }
        }

        public virtual bool IsMissionExpired()
        {
            return (ModelManager.Instance.GetMission().End_date - DateTime.Now).TotalSeconds < 0;
        }

        protected bool CheckClaimActiveMission()
        {
            if (To_claim_mission != null)
            {
                if (To_claim_mission.Count > 0)
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        public virtual bool UserOverMissionMinLevel()
        {
            return ModelManager.Instance.GetUser().GetLevel() >= m_min_mission_level;
        }

        public bool ShowMissionCollectIndication()
        {

            if (m_to_claim_mission.Count > 0)
                return true;
            else
                return false;

        }

        /// <summary>
        /// updates the lobby icon according to active challenge
        /// </summary>
        public void UpdateMissionLobbyIcons()
        {
            if (ModelManager.Instance.GetMission() != null)
            {
                if (ModelManager.Instance.GetMission().Status == MissionStatus.Expired)
                    ValidateMissionActiveForLobby(null);
                else
                    ValidateMissionActiveForLobby(ModelManager.Instance.GetMission().GetChallenge(ModelManager.Instance.GetMission().Active_challenge_index));

            }
        }



        protected abstract void ValidateMissionActiveForLobby(Challenge challenge);

        public Sprite Locked_bg
        {
            get
            {
                return m_locked_bg;
            }
        }

        public Sprite Completed_bg
        {
            get
            {
                return m_completed_bg;
            }
        }

        public void MissionPopupTimerFinished()
        {
            //wait 3 seconds so if the server timer isnt same as the client timer we will not get an expired mission 
            StartCoroutine(RequestNewChallanges());
        }

        protected IEnumerator RequestNewChallanges(Action<bool> mission_received_callback = null, bool silent = false, bool auto_open_panel_enabled = true)
        {
            if (silent == false)
                CardGamesPopupManager.Instance.ShowWaitingIndication(true,20);

            //this is because of a "hole" in the server side between missions
            //the mission ends at 23:59:59 and the new one starts at 00:00:00
            yield return new WaitForSeconds(1.5f);

            LoggerController.Instance.Log("Requesting Get Challenges");

            CardGamesCommManager.Instance.GetChallengesMission(false, (bool success, long next_mission_date, Mission new_mission) =>
            {
                LoggerController.Instance.Log("Get Challenges success: " + success);

                if (success)
                {
                    ModelManager.Instance.SetMission(new_mission);
                    m_next_mission_date = next_mission_date;



                    //this means that auto get new missions will not show automatically on the game - the other times the popup is shown is already not on the game
                    HandleGetChallengesMissionSuccessful(silent, auto_open_panel_enabled);

                }
                else
                {
                    //adding this to prevent a situation that when requesting new challenges (mission) fails - i will still have a non null value in the mission - but its 
                    //already expired or completed - and the startNew Game will tell me to show the button of mission in the InGame
                    ModelManager.Instance.SetMission(null);
                    LoggerController.Instance.Log("Requesting new missions failed");
                }

                if (silent == false)
                    CardGamesPopupManager.Instance.ShowWaitingIndication(false);

                mission_received_callback?.Invoke(success);

            });
        }

        public abstract Sprite GetMissionIcon(Challenge challenge);

        public abstract string GetDescriptionFromChallenge(Challenge challenge);
        public abstract string GetTitleFromChallenge(Challenge challenge);

        public abstract bool IsChallengeActiveInActiveMatch(Challenge challenge, Match match = null);
        public abstract void NewRoundStarted(bool first_round);

        public abstract bool ShouldShowMissionButtonInGame();
        public abstract void MissionButtonClicked(bool claim = true);

        public abstract void HandleGetChallengesMissionSuccessful(bool silent = false, bool auto_open_panel_enabled = true);
        public abstract void ShowChallengeEndGamePanel(ChallengeEndGameView endGameView);

        public void UpdateMissionEndRound(Challenge challenge)
        {
            //updates the mission model and also progress to next challenge if needed

            m_curr_challenge_post_update = challenge;

            Mission mission = ModelManager.Instance.GetMission();

            int challenge_index = challenge.Order - 1;

            //store the challenge before the update
            m_curr_challenge_before_update = mission.GetChallenge(challenge_index);

            challenge.CopyStaticData(m_curr_challenge_before_update);

            mission.SetChallenge(challenge_index, challenge);
        }

        public void RequestMissionIfNeeded(Action<bool> mission_request_done)
        {
            Mission mission = ModelManager.Instance.GetMission();

            if (!UserOverMissionMinLevel())
            {
                // User is under the min level for missions

                mission_request_done?.Invoke(false);
                return;
            }

            if (mission == null)
            {
                // We do not have a misison yet, lets ask
                StartCoroutine(RequestNewChallanges(mission_request_done, false, false));
                return;
            }

            if (IsMissionExpired())
            {
                // SpadesMission has expired, request a new one
                StartCoroutine(RequestNewChallanges(mission_request_done, false, false));
                return;
            }

            // No need to requets a new mission
            mission_request_done?.Invoke(false);
        }

        /// <summary>
        ///updates the mission model and also progress to next challenge if needed
        /// </summary>
        public void UpdateMissionEndGame(Challenge challenge)
        {

            Challenge curr_challenge = null;
            Mission mission = ModelManager.Instance.GetMission();



            if (challenge != null)
            {
                int challenge_index = challenge.Order - 1;

                m_curr_challenge_post_update = challenge;


                //store the challenge before the update
                m_curr_challenge_before_update = mission.GetChallenge(challenge_index);

                challenge.CopyStaticData(m_curr_challenge_before_update);

                mission.SetChallenge(challenge_index, challenge);

                curr_challenge = mission.GetChallenge(challenge_index);

            }
            else
            {
                curr_challenge = m_curr_challenge_post_update;
            }

            if (curr_challenge != null && curr_challenge.Status == ChallengeStatus.Completed)
            {
                if (mission != null)
                {
                    //this means the challenge is done - if there is another one - need to unlock it
                    if (curr_challenge.Order < mission.GetNumChallenges())
                    {
                        mission.Active_challenge_index = curr_challenge.Order;
                        mission.GetChallenge(curr_challenge.Order).Status = ChallengeStatus.InProgress;

                        //UpdateMissionLobbyIcons();
                    }
                    else
                        TrackingManager.Instance.AppsFlyerTrackOneTimeEvent("CompletedDailyChallanges");
                }

            }

        }

        public class ClaimMissionData
        {
            int m_mission_id;
            string m_mission_prize;


            public ClaimMissionData(int mission_id, string mission_prize)
            {
                m_mission_id = mission_id;
                m_mission_prize = mission_prize;
            }

            public int Mission_id
            {
                get
                {
                    return m_mission_id;
                }
            }

            public string Mission_prize
            {
                get
                {
                    return m_mission_prize;
                }
            }

        }
    }
}