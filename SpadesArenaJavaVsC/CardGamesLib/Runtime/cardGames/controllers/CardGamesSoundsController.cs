﻿using common.controllers;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.controllers
{
    public abstract class CardGamesSoundsController : SoundsController
    {

        public static new CardGamesSoundsController Instance;

        [SerializeField] protected List<AudioClip> m_mission_sfx = new List<AudioClip>();

        [SerializeField] AudioClip Game_Win1 = null;
        [SerializeField] AudioClip Game_Win2 = null;

        [SerializeField] AudioClip m_onboarding = null;
        [SerializeField] AudioClip Collect_Bonus = null;
        [SerializeField] AudioClip Card_Dealt = null;
        [SerializeField] AudioClip Card_Down = null;
        [SerializeField] AudioClip CardSDragged = null;

        [SerializeField] AudioClip Start_Turn = null;
        [SerializeField] protected AudioClip Timer_Alert = null;

        [SerializeField] AudioClip Game_Lose = null;
        [SerializeField] AudioClip XP_Update = null;
        [SerializeField] AudioClip Level_Up = null;
        [SerializeField] AudioClip m_leaderboard_unlock = null;
        [SerializeField] AudioClip Gem_Win = null;

        [SerializeField] AudioClip Popup_Open = null;
        [SerializeField] AudioClip Popup_Close = null;
        [SerializeField] AudioClip Bobble_Join = null;
        [SerializeField] AudioClip Bobble_Change = null;

        [SerializeField] AudioClip Table_Unlocked = null;
        [SerializeField] AudioClip Collectible_Unlocked = null;

        [SerializeField] AudioClip ButtonClick = null;
        [SerializeField] AudioClip Button_Toggle = null;
        [Tooltip("used for SliderExtremeSelectButton min")]
        [SerializeField] AudioClip Button_Set_Slider_Min_Click = null;
        [Tooltip("used for SliderExtremeSelectButton max")]
        [SerializeField] AudioClip Button_Set_Slider_Max_Click = null;
        [Tooltip("used for SFX volume after changing value")]
        [SerializeField] AudioClip Slider_Value_Changed_By_Drag = null;

        [SerializeField] AudioClip Cards_Flip = null;

        [SerializeField] AudioClip Cashier = null;
        [SerializeField]protected AudioClip Coins_Balancer = null;
        [SerializeField] protected AudioClip Coins_Trail_SFX = null;

        [SerializeField] AudioClip Searching = null;
        [SerializeField] AudioClip Stage_Cleared = null;
        [SerializeField] AudioClip Summary_Screen = null;

        [SerializeField] AudioClip m_stage_3_pipes = null;
        [SerializeField] AudioClip m_stage_1_pipe = null;

        [SerializeField] AudioClip m_achievement = null;

        [SerializeField] AudioClip m_panel_rotating = null;
        [SerializeField] AudioClip m_panel_selector = null;

        [SerializeField] AudioClip m_fast_forward = null;

        [Header("Avatars")]

        [SerializeField] AudioClip Avatar_Man_Angry = null;
        [SerializeField] AudioClip Avatar_Man_Happy = null;
        [SerializeField] AudioClip Avatar_Man_Sad = null;
        [SerializeField] AudioClip Avatar_Man_InLove = null;
        [SerializeField] AudioClip Avatar_Man_Cursing = null;
        [SerializeField] AudioClip Avatar_Man_Sleepy = null;

        [SerializeField] AudioClip Avatar_Women_Angry = null;
        [SerializeField] AudioClip Avatar_Women_Sad = null;
        [SerializeField] AudioClip Avatar_Women_Happy = null;
        [SerializeField] AudioClip Avatar_Women_InLove = null;
        [SerializeField] AudioClip Avatar_Women_Cursing = null;
        [SerializeField] AudioClip Avatar_Women_Sleepy = null;
        [SerializeField] AudioClip Avatar_Arena_Stage_Jump = null;

        [SerializeField] AudioClip Chat_Open = null;
        [SerializeField] AudioClip Chat_Closed = null;
        [SerializeField] AudioClip Chat_Bubble = null;
        [SerializeField] AudioClip BG_Music = null;

        [SerializeField] AudioClip Lobby_Nav = null;
        [SerializeField] AudioClip Tab_Click = null;

        [Header("Contests")]
        [SerializeField] AudioClip Contest_Big_Win = null;
        [SerializeField] AudioClip Contest_Bounce = null;
        [SerializeField] AudioClip Contest_Bounce_X3 = null;
        [SerializeField] AudioClip Contest_Enter = null;
        [SerializeField] AudioClip Contest_Number_Growing = null;
        [SerializeField] AudioClip Contest_Shoot_Up= null;
        [SerializeField] AudioClip Contest_Slide= null;
        [SerializeField] AudioClip Contest_Small_Trail= null;
        [SerializeField] AudioClip Contest_Small_Win= null;
        [SerializeField] AudioClip Contest_Snap_To_Position= null;
        [SerializeField] AudioClip Contest_Switch_Places= null;
        [SerializeField] AudioClip Contest_Tooltip_Appear= null;


        [Header("Slots")]
        [SerializeField] protected AudioClip Slot_button_release = null;
        [SerializeField] protected AudioClip Slot_coins_count = null;
        [SerializeField] protected AudioClip Slot_coins_trail = null;
        [SerializeField] protected AudioClip Slot_music_intro = null;
        [SerializeField] protected AudioClip Slot_music_loop1 = null;
        [SerializeField] protected AudioClip Slot_music_loop2 = null;
        [SerializeField] protected AudioClip Slot_paytable_slider1 = null;
        [SerializeField] protected AudioClip Slot_paytable_slider2 = null;

        [SerializeField] protected AudioClip[] Slot_Spin_Stop = null;

        [SerializeField] protected AudioClip Slot_spin_Loop = null;
        [SerializeField] protected AudioClip Slot_total_Sliders = null;
        [SerializeField] protected AudioClip Slot_Win_indicator = null;

        [SerializeField] protected AudioSource m_Slot_Loop_Source1 = null;
        [SerializeField] protected AudioSource m_Slot_Loop_Source2 = null;

        [Header("Piggy")]
        [Header("Piggy popup")]
        [Tooltip("accumulating and full on active plan for popup")]
        [SerializeField] protected AudioClip Active_piggy_music_loop = null;
        [Tooltip("on cooldown for popup")]
        [SerializeField] protected AudioClip Sleeping_piggy_music_loop = null;
        [SerializeField] protected AudioClip Piggy_bank_sign_SFX = null;
        [SerializeField] protected AudioClip Piggy_click_on_buy_button_SFX = null;
        [SerializeField] protected AudioClip Piggy_breaking_sequence_SFX_and_music = null;

        [Header("Piggy header")]
        [Tooltip("end game - money in")]
        [SerializeField] protected AudioClip Piggy_deposit_successful = null;
        [Tooltip("end game - piggy full")]
        [SerializeField] protected AudioClip Piggy_deposit_rejected = null;

        protected AudioSource m_spin_loop_channel = null;

        protected AudioSource m_slot_intro_channel = null;

        protected IEnumerator m_slot_loop_routine = null;
        [Space]
        /********************** ARENA SOUNDS ********************/

        [SerializeField] AudioClip m_arena_jump = null;
        [SerializeField] AudioClip m_arena_win = null;

        /********************************************************/

        [SerializeField] AudioClip m_Purchase_Success = null;
        [SerializeField] AudioClip m_Bonus_Received = null;


        protected AudioSource m_timer_channel = null;



        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
                Instance = this;
        }

        public virtual void Start()
        {
            BackgroundController.Instance.OnSessionTimeout += CleanAllSFXChannels;
            PopupManager.PopupOpen += PopupOpen;
            PopupManager.PopupClose += PopupClose;

            SetBGMusic(BG_Music);
        }

        public override void AdjustMusicAndSFX()
        {
            base.AdjustMusicAndSFX();

            float musicValue = StateController.Instance.GetMusicSetting();
            float sfxValue = StateController.Instance.GetSFXSetting();

            m_Slot_Loop_Source1.volume = sfxValue;
            m_Slot_Loop_Source1.mute = m_Slot_Loop_Source1.volume == 0 ? true : false;

            m_Slot_Loop_Source2.volume = sfxValue;
            m_Slot_Loop_Source2.mute = m_Slot_Loop_Source2.volume == 0 ? true : false;

            if (m_spin_loop_channel != null)
            {
                m_spin_loop_channel.volume = sfxValue;
                m_spin_loop_channel.mute = m_spin_loop_channel.volume == 0 ? true : false;
            }

            if (m_slot_intro_channel != null)
            {
                m_slot_intro_channel.volume = sfxValue;
                m_slot_intro_channel.mute = m_slot_intro_channel.volume == 0 ? true : false;
            }
        }
            public void Trigger_Bounce()
        {
            PlayClip(Contest_Bounce);
        }
        public void Trigger_BounceX3()
        {
            PlayClip(Contest_Bounce_X3);
        }
        public void Trigger_ShootUp()
        {
            PlayClip(Contest_Shoot_Up);
        }
        public void Trigger_BigWing()
        {
            PlayClip(Contest_Big_Win);
        }
        public void Trigger_SmallWin()
        {
            PlayClip(Contest_Small_Win);
        }
        public void Trigger_NumberGrowing()
        {
            PlayClip(Contest_Number_Growing);
        }
        public void Trigger_SmallTrail()
        {
            PlayClip(Contest_Small_Trail);
        }
        public void Trigger_SwitchPlaces()
        {
            PlayClip(Contest_Switch_Places);
        }
        public void Trigger_SnapToPosition()
        {
            PlayClip(Contest_Snap_To_Position);
        }
        public void Trigger_TooltipAppear()
        {
            PlayClip(Contest_Tooltip_Appear);
        }
        public void Trigger_Slide()
        {
            PlayClip(Contest_Slide);
        }
        public void Trigger_Enter()
        {
            PlayClip(Contest_Enter);
        }

        public void PlayMissionSFX(SoundsController.MissionSFX missionSFX)
        {
            PlayClip(m_mission_sfx[(int)missionSFX]);
        }


        public void CollectBonus()
        {
            PlayClip(Collect_Bonus);
        }


        protected override void HandleSFX(bool channel_on)
        {
            base.HandleSFX(channel_on);

            m_Slot_Loop_Source1.enabled = channel_on;
            m_Slot_Loop_Source2.enabled = channel_on;

        }


        public override int GetHashCode()
        {
            return GetHashCode();
        }

        protected void PopupOpen()
        {
            PlayClip(Popup_Open);
        }

        protected void PopupClose()
        {
            PlayClip(Popup_Close);
        }

        public void BobbleJoin()
        {
            PlayClip(Bobble_Join);
        }

        protected void BobbleChange()
        {
            PlayClip(Bobble_Change);
        }

        public void PlayPopSounds()
        {
            PlayClip(m_stage_1_pipe);
        }

        public virtual void PlayTurnTimerAlert()
        {
            if (m_timer_channel == null)
                m_timer_channel = PlayClip(Timer_Alert);
        }

        public virtual void StopTurnTimerAlert()
        {
            if (m_timer_channel != null)
            {
                StopClip(m_timer_channel);
                m_timer_channel = null;
            }
        }

        public void GameWin()
        {
            PlayClip(Game_Win1);
            StartCoroutine(PlayClipWithDelay(Game_Win2, 1.2f));
        }


        #region piggy
        public void PlayPiggySignSwooshIn()
        {
            PlayClip(Piggy_bank_sign_SFX);
        }

        /// <summary>
        /// playing when piggy popup opens, different music loops between cooldown and otherwise
        /// </summary>
        /// <param name="isSleeping"></param>
        public void StartPiggyMusicLoop(bool isSleeping = false)
        {
            m_BG_Music_Source.Stop();
            m_Slot_Loop_Source1.clip = isSleeping? Sleeping_piggy_music_loop : Active_piggy_music_loop;
            m_Slot_Loop_Source1.Play();
        }

        public void PlayPiggyPurchaseButtonSFX()
        {
            PlayClip(Piggy_click_on_buy_button_SFX);
        }

        public void PlayPiggyBreakSequence()
        {
            m_slot_intro_channel = PlayClip(Piggy_breaking_sequence_SFX_and_music);
        }


        public void StopPiggyMusic()
        {
            m_BG_Music_Source.Play();
            m_Slot_Loop_Source1.Stop();
        }

        public void PlayPiggyDeposit(bool successful = true)
        {
            if (successful)
                PlayClip(Piggy_deposit_successful);
            else
                PlayClip(Piggy_deposit_rejected);
        }
        #endregion

        #region slot machine
        public void StartSlotIntro(bool start)
        {

            if (start)
            {
                m_BG_Music_Source.Stop();

                m_slot_intro_channel = PlayClip(Slot_music_intro);

                m_Slot_Loop_Source1.clip = Slot_music_loop1;
                m_Slot_Loop_Source2.clip = Slot_music_loop2;
                m_Slot_Loop_Source2.volume = 0f;

                m_slot_loop_routine = DelayPlayBGSlotMusic();

                StartCoroutine(m_slot_loop_routine);
            }
            else
            {
                m_BG_Music_Source.Play();

                if (m_slot_loop_routine != null)
                    StopCoroutine(m_slot_loop_routine);

                m_slot_intro_channel.Stop();
                m_Slot_Loop_Source1.Stop();
                m_Slot_Loop_Source2.Stop();
            }

        }

        protected IEnumerator DelayPlayBGSlotMusic()
        {
            yield return new WaitForSecondsRealtime(5.9f);
            m_Slot_Loop_Source1.Play();
            m_Slot_Loop_Source2.Play();
        }

        public void PlaySlotWinIndicator()
        {
            PlayClip(Slot_Win_indicator);
        }

        public void PlaySlotSidePanels()
        {
            PlayClip(Slot_paytable_slider1);
            PlayClip(Slot_paytable_slider2);
        }

        public void SpinStop(int index)
        {
            PlayClip(Slot_Spin_Stop[index]);
        }

        public void SpinLoop(bool stop = false)
        {
            if (!stop)
                m_spin_loop_channel = PlayClip(Slot_spin_Loop, true);
            else
            {
                if (m_spin_loop_channel != null)
                    StopClip(m_spin_loop_channel);
            }
        }

        public void SlotXPUpdate()
        {
            PlayClip(Slot_coins_count);
        }

        public void SlotTotalCount()
        {
            PlayClip(Slot_total_Sliders);
        }

        public void PlaySlotTrail()
        {
            PlayClip(Slot_coins_trail);
        }

        public void PullSlotHandle()
        {
            PlayClip(Slot_button_release);
        }

        #endregion

        public void AvatarHappy(bool man)
        {
            if (man)
            {
                PlayClip(Avatar_Man_Happy);
            }
            else
            {
                PlayClip(Avatar_Women_Happy);
            }
        }

        public void AvatarAngry(bool man)
        {
            if (man)
            {
                PlayClip(Avatar_Man_Angry);
            }
            else
            {
                PlayClip(Avatar_Women_Angry);
            }
        }

        public void AvatarSad(bool man)
        {
            if (man)
            {
                PlayClip(Avatar_Man_Sad);
            }
            else
            {
                PlayClip(Avatar_Women_Sad);
            }
        }

        public void AvatarInLove(bool man)
        {
            if (man)
            {
                PlayClip(Avatar_Man_InLove);
            }
            else
            {
                PlayClip(Avatar_Women_InLove);
            }
        }

        public void AvatarCursing(bool man)
        {
            if (man)
            {
                PlayClip(Avatar_Man_Cursing);
            }
            else
            {
                PlayClip(Avatar_Women_Cursing);
            }
        }

        public void AvatarSleepy(bool man)
        {
            if (man)
            {
                PlayClip(Avatar_Man_Sleepy);
            }
            else
            {
                PlayClip(Avatar_Women_Sleepy);
            }
        }

        public void ChatOpenClose(bool open)
        {
            if (open)
                PlayClip(Chat_Open);
            else
                PlayClip(Chat_Closed);
        }

        public void ChatBubble()
        {
            PlayClip(Chat_Bubble);
        }

        public void CardDragged()
        {
            PlayClip(CardSDragged);
        }

        public void CardDealt()
        {
            PlayClip(Card_Dealt);
        }

        public void CardsFlip()
        {
            PlayClip(Cards_Flip);
        }

        public void LobbyNav()
        {
            PlayClip(Lobby_Nav);
        }

        public void TabsSFX()
        {
            PlayClip(Tab_Click);
        }

        public void StartTurn()
        {
            PlayClip(Start_Turn);
        }

        public void StageFX3Pipes()
        {
            PlayClip(m_stage_3_pipes, false);
        }

        public void StageFX1Pipe()
        {
            PlayClip(m_stage_1_pipe, false);
        }

        public void PlayPanelFlip()
        {
            PlayClip(m_panel_rotating, false);
        }

        public void PlayPanelSelector()
        {
            PlayClip(m_panel_selector, false);
        }

        public void Achievements()
        {
            PlayClip(m_achievement, false);
        }

        public void CardThrown()
        {
            PlayClip(Card_Down);
        }

        public void TableUnlocked()
        {
            PlayClip(Table_Unlocked);
        }

        public void CollectibleUnlocked()
        {
            PlayClip(Collectible_Unlocked);
        }

        public void ButtonClicked()
        {
            PlayClip(ButtonClick);
        }

        public void ButtonToggle()
        {
            PlayClip(Button_Toggle);
        }

        //TODO: find a way to make it part of the button's script. problem: this button is outside CommonLib as it resides in
        //UnityEngine.UI thus doesn't know about our namespaces. Assign to play it on click on another script is the current way
        //and probably the correct way but maybe there's a better way. Shay.
        public void ButtonSetSliderClickedMin()
        {
            if (Button_Set_Slider_Min_Click != null)
                PlayClip(Button_Set_Slider_Min_Click);
        }

        public void ButtonSetSliderClickedMax()
        {
            if (Button_Set_Slider_Max_Click != null)
                PlayClip(Button_Set_Slider_Max_Click);
        }

        public void SliderValueChangedSFX()
        {
            PlayClip(Slider_Value_Changed_By_Drag);
        }

        public void GameLose()
        {
            PlayClip(Game_Lose);
        }



        public void XPUpdate()
        {
            PlayClip(XP_Update);
        }

        public void LevelUp()
        {
            StartCoroutine(PlayClipWithDelay(Level_Up, 0.18f));
        }

        protected IEnumerator PlayClipWithDelay(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds(delay);
            PlayClip(clip);
        }

        public void GemWin()
        {
            PlayClip(Gem_Win);
        }

        public void CashierSFX()
        {
            PlayClip(Cashier);
        }

        public void Coins_Balance()
        {
            PlayClip(Coins_Balancer);
        }

        public void Coins_Trail()
        {
            PlayClip(Coins_Trail_SFX);
        }

        public void PlayOnBoarding()
        {
            PlayClip(m_onboarding);
        }

        public void StageCleared()
        {
            PlayClip(Stage_Cleared);
        }

        public void SummaryScreen()
        {
            PlayClip(Summary_Screen);
        }

        AudioClip PickRandomFromArray(AudioClip[] array)
        {
            if (array == null || array.Length == 0)
                return null;
            int pick = UnityEngine.Random.Range(0, array.Length);
            return array[pick];
        }

        public void ArenaFall()
        {
            PlayClip(m_arena_jump);
        }

        public void ArenaJump()
        {
            PlayClip(Avatar_Arena_Stage_Jump);
        }

        public void ArenaWin()
        {
            PlayClip(m_arena_win);
        }

        public void LeaderboardUnlocked()
        {
            PlayClip(m_leaderboard_unlock);
        }

        public void PlayPurchaseSuccess()
        {
            PlayClip(m_Purchase_Success);
        }

        public void PlayBonusReceived()
        {
            PlayClip(m_Bonus_Received);
        }
    }
}