﻿using cardGames.models;
using common.utils;
using System;

namespace cardGames.controllers
{
    /// <summary>
    /// currently not in use. needs refactoring so this could serve as a common base for Spades and Gin. Currently
    /// too many things are hard-coded to spades and since singletons cannot be derived we need to think hard how to
    /// overcome this hardle. Probably with splitting scripts + reverse-delegations / Funcs<>.
    /// 
    /// However the "old" UserStatsController was renamed SpadesUserStatsController for the rest of the project until
    /// refactoring will address this.
    /// </summary>
    public abstract class UserStatsController : MonoBehaviour
    {
        public static UserStatsController Instance;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public virtual JSONNode SerializeUserStats(UserStats stats)
        {
            JSONObject result = new JSONObject();

            result["ts"].AsInt = stats.SaveTimeStamp;

            result["rp"].AsInt = stats.RoundPlayed;

            result["ap"].AsInt = stats.ArenaPlayed;
            result["cw"].AsInt = stats.CoinsWon;

            result["bw"].AsInt = stats.BiggestCoinsWin;

            for (int i = 0; i < stats.FiveGamesState.Count; i++)
            {
                result["flg"] += stats.FiveGamesState[i].ToString();
            }

            return result;
        }

        public virtual UserStats ParseUserStats(JSONNode statsJson)
        {
            UserStats stats = CreateUserStats();

            if (statsJson["ts"] != null)
            {
                stats.SaveTimeStamp = statsJson["ts"].AsInt;
            }

            stats.ArenaPlayed = statsJson["ap"].AsInt;
            stats.CoinsWon = statsJson["cw"].AsInt;

            stats.FiveGamesState.Clear();

            String str = statsJson["flg"];

            for (int i = 0; str != null && i < str.Length; i++)
            {
                stats.FiveGamesState.Add(Convert.ToInt32(str.Substring(i, 1)));
            }

            return stats;
        }

        /// <summary>
        /// Updates all the statistics related to starting an arena.
        /// </summary>
        public void UpdateStartArena()
        {
            UserStats stats = GetUserStats();

            stats.ArenaPlayed++;

            SaveUserStatsLocaly(stats);
        }

        protected void SaveUserStatsLocaly(UserStats stats)
        {
            stats.SaveTimeStamp = DateUtils.UnixTimestamp();

            string statsStr = SerializeUserStats(stats).ToString();

            HandleSaveUserStatsJson(statsStr);           
        }

        public abstract void UpdateEndMatch(bool win, int coinsWon = 0);


        protected UserStats GetUserStats()
        {
            return ModelManager.Instance.GetUser().Stats;
        }

        protected abstract UserStats CreateUserStats();

        protected abstract void HandleSaveUserStatsJson(string statsStr);

        public UserStats LoadUserStatsLocaly()
        {
            string statsStr = LoadUserStatsJson();
            if (statsStr == null || statsStr.Trim() == "")
                return null;

            JSONNode statsJson = JSON.Parse(statsStr);

            return ParseUserStats(statsJson);
        }

        protected abstract string LoadUserStatsJson();
    }
}
