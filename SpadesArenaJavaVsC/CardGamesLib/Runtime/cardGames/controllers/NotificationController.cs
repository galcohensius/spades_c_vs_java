﻿using cardGames.models;
using cardGames.models.contests;
using common.controllers;
using common.models;
using common.notification.local;
using System;
using System.Collections.Generic;
using System.Linq;

namespace cardGames.controllers
{

    public class NotificationController : MonoBehaviour
    {

        private const int DEAD_HOURS_START = 23;
        private const int DEAD_HOURS_END = 7;

        public const int DAILY_NOTIF_ID = 1;
        public const int HOURLY_NOTIF_ID = 2;
        public const int FORGOT_HOURLY_BONUS_NOTIF_ID = 3;
        public const int NEW_USER_NOTIF_ID = 4;
        //public const int FORGOT_GIFT_NOTIF_ID = 5; *Removed
        //public const int NOT_IN_GAME_3DAYS_NOTIF_ID = 6; *Replaced with remote
        //public const int NOT_IN_GAME_7DAYS_NOTIF_ID = 7; *Replaced with remote
        public const int CHALLENGE_NOTIF_ID = 8;
        //public const int NOT_IN_GAME_14DAYS_NOTIF_ID = 9; *Replaced with remote
        //public const int NOT_IN_GAME_28DAYS_NOTIF_ID = 10; *Replaced with remote
        public const int LEADERBOARD_END_NOTIF_ID = 11;
        public const int CONTEST_ABOUT_TO_START_NOTIF_ID = 12;
        public const int CONTEST_ABOUT_TO_END_FIRST_NOTIF_ID = 13;
        public const int CONTEST_ABOUT_TO_END_SECOND_NOTIF_ID = 14;
        public const int VOUCHER_EXPIRED_NOTIF_ID = 15;
        public const int PIGGY_ABOUT_TO_EXPIRE_CUSTOM_TIME = 16;
        public const int PIGGY_ABOUT_TO_EXPIRE_IN_48_HOURS = 17; //for when the timer hits 48 last hours
        public const int PIGGY_ABOUT_TO_EXPIRE_IN_24_HOURS = 18; //for when the timer hits 24 last hours
        public const int PIGGY_FULL_IMMEDIATE_PUSH = -1;


        public const string EMOJI_MONEY_FACE = "\U0001F911";
        public const string EMOJI_SLOT_MACHINE = "\U0001F3B0";
        public const string EMOJI_ALARM_CLOCK = "\u23F0";
        public const string EMOJI_MONEY_BAG = "\U0001F4B0";
        public const string EMOJI_GIFT_BOX = "\U0001F381";
        public const string EMOJI_SPADE_SUIT = "\u2660";
        public const string EMOJI_CROWN = "\U0001F451";
        public const string EMOJI_BLACK_HEART = "\U0001F5A4";
        public const string EMOJI_TROPHY = "\U0001F3C6";
        public const string EMOJI_PIG_FACE = "\U0001F437";
        public const string EMOJI_MONEY_WITH_WINGS = "\U0001F4B8";
        public const string EMOJI_ZZZ = "\U0001F4A4";

        NotificationManager m_notification_manager = null;

        public static NotificationController Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {
            // Must be called AFTER CardGamesPushWooshNotificator's Start method is invoked
            // we use the script execution order settings to make sure
            m_notification_manager = NotificationManager.Instance;
        }

        /// <summary>
        /// This function sechedules a notification for the contest when the player is in the money
        /// </summary>
        public void ScheduleContestNotificationInTheMoney(Contest contest, TimeSpan interval, bool isFirst)
        {
            if (isFirst)
                CancelPendingNotifications(CONTEST_ABOUT_TO_END_FIRST_NOTIF_ID);
            else
                CancelPendingNotifications(CONTEST_ABOUT_TO_END_SECOND_NOTIF_ID);

            //CancelPendingNotifications(CONTEST_ABOUT_TO_END_SECOND_NOTIF_ID);
         //   CancelContestNotification();
            if (!StateController.Instance.GetNotificationSetting())
                return;

            if (interval < TimeSpan.FromSeconds(30))
                return;

            string header = "The contest is about to end! " + EMOJI_TROPHY;
            string body = "Defend your prize before the contest is over! " + EMOJI_MONEY_FACE + EMOJI_MONEY_BAG;

            if (isFirst)
                m_notification_manager.ScheduleNotification(CONTEST_ABOUT_TO_END_FIRST_NOTIF_ID, CheckDeadHours(interval), header, body, "coins");
            else
                m_notification_manager.ScheduleNotification(CONTEST_ABOUT_TO_END_SECOND_NOTIF_ID, CheckDeadHours(interval), header, body, "coins");
        }

        /// <summary>
        /// should be called when new piggy becomes available (and after each time money added if amount of money will be updated dybamically)
        /// </summary>
        public void SchedulePiggyTimeRemindingNotification(bool usePopupText = false)
        {
            //clean up if notifications remain
            CancelPiggyTimeRemindingNotifications();

            if (!StateController.Instance.GetNotificationSetting())
                return;

            UserPiggyData piggyData = ModelManager.Instance.GetPiggy();
            if (piggyData.PiggyState == PiggyState.Cooldown)
            {
                LoggerController.Instance.LogWarning("tried to scheduele piggy on cooldown which ATM is not permitted");
                return;
            }

            string header, body;
            if (usePopupText)
            {
                if (piggyData.PiggyState == PiggyState.Accumulating)
                {
                    header = "don't lose your savings" + EMOJI_MONEY_BAG;
                    body = "last chance to break your piggy" + EMOJI_PIG_FACE; 
                }
                else //piggy is full
                {
                    header = "your piggy is full" + EMOJI_PIG_FACE;
                    body = "don't let the extra coins go to waste" + EMOJI_MONEY_WITH_WINGS;
                }
            }
            else
            {
                header = "Oink! Don't miss your Piggy" + EMOJI_PIG_FACE;
                body = "Break the piggy before it falls asleep" + EMOJI_ZZZ;
            }

            //DateTime customTimeEnd = CardGamesStateController.Instance.GetPercentTimePiggyReminderDate();
            //if (customTimeEnd >= DateTime.Now)
            //{
            //    TimeSpan interval = new TimeSpan(customTimeEnd.Ticks - DateTime.Now.Ticks);
            //    m_notification_manager.ScheduleNotification(PIGGY_ABOUT_TO_EXPIRE_CUSTOM_TIME, CheckDeadHours(interval), header, body, "coins");
            //}
            TimeSpan endTime = new TimeSpan(piggyData.EndDate.Ticks - DateTime.Now.Ticks);
            if (endTime.TotalHours >= 48)
            {
                m_notification_manager.ScheduleNotification(PIGGY_ABOUT_TO_EXPIRE_IN_48_HOURS, CheckDeadHours(endTime.Subtract(new TimeSpan(48,0,0))), header, body, "coins");
            }
            if (endTime.TotalHours >= 24)
            {
                m_notification_manager.ScheduleNotification(PIGGY_ABOUT_TO_EXPIRE_IN_24_HOURS, CheckDeadHours(endTime.Subtract(new TimeSpan(24, 0, 0))), header, body, "coins");
            }

        }
        
        /// <summary>
        /// should be called when piggy is bought or expired
        /// </summary>
        public void CancelPiggyTimeRemindingNotifications()
        {
            CancelPendingNotifications(PIGGY_ABOUT_TO_EXPIRE_CUSTOM_TIME);
            CancelPendingNotifications(PIGGY_ABOUT_TO_EXPIRE_IN_48_HOURS);
            CancelPendingNotifications(PIGGY_ABOUT_TO_EXPIRE_IN_24_HOURS);
            CancelPendingNotifications(PIGGY_FULL_IMMEDIATE_PUSH);
        }

        public void PushPiggyFullNotification()
        {
            CancelPendingNotifications(PIGGY_FULL_IMMEDIATE_PUSH);
            string header = "Oink! Your piggy is full" + EMOJI_PIG_FACE;
            string body = "Break the piggy to make room for more coins" + EMOJI_MONEY_WITH_WINGS;
            m_notification_manager.ScheduleNotification(PIGGY_FULL_IMMEDIATE_PUSH, CheckDeadHours(new TimeSpan(0)), header, body, "coins");
        }

        /// <summary>
        /// This function sechedules a notification for the contest
        /// </summary>
        public void ScheduleContestNotification(Contest contest, TimeSpan interval)
        {
            // CancelPendingNotifications(CONTEST_ABOUT_TO_START_NOTIF_ID);
            CancelContestNotification();
            if (!StateController.Instance.GetNotificationSetting())
                return;

            if (interval < TimeSpan.FromSeconds(30))
                return;

            string header = "NEW CONTEST LIVE!" + EMOJI_TROPHY;
            string body = "Don't miss your chance in the new contest!" + EMOJI_MONEY_BAG;
            m_notification_manager.ScheduleNotification(CONTEST_ABOUT_TO_START_NOTIF_ID, CheckDeadHours(interval), header, body, "coins");
        }

        public void CancelContestNotification()
        {
            CancelPendingNotifications(CONTEST_ABOUT_TO_START_NOTIF_ID);
            CancelPendingNotifications(CONTEST_ABOUT_TO_END_FIRST_NOTIF_ID);
            CancelPendingNotifications(CONTEST_ABOUT_TO_END_SECOND_NOTIF_ID);
        }

        public void ScheduleDailyNotification()
        {
            CancelPendingNotifications(NotificationController.DAILY_NOTIF_ID);

            if (!StateController.Instance.GetNotificationSetting())
                return;

            DailyBonus bonus = (DailyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Daily);

            int bonus_time_secs = bonus.Interval;
            int coins = bonus.CoinsSingle;

            string header = "Get Free Coins";
            string body = "Your daily FREE spin is ready! " + EMOJI_SLOT_MACHINE + " Spin & Win! " + EMOJI_SLOT_MACHINE;
            m_notification_manager.ScheduleNotification(DAILY_NOTIF_ID, CheckDeadHours(TimeSpan.FromSeconds(bonus_time_secs)), header, body, "coins");
        }

        public void ScheduleHourlyNotification()
        {
            CancelPendingNotifications(NotificationController.HOURLY_NOTIF_ID);

            if (!StateController.Instance.GetNotificationSetting())
                return;

            HourlyBonus bonus = (HourlyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Hourly);

            double bonus_time_secs = bonus.Interval;

            int coins = bonus.CoinsSingle;
            string header = "Get Free Coins";
            string body = "Tap to collect your FREE COINS NOW! " + EMOJI_MONEY_FACE;

            m_notification_manager.ScheduleNotification(HOURLY_NOTIF_ID, CheckDeadHours(TimeSpan.FromSeconds(bonus_time_secs)), header, body, "coins");
        }


        public void ScheduleForgotHourlyBonusNotification()
        {
            CancelPendingNotifications(NotificationController.FORGOT_HOURLY_BONUS_NOTIF_ID);

            if (!StateController.Instance.GetNotificationSetting())
                return;

            string header = "Get Free Coins";
            string body = "Oops! You forgot to collect your FREE coins! " + EMOJI_MONEY_FACE;

            m_notification_manager.ScheduleNotification(FORGOT_HOURLY_BONUS_NOTIF_ID, CheckDeadHours(TimeSpan.FromHours(1)), header, body, "coins");
        }

        public void ScheduleNewUserNotification()
        {
            if (!StateController.Instance.GetNotificationSetting())
                return;

            string header = "Time For Spades";
            string body = EMOJI_ALARM_CLOCK + " It's game time - PLAY SPADES NOW!";

            m_notification_manager.ScheduleNotification(NEW_USER_NOTIF_ID, CheckDeadHours(TimeSpan.FromHours(23)), header, body, "coins");
        }

        /*
        public void ScheduleForgetGiftNotification ()
        {
            CancelPendingNotifications(SpadesNotificationController.FORGOT_GIFT_NOTIF_ID);

            if (!StateController.Instance.GetNotificationSetting ())
                return;

            string header = "Collect Free Gifts";
            string body = "Oops! You have gifts you forgot to collect!" + EMOJI_GIFT_BOX;
            m_notification_manager.ScheduleNotification (FORGOT_GIFT_NOTIF_ID, CheckDeadHours (TimeSpan.FromHours(1)), header, body, "coins");
        }

		*/

        public void ScheduleChallengeNotification()
        {
            if (!StateController.Instance.GetNotificationSetting())
                return;

            string header = "Complete The Arena SpadesChallenge";
            string body = "You've got an Arena challenge to complete" + EMOJI_MONEY_BAG + ", PLAY NOW!";
            m_notification_manager.ScheduleNotification(CHALLENGE_NOTIF_ID, CheckDeadHours(TimeSpan.FromHours(1)), header, body, "coins");
        }

        /*
		public void Schedule14DaysNotInGameNotification ()
		{
            CancelPendingNotifications(SpadesNotificationController.NOT_IN_GAME_14DAYS_NOTIF_ID);

			if (!StateController.Instance.GetNotificationSetting ())
				return;

            string header = "Time For Spades";
			string body = EMOJI_MONEY_BAG + " Keep your eyes on the prize " + EMOJI_MONEY_BAG + " Play Spades & Win Big!";
            m_notification_manager.ScheduleNotification (NOT_IN_GAME_14DAYS_NOTIF_ID, CheckDeadHours (TimeSpan.FromDays(14)), header, body, "coins");
		}

		public void Schedule28DaysNotInGameNotification ()
		{
            CancelPendingNotifications(SpadesNotificationController.NOT_IN_GAME_28DAYS_NOTIF_ID);

            if (!StateController.Instance.GetNotificationSetting ())
				return;

            string header = "Time For Spades";
			string body = EMOJI_SPADE_SUIT + " Spades is not a game. Spades is LIFE " + EMOJI_BLACK_HEART;
            m_notification_manager.ScheduleNotification (NOT_IN_GAME_28DAYS_NOTIF_ID, CheckDeadHours (TimeSpan.FromDays(24)), header, body, "coins");
		}
        */

        public void ScheduleLeaderboardEndNotification(TimeSpan endLeaderboardInterval)
        {
            if (!StateController.Instance.GetNotificationSetting())
                return;

            if (endLeaderboardInterval.TotalHours < 2)
                return;

            string header = "Time For Spades";
            string body = EMOJI_ALARM_CLOCK + " Leaderboard Ending Soon! Fight for the prize " + EMOJI_MONEY_BAG;


            m_notification_manager.ScheduleNotification(LEADERBOARD_END_NOTIF_ID, CheckDeadHours(endLeaderboardInterval - TimeSpan.FromHours(1)), header, body, "coins");
        }

        /// <summary>
        /// class to hold the local notification information
        /// </summary>
        private class LocalNotification
        {
            public double dueTimeMinutes { get; set; }
            public string header { get; set; }
            public string body { get; set; }

            public LocalNotification (double _due, string _header, string _body)
            {
                dueTimeMinutes = _due;
                header = _header;
                body = _body;
            }
        }

        /// <summary>
        /// Generate expiration notifications for the player's vouchers
        /// notification messages differ per the expiration timing
        /// multiple notifications are group to one (by hour) 
        /// </summary>
        private class VoucherExpireNotify
        {
            
            public List<LocalNotification> voucherNotifications = new List<LocalNotification>();

            private const int hoursGroupBy = 3;
            private List<VouchersGroup> vouchersGroup;
            #if QA_MODE
                private enum ExpirationMinutes { TWO_HOURS = 1, SIX_HOURS = 3, TWENTY_SIX_HOURS = 4};
            #else
                private enum ExpirationMinutes { TWO_HOURS = 120, SIX_HOURS = 360, TWENTY_SIX_HOURS = 1560 };
            #endif

            private Dictionary<ExpirationMinutes, string> expireNotificationHeader = new Dictionary<ExpirationMinutes, string>()
            {
                { ExpirationMinutes.TWO_HOURS, "📣Last chance!" },
                { ExpirationMinutes.SIX_HOURS, "👋Don't Miss out!" },
                { ExpirationMinutes.TWENTY_SIX_HOURS ,"" }
            };

            private Dictionary<ExpirationMinutes, string> singleExpireNotificationBody = new Dictionary<ExpirationMinutes, string>()
            {
                { ExpirationMinutes.TWO_HOURS, "Your ⭐FREE GAME VOUCHER⭐ is about to expire! 🔥PLAY NOW🔥" },
                { ExpirationMinutes.SIX_HOURS, "Your ⭐FREE GAME VOUCHER⭐ awaits! ✨PLAY NOW✨" },
                { ExpirationMinutes.TWENTY_SIX_HOURS ,"🔥 Play your ⭐FREE GAME ⭐ today 🔥 " }
            };

            private Dictionary<ExpirationMinutes, string> multipleExpireNotificationBody = new Dictionary<ExpirationMinutes, string>()
            {
                { ExpirationMinutes.TWO_HOURS, "Your ⭐FREE GAME VOUCHERS⭐ are about to expire! 🔥PLAY NOW🔥" },
                { ExpirationMinutes.SIX_HOURS, "Your ⭐FREE GAME VOUCHERS⭐ await! ✨PLAY NOW✨" },
                { ExpirationMinutes.TWENTY_SIX_HOURS ,"🔥 Play your ⭐FREE GAMES ⭐ today 🔥" }
            };

            public VoucherExpireNotify(List<VouchersGroup> _vouchersGroup)
            {
                vouchersGroup = _vouchersGroup;
            }

            /// <summary>
            /// Group vouchers by the expiration date time
            /// and generate the relevant message accordingly
            /// </summary>
            public void CreateVouchersNotification()
            { 
                List<DateTime> voucherExpirations = new List<DateTime>();
                DateTime now = DateTime.Now;
                foreach (VouchersGroup vouchers in vouchersGroup)
                    voucherExpirations.Add(vouchers.EarliestExpireTime);
                
                // group by the hour
                var hourSpan = new TimeSpan(hoursGroupBy, 0, 0);
                var vouchersList = voucherExpirations.GroupBy(expirationTime => {
                    return expirationTime.Ticks / hourSpan.Ticks;})
                    .Select(expirationTime => 
                                        new { Hour = new DateTime(expirationTime.Key * hourSpan.Ticks),
                                              Count = expirationTime.Sum(x => 1),
                                              EarliestExpiration = expirationTime.Min(),
                                              MinutesToExpire = expirationTime.Min().Subtract(DateTime.Now).TotalMinutes
                                        }).ToList();

                foreach (var entry in vouchersList)
                {
                    LoggerController.Instance.Log(entry.Count.ToString() +  " Vouchers in expiration group :  " + entry.Hour.ToString() + ", Earliest: " + entry.EarliestExpiration.ToString());
                    SetVoucherNotification(entry.MinutesToExpire, entry.Count > 1);
                }

            }

            /// <summary>
            /// Generate the relevent message according to the expiration time & grouping
            /// </summary>
            /// <param name="voucherExpirationMinutes">Time for expiration in minute</param>
            /// <param name="multipleVouchers">is group of vouchers</param>
            private void SetVoucherNotification (double voucherExpirationMinutes, bool multipleVouchers)
            {
                string header = "";
                string body = "";
                foreach (ExpirationMinutes expiration in Enum.GetValues(typeof(ExpirationMinutes)))
                {
                    if (voucherExpirationMinutes > (int)expiration)
                    {
                        header = expireNotificationHeader[expiration];
                        if (multipleVouchers)
                            body = multipleExpireNotificationBody[expiration];
                        else
                            body = singleExpireNotificationBody[expiration];
                        voucherNotifications.Add(new LocalNotification(voucherExpirationMinutes - (int)expiration, header, body));
                    }
                }
            }

        }

        /// <summary>
        /// Create local notifications to remind the player to use the vouchers he received
        /// </summary>
        public void ScheduleVoucherExpiredNotification()
        {
            CancelPendingNotifications(NotificationController.VOUCHER_EXPIRED_NOTIF_ID);
            VoucherExpireNotify vouchersNotification = new VoucherExpireNotify(ModelManager.Instance.Vouchers);
            vouchersNotification.CreateVouchersNotification();
            foreach (LocalNotification notification in vouchersNotification.voucherNotifications)
            {
                if (! IsInDeadHours( TimeSpan.FromMinutes(notification.dueTimeMinutes )) )
                    m_notification_manager.ScheduleNotification(
                        VOUCHER_EXPIRED_NOTIF_ID,
                        TimeSpan.FromMinutes(notification.dueTimeMinutes), 
                        notification.header, 
                        notification.body, 
                        "coins");
            }
        }

        public void CancelPendingNotifications()
        {
            m_notification_manager.CancelPendingNotifications();

        }

        public void CancelPendingNotifications(int notification_id)
        {
            m_notification_manager.CancelPendingNotifications(notification_id);
        }


        public void RequestNotificationsPermission()
        {
            m_notification_manager.RequestNotificationsPermission();
        }

        public bool IsNotificationsPermissionEnabled()
        {
            return m_notification_manager.IsNotificationsPermissionEnabled();
        }

        public void CancelProcessedNotifications()
        {
            if (m_notification_manager != null)
                m_notification_manager.CancelProcessedNotifications();
        }

        /// <summary>
        /// Check if time span is in the dead hours range
        /// </summary>
        private bool IsInDeadHours (TimeSpan span)
        {
            DateTime future = DateTime.Now + span;
            return (future.Hour >= DEAD_HOURS_START || future.Hour < DEAD_HOURS_END);
        }

        /// <summary>
        /// Calculates a new time span if it falls on "dead hours" in the future.
        /// </summary>
		private TimeSpan CheckDeadHours(TimeSpan span)
        {
            DateTime future = DateTime.Now + span;

            if (future.Hour >= DEAD_HOURS_START || future.Hour < DEAD_HOURS_END)
            {
                // The future falls inside the dead hours, forward it to after the dead hours
                DateTime sevenAm = new DateTime(future.Year, future.Month, future.Day, 7, 0, 0);
                if (sevenAm < future)
                    span = sevenAm.AddDays(1) - DateTime.Now;
                else
                    span = sevenAm - DateTime.Now;
            }
            return span;
        }
    }
}