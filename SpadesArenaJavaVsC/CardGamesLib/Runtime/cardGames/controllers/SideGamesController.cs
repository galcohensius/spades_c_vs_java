﻿using System;
using cardGames.comm;
using cardGames.models;
using common.controllers;
using common.ims;
using common.views.popups;

namespace cardGames.controllers
{
    /// <summary>
    /// Side games controller
    /// </summary>
    public class SideGamesController : MonoBehaviour
    {

        public enum SideGames
        {
            Slot3,
            Slot5,
            BlackJack,
            Roulette
        }
        private const int CUR_SLOT3_MACHINE_ID = 0;



        LoadingAssetPopup m_loadingAssetPopup = null;
        Action<bool> m_update_balance;

        public static SideGamesController Instance;

        private int m_slot3MachineVersion;
        private int m_minCasinoLevel=20;

        
        void Awake()
        {
            Instance = this;

            SlotGameManager.GameClosed += GameClosed;
            SlotGameManager.TransactionStarted += TransactionStarted;
            SlotGameManager.GameTransactionComplete += GameTransactionComplete;
            SlotGameManager.InsufficientBalance += InsufficientBalance;
            SlotGameManager.GameLoaded += GameLoaded;
            BackgroundController.Instance.OnSessionTimeout += ForceCloseSlot;
        }

        private void Start()
        {
            // Set mission min level
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_slot3MachineVersion = ldc.GetSettingAsInt("Slot3MachineVersion", 0);
                m_minCasinoLevel = ldc.GetSettingAsInt("MinCasinoLevel", 20);
            };
        }

        public bool IsMiniGameOpen()
        {
            return ChicmicMain.Instance.Slot_target.transform.childCount > 1;
        }

        private void GameLoaded()
        {
            SlotGameManager.UpdateVolume(StateController.Instance.GetSFXSetting());
            SoundsController.Instance.OnSFXVolumeChanged -= SlotGameManager.UpdateVolume;
            SoundsController.Instance.OnSFXVolumeChanged += SlotGameManager.UpdateVolume;
        }

        private void ForceCloseSlot()
        {
            GameClosed();
            ChicmicMain.Instance.DeleteSlot();
        }

        private void BalanceChanged()
        {
            // null reference exception over here sometimes:
            SlotGameManager.UpdateBalance(ModelManager.Instance.GetUser()?.GetCoins() ?? 0);
        }

        private void GameClosed()
        {
            ModelManager.Instance.GetUser().OnBalanceChange -= BalanceChanged;
            SoundsController.Instance.OnSFXVolumeChanged -= SlotGameManager.UpdateVolume;
        }

        private void TransactionStarted(long bet_amount)
        {
            ModelManager.Instance.GetUser().Add_Coins(Convert.ToInt32(-bet_amount));
            m_update_balance(false);
        }

        private void GameTransactionComplete(long NewBalance)
        {
            ModelManager.Instance.GetUser().SetCoins(Convert.ToInt32(NewBalance));
            m_update_balance(true);
        }

        private void InsufficientBalance(long bet_amount, long balance)
        {

            int delta = Convert.ToInt32(bet_amount - balance);
            IMSController.Instance.HandleInsufficientFunds(delta);
        }

        public void InitSideGames(Action<bool> UpdateBalnace)
        {
            m_update_balance = UpdateBalnace;
            ChicmicMain.Instance.Init(ModelManager.Instance.GetUser().GetID().ToString(), CardGamesCommManager.Instance.LoginToken,
                CardGamesCommManager.Instance.Rest_Client.APIServerUrl + "/",
                RemoteAssetManager.Instance.CdnBase + "sideGamesAssets/Slots3/");
        }

        private void OnLoadingProgress(float progress)
        {
            if (m_loadingAssetPopup == null)
                m_loadingAssetPopup = CardGamesPopupManager.Instance.ShowLoadingAssetPopup(null, null, null, LoadingPopupCancelled);

            m_loadingAssetPopup.UpdateProgress(progress);
        }

        private void OnLoadingDone()
        {
            PopupManager.Instance.HidePopup();
            m_loadingAssetPopup = null;
        }

        private void LoadingPopupCancelled()
        {
            ChicmicMain.Instance.OnLoadingDone -= OnLoadingDone;
            ChicmicMain.Instance.OnLoadingProgress -= OnLoadingProgress;
            ChicmicMain.Instance.DeleteSlot();
        }

        public void StartMiniGame(SideGames sideGames)
        {
            if (IsMiniGameOpen())
                return;

            ModelManager.Instance.GetUser().OnBalanceChange -= BalanceChanged;
            ModelManager.Instance.GetUser().OnBalanceChange += BalanceChanged;

            LoadingPopupCancelled();
            ChicmicMain.Instance.OnLoadingDone += OnLoadingDone;
            ChicmicMain.Instance.OnLoadingProgress += OnLoadingProgress;
            ModularClass._Inst.SelectedGameName = "";

            ChicmicMain.Instance.StartGame((int)sideGames, m_slot3MachineVersion, CUR_SLOT3_MACHINE_ID);
        }

        public void DeleteCachedSlot()
        {
            ChicmicMain.Instance.DeleteAllGames("Slots3", 1);
        }

        public int MinCasinoLevel { get => m_minCasinoLevel;}


    }
}