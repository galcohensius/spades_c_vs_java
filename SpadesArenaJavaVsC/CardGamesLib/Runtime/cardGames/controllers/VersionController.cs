﻿using common.controllers;
using System;
using cardGames.models;

namespace cardGames.controllers
{

    public class VersionController : MonoBehaviour
    {


		public static VersionController Instance;
		private void Awake()
		{
			Instance = this;
		}


        public void CheckVersion(VersionData versionData, int current_version, Action on_no_new_version)
        {
#if !UNITY_WEBGL
        LoggerController.Instance.Log ("Installed Version: " + current_version + ", Available Version: " + versionData.Version);

        if (versionData != null && current_version < versionData.Version)
        {
            CardGamesPopupManager.Instance.ShowNewVersionPopup (versionData,current_version, on_no_new_version);

        }
        else
        {
            on_no_new_version ();
        }
#else
            on_no_new_version();
#endif

        }



        public void OpenAppStore()
        {
#if UNITY_EDITOR
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.bbumgames.spadesroyale");
#elif AMAZON_APP_STORE
            Application.OpenURL("amzn://apps/android?p="+ Application.identifier);
#elif UNITY_ANDROID
            Application.OpenURL ("market://details?id="+ Application.identifier );
#elif UNITY_IOS
        Application.OpenURL("itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?type=Purple+Software&id=" + TrackingManager.Instance.Ios_app_id);
#endif
        }
    }

}