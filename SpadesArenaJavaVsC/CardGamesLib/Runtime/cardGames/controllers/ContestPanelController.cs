﻿using cardGames.models;
using cardGames.models.contests;
using common.controllers;

namespace cardGames.controllers
{
    /// <summary>
    /// This class is taking care of the contest panel
    /// </summary>
    public class ContestPanelController : MonoBehaviour
    {
        #region SerializeField Members
        // for panel and exit panel button animation
        [SerializeField] public ContestPanelView m_contest_panel_view;
        [SerializeField] public GameObject m_contest_panel_group;
        [SerializeField] public GameObject m_contest_button;
        [SerializeField] public GameObject m_contest_button_close;
        #endregion

        #region Private Members
        private bool m_during_game = true;
        #endregion

        #region Static Members
        public static ContestPanelController Instance;

        public ContestPanelView Contest_panel_view { get => m_contest_panel_view; }
        public bool During_game { get => m_during_game; }
        #endregion

        void Awake()
        {
            Instance = this;
            m_contest_panel_view.gameObject.SetActive(false);
            BackgroundController.Instance.OnSessionTimeout -= SessionTimeOut;
            BackgroundController.Instance.OnSessionTimeout += SessionTimeOut;
        }

        public void OnEndRound(Contest contest)
        {
            m_during_game = true;
            if (contest == null)
            {
                HidePanel();
                // when the contest has ended and the player starts a new game (without going through the lobby)
                m_contest_panel_view.SetData(null, true);
                return;
            }

            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);

            if (contest.Contest_players_mini_leaderboard_rank != null)
            {
                foreach (ContestPlayer contestPlayer in contest.Contest_players_mini_leaderboard_rank)
                {
                    if (contestPlayer.IsMe)
                    {
                        aContest.Current_playar_in_contest = contestPlayer;
                        aContest.StartGameRank = contestPlayer.Rank;
                        aContest.StartGamePoints = contestPlayer.Points;
                        break;
                    }
                }
                aContest.Rank_player_ids_snapshot = ContestsController.Instance.SetPlayersRanksMovement(aContest.Rank_player_ids_snapshot, contest.Contest_players_mini_leaderboard_rank);
                aContest.Top3_player_ids_snapshot = ContestsController.Instance.SetPlayersRanksMovement(aContest.Top3_player_ids_snapshot, contest.Contest_players_mini_leaderboard_top3);
            }

            m_contest_panel_view.SetData(contest, true);
            m_contest_panel_view.Started_with_2_players = contest.Contest_players_mini_leaderboard_rank?.Count == 2;

            ShowPanel();

            //  OnRoundEnded();
            m_contest_button.gameObject.SetActive(true);
            m_contest_button_close.gameObject.SetActive(true);

            //  StartCoroutine(Contest_panel_view.OpenAndClosePanel());
        }

        public void OnGameStarted(Contest contest)
        {
            m_during_game = true;
            if (contest == null)
            {
                HidePanel();
                // when the contest has ended and the player starts a new game (without going through the lobby)
                m_contest_panel_view.SetData(null, true);
                return;
            }


            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);

            if (contest.Contest_players_mini_leaderboard_rank != null)
            {
                foreach (ContestPlayer contestPlayer in contest.Contest_players_mini_leaderboard_rank)
                {
                    if (contestPlayer.IsMe)
                    {
                        aContest.Current_playar_in_contest = contestPlayer;
                        aContest.StartGameRank = contestPlayer.Rank;
                        aContest.StartGamePoints = contestPlayer.Points;
                        break;
                    }
                }
                aContest.Rank_player_ids_snapshot = ContestsController.Instance.SetPlayersRanksMovement(aContest.Rank_player_ids_snapshot, contest.Contest_players_mini_leaderboard_rank);
                aContest.Top3_player_ids_snapshot = ContestsController.Instance.SetPlayersRanksMovement(aContest.Top3_player_ids_snapshot, contest.Contest_players_mini_leaderboard_top3);
            }

            ShowPanel();

            m_contest_panel_view.SetData(contest, true);
            m_contest_panel_view.Started_with_2_players = contest.Contest_players_mini_leaderboard_rank?.Count == 2;


            m_contest_button.gameObject.SetActive(true);
            m_contest_button_close.gameObject.SetActive(true);

            StartCoroutine(Contest_panel_view.OpenAndClosePanel());
        }

        public void OnGameEnded(Contest contest)
        {
            m_during_game = false;

            if (contest == null)
            {
                HidePanel();
                return;
            }

            ActiveContest aContest = ModelManager.Instance.ContestsLists.GetActiveContest(contest.Id);
            m_contest_panel_view.SetOpenPanel(false);

            ShowPanelWitoutButtons();
            //Show_or_hide_contest_panel(false);
            foreach (ContestPlayer contestPlayer in contest.Contest_players_mini_leaderboard_rank)
            {
                if (contestPlayer.IsMe)
                    aContest.Current_playar_in_contest = contestPlayer;
            }

            aContest.Rank_player_ids_snapshot = ContestsController.Instance.SetPlayersRanksMovement(aContest.Rank_player_ids_snapshot, contest.Contest_players_mini_leaderboard_rank);
            aContest.Top3_player_ids_snapshot = ContestsController.Instance.SetPlayersRanksMovement(aContest.Top3_player_ids_snapshot, contest.Contest_players_mini_leaderboard_top3);

            if (aContest.Current_playar_in_contest != null)
                if (ContestsController.Instance.IsContestPlayerInTheMoney(contest, aContest.Current_playar_in_contest))
                    ContestsController.Instance.HandleNotificationInTheMoney(contest);
            // in the money...


            // call set data again so the contest panel view will be refreshed with the new data
            Contest_panel_view.SetData(contest, false);
        }

        public void TriggerEndGameAnimation()
        {
            m_contest_panel_view.StartEndGameAnimation();
        }

        public void OnRoundEnded()
        {
            m_contest_panel_view.SetOpenPanel(false);
        }

        public void OnPlayersTurn()
        {
            m_contest_panel_view.SetOpenPanel(false);
        }

        public void HidePanel()
        {
            m_contest_panel_view.gameObject.SetActive(false);
            m_contest_button.gameObject.SetActive(false);
        }

        public void ShowPanel()
        {
            m_contest_panel_view.gameObject.SetActive(true);
            m_contest_button.gameObject.SetActive(true);
        }

        public void ShowPanelWitoutButtons()
        {
            m_contest_panel_view.gameObject.SetActive(true);
            m_contest_button.gameObject.SetActive(false);
            m_contest_button_close.gameObject.SetActive(false);
        }

        private void SessionTimeOut()
        {
            HidePanel();
        }
    }
}