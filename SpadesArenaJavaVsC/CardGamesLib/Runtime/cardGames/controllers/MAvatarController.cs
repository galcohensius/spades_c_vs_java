﻿using cardGames.comm;
using cardGames.models;
using cardGames.views;
using common.controllers;
using System;
using System.Collections.Generic;
using static cardGames.models.MAvatarModel;
using System.Collections;

namespace cardGames.controllers
{
    public class MAvatarController : MonoBehaviour
    {
        #region Const Members
        public static string DEFAULT_AVATAR_NAME = "Guest";
        #endregion

        #region Serialized Members
        [SerializeField] TextAsset m_mappingAvatars;
        [SerializeField] private Serialized_KeyStringValueSprite_Dictionary m_headSpriteDictionary;
        [SerializeField] private Serialized_KeyStringValueSprite_Dictionary m_bustSpriteDictionary;
        [SerializeField] private Sprite m_defaultBase;
        [SerializeField] GameObject m_defaultMaleFace;
        [SerializeField] GameObject m_defaultFemaleFace;

        [SerializeField] GameObject m_defaultMaleHair;
        [SerializeField] GameObject m_defaultFemaleHair;

        [SerializeField] GameObject m_defaultMaleOutfit;
        [SerializeField] GameObject m_defaultFemaleOutfit;

        [SerializeField] GameObject m_defaultMaleBody;
        [SerializeField] GameObject m_defaultFemaleBody;
        #endregion

        #region Private Members
        Dictionary<string, AssetBundle> m_loadedBundles = new Dictionary<string, AssetBundle>();
        List<BundleData> m_bundlesData = new List<BundleData>();
        List<string> m_bundles_to_ignore = new List<string>();
        JSONNode m_avatarMap;
        MAvatarModel m_tempAvatarModel;
        #endregion

        public Action OnCategoryItemSelected;
        public Action OnAvatarShownInHierarchy;

#if UNITY_EDITOR
        [Tooltip("allow avatar edits with unobtained items (editor only)")]
        private bool loadEntireInventory = false;
#endif

        private bool m_initialized;

        private Action<bool> m_onLoadingComplete;
       


        private void Awake()
        {
            if (Instance == null)
                Instance = this;

            if (m_mappingAvatars != null)
                m_avatarMap = JSON.Parse(m_mappingAvatars.ToString());
            else
                m_avatarMap = new JSONObject();

            m_tempAvatarModel = new MAvatarModel();
        }

        public void Init(Action<bool> onLoadingComplete)
        {
            // Prevent double initialization
            if (m_initialized)
            {
                onLoadingComplete?.Invoke(true);
                return;
            }

            m_onLoadingComplete = onLoadingComplete;

            // list of bundles to ignore
            m_bundles_to_ignore.Add("1");

            // Load all local and then all remote bundles
            LoadAllLocalBundles(LoadRemoteBundles);

            m_initialized = true;

        }




        private void LoadAllLocalBundles(Action done)
        {
            ReadAllLocalBundleNames();

            // No local bundles
            if (m_bundlesData.Count == 0)
            {
                done?.Invoke();
            }

            int counter = 0;
            foreach (BundleData bundleDataItem in m_bundlesData)
            {
                Hash128 version = Hash128.Compute(bundleDataItem.Version);

                string bundleName = CreateBundleNameById(bundleDataItem.Id);

                //if version isnt cached - removed all bundle versions and download again
                if (!Caching.IsVersionCached(bundleName, version))
                {
                    LoggerController.Instance.LogFormat("Local avatars bundle {0} version {1} not found in cache. Clearing old versions...", bundleName, bundleDataItem.Version);
                    Caching.ClearAllCachedVersions(bundleName);
                }
                else
                {
                    LoggerController.Instance.LogFormat("Local avatars bundle {0} version {1} found in cache", bundleName, bundleDataItem.Version);
                }


#if UNITY_WEBGL && !UNITY_EDITOR
                // On WebGL, we need to also consider the browser's cache, so the version must also be added to the URL
                bundleName += "?v=" + version;
#endif


                // Create the base URL
                // On iOS, we need to add the file:// prefix
                // See: https://forum.unity.com/threads/unitywebrequest-getassetbundle-from-streamingassets.430689/
                string baseUrl = Application.streamingAssetsPath + "/AvatarBundles/";

                if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXEditor)
                    baseUrl = "file://" + baseUrl;

                LoggerController.Instance.LogFormat("Loading local avatars bundle {0} with version {1}", bundleName, bundleDataItem.Version);

                StartCoroutine(RemoteAssetManager.Instance.LoadAssetBundle(baseUrl, bundleName, version, (AssetBundle bundle) =>
                {
                    counter++;
                    SetBundleDataInModel(bundle, bundleDataItem.Id);

                    if (counter == m_bundlesData.Count)
                    {
                        done?.Invoke();
                    }
                }));

            }
        }

        private void ReadAllLocalBundleNames()
        {

            TextAsset bundlesJson = Resources.Load<TextAsset>("avatar_local_bundles");
            if (bundlesJson == null)
            {
                LoggerController.Instance.LogError("No local bundles file 'avatar_local_bundles.json' found in Resources.");
                return;
            }

            JSONNode root = JSON.Parse(bundlesJson.text);
            JSONArray bundlesArray = root.AsArray;

            foreach (JSONNode item in bundlesArray)
                m_bundlesData.Add(new BundleData(item["id"].Value, item["version"].Value));

        }


        private void LoadRemoteBundles()
        {
            int currNumItems = m_bundlesData.Count;
            ReadAllRemoteBundleNames((success) =>
            {
                if (success)
                {
                    if (m_bundlesData.Count == currNumItems)
                    {
                        m_onLoadingComplete?.Invoke(true);
                    }

                    int counter = 0;

                    //start from middle of the list as the first indexes are used by the local
                    for (int i = currNumItems; i < m_bundlesData.Count; i++)
                    {
                        BundleData bundleDataItem = m_bundlesData[i];

                        Hash128 version = Hash128.Compute(bundleDataItem.Version);

                        string bundleName = CreateBundleNameById(bundleDataItem.Id);

                        //if version isnt cached - removed all bundle versions and download again
                        if (!Caching.IsVersionCached(bundleName, version))
                        {
                            LoggerController.Instance.LogFormat("Remote avatars bundle {0} version {1} not found in cache. Clearing old versions...", bundleName, bundleDataItem.Version);
                            Caching.ClearAllCachedVersions(bundleName);
                        }
                        else
                        {
                            LoggerController.Instance.LogFormat("Remote avatars bundle {0} version {1} found in cache", bundleName, bundleDataItem.Version);
                        }


#if UNITY_WEBGL && !UNITY_EDITOR
                        // On WebGL, we need to also consider the browser's cache, so the version must also be added to the URL
                        bundleName += "?v=" + version;
#endif

                        StartCoroutine(RemoteAssetManager.Instance.LoadAssetBundle(RemoteAssetManager.Instance.CdnBase + "AvatarBundles/", bundleName, version, (AssetBundle bundle) =>
                        {
                            counter++;
                            SetBundleDataInModel(bundle, bundleDataItem.Id);

                            if (counter == m_bundlesData.Count - currNumItems)
                                m_onLoadingComplete?.Invoke(true);

                        }));
                    }
                }
                else
                {
                    LoggerController.Instance.LogError("Error getting remote bundles catalog file");
                }
            });
        }

        private void ReadAllRemoteBundleNames(Action<bool> done)
        {
            RemoteAssetManager.Instance.LoadJSON("AvatarBundles/avatar_bundles_list.json?c=" + DateTime.Now.Ticks, (bool success, JSONNode jsonData) =>
            {
                if (success)
                {
                    JSONArray bundlesArray = jsonData.AsArray;

                    foreach (JSONNode item in bundlesArray)
                        m_bundlesData.Add(new BundleData(item["id"].Value, item["version"].Value));
                }

                done(success);
            });
        }

        // load all assets async 
        private IEnumerator LoadPrefabsAsync(AssetBundle assetBundle,string bundleID)
        {
            AssetBundleRequest request = assetBundle.LoadAllAssetsAsync<GameObject>();
            yield return request;

            while (!request.isDone)
                yield return null;

            object tempObj = assetBundle.LoadAsset("catalogue");
            JSONArray jSONArray = (JSONArray)JSON.Parse(tempObj.ToString());
            Debug.Log("CATALOUGE: " + jSONArray.ToString());

            for (int i = 0; i < jSONArray.Count; i++)
            {
                MavatarItem mavatarItem = new MavatarItem(jSONArray[i]["ID"].Value);
                mavatarItem.StoreOrder = jSONArray[i]["StoreOrder"].AsInt;
                mavatarItem.MinLevel = jSONArray[i]["MinLevel"].AsInt;
#if UNITY_EDITOR
                mavatarItem.InventoryID = loadEntireInventory ? "" : jSONArray[i]["InventoryID"].Value;
#else                                                                              
                mavatarItem.InventoryID = jSONArray[i]["InventoryID"].Value;
#endif
                mavatarItem.Gender = GetGenderTypeByLetter(jSONArray[i]["Gender"].Value);
                mavatarItem.ItemType = GetItemTypeByLetter(jSONArray[i]["Type"].Value);
                mavatarItem.Animated = jSONArray[i]["Animated"].AsBool;
                mavatarItem.Featured = jSONArray[i]["Featured"].AsBool;
                mavatarItem.BotRandomPercentage = jSONArray[i]["BotRandomPercentage"].AsFloat;
                mavatarItem.BundleName = bundleID;
                mavatarItem.PrefabName = jSONArray[i]["Prefab"].Value;
                ModelManager.Instance.AvatarData.AddItemToModel(mavatarItem);
            }

            yield return null;
        }


        private void SetBundleDataInModel(AssetBundle bundle, string bundleID)
        {
            m_loadedBundles.Add(bundleID, bundle);

            // load all assets async 
            StartCoroutine(LoadPrefabsAsync(bundle, bundleID));
        }

       

        /// <summary>
        /// This funciton generates the avatar for the bots by parameters like nickname, gender and fbId
        /// </summary>
        public MAvatarModel GenerateMavatarForBot(string nickname, MAvatarModel.GenderType gender, string fbId, int botLevel)
        {
            MAvatarModel mAvatarModel;
            if (string.IsNullOrEmpty(fbId) || string.IsNullOrWhiteSpace(fbId))
                mAvatarModel = new MAvatarModel(MAvatarModel.MAvatarMode.Avatar);
            else
                mAvatarModel = new MAvatarModel(MAvatarModel.MAvatarMode.Facebook, fbId);

            int headShapeIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(MAvatarModel.HeadShapeType)).Length);
            int skinColorIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(MAvatarModel.SkinColorType)).Length);
            mAvatarModel.NickName = nickname;
            mAvatarModel.FB_ID = fbId;

            mAvatarModel.HeadShape = (MAvatarModel.HeadShapeType)headShapeIndex;
            mAvatarModel.SkinColor = (MAvatarModel.SkinColorType)skinColorIndex;
            mAvatarModel.Gender = gender;

            // hair
            mAvatarModel.Hair = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Hair,true);

            // face
            mAvatarModel.Face = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Face,true);

            // accessory
            int factor = UnityEngine.Random.Range(0, 10);
            if (factor < 2)
                mAvatarModel.Accessory = new MavatarItem("");
            else
                mAvatarModel.Accessory = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Accessory,true);

            // outfit
            mAvatarModel.Outfit = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Outfit,true);

            // trophy
            mAvatarModel.Trophy = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Trophy,true);

            // body
            mAvatarModel.Body = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Body,true);

            // costume
            factor = UnityEngine.Random.Range(0, 10);
            if (factor < 3)
            {
                mAvatarModel.Costume = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Costume,true);
            }
            else
            {
                mAvatarModel.Costume = new MavatarItem("");
            }

            return mAvatarModel;
        }

        public MAvatarModel CreateRandomAvatarModel(MAvatarModel.GenderType gender, string facebookUserId = "")
        {
            MAvatarModel mAvatarModel;
            if (string.IsNullOrEmpty(facebookUserId))
                mAvatarModel = new MAvatarModel(MAvatarModel.MAvatarMode.Avatar, facebookUserId);
            else
                mAvatarModel = new MAvatarModel(MAvatarModel.MAvatarMode.Facebook, facebookUserId);

            mAvatarModel.IsAutoGenerated = true;
            int headShapeIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(MAvatarModel.HeadShapeType)).Length);
            int skinColorIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(MAvatarModel.SkinColorType)).Length);

            mAvatarModel.HeadShape = (MAvatarModel.HeadShapeType)headShapeIndex;
            mAvatarModel.SkinColor = (MAvatarModel.SkinColorType)skinColorIndex;
            mAvatarModel.Gender = gender;

            mAvatarModel.Hair = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Hair,false);
            mAvatarModel.Face = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Face, false);
            mAvatarModel.Accessory = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Accessory, false);
            mAvatarModel.Outfit = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Outfit, false);
            mAvatarModel.Body = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Body, false);

            // no costume or trophies on random 
            mAvatarModel.Costume = new MavatarItem("");
            mAvatarModel.Trophy = new MavatarItem("");

            /* - old custom random factor, removed due to request for no costume on random
             int costumeFactor = UnityEngine.Random.Range(0, 11);
               // should get a parameter if costume should be included in this random function
           if (costumeFactor >= 7)
                mAvatarModel.Costume = ModelManager.Instance.AvatarData.GetRandomItem(mAvatarModel.Gender, MAvatarModel.ItemType.Costume);
            else
                mAvatarModel.Costume = new MavatarItem(""); // no costume
            */

            return mAvatarModel;
        }

        public GameObject GetDefaultPrefab(MAvatarModel.ItemType type, MAvatarModel.GenderType gender, Transform parent)
        {
            Debug.Log("GetDefaultPrefab");
            if (type == MAvatarModel.ItemType.Face)
            {
                if (gender == MAvatarModel.GenderType.Male)
                    return Instantiate(m_defaultMaleFace, parent);
                else
                    return Instantiate(m_defaultFemaleFace, parent);
            }

            else if (type == MAvatarModel.ItemType.Hair)
            {
                if (gender == MAvatarModel.GenderType.Male)
                    return Instantiate(m_defaultMaleHair, parent);
                else
                    return Instantiate(m_defaultFemaleHair, parent);
            }

            else if (type == MAvatarModel.ItemType.Outfit)
            {
                if (gender == MAvatarModel.GenderType.Male)
                    return Instantiate(m_defaultMaleOutfit, parent);
                else
                    return Instantiate(m_defaultFemaleOutfit, parent);
            }

            else if (type == MAvatarModel.ItemType.Body)
            {
                if (gender == MAvatarModel.GenderType.Male)
                    return Instantiate(m_defaultMaleBody, parent);
                else
                    return Instantiate(m_defaultFemaleBody, parent);
            }

            // accessory, trophy
            return null;
        }

        private Dictionary<string, object> assetDictionary = new Dictionary<string, object>();

        public GameObject GetPrefabByID(string id, Transform parent)
        {
            Debug.Log("GetPrefabByID");

            MavatarItem PrefabsmavatarItem = ModelManager.Instance.AvatarData.GetItemByID(id);

            if (PrefabsmavatarItem == null)
            {
                return null;
            }
            else
            {
                string bundle_id = id.Split('_')[0];
                AssetBundle currBundle;

                if(assetDictionary.ContainsKey(PrefabsmavatarItem.PrefabName) )
                {
                    GameObject prefab = Instantiate(assetDictionary[PrefabsmavatarItem.PrefabName] as GameObject, parent.transform);
                    return prefab;
                }
                else if (m_loadedBundles.TryGetValue(bundle_id, out currBundle))
                {
                    object tempObj = currBundle.LoadAsset(PrefabsmavatarItem.PrefabName);
                    assetDictionary.Add(PrefabsmavatarItem.PrefabName, tempObj);
                    
                    GameObject prefab = Instantiate(tempObj as GameObject, parent.transform);
                    return prefab;
                }
                else
                {
                    LoggerController.Instance.Log("Can't find prefab with id: " + id);
                    return null;
                }
            }
        }

        public JSONNode SerializeAvatarData(MAvatarModel avatarData)
        {
            JSONObject result = new JSONObject();
            result["n"] = avatarData.NickName;
            result["me"] = (int)avatarData.Mode;

            if (avatarData.Mode == MAvatarModel.MAvatarMode.Facebook || avatarData.Mode == MAvatarModel.MAvatarMode.FacebookAndAvatar)
                result["fID"] = avatarData.FB_ID;

            if (avatarData.Mode == MAvatarMode.AvatarFlat)
            {
                result["fAi"] = avatarData.FlatAvatarImage;
                return result;
            }

            string trophy = avatarData.Trophy?.ID ?? "";
            string accessory = avatarData.Accessory?.ID ?? "";
            string hair = avatarData.Hair?.ID ?? "";
            string outfit = avatarData.Outfit?.ID ?? "";
            string body = avatarData.Body?.ID ?? "";
            string face = avatarData.Face?.ID ?? "";
            string costume = avatarData.Costume?.ID ?? "";

            List<object> tokens = new List<object>();
            tokens.Add((int)avatarData.Gender);
            tokens.Add((int)avatarData.HeadShape);
            tokens.Add((int)avatarData.SkinColor);
            tokens.Add(body);
            tokens.Add(hair);
            tokens.Add(face);
            tokens.Add(outfit);
            tokens.Add(accessory);
            tokens.Add(trophy);
            tokens.Add(costume);
            string.Join("|", tokens);

            result["d"] = string.Join("|", tokens);

            return result;
        }

        public MAvatarModel DeserializeAvatarData(JSONNode avatarData)
        {
            if (avatarData == null || avatarData.AsObject == null || avatarData.AsObject.Count == 0)
            {
                // No data - create a random avatar
                return CreateRandomAvatarModel(MAvatarModel.GenderType.Male);
            }

            string nickname = avatarData["n"];
            MAvatarMode mode = (MAvatarMode)avatarData["me"].AsInt;
            string fb_id = avatarData["fID"].Value;

            bool wasTranslated = false;

            //check if flat avatar format
            if (mode==MAvatarMode.AvatarFlat || avatarData.HasChild("fAi"))
            {
                // Flat avatar - create a random avatar
                MAvatarModel result = CreateRandomAvatarModel(MAvatarModel.GenderType.Male);
                result.NickName = nickname;
                return result;
            }
            else
            {
                // Check if old avatar format - gender key is only on old avatars
                if (avatarData.HasChild("g") || !avatarData.HasChild("d"))
                {
                    avatarData = TranslateSerializedFromOldToNew(avatarData);
                    wasTranslated = true;
                }
            }
 

            string[] splitData = avatarData["d"].Value.Split('|');

            MavatarItem body = null;
            MavatarItem hair = null;
            MavatarItem face = null;
            MavatarItem outfit = null;
            MavatarItem accessory = null;
            MavatarItem trophy = null;
            MavatarItem costume = null;

            //avatarMode
            // Gender
            MAvatarModel.GenderType gender = (MAvatarModel.GenderType)Convert.ToInt32(splitData[0]);
            // HeadShape
            MAvatarModel.HeadShapeType headShape = (MAvatarModel.HeadShapeType)Convert.ToInt32(splitData[1]);
            // SkinColor
            MAvatarModel.SkinColorType skinColor = (MAvatarModel.SkinColorType)Convert.ToInt32(splitData[2]);

            // Body
            body = new MavatarItem(splitData[3]);
            body.ItemType = ItemType.Body;

            // Hair
            hair = new MavatarItem(splitData[4]);
            hair.ItemType = ItemType.Hair;

            // Face
            face = new MavatarItem(splitData[5]);
            face.ItemType = ItemType.Face;

            // Outfit
            outfit = new MavatarItem(splitData[6]);
            outfit.ItemType = ItemType.Outfit;

            // Accessory
            accessory = new MavatarItem(splitData[7]);
            accessory.ItemType = ItemType.Accessory;

            // Trophy
            trophy = new MavatarItem(splitData[8]);
            trophy.ItemType = ItemType.Trophy;

            // Costume
            costume = new MavatarItem(splitData[9]);
            costume.ItemType = ItemType.Costume;

            MAvatarModel mAvatarModel = new MAvatarModel();
            mAvatarModel.NickName = nickname;
            mAvatarModel.Mode = mode;
            mAvatarModel.Gender = gender;
            mAvatarModel.HeadShape = headShape;
            mAvatarModel.SkinColor = skinColor;
            mAvatarModel.Hair = new MavatarItem(hair);
            mAvatarModel.Face = new MavatarItem(face);
            mAvatarModel.Outfit = new MavatarItem(outfit);
            mAvatarModel.Accessory = new MavatarItem(accessory);
            mAvatarModel.Trophy = new MavatarItem(trophy);
            mAvatarModel.Costume = new MavatarItem(costume);
            mAvatarModel.Body = new MavatarItem(body);
            mAvatarModel.FB_ID = fb_id;
            mAvatarModel.IsAutoGenerated = wasTranslated;

            return mAvatarModel;
        }

        private JSONNode TranslateFlatToMAvatar(JSONNode avatarData)
        {
            return m_avatarMap[avatarData["fAi"].Value];
        }

        private JSONNode TranslateSerializedFromOldToNew(JSONNode avatarData)
        {
            // translate the old into the new version
            MAvatarModel.GenderType gender = (MAvatarModel.GenderType)avatarData["g"].AsInt;
            MAvatarModel.MAvatarMode mode = (MAvatarModel.MAvatarMode)avatarData["me"].AsInt;

            string oldHair = MakeId("H", avatarData["h"], gender);
            string oldFace = MakeId("F", avatarData["f"], gender);
            string oldOutfit = MakeId("O", avatarData["o"], gender);
            string oldAccessory = MakeId("A", avatarData["a"], GenderType.Unisex); // Accessories are always unisex
            int oldSkinColor = int.Parse((MakeId("SC", avatarData["sC"], gender)));
            int oldHeadShape = int.Parse((MakeId("HS", avatarData["hS"], gender)));

            string newHair = m_avatarMap[oldHair];
            string newFace = m_avatarMap[oldFace];
            string newOutfit = m_avatarMap[oldOutfit];
            string newAccessory = m_avatarMap[oldAccessory];
            int newSkinColor = oldSkinColor;
            int newHeadShape = oldHeadShape;

            // building the mavatar model from the mavatar items
            MAvatarModel mavatarModel = new MAvatarModel();
            mavatarModel.Hair = new MavatarItem(newHair ?? "");
            mavatarModel.Face = new MavatarItem(newFace ?? "");
            mavatarModel.Outfit = new MavatarItem(newOutfit ?? "");
            mavatarModel.Accessory = new MavatarItem(newAccessory ?? "");
            mavatarModel.SkinColor = (MAvatarModel.SkinColorType)(newSkinColor);
            mavatarModel.HeadShape = (MAvatarModel.HeadShapeType)(newHeadShape);
            mavatarModel.Mode = mode;
            mavatarModel.FB_ID = avatarData["fID"].Value;
            mavatarModel.Gender = gender;
            mavatarModel.NickName = avatarData["n"].Value;

            mavatarModel.Body = new MavatarItem("");

            return SerializeAvatarData(mavatarModel);
        }

        private string MakeId(string typeName, JSONNode item, MAvatarModel.GenderType gender)
        {
            if (typeName == "SC" || typeName == "HS")
                return item.Value == "" ? "0" : item.Value;

            string result = typeName;

            switch (gender)
            {
                case MAvatarModel.GenderType.Male:
                    result += "_M";
                    break;

                case MAvatarModel.GenderType.Female:
                    result += "_F";
                    break;

                case MAvatarModel.GenderType.Unisex:
                    result += "_U";
                    break;
            }

            result += "_" + item["ID"].Value;

            return result;
        }

        /// <summary>
        /// Invoked when new items are purchased/received (from a Goodvirtz)
        /// </summary>
        public void NewInventoryItemsArrived(List<string> inventoryIds)
        {
            foreach (string inventoryId in inventoryIds)
            {
                List<MavatarItem> items = null;
                if (ModelManager.Instance.AvatarData.InventoryItems.TryGetValue(inventoryId, out items))
                {
                    foreach (MavatarItem item in items)
                    {
                        CardGamesStateController.Instance.AddNewAvatarItemId(item.ID, item.ItemType);
                    }
                }
            }
        }

        /// <summary>
        /// Invoked when a user is leveled up to mark new items as new
        /// </summary>
        public void UserLeveledUp(int oldLevel, int newLevel)
        {
            foreach (MavatarItem item in ModelManager.Instance.AvatarData.AllAvatarData.Values)
            {
                // check if this item should be ignored from the "new flag"
                if (!IsItemInIgnoredBundles(item))
                {
                    int minLevel = item.MinLevel;

                    // Get all MavatarItems that has a level between old and new (,]
                    if (minLevel > oldLevel && minLevel <= newLevel)
                    {
                        if (!string.IsNullOrEmpty(item.InventoryID))
                        {
                            CardGamesStateController.Instance.AddNewAvatarItemId(item.ID, item.ItemType);
                        }
                        else
                        {
                            // This is a possible bought item - check if the user already bought it
                            if (ModelManager.Instance.Inventory.HasItem(item.InventoryID))
                            {
                                CardGamesStateController.Instance.AddNewAvatarItemId(item.ID, item.ItemType);
                            }
                        }
                    }
                }
            }
        }

        private bool IsItemInIgnoredBundles(MavatarItem item)
        {
            if (m_bundles_to_ignore.Contains(item.BundleName))
                return true;

            return false;
        }

        public HashSet<string> GetNewAvatarItemIds()
        {
            return CardGamesStateController.Instance.GetNewAvatarItemIds();
        }

        public HashSet<string> GetNewAvatarItemIdsForCategory(MAvatarModel.ItemType category)
        {
            HashSet<string> itemIds = CardGamesStateController.Instance.GetNewAvatarItemIdsWithPrefix();
            HashSet<string> result = new HashSet<string>();
            foreach (string idWithPrefix in itemIds)
            {
                string id = idWithPrefix.Substring(1); // id without prefix
                string prefix = idWithPrefix.Substring(0, 1); // get only the prefix
                if (GetItemTypeByLetter(prefix) == category)
                    result.Add(id);
            }

            return result;
        }

        public List<GameObject> GetListOfItemsByInventoryId(List<MavatarItem> items, GameObject parent)
        {
            if (items.Count == 0)
                return null;
            List<GameObject> result = new List<GameObject>();
            GameObject go = GetPrefabByID(items[0].ID, transform);
            result.Add(go);
            return result;
        }

        private MAvatarModel.GenderType GetGenderTypeByLetter(string letter)
        {
            if (letter == "M")
                return MAvatarModel.GenderType.Male;
            else if (letter == "F")
                return MAvatarModel.GenderType.Female;
            else return MAvatarModel.GenderType.Unisex;
        }

        private MAvatarModel.ItemType GetItemTypeByLetter(string letter)
        {
            if (letter == "H")
                return MAvatarModel.ItemType.Hair;
            else if (letter == "A")
                return MAvatarModel.ItemType.Accessory;
            else if (letter == "O")
                return MAvatarModel.ItemType.Outfit;
            else if (letter == "F")
                return MAvatarModel.ItemType.Face;
            else if (letter == "B")
                return MAvatarModel.ItemType.Trophy;
            else if (letter == "D")
                return MAvatarModel.ItemType.Body;
            else if (letter == "C")
                return MAvatarModel.ItemType.Costume;
            return MAvatarModel.ItemType.Accessory;
        }

        public Sprite GetDefaultBase()
        {
            return m_defaultBase;
        }

        //this is used to get the static images that are not from assetbundle - they are linked in the scene - they are for head and bust only
        public Sprite GetStaticSprite(bool head, string id)
        {
            if (head)
            {
                Sprite staticAsset = null;
                m_headSpriteDictionary.TryGetValue(id, out staticAsset);
                return staticAsset;
            }
            //Bust
            else
            {
                Sprite staticAsset = null;
                m_bustSpriteDictionary.TryGetValue(id, out staticAsset);
                return staticAsset;
            }
        }

        public void CopyMavatarModel(MAvatarModel source, MAvatarModel dest)
        {
            // Mode
            dest.Mode = source.Mode;

            // Gender
            dest.Gender = source.Gender;

            // HeadShape
            dest.HeadShape = source.HeadShape;

            // SkinColor
            dest.SkinColor = source.SkinColor;

            // FB_ID
            dest.FB_ID = source.FB_ID;

            // Accessory
            dest.Accessory = new MavatarItem(source.Accessory);

            // Face
            dest.Face = new MavatarItem(source.Face);

            // Body
            dest.Body = new MavatarItem(source.Body);

            // Trophy
            dest.Trophy = new MavatarItem(source.Trophy);

            // Hair
            dest.Hair = new MavatarItem(source.Hair);

            // Outfit
            dest.Outfit = new MavatarItem(source.Outfit);

            // Costume 
            dest.Costume = new MavatarItem(source.Costume);

            // Nickname
            dest.NickName = source.NickName;

            // For flats
            dest.FlatAvatarImage = source.FlatAvatarImage;
        }

        public bool IsEqual(MAvatarModel mavatar1, MAvatarModel mavatar2)
        {
            if (mavatar1 == null || mavatar2 == null)
                return true;

            // Nickname
            if (mavatar2.NickName != mavatar1.NickName)
                return false;

            // Mode
            if (mavatar2.Mode != mavatar1.Mode)
                return false;

            // Gender
            if (mavatar2.Gender != mavatar1.Gender)
                return false;

            // HeadShape
            if (mavatar2.HeadShape != mavatar1.HeadShape)
                return false;

            // SkinColor
            if (mavatar2.SkinColor != mavatar1.SkinColor)
                return false;

            // FB_ID
            if (mavatar2.FB_ID != mavatar1.FB_ID)
                return false;

            // Accessory
            if (mavatar2.Accessory.ID != mavatar1.Accessory.ID)
                return false;

            // Face
            if (mavatar2.Face.ID != mavatar1.Face.ID)
                return false;

            // Body
            if (mavatar2.Body.ID != mavatar1.Body.ID)
                return false;

            // Trophy
            if (mavatar2.Trophy.ID != mavatar1.Trophy.ID)
                return false;

            // Hair
            if (mavatar2.Hair.ID != mavatar1.Hair.ID)
                return false;

            // Outfit
            if (mavatar2.Outfit.ID != mavatar1.Outfit.ID)
                return false;

            // Costume 
            if (mavatar2.Costume.ID != mavatar1.Costume.ID)
                return false;

            return true;
        }

        public void SelectSkinTone(MAvatarModel.SkinColorType skinColorType)
        {
            m_tempAvatarModel.SkinColor = skinColorType;
            m_tempAvatarModel.FireOnModelChangedEvent(false);
        }

        public void SelectGender(bool isMale)
        {
            // don't reset items when the gender doesn't change
            if (m_tempAvatarModel.Gender == MAvatarModel.GenderType.Male && isMale
                || m_tempAvatarModel.Gender == MAvatarModel.GenderType.Female && !isMale)
                return;

            if (isMale)
            {
                m_tempAvatarModel.Gender = MAvatarModel.GenderType.Male;
                m_tempAvatarModel.Body = ModelManager.Instance.AvatarData.Body_Male[0];
                m_tempAvatarModel.Face = ModelManager.Instance.AvatarData.Face_Male[0];
                m_tempAvatarModel.Hair = ModelManager.Instance.AvatarData.Hair_Male[0];
                m_tempAvatarModel.Outfit = ModelManager.Instance.AvatarData.Outfit_Male[0];
            }
            else
            {
                m_tempAvatarModel.Gender = MAvatarModel.GenderType.Female;
                m_tempAvatarModel.Body = ModelManager.Instance.AvatarData.Body_Female[0];
                m_tempAvatarModel.Face = ModelManager.Instance.AvatarData.Face_Female[0];
                m_tempAvatarModel.Hair = ModelManager.Instance.AvatarData.Hair_Female[0];
                m_tempAvatarModel.Outfit = ModelManager.Instance.AvatarData.Outfit_Female[0];
            }
            m_tempAvatarModel.Accessory.ID = "";

            m_tempAvatarModel.FireOnModelChangedEvent(false);
        }

        public void SelectItem(MavatarItem avatarItem, ACCategorySection.SectionType sectionType, MAvatarModel.HeadShapeType headShape)
        {
            if (m_tempAvatarModel.Mode == MAvatarModel.MAvatarMode.Facebook)
                m_tempAvatarModel.Mode = MAvatarModel.MAvatarMode.FacebookAndAvatar;

            switch (sectionType)
            {
                case ACCategorySection.SectionType.ShapeSection:
                    m_tempAvatarModel.HeadShape = headShape;
                    m_tempAvatarModel.Costume.ID = "";
                    break;

                case ACCategorySection.SectionType.FaceSection:
                    m_tempAvatarModel.Face = new MavatarItem(avatarItem);
                    m_tempAvatarModel.Costume.ID = "";
                    break;
            }
            m_tempAvatarModel.FireOnModelChangedEvent(false);
        }

        public void SelectItem(MavatarItem avatarItem)
        {
            if (m_tempAvatarModel.Mode == MAvatarModel.MAvatarMode.Facebook)
                m_tempAvatarModel.Mode = MAvatarModel.MAvatarMode.FacebookAndAvatar;

            switch (avatarItem.ItemType)
            {
                case ItemType.Face:
                    m_tempAvatarModel.Face = new MavatarItem(avatarItem);
                    m_tempAvatarModel.Costume.ID = "";
                    break;

                case ItemType.Costume:
                    m_tempAvatarModel.Costume = new MavatarItem(avatarItem);
                    break;

                case ItemType.Hair:
                    m_tempAvatarModel.Hair = new MavatarItem(avatarItem);
                    m_tempAvatarModel.Costume.ID = "";
                    break;

                case ItemType.Outfit:
                    m_tempAvatarModel.Outfit = new MavatarItem(avatarItem);
                    m_tempAvatarModel.Costume.ID = "";
                    break;

                case ItemType.Trophy:
                    m_tempAvatarModel.Trophy = new MavatarItem(avatarItem);
                    break;

                case ItemType.Accessory:
                    m_tempAvatarModel.Accessory = new MavatarItem(avatarItem);
                    m_tempAvatarModel.Costume.ID = "";
                    break;
            }
            m_tempAvatarModel.FireOnModelChangedEvent();
        }

        public void UpdateAvatarInfo()
        {
            // TODO: Remove second parameter (be carefull with rummy)
            CardGamesCommManager.Instance.UpdateUserAvatar(ModelManager.Instance.GetUser().GetMAvatar(), ModelManager.Instance.GetUser().GetMAvatar().NickName);
        }

        private string CreateBundleNameById(string bundleId)
        {
            int platformCode = StateController.Instance.GetPlatformCode();
            if (platformCode == 4)
                platformCode = 2; // Amazon is the same as Android for this matter
            return $"{platformCode}_AvatarItems_{bundleId}.bundle";
        }

        public MAvatarModel TempAvatarModel { get => m_tempAvatarModel; set => m_tempAvatarModel = value; }

        public class BundleData
        {
            string m_id;
            string m_version;

            public string Id { get => m_id; set => m_id = value; }
            public string Version { get => m_version; set => m_version = value; }

            public BundleData(string id, string version)
            {
                m_id = id;
                m_version = version;
            }
        }
        public static MAvatarController Instance { get; private set; }
    }
}