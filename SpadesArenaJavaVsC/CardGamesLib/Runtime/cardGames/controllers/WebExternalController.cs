﻿using cardGames.models;
using cardGames.comm;
using common.facebook;

namespace cardGames.controllers
{

    /// <summary>
    /// A controller for invoking methods from Javascript on WebGL platform
    /// </summary>
	public class WebExternalController : MonoBehaviour
    {


		public static WebExternalController Instance;

		private void Awake()
		{
            if (Instance == null)
			    Instance = this;
		}


        public void OpenSupportPage()
        {
            User user = ModelManager.Instance.GetUser();

            int? userId = user?.GetID();
            string lang = "en";
            int platformId = CardGamesStateController.Instance.GetPlatformCode();

			string email = FacebookController.Instance.UserFacebookData?.Email;

            string fbCanvasUrl = CardGamesCommManager.Instance.FbCanvasUrl;

            string url = string.Format("{0}/support.html?did=&uid={1}&email={2}&plt={3}&lang={4}",
                                       fbCanvasUrl, userId, email, platformId, lang);

			OpenUrl (url);

        }

        public void OpenHelpPage(string rootHelpPageUrl)
        {
            User user = ModelManager.Instance.GetUser();

            int? userId = user?.GetID();
            string lang = "en";
            int platformId = CardGamesStateController.Instance.GetPlatformCode();

            string email = FacebookController.Instance.UserFacebookData?.Email;


            string url = string.Format("{0}?did=&uid={1}&email={2}&plt={3}&lang={4}",
                                       rootHelpPageUrl, userId, email, platformId, lang);

            OpenUrl(url);

        }

        public void OpenUrl(string url) {
			#if UNITY_WEBGL && !UNITY_EDITOR
			openBrowserWindow (url);
			#else
			Application.OpenURL(url);
			#endif
		}

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void openBrowserWindow(string url);
#endif
    }

}