﻿using common.facebook;
using System.Collections.Generic;
using common.controllers.social.inviter;
using cardGames.models;

namespace cardGames.controllers.Inviter
{
	public class FacebookInviter:InviterBase
	{
		public override string ChannelName {
			get {
				return "facebook";
			}
		}

		public override void OnClick ()
		{
			base.OnClick ();
            //TODO: find a way to pass the real user ID.
            //FacebookController.Instance.SendInviteFriends ("", (List<string> results) =>
            FacebookController.Instance.SendInviteFriends (ModelManager.Instance.GetUser().GetID(), SocialController.Instance.InviteBody, (List<string> results) => {
			    
			});

		}

		protected override bool IsAppInstalled ()
		{
            return FacebookController.Instance.IsLoggedIn();
        }
	}
}

