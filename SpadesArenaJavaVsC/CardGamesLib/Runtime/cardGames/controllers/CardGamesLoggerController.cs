﻿using cardGames.comm;
using common.controllers;

namespace cardGames.controllers
{
    public class CardGamesLoggerController : LoggerController
    {
      
        public override void SendLogDataToServer(string logType, string send_data_reason)
        {

            AddToLog(send_data_reason, LogLevel.Error, Module.General);

            if (m_clientLogTypesToSend.Contains(logType))
            {
                CardGamesCommManager.Instance.SendLoggingData(logType, GetListLog());
            }
        }
    }
}