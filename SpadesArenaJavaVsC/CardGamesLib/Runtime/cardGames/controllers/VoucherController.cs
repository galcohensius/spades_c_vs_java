﻿using cardGames.models;
using common.controllers;
using common.models;
using common.utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.controllers
{
    public class VoucherController : MonoBehaviour
    {
        #region Public Members
        public static VoucherController Instance;
        #endregion
        
        #region Private Members
        private Coroutine m_earliestExpireTimerCoroutine;
        [SerializeField] GameObject m_voucherViewPrefab; 
        #endregion

        public Action OnVouchersAdded;


        public virtual void Awake()
        {
            Instance = this;
            BackgroundController.Instance.OnBackFromBackground += OnBackFromBackground;
        }

        void OnBackFromBackground(bool sessionTimeout, TimeSpan backgroundTime)
        {
            FindEarliestVoucherExpiration();
            LoggerController.Instance.Log("VoucherController OnBackFromBackground");
        }

        public void NewVouchersReceived(List<VouchersGroup> vouchersGroup, bool clearOldVouchers = false)
        {
            if (clearOldVouchers)
                ModelManager.Instance.Vouchers.Clear();

            // Merge the new vouchers with the existing vouchers groups according to value and table filter
            foreach (VouchersGroup newVoucherGroup in vouchersGroup)
            {
                bool existingFound = false;

                foreach (VouchersGroup voucher in ModelManager.Instance.Vouchers)
                {
                    if (newVoucherGroup.VouchersValue == voucher.VouchersValue)
                    {
                        if (newVoucherGroup.TableFilterData == voucher.TableFilterData)
                        {
                            voucher.AddVouchersGroup(newVoucherGroup);
                            existingFound = true;
                        }
                    }
                }
                // Does not exist yet
                if (!existingFound)
                    ModelManager.Instance.Vouchers.Add(newVoucherGroup);

                //CardGamesLoggerController.Instance.Log("New voucher received: " + newVoucherGroup);
            }

            FindEarliestVoucherExpiration();

            NotificationController.Instance.ScheduleVoucherExpiredNotification();

            OnVouchersAdded?.Invoke();
        }

        public void UseVoucher(string voucherId)
        {
            // Iterate over the vouchers and search for a voucher to remove
            foreach (VouchersGroup voucher in ModelManager.Instance.Vouchers)
            {
                if (voucher.RemoveVoucherById(voucherId) != null)
                    return;
            }
        }

        private IEnumerator CheckVoucherExpiration(DateTime expireTime)
        {
            long secLeft = DateUtils.TotalSeconds(expireTime);
            yield return new WaitForSeconds(secLeft);
            RemoveExpiredVouchers();
            FindEarliestVoucherExpiration();
        }

        private void RemoveExpiredVouchers()
        {
            List<VouchersGroup> vouchersGroup = ModelManager.Instance.Vouchers;

            foreach (VouchersGroup vouchers in vouchersGroup)
                vouchers.RemoveExpiredVouchers();
        }

        private void FindEarliestVoucherExpiration()
        {
            List<VouchersGroup> vouchersGroup = ModelManager.Instance.Vouchers;
            DateTime earliestExpireTime = DateTime.Now.AddYears(100);
            foreach (VouchersGroup vouchers in vouchersGroup)
            {
                if (vouchers.Count > 0 && earliestExpireTime > vouchers.EarliestExpireTime)
                    earliestExpireTime = vouchers.EarliestExpireTime;
            }

            earliestExpireTime = earliestExpireTime.AddSeconds(1); // Make sure the voucher is really expired

            // Start coroutine
            if (m_earliestExpireTimerCoroutine != null)
                StopCoroutine(m_earliestExpireTimerCoroutine);
            m_earliestExpireTimerCoroutine = StartCoroutine(CheckVoucherExpiration(earliestExpireTime));
        }

        public GameObject VoucherViewPrefab { get => m_voucherViewPrefab; set => m_voucherViewPrefab = value; }
    }
}



