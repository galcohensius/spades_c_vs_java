﻿using System;
using System.Text.RegularExpressions;
using cardGames.models;
using common.controllers;

namespace cardGames.controllers
{
    public class FlatAvatarController : MonoBehaviour
    {
        [SerializeField]
        private Sprite[] femaleAvatars;

        [SerializeField]
        private Sprite[] maleAvatars;

        [SerializeField]
        private Sprite default_FB_image;

        public Sprite CurrentAvatar { get; private set; }

        public Action<string> OnAvatarChanged;

        public static FlatAvatarController Instance;



        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

       

        public Sprite GetAvatar(MAvatarModel avatarModel)
        {

            string flatAvatarImage = avatarModel.FlatAvatarImage;
            if (string.IsNullOrEmpty(flatAvatarImage))
            {
                LoggerController.Instance.LogWarning("empty flat avatar image. Using default");
                flatAvatarImage = "Male_1";
                avatarModel.FlatAvatarImage = flatAvatarImage;
            }

            if (flatAvatarImage.Contains("ale"))
            {
                string numberString = Regex.Match(flatAvatarImage, @"\d+").Value;
                int index = System.Convert.ToInt32(numberString) - 1;

                if (flatAvatarImage.Contains("Female"))
                {
                    if (index< 0 || index >= femaleAvatars.Length)
                    {
                        LoggerController.Instance.LogError("out of bounds index for female avatars: " + index);
                        return null;
                    }
                    return femaleAvatars[index];
                }
                else if (flatAvatarImage.Contains("Male"))
                {
                    if (index< 0 || index >= maleAvatars.Length)
                    {
                        LoggerController.Instance.LogError("out of bounds index for male avatars: " + index);
                        return null;
                    }
                    return maleAvatars[index];
                }
                else
                {
                    LoggerController.Instance.LogError("WTF nigger?! you tried to parse bad preset string.");
                    return null;
                }
            }
            else
            {
                LoggerController.Instance.LogError("WTF nigger?! you tried to parse bad preset string.");
                return null;
            }

        }

        public Sprite GetRandomAvatar()
        {
            int gender = UnityEngine.Random.Range(0, 2);
            if (gender == 0)
                return femaleAvatars[UnityEngine.Random.Range(0, femaleAvatars.Length)];
            else //(gender == 1)
                return maleAvatars[UnityEngine.Random.Range(0, maleAvatars.Length)];

        }

        //used for guest user
        public MAvatarModel GetRandomAvatarModel()
        {
            return new MAvatarModel(MAvatarController.DEFAULT_AVATAR_NAME, MAvatarModel.MAvatarMode.AvatarFlat, GetRandomAvatar().name);
        }


    }
}