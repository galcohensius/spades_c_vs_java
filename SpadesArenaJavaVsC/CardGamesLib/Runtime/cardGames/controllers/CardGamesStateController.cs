﻿using cardGames.models;
using cardGames.models.contests;
using common.controllers;
using common.models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace cardGames.controllers
{
    public abstract class CardGamesStateController : StateController
    {
        public static new CardGamesStateController Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;
                LeaderboardsModel.OnJoinedRanksData += Instance.StoreLeaderboardRanks;
            }
        }

        public void AddNewAvatarItemId(string id, MAvatarModel.ItemType itemType)
        {
            // Get the current set
            HashSet<string> ids = GetNewAvatarItemIds();

            // Add the new id to the set (Check if not already added)
            if (ids.Contains(id)) return;

            string prefix = GetPrefix(itemType);
            ids.Add(prefix + id);

            // Convert to string and save
            string idsStr = string.Join(",", ids);
            PlayerPrefs.SetString(user_id + "AvatarNewItemIds", idsStr);
        }

        public HashSet<string> GetNewAvatarItemIdsWithPrefix()
        {
            // Get the string (if exists)
            string idsStr = PlayerPrefs.GetString(user_id + "AvatarNewItemIds", "");
            if (idsStr == string.Empty)
                return new HashSet<string>();
            // Convert to HashSet and return
            string[] ids = idsStr.Split(',');

            return new HashSet<string>(ids);
        }

        public HashSet<string> GetNewAvatarItemIds()
        {
            HashSet<string> ids = GetNewAvatarItemIdsWithPrefix();
            // remove the prefix
            foreach (var item in ids)
                item.Substring(1);

            return new HashSet<string>(ids);
        }

        public void RemoveNewAvatarItemIds(HashSet<string> idsToRemove, MAvatarModel.ItemType itemType)
        {
            // Get the current set
            HashSet<string> ids = GetNewAvatarItemIds();

            string prefix = GetPrefix(itemType);

            // Remove the ids
            foreach (string id in idsToRemove)
            {
                ids.Remove(prefix + id);
            }

            // Convert to string and save
            string idsStr = string.Join(",", ids);
            PlayerPrefs.SetString(user_id + "AvatarNewItemIds", idsStr);
        }

        private string GetPrefix(MAvatarModel.ItemType itemType)
        {
            //add a prefix just for the playerprefs for indication:
            switch (itemType)
            {
                case MAvatarModel.ItemType.Accessory:
                    return "A";
                case MAvatarModel.ItemType.Costume:
                    return "C";
                case MAvatarModel.ItemType.Hair:
                    return "H";
                case MAvatarModel.ItemType.Outfit:
                    return "O";
                case MAvatarModel.ItemType.Trophy:
                    return "B"; // this was in the MavatarController
                case MAvatarModel.ItemType.Face:
                    return "F";
            }
            return "A";
        }
        public int GetLastLevelInAvatarCreatorPopup()
        {
            return PlayerPrefs.GetInt(user_id + "LastLevelInAvatar", ModelManager.Instance.GetUser().GetLevel());
        }

        public void SaveLastLevelInAvatarCreatorPopup()
        {
            PlayerPrefs.SetInt(user_id + "LastLevelInAvatar", ModelManager.Instance.GetUser().GetLevel());
        }

        public void StoreLeaderboardRanks(LeaderboardsModel.LeaderboardType type, string val)
        {
            PlayerPrefs.SetString(user_id + "Leaderboard_Ranks" + type.ToString(), val);
        }

        public string GetLastSeendMissionId()
        {
            return PlayerPrefs.GetString(user_id + "_mission_id", "");
        }

        public void SetLastSeenMissionId(string mission_id)
        {
            PlayerPrefs.SetString(user_id + "_mission_id", mission_id);
        }

        public string GetTableChatTextFavorites(string type)
        {
            return PlayerPrefs.GetString(user_id + "_" + type + "table_chat", "");
        }

        public void SetTableChatTextFavorites(string type, string v)
        {
            PlayerPrefs.SetString(user_id + "_" + type + "table_chat", v);
        }

        public void SetContestRemindMe(int contest_id, bool shouldRemind)
        {
            // There are schedules already
            HashSet<int> remindMeSet = GetRemindMeContestIdSet();

            if (shouldRemind)
                remindMeSet.Add(contest_id);
            else
                remindMeSet.Remove(contest_id);

            PlayerPrefs.SetString(user_id + "_ContestRemindMe", string.Join(",", remindMeSet));
        }


        /// <summary>
        /// Returns true or false weather the contest is already scheduled
        /// </summary>
        /// <param name="name">The name of the player prefs that contains the data on the contest schedule notification</param>
        /// <returns></returns>
        public bool IsContestRemindMe(int contest_id)
        {
            HashSet<int> remindMeSet = GetRemindMeContestIdSet();
            return remindMeSet.Contains(contest_id);
        }

        public void RemovedOldContestRemindMe(List<Contest> futureContests)
        {
            HashSet<int> remindMeSet = GetRemindMeContestIdSet();

            List<int> contestIds = futureContests.Select(c => c.Id).ToList();

            // run thorugh the hashset

            HashSet<int> remainingIds = new HashSet<int>();

            foreach (int contestId in remindMeSet)
            {
                //DeleteData from the hash list
                if (contestIds.Contains(contestId))
                    remainingIds.Add(contestId);
            }
            PlayerPrefs.SetString(user_id + "_ContestRemindMe", string.Join(",", remainingIds));
        }

        HashSet<int> GetRemindMeContestIdSet()
        {
            string values = PlayerPrefs.GetString(user_id + "_ContestRemindMe", "");
            if (values == "")
                values = "0";

            return new HashSet<int>(values.Split(',').Select(int.Parse).ToList());
        }

        public void SaveCurrentLevelForTableUnlock(int level)
        {
            PlayerPrefs.SetInt(user_id + "Table_Unlock", level);
        }

        public int GetCurrentLevelForTableUnlock()
        {
            return PlayerPrefs.GetInt(user_id + "Table_Unlock", 1);
        }

        public void SaveLevelUnlocked(string match_key, string world_key)
        {
            PlayerPrefs.SetString(user_id + "World_Unlock_Key", world_key);
            PlayerPrefs.SetString(user_id + "Match_Item_Unlock_Key", match_key);
        }

        public void RemoveLevelUnlcoked()
        {
            PlayerPrefs.DeleteKey(user_id + "World_Unlock_Key");
            PlayerPrefs.DeleteKey(user_id + "Match_Item_Unlock_Key");
        }

        public void SetShouldShowMissionUnlockOverlay(bool value)
        {
            PlayerPrefs.SetInt(user_id + "ShouldShowMissionUnlockOverlay", value ? 1 : 0);
        }

        public bool GetShouldShowMissionUnlockedOverlay()
        {
            return PlayerPrefs.GetInt(user_id + "ShouldShowMissionUnlockOverlay", 0) != 0;
        }


        public void SetLastOpenItemLevel(int level)
        {
            PlayerPrefs.SetInt(user_id + "Last_Open_Item_Level", level);
        }

        public int GetLastOpenItemLevel()
        {
            return PlayerPrefs.GetInt(user_id + "Last_Open_Item_Level", 1);
        }

        public string GetLevelUnlockedItemKey()
        {
            return PlayerPrefs.GetString(user_id + "Match_Item_Unlock_Key", null);
        }

        public void SetFBShare(string key, bool val)
        {
            PlayerPrefs.SetInt(user_id + "FB_Share_" + key, val ? 1 : 0);
        }

        public bool IsFBShare(string key)
        {
            int FBShare = PlayerPrefs.GetInt(user_id + "FB_Share_" + key, 1);
            return FBShare == 1;
        }


        public string LoadUserStatsJson()
        {
            return PlayerPrefs.GetString(user_id + "User_Stats");
        }

        public void SaveUserStatsJson(string userStatsJson)
        {
            PlayerPrefs.SetString(user_id + "User_Stats", userStatsJson);
        }

        public string LoadSendGiftsJson()
        {
            return PlayerPrefs.GetString(user_id + "User_sent_gifts", "{}");
        }

        public void SaveSendGiftsJson(JSONObject SendGiftsJson)
        {
            PlayerPrefs.SetString(user_id + "User_sent_gifts", SendGiftsJson.ToString());
        }

        public long LoadLeaderboardFriendsUpdatedTime()
        {
            string val = PlayerPrefs.GetString(user_id + "LB_Friends_Updated_Time", "0");
            return Convert.ToInt64(val);
        }

        public void SaveLeaderboardFriendsUpdatedTime(long time)
        {
            PlayerPrefs.SetString(user_id + "LB_Friends_Updated_Time", time.ToString());
        }

        public int LoadLeaderboardFriendsNum()
        {
            return PlayerPrefs.GetInt(user_id + "LB_Friends_Num", 0);
        }

        public void SaveLeaderboardFriendsNum(int friendsNum)
        {
            PlayerPrefs.SetInt(user_id + "LB_Friends_Num", friendsNum);
        }


        public void SaveLeaderboardCollectedWeek(int weekNo)
        {
            PlayerPrefs.SetInt(user_id + "Leaderboard_Bonus_Collected", weekNo);
        }

        public int GetLeaderboardCollectedWeek()
        {
            return PlayerPrefs.GetInt(user_id + "Leaderboard_Bonus_Collected", 1);
        }

        public String GetLeaderboardRanks(LeaderboardsModel.LeaderboardType type)
        {
            return PlayerPrefs.GetString(user_id + "Leaderboard_Ranks" + type.ToString(), "");
        }

        public string GetTableChatEmojiFavorites(string type)
        {
            return PlayerPrefs.GetString(user_id + "_" + type + "table_chat_emoji", "");
        }

        public void SetTableChatEmojiFavorites(string type, string v)
        {
            PlayerPrefs.SetString(user_id + "_" + type + "table_chat_emoji", v);
        }

        public string GetUserDetails()
        {
            return PlayerPrefs.GetString(user_id + "UserDetails", "");
        }

        public void SetUserDetails(string userDetails)
        {
            PlayerPrefs.SetString(user_id + "UserDetails", userDetails);
        }

        public void SetDontShowGameCenterLogin(bool value)
        {
            PlayerPrefs.SetInt(user_id + "GC_Login", value ? 1 : 0);
        }

        public bool GetShouldShowGameCenterLogin()
        {
            return PlayerPrefs.GetInt(user_id + "GC_Login") == 1;
        }

        public void ClearAgeVars()
        {
            PlayerPrefs.DeleteKey(user_id + "age_timer");
            PlayerPrefs.DeleteKey(user_id + "age_test_failed");
            PlayerPrefs.DeleteKey(user_id + "age_verification_done");

        }

        public void SetAgeVerificationTimer(float min_to_wait)
        {
            TimeSpan extra = TimeSpan.FromMinutes(min_to_wait);
            DateTime curr_time = DateTime.Now + extra;
            string curr_time_str = curr_time.ToString();

            PlayerPrefs.SetString(user_id + "age_timer", curr_time_str);
        }

        public string GetAgeVerifictionTimer()
        {
            string timer_start_time = PlayerPrefs.GetString(user_id + "age_timer");

            if (timer_start_time == "")
                return null;

            return timer_start_time;
        }

        public void IncrementAgeVerificationFailed(bool reset = false)
        {
            int curr_failed = PlayerPrefs.GetInt(user_id + "age_test_failed");
            curr_failed++;

            if (reset)
                curr_failed = 0;

            PlayerPrefs.SetInt(user_id + "age_test_failed", curr_failed);
        }

        public int GetAgeVerificationFailed()
        {
            return PlayerPrefs.GetInt(user_id + "age_test_failed");
        }

        public void SetAgeVerificationCompleted()
        {
            PlayerPrefs.SetInt(user_id + "age_verification_done", 1);
        }

        public bool GetAgeVerificationComplete()
        {
            return PlayerPrefs.GetInt(user_id + "age_verification_done") == 1 ? true : false;
        }

        public string GetPushToken()
        {
            return PlayerPrefs.GetString(user_id + "Push_Token", "");
        }

        public void SetPushToken(string pushToken)
        {
            PlayerPrefs.SetString(user_id + "Push_Token", pushToken);
        }

        public bool GetShouldShowLeaderboardsUnlocked()
        {
            return PlayerPrefs.GetInt(user_id + "Should_Show_Leaderboards_Unlocked", 0) != 0;
        }

        //set only if level lower than 15 and no flag
        public void SetShouldShowLeaderboardsUnlocked()
        {
            PlayerPrefs.SetInt(user_id + "Should_Show_Leaderboards_Unlocked", 1);
        }

        public void ClearShouldShowLeaderboardsUnlocked()
        {
            PlayerPrefs.DeleteKey(user_id + "Should_Show_Leaderboards_Unlocked");
        }

        public bool GetShouldShowContestsUnlocked()
        {
            return PlayerPrefs.GetInt(user_id + "Should_Show_Contests_Unlocked", 0) != 0;
        }

        //set only if level lower than 15 and no flag
        public void SetShouldShowContestsUnlocked()
        {
            PlayerPrefs.SetInt(user_id + "Should_Show_Contests_Unlocked", 1);
        }

        public void ClearShouldShowContestsUnlocked()
        {
            PlayerPrefs.DeleteKey(user_id + "Should_Show_Contests_Unlocked");
        }

        public void SetAssignedBotId(int leaderboardId, int botId)
        {
            if (leaderboardId == -1)
                return;

            PlayerPrefs.SetString(user_id + "AssignedBotId", leaderboardId + ":" + botId);
            LoggerController.Instance.LogFormat("Setting assign bot id: {0}", botId);
        }

        public int GetAssignedBotId(int leaderboardId)
        {
            int result = 0;
            try
            {
                if (leaderboardId == -1)
                    return result;

                string assignedBot = PlayerPrefs.GetString(user_id + "AssignedBotId");

                // Compare the leaderboard id to the stored one
                string[] tokens = assignedBot.Split(':');
                if (tokens.Length == 2)
                {
                    if (tokens[0] != leaderboardId.ToString())
                    {
                        result = 0;
                        // Delete the old player pref
                        PlayerPrefs.DeleteKey(user_id + "AssignedBotId");
                    }
                    else
                    {
                        result = Int32.Parse(tokens[1]);
                    }
                }

                LoggerController.Instance.LogFormat("Getting assign bot: {0}", result);
            }
            catch
            {
                result = 0;
            }
            return result;
        }

        public int GetAvatarScreenPanelIndex()
        {
            return PlayerPrefs.GetInt(user_id + "AvatarScreenPanelIndex");
        }

        public void SetAvatarScreenPanelIndex(int index)
        {
            PlayerPrefs.SetInt(user_id + "AvatarScreenPanelIndex", index);
        }

        public int GetAvatarScreenItemTypeIndex()
        {
            return PlayerPrefs.GetInt(user_id + "AvatarScreenItemTypeIndex");
        }

        public void SetAvatarScreenItemTypeIndex(int index)
        {
            PlayerPrefs.SetInt(user_id + "AvatarScreenItemTypeIndex", index);
        }

        public bool GetLocalServerSetting()
        {
            return PlayerPrefs.GetInt(user_id + "UseLocalServer") == 1;
        }

        public bool GetShouldShowPiggyUnlocked()
        {
            return PlayerPrefs.GetInt(user_id + "piggyUnlocked", 0) == 1;
        }

        //set only if level lower than piggy unlock (5) and no flag
        public void SetShouldShowPiggyUnlocked()
        {
            PlayerPrefs.SetInt(user_id + "piggyUnlocked", 1);
        }

        public void ClearShouldShowPiggyUnlocked()
        {
            PlayerPrefs.DeleteKey(user_id + "piggyUnlocked");
        }

        public void SetActivePiggyAtLastUpdate(bool isActive)
        {
            PlayerPrefs.SetInt(user_id + "activePiggyLastUpdate", isActive ? 1 : 0);
        }

        public bool GetPiggyWasActiveAtLastUpdate()
        {
            return PlayerPrefs.GetInt(user_id + "activePiggyLastUpdate", 0) == 1;
        }
        

        //TODO: if no logner break stuff delete these methods for good
        //should not be used in multiplayer. not removed to not maybe break stuff
        public string GetLastGameId()
        {
            return PlayerPrefs.GetString(user_id + "LastGameId");
        }

        //should not be used in multiplayer. not removed to not maybe break stuff
        public void SetLastGameId(string game_id)
        {
            PlayerPrefs.SetString(user_id + "LastGameId", game_id);
        }

        //should not be used in multiplayer. not removed to not maybe break stuff
        public string GetLastGameTable()
        {
            return PlayerPrefs.GetString(user_id + "LastGameTable");
        }

        //should not be used in multiplayer. not removed to not maybe break stuff
        public void SetLastGameTable(string m_last_table_id)
        {
            PlayerPrefs.SetString(user_id + "LastGameTable", m_last_table_id);
        }

    }
}