using cardGames.comm;
using cardGames.models;
using common.controllers;
using common.ims.model;
using common.models;
using System;
using System.Collections.Generic;

namespace cardGames.controllers
{


    public delegate void BonusClaimedDelegate(bool success, BonusAwarded bonusAwarded);

    public class BonusController : MonoBehaviour
    {

        public const int DAILY_BONUS_FRIENDS_CAP = 10; // TODO: Should be read from the server

#if QA_MODE || DEV_MODE
        public static TimeSpan HOURLY_BONUS_INTERVAL = TimeSpan.FromMinutes(2.5f); // Used to show the hourly video button
#else
        public static TimeSpan HOURLY_BONUS_INTERVAL = TimeSpan.FromHours(4); // Used to show the hourly video button
#endif

        public static BonusController Instance;

        private void Awake()
        {
            Instance = this;
        }



        public void ClaimBonus(Bonus.BonusTypes type, int sub_type = 0, int num_of_friends = 0, List<string> senderIds = null, int leaderboardRank = 0,
            BonusClaimedDelegate claimedDelegate = null, string token = null, bool showErrorMessage = true, bool isRedeem = false,string imsMetadata=null)
        {
            CardGamesCommManager.Instance.ClaimBonus(type, sub_type, token, num_of_friends, senderIds, leaderboardRank, isRedeem,imsMetadata, (bool success, int newCoinsBalance, int newDiamondsBalance, BonusAwarded bonusAwarded) =>
            {
                if (success)
                {
                    LoggerController.Instance.LogFormat("Bonus awarded: {0}", bonusAwarded);

                    // Update user with new balances
                    ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);

                    bonusAwarded.SubType = sub_type;

                    switch (type)
                    {
                        case Bonus.BonusTypes.Hourly:
                            // Update the hourly bonus with the new interval
                            HourlyBonus hBonus = (HourlyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Hourly);
                            hBonus.Interval = bonusAwarded.Interval;
                            break;
                        case Bonus.BonusTypes.Daily:
                            // Update the next step and the next step interval
                            DailyBonus dBonus = (DailyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Daily);
                            dBonus.Interval = bonusAwarded.Interval;
                            dBonus.NextStep = bonusAwarded.NextStep;
                            dBonus.IsEntitled = false;
                            break;

                        case Bonus.BonusTypes.Invite:
                            InviteBonus iBonus = (InviteBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Invite);                           
                            TrackingManager.Instance.AppsFlyerTrackIncrementalEvent("FriendsJoined", iBonus.Inviters.Count, new int[] { 3, 5, 10 });
                            break;

                    }

                    if (claimedDelegate != null)
                    {
                        claimedDelegate(true, bonusAwarded);
                    }
                }
                else
                {
                    LoggerController.Instance.LogFormat("Bonus type: {0}, sub-type: {1} was not awarded", type, sub_type);

                    if (showErrorMessage)
                        PopupManager.Instance.ShowMessagePopup("This bonus can no longer be collected.");

                    if (claimedDelegate != null)
                    {
                        claimedDelegate(false, bonusAwarded);
                    }
                }
            });
        }

        public void ClaimIMSBonus(IMSInteractionZone iZone, BonusClaimedDelegate OnBonusClaim)
        {
            //show wait
            CardGamesPopupManager.Instance.ShowWaitingIndication(true, 5);

            IMSActionValue actionVal = iZone.Action.Values[0];
            string imsMetadata = iZone.Metadata;
            CardGamesCommManager.Instance.ClaimIMSBonus(actionVal, imsMetadata, (bool success, int newCoinsBalance, BonusAwarded bonusAwarded) =>
             {
                 if (success)
                 {
                     // update coins
                     ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);

                     // update avater item
                     foreach (string item in bonusAwarded.GoodsList.InventoryItems)
                         ModelManager.Instance.Inventory.AddItem(item);

                     // update voucher
                     VoucherController.Instance?.NewVouchersReceived(bonusAwarded.GoodsList.Vouchers);

                     if (OnBonusClaim != null)
                     {
                         OnBonusClaim(true, bonusAwarded);
                     }
                 }
                 else
                 {

                     // if (showErrorMessage)
                     PopupManager.Instance.ShowMessagePopup("This bonus can no longer be collected.");

                     if (OnBonusClaim != null)
                     {
                         OnBonusClaim(false, bonusAwarded);
                     }
                 }
                 CardGamesPopupManager.Instance.ShowWaitingIndication(false);
             });
        }

        public void ClaimBonusForMES(string bonusToken, BonusClaimedDelegate OnBonusClaim)
        {
            //show wait
            CardGamesPopupManager.Instance.ShowWaitingIndication(true, 5);
            //send bonus to BO
            ClaimBonus(
                type: Bonus.BonusTypes.GeneralBonus,
                token: bonusToken,
                claimedDelegate: (bool success, BonusAwarded bonusAwarded) =>
                {
                    CardGamesPopupManager.Instance.ShowWaitingIndication(false);
                    OnBonusClaim(success, bonusAwarded);
                });
        }
    }
}
