﻿using common.utils;


namespace cardGames.piggy.views
{
    public class PiggyHeaderRocketView : MonoBehaviour
    {
        private Animator rocketAnimator;
        [SerializeField]
        private float pauseBetweenAnimations = 2;
        [SerializeField]
        private int numOfIgnitesBeforeLiftOff = 2;
        private int currentCycle = 0;

        private void Awake()
        {
            rocketAnimator = GetComponent<Animator>();
        }

        public void Reset()
        {
            rocketAnimator.SetTrigger("reset");
            currentCycle = 0;
            rocketAnimator.SetTrigger("ignite");
        }

        public void RoocketAnimCycleCompleted() //animation event
        {
            InvokingUtils.InvokeInSeconds(pauseBetweenAnimations, () =>
            {
                currentCycle++;
                if (currentCycle > numOfIgnitesBeforeLiftOff)
                {
                    rocketAnimator.SetTrigger("fly");
                    currentCycle = 0;
                }
                else
                    rocketAnimator.SetTrigger("ignite");
            }, true, this);
        }
    }
}