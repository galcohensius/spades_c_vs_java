﻿using System.Collections;
using cardGames.models;
using common.utils;
using System;
using cardGames.controllers;
using cardGames.piggy.controllers;
using common.controllers;

namespace cardGames.piggy.views
{
    public class PiggyHeaderView : MonoBehaviour
    {
        private Animator animator;
        private UserPiggyData piggyData;

        [SerializeField]
        private GameObject sparks;
        [SerializeField]
        private GameObject[] piggyStates; //coresponding to PiggyState enum + last one is locked
        private bool isLocked = true;
        [SerializeField]
        private GameObject lockIcon;
        [SerializeField]
        private GameObject timer;
        [SerializeField]
        private GameObject newIcon;
        [SerializeField]
        private GameObject tooltip;

        [SerializeField]
        private GameObject boostBadge;

        private IEnumerator headerTimer;
        private IEnumerator startBoostTimer;
        private IEnumerator endBoostTimer;

        public float sparksLoopTime = 5;
        Tween sparksTween;

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        private void Start()
        {
            GetComponent<Button>().onClick.AddListener(OpenPiggyPopup);
            AnimateSparks();
            DisablePiggy();
            PiggyController.Instance.OnPiggyUpdated += UpdatePiggy;
            PiggyController.Instance.OnPiggyRefreshing += DisablePiggy;
            PiggyController.Instance.OnEndGame += AnimatePiggyDeposit;
            PiggyController.Instance.OnUnlockCheck += Unlock;
            PiggyController.Instance.ActivatePiggy += ActivatePiggy;
            LockPiggy();
        }

        private void Unlock(bool isUnlocked)
        {
            if (isUnlocked == false)
            {
                DisablePiggy();
                LockPiggy();
            }
            else
            {
                lockIcon.SetActive(false);
                isLocked = false;
            }
        }

        public void ActivatePiggy()
        {
            bool active = PiggyController.Instance.IsPiggyActiveForUser;
            this.gameObject.SetActive(active);
            GetComponent<Button>().interactable = active;
        }

        //call this after model is updated
        public void UpdatePiggy(UserPiggyData piggyData, bool piggyWasActive)
        {
            if (piggyData == null)
            {
                CardGamesLoggerController.Instance.LogError("tried to update piggy in header with no piggy data");
                return;
            }
            else
            {
                if (piggyData != this.piggyData)
                    piggyData.OnPiggyUpdated += RefreshPiggy;
                this.piggyData = piggyData;
            }

            if (ModelManager.Instance.GetUser().GetLevel() < PiggyController.Instance.MinPiggyLevel)
            {
                LockPiggy();
                LoggerController.Instance.LogError("UpdatePiggy occured while it is locked!");
                return;
            }

            //TODO: uncomment when we'll use the "new" badge again
            //newIcon.SetActive(!piggyWasActive && piggyData.PiggyState == PiggyState.Accumulating);

            UpdatePiggyGraphics();
            AnimateSparks();
            SetTimer();
            SetBoost();
            Unlock(true); 
        }

        public void LockPiggy()
        {
            piggyStates[(int)PiggyState.Accumulating].SetActive(true);
            lockIcon.SetActive(true);
            lockIcon.GetComponentInChildren<TextMeshProUGUI>().text = "lvl " + PiggyController.Instance.MinPiggyLevel;
            isLocked = true;
        }

        private void RefreshPiggy(int coinsDelta)
        {
            UpdatePiggyGraphics();
        }

        private void DisablePiggy()
        {
            if (sparksTween != null)
                sparksTween.Pause();
            sparks.SetActive(false);
            isLocked = true;
            timer.SetActive(false);
            newIcon.SetActive(false);
            tooltip.SetActive(false);
            lockIcon.SetActive(false);
            AnimateRocket(false);
        }

        private void UpdatePiggyGraphics()
        {
            int activeState = (int)piggyData.PiggyState;
            for (int i = 0; i < piggyStates.Length; i++)
                piggyStates[i].SetActive(activeState == i);
        }

        private void AnimateSparks()
        {
            if (sparksTween == null)
            {
                Image sparksImg = sparks.GetComponentInChildren<Image>();
                sparksImg.color = new Color(1, 1, 1, 0);
                sparksTween = sparks.GetComponentInChildren<Image>().DOFade(1, sparksLoopTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
            }
            else
                sparksTween.Restart();
        }

        private void SetTimer()
        {
            timer.SetActive(false);
            TimeSpan ts = new TimeSpan(piggyData.EndDate.Ticks - DateTime.Now.Ticks);
            if (ts.TotalDays > 2)
            {
                float delay = (float)(ts.Subtract(new TimeSpan(48, 0, 0))).TotalSeconds;
                if (delay < 0)
                    delay = 0;
                InvokingUtils.InvokeInSeconds(delay, SetTimer, true, this);
                return;
            }

            if (headerTimer != null)
                StopCoroutine(headerTimer);

            if (ts.TotalSeconds <= 0)
                return;

            timer.SetActive(true);
            headerTimer = DateUtils.RunTimer48Format(ts, timer.GetComponentInChildren<TextMeshProUGUI>(), "", /*OnTimerDepleted*/null);
            StartCoroutine(headerTimer);
        }

        private void SetBoost()
        {
            if (startBoostTimer != null)
                StopCoroutine(startBoostTimer);
            if (endBoostTimer != null)
                StopCoroutine(endBoostTimer);
            AnimateRocket(false);

            if (piggyData.CoinsMultiplier > 1 && DateTime.Now < piggyData.CoinsMultiplierEndDate)
            {
                //boost is active now
                if (piggyData.CoinsMultiplierStartDate <= DateTime.Now)
                {
                    boostBadge.SetActive(true);
                    TimeSpan ts = new TimeSpan(piggyData.CoinsMultiplierEndDate.Ticks - DateTime.Now.Ticks);
                    endBoostTimer = ShowBoosterBadge((float)ts.TotalSeconds, false);
                    StartCoroutine(endBoostTimer);
                    AnimateRocket(true);
                }
                else //futue boost, no visible timer but need to show the rocket in time
                {   
                    boostBadge.SetActive(false);
                    TimeSpan ts = new TimeSpan(piggyData.CoinsMultiplierStartDate.Ticks - DateTime.Now.Ticks);
                    startBoostTimer = ShowBoosterBadge((float)ts.TotalSeconds, true);
                    StartCoroutine(startBoostTimer);
                    ts = new TimeSpan(piggyData.CoinsMultiplierEndDate.Ticks - DateTime.Now.Ticks);
                    endBoostTimer = ShowBoosterBadge((float)ts.TotalSeconds, false);
                    StartCoroutine(endBoostTimer);
                }
            }
            else
                boostBadge.SetActive(false);
        }

        private IEnumerator ShowBoosterBadge(float inSeconds, bool show)
        {
            yield return new WaitForSecondsRealtime(inSeconds);
            AnimateRocket(show);
        }

        private void AnimateRocket(bool start)
        {
            if (start)
            {
                boostBadge.SetActive(true);
            }
            else
            {
                boostBadge.SetActive(false);
            }
            boostBadge.GetComponent<PiggyHeaderRocketView>().Reset();
        }

        private void OnTimerDepleted()
        {
            DisablePiggy();
        }

        public void OpenPiggyPopup()
        {
            if (isLocked)
            {
                PopupManager.Instance.ShowMessagePopup("CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + PiggyController.Instance.MinPiggyLevel + "!", PopupManager.AddMode.ShowAndRemove, true);
                return;
            }
            CardGamesPopupManager.Instance.ShowPiggyOfferPopup();
        }

        public void ShowNewIcon(bool show = true)
        {
            newIcon.SetActive(show);
        }

        public void ShowTooltip(string message = "")
        {
            tooltip.SetActive(true);
            if (message != "")
                tooltip.GetComponentInChildren<TextMeshProUGUI>().text = message;

        }

        public void AnimatePiggyDeposit()
        {
            //do nothing if feature not unlocked
            if (isLocked)
                return;

            if (piggyData == null)
            {
                LoggerController.Instance.LogError("Piggy header tried to query piggy model and it's null");
                return;
            }

            
            if (piggyData.PiggyState == PiggyState.Cooldown)
                return;

            switch (piggyData.PiggyState)
            {
                case PiggyState.Accumulating:
                    animator.SetTrigger("deposit");
                    break;
                case PiggyState.Full:
                    animator.SetTrigger("reject");
                    break;
                case PiggyState.Cooldown:
                    //let sleeping pigs lay. no animations ATM
                    break;
                default:
                    throw new Exception("unhandled piggy state WTF");
            }
        }
        //animation trigger
        public void PlayPiggyDepositSFX()
        {
            CardGamesSoundsController.Instance.PlayPiggyDeposit();
        }
        //animation trigger
        public void PlayPiggyDepositRejectedSFX()
        {
            CardGamesSoundsController.Instance.PlayPiggyDeposit(false);
        }

        private void OnDestroy()
        {
            PiggyController.Instance.OnPiggyUpdated -= UpdatePiggy;
            PiggyController.Instance.OnPiggyRefreshing -= DisablePiggy;
            PiggyController.Instance.OnEndGame -= AnimatePiggyDeposit;
            PiggyController.Instance.OnUnlockCheck -= Unlock;
            PiggyController.Instance.ActivatePiggy -= ActivatePiggy;
        }
    }
}