﻿using cardGames.comm;
using cardGames.controllers;
using cardGames.models;
using common.controllers;
using common.controllers.purchase;
using common.ims;
using common.ims.model;
using common.mes;
using common.models;
using common.utils;
using System;
using System.Collections;
using System.Collections.Generic;

namespace cardGames.piggy.controllers
{
    public class PiggyController : MonoBehaviour
    {
        public const string PIGGY_BANNER = "PG";
        public bool IsPiggyActiveForUser { get; private set; } = true; //controlled by IMS
        public Action ActivatePiggy;

        private CardGamesModelParser cardGamesModelParser;
        //bool -> true - piggy was active during last GetPiggy, false - piggy was on cooldown
        public Action<UserPiggyData, bool> OnPiggyUpdated;
        public Action<bool> OnUnlockCheck; //true -> piggy is unlocked, false -> piggy is locked
        public Action OnPiggyRefreshing;
        public Action OnShowCapRemonder;
        public Action OnEndGame;
        public int MinPiggyLevel { get; private set; } = 5;
        /// <summary>
        /// the number of MINUTES before the offer ends to show time based reminder
        /// </summary>
        public int IntervalForReminder { get; private set; } = 720; //normally 12 hours, for QA 5 minutes

        bool m_purchase_locked = false;

        private Coroutine m_lockReleaseCoroutine;
        public event Action<int> PurchaseSuccessful;
        public event Action<string, IMSGoodsList> PurchaseCompleted;
        public event Action<string> PurchaseCanceled;

        /// <summary>
        /// if in lobby show waiting indicator, if in game don't show
        /// </summary>
        [HideInInspector]
        public bool ShowWaitingIndication = true;

        public static PiggyController Instance;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        private void Start()
        {
            cardGamesModelParser = CardGamesCommManager.Instance.GetModelParser() as CardGamesModelParser;

            // Set contests min level
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                MinPiggyLevel = ldc.GetSettingAsInt("MinPiggyLevel", 5);
                IntervalForReminder = ldc.GetSettingAsInt("PiggyIntervalForReminderMin", 5);
            };
        }

        public bool ShowOrDeactivatePiggy()
        {
            IsPiggyActiveForUser = IMSController.Instance.BannersByLocations.ContainsKey(PIGGY_BANNER);
            return IsPiggyActiveForUser;
        }

        /// <summary>
        /// locks/unlocks piggy
        /// </summary>
        /// <returns>is piggy unlocked</returns>
        public bool LockUnlockPiggy()
        {
            if (!IsPiggyActiveForUser)
                return false;

            int userLevel = ModelManager.Instance.GetUser().GetLevel();
            if (userLevel < MinPiggyLevel)
            {
                CardGamesStateController.Instance.SetShouldShowPiggyUnlocked();
                OnUnlockCheck?.Invoke(false);
                return false;
            }

            //check and show piggy unlock popup-overlay
            if (userLevel >= MinPiggyLevel && CardGamesStateController.Instance.GetShouldShowPiggyUnlocked())
            {
                CardGamesStateController.Instance.ClearShouldShowPiggyUnlocked();
                SetDummySleepingPiggy();
                RefreshPiggy(false);
                CardGamesPopupManager.Instance.ShowPiggyUnlockedPopup(PopupManager.AddMode.ShowAndRemove);
            }

            OnUnlockCheck?.Invoke(true);
            return true;
        }

        public void SetDummySleepingPiggy()
        {
            UserPiggyData dummyPiggy = ModelManager.Instance.GetPiggy();
            if (dummyPiggy == null)
            {
                dummyPiggy = new UserPiggyData(0, -1, -1, 1, 4.04f, new List<List<int>>(), 10, -1, "dummy piggy", 0, "dummy piggy", "", -1, 2);
                ModelManager.Instance.SetPiggy(dummyPiggy);
            }
            else
                dummyPiggy.OverrideToCooldownStateWhileRefreshing();
            
            OnPiggyUpdated?.Invoke(dummyPiggy, true);
        }

        public void RefreshPiggy(bool addDelay = true)
        {
            if (!IsPiggyActiveForUser)
                return;

            UserPiggyData userPiggyData = ModelManager.Instance.GetPiggy();
            userPiggyData.OverrideToCooldownStateWhileRefreshing();
            OnPiggyUpdated?.Invoke(userPiggyData, true);

            float randTime = addDelay ? UnityEngine.Random.Range(0, 20f) : 0;
            InvokingUtils.InvokeActionWithDelay(false, randTime, () =>
            {
                // Don't think it's needed (RAN 25/5/2020)
                //if (ShowWaitingIndication)
                //    CardGamesPopupManager.Instance.ShowWaitingIndication(true);
                OnPiggyRefreshing?.Invoke();

                cardGames.controllers.CommManagerJava.Instance.GetPiggy((success, responseData) =>
                {
                    if (success)
                        SetUserPiggy(responseData);
                    else
                        LoggerController.Instance.LogError("bad piggy");

                    CardGamesPopupManager.Instance.ShowWaitingIndication(false);
                });

            }, this);
        }

        public void SetUserPiggy(JSONNode jSONNode)
        {
            if (ModelManager.Instance.GetUser().GetLevel() < MinPiggyLevel)
                return;

            UserPiggyData userPiggyData = cardGamesModelParser.ParseUserPiggyDate(jSONNode);

            //set the model
            ModelManager.Instance.SetPiggy(userPiggyData);

            if (userPiggyData.PiggyState!=PiggyState.Cooldown)
            {
                //set the external purchase data (for mobile)   
                ModelManager.Instance.AddExternalPackage(userPiggyData.PackageID, userPiggyData.ExternalPackageID);
                PurchaseController.Instance.UpdatePurchaser();
            }


            OnPiggyUpdated?.Invoke(userPiggyData, CardGamesStateController.Instance.GetPiggyWasActiveAtLastUpdate());

            if (userPiggyData.EndDateInterval > 0)
            {
                if (CardGamesStateController.Instance.GetNotificationSetting())
                    NotificationController.Instance.SchedulePiggyTimeRemindingNotification();
                else
                    NotificationController.Instance.CancelPiggyTimeRemindingNotifications();

                InvokingUtils.InvokeActionWithDelay(false, userPiggyData.EndDateInterval, ()=> RefreshPiggy(), this);
            }

            CardGamesStateController.Instance.SetActivePiggyAtLastUpdate(userPiggyData.PiggyState != PiggyState.Cooldown);
        }

        public void UpdatePiggy(JSONNode jSONNode)
        {
            EndGamePiggyData endGamePiggyData = cardGamesModelParser.ParseEndGamePiggyData(jSONNode);
            if (ModelManager.Instance.GetPiggy().PiggyState == PiggyState.Cooldown && endGamePiggyData.PiggyState != PiggyState.Cooldown)
            {
                RefreshPiggy(false);
                return;
            }
            if (ModelManager.Instance.UpdatePiggy(endGamePiggyData)) //did the piggy filled in last game
                NotificationController.Instance.PushPiggyFullNotification();
        }

        public void PurchasePiggy()
        {
            UserPiggyData userPiggyData = ModelManager.Instance.GetPiggy();

            string packageID = userPiggyData.PackageID;

            PurchaseTrackingData purchaseTrackingData = default;
            purchaseTrackingData.imsMetaData = userPiggyData.PackageMetaData;

            PopupManager.Instance.ShowWaitingIndication(true);

            PurchaseController.Instance.MakePurchase(packageID, CardGamesCommManager.Instance.LoginToken, purchaseTrackingData, (bool success, bool canceled, bool latePayment,
                    int newCoinsBalance, IMSGoodsList imsGoodsList, Cashier new_cashier) =>
            {
                
                //there is a problem with the callback - so i am calling the release of the lock from here - after an answer came - there will be a popup so cashier will be closed
                m_purchase_locked = false;

                //close the waiting popup
                PopupManager.Instance.ShowWaitingIndication(false);


                if (success)
                {
                    // Store the last goodvirtz - might be used on chained purchased
                    PurchaseController.Instance.LastGoodsList.Add(imsGoodsList);

                    //update the cashier data
                    if (new_cashier != null)
                    {
                        ModelManager.Instance.SetCashier(new_cashier);
                    }

                    LoggerController.Instance.LogFormat("Purchase of item {0} successful", packageID);

                    //animations should come here
                    PurchaseSuccessful?.Invoke(newCoinsBalance);

                    User user = ModelManager.Instance.GetUser();
                    // Must be set after the popup - TOOD: remove this limitation
                    user.SetCoins(newCoinsBalance);

                    VoucherController.Instance?.NewVouchersReceived(imsGoodsList.Vouchers);

                    // update avater item
                    foreach (string item in imsGoodsList.InventoryItems)
                        ModelManager.Instance.Inventory.AddItem(item);
                    MAvatarController.Instance.NewInventoryItemsArrived(imsGoodsList.InventoryItems);

                    // Check if first deposit
                    if (user.User_type == User.UserType.NonPaying)
                    {
                        // First deposit
                        user.User_type = User.UserType.Paying;

                        // Appsflyer first deposit event
                        TrackingManager.Instance?.AppsFlyerTrackEvent(TrackingManager.EventName.FirstDeposit);

                    }

                    //firing the purchase complete event for special PO and invite banner to listen to
                    PurchaseCompleted?.Invoke(packageID, imsGoodsList);

                    //calls the MES Controller to update the banners - 
                    MESBase.Instance?.Execute(MESBase.TRIGGER_LOGIN, true);

                    //piggy was purchased, remove all future notifications for this piggy
                    NotificationController.Instance.CancelPiggyTimeRemindingNotifications();

                }
                else
                {
                    if (latePayment)
                    {
                        PopupManager.Instance.ShowMessagePopup("Your purchase was initiated, coins balance will be updated when the transaction completes", PopupManager.AddMode.ShowAndKeep, true, PurchaseAnswerPopupClosed);

                    }
                    else if (canceled)
                    {

                        PopupManager.Instance.ShowMessagePopup("Your purchase has not been processed", PopupManager.AddMode.ShowAndKeep, true, PurchaseAnswerPopupClosed);

                    }
                    else
                    {

                        PopupManager.Instance.ShowMessagePopup("Your payment processing was unsuccessful", PopupManager.AddMode.ShowAndKeep, true, PurchaseAnswerPopupClosed, "TRY AGAIN");

                    }
                    //firing the purchase complete event for special PO and invite banner to listen to
                    if (PurchaseCanceled != null)
                        PurchaseCanceled(packageID);

                }
                
            });
        }


        public bool ShowTimeReminder(UserPiggyData userPiggyData = null)
        {
            if (userPiggyData == null)
                userPiggyData = ModelManager.Instance.GetPiggy();
            DateTime reminder = userPiggyData.EndDate.Subtract(new TimeSpan(0, IntervalForReminder, 0));
            return DateTime.Now >= reminder;
        }

        private void PurchaseAnswerPopupClosed()
        {
            m_purchase_locked = false;
            if (m_lockReleaseCoroutine != null)
                StopCoroutine(m_lockReleaseCoroutine);
        }

        private IEnumerator LockRelease()
        {
            yield return new WaitForSecondsRealtime(15);
            m_purchase_locked = false;
        }

        public bool SendUpdatePiggyAtEndGame()
        {
            if (!IsPiggyActiveForUser)
                return false;
            UserPiggyData piggyData = ModelManager.Instance.GetPiggy();
            if (piggyData == null)
                return false;   //if null then the feature is available to the user but haven't been unlocked yet

            //if (!CardGamesStateController.Instance.GetShouldShowPiggyUnlocked())
            //    return false;

            return piggyData.PiggyState != PiggyState.Full; //we ask only if we can accumulate or on cooldown and attempting to become accumulating
        }
    }
}