﻿public class ItemView : MonoBehaviour
{
    [SerializeField] int minLevel=1;
    [SerializeField] int storeOrder;
    [SerializeField] string inventoryID;
    [SerializeField] string id;
    [SerializeField] bool featured;
    [SerializeField] bool animated;
    [SerializeField] float botRandomPercentage;

    public int MinLevel { get => minLevel;}
    public int StoreOrder { get => storeOrder;}
    public string Id { get => id; }
    public string InventoryID { get => inventoryID;}
    public bool Featured { get => featured; }
    public bool Animated{ get => animated; }
    public float BotRandomPercentage{ get => botRandomPercentage; }
}
