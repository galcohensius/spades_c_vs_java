﻿using common.controllers;

namespace cardGames.views
{
    [System.Serializable]

    public class QAEnvSelectionPanelView : MonoBehaviour
    {
        [SerializeField] private TMP_Text m_currentEnvText;
        [SerializeField] private UnityEvent m_onLoginRequired;

        private int m_envIndex;

        private void Start()
        {
            m_envIndex = StateController.Instance.GetQAEnvironment();
            switch (m_envIndex)
            {
                case 2:
                    m_currentEnvText.text = "Current Env: 2";
                    break;
                default:
                    m_currentEnvText.text = "Current Env: 1";
                    break;
            }
        }

        public void btn_Env1Click()
        {
            if (m_envIndex == 1)
                return;

            m_currentEnvText.text = "Current Env: 1";
            StateController.Instance.SetQAEnvironment(1);

            m_onLoginRequired?.Invoke();
        }

        public void btn_Env2Click()
        {
            if (m_envIndex == 2)
                return;

            m_currentEnvText.text = "Current Env: 2";
            StateController.Instance.SetQAEnvironment(2);

            m_onLoginRequired?.Invoke();
        }

    }

}
