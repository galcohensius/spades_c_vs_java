﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using common.models;
using System;
using cardGames.models;

namespace Tests.cardGames.models
{
    public class CardTests
    {
        // A Test behaves as an ordinary method
        [TestCase("2C", 2, 0)]
        [TestCase("0D", 10, 1)]
        [TestCase("JH", 11, 2)]
        [TestCase("QS", 12, 3)]
        [TestCase("*J", 15, 4)]
        public void CreateCardFromString_Pass(string cardCode, int rank, int suit)
        {
            Card card = new Card(cardCode);
            Assert.True((int)card.Rank == rank);
            Assert.True((int)card.Suit == suit);
        }

        /// <summary>
        /// for the time being and for Spades only - jokers are treated as Spades suit and not as Jokers suit.
        /// </summary>
        /// <param name="invalidCode"></param>
        [TestCase("1C")]
        [TestCase("12D")]
        [TestCase("*K")]
        [TestCase("qm")]
        [TestCase("73")]
        [TestCase("-5H")]
        [TestCase("H")]
        [TestCase("8")]
        [TestCase("")]
        [TestCase(null)]
        public void CreateCardFromString_Fail (string invalidCode)
        {
            Assert.Throws(typeof(Exception), () => new Card(invalidCode));
        }


    }
}
