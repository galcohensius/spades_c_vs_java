﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using common.models;
using System;
using cardGames.models;

namespace Tests.cardGames.models
{
    public class CardsListTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void AddCardsFrom_StringArray_Pass()
        {
            CardsList cardslist = new CardsList();
            string[] cards = new string[] { "2C", "0D", "JH", "QS", "*J" };
            Assert.DoesNotThrow(() => cardslist.SetCards(cards));
            Assert.True(cardslist[0].Rank == Card.RankType.Two && cardslist[0].Suit == Card.SuitType.Clubs);
            Assert.True(cardslist[1].Rank == Card.RankType.Ten && cardslist[1].Suit == Card.SuitType.Diamonds);
            Assert.True(cardslist[2].Rank == Card.RankType.Jack && cardslist[2].Suit == Card.SuitType.Hearts);
            Assert.True(cardslist[3].Rank == Card.RankType.Queen && cardslist[3].Suit == Card.SuitType.Spades);
            Assert.True(cardslist[4].Rank == Card.RankType.JokerSmall && cardslist[4].Suit == Card.SuitType.Joker);
        }

        [Test]
        public void AddCardsFrom_StringArray_Fail()
        {
            CardsList cardslist = new CardsList();
            string[] cards = new string[] { "1C", "12D", "*K", "qm", "73" };
            Assert.Throws(typeof(System.Exception), () => cardslist.SetCards(cards));

            cards = new string[] { "1" };
            Assert.Throws(typeof(System.Exception), () => cardslist.SetCards(cards));

            cards = new string[] { "" };
            Assert.Throws(typeof(System.Exception), () => cardslist.SetCards(cards));
        }

        [Test]
        public void AddCardsFrom_SingleString_Pass()
        {
            CardsList cardslist = null;
            Assert.DoesNotThrow(() => cardslist = new CardsList("2C0DJHQS*J"));
            Assert.True(cardslist[0].Rank == Card.RankType.Two && cardslist[0].Suit == Card.SuitType.Clubs);
            Assert.True(cardslist[1].Rank == Card.RankType.Ten && cardslist[1].Suit == Card.SuitType.Diamonds);
            Assert.True(cardslist[2].Rank == Card.RankType.Jack && cardslist[2].Suit == Card.SuitType.Hearts);
            Assert.True(cardslist[3].Rank == Card.RankType.Queen && cardslist[3].Suit == Card.SuitType.Spades);
            Assert.True(cardslist[4].Rank == Card.RankType.JokerSmall && cardslist[4].Suit == Card.SuitType.Joker);
            Debug.Log(cardslist.ToString());
        }

        [Test]
        public void AddCardsFrom_SingleString_Fail()
        {
            CardsList cardslist;
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("1"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("1C"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("12D"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("*K"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("qm"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("73"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("2C0"));
            Assert.Throws(typeof(Exception), () => cardslist = new CardsList("-2C0D"));
        }


        [Test]
        public void CardsBySuit_HighToLow_Sorting_Collections()
        {
            CardsList cardslist = new CardsList("2C4C5H8D");

            List<Card.SuitType> suitTypes = new List<Card.SuitType>();
            suitTypes.Add(Card.SuitType.Clubs);
            suitTypes.Add(Card.SuitType.Hearts);
            suitTypes.Add(Card.SuitType.Spades);

            cardslist = cardslist.CardsBySuits(suitTypes);
            Assert.AreEqual("5H4C2C" ,cardslist.ToString());



            cardslist = new CardsList("2C9S$J8D");

            suitTypes = new List<Card.SuitType>();
            suitTypes.Add(Card.SuitType.Joker);
            suitTypes.Add(Card.SuitType.Diamonds);
            suitTypes.Add(Card.SuitType.Hearts);

            cardslist = cardslist.CardsBySuits(suitTypes);
            Assert.AreEqual("$J8D", cardslist.ToString());
        }

        [Test]
        public void CardsBySuit_HighToLow_Sorting_Params()
        {
            CardsList cardslist = new CardsList("2C4C5H8D");
            cardslist = cardslist.CardsBySuits(Card.SuitType.Hearts, Card.SuitType.Spades);
            Assert.AreEqual("5H", cardslist.ToString());

            cardslist = new CardsList("2C4C5H8D");
            cardslist = cardslist.CardsBySuits(Card.SuitType.Clubs, Card.SuitType.Joker);
            Assert.AreEqual("4C2C", cardslist.ToString());

            cardslist = new CardsList("2C4C5H8D$JAS");
            cardslist = cardslist.CardsBySuits(Card.SuitType.Clubs, Card.SuitType.Joker, Card.SuitType.Diamonds, Card.SuitType.Spades, Card.SuitType.Hearts);
            Assert.AreEqual("$JAS5H8D4C2C", cardslist.ToString());
        }
    

        [Test]
        public void HasRank_TestPass()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            Assert.True(cardslist.HasRank(Card.RankType.Two));
            Assert.True(cardslist.HasRank(Card.RankType.Ten));
            Assert.True(cardslist.HasRank(Card.RankType.Jack));
            Assert.True(cardslist.HasRank(Card.RankType.Queen));
            Assert.True(cardslist.HasRank(Card.RankType.JokerSmall));
        }

        [Test]
        public void HasRank_TestFail()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            Assert.False(cardslist.HasRank(Card.RankType.Four));
            Assert.False(cardslist.HasRank(Card.RankType.King));
            Assert.False(cardslist.HasRank(Card.RankType.Seven));
            Assert.False(cardslist.HasRank(Card.RankType.Three));
            Assert.False(cardslist.HasRank(Card.RankType.JokerBig));
        }

        [Test]
        public void GetCard_TestPass()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            Assert.IsNotNull(cardslist.getCard(Card.RankType.Two, Card.SuitType.Clubs));
            Assert.IsNotNull(cardslist.getCard(Card.RankType.Ten, Card.SuitType.Diamonds));
            Assert.IsNotNull(cardslist.getCard(Card.RankType.Jack, Card.SuitType.Hearts));
            Assert.IsNotNull(cardslist.getCard(Card.RankType.Queen, Card.SuitType.Spades));
            Assert.IsNotNull(cardslist.getCard(Card.RankType.JokerSmall, Card.SuitType.Joker));
        }

        [Test]
        public void GetCard_TestFail()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            Assert.IsNull(cardslist.getCard(Card.RankType.Seven, Card.SuitType.Clubs));
            Assert.IsNull(cardslist.getCard(Card.RankType.Ten, Card.SuitType.Clubs));
            Assert.IsNull(cardslist.getCard(Card.RankType.Queen, Card.SuitType.Hearts));
            Assert.IsNull(cardslist.getCard(Card.RankType.Seven, Card.SuitType.Joker));
            Assert.IsNull(cardslist.getCard(Card.RankType.JokerBig, Card.SuitType.Clubs));
        }

        [Test]
        public void SortHighToLowRankFirstSpadesFirst_Test()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            cardslist.SortHighToLowRankFirstSpadesFirst();
            Assert.AreEqual("KSQS0S5S4S*JKDKCQCJH0D2C", cardslist.ToString());
        }

        [Test]
        public void SortHighToLowSuitFirst_Test()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            cardslist.SortHighToLowSuitFirst();
            Assert.AreEqual("*JKSQS0S5S4SJHKD0DKCQC2C", cardslist.ToString());
        }

        [Test]
        public void SortHighToLowRankFirst_Test()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            cardslist.SortHighToLowRankFirst();
            Assert.AreEqual("*JKSKDKCQSQCJH0S0D5S4S2C", cardslist.ToString());
        }

        [Test]
        public void CountGreaterThan_Test_Pass()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.AreEqual(9, cardslist.CountGreaterThan(Card.RankType.Six));
            Assert.AreEqual(0, cardslist.CountGreaterThan(Card.RankType.JokerSmall));
            Assert.AreEqual(4, cardslist.CountGreaterThan(Card.RankType.Queen));
            Assert.AreEqual(11, cardslist.CountGreaterThan(Card.RankType.Two));
        }

        [Test]
        public void CountGreaterThan_Test_Fail()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.AreNotEqual(10, cardslist.CountGreaterThan(Card.RankType.Six));
            Assert.AreNotEqual(1, cardslist.CountGreaterThan(Card.RankType.JokerSmall));
            Assert.AreNotEqual(5, cardslist.CountGreaterThan(Card.RankType.Queen));
            Assert.AreNotEqual(12, cardslist.CountGreaterThan(Card.RankType.Two));
        }

        [Test]
        public void CountLowerThan_Test_Pass()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.AreEqual(3, cardslist.CountLowerThan(Card.RankType.Six));
            Assert.AreEqual(11, cardslist.CountLowerThan(Card.RankType.JokerSmall));
            Assert.AreEqual(6, cardslist.CountLowerThan(Card.RankType.Queen));
            Assert.AreEqual(0, cardslist.CountLowerThan(Card.RankType.Two));
        }

        [Test]
        public void CountLowerThan_Test_Fail()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.AreNotEqual(10, cardslist.CountLowerThan(Card.RankType.Six));
            Assert.AreNotEqual(1, cardslist.CountLowerThan(Card.RankType.JokerSmall));
            Assert.AreNotEqual(5, cardslist.CountLowerThan(Card.RankType.Queen));
            Assert.AreNotEqual(12, cardslist.CountLowerThan(Card.RankType.Two));
        }

        [Test]
        public void CountSuit_Test_Pass()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.AreEqual(3, cardslist.CountSuit(Card.SuitType.Clubs));
            Assert.AreEqual(2, cardslist.CountSuit(Card.SuitType.Diamonds));
            Assert.AreEqual(1, cardslist.CountSuit(Card.SuitType.Hearts));
            Assert.AreEqual(5, cardslist.CountSuit(Card.SuitType.Spades));
            Assert.AreEqual(1, cardslist.CountSuit(Card.SuitType.Joker));

            cardslist = new CardsList("2CQC0D4SKD0S5SQSKCKS");
            Assert.AreEqual(0, cardslist.CountSuit(Card.SuitType.Hearts));
            Assert.AreEqual(0, cardslist.CountSuit(Card.SuitType.Joker));
        }

        [Test]
        public void CountSuit_Test_Fail()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.AreNotEqual(4, cardslist.CountSuit(Card.SuitType.Clubs));
            Assert.AreNotEqual(1, cardslist.CountSuit(Card.SuitType.Diamonds));
            Assert.AreNotEqual(2, cardslist.CountSuit(Card.SuitType.Hearts));
            Assert.AreNotEqual(4, cardslist.CountSuit(Card.SuitType.Spades));
            Assert.AreNotEqual(2, cardslist.CountSuit(Card.SuitType.Joker));

            cardslist = new CardsList("2CQC0D4SKD0S5SQSKCKS");
            Assert.AreNotEqual(1, cardslist.CountSuit(Card.SuitType.Hearts));
            Assert.AreNotEqual(-6, cardslist.CountSuit(Card.SuitType.Joker));
        }

        [Test]
        public void HasSingleSuit_Test_Pass()
        {
            CardsList cardslist = new CardsList("2CQC0D4SJHKD0S5SQSKC*JKS");
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Clubs));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Diamonds));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Hearts));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Spades));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Joker));


            cardslist = new CardsList("2D4D0DQD");
            Assert.True(cardslist.HasSingleSuit(Card.SuitType.Diamonds));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Clubs));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Joker));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Spades));

            cardslist = new CardsList("*J$J");
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Diamonds));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Clubs));
            Assert.True(cardslist.HasSingleSuit(Card.SuitType.Joker));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Spades));

            cardslist = new CardsList("3H5HJHQH");
            Assert.True(cardslist.HasSingleSuit(Card.SuitType.Hearts));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Clubs));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Joker));
            Assert.False(cardslist.HasSingleSuit(Card.SuitType.Spades));
        }

        //assuming single suit
        [Test]
        public void GetWeakestAboveRank_HasCard()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Four), cardslist.GetWeakestAboveRank(Card.RankType.Two));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Four), cardslist.GetWeakestAboveRank(Card.RankType.Three));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Queen), cardslist.GetWeakestAboveRank(Card.RankType.Jack));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Ten), cardslist.GetWeakestAboveRank(Card.RankType.Nine));
        }

        //assuming single suit
        [Test]
        public void GetWeakestAboveRank_NoCard()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            Assert.AreEqual(null, cardslist.GetWeakestAboveRank(Card.RankType.Queen));
            Assert.AreEqual(null, cardslist.GetWeakestAboveRank(Card.RankType.King));
            Assert.AreEqual(null, cardslist.GetWeakestAboveRank(Card.RankType.Ace));
        }

        [Test]
        public void GetWeakest_Test()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Two), cardslist.GetWeakest());

            cardslist = new CardsList("3H4D0DQD");
            Assert.AreEqual(new Card(Card.SuitType.Hearts, Card.RankType.Three), cardslist.GetWeakest());

            cardslist = new CardsList("QSQCQHQD");
            Assert.AreEqual(new Card(Card.SuitType.Clubs, Card.RankType.Queen), cardslist.GetWeakest());

            cardslist = new CardsList();
            Assert.AreEqual(null, cardslist.GetWeakest());
        }

        [Test]
        public void GetSecondWeakest_Test()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Four), cardslist.GetSecondWeakestCard());

            cardslist = new CardsList("3H4D0DQD");
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Four), cardslist.GetSecondWeakestCard());

            cardslist = new CardsList("QSQCQHQD");
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Queen), cardslist.GetSecondWeakestCard());

            cardslist = new CardsList("AD");
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Ace), cardslist.GetSecondWeakestCard());

            cardslist = new CardsList();
            Assert.AreEqual(null, cardslist.GetSecondWeakestCard());
        }

        //assuming single suit
        [Test]
        public void GetStrongestBelowRank_Test()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            Assert.AreEqual(null, cardslist.GetStrongestBelowRank(Card.RankType.Two));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Two), cardslist.GetStrongestBelowRank(Card.RankType.Three));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Two), cardslist.GetStrongestBelowRank(Card.RankType.Four));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Queen), cardslist.GetStrongestBelowRank(Card.RankType.Ace));
            Assert.AreEqual(new Card(Card.SuitType.Diamonds, Card.RankType.Ten), cardslist.GetStrongestBelowRank(Card.RankType.Queen));
        }

        //assuming single suit
        [Test]
        public void GetAllCardsAboveRank_Test()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            CollectionAssert.AreEquivalent(new CardsList("4D0DQD"), cardslist.GetAllCardsAboveRank(Card.RankType.Two));
            CollectionAssert.AreEquivalent(new CardsList("0D4DQD"), cardslist.GetAllCardsAboveRank(Card.RankType.Three));
            CollectionAssert.AreEquivalent(new CardsList("QD0D"), cardslist.GetAllCardsAboveRank(Card.RankType.Four));
            Assert.AreEqual(null, cardslist.GetAllCardsAboveRank(Card.RankType.Ace));
            CollectionAssert.AreEquivalent(new CardsList("QD"), cardslist.GetAllCardsAboveRank(Card.RankType.Jack));
        }

        //assuming single suit
        [Test]
        public void GetStrongestBelowRankBelowSecondHighest_Test()
        {
            CardsList cardslist = new CardsList("2D4D0DQD");
            var card = cardslist.GetStrongestBelowRankBelowSecondHighest(Card.RankType.Queen, Card.SuitType.Diamonds, "no");
            Assert.AreEqual(new Card("0D"), card);

            card = cardslist.GetStrongestBelowRankBelowSecondHighest(Card.RankType.Two, Card.SuitType.Diamonds, "no");
            Assert.AreEqual(null, card);

            card = cardslist.GetStrongestBelowRankBelowSecondHighest(Card.RankType.Queen, Card.SuitType.Diamonds, "jokers");
            Assert.AreEqual(new Card("0D"), card);

            cardslist = new CardsList("2C4H0SQD$J");
            card = cardslist.GetStrongestBelowRankBelowSecondHighest(Card.RankType.JokerBig, Card.SuitType.Diamonds, "jokers");
            Assert.AreEqual(new Card("QD"), card);

            card = cardslist.GetStrongestBelowRankBelowSecondHighest(Card.RankType.King, Card.SuitType.Diamonds, "no");
            Assert.AreEqual(new Card("QD"), card);
        }

        [Test]
        public void GetStrongestBelowOrEqualRank_Test()
        {
            CardsList cardslist = new CardsList("3S3C4C4H0SQD$J");
            var card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.JokerBig);
            Assert.AreEqual(new Card("$J"), card);

            card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.King);
            Assert.AreEqual(new Card("QD"), card);

            card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.Ten);
            Assert.AreEqual(new Card("0S"), card);

            card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.Three);
            Assert.AreEqual(new Card("3S"), card);

            card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.Four);
            Assert.AreEqual(new Card("4H"), card);

            card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.Five);
            Assert.AreEqual(new Card("4H"), card);

            card = cardslist.GetStrongestBelowOrEqualRank(Card.RankType.Two);
            Assert.AreEqual(null, card);
        }

        [Test]
        public void GetMediumCard_Test()
        {
            CardsList cardslist = new CardsList("3S3C4C4H0SQD$J");
            var card = cardslist.GetMediumCard();
            Assert.AreEqual(new Card("0S"), card);

            cardslist = new CardsList("3S3C4C4H0SQD");
            card = cardslist.GetMediumCard();
            Assert.AreEqual(new Card("0S"), card);

            cardslist = new CardsList("4H4C4S");
            card = cardslist.GetMediumCard();
            Assert.AreEqual(new Card("4H"), card);

            cardslist = new CardsList();
            card = cardslist.GetMediumCard();
            Assert.AreEqual(null, card);
        }

        [Test]
        public void RemoveCardsFromEnd_Test()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            var removedCards = cardslist.RemoveCardsFromEnd(0);
            CollectionAssert.IsEmpty(removedCards);

            removedCards = cardslist.RemoveCardsFromEnd(1);
            CollectionAssert.AreEquivalent(new CardsList("*J"),removedCards);
            Assert.True(cardslist.Count == 4);

            removedCards = cardslist.RemoveCardsFromEnd(3);
            CollectionAssert.AreEquivalent(new CardsList("0DJHQS"), removedCards);
            Assert.True(cardslist.Count == 1);

            cardslist = new CardsList("2C0DJHQS*J");
            removedCards = cardslist.RemoveCardsFromEnd(8);
            CollectionAssert.IsEmpty(cardslist);
            CollectionAssert.AreEquivalent(new CardsList("2C0DJHQS*J"), removedCards);
        }

        [Test]
        public void RemoveCardsFromBeginning_Test()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            var removedCards = cardslist.RemoveCardsFromBeginning(0);
            CollectionAssert.IsEmpty(removedCards);

            removedCards = cardslist.RemoveCardsFromBeginning(1);
            CollectionAssert.AreEquivalent(new CardsList("2C"), removedCards);
            Assert.True(cardslist.Count == 4);

            removedCards = cardslist.RemoveCardsFromBeginning(3);
            CollectionAssert.AreEquivalent(new CardsList("0DJHQS"), removedCards);
            Assert.True(cardslist.Count == 1);

            cardslist = new CardsList("2C0DJHQS*J");
            removedCards = cardslist.RemoveCardsFromBeginning(8);
            CollectionAssert.IsEmpty(cardslist);
            CollectionAssert.AreEquivalent(new CardsList("2C0DJHQS*J"), removedCards);
        }

        [Test]
        public void RemoveSomeCards_Test()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");
            CardsList cardsToRemoveList = new CardsList();
            cardslist.RemoveSomeCards(cardsToRemoveList);
            CollectionAssert.AreEqual(new CardsList("2C0DJHQS*J"), cardslist);

            cardsToRemoveList = new CardsList("3C4D5H");
            cardslist.RemoveSomeCards(cardsToRemoveList);
            CollectionAssert.AreEqual(new CardsList("2C0DJHQS*J"), cardslist);

            cardsToRemoveList = new CardsList("2C*JJH");
            cardslist.RemoveSomeCards(cardsToRemoveList);
            CollectionAssert.AreEquivalent(new CardsList("0DQS"), cardslist);

            cardsToRemoveList = new CardsList("0DAC");
            cardslist.RemoveSomeCards(cardsToRemoveList);
            CollectionAssert.AreEquivalent(new CardsList("QS"), cardslist);
        }

        [Test]
        public void MoveCardToBeginning_Test()
        {
            CardsList cardslist = new CardsList("2C0DJHQS*J");

            cardslist.MoveCardToBeginning(new Card("$J"));
            CollectionAssert.AreEqual(new CardsList("2C0DJHQS*J"), cardslist);

            cardslist.MoveCardToBeginning(new Card("JH"));
            CollectionAssert.AreEqual(new CardsList("JH2C0DQS*J"), cardslist);

            cardslist.MoveCardToBeginning(new Card("JH"));
            CollectionAssert.AreEqual(new CardsList("JH2C0DQS*J"), cardslist);

            cardslist.MoveCardToBeginning(new Card("2C"));
            CollectionAssert.AreEqual(new CardsList("2CJH0DQS*J"), cardslist);

            cardslist.MoveCardToBeginning(new Card("AC"));
            CollectionAssert.AreEqual(new CardsList("2CJH0DQS*J"), cardslist);
        }

        [TestCase("2C0DJHQS*J",3)]
        [TestCase("2C4D5H9S", 0)]
        [TestCase("JSQHACKD$J", 5)]
        [TestCase("", 0)]
        public void HonorsCount_Test(string deckString, int honors)
        {
            CardsList cardslist = new CardsList(deckString);
            Assert.AreEqual(honors, cardslist.HonorsCount);
        }


        [TestCase("2C0DJHQS*J", "")]
        [TestCase("2C4D5H9S", "$J2C5H")]
        [TestCase("JSQHACKD$J", "ASKDQCJH")]
        [TestCase("", "ASKDQCJH")]
        public void OperatorPlus_Test(string deckString1, string deckString2)
        {
            CardsList cardslist1 = new CardsList(deckString1);
            CardsList cardslist2 = new CardsList(deckString2);
            CardsList merge = cardslist1 + cardslist2;

            CollectionAssert.AreEqual(new CardsList(deckString1 + deckString2), cardslist1);
            CollectionAssert.AreEqual(new CardsList(deckString1 + deckString2), merge);
        }

        [TestCase("2C0DJHQS*J")]
        [TestCase("2C4D5H9S")]
        [TestCase("JSQHACKD$J")]
        [TestCase("")]
        public void ToString_Test(string deckString)
        {
            CardsList cardslist = new CardsList(deckString);
            Assert.AreEqual(deckString, cardslist.ToString());

        }
    }
}
