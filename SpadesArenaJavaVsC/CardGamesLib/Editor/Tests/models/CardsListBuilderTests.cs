﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using common.models;
using System.Text;
using cardGames.models;

namespace Tests.cardGames.models
{
    public class CardsListBuilderTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void FullDeckNoJokers_Test()
        {
            StringBuilder buildString = new StringBuilder();
            //manually build deck
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "C"));
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "D"));
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "H"));
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "S"));
            for (int i = 2; i <= 9; i++)
            {
                buildString.Append(i + "C");
                buildString.Append(i + "D");
                buildString.Append(i + "H");
                buildString.Append(i + "S");
            }
            CardsList fullDeckNoJokers = new CardsList(buildString.ToString());

            CollectionAssert.AreEquivalent(fullDeckNoJokers, CardsListBuilder.FullDeckNoJokers());
        }

        [TestCase("C", Card.SuitType.Clubs)]
        [TestCase("D", Card.SuitType.Diamonds)]
        [TestCase("H", Card.SuitType.Hearts)]
        [TestCase("S", Card.SuitType.Spades)]
        public void SingleSuit_Test(string suit, Card.SuitType suitType)
        {
            StringBuilder buildString = new StringBuilder();
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", suit));
            for (int i = 2; i <= 9; i++)
            {
                buildString.Append(string.Format("{0}{1}", i, suit));
            }
            CardsList singleSuit = new CardsList(buildString.ToString());
            CollectionAssert.AreEquivalent(singleSuit, CardsListBuilder.SingleSuit(suitType));
        }

        [Test]
        public void DeckWithJokers_52Cards_Test()
        {
            StringBuilder buildString = new StringBuilder();
            //manually build deck
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "C"));
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "D"));
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "H"));
            buildString.Append(string.Format("A{0}J{0}Q{0}K{0}0{0}", "S"));
            for (int i = 2; i <= 9; i++)
            {
                buildString.Append(i + "C");
                buildString.Append(i + "S");
            }

            //these suits have 12 cards starting at 3
            for (int i = 3; i <= 9; i++)
            {
                buildString.Append(i + "D");
                buildString.Append(i + "H");
            }

            Debug.LogWarning("This methods assigns Jokers to Spades suit and NOT to the Jokers suit");
            CardsList deckWithJokers = new CardsList(buildString.ToString());
            //building through string does not allow jokers not in the jokers suit
            deckWithJokers.Add(new Card(Card.SuitType.Spades, Card.RankType.JokerSmall));
            deckWithJokers.Add(new Card(Card.SuitType.Spades, Card.RankType.JokerBig));

            CollectionAssert.AreEquivalent(deckWithJokers, CardsListBuilder.FullDeckWithJokers());
        }
    }
}
