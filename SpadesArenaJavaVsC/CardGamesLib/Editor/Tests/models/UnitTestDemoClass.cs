﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using SimpleJSON;
using cardGames.models.contests;
using System.Collections.Generic;
using System;

namespace Tests.cardGames.models
{
    public class UnitTestDemoClass
    {
        private JSONNode jsonNode;

        [Test]
        public void ReadJson2()
        {

            var jsonTextFile = Resources.Load<TextAsset>("Contests/FakeContestData");
            jsonNode = JSON.Parse(jsonTextFile.text);
            string testData = jsonNode["contests_lists"][2]["future"].ToString();
            Debug.Log("<color=green>conests_test: </color>" + testData);

            int listSize = jsonNode["contests_lists"][0]["completed"].Count;
            Debug.Log("id: " + jsonNode["contest"][2]["id"]);

            string name = "contest";
            int index = 0;
            string type = "completed";

            Debug.Log("<color=blue>the start_date is:</color>" + jsonNode["contest"][0]["start_date"]);
            Debug.Log("<color=blue>the start_date is:</color>" + jsonNode["contest"][1]["start_date"]);
        }

        [Test]
        public void GetPaytableList()
        {
            Debug.Log("yeah");
            // JSONArray arr = jsonNode[name].AsArray;
            //  List<ContestPaytableLine> list = new List<ContestPaytableLine>();
            var jsonTextFile = Resources.Load<TextAsset>("Contests/FakeContestData");
            jsonNode = JSON.Parse(jsonTextFile.text);
            string testData = jsonNode["contest"][0]["contest_paytable_line"][5]["min_rank"].ToString();
            Debug.Log(testData);
        }

        [Test]
        public void TrimStr()
        {
            char[] separators = new char[] { ',' };
            string s = "100,100,500";
            string[] temp = s.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            s = String.Join("", temp);
            Debug.Log(s);

            String header = "* A, Short String.,, *";
            Debug.Log(header);
            Debug.Log(header.Trim(new Char[] { ' ', '*', '.', ',' }));
        }

        [Test]
        public void ParsePaytable(JSONNode node, int index)
        {

            Debug.Log(node["contest"][index]["contest_paytable_line"][0]["min_rank"]);
            // string type = "contest_paytable_line";
            /* JSONNode jNode = node[index]["contest_paytable_line"][0];

             for (int j = 0; j < node.Count; j++)
             {
                 ContestPaytable contestPaytable = new ContestPaytable();

                 contestPaytable.PayLines[index].MinRank = node["min_rank"];
                 Debug.Log("<color=green>the min_rank is:</color>" + contestPaytable.PayLines[index].MinRank);

                 contestPaytable.PayLines[index].MaxRank = node["max_rank"];
                 Debug.Log("<color=green>the max_rank is:</color>" + contestPaytable.PayLines[index].MaxRank);

                 contestPaytable.PayLines[index].Prize = node["prize"];
                 Debug.Log("<color=green>the prize is:</color>" + contestPaytable.PayLines[index].Prize);
             }
             */
            // *************************
            // *************************

        }

        [UnityTest]
        public string ReadJson()
        {
            return "yes";
        }

    }
}
