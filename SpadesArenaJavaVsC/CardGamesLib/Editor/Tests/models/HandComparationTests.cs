﻿using System.Collections;
using System.Collections.Generic;
using cardGames.models;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using System.Linq;

namespace Tests.cardGames.models
{
    //checks if two cardslists of the same length with the same cards are considered equal if they have the same cards but not in the same
    //order using the custom GetHashCode
    public class HandComparationTests
    {
        [Test]
        public void HandComparationTests_Simple_Passes()
        {
            CardsList cl1 = new CardsList("AC2D3H4S5C6D7H8S9C0D");
            CardsList cl2 = new CardsList(cl1);
            cl2.Reverse();

            Assert.AreEqual(cl1.GetHashCode(), cl2.GetHashCode());


            /////////////////////////////////////////////////////////


            cl1 = new CardsList("4C5D6H7S8C9D0HJSQCKD");
            cl2 = new CardsList(cl1);
            cl2.Reverse();

            Assert.AreEqual(cl1.GetHashCode(), cl2.GetHashCode());

            //////////////////////////////////////////////////////////

            cl1 = new CardsList("JCJDQHQSQCQDKHKSKCKD");
            cl2 = new CardsList(cl1);
            cl2.Reverse();

            Assert.AreEqual(cl1.GetHashCode(), cl2.GetHashCode());

        }

        [Test]
        public void HandComparationTests_Simple_Fails()
        {
            CardsList cl1 = new CardsList("AC2D3H4S5C6D7H8S9C0D");
            CardsList cl2 = new CardsList("2C3D4H5S6C7D8H9S0CJD");
            cl2.Reverse();

            Assert.AreNotEqual(cl1.GetHashCode(), cl2.GetHashCode());


            /////////////////////////////////////////////////////////


            cl1 = new CardsList("4C5D6H7S8C9D0HJSQCKD");
            cl2 = new CardsList("4H5C6S7D8H9C0SJDQHKC");
            cl2.Reverse();

            Assert.AreNotEqual(cl1.GetHashCode(), cl2.GetHashCode());

            //////////////////////////////////////////////////////////

            cl1 = new CardsList("JCJDQHQSQCQDKHKSKCKD");
            cl2 = new CardsList(cl1.ToString() + "AC");
            cl2.Reverse();

            Assert.AreNotEqual(cl1.GetHashCode(), cl2.GetHashCode());
        }
    }
}
