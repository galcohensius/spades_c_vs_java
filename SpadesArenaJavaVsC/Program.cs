﻿using System;
using System.Collections.Generic;
using cardGames.models;
using spades.models;
using SpadesAI;
using SpadesAI.model;

namespace SpadesArenaJavaVsC
{
    internal class Program
    {
        private static ServerNew server;

        private static void Main()
        {
            server = new ServerNew();
            server.ClientConnected += clientConnected;
            server.ClientDisconnected += clientDisconnected;
            server.MessageReceived += messageReceived;
            server.start();

            Console.WriteLine("SERVER STARTED: " + DateTime.Now);
            Console.ReadKey();
        }

        private static void clientConnected(Client c){
        }

        private static void clientDisconnected(Client c){
            Console.WriteLine("DISCONNECTED: " + c);
        }


        /** Commands to c# bots:
           void StartMatch()
           void StartRound(int roundNum, Position pos, CardsList hand)
           int MakeBid(Position pos)
           void SetBid(Position pos, int bid)
           Card PlayMove(Position pos)
           void SetMove(Position pos, Card card)
           void EndRound(Dictionary<Position,int> TotalPoints,Dictionary<Position,int> TotalBags)
        */
        private static void messageReceived(Client myClient)
        {
            string[] messageToArray = myClient.lastMsg.Split(' ');
            Position myPosition;
            switch (myClient.lastMsg)
            {
                case { } msg when msg.StartsWith("MATCH"):
                    // StartMatch(LoseConditionPoints, WinConditionPoints, Mode, Variation, BagsToIncurPenalty, BagsPenalty)
                    // e.g. "SM -100 100 Partners Classic 4 40"

                    AIMatchData matchData;
                    if (messageToArray.Length == 7)
                    {
                        int.TryParse(messageToArray[1], out int loseConditionPoints);
                        int.TryParse(messageToArray[2], out int winConditionPoints);
                        string playingMode = messageToArray[3];
                        string playingVariant = messageToArray[4];
                        int.TryParse(messageToArray[5], out int bagsToIncurPenalty);
                        int.TryParse(messageToArray[6], out int bagsPenalty);

                        matchData = new AIMatchData
                        {
                            LoseConditionPoints = loseConditionPoints,
                            WinConditionPoints = winConditionPoints,
                            Mode = playingMode == "SOLO" ? PlayingMode.Solo : PlayingMode.Partners,
                            BagsToIncurPenalty = bagsToIncurPenalty,
                            BagsPenalty = bagsPenalty
                        };
                        switch (playingVariant)
                        {
                            default:
                                matchData.Variation = PlayingVariant.Classic;
                                break;
                            case "JOKERS":
                                matchData.Variation = PlayingVariant.Jokers;
                                break;
                            case "MIRROR":
                                matchData.Variation = PlayingVariant.Mirror;
                                break;
                            case "WHIZ":
                                matchData.Variation = PlayingVariant.Whiz;
                                break;
                            case "SUICIDE":
                                matchData.Variation = PlayingVariant.Suicide;
                                break;
                        }
                        
                    }
                    else
                    {
                        matchData = new AIMatchData
                        {
                            LoseConditionPoints = -100,
                            WinConditionPoints = 180,
                            Mode = PlayingMode.Partners,
                            Variation = PlayingVariant.Classic,
                            BagsToIncurPenalty = 8,
                            BagsPenalty = 80
                        };
                    }

                    switch (matchData.Variation)
                    {
                        case PlayingVariant.Classic:
                            myClient.spadesBrain = matchData.Mode == PlayingMode.Partners 
                                ? new SpadesBrainImpl(BrainType.ClassicV4) // TODO: change back to V4
                                : new SpadesBrainImpl(BrainType.ClassicV3);
                            break;
                        case PlayingVariant.Jokers:
                            myClient.spadesBrain = new SpadesBrainImpl(BrainType.Jokers);
                            break;
                        case PlayingVariant.Mirror:
                            myClient.spadesBrain = new SpadesBrainImpl(BrainType.Mirror);
                            break;
                        case PlayingVariant.Whiz:
                            myClient.spadesBrain = new SpadesBrainImpl(BrainType.Whiz);
                            break;
                        case PlayingVariant.Suicide:
                            myClient.spadesBrain = new SpadesBrainImpl(BrainType.Suicide);
                            break;
                        case PlayingVariant.Royale:
                            myClient.spadesBrain = new SpadesBrainImpl(BrainType.RoyaleBot);
                            break;
                        case PlayingVariant.CallBreak:
                            myClient.spadesBrain = new SpadesBrainImpl(BrainType.CallBreak);
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    
                    myClient.spadesBrain.StartMatch(matchData);

                    server.sendMessageToClient(myClient, "MATCH ");
                    break;


                // StartRound(int roundNum, Position pos, CardsList hand)
                case { } msg when msg.StartsWith("ROUND"):
//                    int.TryParse(messageToArray[1], out int roundNum); // roundNum
                    myPosition = GetPositionFromString(messageToArray[2]);
                    CardsList myHand = new CardsList(messageToArray[3]);

                    myClient.spadesBrain.StartRound(new SpadesTable(), new Dictionary<Position, int>
                    {
                        {Position.South, 3},
                        {Position.East, 3},
                        {Position.North, 3},
                        {Position.West, 3}
                    },
                    new Dictionary<Position, BrainType>
                    {
                        {Position.South, BrainType.ClassicV4}, //TODO: change to the correct brain-type
                        {Position.East, BrainType.ClassicV4},
                        {Position.North, BrainType.ClassicV4},
                        {Position.West, BrainType.ClassicV4}
                    });

                    myClient.spadesBrain.SetHand(myPosition, myHand);

                    server.sendMessageToClient(myClient, "ROUND " + myPosition);
                    break;


                // MakeBid(Position pos)
                case { } msg when msg.StartsWith("BID"):
                    myPosition = GetPositionFromString(messageToArray[1]);
                    int myBid = myClient.spadesBrain.MakeBid(myPosition, new SpadesPlayerRoundData());
                    server.sendMessageToClient(myClient, "BID " + myBid + " " + myPosition); 
                    break;


                // SetBid(Position pos, int bid)
                case { } msg when msg.StartsWith("SB"):
                    Position bidderPos = GetPositionFromString(messageToArray[1]);
                    int.TryParse(messageToArray[2], out int herBid);
                    myClient.spadesBrain.SetBid(bidderPos, herBid);
                    server.sendMessageToClient(myClient, "SB " + bidderPos +" "+ herBid);
                    break;


                // Card PlayMove(Position pos)
                case { } msg when msg.StartsWith("MOVE"):
                    myPosition = GetPositionFromString(messageToArray[1]);
                    Card playedCard = myClient.spadesBrain.PlayMove(myPosition, new SpadesPlayerRoundData());
                    myClient.spadesBrain.SetMove(myPosition, playedCard);
                    server.sendMessageToClient(myClient, "MOVE " + myPosition + " " + playedCard);
                    break;

                // SetMove(Position pos, Card card)
                case { } msg when msg.StartsWith("SM"):
                    Position herPosition = GetPositionFromString(messageToArray[1]);
                    Card herCard = new Card(messageToArray[2]);
                    myClient.spadesBrain.SetMove(herPosition, herCard);
                    server.sendMessageToClient(myClient, "SM " + herPosition + " " + herCard);
                    break;

                // TRICK(Position trickWinner)
                case { } msg when msg.StartsWith("TRICK"):
                    Position trickWinner = GetPositionFromString(messageToArray[1]);
                    myClient.spadesBrain.TakeTrick(trickWinner);
                    server.sendMessageToClient(myClient, "TRICK " + trickWinner );
                    break;

                // EndRound(Dictionary<Position, int> TotalPoints, Dictionary<Position, int> TotalBags)
                case { } msg when msg.StartsWith("END_ROUND"):

                    if (messageToArray.Length == 5) // Partners
                    {
                        int.TryParse(messageToArray[1], out int TotalPoints_West);
                        int.TryParse(messageToArray[2], out int TotalPoints_North);
                        myClient.spadesBrain.MatchData.MatchPoints[Position.West] = TotalPoints_West;
                        myClient.spadesBrain.MatchData.MatchPoints[Position.North] = TotalPoints_North;
                        int.TryParse(messageToArray[3], out int TotalBags_West);
                        int.TryParse(messageToArray[4], out int TotalBags_North);
                        myClient.spadesBrain.MatchData.MatchBags[Position.West] = TotalBags_West;
                        myClient.spadesBrain.MatchData.MatchBags[Position.North] = TotalBags_North;
                    }
                    else // Solo (Length == 9)
                    {
                        int.TryParse(messageToArray[1], out int TotalPoints_West);
                        int.TryParse(messageToArray[2], out int TotalPoints_North);
                        int.TryParse(messageToArray[3], out int TotalPoints_East);
                        int.TryParse(messageToArray[4], out int TotalPoints_South);
                        myClient.spadesBrain.MatchData.MatchPoints[Position.West] = TotalPoints_West;
                        myClient.spadesBrain.MatchData.MatchPoints[Position.North] = TotalPoints_North;
                        myClient.spadesBrain.MatchData.MatchPoints[Position.East] = TotalPoints_East;
                        myClient.spadesBrain.MatchData.MatchPoints[Position.South] = TotalPoints_South;

                        int.TryParse(messageToArray[5], out int TotalBags_West);
                        int.TryParse(messageToArray[6], out int TotalBags_North);
                        int.TryParse(messageToArray[7], out int TotalBags_East);
                        int.TryParse(messageToArray[8], out int TotalBags_South);
                        myClient.spadesBrain.MatchData.MatchBags[Position.West] = TotalBags_West;
                        myClient.spadesBrain.MatchData.MatchBags[Position.North] = TotalBags_North;
                        myClient.spadesBrain.MatchData.MatchBags[Position.East] = TotalBags_East;
                        myClient.spadesBrain.MatchData.MatchBags[Position.South] = TotalBags_South;
                    }
                    
                    server.sendMessageToClient(myClient, "END_ROUND ");
                    break;
            }
        }

        private static Position GetPositionFromString(string stringPos)
        {
            return stringPos switch
            {
                "W" => Position.West,
                "N" => Position.North,
                "E" => Position.East,
                "S" => Position.South,
                // ReSharper disable once PossibleNullReferenceException
                _ => throw null
            };
        }
    }
}