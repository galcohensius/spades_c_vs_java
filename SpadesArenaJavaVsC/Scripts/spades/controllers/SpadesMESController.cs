using spades.models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace spades.controllers
{
    public class SpadesMESController : MESBase
    {

        List<int> m_removing_all_action_ids = new List<int>();


        public Action<int> OnActionExecuted;

        public static new SpadesMESController Instance;

        bool m_shouldClearRotatingBanners;

        protected override void Awake()
        {
            base.Awake();
            Instance = this as SpadesMESController;

            // Remove all future popup for actions that show a screen
            m_removing_all_action_ids.Add(ACTION_GOTO_ROOM);
            m_removing_all_action_ids.Add(ACTION_OPEN_TABLE);
            m_removing_all_action_ids.Add(ACTION_CLAIM_AND_OPEN_TABLE);
            m_removing_all_action_ids.Add(ACTION_OPEN_LEADERBOARD);
            m_removing_all_action_ids.Add(ACTION_SHOW_AVATAR);
            m_removing_all_action_ids.Add(ACTION_OPEN_ARENA);


        }


        public override bool Execute(int eventId, bool banners = false)
        {
            m_shouldClearRotatingBanners = true;

            return base.Execute(eventId, banners);
        }

        public override void ExecuteDecision(List<MESDecision> decisions, int eventId)
        {
            if (decisions == null)
            {
                return;
            }

            for (int i = 0; i < decisions.Count; i++)
            {
                if (!decisions[i].Fullfilled && UnityEngine.Random.Range(0, 100) < decisions[i].Weight)
                {
                    if (decisions[i].OneTimeSession)
                    {
                        decisions[i].Fullfilled = true;
                    }

                    switch (decisions[i].Id)
                    {
                        case DECISION_SHOW_POPUPS:
                            for (int j = 0; j < decisions[i].MarketingMaterials.Count; j++)
                            {
                                MESMaterial material = decisions[i].MarketingMaterials[j];
                                if (ShouldShowMaterial(material))
                                    SpadesPopupManager.Instance.ShowPromotionPopup(PopupManager.AddMode.DontShowIfPopupShown, material, null);

                            }
                            break;

                        case DECISION_SHOW_BANNERS:

                            ExecuteBanner(decisions[i]);

                            break;

                        case DECISION_OPEN_INVITE:
                            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.DontShowIfPopupShown, SocialPopup.SocialTab.Invite);
                            break;
                        case DECISION_OPEN_CASHIER:
                            SpadesPopupManager.Instance.ShowCashierPopup(PopupManager.AddMode.DontShowIfPopupShown);
                            break;
                        case DECISION_OPEN_INBOX:
                            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.DontShowIfPopupShown, SocialPopup.SocialTab.App_requests);
                            break;
                        case DECISION_OPEN_SEND_GIFT:
                            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.DontShowIfPopupShown, SocialPopup.SocialTab.Send);
                            break;
                        case DECISION_SHOW_PROFILE:
                            SpadesPopupManager.Instance.ShowProfilePopup();
                            break;
                        case DECISION_SHOW_AVATAR:
                            CardGamesPopupManager.Instance.ShowMavatarCreationPopup(PopupManager.AddMode.ShowAndRemove);
                            break;
                        case DECISION_SHOW_ACHHIVMENT:

                            break;
                        case DECISION_SHOW_SETTING:
                            SpadesPopupManager.Instance.ShowSettingsPopup();
                            break;
                        case DECISION_SHOW_TROPHY:
                            SpadesPopupManager.Instance.ShowTrophyRoomPopup(PopupManager.AddMode.DontShowIfPopupShown);
                            break;
                        case DECISION_SHOW_CHAT_BOT:
                            break;
                        case DECISION_SHOW_RATE_US:
                            SpadesPopupManager.Instance.ShowRateQuestionPopup();
                            break;
                        case DECISION_SHOW_REDEEM_BONUS:
                            SpadesPopupManager.Instance.ShowRedeemCodePopup();
                            break;

                    }
                }
            }

        }

        private bool ShouldShowMaterial(MESMaterial material)
        {
            // Check expired material
            if (material.expired > 0)
            {
                double diff = DateUtils.TotalSeconds((int)material.expired);
                if (diff <= 0)
                {
                    return false;
                }
            }

            // For play video action, need to check that the user is not capped
            if (material.actionId == SpadesMESController.ACTION_SHOW_REWARDED_VIDEO ||
                material.actionId == SpadesMESController.ACTION_SHOW_DAILY_REWARDED_VIDEO ||
                material.actionId == SpadesMESController.ACTION_SHOW_HOURLY_REWARDED_VIDEO ||
                material.actionId == SpadesMESController.ACTION_SHOW_LEVELUP_REWARDED_VIDEO)
            {
                VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(ModelManager.Instance.GetUser()); // Init here again if needed
                if (VideoAdController.Instance.IsRewardedVideoCapped())
                    return false;
            }

            return true;
        }

        public override void ExecuteBannerDecision(List<MESDecision> decisions)
        {
            foreach (var decision in decisions)
            {
                ExecuteBanner(decision);
            }
        }


        private void ExecuteBanner(MESDecision decision)
        {
            if (decision.Id == DECISION_SHOW_BANNERS)
            {
                for (int j = 0; j < decision.MarketingMaterials.Count; j++)
                {
                    if (decision.MarketingMaterials[j].location.IndexOf(MATERIAL_LOCATION_LOBBY) == 0 ||
                        decision.MarketingMaterials[j].location == MATERIAL_LOCATION_LOBBY_OLD)
                    {

                        SpadesIMSController.Instance.AddNewMesBanner(decision.MarketingMaterials[j]);


                    }
                    else if (decision.MarketingMaterials[j].location == MATERIAL_LOCATION_DEAL_BUTTON)
                    {
                        //converting the interval to expired times
                        if (decision.MarketingMaterials[j].interval > 0)
                        {
                            decision.MarketingMaterials[j].expired = DateUtils.DateToUnixTimestamp(DateTime.UtcNow + TimeSpan.FromSeconds(decision.MarketingMaterials[j].interval));
                            decision.MarketingMaterials[j].interval = 0;
                        }

                        LobbyTopController.Instance.NewPoReceived(decision.MarketingMaterials[j]);
                    }
                    else if (decision.MarketingMaterials[j].location == MATERIAL_LOCATION_SMALL_BUY_BUTTON)
                    {
                        LobbyTopController.Instance.NewBannerReceived(decision.MarketingMaterials[j]);
                    }
                    else if (decision.MarketingMaterials[j].location == MATERIAL_LOCATION_BUY_BUTTON)
                    {
                        LobbyTopController.Instance.NewBannerReceived(decision.MarketingMaterials[j]);
                    }
                    else if (decision.MarketingMaterials[j].location == MATERIAL_LOCATION_PROMO_ROOM)
                    {
                        //set button active
                        // MOVED TO IMS
                    }
                }
            }


        }

        public override int GetOperandValue(int operandId)
        {

            SpadesUser user = ModelManager.Instance.GetUser() as SpadesUser;
            SpadesUserStats stats = user.Stats as SpadesUserStats;

            int result = 0;
            switch (operandId)
            {
                case CONDITION_OPERAND_BALANCE:
                    result = user.GetCoins();
                    break;
                case CONDITION_OPERAND_NUM_GAMES:
                    result = stats.SessionMatchedPlayed;
                    break;
                case CONDITION_OPERAND_EVERY_NUM_GAME:
                    result = stats.SessionMatchedPlayed;
                    break;
                case CONDITION_OPERAND_NUM_WIN:
                    result = stats.SessionWins;
                    break;
                case CONDITION_OPERAND_EVERY_NUM_WIN:
                    result = stats.SessionWins;
                    break;
                case CONDITION_OPERAND_NUM_LOSE:
                    result = stats.SessionLoses;
                    break;
                case CONDITION_OPERAND_WIN_IN_ROW:
                    result = stats.SessionWinsInRow;
                    break;
                case CONDITION_OPERAND_LOSE_IN_ROW:
                    result = stats.SessionLosesInRow;
                    break;
                case CONDITION_OPERAND_NUM_SESSION:
                    //result = user.sessions;
                    break;
                case CONDITION_OPERAND_EVERY_NUM_SESSION:
                    //result = user.sessions;
                    break;
                case CONDITION_OPERAND_LEVEL:
                    result = user.GetLevel();
                    break;
                case CONDITION_OPERAND_MIN_TABLE:
                    break;
                case CONDITION_OPERAND_TABLE_WIN_IN_ROW:
                    break;
                case CONDITION_OPERAND_TABLE_LOSE_IN_ROW:
                    break;
            }
            return result;
        }

        //special function to handle DirectBuy with the option to send actionParam for multi offer PO's
        public void ExecuteMultiOfferDirectBuyAction(MESMaterial material, int action_param_index)
        {
            if (material == null)
            {
                return;
            }

            JSONArray apm = material.actionParam.AsArray;

            string packageId = apm[action_param_index]["pId"];

            CashierController.Instance.BuyPackage(packageId);
        }

        public void ExecuteMultiOfferOpenTable(MESMaterial material, int action_param_index)
        {
            if (material == null)
                return;

            RemoveAllFuturePopups(material.actionId);

            JSONArray apm = material.actionParam.AsArray;

            string match_id = apm[action_param_index].Value;

            SingleMatchController.Instance.GetAndStartSingleMatch(match_id);


        }

        public void ExecuteMultiOfferOpenArena(MESMaterial material, int action_param_index)
        {
            if (material == null)
            {
                return;
            }

            JSONArray apm = material.actionParam.AsArray;

            string arena_id = apm[action_param_index].Value;

            SpadesArena arena = ModelManager.Instance.GetArena(arena_id) as SpadesArena;
            if (arena != null)
            {
                RemoveAllFuturePopups(material.actionId);

                SpadesArenaController.Instance.Arena_Selected(arena);
                LobbyController.Instance.ShowLobby(false);
                LobbyTopController.Instance.ShowHideIndicationPoints(false);

            }
            else
            {
                LoggerController.Instance.Log("Cannot open arena, arena id doesnt exist: " + arena_id);
            }
        }

        private void RemoveAllFuturePopups(int action_id)
        {
            if (m_removing_all_action_ids.Contains(action_id))
            {
                PopupManager.Instance.RemoveAllFuturePopups();
            }

        }


        public void ExecuteAction(MESMaterial material, bool isBanner = false)
        {
            LoggerController.Instance.Log("material.actionId == " + material.actionId);

            if (material == null)
            {
                return;
            }

            // If needed, remove future popups
            RemoveAllFuturePopups(material.actionId);


            switch (material.actionId)
            {
                case ACTION_OPEN_CASHIER:
                    SpadesPopupManager.Instance.ShowCashierPopup(PopupManager.AddMode.ShowAndRemove);
                    break;
                case ACTION_DIRECT_BUY:
                    string packageId = material.actionParam["pId"];

                    CashierController.Instance.BuyPackage(packageId);
                    break;
                case ACTION_OPEN_FB_INVITE_FRIENDS:
                    SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
                    break;
                case ACTION_GOTO_ROOM:

                    int room_id = Convert.ToInt32(material.actionParam.Value);
                    LobbyController.Instance.Lobby_view.GetSwipe().InitSwipe(room_id);

                    break;
                case ACTION_OPEN_SEND_GIFT:
                    SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Send);
                    break;
                case ACTION_OPEN_LEADERBOARD:
                    PopupManager.Instance.HidePopup();
                    LeaderboardsModel.LeaderboardType lbType = (LeaderboardsModel.LeaderboardType)material.actionParam.AsInt;
                    LeaderboardsController.Instance.ShowLeaderboard(lbType);

                    break;
                case ACTION_OPEN_CONTEST:
                    break;
                case ACTION_OPEN_RATEUS:
                    SpadesPopupManager.Instance.ShowRateQuestionPopup();
                    break;
                case ACTION_SHOW_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoBonus);
                    break;
                case ACTION_SHOW_DAILY_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedSlotVideoBonus);
                    break;
                case ACTION_SHOW_HOURLY_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedHourlyVideoBonus);
                    break;
                case ACTION_SHOW_LEVELUP_REWARDED_VIDEO:
                    VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoLevelUp);
                    break;
                case ACTION_CLOSE_CONTINUE:
                    PopupManager.Instance.HidePopup();
                    break;
                case ACTION_OPEN_EXTERNAL_URL:

                    string url = material.actionParam.Value;
#if UNITY_WEBGL && !UNITY_EDITOR
					openBrowserWindow (url);
#else
                    Application.OpenURL(url);
#endif


                    break;
                case ACTION_OPEN_DAILYBONUS:
                    break;
                case ACTION_FB_CONNECT:
                    StartCoroutine(DelayedFBConnect());
                    break;
                case ACTION_OPEN_INVITE:
                    SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
                    break;
                case ACTION_OPEN_TABLE:
                    string match_id = material.actionParam.Value;
                    SingleMatchController.Instance.GetAndStartSingleMatch(match_id);
                    break;
                case ACTION_CLAIM_BONUS:
                case ACTION_CLAIM_DYNAMIC_BONUS:
                    BonusController.Instance.ClaimBonusForMES(material.actionParam, (bool success, BonusAwarded bonusAwarded) =>
                     {
                         if (success)
                             SpadesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.GeneralBonus, PopupManager.AddMode.ShowAndRemove, null, true, bonusAwarded);
                     });
                    break;
                case ACTION_CLAIM_AND_OPEN_TABLE:
                    string[] splited = material.actionParam.Value.Split('-');
                    string bonus_id = splited[0];
                    match_id = splited[1];

                    BonusController.Instance.ClaimBonusForMES(bonus_id, (bool success, BonusAwarded bonus) =>
                    {
                        if (success)
                        {
                            SingleMatchController.Instance.GetAndStartSingleMatch(match_id);
                        }
                    });
                    break;
                case ACTION_SHOW_PROFILE:
                    SpadesPopupManager.Instance.ShowProfilePopup();
                    break;
                case ACTION_SHOW_AVATAR:
                    CardGamesPopupManager.Instance.ShowMavatarCreationPopup(PopupManager.AddMode.ShowAndRemove);
                    break;
                case ACTION_SHOW_ACHIEVEMENT:

                    break;
                case ACTION_OPEN_ARENA:

                    string arena_id = material.actionParam.Value;

                    SpadesArena arena = (SpadesArena) ModelManager.Instance.GetArena(arena_id);

                    // Special case for MES (27/2/2020):
                    // Since there is a problem with MES open arena segmentation, we have a kombina.
                    // We use the arena we found by ID to get the world id and game mode.
                    // The active arena is then selected by playing mode.
                    SpadesWorld arenaWorld = (SpadesWorld) arena.World;
                    SpadesArena activeArena = arenaWorld.GetArena(arena.Playing_mode);

                    SpadesArenaController.Instance.Arena_Selected(activeArena);
                    LobbyController.Instance.ShowLobby(false);
                    LobbyTopController.Instance.ShowHideIndicationPoints(false);

                    break;
                case ACTION_SHOW_SETTING:
                    SpadesPopupManager.Instance.ShowSettingsPopup();
                    break;
                case ACTION_SHOW_TROPHY:
                    SpadesPopupManager.Instance.ShowTrophyRoomPopup(PopupManager.AddMode.ShowAndRemove);
                    break;
                case ACTION_OPEN_REDEEM_BONUS:
                    SpadesPopupManager.Instance.ShowRedeemCodePopup();
                    break;
                case ACTION_SHOW_MISSION_POPUP:
                    MissionController.Instance.MissionButtonClicked(false);
                    break;
                case ACTION_OPEN_POPUP:
                    MESMaterial popupMaterial;

                    LoggerController.Instance.Log("material.actionParam == " + material.actionParam.AsInt);
                    if (material.actionParam is JSONArray)
                    {
                        //int id, string mime, int aId, string loc, JSONNode sty, JSONNode acPa, string src, double expired, double interval, string timerPos, int timerVisibilty,JSONArray dinamicContent
                        popupMaterial = new MESMaterial(material.actionParam[0]["id"].AsInt,
                            material.actionParam[0]["mTe"],
                            material.actionParam[0]["aId"].AsInt,
                            material.actionParam[0]["loc"],
                            material.actionParam[0]["ste"],
                            material.actionParam[0]["aPm"],
                            material.actionParam[0]["src"],
                            material.actionParam[0]["eDe"].AsDouble,
                            material.actionParam[0]["eIl"].AsDouble,
                            material.actionParam[0]["tPn"],
                            material.actionParam[0]["tVy"].AsInt,
                            material.actionParam[0]["dCt"].AsArray,
                            material.actionParam[0]["cBBr"],
                            material.actionParam[0]["tId"]);

                    }
                    else
                    {
                        popupMaterial = new MESMaterial(material.actionParam["id"].AsInt,
                            material.actionParam["mTe"],
                            material.actionParam["aId"].AsInt,
                            material.actionParam["loc"],
                            material.actionParam["ste"],
                            material.actionParam["aPm"],
                            material.actionParam["src"],
                            material.actionParam["eDe"].AsDouble,
                            material.actionParam["eIl"].AsDouble,
                            material.actionParam["tPn"],
                            material.actionParam["tVy"].AsInt,
                            material.actionParam["dCt"].AsArray,
                            material.actionParam["cBBr"],
                            material.actionParam["tId"]);
                    }

                    //need to add here something to get value of asset_budle_flag 
                    MESPopup popup = SpadesPopupManager.Instance.ShowPromotionPopup(PopupManager.AddMode.ShowAndRemove, popupMaterial);
                    break;
            }

            // Hide the MES popup unless we are showing a direct buy. In this case, the popup will be hidden after the 
            // purchase succeeded or canceled
            // Do not hide when the action is OPEN_POPUP since it will hide instantly
            if (!isBanner && material.actionId != ACTION_DIRECT_BUY && material.actionId != ACTION_OPEN_POPUP
                && material.actionId != ACTION_CLAIM_BONUS && material.actionId != ACTION_CLAIM_DYNAMIC_BONUS)
                PopupManager.Instance.HidePopup();


            // Fire event
            if (OnActionExecuted != null)
                OnActionExecuted(material.actionId);

        }

        public override void HandleInsufficientFunds(int delta, PopupManager.AddMode addMode = PopupManager.AddMode.ShowAndRemove)
        {
            if (!Execute(SpadesMESController.TRIGGER_INSUFFICIAN_BALANCE))
            {
                if (PO_material != null && !IsMaterialExpired(PO_material))
                    SpadesPopupManager.Instance.ShowPromotionPopup(addMode, PO_material, null, ShowFreeCoinsPopup);
                else
                {
                    SpadesPopupManager.Instance.ShowinsufficientFundsPopup(delta, addMode, null, ShowFreeCoinsPopup);
                }
            }
        }

        private void ShowFreeCoinsPopup()
        {
            CardGamesPopupManager.Instance.ShowFreeCoinsPopup(PopupManager.AddMode.ShowAndRemove);
        }

        /// <summary>
        /// Check if a specific material is expired.
        /// If the material has an expired or interval field, check if the specific time has passed.
        /// </summary>
        /// <returns><c>true</c>, if material expired was ised, <c>false</c> otherwise.</returns>
        /// <param name="material">Material.</param>
        public bool IsMaterialExpired(MESMaterial material)
        {
            if ((int)material.expired == 0)
                return false;

            long timeToExpiration = DateUtils.TotalSeconds((int)material.expired);
            return timeToExpiration <= 0;

        }

        /// <summary>
        /// Delays the FB connect by some fraction of a second. 
        /// This is used to make sure the popup have time to disappear before we go out to Facebook's UI.
        /// </summary>
        private IEnumerator DelayedFBConnect()
        {
            yield return new WaitForSecondsRealtime(0.1f);
            LobbyController.Instance.FBLogin(null);
        }

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void openBrowserWindow(string url);
#endif


    }
}

