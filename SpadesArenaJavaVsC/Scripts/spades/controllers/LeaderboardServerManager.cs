using System;
using System.Collections;


namespace spades.controllers
{
    public class LeaderboardServerManager : MonoBehaviour
	{

		private Connector m_connector;
		private CommandsManager m_commands_manager;
		private bool m_connected = false;

		[SerializeField] string m_host = null;
        [SerializeField] string m_hostQAMode = null;
        [SerializeField] string m_hostDevMode = null;
		[SerializeField]	int m_port = 0;
		[SerializeField]	int m_portWebSocket = 0;

		const float CONNECTION_KEEP_ALIVE_DELAY = 10f;
		
		public Action Disconnected_Event;
		private Coroutine m_ConnectionKeepAliveCoroutine;

		public static LeaderboardServerManager Instance;

		private void Awake()
        {
            Instance = this;
        }


		void Start ()
		{
			m_commands_manager = new CommandsManager ();
			RegisterCommands ();

			string host = m_host;
			#if QA_MODE
			host = m_hostQAMode;
			#elif DEV_MODE
			host = m_hostDevMode;
			#endif


			#if UNITY_EDITOR
			m_connector = new SocketConnector (host, m_port, m_commands_manager);
			//m_connector = new WSConnector(host,m_portWebSocket,m_commands_manager);
			//         GameObject delegateGO = new GameObject("LeaderboardServerManagerDelegate");
			//         WSConnector.WSDelegate ws_delegate = delegateGO.AddComponent<WSConnector.WSDelegate>();
			//ws_delegate.Connector = (WSConnector)m_connector;
			#elif UNITY_WEBGL
            m_connector = new WSConnector(host,m_portWebSocket,m_commands_manager);
            GameObject delegateGO = new GameObject("LeaderboardServerManagerDelegate");
            WSConnector.WSDelegate ws_delegate = delegateGO.AddComponent<WSConnector.WSDelegate>();
            ws_delegate.Connector = (WSConnector)m_connector;
			#else
			m_connector = new SocketConnector(host,m_port,m_commands_manager);
			#endif

		}

		private void RegisterCommands ()
		{
			Command get_leader_board_command = new GetLeaderBoardCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.GET_LEADER_BOARD, get_leader_board_command);

			Command update_leader_board_command = new UpdateLeaderBoardCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.UPDATE_LEADER_BOARD, update_leader_board_command);

			Command update_user_friends_command = new UpdateUserFriendsCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.UPDATE_USER_FRIENDS, update_user_friends_command);

		}


		public void Connect (Action<bool> ConnectCallback = null)
		{
			m_connector.Connect ((success) => {
				m_connected = success;

                if (success) {
                    LoggerController.Instance.Log("Connected to leaderboard server");
                }

                if (ConnectCallback != null) 
					ConnectCallback (success);
			});
		}

       

		public void Disconnect ()
		{
			if (m_connected) {
				m_connector.Disconnect ();
				m_connected = false;
				
				LoggerController.Instance.Log ("Disconnected from leaderboard server");
            }
		}

		public void Send (MMessage message)
		{
			if (m_connected) {
				m_connector.Send (message);
                UpdateLastCommandTime();
			} else {
				Connect ((bool success) => {
					if (success) {
						m_connector.Send (message);
                        UpdateLastCommandTime();
					}
				});

			}
		}


		public bool Connected {
			get {
				return m_connected;
			}
		
		}


        private void UpdateLastCommandTime ()
		{
			if (m_ConnectionKeepAliveCoroutine != null) {
				StopCoroutine (m_ConnectionKeepAliveCoroutine);
			}

            m_ConnectionKeepAliveCoroutine = StartCoroutine (ConnectionKeepAlive());
		}
		
		

        private IEnumerator ConnectionKeepAlive ()
		{

            yield return new WaitForSecondsRealtime (CONNECTION_KEEP_ALIVE_DELAY);

			Disconnect ();
		}
	}

}

