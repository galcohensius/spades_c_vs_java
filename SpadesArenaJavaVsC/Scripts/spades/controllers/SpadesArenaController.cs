using System.Collections;
using spades.models;
using System;
using System.Collections.Generic;

namespace spades.controllers
{
    public delegate void RebuyRequested(Action action_completed);
    public delegate void RebuyQuitRequested(bool isClose);

    public class SpadesArenaController : MonoBehaviour
    {

        SpadesArena m_arena = null;
        SpadesActiveArena m_active_arena = null;

        int m_match_popup_screen = 0;

        ArenaScreen m_arena_lobby_screen;


        LoseArenaOverlay m_lose_arena_overlay;
        WinArenaStageOverlay m_stage_win_overlay;
        WinArenaStage2ndPlaceOverlay m_stage_win_2nd_place_overlay;

        SearchingForPlayersPopup m_searching_popup;


        int m_last_elimination_rebuy;
        int m_last_elimination_stage;
        int m_entry_highest_gem = 0;

        ArenaScreen.BackFromReason m_back_to_lobby_reason = ArenaScreen.BackFromReason.lobby;

        int m_last_stage_before_lose;
        //this is for cancelling an arena match on the first stage

        public static SpadesArenaController Instance;

        private void Awake()
        {
            Instance = this;
        }

        /// <summary>
        /// triggered when an arena is selected  - initiates loading the arena popup view
        /// </summary>
        /// <param name="arena">Arena.</param>
        public void Arena_Selected(SpadesArena arena)
        {
            m_arena = arena;
            m_arena_lobby_screen = SpadesScreensManager.Instance.ShowArenaLobby(m_arena, ArenaScreen.BackFromReason.lobby);

        }

        /// <summary>
        /// when button clicked already inside an arena lobby
        /// </summary>
        public void Enter_Arena()
        {

            m_active_arena = SpadesModelManager.Instance.GetActiveArena(m_arena.GetID());

            if (m_active_arena != null)
            {
                PlayNextArena(OnCancelPlayClick);
            }
            else
            {

                int coins = ModelManager.Instance.GetUser().GetCoins();
                int buyin = m_arena.GetBuyInCoins();

                //attempting to buy in to the match - we need to check if they have enough money and if so - reduce it and start the game
                if (coins < buyin)
                {

                    (IMSController.Instance as SpadesIMSController).HandleInsufficientFunds(buyin - coins);

                    m_arena_lobby_screen.UnlockBuyButton();

                }
                else
                {
                   m_active_arena = new SpadesActiveArena(m_arena);

                   CardGamesCommManager.Instance.ArenaBuyIn(0, m_active_arena.GetArena().GetID(), (bool success, int curStage, int newCoinsBalance) =>
                   {
                       if (success)
                       {

                           ModelManager.Instance.AddActiveArena(m_active_arena);

                           ModelManager.Instance.GetUser().Add_Coins(-buyin);

                           m_active_arena.SetBought(true);

                           // User stats
                           UserStatsController.Instance.UpdateStartArena();

                           m_active_arena.SetCurrStage(curStage);

                           ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);
                           LobbyTopController.Instance.UpdateCoinsText();
                           m_arena_lobby_screen.ArenaBought(() => { PlayNextArena(OnCancelPlayClick); });

                           // Appsflyer event
                           TrackingManager.Instance.AppsFlyerTrackEvent(TrackingManager.EventName.StartedChallenge,
                              new Dictionary<string, string>() {
                                    { "arena_id",m_active_arena.GetArena ().GetID ().ToString () }
                              });
                           // Facebook event
                           var data = new Dictionary<string, object> { {
                                    "arena_id",
                                    m_active_arena.GetArena ().GetID ()
                                } };


                           // Appsflyer event
                           TrackingManager.Instance.AppsFlyerTrackEvent(TrackingManager.EventName.StartedChallenge,
                              new Dictionary<string, string>() {
                                    { "arena_id",m_active_arena.GetArena ().GetID ().ToString () }
                              });

                       }
                       else
                       {
                           PopupManager.Instance.ShowMessagePopup("The arena can't start right now.\nNo coins were deducted from your balance.", PopupManager.AddMode.ShowAndRemove, true, () =>
                           {
                               m_active_arena = null;
                               m_arena_lobby_screen.CloseButtonClicked();
                           });
                       }
                   });

                }
            }
        }


        private void OnCancelPlayClick()
        {
            m_arena_lobby_screen.OnCancelClick();
        }


        /// <summary>
        /// gets the next match from an arena - and starts it
        /// </summary>
        public void PlayNextArena(SearchingForPlayersPopup.SearchingPopupCancelClicked cancel_delegate = null)
        {

            // Clear the bot's stat cache
            PlayerView.ClearPlayerCache();


            m_last_stage_before_lose = m_active_arena.GetCurrStage();

            // when the game is finished we need to clear the player so bot data will be sent to it in the next game
            RoundController.Instance.Player = null;
            RoundController.Instance.Table_found_delegate = TableFound;

            ManagerView.Instance.LoadTableSprite(m_active_arena.GetCurrArenaMatch(), (Sprite tableSprite) =>
            {
                if (tableSprite != null)
                    m_searching_popup = LobbyController.Instance.StartSearchingForPlayers(m_active_arena.GetCurrArenaMatch(), cancel_delegate);
            });


        }

        public void TableFound(Action table_found_done)
        {
            SpadesModelManager model_manager = ModelManager.Instance as SpadesModelManager;

            SpadesActiveMatch active_match = new SpadesActiveMatch(m_arena.GetArenaMatch(m_active_arena.GetCurrStage()));
            ModelManager.Instance.SetActiveMatch(active_match);

            LobbyController.Instance.ShowLobby(false);
            LobbyTopController.Instance.ShowTop(false, false);

            SpadesScreensManager.Instance.HideScreen();
            SpadesOverlayManager.Instance.HideAllOverlays();
            PopupManager.Instance.HidePopup();

            RoundController.Instance.Init(MatchEndedCallback, MatchCancelledCallback);

            active_match.Game_id = ModelManager.Instance.GetTable().Game_id;

            ManagerView.Instance.InGameGui.GetComponent<InGameUIView>().SetActiveMatch(active_match);

            SpadesWorld highestWorldWithGem = GetHighestWorldWithGem();

            if (highestWorldWithGem != null)
            {
                m_entry_highest_gem = highestWorldWithGem.Index + 1; // TODO: Check if this is the right place for this (Ran 23/3)
            }

            // handles the avatars in game between the rounds
            MAvatarController.Instance.OnAvatarShownInHierarchy?.Invoke();

            SpadesCommManager.Instance.StartArenaStage(m_active_arena.GetArena().GetID(), active_match.Game_id, (success, matchTransId, newCoinsBalance, matchBotsData,show_missions, contest,voucher_id) =>
            {
                if (success)
                {
                    //remove the received voucher
                    if (voucher_id != null)
                        SpadesVoucherController.Instance.UseVoucher(voucher_id);

                    m_active_arena.GetCurrArenaMatch().BotsData = matchBotsData;

                    active_match.Game_trans_id = matchTransId;

                    ManagerView.Instance.InGameGui.GetComponent<InGameUIView>().SetGameTransId(matchTransId);

                    if (show_missions)
                    {
                        LoggerController.Instance.Log("SERVER RETURNS SHOW MISSIONS FOR ARENA");
                    }
                    MissionController.Instance.Show_mission_button_in_game = false;

                    // no contest for arena 
                    ContestPanelController.Instance.OnGameStarted(null);

                    // Start the game after start match response - This is needed in order to user the bots data received from
                    // the server
                    RoundController.Instance.StartGame(true);

                    if (table_found_done != null)
                        table_found_done();
                } else
                {
                    PopupManager.Instance.ShowMessagePopup("The game can't start right now.\nNo coins were deducted from your balance.", PopupManager.AddMode.ShowAndRemove, true, () =>
                    {
                        m_arena_lobby_screen.CloseButtonClicked();  
                    });
                }
            });

        }


        IEnumerator ShowPlayersAndCloseTable(Action done_action)
        {

            if (m_searching_popup.Players_ready < 4)
            {
                StartCoroutine(m_searching_popup.AssignPlayers(4, 0, 0, true));

                yield return new WaitForSeconds(0.25f);
                PopupManager.Instance.HidePopup();
                if (done_action != null)
                    done_action();
            }
            else
            {
                PopupManager.Instance.HidePopup();
                if (done_action != null)
                    done_action();
            }
        }

        private void MatchEndedCallback(int win_place, int num_winners, string match_history = null, Action received_answer = null)
        {
            SpadesModelManager mm = ModelManager.Instance as SpadesModelManager;
            SpadesActiveMatch active_match = mm.GetActiveMatch();

            PlayingMode pm = active_match.GetMatch().Playing_mode;

            int points = 0;
            if (pm == PlayingMode.Partners)
                points = active_match.GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South);
            else
                points = active_match.GetCalculatedTotalResults(SpadesRoundResults.Positions.South);

            JSONObject user_achievements = AchievementsManager.Instance.EncodeUserAchievements();

            SpadesWorld world = (SpadesWorld) m_active_arena.GetArena().World;

            RoundController.Instance.StopAllChatters();

            SpadesCommManager.Instance.EndArenaStage(m_active_arena.GetArena().GetID(),
                0, active_match.Game_trans_id, active_match.GetRoundsCount(), points, false, win_place, 1, (mm.GetUser() as SpadesUser).Stats as SpadesUserStats, match_history, user_achievements,
                (success, coinsPayout, xpPayout, newCoinsBalance, newDiamondsBalance, newXpBalance, levelData, next_arena_stage, currStep, totalGemsWon, newLeaderboardId, minBotFactor, maxBotFactor, new_cashier) =>
                {
                    if (success)
                    {


                        if (received_answer != null)
                            received_answer();

                        //Leaderdoards
                        if (win_place > 0)
                        {
                            if (newLeaderboardId != -1 && ModelManager.Instance.Leaderboards.CurrentLeaderboardId != newLeaderboardId)
                            {
                                ModelManager.Instance.Leaderboards.ClearCache(true);
                                ModelManager.Instance.Leaderboards.CurrentLeaderboardId = newLeaderboardId;
                            }

                            int botId = SpadesStateController.Instance.GetAssignedBotId(newLeaderboardId);
                            /*if (botId == 0)
                            {
                                CardGamesLocalDataController.AVModelAndCountry assignedBot = (LocalDataController.Instance as CardGamesLocalDataController).FetchRandomBots(1, false, 0,0)[0];

                                SpadesStateController.Instance.SetAssignedBotId(newLeaderboardId, assignedBot.Id);
                            }*/

                            ModelManager.Instance.Leaderboards.MinBotFactor = minBotFactor;
                            ModelManager.Instance.Leaderboards.MaxBotFactor = maxBotFactor;

                            LeaderboardsController.Instance.UpdateLeaderboards(coinsPayout);
                        }

                        if (newLeaderboardId != -1)
                        {
                            ModelManager.Instance.Leaderboards.CurrentLeaderboardId = newLeaderboardId;
                        }

                        // Calculate the delta XP won and set the new XP
                        mm.GetUser().SetXPInLevel(newXpBalance);

                        if (levelData != null)
                        {
                            int newLevel = levelData.Level;

                            mm.GetUser().SetLevel(newLevel, levelData.XpThreshold, newXpBalance);
                            mm.Last_levelup_bonus_awarded = levelData.LevelBonus;
                            // Overwrite new bonuses data that changed on level up
                            if (levelData.NewBonuses != null)
                                mm.Bonuses.Merge(levelData.NewBonuses);

                            // Appsflyer event
                            if (newLevel == 10 || newLevel == 20)
                            {
                                TrackingManager.Instance.AppsFlyerTrackEvent("Reached_level_" + newLevel, null);
                            }

                        }

                        if (next_arena_stage == 0)
                        {

                            //update the cashier data
                            if (new_cashier != null)
                            {
                                ModelManager.Instance.SetCashier(new_cashier);
                            }

                            // Arena has ended if win, or eliminated if lose
                            mm.GetUser().SetCoins(newCoinsBalance);

                            if (win_place == 1)
                            {
                                bool gemWon = false;
                                if (currStep >= 0)
                                {
                                    if (world.Arena_Gems_Won < totalGemsWon)
                                    {
                                        gemWon = true;

                                        var m_model_manager = ModelManager.Instance as SpadesModelManager;
                                        m_model_manager.Inventory.AddItem($"gem_{world.GetID()}_{world.Arena_Gems_Won + 1}");

                                    }

                                
                                    world.Arena_Gem_Curr_Step = currStep;
                                    world.Arena_Gems_Won = totalGemsWon;
                                }
                                ShowFullArenaWin(coinsPayout, xpPayout, gemWon, levelData != null);



                                //removing active arena from local dictionary
                                if (m_active_arena != null)
                                    mm.RemoveActiveArena(m_active_arena.GetArena().GetID());

                            }
                            else if (win_place == 0)
                            {
                                //lost arena - eliminated
                                ArenaEliminated(m_last_stage_before_lose, m_active_arena.GetArena().GetName(), 0, xpPayout);
                            }
                        }
                        else
                        {
                            if (win_place > 0)
                            {
                                if (currStep > 0)
                                {
                                    world.Arena_Gem_Curr_Step = currStep;
                                    world.Arena_Gems_Won = totalGemsWon;
                                }

                                if (win_place == 2 && pm == PlayingMode.Solo)
                                {


                                    m_active_arena.SetCurrStage(next_arena_stage);

                                    m_back_to_lobby_reason = ArenaScreen.BackFromReason.second_place;

                                    m_stage_win_2nd_place_overlay = SpadesOverlayManager.Instance.ShowWinArena2ndPlaceStageOverlay();
                                    int gap = 0;
                                    m_stage_win_2nd_place_overlay.SetWinScreenData(gap, coinsPayout, xpPayout, LobbyClicked, NewGameClicked, () =>
                                    {
                                        //TODO ALON - POST ANIMATION SHOW CHALLENGE
                                    });

                                }
                                else
                                {
                                    m_active_arena.SetCurrStage(next_arena_stage);
                                    //won stage

                                    m_back_to_lobby_reason = ArenaScreen.BackFromReason.win;

                                    if (ModelManager.Instance.WinsInRow.ContainsKey("a_" + (active_match.GetMatch() as Match).GetID()))
                                        ModelManager.Instance.WinsInRow["a_" + (active_match.GetMatch() as Match).GetID()] = ModelManager.Instance.WinsInRow["a_" + (active_match.GetMatch() as Match).GetID()] + 1;
                                    else
                                        ModelManager.Instance.WinsInRow.Add("a_" + (active_match.GetMatch() as Match).GetID(), 1);

                                    m_stage_win_overlay = SpadesOverlayManager.Instance.ShowWinArenaStageOverlay();

                                    m_stage_win_overlay.SetWinScreenData(coinsPayout, xpPayout, LobbyClicked, NewGameClicked, () =>
                                    {
                                        //TODO ALON - POST ANIMATION SHOW CHALLENGE
                                    });

                                }

                            }
                        }

                        // Achievements
                        RoundController.Instance.UpdateAllMatchEndAchievements(win_place, coinsPayout, true);

                        // User stats
                        UserStatsController.Instance.UpdateEndMatch(win_place == 1, coinsPayout);

                        // Update the purchaser, so if there are changes in the packages, it will be recorded
                        PurchaseController.Instance.UpdatePurchaser();


                        //calls the MES Controller to update the banners - 
                        (SpadesMESController.Instance as SpadesMESController).Execute(SpadesMESController.TRIGGER_LOGIN, true);

                        //public void GetArenaEndGamePiggyUpdate(int arenaID, string loginToken, int xpDelta, int stageID, int userID, GetPiggyArenaEndGameCompleted getPiggyArenaEndGameUpdateCompleted)

                        if (PiggyController.Instance.SendUpdatePiggyAtEndGame())
                        {
                            CommManagerJava.Instance.GetArenaEndGamePiggyUpdate(Int32.Parse(m_active_arena.GetArena().GetID()), CardGamesCommManager.Instance.LoginToken, xpPayout, next_arena_stage, ModelManager.Instance.GetUser().GetID(),
                            (piggySuccess, piggyJSON) =>
                            {
                                if (piggySuccess)
                                {
                                    PiggyController.Instance.UpdatePiggy(piggyJSON);
                                }
                                else
                                    LoggerController.Instance.LogError("update piggy at end arena match failed");
                            });
                        }
                    }
                    else
                    {
                        // SHow try again popup
                        ManagerView.Instance.NetworkErrorHandler("endArenaStage");
                    }

                });


        }

        private void ShowFullArenaWin(int delta_coins, int delta_xp, bool gemWon, bool level_up)
        {

            m_stage_win_overlay = SpadesOverlayManager.Instance.ShowWinArenaStageOverlay();

            if (gemWon)
                m_back_to_lobby_reason = ArenaScreen.BackFromReason.big_win_with_gem;
            else
            {
                m_back_to_lobby_reason = ArenaScreen.BackFromReason.big_win;
                // m_back_to_lobby_reason = ArenaScreen.BackFromReason.big_win_with_gem; //FOR TEST
            }


            SpadesWorld highestWorldWithGem = GetHighestWorldWithGem();

            // TODO : Need To Think What We Update And Do Gem Win
            if (gemWon && highestWorldWithGem != null)
            {
                //ModelManager.Instance.GetUser().GetAvatar().Base_Index = highestWorldWithGem.Index + 1;
                //ModelManager.Instance.GetUser().GetAvatar().GemsCount = highestWorldWithGem.Arena_Gems_Won;
            }

            m_stage_win_overlay.SetWinScreenData(delta_coins, delta_xp, LobbyClicked, null, () =>
             {
                 //TODO ALON - POST ANIMATION SHOW CHALLENGE
             });
            CardGamesSoundsController.Instance.GameWin();
        }



        //quitting arena
        private void MatchCancelledCallback(bool game_started, Action leave_done, string match_history = null)
        {
            SpadesModelManager mm = ModelManager.Instance as SpadesModelManager;

            SpadesActiveMatch active_match = mm.GetActiveMatch();
            if (game_started)
            {

                int win_coins = m_arena.GetAccumulatedPayoutCoinsByStage(m_last_stage_before_lose);

                (UserStatsController.Instance as SpadesUserStatsController).UpdateEndRound(true, (mm.GetTable() as SpadesTable).GetPlayer(3).GetBids(), (mm.GetTable() as SpadesTable).GetPlayer(3).GetTakes());
                UserStatsController.Instance.UpdateEndMatch(false, win_coins);

                RoundController.Instance.UpdateAllMatchEndAchievements(0, win_coins, true);

                JSONObject user_achievements = AchievementsManager.Instance.EncodeUserAchievements();

                SpadesCommManager.Instance.EndArenaStage(m_active_arena.GetArena().GetID(),
                    0, active_match.Game_trans_id, active_match.GetRoundsCount(), 0, true, 0, 1, (mm.GetUser() as SpadesUser).Stats as SpadesUserStats, match_history, user_achievements,
                    (success, coinsPayout, xpPayout, newCoinsBalance, newDiamondsBalance, newXpBalance, levelData, next_arena_stage, currStep, totalGemsWon, newLeaderboardId, minBotFactor, maxBotFactor, new_cashier) =>
                    {
                        mm.GetUser().SetXPInLevel(newXpBalance);

                        if (next_arena_stage == 0)
                        { //this means you are elimanted in lose or that you cleared the whole arena if you won

                            //won arena - need to get all the money and the XP - and remove arena from the 

                            //update the cashier data
                            if (new_cashier != null)
                            {
                                ModelManager.Instance.SetCashier(new_cashier);
                            }

                            mm.GetUser().SetCoins(newCoinsBalance);

                            if (levelData != null)
                            {
                                int newLevel = levelData.Level;

                                mm.GetUser().SetLevel(levelData.Level, levelData.XpThreshold, newXpBalance);
                                mm.Last_levelup_bonus_awarded = levelData.LevelBonus;
                                // Overwrite new bonuses data that changed on level up
                                if (levelData.NewBonuses != null)
                                    mm.Bonuses.Merge(levelData.NewBonuses);

                                // Appsflyer event
                                if (newLevel == 10 || newLevel == 20)
                                {
                                    TrackingManager.Instance.AppsFlyerTrackEvent("Reached_level_" + newLevel, null);
                                }
                            }

                            leave_done();

                            ArenaEliminated(m_last_stage_before_lose, m_active_arena.GetArena().GetName().ToString(), 0, xpPayout);

                            if (newLeaderboardId != -1)
                            {
                                ModelManager.Instance.Leaderboards.CurrentLeaderboardId = newLeaderboardId;
                            }

                        }
                        else
                        {
                            //lost stage	
                            BackToArena(ArenaScreen.BackFromReason.lose);
                            leave_done();
                            //m_stage_lose_overlay = SpadesOverlayManager.Instance.ShowLoseArenaStageOverlay (StageLoseClosed);

                            if (ModelManager.Instance.WinsInRow.ContainsKey("a_" + (active_match.GetMatch() as Match).GetID()))
                                ModelManager.Instance.WinsInRow["a_" + (active_match.GetMatch() as Match).GetID()] = ModelManager.Instance.WinsInRow["a_" + (active_match.GetMatch() as Match).GetID()] - 1;
                            else
                                ModelManager.Instance.WinsInRow.Add("a_" + (active_match.GetMatch() as Match).GetID(), -1);

                            SpadesOverlayManager.Instance.ShowLoseArenaOverlay();
                        }

                        if (PiggyController.Instance.SendUpdatePiggyAtEndGame())
                        {
                            CommManagerJava.Instance.GetArenaEndGamePiggyUpdate(Int32.Parse(m_active_arena.GetArena().GetID()), CardGamesCommManager.Instance.LoginToken,  xpPayout, next_arena_stage, ModelManager.Instance.GetUser().GetID(),
                            (piggySuccess, piggyJSON) =>
                            {
                                if (piggySuccess)
                                {
                                    PiggyController.Instance.UpdatePiggy(piggyJSON);
                                }
                                else
                                    LoggerController.Instance.LogError("update piggy at end arena match failed");
                            });
                        }

                        // Update the purchaser, so if there are changes in the packages, it will be recorded
                        PurchaseController.Instance.UpdatePurchaser();

                        CardGamesPopupManager.Instance.ShowWaitingIndication(false);


                        //calls the MES Controller to update the banners - 
                        (SpadesMESController.Instance as SpadesMESController).Execute(SpadesMESController.TRIGGER_LOGIN, true);

                    });
            }
            else
                Arena_Selected(m_arena);


        }


        public void BackToArena(ArenaScreen.BackFromReason back_from_win)
        {
            ManagerView.Instance.Table.GetComponent<TableView>().ShowGameUI(false);

            SpadesScreensManager.Instance.ShowArenaLobby(m_arena, back_from_win);

        }

        public void LobbyClicked()
        {
            SpadesOverlayManager.Instance.HideAllOverlays();
            RoundController.Instance.Table_view.ShowGameUI(false);
            BackToArena(m_back_to_lobby_reason);
        }

        public SpadesWorld GetHighestWorldWithGem()
        {
            SpadesWorld result = null;

            if (ModelManager.Instance.Worlds != null)
            {
                foreach (SpadesWorld world in ModelManager.Instance.Worlds)
                {
                    if (world.Arena_Gems_Won > 0)
                    {
                        result = world;
                    }
                }
            }
            return result;
        }

        public void NewGameClicked()
        {
            //show arena lobby
            SpadesOverlayManager.Instance.HideAllOverlays();
            RoundController.Instance.Table_view.ShowGameUI(false);
            BackToArena(ArenaScreen.BackFromReason.winPlayNext);

            //after play next
            //PlayNextArena (LobbyClicked);
        }

        public SpadesArena GetCurrArena()
        {
            return m_arena;
        }

        public void SetMatchPopupScreen(int match_popup_screen)
        {
            m_match_popup_screen = match_popup_screen;
        }

        public int GetMatchPopupScreen()
        {
            return m_match_popup_screen;
        }

        public int GetCurrentRebuy()
        {
            return m_last_elimination_rebuy;
        }

        private void ArenaEliminated(int stage, string arena_name, int lose_coins, int lose_xp)
        {

            CardGamesSoundsController.Instance.GameLose();
            m_lose_arena_overlay = SpadesOverlayManager.Instance.ShowLoseArenaOverlay(() =>
            {
                int rebuy_index = Mathf.Min(m_arena.Curr_rebuy_num, m_arena.Arena_matches[stage - 1].Rebuys.Count - 1);

                rebuy_index = Mathf.Max(rebuy_index, 0);

                int curr_rebuy = 0;

                if (m_arena != null)
                    if (m_arena.Arena_matches[stage - 1].Rebuys.Count > 0)
                        curr_rebuy = m_arena.Arena_matches[stage - 1].Rebuys[rebuy_index];
                    else
                        curr_rebuy = 0;

                if (curr_rebuy != 0)
                {
                    m_last_elimination_rebuy = curr_rebuy;
                    m_last_elimination_stage = stage;
                    m_arena_lobby_screen.ShowRebuy(m_last_elimination_rebuy, m_last_elimination_stage, RebuyRequested, QuitRebuyRequested);
                }
                else
                {
                    ModelManager.Instance.RemoveActiveArena(m_active_arena.GetArena().GetID());
                    m_active_arena = null;
                    m_arena_lobby_screen.Eliminated();
                }
            });
            m_back_to_lobby_reason = ArenaScreen.BackFromReason.lose;
            int gap = 0;
            m_lose_arena_overlay.SetData(gap, stage, arena_name, lose_coins, lose_xp, LobbyClicked, NewGameClicked, () =>
            {
                //TODO ALON - POST ANIMATION SHOW CHALLENGE
            });

        }
        public void RebuyRequested(Action action_completed)
        {
            int curr_stage = m_active_arena.GetCurrStage();
            int rebuy_index = Mathf.Min(m_arena.Curr_rebuy_num, m_arena.Arena_matches[curr_stage - 1].Rebuys.Count - 1);
            int curr_rebuy = m_arena.Arena_matches[curr_stage - 1].Rebuys[rebuy_index];

            m_arena.Curr_rebuy_num++;
            //attempting to buy in to the match - we need to check if they have enough money and if so - reduce it and start the game

            if (ModelManager.Instance.GetUser().GetCoins() < curr_rebuy)
            {
                (IMSController.Instance as SpadesIMSController).HandleInsufficientFunds(curr_rebuy - ModelManager.Instance.GetUser().GetCoins());

                action_completed();
            }
            else
            {
                CardGamesCommManager.Instance.ArenaBuyIn(1, m_active_arena.GetArena().GetID(), (bool success, int curStage, int newCoinsBalance) =>
                {
                    if (success)
                    {
                        ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);
                        LobbyTopController.Instance.UpdateCoinsText();
                        m_arena_lobby_screen.RebuildArenaStages(true);
                        PopupManager.Instance.HidePopup();
                    }

                    action_completed();

                });
            }
        }



        public void QuitRebuyRequested(bool isClose)
        {
            if (isClose)
            {
                ModelManager.Instance.RemoveActiveArena(m_active_arena.GetArena().GetID());
                m_active_arena = null;
                m_arena_lobby_screen.RebuildArenaStages(false, isClose);
                return;
            }
            ArenaBuyWarningPopup arenaBuyWarning = CardGamesPopupManager.Instance.ShowArenaBuyWarning();

            arenaBuyWarning.SetData(SpadesArenaController.Instance.GetCurrentRebuy(), () =>
            {
                ModelManager.Instance.RemoveActiveArena(m_active_arena.GetArena().GetID());
                m_active_arena = null;
                m_arena_lobby_screen.RebuildArenaStages(false, isClose);
            }, () => { SpadesArenaController.Instance.RebuyRequested(() => { }); });
        }


        public int Entry_highest_gem
        {
            get
            {
                return m_entry_highest_gem;
            }
        }
    }
}
