using spades.models;

namespace spades.controllers
{

    /// <summary>
    /// Spades state controller
    /// </summary>
    public class SpadesStateController : CardGamesStateController
    {
        public static new SpadesStateController Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;
            }
        } 

        public PlayingMode GetPlayingMode()
        {
            return (PlayingMode)PlayerPrefs.GetInt(user_id + "playing_mode", (int)PlayingMode.Partners);
        }

        public void SetPlayingMode(PlayingMode pm)
        {
            PlayerPrefs.SetInt(user_id + "playing_mode", (int)pm);
        }


    }
}

