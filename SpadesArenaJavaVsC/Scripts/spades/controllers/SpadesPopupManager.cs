using System;

namespace spades.controllers
{
    public class SpadesPopupManager : CardGamesPopupManager
    {
        [SerializeField] GameObject m_round_summary_partners = null;
        [SerializeField] GameObject m_round_summary_solo = null;

        [SerializeField] protected GameObject m_age_verification_popup = null;
        [SerializeField] GameObject m_slot_popup = null;

        //used for closing the winLose view when the mini game comes
        private Action mini_game_initiated;

        public static new SpadesPopupManager Instance;


        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
                Instance = this;
        }

        protected override void Start()
        {
            base.Start();
            LobbyController.Instance.OnLogin += () =>
            {
                // Remove all popups except popups that include the facebook connect button
                PopupManager.Instance.RemovePopups(
                    typeof(SettingsPopup),
                    typeof(SocialPopup),
                    typeof(NewPlayerProfilePopup));
            };
        }


        public void ShowC4PQuitPopup(AddMode addMode, string yes_title, string no_title, Action yes_action, Action no_action = null)
        {
            GameObject popup = (GameObject)Instantiate(m_c4pQuit);
            C4PQuitPopup c4pQuitPopup = popup.GetComponent<C4PQuitPopup>();
            c4pQuitPopup.SetTexts(yes_title, no_title);
            c4pQuitPopup.Yes_action = yes_action;
            c4pQuitPopup.No_action = no_action;

            AddPopup(popup, addMode);
        }
        // from the buy coins button in unity
        public void ShowCashierPopup()
        {
            ShowCashierIMSPopup(AddMode.ShowAndRemove);
        }

        public void ShowCashierPopup(AddMode addMode = AddMode.ShowAndRemove)
        {
            ShowCashierIMSPopup(addMode);
        }

        public void ShowSlotPopup(PopupClosedDelegate close_delegate = null)
        {
            GameObject popup = (GameObject)Instantiate(m_slot_popup);
            SlotPopup slot_popup_script = popup.GetComponent<SlotPopup>();
            slot_popup_script.InitMachine();
            AddPopup(popup, AddMode.DontShowIfPopupShown, close_delegate);
        }

        public new SpadesIMSOpenTablePopup ShowIMSOpenTablePopup(AddMode add_mode, IMSInteractionZone iZone, PopupClosedDelegate close_delegate = null)
        {
            return base.ShowIMSOpenTablePopup(add_mode, iZone, close_delegate) as SpadesIMSOpenTablePopup;
        }

        public void ShowRoundSummary(bool IsSolo, Action<Action> Next_button, bool match_ended, bool prev_results, bool menu_results = false, PopupClosedDelegate close_delegate = null)
        {
            GameObject popup;

            if (IsSolo)
            {
                popup = (GameObject)Instantiate(m_round_summary_solo);
                popup.GetComponent<RoundSummarySoloPopup>().Next_clicked = Next_button;
                popup.GetComponent<RoundSummarySoloPopup>().SetMatchEnded(match_ended, prev_results, menu_results);
            }
            else
            {
                popup = (GameObject)Instantiate(m_round_summary_partners);
                popup.GetComponent<RoundSummaryPartnerPopup>().Next_clicked = Next_button;
                popup.GetComponent<RoundSummaryPartnerPopup>().SetMatchEnded(match_ended, prev_results, menu_results);
            }

            AddPopup(popup, AddMode.ShowAndRemove, close_delegate);

        }

        public void ShowAgeVerificationPopup(AddMode add_mode, PopupClosedDelegate close_delegate, Action age_confirmed)
        {
            GameObject popup = (GameObject)Instantiate(m_age_verification_popup);
            AgeVerificationPopup avp = popup.GetComponent<AgeVerificationPopup>();
            avp.SetCallback(age_confirmed);
            AddPopup(popup, add_mode, close_delegate);
        }


        public void ShowProfilePopup(bool disableEdit = false)
        {
            GameObject popup = (GameObject)Instantiate(m_profile);
            AddPopup(popup, AddMode.DontShowIfPopupShown);

            popup.GetComponent<NewPlayerProfilePopup>().SetData(disableEdit, null, false);
        }


        public NewPlayerProfilePopup ShowOtherProfilePopup()
        {
            GameObject popup = (GameObject)Instantiate(m_profile);
            AddPopup(popup, AddMode.DontShowIfPopupShown);
            return popup.GetComponent<NewPlayerProfilePopup>();
        }

        public override void ShowSettingsPopup(AddMode add_mode = AddMode.ShowAndRemove, bool disable_FB_button = false)
        {
            GameObject popup = (GameObject)Instantiate(m_settings_popup);
            popup.GetComponent<SettingsPopup>().Disable_fb_connect_button = disable_FB_button;
            AddPopup(popup, add_mode, null);
        }

        public MESPopup ShowPromotionPopup(AddMode add_mode, MESMaterial m = null, PopupClosedDelegate close_delegate = null, PopupClosedDelegate cancel_delegate = null)
        {

            GameObject popup = (GameObject)Instantiate(m_promotion_popup);
            MESPopup popupPromoScript = popup.GetComponent<MESPopup>();

            popupPromoScript.InitPopup(m, cancel_delegate);

            AddPopup(popup, add_mode, close_delegate);

            return popupPromoScript;
        }

        public GameObject Round_summary_partners
        {
            get
            {
                return m_round_summary_partners;
            }
        }

        public GameObject Round_summary_solo
        {
            get
            {
                return m_round_summary_solo;
            }
        }

        public Action Mini_game_initiated { get => mini_game_initiated; set => mini_game_initiated = value; }
    }
}
