using System.Collections.Generic;

namespace spades.controllers
{

    public class SpadesSoundsController : CardGamesSoundsController
    {
        [SerializeField] AudioClip High_Card = null;

        [SerializeField] AudioClip Player_Bid = null;
        [SerializeField] AudioClip Spades_Broken_Cards_Move = null;
        [SerializeField] AudioClip Spades_Broken = null;


        [SerializeField] AudioClip m_fireworks_shot1 = null;
        [SerializeField] AudioClip m_fireworks_shot3 = null;
        [SerializeField] AudioClip m_fireworks_burst1 = null;

        [SerializeField] AudioClip m_champaigne = null;

        protected List<AudioSource> m_fireworks_sources = new List<AudioSource>();


        public static new SpadesSoundsController Instance;

        protected override void Awake()
        {
            base.Awake();
            Instance = this;

        }

        public override void Start()
        {
            base.Start();
        }

        public void PlayerBid()
        {
            PlayClip(Player_Bid);
        }

        public void SpadesBroken()
        {
            PlayClip(Spades_Broken);
        }

        public void HighCard()
        {
            PlayClip(High_Card);
        }

        public void PlayFireWorksSFX(bool start)
        {
            if (start)
            {
                m_fireworks_sources.Add(PlayClip(m_fireworks_shot1, true));
                m_fireworks_sources.Add(PlayClip(m_fireworks_shot3, true));
                m_fireworks_sources.Add(PlayClip(m_fireworks_burst1, true));
            }
            else
            {
                if (m_fireworks_sources.Count > 0)
                {
                    StopClip(m_fireworks_sources[0]);
                    StopClip(m_fireworks_sources[1]);
                    StopClip(m_fireworks_sources[2]);
                    m_fireworks_sources.Clear();
                }
            }

        }

        public void PlaySFXCoin()
        {
            PlayClip(Coins_Balancer, false);
        }

        public void Champaigne()
        {
            PlayClip(m_champaigne, false);
        }



        public void SpadesBrokenCardsMove()
        {
            PlayClip(Spades_Broken_Cards_Move);
        }

   
    }
}
