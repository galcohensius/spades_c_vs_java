using System.Collections;
using spades.models;
using System.Collections.Generic;

namespace spades.controllers
{
    public class ChatAIController : MonoBehaviour
    {
        public static ChatAIController Instance;

        [SerializeField] MAvatarView m_NAvatarView = null;
        [SerializeField] MAvatarView m_EAvatarView = null;
        [SerializeField] MAvatarView m_WAvatarView = null;

        private List<OptionSpan> m_opponentOptionSpans = null;
        private List<OptionSpan> m_partnerOptionSpans = null;
        private List<OptionSpan> m_commonOptionSpans = null;

        private Dictionary<int, List<ChatResponse>> m_opponentBotTriggeredResponsesSpanDictionary = null;
        private Dictionary<int, List<ChatResponse>> m_partnerBotTriggeredResponsesSpanDictionary = null;
        private Dictionary<int, List<ChatResponse>> m_commonBotTriggeredResponsesSpanDictionary = null;

        private bool m_areDictionariesInitialized = false;
        private bool m_isLocked = false;

        private void Awake()
        {
            Instance = this;

        }

        private void Start()
        {
            m_isLocked = false;

            if (!m_areDictionariesInitialized)
            {
                GenerateAIChatResponseOptionSpansFromJson();
                InitTargetBotTriggeredResponsesSpanDictionary(m_opponentBotTriggeredResponsesSpanDictionary, ResponseOptionSpanTarget.Opponent);
                InitTargetBotTriggeredResponsesSpanDictionary(m_partnerBotTriggeredResponsesSpanDictionary, ResponseOptionSpanTarget.Partner);
                InitTargetBotTriggeredResponsesSpanDictionary(m_commonBotTriggeredResponsesSpanDictionary, ResponseOptionSpanTarget.Common);
                m_areDictionariesInitialized = true;
            }
        }

        public void TriggerAIChatEvent(int triggerID, PlayingMode playingMode)
        {
            try
            {
                if (!m_isLocked)
                {
                    List<ChatResponse> responseSpan = GetResponseSpanForTrigger(m_opponentBotTriggeredResponsesSpanDictionary, triggerID);

                    if (responseSpan != null)
                    {
                        ChatResponse response = responseSpan[Random.Range(0, responseSpan.Count)];
                        InitiateResponse(response, m_WAvatarView);

                        response = responseSpan[Random.Range(0, responseSpan.Count)];
                        InitiateResponse(response, m_EAvatarView);
                    }

                    if (playingMode == PlayingMode.Partners)
                    {
                        responseSpan = GetResponseSpanForTrigger(m_partnerBotTriggeredResponsesSpanDictionary, triggerID);
                    }

                    if (responseSpan != null)
                    {
                        ChatResponse response = responseSpan[Random.Range(0, responseSpan.Count)];
                        InitiateResponse(response, m_NAvatarView);
                    }

                    StartCoroutine(InitiateTimedLock());
                }
            }
            catch(System.Exception e)
            {
                LoggerController.Instance.LogError("Cannot trigger AI chat event\n"+ e);
            }

        }

        private List<ChatResponse> GetResponseSpanForTrigger(Dictionary<int, List<ChatResponse>> targetBotTriggeredResponsesSpanDictionary, int triggerID)
        {
            List<ChatResponse> responseSpan = null;

            if (targetBotTriggeredResponsesSpanDictionary.ContainsKey(triggerID))
            {
                responseSpan = targetBotTriggeredResponsesSpanDictionary[triggerID];
            }
            else
            {
                if (m_commonBotTriggeredResponsesSpanDictionary.ContainsKey(triggerID))
                {
                    responseSpan = m_commonBotTriggeredResponsesSpanDictionary[triggerID];
                }
            }

            return responseSpan;
        }

        private void InitiateResponse(ChatResponse response, MAvatarView targetAvatar)
        {
            switch (response.ResponseType)
            {
                case ResponseType.Animation:
                    targetAvatar.PlayAnimation((MAvatarView.AvatarAnimationStateTrigger)(response.ResponseID), Random.Range(1.5f, 5f));
                    break;

                case ResponseType.SoloChat:
                   // targetAvatar.ShowChatText(TableChatController.Instance.SoloChatModel.FindMessageById(response.ResponseID), Random.Range(1.5f, 5f));
                    break;

                case ResponseType.PartnerChat:
                   // targetAvatar.ShowChatText(TableChatController.Instance.PartnersChatModel.FindMessageById(response.ResponseID), Random.Range(1.5f, 5f));
                    break;

                case ResponseType.None:
                default:
                    // Do Nothing !
                    break;
            }
        }

        private IEnumerator InitiateTimedLock()
        {
            m_isLocked = true;

            yield return new WaitForSeconds(5f);

            m_isLocked = false;
        }

        private void InitTargetBotTriggeredResponsesSpanDictionary(Dictionary<int, List<ChatResponse>> targetBotTriggeredResponsesSpanDictionary, ResponseOptionSpanTarget optionSpanTarget)
        {
            List<OptionSpan> optionSpanList = GetTargetOptionSpanList(optionSpanTarget);

            foreach (OptionSpan optionSpan in optionSpanList)
            {
                List<ChatResponse> responseSpan = new List<ChatResponse>();

                foreach (ResponseOption responseOption in optionSpan.ResponseOptionList)
                {
                    AddResponseToTargetSpan(responseSpan, responseOption.ChatResponse, responseOption.ProbabilityFactor);
                }

                targetBotTriggeredResponsesSpanDictionary.Add(optionSpan.TriggerID, responseSpan);
            }
        }

        private void AddResponseToTargetSpan(List<ChatResponse> targetResponseSpan, ChatResponse response, int count)
        {
            while (count > 0)
            {
                targetResponseSpan.Add(response);
                count--;
            }
        }

        private List<OptionSpan> GetTargetOptionSpanList(ResponseOptionSpanTarget optionSpanTarget)
        {
            List<OptionSpan> optionSpanList = null;

            switch (optionSpanTarget)
            {
                case ResponseOptionSpanTarget.Opponent:
                    optionSpanList = m_opponentOptionSpans;
                    break;

                case ResponseOptionSpanTarget.Partner:
                    optionSpanList = m_partnerOptionSpans;
                    break;

                case ResponseOptionSpanTarget.Common:
                    optionSpanList = m_commonOptionSpans;
                    break;
            }

            return optionSpanList;
        }

        private void GenerateAIChatResponseOptionSpansFromJson()
        {
            InitNewLists();
            string jsonTextString = (LocalDataController.Instance as CardGamesLocalDataController).AIChatResponseOptionSpansJsonFile.text;
            DeserializeAIChatResponseOptionSpansFromJsonTextString(jsonTextString);
        }

        private void DeserializeAIChatResponseOptionSpansFromJsonTextString(string jsonTextString)
        {
            JSONNode avatarJsonObject = JSONNode.Parse(jsonTextString);
            JSONObject jsonObject = avatarJsonObject.AsObject;

            DeserializeOptionSpanList(jsonObject["opp"].AsArray, m_opponentOptionSpans);
            DeserializeOptionSpanList(jsonObject["par"].AsArray, m_partnerOptionSpans);
            DeserializeOptionSpanList(jsonObject["com"].AsArray, m_commonOptionSpans);
        }

        private void DeserializeOptionSpanList(JSONArray sourceJsonArray, List<OptionSpan> targetOptionSpansList)
        {
            for (int i = 0; i < sourceJsonArray.Count; i++)
            {
                targetOptionSpansList.Add(DeserializeOptionSpan(sourceJsonArray[i].AsObject));
            }
            LoggerController.Instance.Log("Deserialized " + sourceJsonArray.Count + " bot chat responses");
        }

        private OptionSpan DeserializeOptionSpan(JSONObject sourceJsonObject)
        {
            int triggerID = sourceJsonObject["tID"].AsInt;
            List<ResponseOption> responseOptionList = DeserializeResponseOptionList(sourceJsonObject["oSl"].AsArray);
            AddRemainingNoneResponseToTargetResponseOptionList(responseOptionList);

            return new OptionSpan(triggerID, responseOptionList);
        }

        private List<ResponseOption> DeserializeResponseOptionList(JSONArray sourceJsonArray)
        {
            List<ResponseOption> responseOptionList = new List<ResponseOption>();

            for (int i = 0; i < sourceJsonArray.Count; i++)
            {
                responseOptionList.Add(DeserializeResponseOption(sourceJsonArray[i].AsObject));
            }

            return responseOptionList;
        }

        private ResponseOption DeserializeResponseOption(JSONObject sourceJsonObject)
        {
            int probabilityFactor = sourceJsonObject["pF"].AsInt;
            ChatResponse chatResponse = DeserializeChatResponse(sourceJsonObject["cR"].AsObject);

            return new ResponseOption(probabilityFactor, chatResponse);
        }

        private ChatResponse DeserializeChatResponse(JSONObject sourceJsonObject)
        {
            int responseID = sourceJsonObject["rID"].AsInt;
            ResponseType responseType = (ResponseType)sourceJsonObject["rT"].AsInt;

            return new ChatResponse(responseID, responseType);
        }

        private void AddRemainingNoneResponseToTargetResponseOptionList(List<ResponseOption> targetResponseOptionList)
        {
            int noneResponseProbabilityFactor = 100;

            foreach (ResponseOption responseOption in targetResponseOptionList)
            {
                noneResponseProbabilityFactor -= responseOption.ProbabilityFactor;
            }

            ChatResponse noneChatResponse = new ChatResponse(-1, ResponseType.None);
            targetResponseOptionList.Add(new ResponseOption(noneResponseProbabilityFactor, noneChatResponse));
        }

        private void InitNewLists()
        {
            m_opponentOptionSpans = new List<OptionSpan>();
            m_partnerOptionSpans = new List<OptionSpan>();
            m_commonOptionSpans = new List<OptionSpan>();
            m_opponentBotTriggeredResponsesSpanDictionary = new Dictionary<int, List<ChatResponse>>();
            m_partnerBotTriggeredResponsesSpanDictionary = new Dictionary<int, List<ChatResponse>>();
            m_commonBotTriggeredResponsesSpanDictionary = new Dictionary<int, List<ChatResponse>>();
        }
    }

    public class OptionSpan
    {
        public int TriggerID { get; private set; }
        public List<ResponseOption> ResponseOptionList { get; private set; }

        public OptionSpan(int i_triggerID, List<ResponseOption> i_responseOptionList)
        {
            TriggerID = i_triggerID;
            ResponseOptionList = i_responseOptionList;
        }
    }

    public class ResponseOption
    {
        public int ProbabilityFactor { get; private set; }
        public ChatResponse ChatResponse { get; private set; }

        public ResponseOption(int i_probabilityFactor, ChatResponse i_chatResponse)
        {
            ProbabilityFactor = i_probabilityFactor;
            ChatResponse = i_chatResponse;
        }
    }

    public class ChatResponse
    {
        public int ResponseID { get; private set; }
        public ResponseType ResponseType { get; private set; }

        public ChatResponse(int i_responseID, ResponseType i_responseType)
        {
            ResponseID = i_responseID;
            ResponseType = i_responseType;
        }
    }

    public enum ResponseType
    {
        None = 0,
        Animation,
        SoloChat,
        PartnerChat
    }

    public enum ResponseOptionSpanTarget
    {
        Opponent,
        Partner,
        Common
    }

    // Events = 100-199 , UserChat = 200-299 , UserAnimation = 300-399
    public enum BotResponseTrigger
    {
        None = 0,
        OpponentNilIsBroken = 100,
        PlayerNilIsBroken = 101,
        PlayerBids6Plus = 102,
        PlayerTakesBagThatMakesPenalty = 103,
        PlayerBidsBlindNil = 104,
        PlayerCutsOpponentAceWithSpade = 105,
        PlayerCutsPartnerAceWithSpade = 106,
        PlayerProtectsPartnerNil = 107,
        PlayerBreaksSpade = 108,
        WellPlayed = 200,
        Hello = 201,
        GoodLuck = 202,
        WhatAreYouDoing = 203,
        GoodCover = 204,
        WayToGoPartner = 205,
        Surprise = 206,
        ShouldHaveWon = 207,
        CursingAnim = 300,
        InLoveAnim = 301
    }
}
