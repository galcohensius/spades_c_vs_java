using spades.models;
using spades.utils;
using cardGames.models;

namespace spades.controllers
{
	/// <summary>
	/// Spades engine - manages a spades game - also holds the deck
	/// </summary>
	public class SpadesEngine
	{
		// Score constants
		const int SOLO_NIL_PENALTY = -50;
		const int PARTNERS_NIL_PENALTY = -100;
		const int BLIND_NIL_PENALTY = -200;
		const int SCORE_PER_TRICK = 10;
        // Variants
        private const int ROYALE_SOLO_NIL_PENALTY = -25;
        private const int ROYALE_PARTNERS_NIL_PENALTY = -50;
        //const float SCORE_PER_TRICK_CALLBREAK = 1;
        //const float SCORE_PER_BAG_CALLBREAK = 0.1f;

        int	m_curr_player_index;
		int	m_curr_trick_num;
		int m_number_of_player_played;
		int m_number_of_player_bid;
		SpadesTable m_table;

		/// <summary>
		/// Starts a new Spades round
		/// </summary>
		public void StartRound ()
		{
			LoggerController.Instance.Log ("Spades engine starting a new round....");

            m_curr_trick_num = 0;
			m_number_of_player_bid = 0;

			m_table = ModelManager.Instance.GetTable() as SpadesTable;
			m_table.SetSpadesBroken (false);

		}

		/// <summary>
		/// Deals a new deck to 4 players, each player gets 13 cards, with or without Jokers
		/// </summary>
		public CardsList[] Deal (PlayingVariant playingVariation)
		{
			CardsList deck = playingVariation == PlayingVariant.Jokers ? CardsListBuilder.FullDeckWithJokers() : CardsListBuilder.FullDeckNoJokers();
			deck.Shuffle ();

			CardsList[] result = new CardsList[4];
			for (int i = 0; i < 4; i++)
			{
				result [i] = deck.RemoveCardsFromEnd (13);
			}
			return result;
		}

		/// <summary>
		/// get the message that all dealing was done 
		/// </summary>
		public void SetInitialDealer (int dealer)
		{
			LoggerController.Instance.Log ("Initial dealer: " + dealer);
			//start bidding process - first bidder is left of the dealer
			m_curr_player_index = dealer;
			MoveToNextPlayer ();
		}

		private void MoveToNextPlayer ()
		{
			m_curr_player_index = (m_curr_player_index + 1) % 4;
		}

		
		/// <summary>
		/// Starts the trick.
		/// </summary>
		public void StartTrick ()
		{	
			m_curr_trick_num++;
			m_table.SetLeadingSuit (null);
			m_number_of_player_played = 0;
		    
		    m_table.Thrown_cards = new CardsList() {null,null,null,null};
        }


        /// <summary> -- 
        /// if return value is true - means bidding complete - if false - not complete
        /// </summary>
        public bool PlayerBid ()
		{
			m_number_of_player_bid++;
			MoveToNextPlayer ();
			if (m_number_of_player_bid >= 4)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		/// <summary>
		/// true means trick ended - look at CurrPlayerIndex for the winner - false - trick didn't end yet
		/// </summary>
		public bool PlayerPlayed (Card card)
		{
			m_number_of_player_played++;

			if (m_number_of_player_played == 4)
			{
				//trick ended
				//m_number_of_player_played=0;
				m_curr_player_index = FindTrickWinner ();

				StartTrick ();
				return true;
			}
			else
			{
				if (m_number_of_player_played == 1)
				{
					//set what he played to be suit - makes sense more here and not in the bot logic part
					m_table.SetLeadingSuit (card.Suit);
				}
				MoveToNextPlayer ();

				return false;
			}
		}

		/// <summary>
		/// Checks the score - finds the winner
		/// </summary>
		/// <returns>The winner index</returns>
		int FindTrickWinner ()
		{
			return CardsUtils.FindWinningCardIndex (m_table.GetLeadingSuit (), m_table.Thrown_cards);
		}

		/// <summary>
		/// Checks the round score - will also notify the round controller to create the popup
		/// </summary>

		public void CalcRoundScore ()
		{
			if (((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch ()).GetMatch ().Playing_mode == PlayingMode.Partners)
				CalcPartnerRoundScore ();
			else
				CalcSoloRoundScore ();
		}

		private void CalcSoloRoundScore ()
		{
			LoggerController.Instance.Log ("Calculating results for Solo round " + m_table.GetRoundNumber ());

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch ();
			PlayerRoundResults[]	player_round_results = new PlayerRoundResults[4];

			for (int i = 0; i < 4; i++)
			{
				player_round_results [i] = new PlayerRoundResults ();
				
				if (m_table.GetRoundNumber () > 1)
				{
					player_round_results [i].Round_Score_With_Penalty = active_match.GetLastRoundResults ().GetResults ((SpadesRoundResults.Positions)i).Round_Score_With_Penalty;
					player_round_results [i].Bags_total = active_match.GetLastRoundResults ().GetResults ((SpadesRoundResults.Positions)i).Bags_total;
				}
				else
				{
					player_round_results [i].Round_Score_With_Penalty = 0;
					player_round_results [i].Bags_total = 0;
				}
			}

			for (int i = 0; i < 4; i++)
			{
				player_round_results [i].Round_score = 0;
				player_round_results [i].Bags_round = 0;
				player_round_results [i].Bags_penalty = 0;
				player_round_results [i].Nil_bonus_or_penalty = 0;
				player_round_results [i].Bids = m_table.GetPlayer (i).GetBids ();
				player_round_results [i].Takes = m_table.GetPlayer (i).GetTakes ();
			}

		    for (int i = 0; i < 4; i++)
			{
				//CHECKING TAKES-----
				if (player_round_results [i].Takes >= player_round_results [i].Bids)
				{
					player_round_results [i].Round_score = player_round_results [i].Bids * SCORE_PER_TRICK;
					player_round_results [i].Bags_round = player_round_results [i].Takes - player_round_results [i].Bids;
					player_round_results [i].Round_score += player_round_results [i].Bags_round;
				}
				else
				{
					player_round_results [i].Round_score = player_round_results [i].Bids * -SCORE_PER_TRICK;
				}
				//CHECKING BAGS-----
				var total_bags = player_round_results [i].Bags_total + player_round_results [i].Bags_round;
				player_round_results [i].Bags_penalty = active_match.GetMatch ().BagsPanelty * (total_bags / active_match.GetMatch ().BagsCount);
				player_round_results [i].Bags_total = total_bags % active_match.GetMatch ().BagsCount;

				//CHECKING BONUSES---------
				if (player_round_results [i].Bids == 0)
				{
					if (m_table.GetPlayer (i).GetBlind ())
					{
						if (player_round_results [i].Takes == 0)
							player_round_results [i].Nil_bonus_or_penalty -= BLIND_NIL_PENALTY;
						else
							player_round_results [i].Nil_bonus_or_penalty += BLIND_NIL_PENALTY;
					}
                    else
                    {
                        if (((SpadesActiveMatch) ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale)
                        {
                            if (player_round_results[i].Takes == 0)
                                player_round_results[i].Nil_bonus_or_penalty -= ROYALE_SOLO_NIL_PENALTY;
                            else
                                player_round_results[i].Nil_bonus_or_penalty += ROYALE_SOLO_NIL_PENALTY;
                        }
                        else
                        {
                            if (player_round_results[i].Takes == 0)
                                player_round_results[i].Nil_bonus_or_penalty -= SOLO_NIL_PENALTY;
                            else
                                player_round_results[i].Nil_bonus_or_penalty += SOLO_NIL_PENALTY;
                        }
                    }
                }

				//TOTALING----
				player_round_results [i].Round_score += player_round_results [i].Nil_bonus_or_penalty;
				player_round_results [i].Round_Score_With_Penalty = player_round_results [i].Round_score + player_round_results [i].Bags_penalty;
				//player_round_results [i].Round_Score_With_Penalty = player_round_results [i].Score_round;
			}

			int round_number = m_table.GetRoundNumber ();

			active_match.AddResults (new SpadesRoundResults (player_round_results, round_number));

			//logging results in round logger
			RoundController.Instance.Match_logger.SetRoundResults (player_round_results);

			RoundController.Instance.RoundEndedSolo ();
		}

		private void CalcPartnerRoundScore ()
		{
			LoggerController.Instance.Log ("Calculating results for Partners round " + m_table.GetRoundNumber ());
			SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

			int east_west_prev_total, north_south_prev_total;
			int east_west_curr_bags, north_south_curr_bags;

			if (m_table.GetRoundNumber () > 1)
			{
				east_west_prev_total = active_match.GetLastRoundResults ().GetResults (SpadesRoundResults.Positions.West_East).Round_Score_With_Penalty;
				north_south_prev_total = active_match.GetLastRoundResults ().GetResults (SpadesRoundResults.Positions.North_South).Round_Score_With_Penalty;
				east_west_curr_bags = active_match.GetLastRoundResults ().GetResults (SpadesRoundResults.Positions.West_East).Bags_total;
				north_south_curr_bags = active_match.GetLastRoundResults ().GetResults (SpadesRoundResults.Positions.North_South).Bags_total;
			}
			else
			{
				east_west_prev_total = 0;
				north_south_prev_total = 0;
				east_west_curr_bags = 0;
				north_south_curr_bags = 0;
			}
			int east_west_round_score = 0;
			int north_south_round_score = 0;

			int east_west_round_bags = 0;
			int north_south_round_bags = 0;

			int east_west_bags_penalty = 0;
			int north_south_bags_penalty = 0;

			int east_west_nil_bonus_penalty = 0;
			int north_south_nil_bonus_penalty = 0;

			int east_west_round_total_score = 0;
			int north_south_round_total_score = 0;

			int east_bid = m_table.GetPlayer (2).GetBids ();
			int west_bid = m_table.GetPlayer (0).GetBids ();
			int north_bid = m_table.GetPlayer (1).GetBids ();
			int south_bid = m_table.GetPlayer (3).GetBids ();

			int east_takes = m_table.GetPlayer (2).GetTakes ();
			int west_takes = m_table.GetPlayer (0).GetTakes ();
			int north_takes = m_table.GetPlayer (1).GetTakes ();
			int south_takes = m_table.GetPlayer (3).GetTakes (); 	

			int east_west_bid = east_bid + west_bid;
			int north_south_bid = north_bid + south_bid;

			int east_west_takes = east_takes + west_takes;
			int north_south_takes = north_takes + south_takes; 

			//player bids 0 - his takes dont count, except when both partners bid 0.
			if (east_bid == 0 && west_bid != 0)
				east_west_takes = west_takes;
			if (west_bid == 0 && east_bid != 0)
				east_west_takes = east_takes;
			if (north_bid == 0 && south_bid != 0)
				north_south_takes = south_takes;
			if (south_bid == 0 && north_bid != 0)
				north_south_takes = north_takes;

			//CHECKING TAKES------EAST-WEST
            if (east_west_takes >= east_west_bid)
			{
				east_west_round_score = east_west_bid * SCORE_PER_TRICK;
				east_west_round_bags = east_west_takes - east_west_bid;
				east_west_round_score += east_west_round_bags;
			}
			else
			{
				east_west_round_score = (east_west_bid) * -SCORE_PER_TRICK;
			}

			//CHECKING BAGS------EAST-WEST
			int total_east_west_bags = east_west_curr_bags + east_west_round_bags;
			east_west_bags_penalty = active_match.GetMatch ().BagsPanelty * (total_east_west_bags / active_match.GetMatch ().BagsCount);
			east_west_curr_bags = total_east_west_bags % active_match.GetMatch ().BagsCount;

			//CHECKING BONUSES----------EAST-WEST
			//checking east
			if (east_bid == 0)
			{
				if (m_table.GetPlayer (2).GetBlind ())
				{
					if (east_takes == 0)
					{
						east_west_nil_bonus_penalty -= BLIND_NIL_PENALTY;
					}
					else
					{
						east_west_nil_bonus_penalty += BLIND_NIL_PENALTY;
					}
				}
				else
				{ // not blind
					if (east_takes == 0)
					{
						east_west_nil_bonus_penalty -= ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
					else
					{
						east_west_nil_bonus_penalty += ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
				}
			}


			//checking west
			if (west_bid == 0)
			{
				if (m_table.GetPlayer (0).GetBlind ())
				{
					if (west_takes == 0)
					{
						east_west_nil_bonus_penalty -= BLIND_NIL_PENALTY;
					}
					else
					{
						east_west_nil_bonus_penalty += BLIND_NIL_PENALTY;
					}
				}
				else
				{ // not blind
					if (west_takes == 0)
					{
						east_west_nil_bonus_penalty -= ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
					else
					{
						east_west_nil_bonus_penalty += ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
				}
			}

			//TOTALING---- EAST WEST
			east_west_round_score += east_west_nil_bonus_penalty;
			east_west_round_total_score = east_west_round_score + east_west_bags_penalty;

			//CHECKING TAKES------NORTH SOUTH
			if (north_south_takes >= north_south_bid)
			{
				north_south_round_score = north_south_bid * SCORE_PER_TRICK;
				north_south_round_bags = north_south_takes - north_south_bid;
				north_south_round_score += north_south_round_bags;
			}
			else
			{
				north_south_round_score = (north_south_bid) * -SCORE_PER_TRICK;
			}

			//CHECKING BAGS------EAST-WEST
			int total_north_south_bags = north_south_curr_bags + north_south_round_bags;
			north_south_bags_penalty = active_match.GetMatch ().BagsPanelty * (total_north_south_bags / active_match.GetMatch ().BagsCount);
			north_south_curr_bags = total_north_south_bags % active_match.GetMatch ().BagsCount;

			//CHECKING BONUSES----------NORTH SOUTH
			//checking north
			if (north_bid == 0)
			{
				if (m_table.GetPlayer (1).GetBlind ())
				{
					if (north_takes == 0)
					{
						north_south_nil_bonus_penalty -= BLIND_NIL_PENALTY;
					}
					else
					{
						north_south_nil_bonus_penalty += BLIND_NIL_PENALTY;
					}
				}
				else
				{ // not blind
					if (north_takes == 0)
					{
						north_south_nil_bonus_penalty -= ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
					else
					{
						north_south_nil_bonus_penalty += ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
				}
			}
			

			//checking south
			if (south_bid == 0)
			{
				if (m_table.GetPlayer (3).GetBlind ())
				{
					if (south_takes == 0)
					{
						north_south_nil_bonus_penalty -= BLIND_NIL_PENALTY;
					}
					else
					{
						north_south_nil_bonus_penalty += BLIND_NIL_PENALTY;
					}
				}
				else
				{ // not blind
					if (south_takes == 0)
					{
						north_south_nil_bonus_penalty -= ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
					else
					{
						north_south_nil_bonus_penalty += ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetMatch().Variant == PlayingVariant.Royale
                            ? ROYALE_PARTNERS_NIL_PENALTY
                            : PARTNERS_NIL_PENALTY;
                    }
				}
			}


			//TOTALING---- NORTH_SOUTH
			north_south_round_score += north_south_nil_bonus_penalty;
			north_south_round_total_score = north_south_round_score + north_south_bags_penalty;

			PlayerRoundResults ew = new PlayerRoundResults ();
			PlayerRoundResults ns = new PlayerRoundResults ();

			ew.Bids = east_west_bid;
			ew.Takes = east_west_takes;
			ew.Bags_round = east_west_round_bags;
			ew.Round_score = east_west_round_score;
			ew.Bags_total = east_west_curr_bags;
			ew.Bags_penalty = east_west_bags_penalty;
			ew.Nil_bonus_or_penalty = east_west_nil_bonus_penalty;
			ew.Round_Score_With_Penalty = east_west_round_total_score;
			//ew.Round_Score_With_Penalty = 200;

			ns.Bids = north_south_bid;
			ns.Takes = north_south_takes;
			ns.Bags_round = north_south_round_bags;
			ns.Round_score = north_south_round_score;
			ns.Bags_total = north_south_curr_bags;
			ns.Bags_penalty = north_south_bags_penalty;
			ns.Nil_bonus_or_penalty = north_south_nil_bonus_penalty;
			ns.Round_Score_With_Penalty = north_south_round_total_score;
			//ns.Round_Score_With_Penalty = 200;

			PlayerRoundResults[] player_round_results = { ew, ns };
			int round_number = m_table.GetRoundNumber ();
			active_match.AddResults (new SpadesRoundResults (player_round_results, round_number));

			//logging results in round logger
			RoundController.Instance.Match_logger.SetRoundResults (player_round_results);

			RoundController.Instance.RoundEndedPartners ();
		}

		/// <summary>
		/// CHEAT CHEAT CHEAT
		/// </summary>
		public void EndRoundNowCheat (int winPlace, int points,int bids, int takes)
		{
			SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
			ISpadesMatch match = active_match.GetMatch();

            m_table.GetPlayer(3).SetBids(bids);
            m_table.GetPlayer(3).SetTakes(takes);
            m_table.GetPlayer(0).SetTakes(13 - takes);

            m_table.GetPlayer(1).SetTakes(0);
            m_table.GetPlayer(2).SetTakes(0);

            if (m_table != null && match != null)
			{
				if (match.Playing_mode == PlayingMode.Partners)
				{

					SpadesRoundResults results = new SpadesRoundResults (PlayingMode.Partners);
					results.SetRoundNumber (m_table.GetRoundNumber ());

                   
                    
                    if (winPlace == 1)
                    {
                        results.SetResults (SpadesRoundResults.Positions.North_South, new PlayerRoundResults (points,bids,takes));
                        results.SetResults (SpadesRoundResults.Positions.West_East, new PlayerRoundResults (0,0,0));

					}
					else
					{
						results.SetResults (SpadesRoundResults.Positions.North_South, new PlayerRoundResults (0,bids,takes));
                        results.SetResults (SpadesRoundResults.Positions.West_East, new PlayerRoundResults (points,0,0));
					}

					active_match.AddResults (results);

					// Cancel all error messages
					ManagerView.Instance.ShowErrorPopup = false;
					ManagerView.Instance.Table.GetComponent<TableView> ().StopAllCoroutines ();

					RoundController.Instance.RoundEndedPartners ();

					RoundController.Instance.CheatingStarted ();

				}
				else
				{

					SpadesRoundResults results = new SpadesRoundResults (PlayingMode.Solo);
					results.SetRoundNumber (m_table.GetRoundNumber ());

					if (winPlace == 1)
					{
                        results.SetResults (SpadesRoundResults.Positions.South, new PlayerRoundResults (points,bids,takes));
						results.SetResults (SpadesRoundResults.Positions.East, new PlayerRoundResults (0,0,0));
                        results.SetResults (SpadesRoundResults.Positions.West, new PlayerRoundResults (-10, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.North, new PlayerRoundResults (-60, 0, 0));

					}
					else if (winPlace == 2)
					{
                        results.SetResults (SpadesRoundResults.Positions.South, new PlayerRoundResults (0, bids, takes));
                        results.SetResults (SpadesRoundResults.Positions.East, new PlayerRoundResults (-10, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.West, new PlayerRoundResults (-20, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.North, new PlayerRoundResults (points,0,0));
					}
					else if (winPlace == 3)
					{
                        results.SetResults (SpadesRoundResults.Positions.South, new PlayerRoundResults (50, bids, takes));
                        results.SetResults (SpadesRoundResults.Positions.East, new PlayerRoundResults (-10, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.West, new PlayerRoundResults (50, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.North, new PlayerRoundResults (points,0,0));
					}
					else
					{
                        results.SetResults (SpadesRoundResults.Positions.South, new PlayerRoundResults (-20, bids, takes));
                        results.SetResults (SpadesRoundResults.Positions.East, new PlayerRoundResults (-10, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.West, new PlayerRoundResults (0, 0, 0));
                        results.SetResults (SpadesRoundResults.Positions.North, new PlayerRoundResults (points,0,0));
					}

					active_match.AddResults (results);

					// Cancel all error messages
					ManagerView.Instance.ShowErrorPopup = false;
					ManagerView.Instance.Table.GetComponent<TableView> ().StopAllCoroutines ();

					RoundController.Instance.RoundEndedSolo ();

					RoundController.Instance.CheatingStarted ();


				}



			}

		}

		
		public int Curr_trick_num {
			get {
				return m_curr_trick_num;
			}
		}

		public int Curr_player_index {
			get {
				return m_curr_player_index;
			}

		}

		public bool IsBidding {
			get {
				return m_curr_trick_num == 0;
			}
		}

        public int Number_of_player_played { get => m_number_of_player_played; set => m_number_of_player_played = value; }
    }
}
