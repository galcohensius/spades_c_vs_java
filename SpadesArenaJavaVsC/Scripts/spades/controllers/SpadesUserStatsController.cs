﻿using spades.models;
using System;

namespace spades.controllers
{

    public class SpadesUserStatsController : UserStatsController
    {

        protected override void Awake()
        {
            base.Awake();
        }


        //TODO: could be CardGame? May be true for Gin...
        /// <summary>
        /// Updates all statistics related to ending a round
        /// </summary>
        public void UpdateEndRound(bool quit, int bid, int takes) {
			SpadesUserStats stats = ModelManager.Instance.GetUser().Stats as SpadesUserStats ;
			stats.RoundPlayed++;

			if (bid == takes && !quit) {
				stats.ExactBids++;
			} else if (bid < takes) {
				stats.LesserBids++;
			} else {
				stats.GreaterBids++;
			}

			if (bid == 0) {
				stats.NilBids++;
				if (takes == 0 && !quit) {
					stats.NilTakes++;
				}
			}

			SaveUserStatsLocaly (stats);
		}

        protected override void HandleSaveUserStatsJson(string statsStr)
        {
            SpadesStateController.Instance.SaveUserStatsJson(statsStr);
        }

        /// <summary>
        /// Updates all the statistics related to ending a match, single or Arena
        /// </summary>
        public override void UpdateEndMatch(bool win, int coinsWon=0)
        {
            SpadesUserStats stats = GetUserStats() as SpadesUserStats;

            if (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Playing_mode == PlayingMode.Solo)
			{
				if (win)
				{
					stats.MatchWinsSolo++;
					stats.FiveGamesState.Insert(0, 1);
				}
				else
				{
					stats.MatchLosesSolo++;
					stats.FiveGamesState.Insert(0, 0);
				}
			}
			else
			{
				if (win)
				{
					stats.MatchWinsPartner++;
					stats.FiveGamesState.Insert(0, 1);
				}
				else
				{
					stats.MatchLosesPartner++;
					stats.FiveGamesState.Insert(0, 0);
				}
			}

			if (stats.FiveGamesState.Count > 5) {
				stats.FiveGamesState.RemoveAt (4);
			}

			stats.CoinsWon += coinsWon;

			stats.BiggestCoinsWin = Mathf.Max (stats.BiggestCoinsWin, coinsWon);


            // Session related
            if (win) {
                stats.SessionWins++;
                stats.SessionWinsInRow++;
                stats.SessionLosesInRow = 0;
            } else {
                stats.SessionLoses++;
                stats.SessionLosesInRow++;
                stats.SessionWinsInRow = 0;
            }

			SaveUserStatsLocaly (stats);
		
		}

		public override UserStats ParseUserStats(JSONNode statsJson)
        {
            SpadesUserStats stats = base.ParseUserStats(statsJson) as SpadesUserStats;

            String str = statsJson["flg"];

            if (statsJson ["mws"] != null)
			{
				stats.MatchWinsSolo = statsJson ["mws"].AsInt;
				stats.MatchLosesSolo = statsJson ["mls"].AsInt;

				stats.MatchWinsPartner = statsJson ["mwp"].AsInt;
				stats.MatchLosesPartner = statsJson ["mlp"].AsInt;

				stats.RoundPlayed = statsJson ["rp"].AsInt;
				stats.ExactBids = statsJson ["ebs"].AsInt;

			}

			stats.GreaterBids = statsJson ["gb"].AsInt;
			stats.LesserBids = statsJson ["lb"].AsInt;
			stats.NilBids = statsJson ["nb"].AsInt;
			stats.NilTakes = statsJson ["nt"].AsInt;

			return stats;
		}

        protected override UserStats CreateUserStats()
        {
            return new SpadesUserStats();
        }

        public override JSONNode SerializeUserStats(UserStats stats)
        {
            JSONNode result = base.SerializeUserStats(stats);

            SpadesUserStats spadesStats = stats as SpadesUserStats;

            result["mws"].AsInt = spadesStats.MatchWinsSolo;
            result["mls"].AsInt = spadesStats.MatchLosesSolo;

            result["mwp"].AsInt = spadesStats.MatchWinsPartner;
            result["mlp"].AsInt = spadesStats.MatchLosesPartner;

            result["ebs"].AsInt = spadesStats.ExactBids;
            result["gb"].AsInt = spadesStats.GreaterBids;
            result["lb"].AsInt = spadesStats.LesserBids;
            result["nb"].AsInt = spadesStats.NilBids;
            result["nt"].AsInt = spadesStats.NilTakes;

            return result;
        }

        public JSONNode SerializeUserStats(SpadesUserStats stats) {
			JSONObject result = new JSONObject ();

            result ["ts"].AsInt = stats.SaveTimeStamp;

			result ["mws"].AsInt = stats.MatchWinsSolo;
			result ["mls"].AsInt = stats.MatchLosesSolo;

			result ["mwp"].AsInt = stats.MatchWinsPartner;
			result ["mlp"].AsInt = stats.MatchLosesPartner;

			result ["rp"].AsInt = stats.RoundPlayed;


			result ["ebs"].AsInt = stats.ExactBids;
			result ["gb"].AsInt = stats.GreaterBids;
			result ["lb"].AsInt = stats.LesserBids;
			result ["nb"].AsInt = stats.NilBids;
			result ["nt"].AsInt = stats.NilTakes;

			result ["ap"].AsInt = stats.ArenaPlayed;
			result ["cw"].AsInt = stats.CoinsWon;

			result ["bw"].AsInt = stats.BiggestCoinsWin;

			for (int i = 0; i < stats.FiveGamesState.Count; i++) {
				result ["flg"] += stats.FiveGamesState[i].ToString();
			}

			return result;
		}	


        protected override string LoadUserStatsJson()
        {
            return SpadesStateController.Instance.LoadUserStatsJson();
        }
    }
}