﻿using System;
using spades.models;
using System.Collections.Generic;

namespace spades.controllers
{

	public class ModeSwipe : MonoBehaviour
	{


		//[SerializeField]Transform		m_items=null;
		[SerializeField]GameObject		m_solo_highlight=null;
		[SerializeField]GameObject		m_partner_highlight=null;

		[SerializeField]RectTransform 	m_middle_layer=null;

		const float ITEMS_DISTANCE = 300f;

		float combinedX = 0;

		Vector3 m_prevPos;
		Vector3 delta_dist = new Vector3 (0, 0, 0);

		//bool mouseUp = false;
		//bool mouseStart = false;
		bool m_enable_swipe = false;

		bool m_locked = false;
		bool m_popup_locked = false;
		bool m_screen_locked = false;
		bool m_overlay_locked = false;


		float m_start_drag_x = 0;


		[SerializeField]List<WorldItemView> m_world_script = new List<WorldItemView>();

		int				m_prev_target_x=0;



		private PlayingMode		m_playing_mode; 


		void HandlePopupLockChanged (bool locked)
		{
			m_popup_locked = locked;

			m_locked = (m_popup_locked || m_overlay_locked || m_screen_locked);

		}

		void HandleOverlayLockChanged (bool locked)
		{
			m_overlay_locked = locked;

			m_locked = (m_popup_locked || m_overlay_locked || m_screen_locked);

		}

		void HandleScreenLockChanged (bool locked)
		{
			m_screen_locked = locked;

			m_locked = (m_popup_locked || m_overlay_locked || m_screen_locked);

		}

		void Start()
		{
			
			PopupManager.Instance.OnPopupShown += HandlePopupLockChanged;
			SpadesOverlayManager.Instance.OnOverlayShown += HandleOverlayLockChanged;
			SpadesScreensManager.Instance.OnScreenShown += HandleScreenLockChanged;
		}

		public void AddWorldItem(WorldItemView world_item_view)
		{
			m_world_script.Add (world_item_view);
		}

		public void ResetSwipe (PlayingMode pm)
		{
			
			m_playing_mode = pm;
			combinedX = ManagerView.Instance.Scaler_x / ManagerView.Instance.Scaler_width;
			if (pm == PlayingMode.Partners)
				m_prev_target_x = 0;
			else
				m_prev_target_x = (int)-ITEMS_DISTANCE;

			m_middle_layer.localPosition = new Vector3 (m_prev_target_x, 0, 0);
			
			SetHighlights (pm);

			m_locked = false;
		}


		// Update is called once per frame
		void Update ()
		{

			if (m_enable_swipe && !m_locked)
			{
				#if(!UNITY_EDITOR && !UNITY_WEBGL)

					Vector3 curr_pos=new Vector3();
					if (Input.touchCount > 0)
					{
						curr_pos = new Vector3 (Input.touches[0].position.x * combinedX, 0);
					}

				#else

						Vector3 curr_pos = new Vector3 (Input.mousePosition.x * combinedX, 0);
						
				#endif
				delta_dist = m_prevPos - curr_pos;
				m_middle_layer.localPosition -= delta_dist;
				m_prevPos = curr_pos;
			}
		}

		public void StartSwipe()
		{
			if (m_locked)
				return;


			SetHighlights ();
			m_start_drag_x = m_middle_layer.localPosition.x;

			m_prevPos = new Vector3 (Input.mousePosition.x * combinedX, 0);

			m_enable_swipe = true;

		}

		public void FinsihedSwipe()
		{
			if (m_enable_swipe)
			{
				m_enable_swipe = false;

				float  curr_pos_x =m_middle_layer.localPosition.x;


				//LoggerController.Instance.Log ("Curr Pos: " + curr_pos_x + " Start Drag Pos: " + m_start_drag_x);


				if (curr_pos_x >= m_start_drag_x + ITEMS_DISTANCE / 2)
				{
					SnapBack (m_start_drag_x + ITEMS_DISTANCE);
				}
				else if (curr_pos_x <= m_start_drag_x - ITEMS_DISTANCE / 2)
				{
					SnapBack (m_start_drag_x - ITEMS_DISTANCE);
				}
				else
				{
					SnapBack (m_start_drag_x);
				}
			}
		}

		private void SetHighlights(PlayingMode? pm=null)
		{
			if (pm == null)
			{
				m_partner_highlight.SetActive (false);
				m_solo_highlight.SetActive(false);

			}
			else if (pm == PlayingMode.Partners)
			{
				m_partner_highlight.SetActive (true);
				m_solo_highlight.SetActive(false);
			}
			else if (pm == PlayingMode.Solo)
			{
				m_partner_highlight.SetActive (false);
				m_solo_highlight.SetActive(true);
			}
		}



		private void SnapBack (float target_x)
		{
			SetHighlights (0);

			LoggerController.Instance.Log ("Snap Back TargetX: " + target_x + "Prev TargetX: " + m_prev_target_x);

			//LoggerController.Instance.Log ("Swipe Index: " + LobbyController.Instance.Lobby_view.GetSwipe ().Screen_index);

			if (m_prev_target_x > target_x)
			{
				SwitchPlayingMode ();
				RotateAllPanels(true,SnapBackCompleted);
				m_locked = true;		//This is a lock for not being able to move the bottom slider while its rotating in Itween - this is being released from the SpadesWorld Item View when the Rotate Panel function ends
			}
			else if (m_prev_target_x < target_x)
			{
				SwitchPlayingMode ();
				RotateAllPanels(false,SnapBackCompleted);
				m_locked = true;
			}

			iTween.MoveTo (m_middle_layer.gameObject, iTween.Hash ("x", target_x, "islocal", true, "easeType", "easeOutCubic", "time", .15));

		}


		private void RotateAllPanels(bool left,Action Panels_Spin_Done)
		{
			for (int i = 0; i < m_world_script.Count; i++)
				m_world_script [i].RotatePanels (left,Panels_Spin_Done);
		}

		private void SwitchPlayingMode()
		{
			CardGamesSoundsController.Instance.PlayPanelSelector ();
			if (m_playing_mode == PlayingMode.Partners)
				m_playing_mode = PlayingMode.Solo;
			else
				m_playing_mode = PlayingMode.Partners;
		}

		private void SnapBackCompleted()
		{
			ResetSwipe (m_playing_mode);

		}

		public void SideArrowsClicked(bool left)
		{
			if (m_locked)
				return;
			
			StartSwipe ();

			m_locked = true;
			if (left)
			{
				SnapBack (m_start_drag_x - ITEMS_DISTANCE);
			}
			else
			{
				SnapBack (m_start_drag_x + ITEMS_DISTANCE);
			}
			m_enable_swipe = false;
		}

		public bool Enable_swipe {
			get {
				return m_enable_swipe;
			}
			set {
				m_enable_swipe = value;

			}
		}

	}

}