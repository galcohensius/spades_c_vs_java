using spades.models;
using System;

namespace spades.controllers
{
    public class SpadesMissionController : MissionController
    {

        public const float AUTO_SHOW_CHALLENGE_TIME = 5f;

        public const int OPEN_HEIGHT = 230;
        public const int DONE_HEIGHT = 120;

        [SerializeField] Sprite m_suitcase_closed = null;
        [SerializeField] Sprite m_suitcase_open = null;

        [SerializeField] ChallengeInGameView m_challengeInGameView;

        public static new SpadesMissionController Instance;

        public enum ChallengeType
        {
            Play_X_Games = 1,
            Win_X_Games = 2,
            Win_X_Games_Row = 3,
            Earn_Coins = 4,
            Win_X_Games_Y_Pts = 5,
            Win_X_Games_by_Y_pts = 6,
            Win_X_Games_Y_less_Rounds = 7,
            Win_X_Games_no_bags = 8,
            Meet_Bid_X_Team_Y_Times = 9,
            Meet_Bid_X_Y_Times = 10,
            Bid_Num_Spades_In_Hand = 11,
            Meet_Exact_Bid_X_Team_Y_Times = 12,
            Meet_Exact_Bid_X_Y_Times = 13,
            Take_X_Tricks = 14,
            Take_X_Tricks_Y_Times = 15,
            Earn_X_Points = 18,
            Sum_Bets = 19,
            // FUTURE
            //LevelUp_X_Levels,
            //Invite_X_Frieds

        }


        protected override void Awake()
        {
            base.Awake();
            Instance = this;
        }

        protected override void Start()
        {
            base.Start();
        }

        public override void NewMissionArrived(Mission mission)
        {
            if (mission == null)
                return;

            base.NewMissionArrived(mission);

            LobbyController.Instance.Lobby_view.RegisterToMissionEvents(mission);
            MissionController.Instance.UpdateMissionLobbyIcons();
        }

        public bool GameModeMatchActiveMatch(PlayingMode Challenge_gameMode, Match match = null)
        {
            PlayingMode playingMode = PlayingMode.Partners;

            if (match != null)
                playingMode = (match as ISpadesMatch).Playing_mode;
            else
                playingMode = SpadesModelManager.Instance.GetActiveMatch().GetMatch().Playing_mode;

            if (Challenge_gameMode == PlayingMode.Both)
                return true;
            else if (Challenge_gameMode == PlayingMode.Solo && playingMode == PlayingMode.Solo)
                return true;
            else if (Challenge_gameMode == PlayingMode.Partners && playingMode == PlayingMode.Partners)
                return true;
            else
                return false;

        }

        //no claim flag used for MES events to use the same flow but no show claim - also - when no claim it will not attempt to bring a new mission automatically SR-1955
        public override void MissionButtonClicked(bool claim = true)
        {
            if (!UserOverMissionMinLevel())
            {
                PopupManager.Instance.ShowMessagePopup("CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + m_min_mission_level + "!");
            }
            else
            {

                if (CheckClaimActiveMission() && claim)
                    SpadesOverlayManager.Instance.ShowMissionCollectOverlay(Instance.To_claim_mission);
                else
                    ClaimAllMissionsOrShowMissionPopup(claim);

            }

        }

        private void ClaimAllMissionsOrShowMissionPopup(bool claim)
        {
            if (m_to_claim_mission.Count > 0 && claim)
            {
                SpadesOverlayManager.Instance.ShowMissionCollectOverlay(Instance.To_claim_mission);
            }
            else
            {
                if (ModelManager.Instance.GetMission() != null)
                {
                    //check if Active mission expired
                    if (IsMissionExpired() && claim)
                        StartCoroutine(RequestNewChallanges());
                    else
                        CardGamesPopupManager.Instance.ShowMissionPopup(CardGamesPopupManager.AddMode.DontShowIfPopupShown);
                }
                else
                {
                    StartCoroutine(RequestNewChallanges());
                }

            }
        }


        public override void HandleGetChallengesMissionSuccessful(bool silent = false, bool auto_open_panel_enabled = true)
        {
            if (silent == false && LobbyController.Instance.Lobby_view.gameObject.activeSelf && auto_open_panel_enabled)
            {
                CardGamesPopupManager.Instance.ShowMissionPopup(PopupManager.AddMode.ShowAndRemove);

            }

        }


        public override bool ShouldShowMissionButtonInGame()
        {
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            // Check min level for missions
            if (!UserOverMissionMinLevel())
                return false;

            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch();

            // No missions in Arena for now
            if (activeMatch != null)
                if (activeMatch.IsSingleMatch == false)
                    return false;


            if (mission != null)
            {
                // Check server flag for active mission on this match
                if (m_curr_game_contributing_to_challenge)
                    return true;
                else
                    return false;
                /*
                //make sure mission isnt complete and its not expired
                if ((mission.End_date - DateTime.Now).TotalSeconds > 0 && mission.Status == MissionStatus.InProgress)
                {
                    return true;
                }
                else
                    return false;*/

            }
            else
            {
                return false;
            }
        }

        //this is a ref to the array of images indexes
        //0- money image
        //1 - bid
        //2 - play missions
        private int ChallengeTypeToIndex(ChallengeType challengeType)
        {
            switch (challengeType)
            {
                case ChallengeType.Earn_Coins:
                case ChallengeType.Sum_Bets:
                    return 0;
                case ChallengeType.Play_X_Games:
                case ChallengeType.Win_X_Games:
                case ChallengeType.Win_X_Games_Row:
                case ChallengeType.Win_X_Games_Y_Pts:
                case ChallengeType.Win_X_Games_by_Y_pts:
                case ChallengeType.Win_X_Games_Y_less_Rounds:
                case ChallengeType.Win_X_Games_no_bags:
                case ChallengeType.Earn_X_Points:
                    return 2;
                case ChallengeType.Take_X_Tricks_Y_Times:
                case ChallengeType.Meet_Bid_X_Y_Times:
                case ChallengeType.Meet_Bid_X_Team_Y_Times:
                case ChallengeType.Meet_Exact_Bid_X_Y_Times:
                case ChallengeType.Meet_Exact_Bid_X_Team_Y_Times:
                case ChallengeType.Bid_Num_Spades_In_Hand:
                case ChallengeType.Take_X_Tricks:
                    return 1;
            }

            return 0;
        }

        public Challenge GetChallengeBySpadesMatch(SpadesSingleMatch match)
        {
            Mission mission = ModelManager.Instance.GetMission() as SpadesMission;
            if (mission == null)
                return null;

            SpadesTableFilterData tableFilterData = (mission.GetActiveChallenge() as SpadesChallenge).SpadesTableFilterData;

            if (IsChallengeActiveInActiveMatch(mission.GetActiveChallenge() as SpadesChallenge, match) &&
                tableFilterData.IsSubVariantContained(match.SubVariant) &&
                tableFilterData.IsVariantContained(match.Variant))
            {
                if (tableFilterData.PlayingMode == PlayingMode.Both ||
                    tableFilterData.PlayingMode == match.Playing_mode)
                {
                    if (tableFilterData.SpecialOnly)
                    {
                        if (tableFilterData.SpecialOnly == match.Is_special)
                            return mission.GetActiveChallenge();
                        return null;
                    }
                    else
                        return mission.GetActiveChallenge();
                }
            }
            return null;
        }

        public override Sprite GetActiveMissionIconFromModelManager()
        {
            return m_active_mission_images[ChallengeTypeToIndex((ModelManager.Instance.GetMission().GetChallenge(ModelManager.Instance.GetMission().Active_challenge_index) as SpadesChallenge).Ch_type)];
        }

        public override Sprite GetMissionIcon(Challenge challenge)
        {
            return m_active_mission_images[ChallengeTypeToIndex((challenge as SpadesChallenge).Ch_type)];
        }
        public override string GetTitleFromChallenge(Challenge challenge)
        {
            switch ((challenge as SpadesChallenge).Type)
            {
                case ChallengeType.Earn_Coins:
                    return "EARN COINS";

                case ChallengeType.Play_X_Games:
                    return "PLAY GAMES";

                case ChallengeType.Win_X_Games:
                    return "WIN GAMES";

                case ChallengeType.Win_X_Games_Row:
                    return string.Format("{0:WIN;WINS} IN A ROW", challenge.Thresholds[0]);

                case ChallengeType.Win_X_Games_Y_Pts:
                    return string.Format("WIN WITH {0} PTS", challenge.Thresholds[1]);

                case ChallengeType.Win_X_Games_by_Y_pts:
                    return string.Format("WIN BY {0} PTS", challenge.Thresholds[1]);

                case ChallengeType.Win_X_Games_Y_less_Rounds:
                    return string.Format(pluralFormatProvider, "WIN IN {0:ROUND;ROUNDS}", challenge.Thresholds[1]);

                case ChallengeType.Win_X_Games_no_bags:
                    return "NO BAGS WIN";

                case ChallengeType.Take_X_Tricks_Y_Times:
                    return string.Format(pluralFormatProvider, "{0:TRICK;TRICKS} IN A GAME", challenge.Thresholds[0]);

                case ChallengeType.Meet_Bid_X_Y_Times:
                    return string.Format("ACHIEVE BID OF {0}", challenge.Thresholds[1] == 0 ? "NIL" : challenge.Thresholds[1].ToString());

                case ChallengeType.Meet_Exact_Bid_X_Y_Times:
                    return string.Format("EXACT BIDS OF {0}", challenge.Thresholds[1] == 0 ? "NIL" : challenge.Thresholds[1].ToString());

                case ChallengeType.Meet_Bid_X_Team_Y_Times:
                    return "ACHIEVE TEAM BIDS";

                case ChallengeType.Meet_Exact_Bid_X_Team_Y_Times:
                    return "EXACT TEAM BIDS";

                case ChallengeType.Bid_Num_Spades_In_Hand:
                    return "MIRROR BID";

                case ChallengeType.Take_X_Tricks:
                    return string.Format(pluralFormatProvider, "TAKE {0:TRICK;TRICKS}", challenge.Thresholds[0]);

                case ChallengeType.Earn_X_Points:
                    return string.Format(pluralFormatProvider, "GET {0:POINT;POINTS}", challenge.Thresholds[0]);

                case ChallengeType.Sum_Bets:
                    return "TOTAL BET";


            }

            LoggerController.Instance.LogError("Error in requesting mission text: " + (challenge as SpadesChallenge).Type);

            return "N/A";
        }

        public override string GetDescriptionFromChallenge(Challenge challenge)
        {
            switch ((challenge as SpadesChallenge).Type)
            {
                case ChallengeType.Earn_Coins:
                    return string.Format("Earn a total of {0} coins", FormatUtils.FormatPrice(Convert.ToInt32(challenge.Thresholds[0])));

                case ChallengeType.Play_X_Games:
                    return string.Format(pluralFormatProvider, "Play {0:game;games}", challenge.Thresholds[0]);

                case ChallengeType.Win_X_Games:
                    return string.Format(pluralFormatProvider, "Win {0:game;games}", challenge.Thresholds[0]);

                case ChallengeType.Win_X_Games_Row:
                    return string.Format(pluralFormatProvider, "Win {0:game;games} in a row", challenge.Thresholds[0]);

                case ChallengeType.Win_X_Games_Y_Pts:
                    return string.Format(pluralFormatProvider, "Win {0:game;games} with {1:point;points} or more", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Win_X_Games_by_Y_pts:
                    return string.Format(pluralFormatProvider, "Win {0:game;games} by at least {1} points", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Win_X_Games_Y_less_Rounds:
                    return string.Format(pluralFormatProvider, "Win {0:game;games} in {1:round;or less rounds}", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Win_X_Games_no_bags:
                    return string.Format(pluralFormatProvider, "Win {0:game;games} without taking any bags", challenge.Thresholds[0]);

                case ChallengeType.Take_X_Tricks_Y_Times:
                    if (challenge.Thresholds[1] > 1)
                        return string.Format(pluralFormatProvider, "Take {0:trick;tricks} per game {1} times} ", challenge.Thresholds[0], challenge.Thresholds[1]);
                    else
                        return string.Format(pluralFormatProvider, "Take {0:trick;tricks} in a game", challenge.Thresholds[0]);

                case ChallengeType.Meet_Bid_X_Y_Times:
                    return string.Format(pluralFormatProvider, "Bid and take {1:trick;tricks}, {0:time;times}", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Meet_Exact_Bid_X_Y_Times:
                    return string.Format(pluralFormatProvider, "Bid and take {1:trick;tricks} without bags {0:time;times}", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Meet_Bid_X_Team_Y_Times:
                    return string.Format(pluralFormatProvider, "As a team, bid and take {1:trick;tricks}, {0:time;times}", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Meet_Exact_Bid_X_Team_Y_Times:
                    return string.Format(pluralFormatProvider, "As a team, bid and take {1:trick;tricks} without bags {0:time;times}", challenge.Thresholds[0], challenge.Thresholds[1]);

                case ChallengeType.Bid_Num_Spades_In_Hand:
                    return string.Format(pluralFormatProvider, "Bid and take the number of spades in your hand {0:time;times}", challenge.Thresholds[0]);

                case ChallengeType.Take_X_Tricks:
                    return string.Format(pluralFormatProvider, "Take a total of {0:trick;tricks}", challenge.Thresholds[0]);

                case ChallengeType.Earn_X_Points:
                    return string.Format(pluralFormatProvider, "Score a total of {0} points, positive outcome only", challenge.Thresholds[0]);

                case ChallengeType.Sum_Bets:
                    return string.Format("Bet a total of {0} on the designated tables", FormatUtils.FormatBuyIn(challenge.Thresholds[0]));

            }

            LoggerController.Instance.LogError("Error in requesting mission text: " + (challenge as SpadesChallenge).Type.ToString());
            return "N/A";
        }

        //this function refers to SR-1144 - it checks if this mission was ever automatically opened at the end of the popups in the login only
        //only in here can the show flag be set to true
        private bool CheckAutoShowMissionFlag(SpadesMission mission)
        {
            if (SpadesStateController.Instance.GetLastSeendMissionId() == mission.Mission_id)
            {
                //no need to show mission automatically
                return false;
            }
            else
            {
                //check if all challenges are still locked

                bool all_locked = true;

                for (int i = 0; i < mission.GetNumChallenges(); i++)
                    if (mission.GetChallenge(i).Progress != 0)
                        all_locked = false;


                return (all_locked);

            }
        }

        //TODO: not using update_date?!
        public void SaveChallengesInfo(SpadesChallenge org_data, SpadesChallenge update_data)
        {
            //m_curr_challenge_post_update = update_data; ??? 
            m_curr_challenge_before_update = org_data;
        }

        //TODO: not using update_date?!
        public void SaveChallengesInfo(Challenge org_data, SpadesChallenge update_data)
        {
            //m_curr_challenge_post_update = update_data; ??? 
            m_curr_challenge_before_update = org_data as SpadesChallenge;
        }


        //some code could probably move to cardGames
        public override void NewRoundStarted(bool first_round)
        {
            if (first_round)
                m_curr_challenge_post_update = null;

            if (ShouldShowMissionButtonInGame())
            {
                m_challengeInGameView.ShowChallengePanel(true);


                if (m_curr_challenge_before_update != null || first_round)
                {

                    m_challengeInGameView.ShowActiveChallenge(true);

                    m_curr_challenge_before_update = null;

                }
                else
                    m_challengeInGameView.ShowActiveChallenge(false);


            }
            else
            {
                m_challengeInGameView.ShowChallengePanel(false);
            }

        }

        public void HideInGameChallenge()
        {
            m_challengeInGameView.TakePanelOut();
        }

        protected override void ValidateMissionActiveForLobby(Challenge challenge)
        {

            foreach (SpadesWorld world in ModelManager.Instance.Worlds)
            {
                // PromoRoom Changes
                if (world.WorldType == WorldTypes.Regular)
                {
                    foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(PlayingMode.Solo))
                        singleitem.Active_in_challenge = IsChallengeActiveInActiveMatch(challenge as SpadesChallenge, singleitem);

                    foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(PlayingMode.Partners))
                        singleitem.Active_in_challenge = IsChallengeActiveInActiveMatch(challenge as SpadesChallenge, singleitem);
                }
            }

            foreach (WorldItemView worldItem in LobbyController.Instance.Lobby_view.World_items)
            {
                // PromoRoom Changes
                if (worldItem != null && worldItem.IsRegularRoom())
                    foreach (SingleItemView item in worldItem.Singles_view)
                        item.UpdateChallengeIndication();
            }
        }



        public override void ShowChallengeEndGamePanel(ChallengeEndGameView endGameView)
        {
            //check if there is a difference between the current challenge and the previous 
            if (m_curr_challenge_post_update != null)
            {
                (endGameView as SpadesChallengeEndGameView).SetChallengeData(m_curr_challenge_before_update as SpadesChallenge, m_curr_challenge_post_update as SpadesChallenge, () =>
                {
                    m_curr_challenge_before_update = null;
                    m_curr_challenge_post_update = null;
                });
            }
        }

        public void InGamePanelOut()
        {
            if ((ModelManager.Instance.GetActiveMatch() as SpadesActiveMatch).IsSingleMatch && ModelManager.Instance.GetUser().GetLevel() >= m_min_mission_level)
                //hide the inGame panel
                m_challengeInGameView.TakePanelOut();

        }

        public void AutoShowMissionPopupOnLogin()
        {
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            if (mission != null)
            {
                if (SpadesStateController.Instance.GetLastSeendMissionId() != mission.Mission_id && ShouldShowMissionButtonInGame())
                    CardGamesPopupManager.Instance.ShowMissionPopup(PopupManager.AddMode.DontShowIfPopupShown);
            }
        }

        public override bool IsChallengeActiveInActiveMatch(Challenge challenge, Match match = null)
        {
            if (challenge == null)
                return false;

            int challenge_bet = challenge.Bet;
            bool game_mode_match = GameModeMatchActiveMatch((challenge as SpadesChallenge).SpadesTableFilterData.PlayingMode, match);

            int match_buyin = 0;
            if (match != null)
                match_buyin = match.Buy_in;
            else
            {
                match_buyin = ModelManager.Instance.GetActiveMatch().Match.Buy_in;
            }



            if (challenge.Bet_operator == MissionController.BetOperator.Exact)
            {
                if (challenge_bet == match_buyin && game_mode_match)
                    return (true);
                else
                    return (false);
            }
            else if (challenge.Bet_operator == MissionController.BetOperator.Min)
            {
                if (match_buyin >= challenge_bet && game_mode_match)
                    return (true);
                else
                    return (false);
            }
            else if (challenge.Bet_operator == MissionController.BetOperator.Max)
            {
                if (match_buyin <= challenge_bet && game_mode_match)
                    return (true);
                else
                    return (false);
            }
            else
                return (false);
        }

        public Sprite Suitcase_closed
        {
            get
            {
                return m_suitcase_closed;
            }
        }

        public Sprite Suitcase_open
        {
            get
            {
                return m_suitcase_open;
            }
        }

        public new SpadesChallenge Curr_challenge_before_update { get => m_curr_challenge_before_update as SpadesChallenge; set => m_curr_challenge_before_update = value; }
    }
}


