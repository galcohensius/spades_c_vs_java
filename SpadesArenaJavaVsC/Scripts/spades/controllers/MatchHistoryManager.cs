﻿using System.Text;
using spades.models;
using cardGames.models;

namespace spades.controllers
{

	public class MatchHistoryManager {


		StringBuilder 			m_match_string = new StringBuilder();

		const string 			DATA_VERSION = "01";

		const string			RECORD_DELI="|";
		const string			PART_DELI = ",";
		const string			SUB_PART_DELI = "~";
		const string			ROUNDS_DELI = "^";

		public void SetDealer(int dealer_index)
		{
			if (m_match_string.Length > 0)
				m_match_string.Append (ROUNDS_DELI);

			m_match_string.Append (dealer_index);
			m_match_string.Append (PART_DELI);

		}

        public void SetHands(params CardsList[] hands)
        {
            
            foreach (var hand in hands)
            {
                CardsList h = new CardsList(hand);
                h.SortHighToLowSuitFirst();

                for (int i = 0; i < hand.Count; i++)
                    m_match_string.Append(h[i]);

                //m_match_string.Append(hand.ToString());
                m_match_string.Append(PART_DELI);
            }
            // Replace last delimiter with record delimiter
            m_match_string.Remove(m_match_string.Length - 1, 1);
            m_match_string.Append(RECORD_DELI);


        }


		public void SetBids(string[]	bids)
		{
			for (int i = 0; i < 4; i++)
			{
				m_match_string.Append (bids [i]);

				if(i<3)
					m_match_string.Append (PART_DELI);
				else
					m_match_string.Append (RECORD_DELI);
			}


		}


		public void SetRoundResults(PlayerRoundResults[]	round_results)
		{
			m_match_string.Remove (m_match_string.Length-1, 1);
			m_match_string.Append (RECORD_DELI);
			 
			for (int i = 0; i < round_results.Length; i++)
			{
				m_match_string.Append (round_results [i].Takes);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (round_results [i].Bids);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (round_results [i].Bags_round);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (round_results [i].Nil_bonus_or_penalty);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (round_results [i].Round_score);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (round_results [i].Bags_total);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (round_results [i].Bags_penalty);
				m_match_string.Append (SUB_PART_DELI);
				m_match_string.Append (((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults((SpadesRoundResults.Positions)i).ToString());

				if (i < round_results.Length - 1)
					m_match_string.Append (PART_DELI);
			}
		
		}

		public string GetCompressedHistoryAndVersion()
		{
			string compressed_match =  DATA_VERSION + GZipCompressor.CompressStringWithLen (m_match_string.ToString ());
			return(compressed_match);
		}

		public void SetTurnResults(string[]	played, int winner)
		{
			for (int i = 0; i < 4; i++)
				m_match_string.Append (played [i]);

			m_match_string.Append (SUB_PART_DELI);
			m_match_string.Append (winner);
			m_match_string.Append (PART_DELI);


		}

	}
}