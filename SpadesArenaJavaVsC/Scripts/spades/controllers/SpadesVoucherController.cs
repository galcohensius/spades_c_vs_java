﻿using spades.models;

namespace spades.controllers
{
    public class SpadesVoucherController : VoucherController
    {
        #region Public Members
        public new static SpadesVoucherController Instance;
        #endregion
        public override void Awake()
        {
            base.Awake();
            Instance = this;
        }

        Coroutine m_timer_routine;
        int m_earliest_expire_voucher_id = 0;

        public VouchersGroup GetVouchersBySpadesMatch(SpadesSingleMatch spadesMatch)
        {
            foreach (VouchersGroup voucher in ModelManager.Instance.Vouchers)
            {
                if (voucher.Count == 0)
                    continue;

                SpadesTableFilterData tableFilterData = (SpadesTableFilterData)voucher.TableFilterData;

                if (tableFilterData.IsVariantContained(spadesMatch.Variant) &&
                    tableFilterData.IsSubVariantContained(spadesMatch.SubVariant) &&
                    tableFilterData.IsPlayingModeContained(spadesMatch.Playing_mode) &&
                    spadesMatch.Buy_in == voucher.VouchersValue)
                {
                    if (tableFilterData.SpecialOnly)
                    {
                        if (tableFilterData.SpecialOnly == spadesMatch.Is_special)
                            return voucher;
                        return null;
                    }
                    else
                        return voucher;
                }
            }
            return null;
        }


    }
}



