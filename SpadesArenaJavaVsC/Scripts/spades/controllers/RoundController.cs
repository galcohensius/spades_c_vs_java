using System;
using System.Collections;
using System.Collections.Generic;
using spades.models;
using SpadesAI;
using SpadesAI.model;
using spades.utils;
using spades.ai;
using System.Linq;
using cardGames.models;

namespace spades.controllers
{
    /// <summary>
    /// this class manages the entire round of play - holds references to the model manager and view manager - as well as to the game engine and all the bots
    /// </summary>
    public class RoundController : MonoBehaviour
    {
        private const int ROUNDS_PER_GAME = 5;
        private const int ZOMBIE_TO_BOT_NUM_TURNS = 2;
        private const float BECOME_ZOMBIE_ODS_PER_ROUND = -1; // No disconnection

        public delegate void MatchEndedDelegate(int win_place, int num_winners, string match_history = null, Action received_answer = null);
        public delegate void MatchCancelledDelegate(bool game_started, Action leave_done, string match_history = null);
        public delegate void CardAnimationEndedDelegate(bool trick_ended);

        MatchEndedDelegate m_match_ended_delegate;
        MatchCancelledDelegate m_match_cancelled_delegate;

        SpadesEngine m_spades_engine;

        Dictionary<BrainType, ISpadesBrain> m_brains;
        Dictionary<Position, BrainType> m_brain_type_by_pos;
        Dictionary<Position, int> m_brain_skill_level_by_pos;

        // AI2
        Dictionary<BrainType, ISpadesBrain> m_brains2;

        TableView m_table_view;
        CardView m_selected_card_view = null;

        [SerializeField] GameObject m_bidding_popup = null;
        ChatBotController m_chat_bot_controller = new ChatBotController();

        int m_dealer;
        CardsList m_high_cards = new CardsList();

        bool m_bidding_complete = false;

        float m_bid_delay;
        float m_play_delay;

        bool m_player_bid_or_played = false;
        //helps blocking user from throwing more than one card per turn
        bool m_drag = false;
        bool m_player_left = false;
        bool m_cards_revealed = false;

        const float LAST_PLAYER_EXTRA_DELAY_AFTER_BID = 1f;
        const int GAME_LOGIN_TIMEOUT_SEC = 60;
        public const float EXIT_ANIM_TIME = 0.5f;
        public const float SPADES_BROKEN_TIME = 3.8f;
        public const float ANIM_GRACE_TIME_TIME = 5f;

        Coroutine m_timer_coroutine = null;
        Coroutine m_bot_play_coroutine = null;
        Coroutine m_slave_timer_exp_coroutine = null;
        Coroutine m_master_timer_exp_coroutine = null;
        Coroutine m_special_fix_routine = null;

        //adding data to round logger
        MatchHistoryManager m_match_logger;
        string[] m_turn_logger = new string[4];

        //achievements related
        bool m_precision_game;
        bool m_was_negative;

        SpadesTable m_table;
        Player m_player;

        bool m_is_game_active = false;

        System.Random m_random = new System.Random();

        DealMaker dealMaker;

        Action<Action> m_table_found_delegate;

        //all the reconnect related vars
        DisconnectedPopup m_disconnected_popup = null;
        common.comm.Decoder m_decoder;
        System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding();
        List<Param> m_commands = new List<Param>();
        bool m_play_recorded_commands = false;
        bool m_disconnected = false;
        bool m_card_thrown_processed = false;
        bool m_change_me_to_master_after_FF = false;
        bool m_thorw_anim_ended = false;
        bool m_deal_anim_ended = false;
        int m_playback_commands_counter = 0;

        int m_cards_finished_throw_anim = 0;

        //fake disconnection Params
        int m_fake_disconnect_trick_number = 0;
        bool m_game_had_fake_disconnection = false;
        int m_c4pQuitPoints;
        int m_hiloQuitPoints;

        public static RoundController Instance;

        private void Awake()
        {
            Instance = this;
        }

        public void Start()
        {
            m_decoder = new common.comm.Decoder('[', ']', '|');
            m_table_view = ManagerView.Instance.Table.GetComponent<TableView>();
            CreateBrains();

#if UNITY_WEBGL
            // Memory setting for GGPSpadesAI under WebGL
            GGPSpadesAI.SetPercentUsualMemory(5);
#endif
            BackgroundController.Instance.OnBackFromBackground += OnBackFromBackground;

            // Set mission min level
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_c4pQuitPoints = ldc.GetSettingAsInt("C4PQuitPoints", 200);
                m_hiloQuitPoints = ldc.GetSettingAsInt("HiLoQuitPoints", 100);
            };
        }


        /// <summary>
        /// Initializes the RoundController for a new match
        /// </summary>
        public void Init(MatchEndedDelegate match_end_delegate, MatchCancelledDelegate match_cancel_delegate)
        {
            m_table = ModelManager.Instance.GetTable() as SpadesTable;
            m_player = m_table.GetPlayer(3);
            m_match_ended_delegate = match_end_delegate;
            m_match_cancelled_delegate = match_cancel_delegate;
            m_spades_engine = new SpadesEngine();
            m_player_left = false;
            SpadesActiveMatch aMatch = SpadesModelManager.Instance.GetActiveMatch();
            m_bid_delay = aMatch.GetMatch().BidDelay;
            m_play_delay = aMatch.GetMatch().PlayDelay;
            m_bidding_popup.GetComponent<BiddingPopup>().CloseBiddingKeyboard();
            //SoundsController.Instance.ChangeVolume (true, 0.35f);
            m_table_view.TableInit(aMatch.GetMatch().Playing_mode);  //creates the visual objects
            m_table_view.ToggleAvatarObjectsActiveState(true);

            // Cancel sleep when on table
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            m_table_view.UpdatePartnersPointsTable();
        }

        /// <summary>
        /// Starts a new match or round
        /// </summary>
        public void StartGame(bool new_game)
        {
			PiggyController.Instance.ShowWaitingIndication = false;

			LoggerController.Instance.Log(LoggerController.Module.Game, "Start Game function: " + new_game);
            m_table_view.ToggleAvatarObjectsActiveState(true);

            m_is_game_active = true;
            m_cards_revealed = false;
            m_player_left = false;

            SpadesActiveMatch activeMatch = ModelManager.Instance.GetActiveMatch() as SpadesActiveMatch;
            ISpadesMatch match = activeMatch.GetMatch();

            m_table_view.HideBidSignsAndBubbles();
            m_table_view.ResetPlayerIndications();
            m_table_view.UpdatePartnersPointsTable();

            ResetTable();

            SpadesRoundData roundData = new SpadesRoundData();
            ModelManager.Instance.RoundData = roundData;

			if (new_game)
			{
				InitBrains(m_table, match.BotsData, match.Variant);
				m_match_logger = new MatchHistoryManager();
				LobbyController.Instance.SetOldXP(ModelManager.Instance.GetUser().GetXPInLevel());
				LobbyController.Instance.SetOldLevel(ModelManager.Instance.GetUser().GetLevel());
				LobbyController.Instance.Old_threshold = ModelManager.Instance.GetUser().GetLevelTotalXP();

				foreach (ISpadesBrain brain in m_brains.Values)
                { 
                    AIMatchData matchData = new AIMatchData
                    {
                        LoseConditionPoints = match.LowPoints,
                        WinConditionPoints = match.HighPoints,
                        Mode = match.Playing_mode,
                        Variation = match.Variant,
                        BagsToIncurPenalty = match.BagsCount,
                        BagsPenalty = match.BagsPanelty
                    };

                    brain.StartMatch (matchData);
					brain.StartRound (m_table, m_brain_skill_level_by_pos,m_brain_type_by_pos);
				}
                foreach (ISpadesBrain brain in m_brains2.Values)
                {
                    AIMatchData matchData = new AIMatchData
                    {
                        LoseConditionPoints = match.LowPoints,
                        WinConditionPoints = match.HighPoints,
                        Mode = match.Playing_mode,
                        Variation = match.Variant,
                        BagsToIncurPenalty = match.BagsCount,
                        BagsPenalty = match.BagsPanelty
                    };

                    brain.StartMatch(matchData);
                    brain.StartRound(m_table, m_brain_skill_level_by_pos, m_brain_type_by_pos);
                }


                if (m_player.Master)
                    Deal(true);

                // Set the achievement flags
                m_precision_game = true;
                m_was_negative = false;

                //making sure fake disconnection can only happen once a match
                m_game_had_fake_disconnection = false;


                // Trigger start game event - IMS
                SpadesIMSController.Instance.TriggerEvent(IMSController.EVENT_START_GAME,true);

            }
            else // new round
            {
                // Update the brain's match data with points and bags
                SpadesRoundResults results = activeMatch.GetLastRoundResults();
                foreach (ISpadesBrain brain in m_brains.Values)
                {
                    if (match.Playing_mode == PlayingMode.Solo)
                    {
                        brain.MatchData.MatchPoints[Position.East] += results.GetResults(SpadesRoundResults.Positions.East).Round_Score_With_Penalty;
                        brain.MatchData.MatchPoints[Position.North] += results.GetResults(SpadesRoundResults.Positions.North).Round_Score_With_Penalty;
                        brain.MatchData.MatchPoints[Position.South] += results.GetResults(SpadesRoundResults.Positions.South).Round_Score_With_Penalty;
                        brain.MatchData.MatchPoints[Position.West] += results.GetResults(SpadesRoundResults.Positions.West).Round_Score_With_Penalty;

                        brain.MatchData.MatchBags[Position.East] = results.GetResults(SpadesRoundResults.Positions.East).Bags_total;
                        brain.MatchData.MatchBags[Position.North] = results.GetResults(SpadesRoundResults.Positions.North).Bags_total;
                        brain.MatchData.MatchBags[Position.South] = results.GetResults(SpadesRoundResults.Positions.South).Bags_total;
                        brain.MatchData.MatchBags[Position.West] = results.GetResults(SpadesRoundResults.Positions.West).Bags_total;
                    }
                    else
                    {
                        // In partners, we can just set one player of the two since it is always calculated for both players
                        brain.MatchData.MatchPoints[Position.East] += results.GetResults(SpadesRoundResults.Positions.West_East).Round_Score_With_Penalty;
                        brain.MatchData.MatchPoints[Position.North] += results.GetResults(SpadesRoundResults.Positions.North_South).Round_Score_With_Penalty;

                        brain.MatchData.MatchBags[Position.East] = results.GetResults(SpadesRoundResults.Positions.West_East).Bags_total;
                        brain.MatchData.MatchBags[Position.North] = results.GetResults(SpadesRoundResults.Positions.North_South).Bags_total;
                    }
                    brain.StartRound(m_table, m_brain_skill_level_by_pos, m_brain_type_by_pos);
                }

                // AI2
				foreach (ISpadesBrain brain in m_brains2.Values)
				{
					if (match.Playing_mode == PlayingMode.Solo)
					{
						brain.MatchData.MatchPoints[Position.East] += results.GetResults(SpadesRoundResults.Positions.East).Round_Score_With_Penalty;
						brain.MatchData.MatchPoints[Position.North] += results.GetResults(SpadesRoundResults.Positions.North).Round_Score_With_Penalty;
						brain.MatchData.MatchPoints[Position.South] += results.GetResults(SpadesRoundResults.Positions.South).Round_Score_With_Penalty;
						brain.MatchData.MatchPoints[Position.West] += results.GetResults(SpadesRoundResults.Positions.West).Round_Score_With_Penalty;

						brain.MatchData.MatchBags[Position.East] = results.GetResults(SpadesRoundResults.Positions.East).Bags_total;
						brain.MatchData.MatchBags[Position.North] = results.GetResults(SpadesRoundResults.Positions.North).Bags_total;
						brain.MatchData.MatchBags[Position.South] = results.GetResults(SpadesRoundResults.Positions.South).Bags_total;
						brain.MatchData.MatchBags[Position.West] = results.GetResults(SpadesRoundResults.Positions.West).Bags_total;
					}
					else
					{
						// In partners, we can just set one player of the two since it is always calculated for both players
						brain.MatchData.MatchPoints[Position.East] += results.GetResults(SpadesRoundResults.Positions.West_East).Round_Score_With_Penalty;
						brain.MatchData.MatchPoints[Position.North] += results.GetResults(SpadesRoundResults.Positions.North_South).Round_Score_With_Penalty;

						brain.MatchData.MatchBags[Position.East] = results.GetResults(SpadesRoundResults.Positions.West_East).Bags_total;
						brain.MatchData.MatchBags[Position.North] = results.GetResults(SpadesRoundResults.Positions.North_South).Bags_total;
					}
					brain.StartRound(m_table, m_brain_skill_level_by_pos, m_brain_type_by_pos);
				}

				if (m_player.Master)
					Deal (false);
			}


            // Fill round data 
            roundData.GameTransId = activeMatch.Game_trans_id;
            roundData.RoundNum = activeMatch.GetRoundsCount() + 1;
            roundData.GameId = activeMatch.Game_id;
            foreach (Player player in m_table.Players)
            {
                if (player.Player_Type == Player.PlayerType.Human)
                {
                    roundData[player.ServerPos].UserId = player.User.GetID();
                }
                else
                {
                    roundData[player.ServerPos].BotType = m_brain_type_by_pos[AIAdapter.IndexToPosition(player.GetTablePos())];
                    roundData[player.ServerPos].BotLevel = m_brain_skill_level_by_pos[AIAdapter.IndexToPosition(player.GetTablePos())];
                }
            }

            if (m_player.Master)
                m_chat_bot_controller.RoundStarted();

            GenerateFakeDisconnectionParams();

            m_table_view.ResetSpadesBrokenAnimations(false);
            m_table_view.ShowAllPlayerNames();

            MissionController.Instance.NewRoundStarted(new_game);
        }

        public void StopAllChatters()
        {
            MAvatarView[] players = ManagerView.Instance.Game_Table.GetComponentsInChildren<MAvatarView>(true);
            foreach(MAvatarView player in players)
            {
                player.HideChat();
            }
        }

        private void CreateBrains() {
            m_brain_type_by_pos = new Dictionary<Position, BrainType>();
            m_brain_skill_level_by_pos = new Dictionary<Position, int>();
            // AI1
            m_brains = new Dictionary<BrainType, ISpadesBrain>
            {
                [BrainType.ClassicV1] = new SpadesBrainImpl(BrainType.ClassicV1),
                [BrainType.ClassicV2] = new SpadesBrainImpl(BrainType.ClassicV2),
                [BrainType.ClassicV3] = new SpadesBrainImpl(BrainType.ClassicV3),
                [BrainType.ClassicV4] = new SpadesBrainImpl(BrainType.ClassicV4),
                [BrainType.ClassicV4Peeking] = new SpadesBrainImpl(BrainType.ClassicV4Peeking),
                [BrainType.Mirror] = new SpadesBrainImpl(BrainType.Mirror),
                [BrainType.Whiz] = new SpadesBrainImpl(BrainType.Whiz),
                [BrainType.Suicide] = new SpadesBrainImpl(BrainType.Suicide),
                [BrainType.Jokers] = new SpadesBrainImpl(BrainType.Jokers),
                [BrainType.CallBreak] = new SpadesBrainImpl(BrainType.CallBreak),
                [BrainType.UCT] = new SpadesBrainImpl(BrainType.UCT)
            };
            // AI2
            m_brains2 = new Dictionary<BrainType, ISpadesBrain>
            {
                [BrainType.Jokers2020] = new SpadesAI2.SpadesBrainImpl(BrainType.Jokers2020),
                [BrainType.V3_LessObvious] = new SpadesAI2.SpadesBrainImpl(BrainType.V3_LessObvious),
                [BrainType.ClassicV4JustToValidate] = new SpadesAI2.SpadesBrainImpl(BrainType.ClassicV4JustToValidate),
                [BrainType.RoyaleBot] = new SpadesAI2.SpadesBrainImpl(BrainType.RoyaleBot),
            };

            // GGP - unused
            GGPSpadesAI ggpSpadesAI = new GGPSpadesAI();
            m_brains[BrainType.GGP] = ggpSpadesAI;
            LoggerController.Instance.Log(LoggerController.Module.Game, "GGP Spades AI version: " + ggpSpadesAI.VersionString());
		}


	    /// <summary>
	    /// Factory method for bot brains.
	    /// </summary>
	    /// <param name="table"></param>
	    /// <param name="botsData">Bots data.</param>
	    /// <param name="playingVariation"></param>
        private void InitBrains (SpadesTable table, BotsData botsData, PlayingVariant playingVariation)
		{
			// Iterate server positions and assign brains
			for (int serverPos = 0; serverPos < 4; serverPos++)
			{
				Position pos = RoundController.AIAdapter.IndexToPosition (table.ServerToLocalPos (serverPos));

				if (botsData.BotTypes.ContainsKey (serverPos))
				{
					m_brain_skill_level_by_pos[pos] = botsData.BotTypes[serverPos].skill;
				    switch (playingVariation)
				    {
                        case PlayingVariant.Classic:
                            switch (botsData.BotTypes[serverPos].type)
                            {
                                case "A":
                                    m_brain_type_by_pos[pos] = BrainType.ClassicV1;
                                    break;
                                case "S":
                                    m_brain_type_by_pos[pos] = BrainType.ClassicV2;
                                    break;
                                case "P":
                                    m_brain_type_by_pos[pos] = BrainType.ClassicV3;
                                    break;
                                case "O":
                                    m_brain_type_by_pos[pos] = BrainType.V3_LessObvious;
                                    break;
                                default:
                                case "G":
                                    m_brain_type_by_pos[pos] = BrainType.ClassicV4;
                                    break;

                                case "T":
                                    m_brain_type_by_pos[pos] = BrainType.UCT;
                                    break;
                                case "U":
                                    m_brain_type_by_pos[pos] = BrainType.GGP;
                                    break;
                                case "K":
                                    m_brain_type_by_pos[pos] = BrainType.ClassicV4Peeking;
                                    break;
                            }
                            break;
                        case PlayingVariant.Jokers:
                            switch (botsData.BotTypes[serverPos].type)
                            {
                                default:
                                case "J":
                                    m_brain_type_by_pos[pos] = BrainType.Jokers;
                                    break;
                                case "Z":
                                    m_brain_type_by_pos[pos] = BrainType.Jokers2020;
                                    break;
                            }

                            break;
                        case PlayingVariant.Mirror:
                            m_brain_type_by_pos[pos] = BrainType.Mirror;

                            break;
                        case PlayingVariant.Suicide:
                            m_brain_type_by_pos[pos] = BrainType.Suicide;

                            break;
                        case PlayingVariant.Whiz:
                            m_brain_type_by_pos[pos] = BrainType.Whiz;

                            break;
				        case PlayingVariant.CallBreak:
				            m_brain_type_by_pos[pos] = BrainType.CallBreak;
                            break;
                        case PlayingVariant.Royale:
                            m_brain_type_by_pos[pos] = BrainType.RoyaleBot;
                            break;
                        default:
                            m_brain_type_by_pos[pos] = BrainType.ClassicV4;
                            break;
                    }

                    // ------------ Comment out after Debug: ------- TODO 
//                    if (pos == Position.East || pos == Position.West || pos == Position.North)
//                    {
//                        m_brain_type_by_pos[pos] = BrainType.V3_LessObvious;
//                        m_brain_skill_level_by_pos[pos] = 6;
//                    }
                    // ------------ Comment out after Debug: -------TODO

                    LoggerController.Instance.LogFormat(LoggerController.Module.Game, "Spades brain {0}, level {2}, sitting at {1}", m_brain_type_by_pos[pos], pos, m_brain_skill_level_by_pos[pos]);
                }
				else
				{
					m_brain_type_by_pos [pos] = BrainType.ClassicV4;
				    LoggerController.Instance.LogFormat(LoggerController.Module.Game, "Spades brain {0}, sitting at {1}", m_brain_type_by_pos[pos], pos);
                }

            }
            // Create the deal maker with the current brain type
            if (playingVariation==PlayingVariant.Jokers)
                dealMaker = new DealMaker(m_brains[BrainType.Jokers].BiddingBrain);
            else
                dealMaker = new DealMaker (m_brains [BrainType.ClassicV4].BiddingBrain);
		}

		private void ResetTable ()
		{
			//internal counter of PlayTrickWinnerAnim that needs to be reset
			m_cards_finished_throw_anim = 0;

			m_bidding_popup.SetActive (false);

			CancelTimers ();

            SpadesSoundsController.Instance.StopTurnTimerAlert();

            (ModelManager.Instance.GetTable() as SpadesTable).ClearAllBidsAndTakes ();
            //clean the table thrown cards
            m_table_view.ClearTable ();
            //delete cards on table if any are there
            m_table_view.DeleteAllPlayerCards ();

			//reset indicators
			m_bidding_complete = false;

            m_table_view.Game_ui_object.SetActive(true);

            Time.timeScale = 1;
        }

		public void LeaveTable (bool money_back, bool timeout_leave = false)
		{
			// Disconnect the game server socket
			SpadesGameServerManager.Instance.Disconnect ();

            SpadesSoundsController.Instance.StopTurnTimerAlert();
            SpadesMissionController.Instance.HideInGameChallenge();

            StopAllChatters();

			string match_history = null;

            if (ModelManager.Instance.GetUser ().GetMatchHistoryFlag () && match_history!=null)
				match_history = m_match_logger.GetCompressedHistoryAndVersion ();

			if (!timeout_leave)
			{
				//special override for the timeout 
				m_match_cancelled_delegate (!money_back, LeaveCallback, match_history);
			}
			else
			{
				LeaveCallback ();
			}

			m_is_game_active = false;
			DestroyAllGame ();
		}

		private void LeaveCallback ()
		{
			m_table_view.StopAllCoroutines ();

			ResetTable ();
			m_table_view.ShowGameUI (false);

			m_player_left = true;
			StopAllCoroutines ();
		}

		private void Deal (bool highCard)
		{
            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch ();

            // Create a new deck and shuffle
		    CardsList deck = GetDeck(activeMatch);
            deck.Shuffle();

			// High card 
			CardsList server_high_cards = GetDealerPosAndHighCards(highCard, deck, activeMatch);

            // Deal
            CardsList[] hands = m_spades_engine.Deal(SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant);
//            SetHandsDebug(hands); // Comment out

            // Set the model
            for (int i = 0; i < 4; i++){
				m_table.GetPlayer(i).SetCards(hands[i]);
			}
			// Cards switch #BlackMagic
			dealMaker.SwitchCards(m_table, activeMatch);

            // Update the hands array after switching
            for (int i = 0; i < 4; i++){
                hands[i] = m_table.GetPlayer(i).GetCards();
            }

            // Store the deal in the round data - after card switch
            SpadesRoundData roundData = ModelManager.Instance.RoundData as SpadesRoundData;
            for (int pos = 0; pos < 4; pos++)
            {
                int serverPos = m_table.LocalToServerPos(pos);
                roundData[serverPos].Hand = CardGamesFormatUtils.FormatHand(hands[pos],null); 
            }

			string command_c3_deal = SendDataCommandsBuilder.Instance.BuildDealCommand (m_dealer, hands, server_high_cards);
			MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_deal, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_DEAL);
			SpadesGameServerManager.Instance.Send (msg);
		}

        private static void SetHandsDebug(CardsList[] hands)
        {
            hands[0].SetCards("5HADKD9D5D2DJC0C8C2C0S5S4S"); // West
            hands[1].SetCards("9H8H0D3DACKCQC7C5CASKS9S7S"); // North
            hands[2].SetCards("KHJH0H7H6HJD8D7DQSJS8S6S3S"); // East
            hands[3].SetCards("AHQH4H3H2HQD6D4D9C6C4C3C2S"); // South
        }

        private static CardsList GetDeck(SpadesActiveMatch activeMatch)
        {
            CardsList deck;
            switch (activeMatch.GetMatch().Variant)
            {
                default:
                    deck = CardsListBuilder.FullDeckNoJokers();
                    break;
                case PlayingVariant.Jokers:
                    deck = CardsListBuilder.FullDeckWithJokers();
                    break;
                case PlayingVariant.Royale:
                    deck = CardsListBuilder.ShortDeck7ToAce();
                    break;
            }

            return deck;
        }

        private CardsList GetDealerPosAndHighCards(bool highCard, CardsList deck, SpadesActiveMatch activeMatch)
        {
            m_high_cards.Clear();
            CardsList server_high_cards = new CardsList();
            if (highCard)
            {
                for (int i = 0; i < 4; i++)
                {
                    m_high_cards.Add(deck[i]);
                }

                // Deal maker
                dealMaker.UserBidsFirst(m_table, activeMatch, m_high_cards);

                // Find the highest card index
                SpadesCardComparer comparer = new SpadesCardComparer();
                int highestIndex = 0;
                Card highestCard = m_high_cards[0];
                for (int i = 1; i < 4; i++)
                {
                    if (comparer.Compare(highestCard, m_high_cards[i]) < 0)
                    {
                        highestCard = m_high_cards[i];
                        highestIndex = i;
                    }
                }

                m_dealer = highestIndex;

                SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
                Card[] cards_arr_server = new Card[4];

                for (int i = 0; i < 4; i++)
                {
                    cards_arr_server[table.LocalToServerPos(i)] = m_high_cards[i];
                }

                for (int i = 0; i < 4; i++)
                {
                    server_high_cards.Add(cards_arr_server[i]);
                }
            }
            else
            {
                m_dealer = (m_dealer + 1) % 4;
            }

            //LoggerController.Instance.Log(LoggerController.Module.Game, "Current dealer local position: " + m_dealer);
            return server_high_cards;
        }


        public void PlayHeadAnimations ()
		{
			m_table_view.BounceAllHeads ();
		}


        private IEnumerator BiddingCompleted ()
		{
			yield return new WaitForSeconds (LAST_PLAYER_EXTRA_DELAY_AFTER_BID);

			string[] bids = new string[4];

			//adding data to round logger
			for (int i = 0; i < 3; i++)
			{
				if ((ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (i).GetBids () == 0)
					bids [i] = "N";
				else
					bids [i] = (ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (i).GetBids ().ToString ();
			}

			if ((ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (3).GetBids () == 0)
			{
				if ((ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (3).GetBlind ())
					bids [3] = "B";
				else
					bids [3] = "N";
			}
			else
				bids [3] = (ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (3).GetBids ().ToString ();

			m_match_logger.SetBids (bids);

			m_spades_engine.StartTrick ();
            //LoggerController.Instance.Log(LoggerController.Module.Game, "------------------------------------------Starting Trick # " + Spades_engine.Curr_trick_num);
			StartTurn (m_spades_engine.Curr_player_index);

			m_bidding_complete = true;

			foreach (ISpadesBrain brain in m_brains.Values)
			{
				brain.StartTrick ();
			}

			foreach (ISpadesBrain brain in m_brains2.Values)
			{
				brain.StartTrick();
			}
		}

		public void BiddingComplete ()
		{
			StartCoroutine (BiddingCompleted ());
		}


        private IEnumerator BotBidOrPlayCoroutine (int index, bool is_bid, bool no_delay = false)
		{
            LoggerController.Instance.Log(LoggerController.Module.Game, "Starting BOT BotBidOrPlayCoroutine - for bot at Position: " + index);
            PlayerView player_view = m_table_view.GetPlayerView (index);

            // Wait time:
            // For bid: Normal distribution with mean of 4 and the stddev of 1
            // For turn: Triangular distribution between 0 and 3 with most frequent value of 0.5
            float actual_wait_time = 0;
			if (is_bid)
				actual_wait_time = (float)m_random.NextGaussian (4, 1);
			else
				actual_wait_time = (float)m_random.NextTriangular (0, 3, 1.2);

			// On last trick, the bots should play fast like real users
			if (m_spades_engine.Curr_trick_num == 13)
				actual_wait_time = 0;

			float startTime = Time.realtimeSinceStartup;

            Position pos = AIAdapter.IndexToPosition(index);
            BrainType brainType = m_brain_type_by_pos[pos];



			ISpadesBrain curBrain = null;
            if (!m_brains.TryGetValue(brainType, out curBrain))
            {
			   	if (!m_brains2.TryGetValue(brainType, out curBrain))
                {
					throw new Exception("No bot for type: " + brainType);
                }

			}


            int bot_bid = -1;
            Card card = null;
			SpadesPlayerRoundData prData = (ModelManager.Instance.RoundData as SpadesRoundData)[m_table.LocalToServerPos(index)];

            if (is_bid)
            {
                //LoggerController.Instance.Log(LoggerController.Module.Game, " Before Bot's Bid at Position: " + pos);

                BotASyncMakeBid botsASyncMakeBid = new BotASyncMakeBid(curBrain, pos, prData);
                yield return botsASyncMakeBid;
                bot_bid = botsASyncMakeBid.MakeBidResult;

                LoggerController.Instance.Log(LoggerController.Module.Game, "bot at pos: " + pos + " Its current brain is: " + curBrain.BiddingBrain + " Did Bid: " + bot_bid);

                // Bid on boarding message
                ShowOnboardingMessage(false);
            }
            else // playing 
            {
                //LoggerController.Instance.Log(LoggerController.Module.Game, " Before Bot's Play at Position: " + pos + " Trick#: " + m_spades_engine.Curr_trick_num);

                BotASyncPlayMove botASyncPlayMove = new BotASyncPlayMove(curBrain, pos, prData);
                yield return botASyncPlayMove;
                card = botASyncPlayMove.PlayMoveResult;

                LoggerController.Instance.Log(LoggerController.Module.Game, "bot at pos: " + pos + " Picked Card: " + card);
                
                HideOnboardingMessage();
            }


            // NOT USED SINCE THERE ARE NO BOTS THAT ARE TIME CONSUMING (RAN 7/12/2019)
            // Calculate the remaining delay we still have to wait in order to
            // simulate a real person
			//actual_wait_time = Math.Max (0, actual_wait_time - (Time.realtimeSinceStartup - startTime));

			yield return new WaitForSecondsRealtime(actual_wait_time);

            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch();
            if (is_bid)
			{
				// Deal maker
				bot_bid = dealMaker.OverrideBid (m_table, activeMatch, (SpadesRoundResults.Positions)index, bot_bid);

                if (m_brain_type_by_pos[pos]!=BrainType.GGP) {
					if (bot_bid == -2)
						curBrain.SetBid (pos, 0, true); // blind nil
					else
						curBrain.SetBid (pos, bot_bid);
				}

				string command_c3_bid = SendDataCommandsBuilder.Instance.BuildBidCommand (bot_bid, false, index, (int)Player.PlayerType.Bot);
				if (bot_bid == -2)
				{
					// Blind nil
					m_table.GetPlayer (index).SetBids (0);
					m_table.GetPlayer (index).SetBlind (true);
					command_c3_bid = SendDataCommandsBuilder.Instance.BuildBidCommand (0, true, index, (int)Player.PlayerType.Bot);
				}
				else
				{
					m_table.GetPlayer (index).SetBids (bot_bid);
				}

				// Send the data
				MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_bid, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_BID);
				SpadesGameServerManager.Instance.Send (msg);
			}
			else
			{
                
                card = dealMaker.OverridePlay(m_table, activeMatch, (SpadesRoundResults.Positions)index, m_player.GetCards(), card);
               
                //LoggerController.Instance.LogFormat(LoggerController.Module.Game, " Before Server: {0} plays {1}", pos, card);

                AddTurnToLogger (card, index);

				string command_c3_throw = SendDataCommandsBuilder.Instance.BuildThrowCommand (card, index, (int)Player.PlayerType.Bot);
				MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_throw, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_THROW);
				SpadesGameServerManager.Instance.Send (msg);
			}
		}

		public void CancelTimers ()
		{
            //LoggerController.Instance.Log(LoggerController.Module.Game, "Cancel Timers");
            SpadesSoundsController.Instance.StopTurnTimerAlert();

			if (m_timer_coroutine != null)
			{
				StopCoroutine (m_timer_coroutine);
			}

			if (m_slave_timer_exp_coroutine != null)
				StopCoroutine (m_slave_timer_exp_coroutine);

			if (m_master_timer_exp_coroutine != null)
				StopCoroutine (m_master_timer_exp_coroutine);

            if (m_special_fix_routine != null)
                StopCoroutine(m_special_fix_routine);

            for (int i = 0; i < 4; i++)
				m_table_view.GetPlayerView (i).ShowHideTimer (false);
		}

		public void AddTurnToLogger (Card card, int player_index)
		{
			m_turn_logger [player_index] = card.ToString ();
		}

        private IEnumerator StartPlayerTimer (float total_time, bool bid)
		{
            
            PlayerView player_view = m_table_view.GetPlayerView (3);
            m_bidding_popup.GetComponent<BiddingPopup>().SetTimerColor(true);
            if (bid)
			{
                player_view.ShowHideTimer (false);
                SpadesSoundsController.Instance.PlayerBid ();
			}
			else
			{
				if(m_player.GetCards().Count==13)
					ShowOnboardingMessage(true);
				
				player_view.ShowHideTimer (true);
                SpadesSoundsController.Instance.StartTurn ();
			}

			m_player_bid_or_played = false;
			float f_start_time = CurTime ();
            player_view.Player_indication.SetStartTimerImage();
            bool timer_expired = false;
            m_bidding_popup.GetComponent<BiddingPopup>().SetTimerColor(true);

            // ContestPanelController.Instance.OnPlayersTurn();

            while ((CurTime () < f_start_time + total_time) &&  (m_player_bid_or_played == false))
			{
				float time_left = total_time - (CurTime () - f_start_time);
				float timer_value = time_left / total_time;
				player_view.Player_indication.SetTimer (timer_value);
				if (bid)
				{
                    m_bidding_popup.GetComponent<BiddingPopup>().SetTimerProgressValue(timer_value);
                    if (time_left <= 6f && timer_expired == false)
                    {
                        m_bidding_popup.GetComponent<BiddingPopup>().SetTimerColor(false);
                        timer_expired = true;
                        SpadesSoundsController.Instance.PlayTurnTimerAlert();
                    }
                }
                else//play has a different timer
                {
                    if (time_left <= 6f && timer_expired == false)
                    {
                        player_view.Player_indication.SetEndTimerImage();
                        timer_expired = true;
                        SpadesSoundsController.Instance.PlayTurnTimerAlert();
                    }
                }
                yield return new WaitForSecondsRealtime(0.1f); 
			}

            
            if ((m_player_bid_or_played == false) && (m_player_left == false))// automatic bid or throw once timer expired
			{

                if (bid)
				{
					if (SpadesModelManager.Instance.GetActiveMatch().GetMatch ().Playing_mode == PlayingMode.Partners) { }
						ShowCards ();
					
                    MakeBid (m_bidding_popup.GetComponent<BiddingPopup> ().Selected_index, false);
				}
				else
				{
					//play timer finished
					if (m_selected_card_view != null)
					{
						//Player already Select card - throwing this card
						m_selected_card_view.ForceEndDrag ();
					}
					else
					{
						//no card was selected - auto playing
						Card card = AutoPlayTurn (3);
						LoggerController.Instance.Log ("Auto playing lowest card: " + card);

                        if (CheckSpadesBroken(card, 3))
                        {
                            
                            player_view.StartCoroutine(player_view.PlayerThrowCardForBreakingSpades(card, ThrowAnimationEndedCallback));
                            StartBackupThrowAnimationTimer(SPADES_BROKEN_TIME + ANIM_GRACE_TIME_TIME, ThrowAnimationEndedCallback);
                        }
                        else
                        {
                            
                            player_view.StartCoroutine(player_view.PlayerThrowCardAnimation(card, ThrowAnimationEndedCallback));
                            StartBackupThrowAnimationTimer(TableView.CARD_THROW_TIME + ANIM_GRACE_TIME_TIME, ThrowAnimationEndedCallback);
                        }

						SendThrownCardToServer (3, card);
						m_table_view.GetPlayerView (3).EnableCards (null);
						m_drag = false;
						m_player_bid_or_played = true;
					}
				}
			}
			player_view.ShowHideTimer (false);
		}

        private IEnumerator StartOtherPlayerTimer (int index, float total_time, Action<int> timer_expired = null)
		{
            //LoggerController.Instance.Log(LoggerController.Module.Game, "Starting BOT timer - for bot at Position: " + index);
            PlayerView player_view = m_table_view.GetPlayerView (index);

            player_view.Player_indication.SetStartTimerImage();
            bool color_changed = false;
            
			player_view.ShowHideTimer (true);

			float f_start_time = CurTime ();

			while ((CurTime () - f_start_time <=  total_time))
			{

				float time_left = total_time - (CurTime () - f_start_time);

				float timer_value = time_left / total_time;

				player_view.Player_indication.SetTimer (timer_value);

				if (time_left <= 6f && color_changed==false)
				{

                    SpadesSoundsController.Instance.PlayTurnTimerAlert ();
                    player_view.Player_indication.SetEndTimerImage();
                    color_changed = true;
                }

				yield return new WaitForSeconds (0.1f);
			}

            // Make sure the timer really expired and we did not came back from pause
            // if the time is less than total_time + 5 seconds, it might be a real problem 
            if (CurTime()-f_start_time < total_time+5)
            {
			    if (timer_expired != null)
				    timer_expired (index);

                //LoggerController.Instance.SendLogDataToServer("BotTimeout", "BOT Timer Expired");

            }
        }


		private void SendThrownCardToServer (int player_pos, Card card)
		{
			string command_c3_throw = SendDataCommandsBuilder.Instance.BuildThrowCommand (card, player_pos, (int)Player.PlayerType.Human);
			MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_throw, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_THROW);
			SpadesGameServerManager.Instance.Send (msg);

            // On boarding Funnel
            if (ModelManager.Instance.GetUser().NeverPlayed) {
                int cardNum = 13 - (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(3).GetCards().Count();
             /*   if(cardNum==1 || cardNum==13)
                    LobbyController.Instance.Onboarding_tracker.LogEvent("Play_" + cardNum + "_Card");*/
            }
		}


		public void ExecuteDealCommand (int dealer, List<Card> high_card, List<CardsList> hands)
		{
			HighCardAndDealReceivedFromServer (() => {
				StartTurn (m_spades_engine.Curr_player_index);
			}
                , dealer, hands, high_card);
		}

		private void HighCardAndDealReceivedFromServer (Action deal_done_callback, int dealer, List<CardsList> hands, List<Card> high_card)
		{
			//need to check - if i am a slave i need to do all the init and then proceed
			SpadesTable table = ModelManager.Instance.GetTable () as SpadesTable;

		
			UpdateModelDeal (deal_done_callback, dealer, hands, high_card);
			

			StartCoroutine (WaitForDealAnimationEnd (deal_done_callback));
		}


		private void UpdateModelDeal (Action deal_done_callback, int dealer, List<CardsList> hands, List<Card> high_card)
		{
			//updating the model
			m_spades_engine.StartRound ();
			m_table.IncRoundNumber ();

			SpadesTable table = ModelManager.Instance.GetTable () as SpadesTable;
			m_dealer = table.ServerToLocalPos (dealer);
			m_spades_engine.SetInitialDealer (m_dealer);
			LoggerController.Instance.Log ("Server Pos Dealer: " + dealer + " Local Pos Dealer: " + m_dealer);

            for (int i = 0; i < 4; i++)
			{
				int local_pos = table.ServerToLocalPos (i);
				table.GetPlayer (local_pos).SetCards (hands [i]);
			}

			// Set cards to brain
			foreach (ISpadesBrain brain in m_brains.Values)
			{
				brain.SetHand (Position.West, m_table.GetPlayer (0).GetCards ());
				brain.SetHand (Position.North, m_table.GetPlayer (1).GetCards ());
				brain.SetHand (Position.East, m_table.GetPlayer (2).GetCards ());
				brain.SetHand (Position.South, m_table.GetPlayer (3).GetCards ());
			}

			// Set cards to brain
			foreach (ISpadesBrain brain in m_brains2.Values)
			{
				brain.SetHand(Position.West, m_table.GetPlayer(0).GetCards());
				brain.SetHand(Position.North, m_table.GetPlayer(1).GetCards());
				brain.SetHand(Position.East, m_table.GetPlayer(2).GetCards());
				brain.SetHand(Position.South, m_table.GetPlayer(3).GetCards());
			}

			/*
            LoggerController.Instance.LogFormat(LoggerController.Module.Game, "{0} hand: {1}", Position.West, CardGamesFormatUtils.FormatHand(m_table.GetPlayer(0).GetCards()));
            LoggerController.Instance.LogFormat(LoggerController.Module.Game, "{0} hand: {1}", Position.North, CardGamesFormatUtils.FormatHand(m_table.GetPlayer(1).GetCards()));
            LoggerController.Instance.LogFormat(LoggerController.Module.Game, "{0} hand: {1}", Position.East, CardGamesFormatUtils.FormatHand(m_table.GetPlayer(2).GetCards()));
            LoggerController.Instance.LogFormat(LoggerController.Module.Game, "{0} hand: {1}", Position.South, CardGamesFormatUtils.FormatHand(m_table.GetPlayer(3).GetCards()));
            */
			//adding data to round logger
			m_match_logger.SetDealer((m_dealer + 1) % 4);
            m_match_logger.SetHands(m_table.GetPlayer(0).GetCards(),m_table.GetPlayer(1).GetCards(),m_table.GetPlayer(2).GetCards(),m_table.GetPlayer(3).GetCards());

			if (high_card.Count > 0)    //checking if this is the first round and need to close the searching popup - or second and onward rounds so no need for high card
			{
				for (int i = 0; i < 4; i++)
				{
					int local_pos = table.ServerToLocalPos (i);

					LoggerController.Instance.Log ("Server Index: " + i + " Local Pos Index: " + local_pos + " High Card in i: " + high_card [i].ToString ());
                    bool is_dealer = m_dealer == local_pos;

                    //m_table_view.StartCoroutine (m_table_view.ThrowDealerSelectionCards (local_pos , high_card [local_pos], is_dealer));
					m_table_view.StartCoroutine (m_table_view.HighCardAnim (local_pos, high_card [i], is_dealer, () => {
						StartDealCardsAnimation (deal_done_callback);
					}));
				}
			}
			else
			{
				//master already had to do the start game to initiate the whole deal process
				//if(!m_player.Master)
				//	StartGame (false);
				StartDealCardsAnimation (deal_done_callback);
			}
		}

		private void DealAnimationEndedCallback ()
		{
			m_deal_anim_ended = true;
		}

        private IEnumerator WaitForDealAnimationEnd (Action deal_done)
		{
			while (m_deal_anim_ended == false)
			{
				yield return null;
			}

			m_deal_anim_ended = false;

			deal_done ();
		}

		private void StartDealCardsAnimation (Action deal_done_callback)
		{
			TableView table_view = ManagerView.Instance.Table.GetComponent<TableView> ();

			m_table.SortPlayerCards ();

			CardsList[] hands = new CardsList[4];

			for (int i = 0; i < 4; i++)
			{
				hands [i] = m_table.GetPlayer(i).GetCards();
            }

			m_table_view.ShowDealerIndication (m_dealer, m_table.GetRoundNumber () == 1, true);

			CardsList cards = m_table.GetPlayer (3).GetCards ();

			StartCoroutine (m_table_view.DealCardsAnimation (m_dealer, DealAnimationEndedCallback, cards));
		}

		public void ExecuteThrowCommand (Card card, int pos, int player_type)
		{

            //LoggerController.Instance.Log(LoggerController.Module.Game, "ExecuteThrowCommand - Starting - pos: " + pos + " card: " + card);
            BotResponseTrigger botResponseTrigger = CheckForAceCutWithSpadeTrigger(card, pos);

            ThrowReceivedFromServer (card, pos, player_type, (bool trick_ended) => 
            {
                //LoggerController.Instance.Log(LoggerController.Module.Game, "ExecuteThrowCommand - Throw Animation Ended - Trick Ended: " + trick_ended);
                TriggerBotChatResponse(botResponseTrigger);

                //next move indicates who won the trick - -1 means turn didn't end yet - and any other number 0-3 is the winner index
                if (!trick_ended)
				{
					StartTurn (m_spades_engine.Curr_player_index);
				}
				else
				{
					TrickWinner (null);
				}
			});

		}

		public void ThrowReceivedFromServer (Card card, int pos, int player_type, CardAnimationEndedDelegate anim_ended_delegate)
		{
            /*
            switch (player_type)
            {
                case (int)Player.PlayerType.Human:
                    LoggerController.Instance.LogFormat(LoggerController.Module.Game, "==== Position: {0} ==== Human at {0} plays: {1}", pos, card); //, type: {2} ", player_type);
                    break;
                case (int)Player.PlayerType.Zombie:
                    LoggerController.Instance.LogFormat(LoggerController.Module.Game, "==== Position: {0} ==== Zombie at {0} plays: {1}", pos, card); //, type: {2} ", player_type);
                    break;
            }*/

            LoggerController.Instance.Log(LoggerController.Module.Game, "Answer From Server: " +pos+" plays: "+ card);

            (ModelManager.Instance.GetTable () as SpadesTable).AddThrownCardToTable (pos, card);

			Position playerPos = AIAdapter.IndexToPosition (pos);

            // Iterate over the brains, and set the card that was played.
            // For GGP bots, do not set the card if the player is already GGP
		    foreach (KeyValuePair<BrainType, ISpadesBrain> KVP in m_brains)  //foreach (ISpadesBrain brain in m_brains.Values)
		    {
                // Make sure that SetMove for GGP bots is not called on their own moves
		        if (m_brain_type_by_pos[playerPos] == BrainType.GGP && KVP.Value is GGPSpadesAI)
		            continue;

                // Call SetMove only if a brain type is used in the table
		        if (m_brain_type_by_pos.ContainsValue(KVP.Key))
		            KVP.Value.SetMove(playerPos, card);
		    }

			foreach (KeyValuePair<BrainType, ISpadesBrain> KVP in m_brains2)  //foreach (ISpadesBrain brain in m_brains.Values)
			{
				// Call SetMove only if a brain type is used in the table
				if (m_brain_type_by_pos.ContainsValue(KVP.Key))
					KVP.Value.SetMove(playerPos, card);
			}

			AddTurnToLogger (card, pos);

            SpadesSoundsController.Instance.StopTurnTimerAlert();


			if (player_type == (int)Player.PlayerType.Zombie)
			{
				(ModelManager.Instance.GetTable() as SpadesTable).GetPlayer (pos).Turns_played_as_zombie++;

				if ((ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (pos).Turns_played_as_zombie > ZOMBIE_TO_BOT_NUM_TURNS)
                {
                    (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(pos).Player_Type = Player.PlayerType.Bot;
                }
					
			}

			if (pos == 3)
			{
                LoggerController.Instance.Log(LoggerController.Module.Game, "Throw Animation Start: Humans turn (ThrowReceivedFromServer)");
				//in this case i need to go and wait for the animation to end - as it started with the actual throwing of the card
			}
			else
			{
                LoggerController.Instance.Log(LoggerController.Module.Game, "Throw Animation Start: bot turn (ThrowReceivedFromServer)");
                //play the throw animation and wait for it to complete
                PlayOtherPlayerThrowCardAnimation (pos, card, ThrowAnimationEndedCallback);

                if(CheckSpadesBroken(card, pos))
                    StartBackupThrowAnimationTimer(SPADES_BROKEN_TIME + ANIM_GRACE_TIME_TIME, ThrowAnimationEndedCallback);
                else
                    StartBackupThrowAnimationTimer(TableView.CARD_THROW_TIME + ANIM_GRACE_TIME_TIME, ThrowAnimationEndedCallback);
            }

			bool trick_ended = m_spades_engine.PlayerPlayed (card);

			StartCoroutine (WaitForThrowAnimationEnd (card, trick_ended, m_spades_engine.Curr_player_index, anim_ended_delegate));
		}


        private void StartBackupThrowAnimationTimer(float time,Action action)
        {
            if (m_special_fix_routine != null)
                StopCoroutine(m_special_fix_routine);

            m_special_fix_routine = StartCoroutine(WaitAndAction(time, () =>
            {
                 if(m_thorw_anim_ended==false)
                 {
                     //timer should have ended but it didn't - override
                     m_thorw_anim_ended = true;
                     //LoggerController.Instance.SendLogDataToServer("Special Fix", "Special Fix - Throw Timer Expired");
                     action();
                 }
            }));
        }

        private IEnumerator WaitAndAction(float wait_time, Action action)
        {
            yield return new WaitForSecondsRealtime(wait_time);
            action();
        }


        private void TriggerBotChatResponse(BotResponseTrigger botResponseTrigger)
        {
            if (botResponseTrigger != BotResponseTrigger.None)
            {
                ChatAIController.Instance.TriggerAIChatEvent((int)botResponseTrigger, (ModelManager.Instance as SpadesModelManager).Playing_mode);
            }
        }

        private BotResponseTrigger CheckForAceCutWithSpadeTrigger(Card currentPlayedCard, int playerIndex)
        {
            BotResponseTrigger botResponseTrigger = BotResponseTrigger.None;

            bool isSpadeOnTable = false;
            bool isOpponentNonSpadeAceFound = false;
            bool isPartnerNonSpadeAceFound = false;

            CardsList currentTableCardList = ModelManager.Instance.GetTable().Thrown_cards;

            // My Turn + Played Spade Card
            if (playerIndex == 3 && currentPlayedCard.Suit == Card.SuitType.Spades)
            {
                for (int i = 0; i < currentTableCardList.Count; i++)
                {
                    if (currentTableCardList[i] != null)
                    {
                        // Found Spade Card On Table
                        if (currentTableCardList[i].Suit == Card.SuitType.Spades)
                        {
                            isSpadeOnTable = true;
                        }
                        else
                        {
                            // Found Non Spade Ace Card On Table
                            if (currentTableCardList[i].Rank == Card.RankType.Ace)
                            {
                                if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
                                {
                                    if (i == 1)
                                    {
                                        isPartnerNonSpadeAceFound = true;
                                    }
                                    else
                                    {
                                        isOpponentNonSpadeAceFound = true;
                                    }
                                }
                                else
                                {
                                    isOpponentNonSpadeAceFound = true;
                                }
                            }
                        }
                    }
                }

                if (isPartnerNonSpadeAceFound)
                {
                    if (!isSpadeOnTable)
                    {
                        botResponseTrigger = BotResponseTrigger.PlayerCutsPartnerAceWithSpade;
                    }
                }
                else
                {
                    if (isOpponentNonSpadeAceFound)
                    {
                        if (!isSpadeOnTable)
                        {
                            botResponseTrigger = BotResponseTrigger.PlayerCutsOpponentAceWithSpade;
                        }
                    }
                }
            }

            return botResponseTrigger;
        }

		private void ThrowAnimationEndedCallback ()
		{
            if (m_special_fix_routine != null)
                StopCoroutine(m_special_fix_routine);

            LoggerController.Instance.Log(LoggerController.Module.Game, "Throw Animation Ended - received");
            HideOnboardingMessage();
			m_thorw_anim_ended = true;
		}

        private IEnumerator WaitForThrowAnimationEnd (Card card, bool trick_ended, int next_player_index, CardAnimationEndedDelegate anim_ended_delegate)
		{
            //LoggerController.Instance.Log(LoggerController.Module.Game, "Starting to wait for Throw animation to end");
			while (m_thorw_anim_ended == false)
			{
				yield return null;
			}

			m_thorw_anim_ended = false;
			//next move is calculated in the ThrowReceivedFromServer function - and if it returns -1 means play next player - and if 0-3 means trick winner for that player
			anim_ended_delegate (trick_ended);
		}

        private void ShowBiddingPopup ()
		{
            HideOnboardingMessage();

			float bid_delay_time = m_bid_delay;

			//TODO: check why bid delay is reduced
			if (SpadesModelManager.Instance.GetActiveMatch().GetMatch ().Playing_mode == PlayingMode.Partners && ModelManager.Instance.GetTable ().GetRoundNumber () > 1)
			{
				bid_delay_time /= 2;
			}

            // on the first 6 games, give player more time.
            if (((SpadesUserStats) ModelManager.Instance.GetUser().Stats).TotalMatchesPlayed < 6)
                bid_delay_time = 40f; 

            m_timer_coroutine = StartCoroutine (StartPlayerTimer (bid_delay_time, true));
			m_bidding_popup.GetComponent<BiddingPopup> ().Show (m_table.GetRoundNumber ());
		}

		/// <summary>
		/// checks if round or match ended
		/// </summary>
		public void RoundEndedPartners ()
		{
            TableChatController.Instance.HideChatPanel();
            m_table_view.ToggleAvatarObjectsActiveState(false);
            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch ();

            m_table_view.Game_ui_object.SetActive(false);

			int total_ew = active_match.GetCalculatedTotalResults (SpadesRoundResults.Positions.West_East);
			int total_ns = active_match.GetCalculatedTotalResults (SpadesRoundResults.Positions.North_South);
			int match_high = active_match.GetMatch ().HighPoints;
			int match_low = active_match.GetMatch ().LowPoints;
			bool match_ended = false;

		    if (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant != PlayingVariant.CallBreak)
		    {
		        if (total_ew >= match_high || total_ew <= match_low || total_ns >= match_high || total_ns <= match_low)
		            match_ended = true;
            }
            else // PlayingVariation.CallBreak
            {
		        if (ModelManager.Instance.RoundData.RoundNum >= ROUNDS_PER_GAME)
		            match_ended = true;
		    }

			int win_place = 0;
			if (total_ns > total_ew)
				win_place = 1;

			if (total_ew == total_ns)
				win_place = 1;

			//user stat round 
			int bids = m_player.GetBids ();
			int takes = m_player.GetTakes ();
            (UserStatsController.Instance as SpadesUserStatsController).UpdateEndRound (false, bids, takes);

            SpadesMissionController.Instance.InGamePanelOut();

            SpadesPopupManager.Instance.ShowRoundSummary (false, (Action next) => {
				if (match_ended)
				{
                    LoggerController.Instance.Log(LoggerController.Module.Game, "Match Ended");

                    int num_winners = 1;

					if (total_ew == total_ns)
						num_winners = 2;

					MatchEnded (win_place, num_winners, next);
				}
				else
				{
                    LoggerController.Instance.Log(LoggerController.Module.Game, "Round Ended");

                    if (m_player.Master)
						StartGame (false);

					if (next != null)
						next ();
                }
			}, match_ended, false, false);


			//need to check if this is the end of entire match - and show matching screen - or end of round and show matching screen
			DestroyAllGame();


			// Round data
			SpadesRoundData rData = ModelManager.Instance.RoundData as SpadesRoundData;

			for (int pos = 0; pos < 4; pos++)
			{
				SpadesPlayerRoundData prData = rData [m_table.LocalToServerPos (pos)];

				Player player = m_table.GetPlayer (pos);
				prData.Bid = player.GetBids ();
				prData.Takes = player.GetTakes ();
				if (player.Player_Type == Player.PlayerType.Human && player.GetBlind())
					prData.NumericBid = 1;
 			}

			PlayerRoundResults playerResults = active_match.GetLastRoundResults ().GetResults (SpadesRoundResults.Positions.North_South);
			SpadesPlayerRoundData prData1 = rData [m_table.LocalToServerPos (1)];
			SpadesPlayerRoundData prData2 = rData [m_table.LocalToServerPos (3)];

			prData1.PartnersTakes = prData2.PartnersTakes = playerResults.Takes;
			prData1.PartnersBid = prData2.PartnersBid = playerResults.Bids;
			prData1.PartnersBags = prData2.PartnersBags = playerResults.Bags_round;
			prData1.Points = prData2.Points = playerResults.Round_score;
			prData1.BagsPenalty = prData2.BagsPenalty = playerResults.Bags_penalty;
			prData1.NilBonus = prData2.NilBonus = playerResults.Nil_bonus_or_penalty;

            if (match_ended) {
                prData1.WinPlace = prData2.WinPlace = win_place;
            } else {
                prData1.WinPlace = prData2.WinPlace = -1;
            }

			playerResults = active_match.GetLastRoundResults ().GetResults (SpadesRoundResults.Positions.West_East);
			prData1 = rData [m_table.LocalToServerPos (0)];
			prData2 = rData [m_table.LocalToServerPos (2)];

			prData1.PartnersTakes = prData2.PartnersTakes = playerResults.Takes;
			prData1.PartnersBid = prData2.PartnersBid = playerResults.Bids;
			prData1.PartnersBags = prData2.PartnersBags = playerResults.Bags_round;
			prData1.Points = prData2.Points = playerResults.Round_score;
			prData1.BagsPenalty = prData2.BagsPenalty = playerResults.Bags_penalty;
			prData1.NilBonus = prData2.NilBonus = playerResults.Nil_bonus_or_penalty;
            if (match_ended)
            {
                prData1.WinPlace = prData2.WinPlace = Math.Abs(win_place-1);
            }
            else
            {
                prData1.WinPlace = prData2.WinPlace = -1;
            }

			CardGamesCommManager.Instance.EndRound ((active_match.GetMatch() as Match).GetID(), rData, UpdateEndRoundData);

            ContestPanelController.Instance.OnRoundEnded();
		}


		public void RoundEndedSolo ()
		{
            TableChatController.Instance.HideChatPanel();
            m_table_view.ToggleAvatarObjectsActiveState(false);
            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch ();

            m_table_view.Game_ui_object.SetActive(false);

            // Check if match has ended
            int[] total_scores = new int[4];
            for (int i = 0; i < 4; i++) //filling score to aux
                total_scores[i] = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)i);


		    bool matchEnded = false;
            if (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant != PlayingVariant.CallBreak)
		    {
		        if (total_scores.Max() >= active_match.GetMatch().HighPoints || total_scores.Min() <= active_match.GetMatch().LowPoints)
		            matchEnded = true;
		    }
            else // PlayingVariation.CallBreak
            {
		        if (ModelManager.Instance.RoundData.RoundNum >= ROUNDS_PER_GAME)
		            matchEnded = true;
		    }


            //user stat round 
            int bids = m_player.GetBids ();
			int takes = m_player.GetTakes ();
            (UserStatsController.Instance as SpadesUserStatsController).UpdateEndRound (false, bids, takes);

            SpadesMissionController.Instance.InGamePanelOut();

            SpadesPopupManager.Instance.ShowRoundSummary (true, (Action next) => {
                if (matchEnded) {
                    LoggerController.Instance.Log(LoggerController.Module.Game, "Match Ended");
                    int winPlace = FindWinPlaceSoloByPos(active_match, SpadesRoundResults.Positions.South);
                    int numOfWinners = FindNumOfWinnersByWinPlace(active_match, winPlace);
                    MatchEnded (winPlace, numOfWinners, next);
                }
				else
				{
                    LoggerController.Instance.Log(LoggerController.Module.Game, "Round Ended");
                    if (m_player.Master)
						StartGame (false);

					if (next != null)
						next ();
                }
            }, matchEnded, false, false);


			//need to check if this is the end of entire match - and show matching screen - or end of round and show matching screen
			DestroyAllGame ();


			// Round data
			SpadesRoundData rData = ModelManager.Instance.RoundData as SpadesRoundData;
			for (int pos = 0; pos < 4; pos++)
			{
				SpadesPlayerRoundData prData = rData [m_table.LocalToServerPos (pos)];

				Player player = m_table.GetPlayer (pos);
				prData.Bid = player.GetBids ();
				prData.Takes = player.GetTakes ();
				if (player.Player_Type == Player.PlayerType.Human && player.GetBlind())
                    prData.NumericBid = 1;


				PlayerRoundResults playerResults = active_match.GetLastRoundResults ().GetResults ((SpadesRoundResults.Positions)pos);
				prData.Bags = playerResults.Bags_round;
				prData.Points = playerResults.Round_score;
				prData.BagsPenalty = playerResults.Bags_penalty;
				prData.NilBonus = playerResults.Nil_bonus_or_penalty;
				
                if (matchEnded) {
                    prData.WinPlace = FindWinPlaceSoloByPos(active_match,(SpadesRoundResults.Positions)pos);
                } else {
					prData.WinPlace = -1;
                }
			}
			CardGamesCommManager.Instance.EndRound((active_match.GetMatch() as Match).GetID(), rData, UpdateEndRoundData);
            ContestPanelController.Instance.OnRoundEnded();
        }

        private void UpdateEndRoundData(Challenge challengeUpdate, Contest contest)
        {
            if (challengeUpdate != null)
                MissionController.Instance.UpdateMissionEndRound(challengeUpdate);


			// Check if we are in Arena - for now there are no contests in Arena by definition (Ran 6/2/2020)
			SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
			if (!active_match.IsSingleMatch)
				contest = null;

			ContestPanelController.Instance.OnEndRound(contest);

		}


		/// <summary>
		/// Finds the win place by position for solo games.
		/// </summary>
		private int FindWinPlaceSoloByPos(SpadesActiveMatch activeMatch, SpadesRoundResults.Positions pos) {
            // Put all the total scores into a set to eliminate duplicates
            HashSet<int> scoresSet = new HashSet<int>();
            for (int p = 0; p < 4; p++)
            {
                scoresSet.Add(activeMatch.GetCalculatedTotalResults((SpadesRoundResults.Positions)p));
            }

            int highestScore = scoresSet.Max(r => r);
            scoresSet.Remove(highestScore);
            int nextHighestScore = scoresSet.Max(r => r);

            if (activeMatch.GetCalculatedTotalResults(pos) == highestScore) return 1;
            else if (activeMatch.GetCalculatedTotalResults(pos) == nextHighestScore) return 2;
            else return 0;
        }


        private int FindNumOfWinnersByWinPlace(SpadesActiveMatch activeMatch, int winPlace) {
            int result = 0;
            for (int p = 0; p < 4; p++)
            {
                if (FindWinPlaceSoloByPos(activeMatch, (SpadesRoundResults.Positions)p) == winPlace)
                    result++;
            }
            return result;
        }


		public void MatchEnded (int win_place, int num_winners, Action answer_from_server_received)
		{
			m_is_game_active = false;

			// Disconnect the game server socket
			SpadesGameServerManager.Instance.Disconnect ();

            // Cheaters checks:
            // Total takes for all players should be exactly 13
            int totalTakes = 0;
            for (int i = 0; i < 4; i++)
            {
                totalTakes+= m_table.GetPlayer(i).GetTakes();
            }
            if (totalTakes != 13)
                throw new Exception("Cannot finish this round");

            if (ModelManager.Instance.GetUser ().GetMatchHistoryFlag ())
				m_match_ended_delegate (win_place, num_winners, m_match_logger.GetCompressedHistoryAndVersion (), answer_from_server_received);
			else
				m_match_ended_delegate (win_place, num_winners, null, answer_from_server_received);
		}


		public void DestroyAllGame ()
		{
			//resetting all the indicators
			m_bidding_complete = false;
			for (int i = 0; i < 4; i++)
			{
				(ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (i).DeleteAllCards ();
				m_table_view.GetPlayerView (i).DeleteAllCards ();
			}
			m_table_view.ClearTable ();

		    if ((ModelManager.Instance.GetActiveMatch() as SpadesActiveMatch).GetMatch().Variant != PlayingVariant.CallBreak)
		    {
		        m_table_view.ResetSpadesBrokenAnimations(true);
            }
		    else
		    {
                // TODO: CALLBREAK HANDLING NEEDED??
                m_table_view.ResetSpadesBrokenAnimations(false);
            }

            SpadesSoundsController.Instance.StopTurnTimerAlert ();

			StopAllCoroutines ();
            m_table_view.StopAllCoroutines();

            // Make sure fast forward in stopped
            Time.timeScale = 1;
        }


        private void FakeDisconnection()
        {
            if (m_spades_engine.Curr_trick_num == m_fake_disconnect_trick_number)
            {
                m_fake_disconnect_trick_number = -1;

                int disconnecting_Bot_Index = m_random.Next(0, 3);

                while (disconnecting_Bot_Index == m_spades_engine.Curr_player_index)
                    disconnecting_Bot_Index = m_random.Next(0, 3);

                m_game_had_fake_disconnection = true;
                StartCoroutine(DelayFakeDisconnection(disconnecting_Bot_Index));
            }
        }


        IEnumerator DelayFakeDisconnection(int index)
        {
            yield return new WaitForSecondsRealtime((float)m_random.NextDouble() * 2);

            m_table.GetPlayer(index).Player_Type = Player.PlayerType.Zombie;
        }


		public void StartTurn (int pos)
		{
            if (Spades_engine.Curr_trick_num==0)
            {
                LoggerController.Instance.Log(LoggerController.Module.Game, "Turn Started - Bidding Phase  - Current Player: " + Spades_engine.Curr_player_index);
            }
            else
            {
                if (Spades_engine.Number_of_player_played == 0)
                    LoggerController.Instance.Log(LoggerController.Module.Game, "--------------------------------------------------Turn Started - for Trick# " + Spades_engine.Curr_trick_num + " Playing Turn# "+ Spades_engine.Number_of_player_played+ " - Curr Player: " + Spades_engine.Curr_player_index);
                else
                    LoggerController.Instance.Log(LoggerController.Module.Game, "Turn Started - for Trick# " + Spades_engine.Curr_trick_num + " Playing Turn# " + Spades_engine.Number_of_player_played + " - Curr Player: " + Spades_engine.Curr_player_index);

            }

            //check if need to activate the fake disconnection
            FakeDisconnection();
            
            //stop and hides the previous player timer
            CancelTimers ();

			if (pos == 3) // Human
			{
                //Current player
                if (m_spades_engine.IsBidding)
				{
					//bidding phase
					ShowBiddingPopup ();
				}
				else
				{
					//playing phase

					//enable playable cards
					PlayerView player_view = m_table_view.GetPlayerView (3);
					Player player = m_table.GetPlayer (3);
				    
                    PlayingVariant gameVariation = (ModelManager.Instance.GetActiveMatch()as SpadesActiveMatch).GetMatch().Variant;

                    CardsList playableCards = SpadesUtils.GetPlayableCards (m_table.GetLeadingSuit (), m_table.GetSpadesBroken (), player.GetCards (), gameVariation, m_table.Thrown_cards); 
					player_view.EnableCards (playableCards);

					float delay = m_play_delay;
					//last card - auto throw no waiting
					if (m_spades_engine.Curr_trick_num == 13)
						delay = 0f;

					//start timer animation
					m_timer_coroutine = StartCoroutine (StartPlayerTimer (delay, false));
				}
			}
            else //Bot:
            {
				//set the delay
				float delay = m_play_delay;
				if (m_spades_engine.IsBidding)
					delay = m_bid_delay;

                m_timer_coroutine = StartCoroutine (StartOtherPlayerTimer (pos, delay, OtherPlayerTimerCompleted));

                //if you master - start the bot coroutine 

				if (m_player.Master)
				{
                    if (m_table.GetPlayer (pos).Player_Type == Player.PlayerType.Bot)
						m_bot_play_coroutine = StartCoroutine (BotBidOrPlayCoroutine (pos, m_spades_engine.IsBidding));
				}
			}
		}


		private void OtherPlayerTimerCompleted (int pos)
		{
            LoggerController.Instance.Log(LoggerController.Module.Game, "Timer completed for player at local pos: " + pos);

            //slaves - do nothing
            if (!m_player.Master)
			{
				m_slave_timer_exp_coroutine = StartCoroutine (WaitAndCheckAgainTimerComplete (pos));
				return;
			}

			// i am master
			Player player = m_table.GetPlayer (pos);
            LoggerController.Instance.Log(LoggerController.Module.Game, "The player we need to play for is of type: " + player.Player_Type.ToString());

            //human - do nothing, will auto play for himself
            if (player.Player_Type == Player.PlayerType.Human)
			{
				m_master_timer_exp_coroutine = StartCoroutine (WaitForHumanToPlayOrDisconnect (pos));
				return;
			}
            //zombie - play auto card
            PlayerView player_view = m_table_view.GetPlayerView(pos);

			if (player.Player_Type == Player.PlayerType.Zombie)
				AutoPlayTurnZombie (pos, !m_spades_engine.IsBidding, player_view);

            // bot - when master changes during the turn
            // NOTE: There is an assumption here that a bot will never reach the end of the timer!
            // REMOVED FOR NOW BY RAN (12/4/2018)
            //if (player.Player_Type == Player.PlayerType.Bot)
            //	m_bot_play_coroutine = StartCoroutine (BotBidOrPlayCoroutine (pos, m_spades_engine.IsBidding, true));
        }

        private IEnumerator WaitAndCheckAgainTimerComplete (int pos)
		{
			yield return new WaitForSeconds (0.5f);
			OtherPlayerTimerCompleted (pos);
		}

        private IEnumerator WaitForHumanToPlayOrDisconnect (int pos)
		{
			yield return new WaitForSeconds (7f);

			if (m_bot_play_coroutine != null)
				StopCoroutine (m_bot_play_coroutine);

			//this is for the case that i got disconnected - so nbo point to play it on the local - as the command to play this will come back in the C6
			if (!m_disconnected)
				m_bot_play_coroutine = StartCoroutine (BotBidOrPlayCoroutine (pos, m_spades_engine.IsBidding, true));

			//notify that player in pos pos is disconnected - with a new command to Alon 
		}

		private void AutoPlayTurnZombie (int pos, bool is_play, PlayerView player_view = null)
		{
			if (!m_disconnected && (ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (pos).Player_Type != Player.PlayerType.Human)
			{
				if (is_play)//play
				{
					Card card = AutoPlayTurn (pos);
					(ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (pos).Turns_played_as_zombie++;

                    LoggerController.Instance.Log(LoggerController.Module.Game, "Auto play turn zombie: index: " + pos + "Play or bid: " + is_play);

                    //m_spadesBrain.SetMove (AIAdapter.IndexToPosition (pos), card);
                    string command_c3_throw = SendDataCommandsBuilder.Instance.BuildThrowCommand (card, pos, (int)Player.PlayerType.Zombie);
					MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_throw, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_THROW);
					SpadesGameServerManager.Instance.Send (msg);
				}
				else//bid
				{
					(ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (pos).SetBids (1);
					(ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (pos).Turns_played_as_zombie++;


					//if i am the master - need to send the bid to the others
					string command_c3_bid = SendDataCommandsBuilder.Instance.BuildBidCommand (1, false, pos, (int)Player.PlayerType.Zombie);
					MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_bid, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_BID);
					SpadesGameServerManager.Instance.Send (msg);
				}
			}
		}

		/// <summary>
		/// executes the bid
		/// </summary>
		/// <param name="bid_value">Bid value.</param>
		public void MakeBid (int bid_value, bool blind)
		{
		    // save bidding position in prData.BiddingPosition 
		    int biddingPosition = 1;
		    List<int> pos_list = new List<int> { 0, 1, 2, 3 };
		    foreach (int p in pos_list)
		    {
		        if (m_table.GetPlayer(p).GetBids() != -1)
		        {biddingPosition++;}
		    }
            SpadesPlayerRoundData prData = (ModelManager.Instance.RoundData as SpadesRoundData)[m_table.LocalToServerPos(m_spades_engine.Curr_player_index)];
            prData.BiddingPosition = biddingPosition;


            //if bidding keyboard not marked - auto bid 1
		    if (bid_value == -1)
		    {
		        switch (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant)
		        {
		            case PlayingVariant.Classic:
                        int totalMatches = (ModelManager.Instance.GetUser().Stats as SpadesUserStats).TotalMatchesPlayed; // Get the number of games played //
                        bid_value = totalMatches < 6 ? 4 : 1;
                        break;
                    case PlayingVariant.Jokers:
		            case PlayingVariant.CallBreak:
		                bid_value = 1;
                        break;
                    case (PlayingVariant.Suicide):
                        // If the partner has not bid yet or bid 0, bid 4.
                        // else bid 0
                        int partnerBid = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(1).GetBids();
                        if (partnerBid<=0)
		                    bid_value = 4;
                        else {
                            bid_value = 0;
                        }
                        break;
		            case PlayingVariant.Whiz: // bid = #Spades or 0    
                    case PlayingVariant.Mirror:  // bid = #Spades
                        bid_value = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(3).GetCards().CardsBySuits(Card.SuitType.Spades).Count ;
                        break;
                    default:
                        LoggerController.Instance.Log(LoggerController.Module.Game, "Unknown variation for MakeBid");
                        break;
		        }
		    }

			m_player_bid_or_played = true;
            SpadesSoundsController.Instance.StopTurnTimerAlert();
			m_player.SetBids (bid_value);

            if (bid_value > 5)
            {
                ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.PlayerBids6Plus, (ModelManager.Instance as SpadesModelManager).Playing_mode);
            }

            if (blind)
            {
                m_player.SetBlind(true);

                if (bid_value == 0)
                {
                    ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.PlayerBidsBlindNil, (ModelManager.Instance as SpadesModelManager).Playing_mode);
                }
            }

			//sending the bid to all players
			string command_c3_bid = SendDataCommandsBuilder.Instance.BuildBidCommand (bid_value, blind, 3, (int)Player.PlayerType.Human);
			MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand (command_c3_bid, m_table.Game_id, SendDataCommandsBuilder.SEND_DATA_BID);
			SpadesGameServerManager.Instance.Send (msg);

            // On-boarding Funnel
           /* if (ModelManager.Instance.GetUser().NeverPlayed)
                LobbyController.Instance.Onboarding_tracker.LogEvent("Bid");
*/
		}

		public void ExecuteBidCommand (int bid_value, int player_pos, bool blind, int player_type)
		{
			BidReceivedFromServer (bid_value, player_pos, blind, player_type, () => {
				if (m_spades_engine.PlayerBid ())
					BiddingComplete ();
				else
				{
					StartTurn (m_spades_engine.Curr_player_index);
				}
			});
		}

		public void BidReceivedFromServer (int bid_value, int player_pos, bool blind, int player_type, Action bid_done)
		{
            LoggerController.Instance.Log(LoggerController.Module.Game, "BidReceivedFromServer - position: " + player_pos + ", bid: " + bid_value + ", blind: " + blind);

            if (player_type == (int)Player.PlayerType.Zombie)
			{
				m_table.GetPlayer (player_pos).Turns_played_as_zombie++;
				if ((ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (player_pos).Turns_played_as_zombie > ZOMBIE_TO_BOT_NUM_TURNS)
                {
                    (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(player_pos).Player_Type = Player.PlayerType.Bot;
                }
					
			}

			m_player_bid_or_played = true;
            SpadesSoundsController.Instance.StopTurnTimerAlert();

			if (bid_value == -1)
				bid_value = 0;

			m_table.GetPlayer (player_pos).SetBids (bid_value);
			if (blind)
				m_table.GetPlayer (player_pos).SetBlind (true);
			m_table_view.GetPlayerView (player_pos).ShowBidBubbleAndSign ();

			// Brain
            foreach (ISpadesBrain brain in m_brains.Values)
            {
				// If the initiating bidding brain is GGP, do not set the GGP twice
				if (m_brain_type_by_pos[AIAdapter.IndexToPosition(player_pos)] == BrainType.GGP && brain is GGPSpadesAI)
					continue;
               
                brain.SetBid(AIAdapter.IndexToPosition(player_pos), bid_value, blind);
			}

			foreach (ISpadesBrain brain in m_brains2.Values)
			{
				brain.SetBid(AIAdapter.IndexToPosition(player_pos), bid_value, blind);
			}

			if (bid_done != null)
				bid_done ();
		}


		/// <summary>
		/// flips the cards over
		/// </summary>
		public void ShowCards ()
		{
			if (!m_cards_revealed)
			{
				ManagerView.Instance.Table.GetComponent<TableView> ().RevealCards ();
				m_cards_revealed = true;
			}
		}

		/// <summary>
		/// Checks the spades broken.
		/// </summary>
		/// <param name="temp_card_model">Temp card model.</param>
		public bool CheckSpadesBroken (Card card, int player_index)
		{
            if (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant == PlayingVariant.CallBreak)
                //in Callbreak there is no spades broken
                return false;

			if (card.Suit == Card.SuitType.Spades)
			{
			    if (!(ModelManager.Instance.GetTable() as SpadesTable).GetSpadesBroken() && (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant != PlayingVariant.CallBreak))
				{
					if (player_index == 3)
						StartCoroutine (DelayBreakSpadesAchievement ());

					(ModelManager.Instance.GetTable () as SpadesTable).SetSpadesBroken (true);
					//ManagerView.Instance.PlaySpadeBrokenAnim ();
					return true;
				}
				else
					return false;
			}
			else
				return false;
		}

        private IEnumerator DelayBreakSpadesAchievement ()
		{
			yield return new WaitForSeconds (1.5f);
			AchievementsManager.Instance.Set (AchievementsController.BreakSpades, 1);
		}

		/// <summary>
		/// triggers the animation in the "other players" - currently also "simulating" the event that animation ended 
		/// </summary>
		/// <param name="player_index">Player index.</param>
		/// <param name="card_index">Card index.</param>
		public void PlayOtherPlayerThrowCardAnimation (int player_index, Card card, Action anim_done_callback)
		{
			bool spades_broken = CheckSpadesBroken (card, player_index);

            if (!m_player_left)
			{
				if (spades_broken)
					m_table_view.StartCoroutine (m_table_view.ComputerThrowSpadesBorkenCard (player_index, card, anim_done_callback));
				else
					m_table_view.StartCoroutine (m_table_view.ComputerThrowCard (player_index, card, anim_done_callback));
			}
		}

		public void DragEnded (float release_pos, Vector3 org_pos, CardView org_card, float org_rot_z)
		{
			//check if released over something or not to throw or to not throw

			int release_limit = 159;
            PlayerView player_view = m_table_view.GetPlayerView(3);
			if (release_pos > release_limit)
			{
				org_card.Card_thrown = true;
				Card selected_card = m_selected_card_view.GetCardModel ();

				if (CheckSpadesBroken (selected_card, 3))
				{
                    //LoggerController.Instance.Log(LoggerController.Module.Game, "DragEnded - Spades broken");
                    player_view.StartCoroutine (player_view.PlayerThrowCardForBreakingSpades (selected_card, ThrowAnimationEndedCallback));
                    StartBackupThrowAnimationTimer(SPADES_BROKEN_TIME + ANIM_GRACE_TIME_TIME, ThrowAnimationEndedCallback);

                }
				else
				{
                    //LoggerController.Instance.Log(LoggerController.Module.Game, "DragEnded - Not Spades broken");
                    player_view.StartCoroutine (player_view.PlayerThrowCardAnimation (selected_card, ThrowAnimationEndedCallback));
                    StartBackupThrowAnimationTimer(TableView.CARD_THROW_TIME + ANIM_GRACE_TIME_TIME, ThrowAnimationEndedCallback);
                }
				SendThrownCardToServer (3, selected_card);
				m_table_view.GetPlayerView (3).EnableCards (null);

				m_drag = false;
				m_player_bid_or_played = true;
                SpadesSoundsController.Instance.StopTurnTimerAlert();
			}
			else
			{
				//make card scale down
				StartCoroutine (player_view.ArrangeHandAnimation (org_card, org_pos, org_rot_z,0.0025f));
				org_card.ScaleMeBackToNormal ();
				org_card.SetMyOrderBack ();
				m_drag = false;
			}
		}

		public void PlayerClickedCard (GameObject cardView)
		{
			if (m_spades_engine.Curr_player_index == 3 && m_bidding_complete && !m_drag && !PopupManager.Instance.IsPopupShown)
			{
				PlayerView temp_player_view = m_table_view.GetPlayerView (3);
				CardView temp_card_view = cardView.GetComponent<CardView> ();

				temp_card_view.ReadScalarData ();
				temp_card_view.StartDrag ();
				m_drag = true;
			}
#if (UNITY_WEBGL || UNITY_EDITOR)
            else if (m_selected_card_view == cardView.GetComponent<CardView> () && m_drag)
			{
				m_selected_card_view.ClickThrowCard ();
			}
			else if (m_selected_card_view != cardView.GetComponent<CardView> () && m_drag)
			{
				m_selected_card_view.MoveCardBackDown ();
                PlayerView temp_player_view = m_table_view.GetPlayerView(3);
				CardView temp_card_view = cardView.GetComponent<CardView> ();
				temp_card_view.StartDrag ();
				m_drag = true;
			}
#endif
		}


		/// <summary>
		/// send the trick winner indication to the right player
		/// </summary>
		/// <param name="index">Index.</param>
		public void TrickWinner (Action trick_winner_done)
		{
			StartCoroutine (PlayTrickWinnerAnim (trick_winner_done));
		}

		private IEnumerator PlayTrickWinnerAnim (Action trick_winner_done)
		{
            //LoggerController.Instance.Log(LoggerController.Module.Game, "Starting PlayTrickWinnerAnim");
			int index = m_spades_engine.Curr_player_index;

			if (m_player.Master)
				m_chat_bot_controller.TrickTaken (m_spades_engine.Curr_trick_num, AIAdapter.IndexToPosition (index));

			yield return new WaitForSeconds (.25f);
			bool done = false;

			while (!done) // this is for the case the last played card is a spades making spades broken - which takes longer - and so we need to delay before we pull the cards on the table
			{
				if (m_cards_finished_throw_anim == 4)
					done = true;
				else
					yield return new WaitForSeconds (0.25f);
			}

			m_cards_finished_throw_anim = 0;
			m_match_logger.SetTurnResults (m_turn_logger, index);

            // start exit animation - 
            LoggerController.Instance.Log(LoggerController.Module.Game, "Starting end trick Animation - winning position: " + index);

            bool exit_anim_called_back = false;
            m_table_view.StartCoroutine (m_table_view.ExitAnimation (index, () => {
                if (!exit_anim_called_back)
                {
                    exit_anim_called_back = true;
                    HandleExitAnimationEnded(trick_winner_done);
                }
			}));

            
            (ModelManager.Instance.GetTable () as SpadesTable).GetPlayer (index).IncrementTakes ();
            // My Player
            if (index == 3)
            {
                CheckForAIChatResponseTriggers();
            }
            else
            {
                int playerBids = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(index).GetBids();
                int playerTakes = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(index).GetTakes();

                if (playerBids == 0 && playerTakes == 1)
                {
                    // We Do Not Trigger This Event In Solo As It Causes Confusion - If All Are Opponents, Which Is Happy And Which Is Sad ?
                    if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
                    {
                        if (index != 1)
                        {
                            ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.OpponentNilIsBroken, (ModelManager.Instance as SpadesModelManager).Playing_mode);
                        }
                    }
                }
            }

			// Brain
			foreach (ISpadesBrain brain in m_brains.Values)
			{
				brain.TakeTrick (AIAdapter.IndexToPosition (index));
			}
			foreach (ISpadesBrain brain in m_brains2.Values)
			{
				brain.TakeTrick(AIAdapter.IndexToPosition(index));
			}
			LoggerController.Instance.LogFormat(LoggerController.Module.Game, "{0} Takes trick", AIAdapter.IndexToPosition(index));

            (ModelManager.Instance.GetTable() as SpadesTable).ResetThrownCardsList();
        }

        private void CheckForAIChatResponseTriggers()
        {
            int playerBids = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(3).GetBids();
            int playerTakes = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(3).GetTakes();

            int playerWithPartnerBids = playerBids + (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(1).GetBids();
            int playerWithPartnerTakes = playerTakes + (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(1).GetTakes();

            if (playerBids == 0 && playerTakes == 1)
            {
                ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.PlayerNilIsBroken, (ModelManager.Instance as SpadesModelManager).Playing_mode);
            }

            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch();
            SpadesRoundResults roundResults = activeMatch.GetLastRoundResults();
            PlayerRoundResults lastRoundResults = null;

            if (roundResults != null)
            {
                try
                {
                    if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
                    {
                        lastRoundResults = roundResults.GetResults(SpadesRoundResults.Positions.North_South);
                    }
                    else 
                    {
                        lastRoundResults = roundResults.GetResults(SpadesRoundResults.Positions.South);
                    }
                }
                catch(Exception e)
                {
                    // NOT SURE WHY AN EXCEPTION IS THROWN HERE, BUT MAKE SURE IT'S CATCH
                    LoggerController.Instance.LogError("CheckForAIChatResponseTriggers: " + e);
                }
            }

            int matchBagsCount = activeMatch.GetMatch().BagsCount;
            int currentRoundBags = playerTakes - playerBids;
            if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
            {
                currentRoundBags = playerWithPartnerTakes - playerWithPartnerBids;
            }
            if (lastRoundResults != null)
            {
                int totalBagsUpToLastRound = lastRoundResults.Bags_total;
                if (totalBagsUpToLastRound + currentRoundBags >= matchBagsCount)
                {
                    ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.PlayerTakesBagThatMakesPenalty, (ModelManager.Instance as SpadesModelManager).Playing_mode);
                }
            }
            else
            {
                if (currentRoundBags >= matchBagsCount)
                {
                    ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.PlayerTakesBagThatMakesPenalty, (ModelManager.Instance as SpadesModelManager).Playing_mode);
                }
            }
        }


        /// <summary>
        /// called when the exit animation ended so we can continue playing
        /// </summary>
        public void HandleExitAnimationEnded (Action exit_anim_done)
		{
            LoggerController.Instance.Log(LoggerController.Module.Game, " ==== Handle Exit Animation For Trick Num: " + (m_spades_engine.Curr_trick_num-1) + " ==========");
            if (m_spades_engine.Curr_trick_num == 14)
			{
                if (m_timer_coroutine != null)
                    StopCoroutine(m_timer_coroutine);

                m_timer_coroutine = null;
				if (m_bot_play_coroutine != null)
				{
					StopCoroutine (m_bot_play_coroutine);
                    LoggerController.Instance.Log(LoggerController.Module.Game, "m_bot_play_coroutine stopped");
                }
				m_bot_play_coroutine = null;
				m_spades_engine.CalcRoundScore ();
			}
			else
			{
                //LoggerController.Instance.Log(LoggerController.Module.Game, "------------------------------------------Starting Trick # " + Spades_engine.Curr_trick_num);
                StartTurn (m_spades_engine.Curr_player_index);

				foreach (ISpadesBrain brain in m_brains.Values)
				{
					brain.StartTrick ();
				}

				foreach (ISpadesBrain brain in m_brains2.Values)
				{
					brain.StartTrick();
				}
			}

			if (exit_anim_done != null)
				exit_anim_done ();
		}

		public Card AutoPlayTurn (int player_index)
		{
		    PlayingVariant gameVariation = SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant;
            CardsList playableCards = SpadesUtils.GetPlayableCards (m_table.GetLeadingSuit (), m_table.GetSpadesBroken (), m_table.GetPlayer (player_index).GetCards (), gameVariation, m_table.Thrown_cards);
            playableCards.SortHighToLowRankFirstSpadesFirst ();

			Card lowestCard = playableCards [playableCards.Count - 1];
			return lowestCard;
		}

		public void ChatReceivedFromServer (int chat_index, int player_pos)
		{
            LoggerController.Instance.Log(LoggerController.Module.Game, "Chat received from server - from player pos: " + player_pos);

            PlayerView player_view = m_table_view.GetPlayerView(player_pos);
		}

		private void DisconnectDetected ()
		{
			//m_disconnected = true;
			//m_disconnected_popup = CardGamesPopupManager.Instance.ShowDisconnectedPopup();
			//this prevents the local game from continuing when disconnection was detected
			CancelTimers ();
			Player.Master = false;
		}

		public void ReconnectReceived (List<Param> commands, List<Param> connected_list)
		{
			//need to FF all the commands and then close the popup

			m_disconnected = false;

			if (m_timer_coroutine != null)
			{
				LoggerController.Instance.Log ("Co routine stopped in ReconnectReceived");
                //StopCoroutine (m_timer_coroutine);
            }

			for (int i = 0; i < 4; i++)
				(ModelManager.Instance.GetTable () as SpadesTable).Players [i].Master = false;

			int master_index = 0;

			if (connected_list.Count > 0)
				master_index = Convert.ToInt32 (connected_list [0].Value);

			int master_index_local_pos = m_table.ServerToLocalPos (master_index);

			if (master_index_local_pos == 3)
			{
				LoggerController.Instance.Log ("I am the new master - however - i will only assign myself after the playing FF commands will finish");
                
                m_change_me_to_master_after_FF = true;
			}
			else
			{
				(ModelManager.Instance.GetTable () as SpadesTable).Players [master_index_local_pos].Master = true;
				m_change_me_to_master_after_FF = false;
			}

			LoggerController.Instance.Log ("Master is in local position: " + master_index_local_pos + " Server pos: " + master_index);

			m_commands = commands;

			//this part fixes a card that was thrown but the server never got it - so only thrown locally
			if (!m_card_thrown_processed)
			{
				//i throw a card - i can see it thrown - but it never got process by the server - need to remove it from the table - and also put it back into the player hand - and re arrange the hand
				//need to look in the table thrown cards - for a Card called Card+server pos index - this is the one that needs to go back to the player hand
				string name = "Card" + m_player.ServerPos;
				bool found = false;
				Card card = null;

				//searching for the thrown card in the thrown cards holder
				foreach (Transform T in m_table_view.Thrown_cards_object.transform)
				{
					if (T.name == "Card3" && !found)
					{
						//delete the card from the table
						card = new Card (T.gameObject.GetComponent<CardView> ().GetCardModel ().Suit, T.gameObject.GetComponent<CardView> ().GetCardModel ().Rank);
						found = true;
						Destroy (T.gameObject);
					}
				}

				if (found)
				{
					//delete all the card views in the hand
					foreach (Transform T in m_table_view.GetPlayerView(3).Hand.transform)
					{
						Destroy (T.gameObject);
					}
					m_table_view.GetPlayerView (3).DeleteAllCards ();

					//builds the hand according to the model 
					m_table_view.GetPlayerView (3).AddCards (m_table.GetPlayer (3).GetCards (), true);
					m_table_view.GetPlayerView (3).RevealCards (true, false);
					m_table_view.GetPlayerView (3).MoveCardsToPositions ();
				}
			}

			bool data_valid = true;
			m_play_recorded_commands = true;

			Time.timeScale = 25f;

			if (m_commands.Count == 1)//this is empty - but i still get something from Alon
			{
				LoggerController.Instance.Log ("VALUE " + m_commands [0]);

				if (m_commands [0].Value == "" || m_commands [0].Value == null)
				{
					LoggerController.Instance.Log ("empty data");
					data_valid = false;
				}
			}
			m_playback_commands_counter = 0;

			LoggerController.Instance.Log ("Starting to play local commands - " + m_commands.Count + " Data valid: " + data_valid);

			if (data_valid)
				PlayNextCommandsLocally ();

			//need to tell the master that the missing player is back - and can be re assigned
			if (m_disconnected_popup != null)
				m_disconnected_popup.CloseDisconnectPopup ();
		}

		private void PlayNextCommandsLocally ()
		{
			if (m_playback_commands_counter < m_commands.Count)
			{
				MMessage msg = new MMessage ();
				string msg_value = m_commands [m_playback_commands_counter].Value;
				msg = m_decoder.Decode (utf8.GetBytes (msg_value));
				LoggerController.Instance.Log ("Playing Command: " + m_playback_commands_counter + " Value: " + m_commands [m_playback_commands_counter].Value);
				ProcessCommand (msg.Code, msg.Params);

				m_playback_commands_counter++;
			}
			else
			{
				PostPlayingLocalCommands ();
			}
		}

		private void PostPlayingLocalCommands ()
		{
			//making the return play the new master ony after he FF his commands
			if (m_change_me_to_master_after_FF)
			{
				LoggerController.Instance.Log ("Playing FF commands, I am now assigned as the new master");
				(ModelManager.Instance.GetTable () as SpadesTable).Players [3].Master = true;
			}

			Time.timeScale = 1f;
			m_play_recorded_commands = false;
			ResumePlaying ();
		}

		private void ResumePlaying ()
		{
			LoggerController.Instance.Log ("Resuming play - Current Player Index: " + m_spades_engine.Curr_player_index);

			if (m_spades_engine.Curr_player_index == 3)
			{
				LoggerController.Instance.Log ("Notify player to play - Round Controller - " + m_spades_engine.Curr_player_index);
				//RoundController.Instance.NotifyPlayerToPlayOrBid (3);

				StartTurn (3);
			}
			else
			{
				m_table_view.GetPlayerView (3).EnableCards (null);
			}
		}

		private void ProcessCommand (string code, List<Param> data)
		{
			switch (code)
			{
			case ("K1")://deal + high card -- DONE
				{

					SimpleParam high_cards_param = (SimpleParam)data [0];
					CompositeParam hands_param = (CompositeParam)data [1];
					SimpleParam dealer_param = (SimpleParam)data [2];

					CardsList high_cards_list = new CardsList (high_cards_param.Value);

					int dealer = Convert.ToInt32 (dealer_param.Value);

					List<CardsList> hands = new List<CardsList> ();

					for (int i = 0; i < 4; i++)
					{
						hands.Add (new CardsList (hands_param.Params [i].Value));
					}

					//RoundController.Instance.HighCardAndDealReceivedFromServer (PlayNextCommandsLocally,dealer, high_cards_list, hands);
					break;
				}
			case ("K2")://bid -- DONE
				{
					int player_server_index = Convert.ToInt32 (data [0].Value);
					int bid_value = Convert.ToInt32 (data [1].Value);
					int player_type = Convert.ToInt32 (data [2].Value);
					int player_pos = ModelManager.Instance.GetTable ().ServerToLocalPos (player_server_index);

					bool blind = bid_value == -1;

                    if (player_pos == 3)//this means its the move i didn't make and the bot played for me
					{
						ShowCards ();
						RoundController.Instance.BidReceivedFromServer (bid_value, player_pos, blind, player_type, PlayNextCommandsLocally);
						m_bidding_popup.GetComponent<BiddingPopup> ().CloseBiddingKeyboard ();
					}
					else
					{
						RoundController.Instance.BidReceivedFromServer (bid_value, player_pos, blind, player_type, PlayNextCommandsLocally);
					}
					break;
				}
			case ("K3")://throw -- DONE
				{
					int player_server_index = Convert.ToInt32 (data [0].Value);
					Card card = new Card (data [1].Value);
					int player_type = Convert.ToInt32 (data [2].Value);
					int player_pos = ModelManager.Instance.GetTable ().ServerToLocalPos (player_server_index);

					if (player_pos == 3)//this means its the move i didn't make and the bot played for me
					{
						LoggerController.Instance.Log ("Throw3 - playing FF commands");
						StartCoroutine (m_table_view.GetPlayerView (3).PlayerThrowCardAnimation (card, ThrowAnimationEndedCallback));
					}

                    ThrowReceivedFromServer(card, player_pos, player_type, (bool trick_ended) =>
                    {
                        if (!trick_ended)
                        {
                            PlayNextCommandsLocally();
                        }
                        else
                        {
                            TrickWinner(PlayNextCommandsLocally);
                        }
                    });
					break;
				}
			case ("K4")://chat
				{
					int player_server_index = Convert.ToInt32 (data [0].Value);
					int chat_index = Convert.ToInt32 (data [1].Value);
					int player_pos = ModelManager.Instance.GetTable ().ServerToLocalPos (player_server_index);

					if (player_pos == 3)//this means its the move i didn't make and the bot played for me
					{
						//PlayUserChat ((spades.views.AvatarPictureView.ChatType)chat_index);
					}
					else
					{
						RoundController.Instance.ChatReceivedFromServer (chat_index, player_pos);
					}
					PlayNextCommandsLocally ();
					break;
				}
			case ("K5")://bots
				{
					break;
				}
			default:
				{
					LoggerController.Instance.Log ("ERROR - UNHANDLED MESSAGE RECEIVED");
					break;
				}
			}
		}


        private void ShowOnboardingMessage (bool is_play)
		{
			//detect On-board sentence and display if needed
			if (ModelManager.Instance.GetUser ().NeverPlayed)
			{

				string play_sentence = "";

#if UNITY_WEBGL
                play_sentence = "Double click the card you would like to play";
#else
				play_sentence = "Drag your card to the center of the table";
#endif
                if (is_play)
					m_table_view.SetOnboardText (play_sentence);
				else
					m_table_view.SetOnboardText ("Please wait while others place their bids");

				m_table_view.Onboard_text.SetActive (true);
			}
		}

        private void HideOnboardingMessage()
        {
            if (m_table_view.Onboard_text!=null)
            {
                m_table_view.Onboard_text.SetActive(false);
            }
        }

        private void OnBackFromBackground(bool sessionTimeout, TimeSpan backgroundTime)
        {
            Time.timeScale = 1; // We need to set this here since if a user goes to the background while we are on time scale,
                                // he will return with a time scale 
            if(m_is_game_active)
            {
                if (backgroundTime.TotalSeconds >= GAME_LOGIN_TIMEOUT_SEC)
                {
                    LeaveTable(false, true);
                    BackgroundController.Instance.ForceSessionTimeout();
                }
            }
        }


        #region Achievements

        public void UpdateAllMatchEndAchievements (int win_place, int coins_delta, bool arena_played)
		{
            return; // NOT USED FOR NOW

			if (win_place == 1)
			{
				if (m_precision_game)
					AchievementsManager.Instance.Set (AchievementsController.PrecisionGame, 1);

				if (m_was_negative)
					AchievementsManager.Instance.Set (AchievementsController.WinAfterNegative, 1);

				AchievementsManager.Instance.Set (AchievementsController.WinGame, 1);
				UpdateAchievementsConsGameWon (true);
			}
			else
			{
				UpdateAchievementsConsGameWon (false);
			}
			UpdateAchievementsGamesPlayedAchievement ();
			UpdateAchievementsCoinsWin (coins_delta);

			if (arena_played)
				AchievementsManager.Instance.Increment (AchievementsController.PlayChallange);

		}

		private void UpdateAchievementsConsGameWon (bool win)
		{
			if (win)
			{
				AchievementsManager.Instance.Increment (AchievementsController.Win3ConsecutiveGames);
				AchievementsManager.Instance.Increment (AchievementsController.Win5ConsecutiveGames);
			}
			else
			{
				AchievementsManager.Instance.Set (AchievementsController.Win3ConsecutiveGames, 0);
				AchievementsManager.Instance.Set (AchievementsController.Win5ConsecutiveGames, 0);
			}
		}

		private void UpdateAchievementsGamesPlayedAchievement ()
		{
			AchievementsManager.Instance.Increment (AchievementsController.PlayGames);

			if ((ModelManager.Instance.GetActiveMatch () as SpadesActiveMatch).GetMatch ().Playing_mode == PlayingMode.Partners)
				AchievementsManager.Instance.Increment (AchievementsController.PlayPartnerGames);
			else
				AchievementsManager.Instance.Increment (AchievementsController.PlaySoloGames);
		}

		private void UpdateAchievementsBlindGamesWonAchievement (bool blind)
		{
			if (blind)
				AchievementsManager.Instance.Increment (AchievementsController.BlindNilBid);
			else
				AchievementsManager.Instance.Increment (AchievementsController.NilBid);
		}

		public void UpdateAchievementsCoinsWin (int coins_delta)
		{
			if (coins_delta > 0)
				AchievementsManager.Instance.Increment (AchievementsController.EarnCoins, coins_delta);
		}

		#endregion


		/// <summary>
		/// Returns the current time according to the platform.
		/// </summary>     
		private float CurTime ()
		{
#if UNITY_WEBGL
            return Time.realtimeSinceStartup;
#else
            return Time.realtimeSinceStartup;
#endif
		}


		public SpadesEngine Spades_engine {
			get {
				return m_spades_engine;
			}
		}

		public CardView Selected_card_view {
			get {
				return m_selected_card_view;
			}
			set {
				m_selected_card_view = value;
			}
		}


		public bool Bidding_complete {
			get {
				return m_bidding_complete;
			}
		}

		public TableView Table_view {
			get {
				return m_table_view;
			}
		}

		/// <summary>
		/// AI adapter for local types to SpadesAI types
		/// </summary>
		public static class AIAdapter
		{

			static System.Random rand = new System.Random ();


			public static Position IndexToPosition (int index)
			{
				switch (index)
				{
				case 0:
					return Position.West;
				case 1:
					return Position.North;
				case 2:
					return Position.East;
				case 3:
					return Position.South;
				}
				return 0;
			}

			internal static int PositionToIndex (Position pos)
			{
				switch (pos)
				{
				case Position.West:
					return 0;
				case Position.North:
					return 1;
				case Position.East:
					return 2;
				case Position.South:
					return 3;
				}
				return 0;
			}
		}

        private void GenerateFakeDisconnectionParams()
        {
            //this flag is sat in FakeDisconnection() - so that only if this trick ever happened it will mark it True
            if (!m_game_had_fake_disconnection)
            {
                if (m_random.Next(0, 100) < BECOME_ZOMBIE_ODS_PER_ROUND*100)
                {
                    //this means this round will have a fake disconnection
                    m_fake_disconnect_trick_number = m_random.Next(1, 13);
                    
                }
                else
                    m_fake_disconnect_trick_number = -1;
            }
            
        }

        public void CheatingStarted ()
		{
			m_bidding_popup.GetComponent<BiddingPopup> ().CloseBiddingKeyboard ();

			StartCoroutine (ShowErrorPopupCoroutine (2));
		}

		private IEnumerator ShowErrorPopupCoroutine (float delay)
		{
			yield return new WaitForSeconds (delay);

			ManagerView.Instance.ShowErrorPopup = true;
		}


		public float Bid_delay {
			get {               
				return m_bid_delay;
			}
		}

		public MatchHistoryManager Match_logger {
			get {
				return m_match_logger;
			}
		}


		public Player Player {
			get {
				return m_player;
			}
			set {
				m_player = value;
			}
		}

		public bool Player_left {
			get {
				return m_player_left;
			}
		}

		public bool Is_game_active {
			get {
				return m_is_game_active;
			}
		}

		public bool Player_bid_or_played {
			get {
				return m_player_bid_or_played;
			}
		}

		public List<Param> Commands {
			get {
				return m_commands;
			}
			set {
				m_commands = value;
			}
		}

		public bool Play_recorded_commands {
			get {
				return m_play_recorded_commands;
			}
		}


		public GameObject Bidding_popup {
			get {
				return m_bidding_popup;
			}
		}

		public bool Card_thrown_processed {
			get {
				return m_card_thrown_processed;
			}
			set {
				m_card_thrown_processed = value;
			}
		}


		public Action<Action> Table_found_delegate {
			get {
				return m_table_found_delegate;
			}
			set {
				m_table_found_delegate = value;
			}
		}


		public int Cards_finished_throw_anim {
			get {
				return m_cards_finished_throw_anim;
			}
			set {
				m_cards_finished_throw_anim = value;
			}
		}

        public int C4pQuitPoints { get => m_c4pQuitPoints; }
		public int HiLoQuitPoints { get => m_hiloQuitPoints; }
	}
}
