using System.Collections;
using System;

namespace spades.controllers
{

    public class LobbyTopController : MonoBehaviour
    {

        public const float  BALANCE_UPDATE_TIME = 0.2f;
        public static LobbyTopController Instance;

        [SerializeField] GameObject m_top_panel = null;
        [SerializeField] GameObject m_coins_icon_object = null;
        [SerializeField] GameObject m_xp_icon_object = null;
        [SerializeField] GameObject m_coinbox_object = null;
      //  [SerializeField] GameObject m_full_screen_button_object = null;
        [SerializeField] GameObject m_indication_points_object = null;

        [SerializeField] ProgressView m_progress_view = null;
        [SerializeField] BalanceCoinTextbox m_coins_text = null;

        bool m_top_showing = true;
        private bool m_is_end_of_match;

        [SerializeField] GameObject m_single_buy_object = null;
        [SerializeField] GameObject m_dual_buy_object = null;
        [SerializeField] TMP_Text m_PO_buy_timer_text = null;
        [SerializeField] GameObject m_PO_timer_bg = null;

        [SerializeField] Image m_buy_button_overlay = null;
        [SerializeField] Image m_small_buy_button_overlay = null;
        [SerializeField] Image m_deal_button_overlay = null;

        [SerializeField] EndGameSpecialOfferButton m_special_offer = null;
        [SerializeField] EndGameBoostOfferButton m_boost_offer = null;

        [SerializeField] GameObject m_centerBgSingle = null;
        [SerializeField] GameObject m_centerBgDualBg = null;


        [SerializeField] Button m_avatarProfileBtn = null;
        [SerializeField] Button m_settingsBtn = null;

        [SerializeField] Transform m_buy_buttons;

        [SerializeField] RectTransform m_profile_bg;

        MESMaterial m_active_PO_material;
        IMSInteractionZone m_active_PO_iZone;


        bool m_PO_buy = false;

        IEnumerator m_timer_routine = null;

        [SerializeField]
        private MAvatarView m_avatarView = null;

        //Graphics overrides
        [SerializeField] Sprite m_BT_CN_single_default;
        [SerializeField] Sprite m_BT_CN_dual_default;

        [SerializeField] Sprite m_BT_CN_single_optional;
        [SerializeField] Sprite m_BT_CN_dual_optional;

        //

        void Awake()
        {

            Instance = this;

        }

        void Start()
        {
            CashierController.Instance.PurchaseCompleted += HandlePurchaseComplete;

            LobbyController.Instance.OnLogin += ResetPOMaterials;

        }

       

        private void HandlePurchaseComplete(string packageId,IMSGoodsList goodsList)
        {
            // On purchase, remove the sale banner
            // If the MES data will contain a new sale banner, it will reappear
            ShowPurchaseButtons();
        }

        public void ShowHideIndicationPoints(bool show)
        {
            m_indication_points_object.SetActive(show);

        }

        public void AdjustSizeForIpad()
        {
            m_top_panel.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 240f);

            m_top_panel.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(900f, 160f);
            m_top_panel.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(900f, 160f);
            m_centerBgSingle.GetComponent<RectTransform>().sizeDelta = new Vector2(806f,170f);
            m_centerBgDualBg.GetComponent<RectTransform>().sizeDelta = new Vector2(926f, 170f);

            m_buy_buttons.localPosition = new Vector2(0, -10f);

            m_indication_points_object.GetComponent<RectTransform>().localPosition = new Vector2(0, -160f);

            m_profile_bg.localScale = new Vector2(1.34f, 1.34f);
        }

        public void ShowTop(bool show, bool animate = false, bool show_dots = false, bool isEndOfMatch = false)
        {
            m_top_showing = show;
            m_is_end_of_match = isEndOfMatch;

            ShowHideIndicationPoints(show_dots);

            m_special_offer.ResetSpecialOffer();

            ShowPurchaseButtons();

            if (animate)
            {
                if (show)
                    iTween.MoveTo(m_top_panel, iTween.Hash("y", 0, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", .25, "oncomplete", "MoveOutDone", "Oncompletetarget", this.gameObject));
                else
                    iTween.MoveTo(m_top_panel, iTween.Hash("y", 200, "islocal", true, "easeType", iTween.EaseType.easeInCirc, "time", .25, "oncomplete", "MoveInDone", "Oncompletetarget", this.gameObject));
            }
            else
            {
                if (show)
                    m_top_panel.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                else
                    m_top_panel.GetComponent<RectTransform>().localPosition = new Vector3(0, 200f, 0);
            }

            if (animate)
            {
                if (show)
                    iTween.MoveTo(m_top_panel, iTween.Hash("y", 0, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", .25, "oncomplete", "MoveOutDone", "Oncompletetarget", this.gameObject));
                else
                    iTween.MoveTo(m_top_panel, iTween.Hash("y", 200, "islocal", true, "easeType", iTween.EaseType.easeInCirc, "time", .25, "oncomplete", "MoveInDone", "Oncompletetarget", this.gameObject));
            }
            else
            {
                if (show)
                    m_top_panel.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
                else
                    m_top_panel.GetComponent<RectTransform>().localPosition = new Vector3(0, 200f, 0);
            }

            m_avatarProfileBtn.interactable = !isEndOfMatch;
            m_settingsBtn.interactable = !isEndOfMatch;


            ManagerView.Instance.EnableDisableButton(m_avatarProfileBtn);
        }


        public void ShowPurchaseButtons()
        {

            // Hide everything
            m_boost_offer.gameObject.SetActive(false);
            m_special_offer.gameObject.SetActive(false);
            m_dual_buy_object.SetActive(false);
            m_centerBgDualBg.SetActive(false);
            m_single_buy_object.SetActive(false);
            m_centerBgSingle.SetActive(false);

            ActiveMatch aMatch = ModelManager.Instance.GetActiveMatch();

            bool hasActivePO = m_active_PO_iZone != null && !IMSController.Instance.IsIZoneExpired(m_active_PO_iZone);
            bool hasEndGameOffer = m_special_offer.HasSpecialOffer();
            bool needABoost = aMatch != null && aMatch.Match.Buy_in > ModelManager.Instance.GetUser().GetCoins();


            if (!m_is_end_of_match || (!hasEndGameOffer && !needABoost))
            {
                // Not end game or is end game but no special offer and no boost
                if (hasActivePO)
                {
                    // double buttons
                    m_centerBgDualBg.SetActive(true);
                    m_dual_buy_object.SetActive(true);
                }
                else
                {
                    // Single button
                    m_single_buy_object.SetActive(true);
                    m_centerBgSingle.SetActive(true);

                }
            }
            else 
            {
                // End game with special offer or boost
                if (hasEndGameOffer)
                    m_special_offer.gameObject.SetActive(true);
                else if (needABoost)
                {
                    //need a boost special case
                    m_boost_offer.Init(aMatch.Match.Buy_in - ModelManager.Instance.GetUser().GetCoins(), OnHideOffer);
                    m_boost_offer.gameObject.SetActive(true);
                }

            }

        }

        private void OnHideOffer()
        {
            m_boost_offer.gameObject.SetActive(false);
            m_special_offer.gameObject.SetActive(false);
            m_dual_buy_object.SetActive(false);
            m_centerBgDualBg.SetActive(false);
            m_single_buy_object.SetActive(true);

        }




        public void UpdateCoinsText(bool count = false)
        {
            if (m_coins_text != null)
            {

                if (!count)
                    m_coins_text.Amount = ModelManager.Instance.GetUser().GetCoins();
                else
                    StartCoroutine(UpdateTextAmountCount(ModelManager.Instance.GetUser().GetCoins(), BALANCE_UPDATE_TIME));
            }
        }

        public void SetAvatarModel(MAvatarModel avatar)
        {
            if (m_avatarView.MAvatarModel != null)
                m_avatarView.MAvatarModel.OnModelChanged -= UpdateNickname;

            m_avatarView.SetAvatarModel(avatar);

            avatar.OnModelChanged += UpdateNickname;
        }
        private void UpdateNickname(MAvatarModel avatar, bool fullChange)
        {
            m_progress_view.SetUserNameText(avatar.NickName);
        }

        IEnumerator UpdateTextAmountCount(int new_total, float time)
        {
            int num_updates = 10;
            int start_value = m_coins_text.Amount;
            int delta = (ModelManager.Instance.GetUser().GetCoins() - start_value)/ num_updates;
            float delta_time = BALANCE_UPDATE_TIME / num_updates;

            int counter = 0;

            while(counter< num_updates)
            {
                counter++;
                m_coins_text.Amount = start_value + (counter * delta);
                yield return new WaitForSecondsRealtime(delta_time);
            }

            m_coins_text.Amount = ModelManager.Instance.GetUser().GetCoins();

        }

        /// <summary>
        /// Callback from iTween
        /// </summary>
        public void MoveOutDone()
        {

        }

        /// <summary>
        /// Callback from iTween
        /// </summary>
        public void MoveInDone()
        {

        }

        public void BUTTON_Avatar_clicked()
        {
            if(SideGamesController.Instance.IsMiniGameOpen()==false)
                SpadesPopupManager.Instance.ShowProfilePopup();
        }

        public void NewPoReceived(MESMaterial material)
        {
            m_active_PO_material = material;

            ShowPurchaseButtons();

            RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, m_active_PO_material.source, m_active_PO_material.id, (Sprite sprite, Texture2D rawTexture, object userData) =>
            {
                if (sprite!=null) {
                    m_deal_button_overlay.sprite = sprite;
                    m_deal_button_overlay.color = Color.white;
                    m_deal_button_overlay.SetNativeSize();
                }
            },null);

            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (material.expired > 0)
            {
                m_timer_routine = POTimer((int)material.expired);
                StartCoroutine(m_timer_routine);
            }
            else 
            {
                m_PO_buy_timer_text.text = "No Time Limit!";
            }
                
            if (material.timerVisibility == 0) {
                m_PO_buy_timer_text.gameObject.SetActive(false);
                m_PO_timer_bg.SetActive(false);
            }

        }

        private void ResetPOMaterials() {
            m_active_PO_material = null;
            m_active_PO_iZone = null;

            m_buy_button_overlay.sprite = null;
            m_buy_button_overlay.color = new Color(1, 1, 1, 0);

            m_deal_button_overlay.sprite = null;
            m_deal_button_overlay.color = new Color(1, 1, 1, 0);

            m_small_buy_button_overlay.sprite = null;
            m_small_buy_button_overlay.color = new Color(1, 1, 1, 0);


            m_special_offer.ResetSpecialOffer();

            m_is_end_of_match = false;

            ShowPurchaseButtons();
        }


        IEnumerator POTimer(int expired)
        {

            while (true)
            {

                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);

                m_PO_buy_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            //switch back to single buy when timer expires
            ShowPurchaseButtons();
        }

        public void NewBannerReceived(MESMaterial mESMaterial)
        {
            ShowPurchaseButtons();

            if (mESMaterial.location == MESBase.MATERIAL_LOCATION_SMALL_BUY_BUTTON)
            {
                RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, mESMaterial.source, mESMaterial.id, (Sprite sprite, Texture2D rawTexture, object userData) =>
                {
                    if (sprite != null)
                    {
                        m_small_buy_button_overlay.sprite = sprite;
                        m_small_buy_button_overlay.color = Color.white;
                        m_small_buy_button_overlay.SetNativeSize();
                    }
                }, null);
			}


            if (mESMaterial.location == MESBase.MATERIAL_LOCATION_BUY_BUTTON)
            {
                RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, mESMaterial.source, mESMaterial.id, (Sprite sprite, Texture2D rawTexture, object userData) =>
                {
                    if (sprite!=null) {
                        m_buy_button_overlay.sprite = sprite;
                        m_buy_button_overlay.color = Color.white;
                        m_buy_button_overlay.SetNativeSize();
                    }
                }, null);
            }

        }

        /// <summary>
        /// Click event for right buy button
        /// </summary>
        public void BT_POBuyClicked()
        {
            if (m_active_PO_iZone!=null)
            {
                (IMSController.Instance as SpadesIMSController).ExecuteAction(m_active_PO_iZone);
            }
            else if (m_active_PO_material != null)
            {
                (MESBase.Instance as SpadesMESController).ExecuteAction(m_active_PO_material, true);
            }

        }

        public void SetIMSBanner(IMSInteractionZone iZone) {
            RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, iZone.Source, iZone.Id, (Sprite sprite, Texture2D rawTexture, object userData) =>
            {
                if (sprite != null)
                {
                    switch (iZone.Location)
                    {
                        case SpadesIMSController.BANNER_HEADER_BUY:
                            m_buy_button_overlay.sprite = sprite;
                            m_buy_button_overlay.color = Color.white;
                            m_buy_button_overlay.SetNativeSize();
                            break;
                        case SpadesIMSController.BANNER_HEADER_LEFT_BUTTON:
                            m_small_buy_button_overlay.sprite = sprite;
                            m_small_buy_button_overlay.color = Color.white;
                            m_small_buy_button_overlay.SetNativeSize();
                            break;
                        case SpadesIMSController.BANNER_HEADER_RIGHT_BUTTON:
                            m_deal_button_overlay.sprite = sprite;
                            m_deal_button_overlay.color = Color.white;
                            m_deal_button_overlay.SetNativeSize();

                            m_active_PO_iZone = iZone;

                            ManageIMSRightButtonTimer(iZone);

                            break;
                    }
                    ShowPurchaseButtons();


                }

                // End game can also be used without a sprite
                if (iZone.Location==SpadesIMSController.BANNER_HEADER_END_GAME_OFFER)
                {
                    m_special_offer.Init(iZone, sprite, OnHideOffer, OnHideOffer);
                    ShowPurchaseButtons();
                }
            }, null);
           
        }

        private void ManageIMSRightButtonTimer(IMSInteractionZone iZone)
        {
            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (iZone.ViewLimit == null)
            {
                // No view limit - no timer
                m_PO_buy_timer_text.gameObject.SetActive(false);
                m_PO_timer_bg.SetActive(false);
                return;
            }

            if (iZone.ViewLimit.Expires > 0)
            {
                m_timer_routine = POTimer((int)iZone.ViewLimit.Expires);
                StartCoroutine(m_timer_routine);
            }
            else
            {
                m_PO_buy_timer_text.text = "No Time Limit!";
            }


            if (!iZone.ViewLimit.IsVIsible)
            {
                m_PO_buy_timer_text.gameObject.SetActive(false);
                m_PO_timer_bg.SetActive(false);
            }
        }

        public GameObject Coins_box_icon
        {
            get
            {
                return m_coins_icon_object;
            }
        }

        public BalanceCoinTextbox Coins_text
        {
            get
            {
                return m_coins_text;
            }
        }

        public bool Top_showing
        {
            get
            {
                return m_top_showing;
            }
        }

        public ProgressView Progress_view
        {
            get
            {
                return m_progress_view;
            }
        }

        public GameObject Xp_icon_object
        {
            get
            {
                return m_xp_icon_object;
            }
        }

        public GameObject Top_panel
        {
            get
            {
                return m_top_panel;
            }
        }

		public MAvatarView AvatarView
        {
            get
            {
                return m_avatarView;
            }
            set
            {
                m_avatarView = value;
            }
        }




        public bool PO_buy
        {
            get
            {
                return m_PO_buy;
            }
            set
            {
                m_PO_buy = value;
            }
        }

        public GameObject Coinbox_object
        {
            get
            {
                return m_coinbox_object;
            }
        }
    }

}
