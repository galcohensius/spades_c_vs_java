﻿using spades.models;
using System.Collections.Generic;

namespace spades.controllers
{
    public class SpadesContestsController : ContestsController
    {
        #region Static Members
        public static SpadesContestsController Instance;
        #endregion

        public override void Awake()
        {
            base.Awake();
            Instance = this;
        }
        // Should be in SpadesContestsController
        /// <summary>
        /// Finds all contests for a range of matches. Doesn't return calculating results contests
        /// </summary>
        public virtual List<Contest> GetOnGoingContestsInRange(int min, int max/*, PlayingMode playingMode*/)
        {
            List<Contest> temp = new List<Contest>();

            ContestsList contestsList = ModelManager.Instance.ContestsLists;

            foreach (Contest contest in contestsList.OngoingContests)
            {
                if ((contest.TableFilterData.MinBuyIn >= min || contest.TableFilterData.MaxBuyIn <= max) && !contest.Is_calculating_results)
                    temp.Add(contest);
            }

            return temp;
        }

        /// <summary>
        /// Returns a contest for a match or null
        /// </summary>
        public Contest GetContestForMatch(SpadesSingleMatch match)
        {
            List<Contest> onGoingContests = ModelManager.Instance.ContestsLists.OngoingContests;
            foreach (Contest contest in onGoingContests)
            {
                if (contest.TableFilterData.MinBuyIn <= match.Buy_in &&
                    contest.TableFilterData.MaxBuyIn >= match.Buy_in &&
                    (contest.TableFilterData as SpadesTableFilterData).IsSubVariantContained(match.SubVariant) &&
                    (contest.TableFilterData as SpadesTableFilterData).IsVariantContained(match.Variant))
                {
                    if ((contest.TableFilterData as SpadesTableFilterData).PlayingMode == PlayingMode.Both ||
                        (contest.TableFilterData as SpadesTableFilterData).PlayingMode == match.Playing_mode)
                    {
                        if (contest.TableFilterData.SpecialOnly)
                        {
                            if (contest.TableFilterData.SpecialOnly == match.Is_special)
                                return contest;
                            return null;
                        }
                        else
                            return contest;

                    }
                }
            }

            return null;
        }
        /// <summary>
        /// Returns a joint contest for all matches or null
        /// </summary>
        public Contest GetContestForMatchList(List<SpadesSingleMatch> matches)
        {
            Contest lastContest = null;
            foreach (SpadesSingleMatch match in matches)
            {
                Contest contest = GetContestForMatch(match);
                if (contest == null)
                    return null;

                if (lastContest == null)
                {
                    lastContest = contest;
                    continue;
                }

                if (lastContest != contest)
                    return null;
            }
            return lastContest;
        }

        public string FormatAvailable(Contest spadesContest)
        {
            // * game mode *
            SpadesTableFilterData stfd = (spadesContest.TableFilterData as SpadesTableFilterData);
            string playingMode = stfd.PlayingMode.ToString();

            if (stfd.PlayingMode == spades.models.PlayingMode.Both)
                playingMode = "Both Partners & Solo";

            return "<b>Available at " + playingMode + " tables</b>";
        }
    }
}
