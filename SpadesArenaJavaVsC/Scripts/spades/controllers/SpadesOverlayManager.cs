using spades.models;
using System.Collections.Generic;
using System;

namespace spades.controllers
{

    public class SpadesOverlayManager : OverlaysManager
    {
        [SerializeField] GameObject m_collectMission_overlay = null;

        [SerializeField] GameObject m_level_up_overlay = null;
        [SerializeField] GameObject m_welcome_overlay = null;


        //lose screens
        [SerializeField] GameObject m_lose_single = null;
        [SerializeField] GameObject m_lose_arena = null;

        //win screens
        [SerializeField] GameObject m_win_single = null;
        [SerializeField] GameObject m_win_single_second_place_overlay = null;

        [SerializeField] GameObject m_win_arena_stage = null;

        [SerializeField] GameObject m_win_arena_stage_second_place = null;




        [SerializeField] GameObject m_gem_unlocked_overlay = null;

        [SerializeField] GameObject m_table_unlocked_overlay = null;
        [SerializeField] GameObject m_arena_unlocked_overlay = null;
        [SerializeField] GameObject m_room_unlocked_overlay = null;
        [SerializeField] GameObject m_contest_unlocked_overlay = null;

        [SerializeField] GameObject m_missions_unlocked_overlay = null;

        [SerializeField] GameObject m_onboarding_overlay2 = null;
        [SerializeField] GameObject m_fast_forward_overlay = null;

        [SerializeField] GameObject m_unlock_leaderboards_overlay = null;

        [SerializeField] GameObject m_arena_win_overlay = null;

        public new static SpadesOverlayManager Instance;

        protected override void Awake()
        {
            base.Awake();
            Instance = this;
        }

        public override void FixCoinBoxDepth(bool show)
        {
            CardGamesPopupManager.Instance.SetCoinBoxOnTop(show);
        }

        public TableUnlockOverlay ShowTableUnlockedOverlay(OverlayClosedDelegate overlayClosedDelegate)
        {
            GameObject overlay = Instantiate(m_table_unlocked_overlay);
            ShowOverlay(overlay, false, overlayClosedDelegate);

            return overlay.GetComponent<TableUnlockOverlay>();
        }

        public RoomUnlockedOverlay ShowRoomUnlockedOverlay(OverlayClosedDelegate overlayClosedDelegate)
        {
            GameObject overlay = Instantiate(m_room_unlocked_overlay);
            ShowOverlay(overlay, false, overlayClosedDelegate);
            return overlay.GetComponent<RoomUnlockedOverlay>();
        }

        public ContestsUnlockOverlay ShowContestsUnlockedOverlay(OverlayClosedDelegate overlayClosedDelegate)
        {
            GameObject overlay = Instantiate(m_contest_unlocked_overlay);
            ShowOverlay(overlay, false, overlayClosedDelegate);
            return overlay.GetComponent<ContestsUnlockOverlay>();
        }


        public void ShowMissionUnlockedOverlay(OverlayClosedDelegate unlock_anim_done)
        {
            GameObject overlay = Instantiate(m_missions_unlocked_overlay);
            ShowOverlay(overlay, false, unlock_anim_done);

        }


        public ArenaUnlockOverlay ShowArenaUnlockedOverlay(OverlayClosedDelegate overlayClosedDelegate)
        {
            GameObject overlay = Instantiate(m_arena_unlocked_overlay);
            ShowOverlay(overlay, false, overlayClosedDelegate);

            return overlay.GetComponent<ArenaUnlockOverlay>();
        }

        public MissionCollectOverlay ShowMissionCollectOverlay(List<SpadesMissionController.ClaimMissionData> to_claim_missions)
        {
            GameObject overlay = Instantiate(m_collectMission_overlay);

            ShowOverlay(overlay, false, null);

            return overlay.GetComponent<MissionCollectOverlay>();
        }

        public void ShowUnlockLeaderboardsOverlay(OverlayClosedDelegate close_delegate = null)
        {
            GameObject overlay = Instantiate(m_unlock_leaderboards_overlay);
            ShowOverlay(overlay, false, close_delegate);
            CardGamesSoundsController.Instance.LeaderboardUnlocked();
        }



        public WinSingleOverlay ShowWinSingleOverlay(OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_win_single);
            ShowOverlay(overlay, true, close_delegate);

            return overlay.GetComponent<WinSingleOverlay>();

        }

        public FastForwardOverlay ShowFastForwardOverlay(OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_fast_forward_overlay);
            ShowOverlay(overlay, true, close_delegate);

            return overlay.GetComponent<FastForwardOverlay>();

        }

        public NewArenaWinOverlay ShowmArenaWinOverlay(OverlayClosedDelegate close_delegate = null)
        {
            GameObject overlay = Instantiate(m_arena_win_overlay);
            ShowOverlay(overlay, true, close_delegate);
            CardGamesSoundsController.Instance.ArenaWin();
            return overlay.GetComponent<NewArenaWinOverlay>();
        }


        public WinSingle2ndPlaceOverlay ShowWin2ndPlaceOverlay(OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_win_single_second_place_overlay);
            ShowOverlay(overlay, true, close_delegate);

            return overlay.GetComponent<WinSingle2ndPlaceOverlay>();

        }

        public OnBoardingOverlay ShowOnboardingOverlay(OverlayClosedDelegate close_delegate = null, Action beforeLoginAction = null)
        {
            GameObject overlay = Instantiate(m_onboarding_overlay);

            // Change the sorting order to hide the top bar
            m_overlay_canvas = GetComponent<Canvas>();
            int oldSortingOrder = m_overlay_canvas.sortingOrder;
            m_overlay_canvas.sortingOrder = 45;

            ShowOverlay(overlay, true, () =>
                  {
                      m_overlay_canvas.sortingOrder = oldSortingOrder;
                  });

            //an action used for TypeB onBoarding - need to clear the game and show lobby before we do the Trigger login in the MES
            overlay.GetComponent<OnBoardingOverlay>().BeforeLoginAction = beforeLoginAction;

            return overlay.GetComponent<OnBoardingOverlay>();
        }

        public OnBoardingOverlay2 ShowOnboardingOverlay2()
        {
            GameObject overlay = Instantiate(m_onboarding_overlay2);

            ShowOverlay(overlay, true);

            return overlay.GetComponent<OnBoardingOverlay2>();
        }



        public GemUnlockedOverlay ShowGemOverlay(SpadesArena arena, OverlayClosedDelegate close_delegate = null)
        {
            //int arena_index,int coins_won, bool isBaseUpdated,
            GameObject overlay = Instantiate(m_gem_unlocked_overlay);
            ShowOverlay(overlay, true, close_delegate);
            CardGamesSoundsController.Instance.GemWin();
            overlay.GetComponent<GemUnlockedOverlay>().SetLevel(arena, close_delegate);

            return overlay.GetComponent<GemUnlockedOverlay>();
        }


        public LoseSingleOverlay ShowLoseSingleOverlay(OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_lose_single);
            ShowOverlay(overlay, false, close_delegate);

            return overlay.GetComponent<LoseSingleOverlay>();

        }


        public WinArenaStageOverlay ShowWinArenaStageOverlay(OverlayClosedDelegate close_delegate = null)
        {
            GameObject overlay = Instantiate(m_win_arena_stage);
            ShowOverlay(overlay, true, close_delegate);

            return overlay.GetComponent<WinArenaStageOverlay>();
        }

        public WinArenaStage2ndPlaceOverlay ShowWinArena2ndPlaceStageOverlay(OverlayClosedDelegate close_delegate = null)
        {
            GameObject overlay = Instantiate(m_win_arena_stage_second_place);
            ShowOverlay(overlay, true, close_delegate);

            return overlay.GetComponent<WinArenaStage2ndPlaceOverlay>();
        }

        public LoseArenaOverlay ShowLoseArenaOverlay(OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_lose_arena);
            ShowOverlay(overlay, false, close_delegate);

            return overlay.GetComponent<LoseArenaOverlay>();

        }

        public LevelUpOverlay ShowLevelUpOverlay(int level, OverlayClosedDelegate close_delegate = null)
        {

            GameObject overlay = Instantiate(m_level_up_overlay);
            ShowOverlay(overlay, true, close_delegate);
            CardGamesSoundsController.Instance.LevelUp();
            LevelUpOverlay level_up_overlay = overlay.GetComponent<LevelUpOverlay>();

            level_up_overlay.SetLevel(level);
            return level_up_overlay;

        }


        public WelcomeOverlay ShowWelcomeOverlay(OverlayClosedDelegate close_delegate = null)
        {

            LobbyController.Instance.Lobby_view.GetSwipe().Enable_swipe = false;
            GameObject overlay = Instantiate(m_welcome_overlay);
            ShowOverlay(overlay, false, close_delegate);

            WelcomeOverlay welcome = overlay.GetComponent<WelcomeOverlay>();


            return welcome;

        }

        /// <summary>
        /// Shows a standard (boring) coins comet
        /// </summary>
        public override void FlyStandardCoinsComet(GameObject startPoint, int amount)
        {
            CometOverlay coins_comet = OverlaysManager.Instance.ShowCometOverlay();
            coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, 5, startPoint,
                    CardGamesPopupManager.Instance.Lobby_flight_target, 0f, 0.15f, .75f,
                    CardGamesPopupManager.Instance.Lobby_coins_balance, amount,
                    CardGamesPopupManager.Instance.Lobby_coins_animator, 2, 0.2f, 0.3f,
                    null, new Vector2(0.6f, 0.3f));
        }
    }
}
