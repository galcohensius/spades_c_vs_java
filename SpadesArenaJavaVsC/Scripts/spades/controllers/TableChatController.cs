using spades.models.tablechat;
using spades.models;
using System;
using System.Collections.Generic;

namespace spades.controllers
{
    public class TableChatController : MonoBehaviour
    {
        public static TableChatController Instance;

        [SerializeField] ChatView m_tableChatView = null;
        [SerializeField] TableChatModel m_soloChatModel = null;
        [SerializeField] TableChatModel m_partnersChatModel = null;
        [SerializeField] MAvatarView m_newSelfAvatarView = null;

        [SerializeField]
        private List<MAvatarView.AvatarAnimationStateTrigger> m_animationTriggers;

        private ISpadesMatch m_curr_match;

        public TableChatModel SoloChatModel
        {
            get
            {
                return m_soloChatModel;
            }

            
        }
        
        public void CloseEmojiMenu()
        {
            TableChatController.Instance.HideChatPanel();
           // m_emojiChatMenu.ToggleEmojiRadial(false);
        }

        public void SendAvatatChat(int chatID, string text)
        {
            SpadesGameServerManager.Instance.Send(CardGamesCommandsBuilder.Instance.BuildChatCommand(/*RoundController.Instance.Game_id*/"0", "T" + chatID, text));
        }

        public void PlayChatText(int id)
        {
            //TODO: implement this chat :
            if (m_curr_match.Playing_mode == PlayingMode.Solo)
                m_newSelfAvatarView.ShowChat(m_soloChatModel.FindMessageById(id), 44);
            // m_newSelfAvatarView.ChatSentenceClicked(m_soloChatModel.FindMessageById(id));
            else
                m_newSelfAvatarView.ShowChat(m_partnersChatModel.FindMessageById(id), 44);
            // m_newSelfAvatarView.ChatSentenceClicked(m_partnersChatModel.FindMessageById(id));
        }
        public TableChatModel PartnersChatModel
        {
            get {return m_partnersChatModel;
            }
        }

        public void ShowChatPanel()
        {
            if (!m_tableChatView.isActiveAndEnabled)
                m_tableChatView.gameObject.SetActive(true);

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            m_curr_match = active_match.GetMatch();

            if (m_curr_match.Playing_mode == PlayingMode.Solo)
            {
                LoadFavorites(PlayingMode.Solo); //load favorites to model
                m_tableChatView.Show(m_soloChatModel);
            }
            else
            {
                LoadFavorites(PlayingMode.Partners);
                m_tableChatView.Show(m_partnersChatModel);
            }
        }

        public void HideChatPanel()
        {
            if (m_curr_match!=null)
                SaveFavorites(m_curr_match.Playing_mode);

            m_tableChatView.Hide();
        }

        public void SendText(int id)
        {
            if (m_curr_match.Playing_mode == PlayingMode.Solo)
            {
                // new
                //  m_newSelfAvatarView.ChatSentenceClicked(m_soloChatModel.FindMessageById(id));
                int triggerID = m_soloChatModel.GetResponseTriggerForUserChat(id, PlayingMode.Solo);
                TriggerAIChat(triggerID, PlayingMode.Solo);
            }
            else
            {
                // new
                // m_newSelfAvatarView.ChatSentenceClicked(m_partnersChatModel.FindMessageById(id));
                int triggerID = m_partnersChatModel.GetResponseTriggerForUserChat(id, PlayingMode.Partners);
                TriggerAIChat(triggerID, PlayingMode.Partners);
            }

            FacebookController.Instance.LogAppEvent("Avatar text chat " + id, new Dictionary<string, object>(), 0);
        }

        private void TriggerAIChat(int triggerID, PlayingMode mode)
        {
            if (triggerID != -1)
            {
                ChatAIController.Instance.TriggerAIChatEvent(triggerID, mode);
            }
            else
            {
                LoggerController.Instance.Log("TableChatController - TriggerAIChat() - Response Tigger Not Found , No Response Initiated");
            }
        }

        /// <summary>
        /// PROBABLY NOT USED - NEED TO CHECK (20/1/19)
        /// </summary>
        public void SendEmoji(int id)
        {
            // new
            //m_newSelfAvatarView.ChatIconClicked(id);


        }

        public void UpdateUserAvatarData()
        {
            if (m_newSelfAvatarView != null && m_newSelfAvatarView.gameObject.activeInHierarchy)
            {
                m_newSelfAvatarView.SetAvatarModel(ModelManager.Instance.GetUser().GetMAvatar());
            }
        }

        public void PlayNewAvatarAnimation(MAvatarView.AvatarAnimationStateTrigger animTrigger)
        {
            m_newSelfAvatarView.PlayAnimation(animTrigger);
            // new
            //int triggerID = m_newSelfAvatarView.GetResponseTriggerForUserAnimation((int)animTrigger);
            int triggerID = 0; // temp;
            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            m_curr_match = active_match.GetMatch();

            if (m_curr_match.Playing_mode == PlayingMode.Solo)
            {
                TriggerAIChat(triggerID, PlayingMode.Solo);
            }
            else
            {
                TriggerAIChat(triggerID, PlayingMode.Partners);
            }

            FacebookController.Instance.LogAppEvent("Avatar emoji chat " + animTrigger, new Dictionary<string, object>(), 0);
        }

        private void LoadFavorites(PlayingMode playingMode)
        {
            string textFavorites = "";
            string[] tokens;
            int[] convertedItems;

            if (playingMode == PlayingMode.Solo && SpadesStateController.Instance.GetTableChatTextFavorites("solo") != "")
            {
                textFavorites = SpadesStateController.Instance.GetTableChatTextFavorites("solo");
                tokens = textFavorites.Split(',');
                convertedItems = Array.ConvertAll<string, int>(tokens, int.Parse);
                m_soloChatModel.ChatTextMessageFavorites = convertedItems;
            }
            else if (playingMode == PlayingMode.Solo)
            {
                //no favirites stored
                m_soloChatModel.ChatTextMessageFavorites = new int[] { 1, 2, 3, 4 }; //Load defaults
            }

            if (playingMode == PlayingMode.Solo && SpadesStateController.Instance.GetTableChatEmojiFavorites("solo") != "")
            {
                textFavorites = SpadesStateController.Instance.GetTableChatEmojiFavorites("solo");
                tokens = textFavorites.Split(',');
                convertedItems = Array.ConvertAll<string, int>(tokens, int.Parse);
                m_soloChatModel.ChatEmojiMessageFavorites = convertedItems;
            }
            else if (playingMode == PlayingMode.Solo)
            {
                //no favirites stored
                m_soloChatModel.ChatEmojiMessageFavorites = new int[] { 1, 2, 3, 4,5 }; //Load defaults
            }


            if (playingMode == PlayingMode.Partners && SpadesStateController.Instance.GetTableChatTextFavorites("partners") != "")
            {
                textFavorites = SpadesStateController.Instance.GetTableChatTextFavorites("partners");
                tokens = textFavorites.Split(',');
                convertedItems = Array.ConvertAll<string, int>(tokens, int.Parse);
                m_partnersChatModel.ChatTextMessageFavorites = convertedItems;
            }
            else if (playingMode == PlayingMode.Partners)
            {
                //no favirites stored
                m_partnersChatModel.ChatTextMessageFavorites = new int[] { 1, 2, 3, 4 }; //Load defaults
            }

            if (playingMode == PlayingMode.Partners && SpadesStateController.Instance.GetTableChatEmojiFavorites("partners") != "")
            {
                textFavorites = SpadesStateController.Instance.GetTableChatEmojiFavorites("partners");
                tokens = textFavorites.Split(',');
                convertedItems = Array.ConvertAll<string, int>(tokens, int.Parse);
                m_partnersChatModel.ChatEmojiMessageFavorites = convertedItems;
            }
            else if (playingMode == PlayingMode.Partners)
            {
                //no favirites stored
                m_partnersChatModel.ChatEmojiMessageFavorites = new int[] { 1, 2, 3, 4,5 }; //Load defaults
            }
        }

        public void AddEmojiToFavorites(int id)
        {
            TableChatModel model;

            if (m_curr_match.Playing_mode == PlayingMode.Solo)
            {
                model = m_soloChatModel;
            }
            else
            {
                model = m_partnersChatModel;
            }

            //check if already in favorites
            bool isInFavorites = false;
            for (int i = 0; i < model.ChatEmojiMessageFavorites.Length; i++)
            {
                if (model.ChatEmojiMessageFavorites[i] == id)
                {
                    isInFavorites = true;
                    break;
                }
            }
            if (!isInFavorites)
            {
                //shift array right and replace first
                for (int j = model.ChatEmojiMessageFavorites.Length - 1; j > 0; j--)
                {
                    model.ChatEmojiMessageFavorites[j] = model.ChatEmojiMessageFavorites[j - 1];
                }

                model.ChatEmojiMessageFavorites[0] = id;
            }
        }

        public void AddTextToFavorites(int id)
        {
            TableChatModel model;

            if (m_curr_match.Playing_mode == PlayingMode.Solo)
            {
                model = m_soloChatModel;
            }
            else
            {
                model = m_partnersChatModel;
            }
            //check if already in favorites
            bool isInFavorites = false;
            for (int i=0;i<model.ChatTextMessageFavorites.Length;i++)
            {
                if (model.ChatTextMessageFavorites[i]==id)
                {
                    isInFavorites = true;
                    break;
                }
            }

            if (!isInFavorites)
            {
                //shift array right and replace first

                for (int j = model.ChatTextMessageFavorites.Length - 1; j > 0; j--)
                {
                    model.ChatTextMessageFavorites[j] = model.ChatTextMessageFavorites[j - 1];
                }

                model.ChatTextMessageFavorites[0] = id;
            }

            m_tableChatView.UpdateTextFavorites(model);
        }

        private void SaveFavorites(PlayingMode playingMode)
        {
            string[] convertedItems;
            if (playingMode == PlayingMode.Solo)
            {
                convertedItems = Array.ConvertAll<int, string>(m_soloChatModel.ChatTextMessageFavorites, x => x.ToString());
                SpadesStateController.Instance.SetTableChatTextFavorites("solo", String.Join(",", convertedItems));
            }
            else
            {
                convertedItems = Array.ConvertAll<int, string>(m_partnersChatModel.ChatTextMessageFavorites, x => x.ToString());
                SpadesStateController.Instance.SetTableChatTextFavorites("partners", String.Join(",", convertedItems));
            }

        }

        public void ToggleSingleMenu(bool isTargetEmojiMenu, bool shouldOpenMenu)
        {
          /*  if (isTargetEmojiMenu)
            {
                if (shouldOpenMenu)
                {
                    if (m_tableChatView.IsExpanded)
                    {
                        m_tableChatView.ToggleTextChatMenuExpanded(false);
                    }

                    m_emojiChatMenu.gameObject.SetActive(true);
                }
                else
                {
                    if (m_emojiChatMenu.IsRadialCurrentlyOpen)
                    {
                        m_emojiChatMenu.Reset();
                    }
                }
            }
            else
            {
                if (shouldOpenMenu)
                {
                    ShowChatPanel();
                }
                else
                {
                    if (m_tableChatView.IsExpanded)
                    {
                        m_tableChatView.ToggleTextChatMenuExpanded(false);
                    }
                }
            }*/
        }

        private void Awake()
        {
            Instance = this;
        }

        public void ToggleChatMenuLock(bool shouldLockMenu)
        {
            m_tableChatView.ToggleChatMenuLock(shouldLockMenu);
        }
    }
}
