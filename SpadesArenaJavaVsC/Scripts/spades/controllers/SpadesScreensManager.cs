using spades.models;

namespace spades.controllers
{

	public class SpadesScreensManager : ScreensManager
	{
        [SerializeField] GameObject m_arena_view_object = null;
        [SerializeField] GameObject m_leaderboards_object = null;

        public static SpadesScreensManager Instance;

        void Awake()
        {
            Instance = this;
            m_arena_view_object.SetActive (false);
        }

      
        public LeaderboardsScreen ShowLeaderboards(ScreenClosedDelegate close_delegate=null)
        {

            LobbyController.Instance.ShowLobby(false);
            LobbyTopController.Instance.ShowTop(false);

            LeaderboardsScreen leaderboardsScreen = m_leaderboards_object.GetComponent<LeaderboardsScreen> ();
            ShowScreen (m_leaderboards_object,()=>{
                if (close_delegate != null)
                    close_delegate();
                LobbyController.Instance.ShowLobby(true);
            });

            return leaderboardsScreen;

        }


		public ArenaScreen ShowArenaLobby(SpadesArena arena,ArenaScreen.BackFromReason back_from_win, ScreenClosedDelegate close_delegate=null)
		{
			ArenaScreen arena_screen = m_arena_view_object.GetComponent<ArenaScreen> ();

			ShowScreen (m_arena_view_object,()=>{	if (close_delegate != null) 
				close_delegate();});

			arena_screen.SetArena (arena,back_from_win);
			return arena_screen;
		}

 		public bool IsLeaderboardShown {
            get {
                LeaderboardsScreen leaderboardsScreen = m_leaderboards_object.GetComponent<LeaderboardsScreen>();
                return leaderboardsScreen.gameObject.activeSelf;
            }
        }

        public bool IsArenaLobbyShown
        {
            get
            {
                ArenaScreen arena_screen = m_arena_view_object.GetComponent<ArenaScreen>();
                return arena_screen.gameObject.activeSelf;
            }
        }

	}

}
