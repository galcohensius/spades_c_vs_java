using System;
using System.Collections.Generic;
using System.Collections;

namespace spades.controllers
{
    public class LeaderboardsController : MonoBehaviour
    {
        const long UPDATE_FRIENDS_DELAY = 8640000000;
        const char USER_DETAILS_DELIMITER = '|';

        private const int IGNORE_BOTS_TRESHOLD = 6;

        private string mUserDetails;

        LeaderboardsScreen m_lb_screen;
        private bool isRealUserUpdate;
        private int m_lastUserEarnings;
        private LeaderboardsModel.LeaderboardType ldbrdType;
        [SerializeField] GameObject m_leaderboardItemRow = null;

        private int lastAssignedBotId;

        public static LeaderboardsController Instance;

        private void Awake()
        {
            Instance = this;
        }


        //show leaderboard view
        public void ShowLeaderboard(LeaderboardsModel.LeaderboardType leaderboardType)
        {
            // Since version 1.23 the daily leaderboard is removed
            if (leaderboardType == LeaderboardsModel.LeaderboardType.GlobalCurrent || leaderboardType == LeaderboardsModel.LeaderboardType.GlobalPrevious)
                leaderboardType = LeaderboardsModel.LeaderboardType.Friends;

            ldbrdType = leaderboardType;
            if (m_lb_screen == null)
            {
                // Always start with current week
                m_lb_screen = SpadesScreensManager.Instance.ShowLeaderboards(() =>
                {
                    m_lb_screen.SwitchTab(leaderboardType);
                    LeaderboardServerManager.Instance.Disconnect();
                });
                m_lb_screen.SwitchTab(leaderboardType);
            }
            else
            {
                m_lb_screen.Alerts.hideAll();
                m_lb_screen.SwitchTab(leaderboardType);
            }

            m_lb_screen.TurnOnWaiting();

            //check if friends need to be updated
            string fb_id;
            bool IsFriendsNeedUpdate = (DateTime.UtcNow.Ticks - SpadesStateController.Instance.LoadLeaderboardFriendsUpdatedTime()) > UPDATE_FRIENDS_DELAY;

            if (FacebookController.Instance.UserFacebookData != null)
                fb_id = FacebookController.Instance.UserFacebookData.User_id;
            else
                fb_id = "0";

            if (leaderboardType == LeaderboardsModel.LeaderboardType.Friends && IsFriendsNeedUpdate && fb_id != "0")
            {
                FacebookController.Instance.GetUserAppFriends(OnGetUserAppFriendsRecieved);
            }
            else
            {
                //get leaderboard
                GetLeaderboard(leaderboardType);
            }
        }

        private void OnGetUserAppFriendsRecieved(List<FacebookData> friends)
        {
            //update leaderboard server with new friends list
            LeaderboardServerManager.Instance.Send(SpadesCommandsBuilder.Instance.BuildUpdateUserFriendsForLeaderboard(FacebookController.Instance.UserFacebookData.User_id, friends));
            SpadesStateController.Instance.SaveLeaderboardFriendsNum(friends.Count);
        }

        public void OnUpdateUserFriendsCompleted(bool success)
        {
            if (success)
            {
                GetLeaderboard(LeaderboardsModel.LeaderboardType.Friends);
            }
            else
            {
                LoggerController.Instance.Log("Error update user friends list");
            }
        }

        public void HideScreen()
        {
            if (m_lb_screen != null)
            {
                SpadesScreensManager.Instance.HideScreen();
                m_lb_screen = null;

            }
        }


        public void GetLeaderboard(LeaderboardsModel.LeaderboardType leaderboardType)
        {
            LoggerController.Instance.Log("GetLeaderboard Type: " + leaderboardType);

            if (FacebookController.Instance.UserFacebookData == null && leaderboardType == LeaderboardsModel.LeaderboardType.Friends)
            {
                m_lb_screen.TurnOffWaiting();
                m_lb_screen.ShowItemsHeader(false);
                m_lb_screen.Alerts.ShowTextMessage(LeaderboardAlerts.LeaderboardAlertType.NO_FACEBOOK_ACCOUNT);
                return;
            }

            if (FacebookController.Instance.UserFacebookData != null && leaderboardType == LeaderboardsModel.LeaderboardType.Friends && SpadesStateController.Instance.LoadLeaderboardFriendsNum() == 0)
            {
                m_lb_screen.TurnOffWaiting();
                m_lb_screen.ShowItemsHeader(false);
                m_lb_screen.Alerts.ShowTextMessage(LeaderboardAlerts.LeaderboardAlertType.NO_FACEBOOK_FRIENDS);
                return;
            }

            m_lb_screen.ShowItemsHeader(true);

            //for friend and hof no need to get metadata
            if (leaderboardType == LeaderboardsModel.LeaderboardType.Friends || leaderboardType == LeaderboardsModel.LeaderboardType.HallOfFame)
            {
                OnGetLeaderboardFromBOCompleted(leaderboardType);
                return;
            }

            // for current or previous check for leaderboard metadata cache
            LeaderboardModel ldr = ModelManager.Instance.Leaderboards.GetLeaderboard(leaderboardType);

            if (ldr.LeaderboardMetaData == null || (ldr.LeaderboardMetaData.TimeToEnd - DateUtils.UnixTimestamp() <= 0 && leaderboardType == LeaderboardsModel.LeaderboardType.GlobalCurrent))
            {
                //get from bo server
                ModelManager.Instance.Leaderboards.ClearCache(true);

                CardGamesCommManager.Instance.GetLeaderboards((success) =>
                {
                    if (success)
                        OnGetLeaderboardFromBOCompleted(leaderboardType);
                });
            }
            else
            {
                //get from cache
                OnGetLeaderboardFromBOCompleted(leaderboardType);
            }
        }

        private void OnGetLeaderboardFromBOCompleted(LeaderboardsModel.LeaderboardType leaderboardType)
        {
            LoggerController.Instance.Log("OnGetLeaderboardFromBOCompleted Type: " + leaderboardType);
            string fb_id;
            LeaderboardModel ldr = ModelManager.Instance.Leaderboards.GetLeaderboard(leaderboardType);

            //check if items cached
            if (ldr.Items == null)
            {

                //get facebook user id
                if (FacebookController.Instance.UserFacebookData != null)
                    fb_id = FacebookController.Instance.UserFacebookData.User_id;
                else
                {
                    fb_id = "0";
                }

                //get items from leaderboard server
                LoggerController.Instance.Log("get items from leaderboard server Type: " + leaderboardType);
                LeaderboardServerManager.Instance.Send(SpadesCommandsBuilder.Instance.BuildGetLeaderboard(leaderboardType,
                    ldr.LeaderboardMetaData == null ? -1 : ldr.LeaderboardMetaData.Id,
                    ModelManager.Instance.GetUser().GetID().ToString(),
                    fb_id, "1.1.1.1"));

            }
            else
            {
                //get items from leaderboard cache
                LoggerController.Instance.Log("get items from leaderboard cache Type: " + leaderboardType);
                OnLeaderboardReceived(leaderboardType);
            }
        }

        public void OnLeaderboardReceived(LeaderboardsModel.LeaderboardType leaderboardType)
        {
            LoggerController.Instance.Log("OnLeaderboardReceived Type: " + leaderboardType);
            LeaderboardModel ldr = ModelManager.Instance.Leaderboards.GetLeaderboard(leaderboardType);

            // If the total number of items is low, do not show the bots (negative user ids)
            if (ldr.Items != null && ldr.Items.Count <= IGNORE_BOTS_TRESHOLD)
            {
                RemoveBotsFromLeaderboard(ldr);
            }

            if (m_lb_screen != null)
            {

                LeaderboardsTopPanel topPanel = m_lb_screen.GetTopPanel();

                if (topPanel.GetCurrentTab() == leaderboardType)
                {
                    m_lb_screen.ShowItems(leaderboardType);

                    m_lb_screen.UpdateRanksView(leaderboardType);

                    if (!IsPrevLeaderboardExists())
                        m_lb_screen.DisablePrevious();

                    //unlock tabs after data is arrived
                    topPanel.UnLockAllTabs();

                }
            }

            // This is here to prevent a bug where the lobby top stays on top of the leaderboard.
            // After the items list is built, we make sure the top is not visible.
            if (SpadesScreensManager.Instance.IsLeaderboardShown)
                LobbyTopController.Instance.ShowTop(false);
        }

        private void RemoveBotsFromLeaderboard(LeaderboardModel ldr)
        {
            int newRank = 1;
            List<LeaderboardItemModel> itemsNoBots = new List<LeaderboardItemModel>();

            foreach (LeaderboardItemModel item in ldr.Items)
            {
                if (item.UserId > 0)
                {
                    itemsNoBots.Add(item);
                    item.Rank = newRank++;
                }

            }
            ldr.Items = itemsNoBots;
        }

        private bool IsPrevLeaderboardExists()
        {
            LeaderboardModel prevLB = ModelManager.Instance.Leaderboards.GetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalPrevious);
            return prevLB.LeaderboardMetaData != null;
        }

        public IEnumerator StartLeaderboardCacheTimer()
        {
            while (true)
            {
                //yield return new WaitForSeconds (30f); //wait 30 min	
                yield return new WaitForSeconds(1800f); //wait 30 min	
                ModelManager.Instance.Leaderboards.ClearCache(false);
            }
        }


        public void UpdateLeaderboards(int userEarnings)
        {

            string fb_id;
            if (FacebookController.Instance.UserFacebookData != null)
                fb_id = FacebookController.Instance.UserFacebookData.User_id;
            else
                fb_id = "0";

            if (userEarnings > 0 && ModelManager.Instance.GetUser().GetLevel() >= LeaderboardsModel.LEADERBOARD_MIN_LEVEL)
            {
                // Get the current user details and compare with the last sent user details
                // We want to send the user details to the leaderboard server only once
                mUserDetails = CreateUserDetails();

                /* OFF FOR NOW. ALWAYS SEND THE DETAILS
                if (mUserDetails == SpadesStateController.Instance.GetUserDetails())
                    mUserDetails = null;
                else
                    SpadesStateController.Instance.SetUserDetails(mUserDetails);
                    */

                // Store the last assigned leaderboard bot so we can use on the second update command
                int lbId = ModelManager.Instance.Leaderboards.CurrentLeaderboardId;
                lastAssignedBotId = SpadesStateController.Instance.GetAssignedBotId(lbId);

                LeaderboardServerManager.Instance.Connect((bool success) =>
                {
                    if (success)
                    {
                        // Send the leaderboard update with delay
                        // With WebGL, we found that sometimes the websocket is still not connected and the callback returns
                        // we assume that adding a short delay might solve the problem (28/1/2018)
                        isRealUserUpdate = true;
                        m_lastUserEarnings = userEarnings;
                        StartCoroutine(DelaySend(1f, SpadesCommandsBuilder.Instance.BuildUpdateLeaderboard(ModelManager.Instance.GetUser().GetID(),
                            fb_id,
                            "82.81.87.18",
                            userEarnings,
                            mUserDetails, ModelManager.Instance.Leaderboards.CurrentLeaderboardId)));

                    }
                    else
                    {
                        LoggerController.Instance.Log("Connection Error To LeaderboardServerManager in UpdateLeaderboards");
                    }
                });
            }
        }

        private IEnumerator DelaySend(float timeInSec, MMessage msg)
        {
            yield return new WaitForSecondsRealtime(timeInSec);

            LeaderboardServerManager.Instance.Send(msg);
        }

        public void UpdateLeaderboardBotWin()
        {

            if (isRealUserUpdate)
            {
                isRealUserUpdate = false;

                // TODO: temp fix
                //  CardGamesLocalDataController.AVModelAndCountry aVModel = (LocalDataController.Instance as CardGamesLocalDataController).GetBotById(lastAssignedBotId);
                MAvatarModel aVModel = MAvatarController.Instance.CreateRandomAvatarModel(MAvatarModel.GenderType.Male);
                //
                string botDetails = CreateUserDetails(aVModel);


                int lbId = ModelManager.Instance.Leaderboards.CurrentLeaderboardId;

                // The fake bot id is build from the user is and the leaderboard id. This to make sure it's unique accross 
                // leaderboards and users. The id is negative to help the server identify it's a fake id.
                string paddedUserId = ModelManager.Instance.GetUser().GetID().ToString("D10");
                string paddedLBId = lbId.ToString("D8").Substring(0, 8);
                string fakeBotId = string.Format("-9{0}{1}", paddedLBId, paddedUserId);


                //send bot update
                StartCoroutine(DelaySend(1f, SpadesCommandsBuilder.Instance.BuildUpdateLeaderboard(Convert.ToInt64(fakeBotId),
                    "0",
                    "82.81.87.18",
                    GetBotWinPrice(m_lastUserEarnings),
                    botDetails, lbId)));

                lastAssignedBotId = 0;
                ModelManager.Instance.Leaderboards.ClearCache(false);
            }
        }


        /// <summary>
        /// Calc the bot earnings according to the user earnings by using a random factor.
        /// Round the bot earnings according to the amount to 10,100,1000,10000
        /// </summary>
        private int GetBotWinPrice(int userEarnings)
        {
            float factor = UnityEngine.Random.Range(ModelManager.Instance.Leaderboards.MinBotFactor, ModelManager.Instance.Leaderboards.MaxBotFactor);

            int botEarning = (int)(userEarnings * factor);

            if (botEarning >= 10000)
            {
                botEarning = botEarning / 1000 * 1000;
            }
            else if (botEarning >= 1000)
            {
                botEarning = botEarning / 100 * 100;
            }
            else if (botEarning >= 100)
            {
                botEarning = botEarning / 10 * 10;
            }

            return botEarning;
        }

        private string CreateUserDetails(MAvatarModel model = null)
        {
            MAvatarModel avatarModel;

            if (model == null)
            {
                avatarModel = ModelManager.Instance.GetUser().GetMAvatar();
            }
            else
            {
                avatarModel = model;
            }

            JSONNode avatarJsonObject = MAvatarController.Instance.SerializeAvatarData(avatarModel);

            return ConvertLeaderboardAvatarString(avatarJsonObject.ToString(), true);
        }

        private string ConvertLeaderboardAvatarString(string originalAvatarString, bool isDirectionToServer)
        {
            string newAvatarString;

            if (isDirectionToServer)
            {
                newAvatarString = originalAvatarString.Replace('{', '(');
                newAvatarString = newAvatarString.Replace('}', ')');
                newAvatarString = newAvatarString.Replace(',', '~');
            }
            else
            {
                newAvatarString = originalAvatarString.Replace('(', '{');
                newAvatarString = newAvatarString.Replace(')', '}');
                newAvatarString = newAvatarString.Replace('~', ',');
            }

            return newAvatarString;
        }

        public void switchCurrentPrevious(bool isCurrent)
        {
            if (isCurrent)
                ShowLeaderboard(LeaderboardsModel.LeaderboardType.GlobalCurrent);
            else
                ShowLeaderboard(LeaderboardsModel.LeaderboardType.GlobalPrevious);
        }

        public MAvatarModel ParseUserDetails(string userDetails, LeaderboardItemModel itemModel)
        {
            MAvatarModel avatarModel;
            string avatarString = ConvertLeaderboardAvatarString(userDetails, false);

            JSONNode avatarJson = JSON.Parse(avatarString);

            if (avatarJson != null && avatarJson.IsObject)
            {
                // New avatars
                avatarModel = MAvatarController.Instance.DeserializeAvatarData(avatarJson);
            }
            else
            {
                // Old avatars
                itemModel.ShouldDisplaySilhouette = true;

                // The Avatar Model Is Created Here Only So We Have A Model To Hold The NickName Which Is Set Right After, Will Not Be displayed As We Display A Silhouette Instead
                avatarModel = MAvatarController.Instance.CreateRandomAvatarModel(MAvatarModel.GenderType.Male);
                GetOldUserNickName(userDetails, avatarModel);
            }

            return avatarModel;
        }

        public void GetOldUserNickName(string userDetails, MAvatarModel avatarModel)
        {
            string[] userDetailsArr = userDetails.Split(USER_DETAILS_DELIMITER);

            if (userDetailsArr.Length > 1)
            {
                avatarModel.NickName = userDetailsArr[2];
            }
        }

        /*public void ShowPlayerProfile(long m_userId, OldAvatarModel avatarModel)
        {

            // Do not show the profile for now, since we can have a situation that bots can be detected in 2 ways:
            // 1. The max level for random profiles is not supported, so users can see bots with higher level than the divisioin
            // 2. The profile is not persistent between sessions, so if I close the app and reopen, I can see a different profile.
            // RAN (16/7/2018)
            return;



            NewPlayerProfilePopup popup = PopupManager.Instance.ShowOtherProfilePopup();
            int minLvl;
            if (ModelManager.Instance.Leaderboards.UserProfileCache.ContainsKey(m_userId))
            {
                popup.SetData(ModelManager.Instance.Leaderboards.UserProfileCache[m_userId], true);
            }
            else
            {
                //get min max level from leaderboard meta

                if (ldbrdType == LeaderboardsModel.LeaderboardType.GlobalCurrent || ldbrdType == LeaderboardsModel.LeaderboardType.GlobalPrevious)
                    minLvl = ModelManager.Instance.Leaderboards.GetLeaderboard(ldbrdType).LeaderboardMetaData.MinLvl;
                else
                    minLvl = -1;

                CommManager.Instance.RequestUserStats(minLvl, (int)m_userId, (bool success, UserStats user_stats, int xp, int level, int total_level_xp) =>
                {
                    if (success)
                    {
                        User user = new User();
                        user.SetID((int)m_userId);
                        user.SetLevel(level, total_level_xp, xp);
                        user.SetStats(user_stats);
                        user.SetAvatar(avatarModel);

                        //save user to cache
                        ModelManager.Instance.Leaderboards.UserProfileCache.Add(m_userId, user);
                        popup.SetData(user, true);

                    }
                    else
                    {
                        PopupManager.Instance.HidePopup();
                    }

                });
            }
        }*/


        public GameObject LeaderboardItemRow
        {
            get
            {
                return m_leaderboardItemRow;
            }
            set
            {
                m_leaderboardItemRow = value;
            }
        }

    }


}

