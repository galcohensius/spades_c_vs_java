using System.Collections.Generic;
using spades.models;
using SpadesAI.model;
using System;
using SpadesAI;

namespace spades.controllers
{

    public class ChatBotController
    {

        public delegate void TriggerChatDelegate(Position pos, MAvatarView.AvatarAnimationStateTrigger type, float delay);

        Dictionary<ChatGroups, List<MAvatarView.AvatarAnimationStateTrigger>> m_chat_bot_dict = new Dictionary<ChatGroups, List<MAvatarView.AvatarAnimationStateTrigger>>();

        public enum AIChatType
        {
            GoodMove,
            OppoGoodMove,
            RoundStart,
            Congrats,
            Random
        }

        public enum ChatGroups
        {
            NegativeResponse,
            PositiveResponse,
            Greet
        }

        private const float PERCENT_START_GAME = 0.2f;
        private const float PERCENT_TRICK_TAKEN = 0.1f;
        private const float PERCENT_PLAYER_CHAT = 0.15f;

        private System.Random rand = new System.Random();

        TriggerChatDelegate m_chat_delegate = null;

        // List of positions of bots (excluding human players)
        private List<Position> botsPositions = new List<Position>();

        public void Init(TriggerChatDelegate chat_delegate)
        {
            m_chat_delegate = chat_delegate;

            m_chat_bot_dict.Clear();

            for (int i = 0; i < ((SpadesTable)ModelManager.Instance.GetTable()).Players.Length; i++)
            {
                if (((SpadesTable)ModelManager.Instance.GetTable()).Players[i].Player_Type == Player.PlayerType.Bot)
                    AddBotPosition(RoundController.AIAdapter.IndexToPosition(i));
            }

            //build my dictionary 
            List<MAvatarView.AvatarAnimationStateTrigger> temp_list_positive = new List<MAvatarView.AvatarAnimationStateTrigger>();
            List<MAvatarView.AvatarAnimationStateTrigger> temp_list_negative = new List<MAvatarView.AvatarAnimationStateTrigger>();
            List<MAvatarView.AvatarAnimationStateTrigger> temp_list_greet = new List<MAvatarView.AvatarAnimationStateTrigger>();

            //build the positive list
            temp_list_positive.Add(MAvatarView.AvatarAnimationStateTrigger.Love_1);
            temp_list_positive.Add(MAvatarView.AvatarAnimationStateTrigger.Sleepy);
            temp_list_positive.Add(MAvatarView.AvatarAnimationStateTrigger.Love_2);


            m_chat_bot_dict.Add(ChatGroups.PositiveResponse, temp_list_positive);

            //build the negative list
            temp_list_negative.Add(MAvatarView.AvatarAnimationStateTrigger.Angry);
            temp_list_negative.Add(MAvatarView.AvatarAnimationStateTrigger.Crying);


            m_chat_bot_dict.Add(ChatGroups.NegativeResponse, temp_list_negative);


            temp_list_greet.Add(MAvatarView.AvatarAnimationStateTrigger.Love_1);

            m_chat_bot_dict.Add(ChatGroups.Greet, temp_list_greet);

        }

        public void RoundStarted()
        {
            if (rand.NextDouble() < PERCENT_START_GAME)
            {
                Position? pos = GetRandomBotPos();

                if (pos != null)
                {

                    MAvatarView.AvatarAnimationStateTrigger type = GetRandomChatType(ChatGroups.Greet);

                    float delay = rand.Next(2, 8);

                    if (m_chat_delegate != null)
                        m_chat_delegate((Position)pos, type, delay);

                }

            }
        }

        public void TrickTaken(int trick_number, Position winnerPos)
        {
            if (rand.NextDouble() < PERCENT_TRICK_TAKEN)
            {

                Position? pos = GetRandomBotPos();
                MAvatarView.AvatarAnimationStateTrigger type;

                if (pos != null)
                {
                    // Check if the chatting bot bid nil
                    bool isNilBid = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(RoundController.AIAdapter.PositionToIndex((Position)pos)).GetBids() == 0;

                    if (isNilBid && pos == winnerPos)
                    {
                        // the bot took a trick while bidding nil
                        type = GetRandomChatType(ChatGroups.NegativeResponse);
                    }
                    else if (pos == winnerPos ||
                      (SpadesUtils.PartnerPosition(winnerPos) == pos && SpadesModelManager.Instance.GetActiveMatch().GetMatch().Playing_mode == PlayingMode.Partners))
                    {
                        // The bot or his partner won the trick
                        type = GetRandomChatType(ChatGroups.PositiveResponse);
                    }
                    else
                    {
                        // the bot or his partner lost the trick
                        type = GetRandomChatType(ChatGroups.NegativeResponse);
                    }

                    float delay = rand.Next(10, 20) / 10;

                    if (m_chat_delegate != null)
                        m_chat_delegate((Position)pos, type, delay);
                }

            }
        }

        public void PlayerChat(MAvatarView.AvatarAnimationStateTrigger incoming_type, Position incoming_pos)
        {
            //find FIRST group that contains the incoming chat type - and based on that we can return a feedback

            LoggerController.Instance.Log("Player Chat received from pos: " + incoming_pos + " with type: " + incoming_type);

            if (rand.NextDouble() < PERCENT_PLAYER_CHAT)
            {
                ChatGroups incoming_group = GetContainingGroup(incoming_type);

                Position? pos = GetRandomBotPos();
                MAvatarView.AvatarAnimationStateTrigger type = MAvatarView.AvatarAnimationStateTrigger.Love_1;

                if (pos != null && pos != incoming_pos)
                {

                    //chat came from other bot - currently still returning based on what i got
                    type = GetRandomChatType(incoming_group);

                    float delay = rand.Next(10, 20) / 10;

                    if (m_chat_delegate != null)
                        m_chat_delegate((Position)pos, type, delay);
                }
            }
        }

        private MAvatarView.AvatarAnimationStateTrigger GetRandomChatType(ChatGroups chat_group)
        {
            List<MAvatarView.AvatarAnimationStateTrigger> temp_list = new List<MAvatarView.AvatarAnimationStateTrigger>();

            m_chat_bot_dict.TryGetValue(chat_group, out temp_list);

            return temp_list[UnityEngine.Random.Range(0, temp_list.Count)];
        }

        private ChatGroups GetContainingGroup(MAvatarView.AvatarAnimationStateTrigger incoming_type)
        {
            //if an item belongs to more than 1 group - it will return the group item that is smallest index in the ChatGroup Enum

            List<MAvatarView.AvatarAnimationStateTrigger> temp_list = new List<MAvatarView.AvatarAnimationStateTrigger>();

            foreach (ChatGroups chat_group in Enum.GetValues(typeof(ChatGroups)))
            {
                if (m_chat_bot_dict.TryGetValue(chat_group, out temp_list))
                {
                    foreach (MAvatarView.AvatarAnimationStateTrigger item in temp_list)
                    {
                        if (item == incoming_type)
                            return chat_group;
                    }
                }
            }

            return ChatGroups.Greet;
        }

        private Position? GetRandomBotPos()
        {
            if (botsPositions.Count == 0)
                return null;

            return botsPositions[rand.Next(0, botsPositions.Count)];
        }

        private void AddBotPosition(Position pos)
        {
            botsPositions.Add(pos);
        }

    }
}
