using System.Collections.Generic;
using System;

namespace spades.controllers
{
    /// <summary>
    /// currently not in use + moved from common to spades.
    /// to be refactored at later date
    /// </summary>
	public class AchievementsManager : MonoBehaviour 
	{

		public delegate void AchievementCompleted (Achievement ac);
		public AchievementCompleted					m_ac_completed;

        [SerializeField] TextAsset				m_static_achievements_json_file= null;


		AchievementsAdaptor 					m_adaptor;

		Dictionary<string,List<Achievement>>	m_achievements = new Dictionary<string, List<Achievement>>();



		public static AchievementsManager Instance;

		private void Awake()
		{
			Instance = this;
		}

		public void ReadStaticAchievementsData()
		{
			JSONObject root = (JSONObject)JSON.Parse (m_static_achievements_json_file.ToString());

			foreach (KeyValuePair<string, JSONNode> N in root)
			{
				List<Achievement> new_group = new List<Achievement> ();

				JSONArray arr = N.Value.AsArray;
				string group_name = N.Key.ToString();

				foreach (JSONNode node in arr)
				{
					AchievementType ac_type;

					string node_type = node ["type"];

					if (node_type == "O")
						ac_type = AchievementType.OneTime;
					else if (node_type == "A")
						ac_type = AchievementType.Accumulative;
					else
						ac_type = AchievementType.InternalAccum;

					int total = 0;

					if (node_type == "O")
						total = 1;
					else
						total = node ["total"].AsInt;

					Achievement	new_ach = new Achievement (Convert.ToInt32(group_name),node["id"].AsInt,node["name"],node["description"],node["coins"].AsInt,ac_type,total,node["fb_id"],node["apple_id"],node["google_id"]);
					new_group.Add (new_ach);
				}

				m_achievements.Add (N.Key, new_group);
			}


		}

        /*
         * NOT USED
         * 
		void Start()
		{
			ReadStaticAchievementsData ();

			int curr_plat = SpadesStateController.Instance.GetPlatformCode ();
            
            if (curr_plat==1)//Ios
                m_adaptor = new iOSAchievementsAdaptor();
            else if(curr_plat==2)//Android
                m_adaptor = new AndroidAchievementsAdaptor();
            else if(curr_plat==3)//FB-WEBGL
				m_adaptor = new FBAchievementsAdaptor();
				
		}
        */

		private void CheckLPMAndUpdate(int SavedLPM)
		{
            /*
			if (SpadesStateController.Instance.GetPlatformCode() != SavedLPM)
			{
				foreach (KeyValuePair<string,List<Achievement>> item in m_achievements)
				{
					for (int i = 0; i < item.Value.Count; i++)
					{
						if (item.Value [i].Counter > 0)
							m_adaptor.UpdateAchievement (item.Value [i]);
					}
				}
			}
			*/
		}

		public void DecodeUserAchievements(JSONNode user_data)
		{
			if (!string.IsNullOrEmpty(user_data.ToString()))
			{

				int savedLPM = 0;
				JSONObject root = (JSONObject)JSON.Parse (user_data.ToString());

				foreach (KeyValuePair<string, JSONNode> N in root)
				{
					if (N.Key != "lpm")
					{
						
						UserDataSet (Convert.ToInt32 (N.Key), N.Value.AsInt);
					}
					else
					{
                        savedLPM = Convert.ToInt32 (N.Value.Value);
					}
				}
				CheckLPMAndUpdate (savedLPM);
			}
		}

		public JSONObject EncodeUserAchievements()
		{
            
			JSONObject root_node = new JSONObject ();

			root_node ["lpm"] =SpadesStateController.Instance.GetPlatformCode();

			foreach (KeyValuePair<string,List<Achievement>> ac_group in m_achievements)
			{
				List<Achievement> ac_list = ac_group.Value;

					if (ac_list [0].Counter != 0 || ac_list [0].IsCompleted())
						root_node.Add (ac_group.Key,  ac_list [0].Counter);
						
			}

			LoggerController.Instance.Log ("USER ACHIEVEMENTS: "+ root_node.ToString());

			return root_node;
		}

		public void Increment(int group_id,int delta=1)
		{
			/*
			List<Achievement> ac_list;
			m_achievements.TryGetValue (group_id.ToString (), out ac_list);

			for (int i = 0; i < ac_list.Count; i++)
			{

				if (!ac_list [i].IsCompleted ())
				{
					ac_list [i].IncCounter (delta);
					m_adaptor.UpdateAchievement (ac_list [i]);

					if (ac_list [i].IsCompleted() && m_ac_completed!=null)
						m_ac_completed (ac_list [i]);
				}
			}
			*/
		}

		private void UserDataSet(int group_id, int counter)
		{
            /*
			List<Achievement> ac_list;
			m_achievements.TryGetValue (group_id.ToString (), out ac_list);

			if (ac_list != null) {
				for (int i = 0; i < ac_list.Count; i++) {
					ac_list [i].Counter = counter;
				}
			}
			*/
		}

		public void Set(int group_id,int counter)
		{
            /*
			List<Achievement> ac_list;
			m_achievements.TryGetValue (group_id.ToString (), out ac_list);

			for (int i = 0; i < ac_list.Count; i++)
			{
					if (!ac_list [i].IsCompleted ())
					{
						ac_list [i].Counter = counter;
						m_adaptor.UpdateAchievement (ac_list [i]);

						if (ac_list [i].IsCompleted () && m_ac_completed != null)
							m_ac_completed (ac_list [i]);
					}

			}
			*/
		}

		public Dictionary<string, List<Achievement>> Achievements {
			get {
				return this.m_achievements;
			}
		}




	}
}