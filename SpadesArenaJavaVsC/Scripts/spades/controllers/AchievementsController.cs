﻿namespace spades.controllers
{

	public class AchievementsController : MonoBehaviour 
	{

		public const int	WinGame=1;
		public const int	ConnectFB=2;
		public const int	BreakSpades=3;
		public const int	PrecisionGame=4;
		public const int	WinAfterNegative=5;

		public const int	PlayGames=100;

		public const int	Win3ConsecutiveGames=16;
		public const int	Win5ConsecutiveGames=17;

		public const int	NilBid=300;

		public const int	BlindNilBid=400;

		public const int	PlayPartnerGames=500;

		public const int	PlaySoloGames=600;

		public const int	PlayChallange=700;
       
		public const int	EarnCoins=800;

		public static AchievementsController Instance;

		private void Awake()
		{
			Instance = this;
		}
        		
	}
}