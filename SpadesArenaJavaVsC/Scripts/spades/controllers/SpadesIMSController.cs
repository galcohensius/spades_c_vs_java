using spades.models;

namespace spades.controllers
{

    public class SpadesIMSController : CardGamesIMSController
    {
        public new static SpadesIMSController Instance;

        protected override void Awake()
        {
            base.Awake();
            Instance = this as SpadesIMSController;
        }

        protected override void ListenToIMSEventOnLogin()
        {
            LobbyController.Instance.OnLogin += () =>
            {
                // Fire the event here, after login
                IMSEventDataChanged?.Invoke();

#if QA_MODE
                // For QA only, always initialize the VideoAdController on login to check integration
                VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(ModelManager.Instance.GetUser());
#endif
            };
        }

        protected override void ShowSocialPopup(AddMode addMode, SocialPopup.SocialTab? startingTab)
        {
            SocialController.Instance.ShowSocialPopup(addMode, startingTab);
        }

        

        protected override void ShowProfile()
        {
            SpadesPopupManager.Instance.ShowProfilePopup();
        }


        protected override void ShowRedeemBonus()
        {
            SpadesPopupManager.Instance.ShowRedeemCodePopup();
        }

        protected override void GoToRoom(int room_id)
        {
            LobbyController.Instance.Lobby_view.GetSwipe().InitSwipe(room_id);
        }

        protected override void MiniGameInitiated()
        {
            SpadesPopupManager.Instance.Mini_game_initiated?.Invoke();
        }

        public override void HandleOpenTable(JSONNode node)
        {
            PopupManager.Instance.RemoveAllFuturePopups();
            SpadesSingleMatch singleMatch = SpadesCommManager.Instance.GetSpadesModelParser().ParseSingleMatch(node, null);
            SingleMatchController.Instance.StartSingleMatch(singleMatch);
        }

        protected override void LobbyFBLogin()
        {
            LobbyController.Instance.FBLogin((success) => { });
        }

        protected override void SetLobbyIMSBanner(IMSInteractionZone iZone)
        {
            LobbyTopController.Instance.SetIMSBanner(iZone);
        }

        protected override void ShowTrophy(AddMode addMode)
        {
            SpadesPopupManager.Instance.ShowTrophyRoomPopup(addMode);
        }

        protected override void UpdateCoinsText()
        {
            LobbyTopController.Instance.UpdateCoinsText();
        }

        public override void ExecuteAction(IMSInteractionZone iZone, int actionParamIndex = 0)
        {

            if (iZone is IMSInteractionZoneMESWrapper)
            {
                SpadesMESController.Instance.ExecuteAction((iZone as IMSInteractionZoneMESWrapper).Material, true);
                return;
            }
            base.ExecuteAction(iZone, actionParamIndex);
        }

        protected override void ShowLeaderboard()
        {
            LeaderboardsController.Instance.ShowLeaderboard(LeaderboardsModel.LeaderboardType.Friends);
        }

        protected override void HandleOpenArena(Arena arena)
        {
            SpadesArenaController.Instance.Arena_Selected(arena as SpadesArena);
            LobbyController.Instance.ShowLobby(false);
            LobbyTopController.Instance.ShowHideIndicationPoints(false);
        }

        protected override void HandleBuyUserInventoryItem(string imsActionValue, string iZoneMetadata, int cost)
        {
            LobbyController.Instance.BuyUserInventoryItem(imsActionValue, iZoneMetadata, cost);
        }

        public void AddNewMesBanner(MESMaterial material)
        {

            string imsLocation = "";

            if (material.location.StartsWith(MESBase.MATERIAL_LOCATION_LOBBY))
                imsLocation = BANNER_LOBBY_LEFT;

            IMSInteractionZoneMESWrapper iZone = new IMSInteractionZoneMESWrapper(material, imsLocation);
            AddBannerByLocation(iZone);

            IMSEventDataChanged?.Invoke(); // Should invoke the event for the rotating banner update
        }
    }

}