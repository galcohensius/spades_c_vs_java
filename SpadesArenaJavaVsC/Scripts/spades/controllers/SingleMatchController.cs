using System.Collections;
using spades.models;
using System;

namespace spades.controllers
{

    public class SingleMatchController : MonoBehaviour
    {

        SpadesSingleMatch m_curr_single_match;
        WinSingleOverlay m_win_overlay;
        WinSingle2ndPlaceOverlay m_win_second_place_overlay;

        SearchingForPlayersPopup m_searching_popup;

        public static SingleMatchController Instance;

        bool m_player_rematch = false;
        bool m_partner_rematch = false;
        Player m_rematch_player = null;

        System.Random rand = new System.Random();

        const float REMATCH_ACCEPT_PROB = 0.7f;
        const int REMATCH_ACCEPT_MAX_TIME_SEC = 10;
        const int MIN_LEVEL_REMATCH = 3;

        private void Awake()
        {
            Instance = this;

            BackgroundController.Instance.OnSessionTimeout += () =>
            {
                StopAllCoroutines();
                RoundController.Instance.Table_view.ShowGameUI(false);
            };
        }


        //avModel optional param is the indication for ReMatch - if its sent - it means its for rematch
        public void StartSingleMatch(SingleMatch single_match, SearchingForPlayersPopup.SearchingPopupCancelClicked cancel_delegate = null,
                                     Action insufficient_funds_delegate = null, bool rematch = false, bool show_cancel = true)
        {
            StopAllCoroutines();

            // Clear the bot's stat cache - skip partner clear if rematch
            PlayerView.ClearPlayerCache(rematch);

            SpadesSingleMatch spades_single_match = single_match as SpadesSingleMatch;

            int coins = ModelManager.Instance.GetUser().GetCoins();
            int buyin = spades_single_match.Buy_in;


            //attempting to buy in to the match - we need to check if they have enough money and if so - reduce it and start the game
            //also checking if there is a voucher to use instead
            if (coins < buyin && SpadesVoucherController.Instance.GetVouchersBySpadesMatch(spades_single_match) == null)
            {
                insufficient_funds_delegate?.Invoke();

                (IMSController.Instance as SpadesIMSController).HandleInsufficientFunds(buyin - coins);

            }
            else
            {

                // when the game is finished we need to clear the player so bot data will be sent to it in the next game
                RoundController.Instance.Player = null;
                RoundController.Instance.Table_found_delegate = TableFound;

                ManagerView.Instance.LoadTableSprite(spades_single_match, (Sprite tableSprite) =>
                {
                    if (tableSprite != null)
                    {
                        MissionController.Instance.RequestMissionIfNeeded((bool success) =>
                        {
                            m_searching_popup = LobbyController.Instance.StartSearchingForPlayers(spades_single_match, cancel_delegate, rematch, show_cancel, () =>
                             {
                                m_curr_single_match = spades_single_match;
                             });

                        });

                    }

                });

            }


        }

       

        private void TableFound(Action table_found_done)
        {

            SpadesModelManager model_manager = ModelManager.Instance as SpadesModelManager;

            SpadesActiveMatch active_match = new SpadesActiveMatch(m_curr_single_match);

            model_manager.SetActiveMatch(active_match);

            LobbyTopController.Instance.ShowTop(false, false);
            LobbyController.Instance.ShowLobby(false);

            PopupManager.Instance.HidePopup();

            RoundController.Instance.Init(MatchEndedCallback, MatchCancelledCallback);

            SpadesOverlayManager.Instance.HideAllOverlays();

            active_match.Game_id = ModelManager.Instance.GetTable().Game_id;


            ManagerView.Instance.InGameGui.GetComponent<InGameUIView>().SetActiveMatch(active_match);

            // check if we have a voucher
            bool useVoucher = false;
            if (SpadesVoucherController.Instance.GetVouchersBySpadesMatch(active_match.GetMatch() as SpadesSingleMatch) != null)
                useVoucher = true;

            // handles the avatars in game between the rounds
            MAvatarController.Instance.OnAvatarShownInHierarchy?.Invoke();

            SpadesCommManager.Instance.StartSingleMatch(m_curr_single_match.GetID(), active_match.Game_id, useVoucher, (success, matchTransId, newCoinsBalance, matchBotsData, show_missions, contest, voucher_id) =>
            {
                if (success)
                {
                    //remove the received voucher
                    if (voucher_id != null)
                    {
                        SpadesVoucherController.Instance.UseVoucher(voucher_id);
                        active_match.UsesVoucher = true;
                    }


                    MissionController.Instance.Show_mission_button_in_game = show_missions;

                    active_match.Game_trans_id = matchTransId;

                    m_curr_single_match.BotsData = matchBotsData;

                    ManagerView.Instance.InGameGui.GetComponent<InGameUIView>().SetGameTransId(matchTransId);

                    ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);
                    LobbyTopController.Instance.UpdateCoinsText();

                    // contest controller should call the function
                    ContestPanelController.Instance.OnGameStarted(contest);

                    // Start the game after start match response - This is needed in order to user the bots data received from
                    // the server

                    RoundController.Instance.StartGame(true);
                    // TODO: Handle special tables (Jokers etc...)
                    if (m_curr_single_match.World != null)
                        TrackingManager.Instance.AppsFlyerTrackOneTimeEvent("Played" + m_curr_single_match.World.GetName());

                    // Update the purchaser, so if there are changes in the packages, it will be recorded
                    PurchaseController.Instance.UpdatePurchaser();

                    if (table_found_done != null)
                        table_found_done();

                }
                else
                {
                    LobbyClicked();
                    PopupManager.Instance.ShowMessagePopup("The game can't start right now.\nNo coins were deducted from your balance.", PopupManager.AddMode.ShowAndRemove, true, () =>
                     {
                         // REMOVE THIS - RAN (3/12/2019)
                         //BackgroundController.Instance.ForceSessionTimeout();
                     });
                }
            });

        }



        private void MatchEndedCallback(int win_place, int num_winners, string match_history = null, Action received_answer = null)
        {

            /*  if (ModelManager.Instance.GetUser().NeverPlayed)
                  LobbyController.Instance.Onboarding_tracker.LogEvent("Match_ended");

              LobbyController.Instance.Onboarding_tracker.TrackEvents = false;
              */
            m_player_rematch = false;
            m_partner_rematch = false;

            SpadesModelManager mm = ModelManager.Instance as SpadesModelManager;

            LobbyController lobby_controller = LobbyController.Instance;

            SpadesActiveMatch active_match = mm.GetActiveMatch();

            RoundController.Instance.StopAllChatters();

            int points = 0;
            int opponentsPointsPartners = 0;

            if (active_match.GetMatch().Playing_mode == PlayingMode.Partners)
            {
                points = active_match.GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South);
                opponentsPointsPartners = active_match.GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East);
            }
            else
            {
                points = active_match.GetCalculatedTotalResults(SpadesRoundResults.Positions.South);
            }

            JSONObject user_achievements = AchievementsManager.Instance.EncodeUserAchievements();

            SpadesCommManager.Instance.EndSingleMatch(m_curr_single_match.GetID(), active_match.Game_trans_id, active_match.GetRoundsCount(),
                points, opponentsPointsPartners, false, win_place, num_winners, (mm.GetUser() as SpadesUser).Stats as SpadesUserStats, match_history, user_achievements,
                (success, coinsPayout, coinsForPointsPayout, xpPayout, newCoinsBalance, newDiamondsBalance, newXpBalance, levelData, newLeaderboardId, minBotFactor, maxBotFactor, challengeUpdate, new_cashier, contest) =>
                {
                    if (success)
                    {
                        //update the cashier data
                        if (new_cashier != null)
                        {
                            ModelManager.Instance.SetCashier(new_cashier);
                        }


                        MissionController.Instance.UpdateMissionEndGame(challengeUpdate);

                        ContestPanelController.Instance.OnGameEnded(contest);

                        if (win_place > 0)
                        {
                            if (newLeaderboardId != -1 && ModelManager.Instance.Leaderboards.CurrentLeaderboardId != newLeaderboardId)
                            {
                                ModelManager.Instance.Leaderboards.ClearCache(true);
                                ModelManager.Instance.Leaderboards.CurrentLeaderboardId = newLeaderboardId;
                            }

                            int botId = SpadesStateController.Instance.GetAssignedBotId(newLeaderboardId);
                            // TODO: what shoul be here:
                            /*if (botId == 0)
                            {
                                CardGamesLocalDataController.AVModelAndCountry assignedBot = (LocalDataController.Instance as CardGamesLocalDataController).FetchRandomBots(1, false, 0,0)[0];
                                SpadesStateController.Instance.SetAssignedBotId(newLeaderboardId, assignedBot.Id);
                            }*/

                            ModelManager.Instance.Leaderboards.MinBotFactor = minBotFactor;
                            ModelManager.Instance.Leaderboards.MaxBotFactor = maxBotFactor;

                            LeaderboardsController.Instance.UpdateLeaderboards(coinsPayout);
                        }

                        if (received_answer != null)
                            received_answer();

                        mm.GetUser().SetCoins(newCoinsBalance);
                        mm.GetUser().SetXPInLevel(newXpBalance);

                        if (levelData != null)
                        {
                            int newLevel = levelData.Level;

                            mm.GetUser().SetLevel(newLevel, levelData.XpThreshold, newXpBalance);
                            mm.Last_levelup_bonus_awarded = levelData.LevelBonus;
                            // Overwrite new bonuses data that changed on level up
                            if (levelData.NewBonuses != null)
                                mm.Bonuses.Merge(levelData.NewBonuses);

                            TrackingManager.Instance.AppsFlyerTrackIncrementalEvent(TrackingManager.EventName.ReachedLevel, newLevel, new int[] { 10, 20, 25, 30, 35, 50 });

                        }

                        int gap = points - opponentsPointsPartners;
                        if (m_curr_single_match.SubVariant==PlayingSubVariant.HiLo)
                        {
                            // For hi-lo, Gap is between the player's points and the high points
                            gap = points - m_curr_single_match.HighPoints;
                        }


                        if (win_place == 1)
                        {

                            m_win_overlay = SpadesOverlayManager.Instance.ShowWinSingleOverlay();

                            //need to decide here if we want to show the close button or not
                            //also this is also checking that we only show rematch if player is over level 3
                            if (active_match.GetMatch().Playing_mode == PlayingMode.Partners && ModelManager.Instance.GetUser().GetLevel() > MIN_LEVEL_REMATCH)
                            {
                                m_win_overlay.SetWinScreenData(gap, coinsPayout, coinsForPointsPayout, xpPayout, levelData != null, true, false);
                                StartCoroutine(FakeRematchFlow());
                            }
                            else
                            {
                                m_win_overlay.SetWinScreenData(gap, coinsPayout, coinsForPointsPayout, xpPayout, levelData != null, false, false);

                            }

                            UserStatsController.Instance.UpdateEndMatch(true, coinsPayout);

                        }
                        else if (win_place == 2)
                        {

                            m_win_second_place_overlay = SpadesOverlayManager.Instance.ShowWin2ndPlaceOverlay();

                            m_win_second_place_overlay.SetWinScreenData(gap, coinsPayout, coinsForPointsPayout, xpPayout, LobbyClicked, NewGameClicked, null);
                            CardGamesSoundsController.Instance.GameWin();
                            UserStatsController.Instance.UpdateEndMatch(false, coinsPayout);


                        }
                        else
                        {
                            LoseSingleOverlay lose_single_overlay = SpadesOverlayManager.Instance.ShowLoseSingleOverlay();

                            lose_single_overlay.SetData(false, gap, coinsPayout, coinsForPointsPayout, xpPayout, LobbyClicked, NewGameClicked, null);

                            CardGamesSoundsController.Instance.GameLose();

                            UserStatsController.Instance.UpdateEndMatch(false, 0);



                        }

                        RoundController.Instance.UpdateAllMatchEndAchievements(win_place, coinsPayout, false);

                        // Update the purchaser, so if there are changes in the packages, it will be recorded
                        PurchaseController.Instance.UpdatePurchaser();

                        //calls the MES Controller to update the banners
                        (SpadesMESController.Instance as SpadesMESController).Execute(SpadesMESController.TRIGGER_LOGIN, true);

                        if (PiggyController.Instance.SendUpdatePiggyAtEndGame())
                        {
                            //calls piggy update                                                                            //currently no HiLow so -1
                            CommManagerJava.Instance.GetEndGameSingleMatchPiggyUpdate(CardGamesCommManager.Instance.LoginToken, m_curr_single_match.GetID(), ModelManager.Instance.GetUser().GetID(), coinsForPointsPayout, -1, (piggySuccess, piggyJSON) =>
                            {
                                if (piggySuccess)
                                    PiggyController.Instance.UpdatePiggy(piggyJSON);
                                else
                                    LoggerController.Instance.LogError("update piggy at end single match failed");
                            });
                        }
                    }
                    else
                    {
                        // SHow try again popup
                        ManagerView.Instance.NetworkErrorHandler("endSingleMatch");
                    }
                }
            );


        }


        private void MatchCancelledCallback(bool game_started, Action leave_done, string match_history = null)
        {


            SpadesModelManager mm = ModelManager.Instance as SpadesModelManager;

            if (game_started)
            {
                SpadesActiveMatch active_match = mm.GetActiveMatch();

                (UserStatsController.Instance as SpadesUserStatsController).UpdateEndRound(true, (mm.GetTable() as SpadesTable).GetPlayer(3).GetBids(), (mm.GetTable() as SpadesTable).GetPlayer(3).GetTakes());
                UserStatsController.Instance.UpdateEndMatch(false, 0);

                SpadesCommManager.Instance.EndSingleMatch(m_curr_single_match.GetID(), active_match.Game_trans_id, active_match.GetRoundsCount(), 0, 0,
                    true, 0, 0, (mm.GetUser() as SpadesUser).Stats as SpadesUserStats, match_history, null,
                    (success, coinsPayout, coinsForPointsPayout, xpPayout, newCoinsBalance, newDiamondsBalance, newXpBalance, levelData, newLeaderboardId, minBotFactor, maxBotFactor, challengeUpdate, new_cashier, contest) =>
                    {
                        if (success)
                        {


                            //update the cashier data
                            if (new_cashier != null)
                            {
                                ModelManager.Instance.SetCashier(new_cashier);
                            }


                            MissionController.Instance.UpdateMissionEndGame(challengeUpdate);

                            ContestPanelController.Instance.OnGameEnded(contest);

                            mm.GetUser().SetCoins(newCoinsBalance);
                            mm.GetUser().SetXPInLevel(newXpBalance);

                            if (levelData != null)
                            {
                                int newLevel = levelData.Level;

                                mm.GetUser().SetLevel(levelData.Level, levelData.XpThreshold, newXpBalance);

                                mm.Last_levelup_bonus_awarded = levelData.LevelBonus;
                                // Overwrite new bonuses data that changed on level up
                                if (levelData.NewBonuses != null)
                                    mm.Bonuses.Merge(levelData.NewBonuses);

                                // Appsflyer event
                                if (newLevel == 10 || newLevel == 20)
                                {
                                    TrackingManager.Instance.AppsFlyerTrackEvent(TrackingManager.EventName.ReachedLevel + newLevel);

                                }
                            }

                            if (PiggyController.Instance.SendUpdatePiggyAtEndGame())
                            {
                                //calls piggy update  
                                CommManagerJava.Instance.GetEndGameSingleMatchPiggyUpdate(CardGamesCommManager.Instance.LoginToken, m_curr_single_match.GetID(), ModelManager.Instance.GetUser().GetID(), coinsForPointsPayout, -1, (piggySuccess, piggyJSON) =>
                                {
                                    if (piggySuccess)
                                        PiggyController.Instance.UpdatePiggy(piggyJSON);
                                    else
                                        LoggerController.Instance.LogError("update piggy at end single match failed");
                                });                                
                           }

                            leave_done();

                            LoseSingleOverlay single_overlay = SpadesOverlayManager.Instance.ShowLoseSingleOverlay();
                            int gap = 0;
                            single_overlay.SetData(true, gap, coinsPayout, coinsForPointsPayout, xpPayout, LobbyClicked, NewGameClicked, null);
                            CardGamesSoundsController.Instance.GameLose();


                            // Update the purchaser, so if there are changes in the packages, it will be recorded
                            PurchaseController.Instance.UpdatePurchaser();

                            CardGamesPopupManager.Instance.ShowWaitingIndication(false);

                            //calls the MES Controller to update the banners
                            (SpadesMESController.Instance as SpadesMESController).Execute(SpadesMESController.TRIGGER_LOGIN, true);
                        }
                        else
                        {
                            // SHow try again popup
                            ManagerView.Instance.NetworkErrorHandler("endSingleMatch");
                        }
                    });

            }
            else
            {
                LobbyController.Instance.ShowLobby(true);
            }

        }



        public void NewGameClicked()
        {

            StopAllCoroutines();

            m_player_rematch = false;

            StartSingleMatch(m_curr_single_match, LobbyClicked);
        }

        public void LobbyClicked()
        {

            StopAllCoroutines();

            m_player_rematch = false;
            SpadesOverlayManager.Instance.HideAllOverlays();
            RoundController.Instance.Table_view.ShowGameUI(false);
            LobbyController.Instance.ShowLobby(true, true, false, true);

        }

        public void RematchClicked()
        {

            // Check if the partner has not already asked for a rematch
            if (m_partner_rematch)
            {
                m_player_rematch = true;

                // We have a rematch!
                m_win_overlay.PlayerAcceptedRematch();
                StartCoroutine(StartRematch());

            }
            else
            {
                // Notify the win screen of a rematch request
                m_player_rematch = true;
                m_win_overlay.PlayerInitiatedRematch();
            }

        }


        private IEnumerator FakeRematchFlow()
        {
            // Decide if a rematch should be accepted or not
            bool acceptRemtach = rand.NextBoolean(REMATCH_ACCEPT_PROB);

            // Wait #1
            yield return new WaitForSecondsRealtime(UnityEngine.Random.Range(3, 12));

            // If the player has asked for a rematch, reply
            if (m_player_rematch)
            {
                if (acceptRemtach)
                {
                    // We have a rematch!
                    m_win_overlay.PartnerAcceptedRematch();

                    yield return StartRematch();

                }
                else
                {
                    m_win_overlay.PartnerRejectedRematch(false);
                }
            }
            else
            {
                // Initiate a rematch or leave 
                if (acceptRemtach)
                {
                    m_win_overlay.PartnerInitatedRematch();
                    m_partner_rematch = true;

                    // Wait for X seconds
                    yield return new WaitForSecondsRealtime(REMATCH_ACCEPT_MAX_TIME_SEC);

                    // Cancel the request
                    m_partner_rematch = false;
                    m_win_overlay.CancelPartnerRematch();
                }
                else
                {
                    m_win_overlay.PartnerRejectedRematch(true);

                }
            }

        }

        private IEnumerator StartRematch()
        {
            yield return new WaitForSecondsRealtime(1);

            m_rematch_player = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(1);
            StartSingleMatch(SingleMatchController.Instance.Curr_single_match, LobbyClicked, null, true);

        }

        public void GetAndStartSingleMatch(string tableId)
        {
            // Check if the table already exists
            SpadesSingleMatch singleMatch = ModelManager.Instance.GetSingleMatch(tableId) as SpadesSingleMatch;
            if (singleMatch != null)
            {
                StartSingleMatch(singleMatch);
                return;
            }

            // Check the single match cahce
            if (SpadesModelManager.Instance.Cached_single_matches.TryGetValue(tableId, out singleMatch))
            {
                StartSingleMatch(singleMatch);
                return;
            }

            // Try to get the data from the server
            CardGamesPopupManager.Instance.ShowWaitingIndication(true);
            CardGamesCommManager.Instance.GetTableData(tableId, (success, matchData) =>
            {
                CardGamesPopupManager.Instance.ShowWaitingIndication(false);
                if (success)
                {
                    SpadesModelManager.Instance.Cached_single_matches[tableId] = matchData as SpadesSingleMatch;
                    StartSingleMatch(matchData);
                }
                else
                {
                    LoggerController.Instance.Log("GetTableData command failed");
                    PopupManager.Instance.ShowMessagePopup("Cannot open table, please try again later.", PopupManager.AddMode.ShowAndRemove);
                }
            });
        }


        public SpadesSingleMatch Curr_single_match
        {
            get
            {
                return m_curr_single_match;
            }
            set
            {
                m_curr_single_match = value;
            }
        }


        public bool Rematch
        {
            get
            {
                return m_player_rematch;
            }
        }

        public Player Rematch_player
        {
            get
            {
                return m_rematch_player;
            }
        }

    }

}
