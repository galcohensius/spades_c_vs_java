using System;
using System.Collections.Generic;

namespace spades.controllers
{

	public class StepSwipe : MonoBehaviour
	{
        public const float SCREEN_MASK_OFFSET = 200f;

		public event Action<int> OnReachLimit;
		// 0 - left - 1 - right 2- none

		[SerializeField]Image m_bg_curr = null;
		[SerializeField]Image m_bg_next = null;


        [SerializeField]List<GameObject>	m_indication_points = null;
		bool m_indication_on = true;
		int m_index_to_off = 0;
		int m_index_to_on = 0;

		[SerializeField]RectTransform m_middle_layer = null;
	
		float m_screen_width = 1920f - SCREEN_MASK_OFFSET;
		int m_screen_index = 1;
        
        // PromoRoom Changes
        int m_num_screens = 5;

		float xFactor = 0;
		float yFactor = 0;
		float m_left_limit = 0;
		float m_right_limit = -1*4*(1920f - SCREEN_MASK_OFFSET);
		Vector3 m_fingerPrevPos;
		Vector3 delta_dist = new Vector3 (0, 0, 0);

		bool m_enable_swipe = false;

		[SerializeField] GameObject m_left_limiter = null;
		[SerializeField] GameObject m_right_limiter = null;

        [SerializeField] CanvasGroup m_left_mask_line;
        [SerializeField] CanvasGroup m_right_mask_line;

        RectTransform m_left_limit_rect;
		RectTransform m_right_limit_rect;

		float m_left_extra_for_drag = 0;
		float m_right_extra_for_drag = 0;
		bool m_left_anim = false;
		bool m_right_anim = false;
		float m_extra_drag_size = 200f;

		bool m_locked = false;
		bool m_popup_locked = false;
		bool m_screen_locked = false;
		bool m_overlay_locked = false;
		bool m_slider_locked = false;

		bool m_registered_to_events = false;

		private float fingerStartTime = 0.0f;
		private Vector2 m_fingerStartPos = Vector2.zero;
		private Vector2 m_fingerCurrStartPos = Vector2.zero;
		private const float MIN_SWIPE_DIST = 15f;
		private const float MAX_SWIPE_TIME = 0.35f;
		private const float TIME_TO_MOVE = 0.3f;
	
		Action	m_button_delegate;

		bool m_is_web_gl = false;
        Action <int> m_onMoveRoomComplete;

        Action<float, float> m_onSwipeUpdate;

        public void StartSwipe (Action button_delegate = null)
		{
            if (m_locked)
				return;

            m_button_delegate = button_delegate;
			m_enable_swipe = true;

			#if(!UNITY_EDITOR && !UNITY_WEBGL)
				m_fingerStartPos = Input.touches [0].position;
			#else
			m_fingerStartPos = Input.mousePosition;
			#endif

#if !UNITY_WEBGL
            // For WebGL, the scroll is not working so the mask lines should not be shown to resolve a release outside problem
            FadeMaskLines(true);
#endif

			m_fingerPrevPos = new Vector3 (m_fingerStartPos.x * xFactor, 0, 0);

			m_fingerCurrStartPos = m_fingerStartPos;

			if ((m_fingerStartPos.y * yFactor > 110) && (m_fingerStartPos.y * yFactor < 980)) {
				fingerStartTime = Time.time;
				m_fingerStartPos = Input.mousePosition;
			}


		}

		public void EndSwipe (bool dont_drag = false)
		{

			if (m_locked)
				return;
			
			#if(!UNITY_EDITOR && !UNITY_WEBGL)
			 	Vector3 endDragPos = Input.touches [0].position;
			#else
			Vector3 endDragPos = Input.mousePosition;
#endif

            

            float gestureTime = Time.time - fingerStartTime;
			float gestureDist = ((Vector2)endDragPos - m_fingerCurrStartPos).magnitude;
			Vector2 direction = (Vector2)endDragPos - m_fingerStartPos;
			bool swipe_detected = false;
			int drag_dir = 1;

			if (direction.x > 0)
				drag_dir = -1;
			
			if (gestureTime < MAX_SWIPE_TIME && gestureDist > MIN_SWIPE_DIST) {
				swipe_detected = true;
			}


			if (!m_is_web_gl && !dont_drag)
				SnapBack (drag_dir, swipe_detected);
			
			float totalGestureDist = ((Vector2)endDragPos - m_fingerStartPos).magnitude;

			if (m_button_delegate != null) {
				if (totalGestureDist < MIN_SWIPE_DIST)
					m_button_delegate ();
			}
			m_enable_swipe = false;
			m_button_delegate = null;

			ResetDragLimit (true);
			ResetDragLimit (false);

            
		}

	

		void HandlePopupLockChanged (bool locked)
		{
			m_popup_locked = locked;

			m_locked = (m_popup_locked || m_overlay_locked || m_screen_locked || m_slider_locked);

						
		}

		void HandleOverlayLockChanged (bool locked)
		{
			m_overlay_locked = locked;

			m_locked = (m_popup_locked || m_overlay_locked || m_screen_locked || m_slider_locked);

			
		}

		void HandleScreenLockChanged (bool locked)
		{
			m_screen_locked = locked;

			m_locked = (m_popup_locked || m_overlay_locked || m_screen_locked || m_slider_locked);

		}



		public void InitSwipe (int screen_index = 1)
		{
			//reading the value again even that its in manager view - because of read timing
			m_screen_width = 1920f - SCREEN_MASK_OFFSET;

			int numVirtual = ModelManager.Instance.Worlds.FindAll(w => w.WorldType == World.WorldTypes.Virtual).Count;

            m_num_screens = ModelManager.Instance.Worlds.Count - numVirtual;

            m_right_limit = (m_num_screens - 1) * m_screen_width * -1;

			#if UNITY_WEBGL
			m_is_web_gl = true;
			#endif

			m_screen_index = screen_index;

            if (!m_registered_to_events) {
				m_registered_to_events = true;
				PopupManager.Instance.OnPopupShown += HandlePopupLockChanged;
				SpadesOverlayManager.Instance.OnOverlayShown += HandleOverlayLockChanged;
				SpadesScreensManager.Instance.OnScreenShown += HandleScreenLockChanged;

				xFactor = ManagerView.Instance.Scaler_x / ManagerView.Instance.Scaler_width;
				yFactor = ManagerView.Instance.Scaler_y / ManagerView.Instance.Scaler_height;

				m_left_limit_rect = m_left_limiter.GetComponent<RectTransform> ();
				m_right_limit_rect = m_right_limiter.GetComponent<RectTransform> ();

			}

            for (int i = 0; i < m_indication_points.Count; i++) {
				m_indication_points [i].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 0);
				m_indication_points[i].transform.parent.gameObject.SetActive(i < m_num_screens);
			}

			m_indication_points [screen_index - 1].GetComponent<Image> ().color = new Color (1f, 1f, 1f, 1f);
			m_indication_on = true;

			//World world = ModelManager.Instance.Worlds[m_screen_index];
			//m_bg_curr.sprite = LobbyController.Instance.Lobby_view.World_bgs [world.Index];
			//m_bg_next.color = new Color (1f, 1f, 1f, 0f);

			m_middle_layer.localPosition = new Vector3 (-m_screen_width * (screen_index - 1), 0f, 0f);

            ManageBGColors(); // need to be placed after middle_layer position calculated

            ManageArrows ();
            TurnIndicationPoint(true);
        }


		// Update is called once per frame
		void Update ()
		{
			
			if (m_enable_swipe && !m_locked && !m_is_web_gl) {
				#if(!UNITY_EDITOR && !UNITY_WEBGL)

					Vector3 mousePos=new Vector3();
					if (Input.touchCount > 0)
					{
					mousePos = Input.touches[0].position;
					}

				#else

				Vector3 mousePos = Input.mousePosition;
				#endif


				Vector3 curr_pos = new Vector3 (mousePos.x * xFactor, 0, 0);

				delta_dist = m_fingerPrevPos - curr_pos;

				m_middle_layer.localPosition -= delta_dist;

				m_fingerPrevPos = curr_pos;

				StayInLimit (delta_dist.x);


				if (Math.Abs (delta_dist.x) < 0.1f) {
					fingerStartTime = Time.time;
					m_fingerCurrStartPos = mousePos;
                } else {
                    if (m_indication_on)
                        TurnIndicationPoint(false);
                }

				ManageBGColors ();
                


            }
		}


		private void SnapBack (int swipe_dir, bool swipe_detected = false)
		{
			float	delta = 0;
			float target_x = 0;

			delta = m_middle_layer.localPosition.x + m_screen_index * m_screen_width - m_screen_width;

			if (swipe_detected) {
				m_screen_index += swipe_dir;
				m_screen_index = Mathf.Max (1, m_screen_index);
				m_screen_index = Mathf.Min (m_num_screens, m_screen_index);
				target_x = m_screen_width - m_screen_width * m_screen_index;
			} else {
				if (delta > 0) { //attemp to drag right - which is to go decrease world number 
					if (delta > m_screen_width / 4) {
						//auto complete the drag right
						m_screen_index--;
						m_screen_index = Mathf.Max (1, m_screen_index);
					}
				} else {
					if (Math.Abs (delta) > m_screen_width / 4) {
						m_screen_index++;
						m_screen_index = Mathf.Min (m_num_screens, m_screen_index);
					}
				}
				target_x = m_screen_width - m_screen_width * m_screen_index;
			}

            if (!m_indication_on)
                TurnIndicationPoint (true);

			//force spin all panels
			LobbyController.Instance.Lobby_view.ForceSpinAllPanels();

			iTween.MoveTo (m_middle_layer.gameObject, iTween.Hash ("x", target_x, "easeType", "easeOutCirc", "time", TIME_TO_MOVE, "islocal", true, "oncomplete", "OnMoveRoomCompleted", "Oncompletetarget", this.gameObject, "onupdatetarget", this.gameObject, "onupdate", "ManageBGColors"));


		}

		void ResetDragLimit (bool left)
		{
			if (left) {
				m_left_anim = true;
				iTween.ScaleTo (m_left_limiter, iTween.Hash ("x", 0, "easeType", "easeOutCubic", "time", .5, "onComplete", "LeftLimiterDone", "onCompleteTarget", this.gameObject));
			} else {
				m_right_anim = true;
				iTween.ScaleTo (m_right_limiter, iTween.Hash ("x", 0, "easeType", "easeOutCubic", "time", .5, "onComplete", "RightLimiterDone", "onCompleteTarget", this.gameObject));
			}

		}

		public void TurnIndicationPoint (bool turn_on)
		{
			if (!turn_on) {
                foreach (GameObject indication_point in m_indication_points)
                {
					indication_point.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0);
                }
				m_indication_on = false;


            } else {
				m_indication_points[m_screen_index - 1].GetComponent<Image>().color = new Color(1f, 1f, 1f, 1);
				m_indication_on = true;
                
            }
            
		}


        public void SideArrowsClicked (bool left)
		{
			CardGamesSoundsController.Instance.LobbyNav ();
            FadeMaskLines(true);
			TurnIndicationPoint (false);
			if (left) {
				SnapBack (-1, true);
			} else {
				SnapBack (1, true);
			}
		}


		public void LeftLimiterDone ()
		{
			m_left_anim = false;
			m_left_extra_for_drag = 0f;

		}

		public void RightLimiterDone ()
		{
			m_right_anim = false;
			m_right_extra_for_drag = 0f;

		}

		bool StayInLimit (float extra_drag)
		{
			//need to check if the drag changed direction -- and if it did need to turn off the drag highlight


			if (m_middle_layer.localPosition.x > m_left_limit) {

				extra_drag *= -1;
				m_middle_layer.localPosition = new Vector3 (m_left_limit, 0, 0);	

				if (!m_left_anim) {
					m_left_extra_for_drag += extra_drag;

					if (m_left_extra_for_drag > m_extra_drag_size)
						m_left_extra_for_drag = m_extra_drag_size;

					m_left_limit_rect.localScale = new Vector3 (m_left_extra_for_drag / m_extra_drag_size, 1f, 1f);	
					m_left_limiter.GetComponent<Image> ().color = new Color (1f, 1f, 1f, m_left_extra_for_drag / m_extra_drag_size - 0.2f);
				}
				return true;
			} else if (m_middle_layer.localPosition.x < m_right_limit) {
				//LoggerController.Instance.Log ("extra drag"+extra_drag);	
				//LoggerController.Instance.Log ("m_left_extra_for_drag"+m_left_extra_for_drag);

				m_middle_layer.localPosition = new Vector3 (m_right_limit, 0, 0);

				if (!m_right_anim) {
					m_right_extra_for_drag += extra_drag;

					if (m_right_extra_for_drag > m_extra_drag_size)
						m_right_extra_for_drag = m_extra_drag_size;

					m_right_limit_rect.localScale = new Vector3 (m_right_extra_for_drag / m_extra_drag_size, 1f, 1f);	
					m_right_limiter.GetComponent<Image> ().color = new Color (1f, 1f, 1f, m_right_extra_for_drag / m_extra_drag_size - 0.2f);


				}
				return true;
			} else {
				
				if (m_left_limit_rect.localScale.x > 0) {
					if (extra_drag > 0)
						ResetDragLimit (true);
				}
				if (m_right_limit_rect.localScale.x == 1f) {
					if (extra_drag < 0)
						ResetDragLimit (false);
				}
				return false;
			}
				
		}

		public void ManageBGColors ()
		{
            
			int dir = 0;

			float pos_x = m_middle_layer.localPosition.x / m_screen_width + m_screen_index - 1;


			//LoggerController.Instance.Log ("Pos: " + pos_x);


			if (pos_x < 0) {
				dir = 1;
				pos_x *= -1;
			} else {
				dir = -1;
			}

			int new_color_index = m_screen_index - 1 + dir;

			new_color_index = Mathf.Max (0, new_color_index);
			new_color_index = Mathf.Min (new_color_index, m_num_screens - 1);

			//LoggerController.Instance.Log(" ScreenIndex: " + m_screen_index + " Swap BG Images: " + new_color_index + " dir: " + dir);

			if (m_screen_index != 0) {
				m_bg_curr.sprite = LobbyController.Instance.Lobby_view.World_bgs [m_screen_index - 1];
				m_bg_next.sprite = LobbyController.Instance.Lobby_view.World_bgs [new_color_index];
			}	

			float alpha_value = Mathf.Lerp (1f, 0f, pos_x);

			m_bg_curr.color = new Color (1f, 1f, 1f, alpha_value);
			m_bg_next.color = new Color (1f, 1f, 1f, 1 - alpha_value);
           
            m_onSwipeUpdate(m_middle_layer.localPosition.x, m_screen_width);
            
		}

        public void OnMoveRoomCompleted()
        {
            ManageArrows();

            FadeMaskLines(false);

            if (m_onMoveRoomComplete != null)
            {
                m_onMoveRoomComplete(m_screen_index-1);
            }
        }

        private void FadeMaskLines(bool fade_in)
        {
            if(fade_in)
                iTween.ValueTo(gameObject, iTween.Hash("from", 0, "to", 1, "time", .1f, "onupdate", "UpdateMaskAlpha", "onupdatetarget", this.gameObject));
            else
                iTween.ValueTo(gameObject, iTween.Hash("from", 1, "to", 0, "time", .1f, "onupdate", "UpdateMaskAlpha", "onupdatetarget", this.gameObject));
        }


        public void UpdateMaskAlpha(float value)
        {
            m_left_mask_line.alpha = 1;
            m_right_mask_line.alpha = value;
        }

        private void ManageArrows ()
		{
			
			if (m_screen_index == 1) {
				if (OnReachLimit != null)
					OnReachLimit (0);
				
			} else if (m_screen_index == m_num_screens) {
				if (OnReachLimit != null)
					OnReachLimit (1);
			} else {
				if (OnReachLimit != null)
					OnReachLimit (2);
			}
		}

        public void AdjustSizeForIpad()
        {
            m_left_mask_line.gameObject.transform.localScale = new Vector3(1, 1.2f, 1);
            m_right_mask_line.gameObject.transform.localScale = new Vector3(1, 1.2f, 1);
        }



        public bool Enable_swipe {
			get {
				return m_enable_swipe;
			}
			set {
				
				m_enable_swipe = value;
					
			}
		}

		public int Screen_index {
			get {
				return m_screen_index;
			}
		}

        public Action<int> OnMoveRoomComplete
        {
            get
            {
                return m_onMoveRoomComplete;
            }

            set
            {
                m_onMoveRoomComplete = value;
            }
        }

        public Action<float,float> OnSwipeUpdate
        {
            get
            {
                return m_onSwipeUpdate;
            }

            set
            {
                m_onSwipeUpdate = value;
            }
        }

    }

}
