using spades.models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace spades.controllers
{
    /// <summary>
    /// This is the main application controller
    /// </summary>
    public class LobbyController : MonoBehaviour
    {
        //public const float EXPIRE_TO_DISCONNECT_INTERVAL = 60;



        public const int FULL_POS = -100;

        public const string ONBOARDING_EXPERIMENT_ID = "5";

        public const string ARENA_PANEL_EXPERIMENT_ID = "9";

        public const string INITIAL_WORLD_EXPERIMENT_ID = "100";

        public static LobbyController Instance;


        [SerializeField] GameObject m_lobby_object = null;
        [SerializeField] LobbyView m_lobby_view = null;

        [SerializeField] HourlyBonusView m_hourly_bonus_view = null;

        [SerializeField] int m_curr_lobby_screen = 0;
        [SerializeField] GameObject m_OnBoardingLobby = null;

        int m_old_level = 0;
        int m_old_xp = 0;
        int m_old_threshold = 0;
        bool m_match_to_focus_is_single;

        SpadesModelManager m_model_manager;
        WelcomeOverlay m_welcome_overlay = null;
        FacebookController m_facebook_controller;
        AppleController m_apple_controller;

        Action<bool> m_facebook_login_callback;


        bool m_need_to_show_update_version = false;

        int m_app_version_code;
        MessagePopup m_message_popup;


        OnBoardingOverlay m_onboard_overlay = null;
        bool m_shouldShowOnboardingAfterGame = false;

        SearchingForPlayersPopup.SearchingPopupCancelClicked m_cancel_delegate = null;
        SearchingForPlayersPopup m_searching_for_players_popup;


        //    OnboardingFunnelTracker m_onboarding_tracker;

        private const string EXPERIMENT_ITEM_CONFIRMATION = "10";

        public Action OnLogin { get; set; }

        Coroutine m_fake_delay_routine = null;

        void Awake()
        {
            // Configure logging - only for dev and qa builds
            Debug.unityLogger.logEnabled = false;
#if DEV_MODE || QA_MODE || UNITY_EDITOR
            Debug.unityLogger.logEnabled = true;
#endif
            m_app_version_code = FormatUtils.VersionToVersionCode(Application.version);
            LocalDataController.Instance.AppVersion = m_app_version_code;

            Instance = this;

            // Register for background controller event
            BackgroundController.Instance.OnSessionTimeout += ShowWelcomeOverlay;
            BackgroundController.Instance.OnBackFromBackground += OnBackFromBackground;

        }



        void Start()
        {
            LoggerController.Instance.Log("Lobby controller starting");

            TrackingManager.Instance.GoogleAnalyticsTrackLoading("05_CONTROLLER_INIT");

            //  m_onboarding_tracker = TrackingManager.Instance.OnboardingFunnelTracker;

            Time.timeScale = 1f;

            // Enable / Disable logging

            FacebookSettings.SelectedAppIndex = 0;
#if QA_MODE
            FacebookSettings.SelectedAppIndex = 1;
#elif DEV_MODE
            FacebookSettings.SelectedAppIndex = 2;
#endif

#if !UNITY_WEBGL
            Application.targetFrameRate = 60;
#endif

            m_lobby_object.SetActive(false);

            m_model_manager = ModelManager.Instance as SpadesModelManager;
            ManagerView.Instance.ReadAndSetScalerInfo();

            CardGamesCommManager.Instance.SessionTimeOut += OnSessionTimeout;

            ExternalParamsManager.Instance.Init();

            m_facebook_controller = FacebookController.Instance;
            m_facebook_controller.DefaultFacebookImage = ManagerView.Instance.Default_facebook_image;
            m_facebook_controller.FBBaseUrl = CardGamesCommManager.Instance.FbCanvasUrl;

            m_apple_controller = AppleController.Instance;

            try
            {
                NotificationController.Instance.CancelProcessedNotifications();
                NotificationController.Instance.RequestNotificationsPermission();
            } catch (Exception e)
            {
                LoggerController.Instance.LogError("Cannot request notification permissions. " + e);
            }

            VideoAdController.Instance.OnRewardedVideoServed += VideoRewardsBonus;
            VideoAdController.Instance.OnVideosNotAvailable += ShowVideosNotAvailablePopup;


            ShowWelcomeOverlay();
            


            // Game center - NO AUTO LOGIN FOR NOW (14/10/18)
            /*
            if (!SpadesStateController.Instance.GetShouldShowGameCenterLogin())
            {
                LoggerController.Instance.Log("LOBBY Trying to login to Game Center...");

                GameCentersManager.Instance.Login((success) =>
                {

                    LoggerController.Instance.Log("Login done in LOBBY - with result: " + success);

                    if (success)
                    {
                        //nothing special to do with it right now
                    }
                    else
                    {
                        SpadesStateController.Instance.SetDontShowGameCenterLogin(1);
                    }
                });
            }
            */

            //register to listen to complete purchases - 
            CashierController.Instance.PurchaseCompleted += (string packageId, IMSGoodsList goodsList) =>
            {
                // If vouchers are bought, show them in the lobby
                if (goodsList.Vouchers.Count > 0)
                    m_lobby_view.CheckVouchersAndGoToHighest();
            };

        }

       
        private void ShowWelcomeOverlay()
        {
            m_welcome_overlay = SpadesOverlayManager.Instance.ShowWelcomeOverlay();//not using the call back - leaving it for the comm manager to do - but keeping the ref to close it
            CardGamesPopupManager.Instance.ShowWaitingIndication(false);

            // Calc the device id, which is an async operation
            // when the callback is invoked, continue with facebook init
            SpadesStateController.Instance.CalcDeviceIdAsync(() =>
            {
                LocalDataController.Instance.LoadRemoteConfig((succ) =>
                {
                    // Init the Avatar controller
                    MAvatarController.Instance.Init((success) =>
                    {
                        m_facebook_controller.InitFB(FBInitDone);
                    });
                });
            });
        }

        private void OnBackFromBackground(bool sessionTimeout, TimeSpan pauseInterval)
        {
            // Check if we got new external params
            ExternalParamsManager.Instance.Init();

            string bonusToken = ExternalParamsManager.Instance.GetParam(ExternalParamsManager.BONUS_CODE_PARAM, false); // Don't delete after read since it will be read again before login
            if (bonusToken != null)
            {
                LoggerController.Instance.Log("Got bonus code on back from background: " + bonusToken);
            }

            string imblockToken = ExternalParamsManager.Instance.GetParam(ExternalParamsManager.MESSENGER_ID_PARAM, false); // Don't delete after read since it will be read again before login
            if (imblockToken != null)
            {
                LoggerController.Instance.Log("Got imblock code on back from background: " + imblockToken);
            }

            // If we got a param, login again
            if (bonusToken != null || imblockToken != null)
                ShowWelcomeOverlay();
        }

        private void FBInitDone()
        {
            TrackingManager.Instance.GoogleAnalyticsTrackLoading("06_FB_INIT_COMPLETE");

            if (m_facebook_controller.IsLoggedIn())
            {

                LoggerController.Instance.Log("User is already logged in to facebook");
                m_facebook_controller.GetUserDetails(UserDetailsCallback);
                SocialController.Instance.RequestFacebookSocialData();
                SocialController.Instance.StartAppRequestCheck();

            }
            else
            {
#if UNITY_WEBGL
                FBLogin((bool success) =>
                {

                    if (!success)
                    {
                        // if FB login failed - show the user a button to try again (only in WEB GL and editor)
                        //m_welcome_overlay.ShowTryAgain();
                        LoggerController.Instance.Log("Login Failed");
                        
                        // Currently facebook for some reason returns false even when a user accept on WebGL (25/10/18)
                        refreshBrowser();
                    }
                });
#else

                if (m_welcome_overlay != null)
                    m_welcome_overlay.UpdateProgress(0.7f);

                Debug.Log("AppleSignIn @ FBInitDone procedure");

                m_apple_controller.SignInWithApple((AppleData appleData) =>
                {
                    LoginWithApple(appleData);
                });
#endif
            }
        }

        public void LoginWithApple(AppleData appleData)
        {
            string bonusToken = ExternalParamsManager.Instance.GetParam(ExternalParamsManager.BONUS_CODE_PARAM);
            string imblockToken = ExternalParamsManager.Instance.GetParam(ExternalParamsManager.MESSENGER_ID_PARAM);

            CardGamesCommManager.Instance.Login(SpadesStateController.Instance.GetDeviceId(), SpadesStateController.Instance.GetPlatformCode(),
                                        m_app_version_code, FacebookController.Instance.UserFacebookData, appleData, null, bonusToken, imblockToken, LoginCompleted, HandleMaintenance);
        }

        private void HandleMaintenance()
        {
            SpadesPopupManager.Instance.ShowMaintenancePopup();
        }



        private void UserDetailsCallback(bool detailsSuccess)
        {
            if (detailsSuccess)
            {
                string bonusToken = ExternalParamsManager.Instance.GetParam(ExternalParamsManager.BONUS_CODE_PARAM);
                string imblockToken = ExternalParamsManager.Instance.GetParam(ExternalParamsManager.MESSENGER_ID_PARAM);

                if (m_welcome_overlay != null)
                    m_welcome_overlay.UpdateProgress(0.7f);

                m_apple_controller.SignInWithApple((AppleData appleData) =>
                {
                    CardGamesCommManager.Instance.Login(SpadesStateController.Instance.GetDeviceId(), SpadesStateController.Instance.GetPlatformCode(),
                                           m_app_version_code, FacebookController.Instance.UserFacebookData,appleData, TrackingManager.Instance.GetTrackingInfo(), bonusToken, imblockToken,
                    (bool success, User user, List<World> worlds, Dictionary<string, ActiveArena> activeArenas, Cashier cashier, Mission mission,
                     List<MissionController.ClaimMissionData> missions_to_claim, long next_mission_date, List<ContestsController.ClaimContestData> contests_to_claim, List<VouchersGroup> vouchers, bool newUser, UserStats stats,
                      List<Bonus> bonuses, VersionData versionData, ItemsInventory inventory) =>
                    {

                        if (success)
                        {
                            LoginCompleted(success, user, worlds, activeArenas, cashier, mission, missions_to_claim, next_mission_date, contests_to_claim, vouchers,
                                newUser, stats as SpadesUserStats, bonuses, versionData, inventory);

                        }

                        m_facebook_login_callback?.Invoke(success);

                    }, HandleMaintenance);
                });

            } else
            {
                LoggerController.Instance.LogError("Cannot get Facebook user details");
                m_facebook_login_callback?.Invoke(false);
            }


        }

        public void LoginCompleted(bool success, User user, List<World> worlds, Dictionary<string, ActiveArena> activeArenas,
                                         Cashier cashier, Mission mission, List<SpadesMissionController.ClaimMissionData> missions_to_claim, long next_mission_date,
                                         List<ContestsController.ClaimContestData> contests_to_claim, List<VouchersGroup> vouchers,
                                          bool newUser, UserStats stats, List<Bonus> bonuses, VersionData versionData, ItemsInventory inventory)
        {
            if (!success)
            {
                ManagerView.Instance.NetworkErrorHandler("login");
                return;
            }
            else
            {
                //    m_onboarding_tracker.TrackEvents = user.NeverPlayed;
            }
            //ExperimentsManager.Instance.OverrideValue(ONBOARDING_EXPERIMENT_ID, 3);

            // m_onboarding_tracker.LogEvent("Login_success");
            // Set the user is to the State controller for user specific settings
            SpadesStateController.Instance.User_id = user.GetID();

            m_model_manager.SetUser(user);


            // transfer gem data from worlds to items list for new avatar 

            MAvatarModel av_model = user.GetMAvatar();
            LobbyTopController.Instance.SetAvatarModel(av_model);

            // This is commented out to make sure it is triggered by model finishing the load 

            if (m_facebook_controller.IsLoggedIn() && FacebookController.Instance.UserFacebookData != null)
            {
                av_model.FB_ID = FacebookController.Instance.UserFacebookData.User_id;

                // Set the name instead of Guest (unless changed by the user)
                if (av_model.NickName == null/* || av_model.NickName == AvatarCreationController.DEFAULT_AVATAR_NAME*/)
                {
                    av_model.NickName = FacebookController.Instance.UserFacebookData.First_name;

                    LobbyTopController.Instance.Progress_view.SetUserNameText(av_model.NickName);

                    // Update the server
                    CardGamesCommManager.Instance.UpdateUserNickname(av_model.NickName, null);
                }


                av_model.FireOnModelChangedEvent();
            }
            else
            {
                av_model.FB_ID = null;
                if (av_model.NickName == null)
                    av_model.NickName = "Guest";//AvatarCreationController.DEFAULT_AVATAR_NAME;

                // Avatar Mode Is Facebook And We Are Not Connected
                if (av_model.Mode == MAvatarModel.MAvatarMode.Facebook)
                {
                    //TODO: should random here?
                    //av_model = AvatarCreationController.Instance.CreateConcreteGuestData();
                    user.SetMAvatar(av_model);

                }
            }

            // If the user model was randomally created, update the server so next login will get the same avatar
            if (av_model.IsAutoGenerated)
                CardGamesCommManager.Instance.UpdateUserAvatar(av_model);

            LobbyTopController.Instance.UpdateCoinsText();


            //social data request
            SocialController.Instance.RequestFacebookSocialData();
            SocialController.Instance.StartAppRequestCheck();
            m_lobby_view.SetUser();

            m_model_manager.Worlds = worlds;

            m_model_manager.SetCashier(cashier);
            m_model_manager.SetMission((SpadesMission)mission);


            MissionController.Instance.Next_mission_date = next_mission_date;
            MissionController.Instance.To_claim_mission = missions_to_claim;


            m_model_manager.SetActiveArenas(activeArenas);

            ModelManager.Instance.On_Mode_Changed = null;
            SpadesModelManager.Instance.Playing_mode = SpadesStateController.Instance.GetPlayingMode();
            SpadesModelManager.Instance.Initial_playing_mode = SpadesModelManager.Instance.Playing_mode;

            CardGamesSoundsController.Instance.AdjustMusicAndSFX();
            CardGamesSoundsController.Instance.PlayBGMusic();

            // Use local stats unless older than server stats
            UserStats localStats = UserStatsController.Instance.LoadUserStatsLocaly();
            if (localStats != null && localStats.SaveTimeStamp >= stats.SaveTimeStamp)
                (m_model_manager.GetUser() as SpadesUser).Stats = localStats;
            else
                (m_model_manager.GetUser() as SpadesUser).Stats = stats as SpadesUserStats; //casting may fail? need to chack

            m_model_manager.Bonuses = bonuses;


            PurchaseController.Instance.InitPurchaser(ModelManager.Instance.GetCashier(), (CardGamesCommManager.Instance as SpadesCommManager), (MESBase.Instance as SpadesMESController), common.ims.IMSController.Instance, PurchaseController.Instance.RestoredPurchaseCallback);
            PurchaseController.Instance.RestoredPurchaseCallback += OnPurchaseComplete;



            // Clear the leaderboards cache + metadata
            ModelManager.Instance.Leaderboards.ClearCache(true);

            m_lobby_view.UpdateContestsLock();

            // Inventory
            m_model_manager.Inventory = inventory;

            // Adding gems from old model to inventory model 
            foreach (var world in worlds)
            {
                for (int i = 0; i < world.Arena_Gems_Won; i++)
                {
                    inventory.AddItem($"gem_{world.GetID()}_{i+1}");
                }
            }

            //contests auto claim 
            ContestsController.Instance.SetContestsToClaim(contests_to_claim);

            SpadesVoucherController.Instance.NewVouchersReceived(vouchers, true);

            NotificationController.Instance.CancelPendingNotifications(NotificationController.NEW_USER_NOTIF_ID);
            if (user.NeverPlayed)
            {
                NotificationController.Instance.ScheduleNewUserNotification();

                // Schedule bonus notifications
                NotificationController.Instance.ScheduleHourlyNotification();
                NotificationController.Instance.ScheduleDailyNotification();
            }


            // Pushwoosh registration
            CardGamesPushWooshNotificator.Instance.SetUserTags(user);
            CardGamesPushWooshNotificator.Instance.SavePushToken();


            // AppsFlyer tracking
            TrackingManager.Instance.AppsFlyerStartTracking(user.GetID(), CardGamesPushWooshNotificator.Instance.Fcm_sender_id);

            // Second day tracking event, send only once
            if (user.Days_from_reg == 1)
            {
                if (!SpadesStateController.Instance.CheckAndSet("SecondDayTrackingEvent"))
                    TrackingManager.Instance.AppsFlyerTrackEvent(TrackingManager.EventName.SecondDayUser, new Dictionary<string, string>());
            }

            //init side games
            SideGamesController.Instance.InitSideGames(UpdateLobbyBalanceView);

            // Init VideoAdController from a specific level and below uvs level
            int minVideoInitLevel = LocalDataController.Instance.GetSettingAsInt("MinVideoInitLevel", 3);
            int maxVideoUvsLevel = LocalDataController.Instance.GetSettingAsInt("MaxVideoUvsLevel", 3);
            if (user.GetLevel()>=minVideoInitLevel && user.Uvs_level<maxVideoUvsLevel)
            {
                VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(user);
            }


            CreatePromoRooms();

            m_lobby_view.CreateLobbyWorlds(m_model_manager.Worlds);

            //update the hourly bonus 
            m_hourly_bonus_view.UpdateBonusView();

            ContestsController.Instance.OnLogin();

            OnLogin?.Invoke(); // Location of this invokation is crucial

            if (m_welcome_overlay != null)
            {
                m_welcome_overlay.UpdateProgress(1, () =>
                {
                    HideWelcomeOverlay(versionData);
                    LoggerController.Instance.Log("Closing Welcome Screen");
                });
            }
            else
            {
                HideWelcomeOverlay(versionData);
                LoggerController.Instance.Log("Closing Welcome Screen");
            }


            // Unity crash report metadata
            CrashReportHandler.SetUserMetadata("user_id", user.GetID().ToString());
            CrashReportHandler.SetUserMetadata("fb_id", user.GetMAvatar()?.FB_ID); //CrashReportHandler.SetUserMetadata("fb_id", user.Facebook_data?.User_id);
            CrashReportHandler.SetUserMetadata("level", user.GetLevel().ToString()); // Level is also changed in User 
            CrashReportHandler.SetUserMetadata("balance", user.GetCoins().ToString()); // Coins is also changed in Use


            bool shouldShowPiggy = PiggyController.Instance.ShowOrDeactivatePiggy();
            if (shouldShowPiggy)
            {
                if (CardGamesStateController.Instance.GetShouldShowPiggyUnlocked() == false && user.GetLevel() >= PiggyController.Instance.MinPiggyLevel)
                {
                    CommManagerJava.Instance.GetPiggy((success3, responseData) =>
                    {
                        if (success3)
                        {
                            PiggyController.Instance.SetUserPiggy(responseData);
                        }
                        else
                            LoggerController.Instance.LogError("bad piggy");
                    });
                }
            }
            else
                PiggyController.Instance.ActivatePiggy?.Invoke();


            //TODO: room 6 special banner on the arena
            WorldItemView worldItemView = Lobby_view.World_items.Find(x => x.World.Index == 5);
            if (worldItemView != null)
                worldItemView.AddIMSBannerIfNeeded();

        }

        private void CreatePromoRooms()
        {
            string worldId = ModelManager.Instance.InitialWorldId;
            World GPS1World = ModelManager.Instance.GetWorldById(worldId);

            Dictionary<string, List<IMSInteractionZone>> banners = IMSController.Instance.BannersByLocations;

            bool promoRoomAfterGPS1 = LocalDataController.Instance.GetSettingAsBool("PromoRoomAfterGPS1", true);

            // If all the tables are locked, we cannot put the promo after the world and must put it before
            if (AreAllTablesLocked(worldId))
                promoRoomAfterGPS1 = false;
            else if (banners.ContainsKey("R1"))
            {
                // There might be an override for the location as part of the banner. We currently just check R1 not R2
                IMSInteractionZone iZoneR1 = banners["R1"][0];
                if (iZoneR1.OptionalParam(IMSController.OPTIONAL_PARAM_PROMO_LOCATION) == "R")
                    promoRoomAfterGPS1 = true;
                else if (iZoneR1.OptionalParam(IMSController.OPTIONAL_PARAM_PROMO_LOCATION) == "L")
                    promoRoomAfterGPS1 = false;
            }
            

            if (banners.ContainsKey("R1"))
                InsertNewPromoWorld(GPS1World, banners["R1"][0], promoRoomAfterGPS1);

            if (banners.ContainsKey("R2"))
                InsertNewPromoWorld(GPS1World, banners["R2"][0], promoRoomAfterGPS1);
        }

        private void InsertNewPromoWorld (World refWorld, IMSInteractionZone promoIZone,bool after = true)
        {
            List<World> worlds = ModelManager.Instance.Worlds;

            // Find the world index
            int worldIndex = 0;
            for (int i = 0; i < worlds.Count; i++)
            {
                if (worlds[i] == refWorld)
                    worldIndex = i;
            }

            World world = new SpadesWorld();
            world.SetId(promoIZone.Location);
            world.WorldType = WorldTypes.Promo;
            world.Promo_iZone = promoIZone;

            if (!after)
                m_model_manager.Worlds.Insert(worldIndex, world);
            else
                m_model_manager.Worlds.Insert(worldIndex+1, world);
        }


        private void UpdateLobbyBalanceView(bool count = false)
        {
            LobbyTopController.Instance.UpdateCoinsText(count);
        }

        /// <summary>
        /// surplus params only to satisfy the signature. was yanked out of PurchaseControllerMobile to separate it from spades.
        /// </summary>
        /// <param name="success"></param>
        /// <param name="canceled"></param>
        /// <param name="latePayment"></param>
        /// <param name="newCoinsBalance"></param>
        /// <param name="cashier"></param>
        private void OnPurchaseComplete(bool success, bool canceled, bool latePayment, int newCoinsBalance, IMSGoodsList imsGoodsList, Cashier cashier)
        {
            ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);
            LobbyTopController.Instance.UpdateCoinsText();
        }


        private void VideoRewardsBonus(Bonus.BonusTypes bonusType,IMSInteractionZone originIZone)
        {
            LoggerController.Instance.Log("Rewarding user for watching video with bonus: "+ bonusType);

            RewardedVideoBonus rewardedVideoBonus = (RewardedVideoBonus)ModelManager.Instance.FindBonus(bonusType);
            if (rewardedVideoBonus != null && rewardedVideoBonus.IsEntitled)
            {
                UnityMainThreadDispatcher.Instance.Enqueue(() =>
                {
                    SpadesPopupManager.Instance.ShowGeneralBonusPopup(bonusType, PopupManager.AddMode.ShowAndRemove,originIZone: originIZone);
                });
            }
            else
            {
                LoggerController.Instance.LogError("Video bonus does not exist for: " + bonusType);
            }


        }

        private void HideWelcomeOverlay(VersionData versionData)
        {
            SpadesOverlayManager.Instance.HideAllOverlays();
            m_welcome_overlay = null;

            ShowLobby(true, false, true, true);


            LoggerController.Instance.Log("Checking for newer version. Version from server: " + versionData);

            VersionController.Instance.CheckVersion(versionData, m_app_version_code, () =>
            {

                // if need age verification - show it - if not - close the welcome 
                // Age verification in only for Android and controlled by a flag from the server
                /*
                if (versionData.ShowAgeVerification && Application.platform == RuntimePlatform.Android && !SpadesStateController.Instance.GetAgeVerificationComplete())
                {
                    // Need to show the age verification screen
                    PopupManager.Instance.ShowAgeVerificationPopup(PopupManager.AddMode.ShowAndKeep, null, () =>
                    {
                        SpadesStateController.Instance.SetAgeVerificationCompleted();
                        //m_welcome_overlay.CloseWelcomeOverlay();
                    });
                }*/
                PostWelcomeOverlay();

            });

        }

        private void PostWelcomeOverlay()
        {

            if (m_model_manager.GetUser().Account_Status == CommonUser.AccountStatus.WarnBeforeBan)
            {
                SpadesPopupManager.Instance.ShowBanUserPopup(PopupManager.AddMode.ShowAndRemove, CommonUser.AccountStatus.WarnBeforeBan);
            }


            //show global bonus
            if (ModelManager.Instance.FindBonus(Bonus.BonusTypes.GeneralBonus, 0) != null)
            {
                SpadesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.GeneralBonus, PopupManager.AddMode.DontShowIfPopupShown);
            }

            FacebookBonus facebookBonus = (FacebookBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.FacebookConnect);
            if (facebookBonus != null && facebookBonus.IsEntitled && Application.platform != RuntimePlatform.WebGLPlayer)
            {
                SpadesPopupManager.Instance.ShowGeneralBonusPopup(Bonus.BonusTypes.FacebookConnect, PopupManager.AddMode.DontShowIfPopupShown);
            }

            //new user gets onboarding
            if (m_model_manager.GetUser().NeverPlayed)
            {
                //   m_onboarding_tracker.LogEvent("Onboarding_started");
                StartOnBoarding();
                return;
            }

            // else if (ExternalParamsManager.Instance.GetParam(ExternalParamsManager.BONUS_CODE_PARAM) != null)
            //{
            //not entitled
            //    PopupManager.Instance.ShowMessagePopup("You're not eligible for the bonus", PopupManager.AddMode.ShowAndRemove, true);
            //}


            // Show invite friends bonus
            Bonus inviteBonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Invite);
            if (inviteBonus != null && inviteBonus.IsEntitled)
            {
                CardGamesPopupManager.Instance.ShowInviteBonusPopup(() =>
                {
                    // Update the lobby balance to show the current model balance
                    // Needed if the animation has not finished
                    LobbyTopController.Instance.UpdateCoinsText();
                });
            }

            // Leaderboard reward is removed on version 1.23
            /*
            if (ModelManager.Instance.LeaderboardReward != null && ModelManager.Instance.LeaderboardReward.ModelIsSet)
            {
                SpadesPopupManager.Instance.ShowLeaderboardRewardPopup();
            }
            */

            //auto claim contests if any are there
            if (ContestsController.Instance.To_claim_contests.Count > 0)
                ContestsController.Instance.ShowContestsPopup();


            // Show Daily bonus
            DailyBonus dailyBonus = (DailyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Daily);
            if (dailyBonus.IsEntitled && !ModelManager.Instance.GetUser().NeverPlayed)
            {
                SpadesPopupManager.Instance.ShowSlotPopup(() =>
                {

                    m_hourly_bonus_view.UpdateBonusView();
                });
            }

            if (m_model_manager.GetUser().GetLevel() >= 3)
            {
                FacebookController.Instance.LogAppEvent("MES_Funnel", new Dictionary<string, object>()
                {
                    {"action","login"}
                }, 0);
            }

            // Trigger login event - IMS
            common.ims.IMSController.Instance.TriggerEvent(common.ims.IMSController.EVENT_LOGIN);

            // Trigger login event 
            (MESBase.Instance as SpadesMESController).Execute(MESBase.TRIGGER_LOGIN);

            if (MESBase.Instance.EventMap.ContainsKey(MESBase.TRIGGER_LOGIN.ToString()))
            {
                FacebookController.Instance.LogAppEvent("MES_Funnel", new Dictionary<string, object>()
                {
                    {"action","got mes"}
                }, 0);
            }


            (MissionController.Instance as SpadesMissionController).AutoShowMissionPopupOnLogin();


        }


        private void StartOnBoarding()
        {
            if (ExperimentsManager.Instance.GetValue(ONBOARDING_EXPERIMENT_ID) == 2)
            {
                // No onboarding, auto start
                StartCoroutine(AutoClickFirstGame());
                return;
            }

            if (ExperimentsManager.Instance.GetValue(ONBOARDING_EXPERIMENT_ID) == 3)
            {
                OnBoardingOverlay2 overlay = SpadesOverlayManager.Instance.ShowOnboardingOverlay2();
                overlay.SetLobbyItemData(SpadesModelManager.Instance.GetWorld("1").GetSingleMatches(PlayingMode.Partners)[0], m_lobby_view.World_items[0]);
                return;
            }

            SpadesOverlayManager.Instance.Onboarding_overlay = m_OnBoardingLobby;
            m_onboard_overlay = SpadesOverlayManager.Instance.ShowOnboardingOverlay();
            m_onboard_overlay.SetLobbyItemData(SpadesModelManager.Instance.GetWorld("1").GetSingleMatches(PlayingMode.Partners)[0]);
        }

        private IEnumerator AutoClickFirstGame()
        {
            yield return new WaitForSeconds(1);
            if (!PopupManager.Instance.IsPopupShown)
            {
                SpadesSingleMatch firstMatch = SpadesModelManager.Instance.GetWorld("1").GetSingleMatches(PlayingMode.Partners)[0];

                SingleMatchController.Instance.StartSingleMatch(firstMatch);
            }
        }

        public void FBLogin(Action<bool> loginCallback)
        {
            if (m_facebook_controller.IsLoggedIn())
            {
                loginCallback(true);
                return;
            }

            LoggerController.Instance.Log("Logging in to facebook");

            m_facebook_login_callback = loginCallback;

            m_facebook_controller.Login((bool success) =>
            {
                if (success)
                {

                    m_facebook_controller.GetUserDetails(UserDetailsCallback);

                }
                else
                {
                    if (m_facebook_login_callback != null)
                        m_facebook_login_callback(false);

                }

            }
            );

        }



        public void FBLogOut(Action<bool> logoutCallback)
        {
            SocialController.Instance.StopAppRequestCheck();

            m_facebook_controller.Logout();

            (CardGamesCommManager.Instance as SpadesCommManager).Login(SpadesStateController.Instance.GetDeviceId(), SpadesStateController.Instance.GetPlatformCode(),
                m_app_version_code, null, m_apple_controller.AppleData, null, null, "",
                (bool success, User user, List<World> worlds, Dictionary<string, ActiveArena> activeArenas,
                 Cashier cashier, Mission mission, List<SpadesMissionController.ClaimMissionData> missions_to_claim, long next_mission_date,
                 List<ContestsController.ClaimContestData> contests_to_claim, List<VouchersGroup> vouchers,
                  bool newUser, UserStats stats, List<Bonus> bonuses, VersionData versionData, ItemsInventory inventory) =>
                {
                    LoginCompleted(success, user, worlds, activeArenas, cashier, mission, missions_to_claim, next_mission_date, contests_to_claim, vouchers, newUser,
                         stats as SpadesUserStats, bonuses, versionData, inventory);

                    m_lobby_view.SetUser();
                    m_lobby_view.UpdateContestsHats();
                    logoutCallback(success);
                }, HandleMaintenance);

        }


        /// <summary>
        /// Shows or hides the lobby.
        /// </summary>
		public void ShowLobby(bool show, bool showTableUnlock = false, bool goToHighestTable = false, bool showLeaderboardsUnlock = false)
        {
            // CardGamesSoundsController.Instance.ChangeVolume(true, 1f);
            SoundsController.Instance.AdjustMusicAndSFX();

            m_lobby_object.SetActive(show);

            PiggyController.Instance.ShowWaitingIndication = true;

            if (show)
            {
                // moved to show so it won't trigger in table on 'another game' 
                PiggyController.Instance.LockUnlockPiggy();

                //check if i need to show the Mission unlcok overlay - and if so after its done show all the MES events popups

                bool tableUnlockOverlayShown = Lobby_view.OpenHighestTable(showTableUnlock, goToHighestTable, () =>
                {
                    RequestNewMission();
                });

                if (tableUnlockOverlayShown == false)
                {
                    RequestNewMission();
                }



                // Lobby_view.UpdateLeaderboardLock();
                Lobby_view.UpdateContestsLock();
                Lobby_view.UpdateContestsHats();
                Lobby_view.UpdateVouchers();
                Lobby_view.HandleLobbyChallengeButtonAndClaimIndication();

                LobbyTopController.Instance.ShowTop(true, false, true);
                LobbyTopController.Instance.UpdateCoinsText();

                // Return sleeping time on lobby
                Screen.sleepTimeout = SleepTimeout.SystemSetting;

                if (showTableUnlock && !tableUnlockOverlayShown && !SpadesScreensManager.Instance.IsLeaderboardShown)
                {

                    // IMS event take precedence 
                    if (IMSController.Instance.HasEvent(IMSController.EVENT_BACK_TO_LOBBY))
                    {
                        IMSController.Instance.TriggerEvent(IMSController.EVENT_BACK_TO_LOBBY, true);
                    }
                    else
                    {
                        MESMaterial poMaterial = (MESBase.Instance as SpadesMESController).PO_material;
                        if (poMaterial != null && !(MESBase.Instance as SpadesMESController).IsMaterialExpired(poMaterial) &&
                            !SpadesOverlayManager.Instance.IsOverlayShown)
                        {
                            (PopupManager.Instance as SpadesPopupManager).ShowPromotionPopup(PopupManager.AddMode.DontShowIfPopupShown, poMaterial);
                        }
                    }
                }
                Lobby_view.LobbyButtonsGroups.ActivateContestsButton();
                ContestPanelController.Instance.HidePanel();
            }

            m_hourly_bonus_view.UpdateBonusView();

            Resources.UnloadUnusedAssets();

        }

        private void RequestNewMission()
        {
            bool shouldShowMissionUnlockOverlay = m_lobby_view.ShouldShowMissionUnlockOverlay();

            if (shouldShowMissionUnlockOverlay)
            {
                SpadesOverlayManager.Instance.ShowMissionUnlockedOverlay(() =>
                {
                    //request new challenges
                    MissionController.Instance.RequestMissionIfNeeded(null);
                });
            }
        }

        public int GetWorldIndexFromBuyIn(int buy_in)
        {
            User user = ModelManager.Instance.GetUser();


            foreach (SpadesWorld world in ModelManager.Instance.Worlds)
            {
                //find highest level 
                foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode))
                {
                    // Do not count special tables as highest tables
                    if (singleitem.Is_special)
                        continue;

                    if (singleitem.Buy_in == buy_in)
                    {
                        return world.Index;
                    }

                }

            }
            return 0;
        }

        public ILobbyItem FindHighestOpenLobbyItem()
        {
            User user = ModelManager.Instance.GetUser();
            ILobbyItem result = null;

            foreach (SpadesWorld world in ModelManager.Instance.Worlds)
            {
                if (world.WorldType == WorldTypes.Regular)
                {

                    //find highest level 
                    foreach (SpadesSingleMatch singleitem in world.GetSingleMatches((SpadesModelManager.Instance.Playing_mode)))
                    {
                        // Do not count special tables as highest tables
                        if (singleitem.Is_special)
                            continue;

                        if (result == null)
                        {
                            result = singleitem;
                            continue;
                        }

                        if (singleitem.Min_level >= result.GetLevel() && singleitem.Min_level <= user.GetLevel())
                        {
                            result = singleitem;
                        }

                    }

                    int arena_level = world.GetArena(SpadesModelManager.Instance.Playing_mode).GetLevel();

                    if (arena_level > result.GetLevel() && arena_level <= user.GetLevel())
                        result = world.GetArena(SpadesModelManager.Instance.Playing_mode);
                }
            }

            return result;

        }

        public SpadesSingleMatch FindUnlockedTableByBuyIn(int buy_in, PlayingMode playingMode)
        {
            User user = ModelManager.Instance.GetUser();

            foreach (SpadesWorld world in ModelManager.Instance.Worlds)
            {
                //find highest level 
                foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(playingMode))
                {
                    if (singleitem.Buy_in == buy_in)
                    {
                        if (user.GetLevel() >= singleitem.Min_level)
                            return singleitem;
                    }
                }
            }
            return null;
        }

        public SearchingForPlayersPopup StartSearchingForPlayers(Match match, SearchingForPlayersPopup.SearchingPopupCancelClicked CancelDelegate = null, bool rematch = false, bool show_cancel_button = true, Action searchingDone = null)
        {
            // Stop pulsign of table 1
            m_lobby_view.World_items[0].StopPulsing(0);

            m_cancel_delegate = CancelDelegate;

            GameObject popup = (GameObject)Instantiate(CardGamesPopupManager.Instance.Searching_for_players);

            m_searching_for_players_popup = popup.GetComponent<SearchingForPlayersPopup>();

            m_searching_for_players_popup.Cancel_clicked_delegate = CancelSearching;

            m_searching_for_players_popup.Match = match;

            m_searching_for_players_popup.IsRematch = rematch;

            m_searching_for_players_popup.SearchingPopupDone = searchingDone;

            m_searching_for_players_popup.Show_cancel_button = show_cancel_button;

            // Onboarding experiment
            if (ModelManager.Instance.GetUser().NeverPlayed &&
                (ExperimentsManager.Instance.GetValue(ONBOARDING_EXPERIMENT_ID) == 2 ||
                 ExperimentsManager.Instance.GetValue(ONBOARDING_EXPERIMENT_ID) == 3))
            {
                m_searching_for_players_popup.Show_cancel_button = false;
            }


            PopupManager.Instance.AddPopup(popup, PopupManager.AddMode.ShowAndRemove);




            CancelDelayFakeRoutine();

            m_fake_delay_routine = StartCoroutine(DelayLocalFake(3));



            return m_searching_for_players_popup;

        }

        private void CancelDelayFakeRoutine()
        {
            if (m_fake_delay_routine != null)
                StopCoroutine(m_fake_delay_routine);
        }

        IEnumerator DelayLocalFake(float delay_time)
        {
            yield return new WaitForSecondsRealtime(delay_time);
            new GameCreatedCommand().ExecuteLocalFake();
        }

        private void CancelSearching()
        {
            CancelDelayFakeRoutine();
            SpadesGameServerManager.Instance.Disconnect();
            if (m_cancel_delegate != null)
                m_cancel_delegate();
        }

        private void OnSessionTimeout()
        {
            int time_out = (int)CardGamesCommManager.SESSION_TIMEOUT_SEC / 60;
            string message = "You have been inactive for more than " + time_out + " minutes, please press OK to continue playing";

            m_message_popup = PopupManager.Instance.ShowMessagePopup(message, PopupManager.AddMode.ShowAndRemove, true, () =>
            {
#if UNITY_WEBGL
                refreshBrowser();
#endif
            });
        }

        public void LoginAfterSessionTimeout()
        {
            ShowWelcomeOverlay();
        }

        public bool AreAllTablesLocked(string worldId)
        {
            SpadesWorld world = (SpadesWorld)ModelManager.Instance.GetWorldById(worldId);
            int userLevel = ModelManager.Instance.GetUser().GetLevel();

            foreach (SpadesSingleMatch match in world.GetSingleMatches(PlayingMode.Solo))
            {
                if (!match.Is_special && userLevel > match.GetLevel())
                    return false;
            }
            foreach (SpadesSingleMatch match in world.GetSingleMatches(PlayingMode.Partners))
            {
                if (!match.Is_special && userLevel > match.GetLevel())
                    return false;
            }

            if (userLevel > world.GetArena(PlayingMode.Solo).GetLevel())
                return false;

            if (userLevel > world.GetArena(PlayingMode.Partners).GetLevel())
                return false;

            return true;
        }


        #region User inventory

        public void BuyUserInventoryItem(string itemData, string imsMetadata, int cost)
        {
            // Confirmation according to AB test
            int expVal = 3;// ExperimentsManager.Instance.GetValue(EXPERIMENT_ITEM_CONFIRMATION);
            if (expVal == 1)
            {
                CardGamesPopupManager.Instance.ShowQuestionPopup(PopupManager.AddMode.ShowAndRemove, null,
                     "Do you want to accept this offer?", "accept!", "Maybe later",
                () =>
                {
                    DoBuyUserInventoryItem(itemData, imsMetadata, cost);
                }, () =>
                {
                    // Close the popup
                    PopupManager.Instance.HidePopup();
                });
            }
            else if (expVal == 2)
            {
                SpadesPopupManager.Instance.ShowItemConfirmationPopup(PopupManager.AddMode.ShowAndRemove, false,
                    () =>
                    {
                        DoBuyUserInventoryItem(itemData, imsMetadata, cost);
                    });

            }
            else if (expVal == 3)
            {
                SpadesPopupManager.Instance.ShowItemConfirmationPopup(PopupManager.AddMode.ShowAndRemove, true,
                   () =>
                   {
                       DoBuyUserInventoryItem(itemData, imsMetadata, cost);
                   });
            }
            else  // No confirmation
            {
                DoBuyUserInventoryItem(itemData, imsMetadata, cost);
            }
        }


        private void DoBuyUserInventoryItem(string itemData, string imsMetadata, int cost)
        {
            // Check for insufficient balance
            int balance = ModelManager.Instance.GetUser().GetCoins();
            if (balance < cost)
            {
                SpadesIMSController.Instance.HandleInsufficientFunds(cost - balance);

                return;
            }

            CardGamesPopupManager.Instance.ShowWaitingIndication(true);

            CardGamesCommManager.Instance.BuyItem(itemData, imsMetadata, (success, itemId, newCoinsBalance) =>
            {
                CardGamesPopupManager.Instance.ShowWaitingIndication(false);
                if (success)
                {
                    // Add the item to the uers's inventory
                    ModelManager.Instance.Inventory.AddItem(itemId);

                    // Update the balance
                    ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);

                    // Remove future popups
                    PopupManager.Instance.RemoveAllPopups();

                    // Make sure the full sets page is shown
                    SpadesStateController.Instance.SetAvatarScreenPanelIndex(1);
                    SpadesStateController.Instance.SetAvatarScreenItemTypeIndex(3);

                    CardGamesPopupManager.Instance.ShowMavatarCreationPopup(PopupManager.AddMode.ShowAndRemove);


                    PopupManager.Instance.ShowMessagePopup(message: "<b>ITEMS PURCHASED!</b>\n\nFind the new items in your inventory", button_text: "Ok");
                }
                else
                {
                    PopupManager.Instance.ShowMessagePopup("Items cannot be purchased right now.");
                }
            });
        }


        #endregion

        private void ShowVideosNotAvailablePopup()
        {
            PopupManager.Instance.ShowMessagePopup("Videos are not available yet.\nPlease try again later.");

            // Track this
            TrackingManager.Instance.LogFirebaseEvent("Videos_not_available");
        }



        public GameObject Lobby_object
        {
            get
            {
                return m_lobby_object;
            }
        }

        public LobbyView Lobby_view
        {
            get
            {
                return m_lobby_view;
            }
        }



        public int Old_threshold
        {
            get
            {
                return m_old_threshold;
            }
            set
            {
                m_old_threshold = value;
            }
        }

        public bool Match_to_focus_is_single
        {
            get
            {
                return m_match_to_focus_is_single;
            }
            set
            {
                m_match_to_focus_is_single = value;
            }
        }

        public int GetOldXP()
        {
            return m_old_xp;
        }

        public void SetOldXP(int xp)
        {
            m_old_xp = xp;
        }

        public int GetOldLevel()
        {
            return m_old_level;
        }

        public void SetOldLevel(int level)
        {
            m_old_level = level;
        }

        public void SetCurrLobbyScreen(int screen_index)
        {
            m_curr_lobby_screen = screen_index;
        }

        public int GetCurrLobbyScreen()
        {
            return m_curr_lobby_screen;
        }

        public bool Need_to_show_update_version
        {
            get
            {
                return m_need_to_show_update_version;
            }
        }

        public SearchingForPlayersPopup Searching_for_players_popup
        {
            get
            {
                return m_searching_for_players_popup;
            }
        }

        public bool ShouldShowOnboardingAfterGame
        {
            get
            {
                return m_shouldShowOnboardingAfterGame;
            }
            set
            {
                m_shouldShowOnboardingAfterGame = value;
            }
        }

        /*   public OnboardingFunnelTracker Onboarding_tracker
           {
               get
               {
                   return m_onboarding_tracker;
               }
           }*/



#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void refreshBrowser();
#endif


    }
}
