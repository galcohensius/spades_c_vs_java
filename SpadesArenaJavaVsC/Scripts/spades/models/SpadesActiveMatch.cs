﻿using System.Collections.Generic;
using System;
using cardGames.models;

namespace spades.models
{
	public class SpadesActiveMatch : ActiveMatch  {

        List<SpadesRoundResults> 		m_rounds_results = new List<SpadesRoundResults>();


        public event Action<SpadesRoundResults> OnScoreChange;


        public SpadesActiveMatch (Match match):base(match)
		{
			
		}
            

        public int GetCalculatedTotalResults(SpadesRoundResults.Positions position,int limit=1000)
		{
			if (position > SpadesRoundResults.Positions.South)
				position -= 4;
			
			int total=  0;
			int counter = 0;

			foreach (SpadesRoundResults results in m_rounds_results)
			{
				counter++;
				if(counter<limit)
					total += results.GetResults (position).Round_Score_With_Penalty;
			}

			return	total;
		}

		public void	AddResults(SpadesRoundResults m_results)
		{
			m_rounds_results.Add(m_results); 

			if (OnScoreChange != null)
				OnScoreChange (m_results);
		}



		public int	GetRoundsCount()
		{
			return m_rounds_results.Count;
		}

		public SpadesRoundResults GetRoundResult(int round_number)
		{
			return	m_rounds_results[round_number-1];
		}

		public SpadesRoundResults GetLastRoundResults()
		{
			return m_rounds_results.FindLast (i=>true);//this return the last child of the list - and it returns null if there is nothing there
		}

        public new ISpadesMatch GetMatch()
        {
            return Match as ISpadesMatch;
        }

		public bool IsSingleMatch
		{
			get{
				return Match.GetType() == typeof(SpadesSingleMatch);
			}
		}


    }

}