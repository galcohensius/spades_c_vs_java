﻿using System;
using spades.utils;
using cardGames.models;

namespace spades.models
{
    /// <summary>
    /// Manages the player and holds a reference to the cards models.
    /// </summary>
    public class Player
    {

        public enum PlayerType
        {
            Human,
            Bot,
            Zombie
        }

//        SpadesUser m_user;
        PlayerType m_player_type;
        SpadesTable m_table;
        int m_tablepos;

        int m_bids;
        int m_takes;
        bool m_blind_nil;

        int m_turns_played_as_zombie = 0;

        CardsList m_cards = new CardsList();

        bool m_master = false;

        public event Action<int> OnBidsChange;
        public event Action<int> OnTakesChange;
        public event Action<int> OnCardRemoved;
        public event Action<int> OnTypeChanged;

		public event Action<bool> OnMasterChanged;


        public Player(SpadesTable table, int tablepos, PlayerType player_type)//, SpadesUser user)
        {
            m_table = table;
            m_tablepos = tablepos;
            m_player_type = player_type;
//            m_user = user;
        }


        public CardsList GetCards()
        {
            return m_cards;
        }

        public void SetCards(CardsList cards)
        {
            m_cards = cards;
        }


        public Card GetCard(int index)
        {
            return m_cards[index];
        }


        public void DeleteCard(int index)
        {
            m_cards.RemoveAt(index);
            OnCardRemoved(index);
        }

        public void DeleteCard(Card card)
        {
            m_cards.Remove(card);
        }


        public void DeleteAllCards()
        {
            m_cards.Clear();
        }

//        public MAvatarModel GetMAvatarModel()
//        {
//            return m_user.GetMAvatar();
//        }

        public void SetBlind(bool blind)
        {
            m_blind_nil = blind;
        }

        public bool GetBlind()
        {
            return m_blind_nil;
        }


        public int GetTablePos()
        {
            return m_tablepos;
        }

        public int GetBids()
        {
            return m_bids;
        }

        /// <summary>
        /// Returns the player's bids as a string.
        /// Blind nil is represented as 00
        /// </summary>
        public string GetBidsStr()
        {
            if (m_blind_nil)
                return "00";
            return m_bids.ToString();
        }

        public void SetBids(int bids)
        {
            m_bids = bids;
            if (OnBidsChange != null)
                OnBidsChange(m_bids);
        }


        public int GetTakes()
        {
            return m_takes;
        }

        /// <summary>
        /// Increments the takes by 1
        /// </summary>
        public void IncrementTakes()
        {
            m_takes++;
            if (OnTakesChange != null)
                OnTakesChange(m_takes);
        }

        public void SetTakes(int takes)
        {
            m_takes = takes;
            if (OnTakesChange != null)
                OnTakesChange(m_takes);
        }

        /// <summary>
        /// Sorts the card in hand by suit and value
        /// </summary>
        public void SortCardInHand()
        {
            m_cards.Sort(new HandComparerHighToLow());

        }

//        public string GetName()
//        {
//            return m_user.GetMAvatar().NickName;
//        }


        public int ServerPos
        {
            get
            {
                return m_table.LocalToServerPos(m_tablepos);
            }
        }


//        public SpadesUser User
//        {
//            get
//            {
//                return m_user;
//            }
//            set
//            {
//                m_user = value;
//            }
//        }

        public bool Master
        {
            get
            {
                return m_master;
            }
            set
            {
                m_master = value;
				if (OnMasterChanged != null)
					OnMasterChanged (m_master);

            }
        }

        public PlayerType Player_Type
        {
            get
            {
                return m_player_type;
            }
            set
            {
                m_player_type = value;
                if (OnTypeChanged != null)
                    OnTypeChanged((int)m_player_type);
            }
        }

        public int Turns_played_as_zombie
        {
            get
            {
                return m_turns_played_as_zombie;
            }
            set
            {
                m_turns_played_as_zombie = value;

            }
        }


    }


}