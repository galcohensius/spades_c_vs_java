﻿using System.Collections.Generic;
using cardGames.models;

namespace spades.models
{

	public class SpadesRoundResults : RoundResults
	{


		public enum Positions
		{
			West,
			North,
			East,
			South,
			West_East,
			North_South
		}


		

		private PlayingMode mode;

		PlayerRoundResults[] players_round_results;

		
        public SpadesRoundResults (PlayerRoundResults[] players_round_results, int round_number)
		{
			this.players_round_results = players_round_results;
			m_round_number = round_number;
		}

		public SpadesRoundResults (PlayingMode mode)
		{
			this.mode = mode;
			if (mode == PlayingMode.Partners) {
				players_round_results = new PlayerRoundResults[2];
			} else {
				players_round_results = new PlayerRoundResults[4];
			}
		}



	
		public void SetResults (Positions position, PlayerRoundResults players_results)
		{
			if (position > Positions.South)
				position -= 4;

			players_round_results [(int)position] = players_results;
		}



		public PlayerRoundResults GetResults (Positions position)
		{
			if (position > Positions.South)
				position -= 4;

			return players_round_results [(int)position]; 
		}

		public bool	HasResults ()
		{
			return players_round_results [0] != null;
		}

		public List<Positions> GetHighestPointsPositions() {
			List<Positions> result = new List<Positions> ();

			int highestPoints = -10000;
			if (mode == PlayingMode.Solo) {
				for (int pos = 0; pos < 4; pos++) {
					if (players_round_results [pos].Round_Score_With_Penalty > highestPoints)
						highestPoints = players_round_results [pos].Round_Score_With_Penalty;
				}
				for (int pos = 0; pos < 4; pos++) {
					if (players_round_results [pos].Round_Score_With_Penalty == highestPoints)
						result.Add ((Positions)pos);
				}
			} else {
				for (int pos = 0; pos < 2; pos++) {
					if (players_round_results [pos].Round_Score_With_Penalty > highestPoints)
						highestPoints = players_round_results [pos].Round_Score_With_Penalty;
				}
				for (int pos = 0; pos < 2; pos++) {
					if (players_round_results [pos].Round_Score_With_Penalty == highestPoints)
						result.Add ((Positions)(pos+4));
				}
			}

			return result;
		}

        public PlayerRoundResults[] Players_round_results
        {
            get
            {
                return players_round_results;
            }
        }


	}

}