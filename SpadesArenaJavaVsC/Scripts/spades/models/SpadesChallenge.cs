﻿using spades.controllers;

namespace spades.models
{
    public class SpadesChallenge : Challenge
    {
        SpadesMissionController.ChallengeType     m_ch_type;
        SpadesTableFilterData m_spadesTableFilterData = new SpadesTableFilterData();

        public SpadesChallenge(SpadesChallenge challenge) : base(challenge)
        {
            m_ch_type = challenge.m_ch_type;
            m_spadesTableFilterData = challenge.m_spadesTableFilterData;
        }

        public SpadesChallenge(int order_id, int progress, int prize, MissionController.ChallengeStatus status) : base(order_id, progress, prize, status)//for update
        {
        }

        public SpadesChallenge(SpadesMissionController.ChallengeType type, MissionController.ChallengeStatus status, int[] thresholds, int order, int prize, int num_swapped,int bet, MissionController.BetOperator betOperator,
           int progress) : base(status, thresholds, order, prize, num_swapped, bet, betOperator, progress)
        {
            m_ch_type = type;
        }

        public SpadesChallenge(Challenge challenge, SpadesMissionController.ChallengeType type, SpadesTableFilterData spadesTableFilterData)
        {
            m_status = challenge.Status;
            m_thresholds = challenge.Thresholds;
            m_order = challenge.Order;
            m_prize = challenge.Prize;
            m_progress = challenge.Progress;
            m_swap_left = challenge.Swap_left;
            m_bet = challenge.Bet;
            m_bet_operator = challenge.Bet_operator;
            m_ch_type = type;
            m_spadesTableFilterData = spadesTableFilterData;
        }

        public void CopyStaticData(SpadesChallenge org_challenge)
        {
            m_ch_type = org_challenge.m_ch_type;
            m_thresholds = org_challenge.m_thresholds;
            m_swap_left = org_challenge.Swap_left;
            m_bet = org_challenge.m_bet;
            m_bet_operator = org_challenge.Bet_operator;
            m_spadesTableFilterData = org_challenge.SpadesTableFilterData;
            m_prize = org_challenge.Prize;
        }

        public override void CopyStaticData(Challenge org_challenge)
        {
            base.CopyStaticData(org_challenge);
            m_ch_type = ((SpadesChallenge)org_challenge).m_ch_type;
            m_spadesTableFilterData = ((SpadesChallenge)org_challenge).SpadesTableFilterData;
        }


        public SpadesMissionController.ChallengeType Type
        {
            get
            {
                return Ch_type;
            }

        }
   

        public SpadesMissionController.ChallengeType Ch_type
        {
            get
            {
                return m_ch_type;
            }
            
        }

        public override int TotalProgress
        {
            get
            {
                // TODO: WTF?!?!?!?!?!?!
                // Remove the specific type reference (RAN 3/12/2019)
                if (m_ch_type == SpadesMissionController.ChallengeType.Take_X_Tricks_Y_Times)
                    return m_thresholds[1];
                else
                    return m_thresholds[0];
            }
        }

        public SpadesTableFilterData SpadesTableFilterData { get => m_spadesTableFilterData; set => m_spadesTableFilterData = value; }
    }
}


