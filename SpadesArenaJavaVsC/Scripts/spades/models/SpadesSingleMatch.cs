﻿using cardGames.models;

namespace spades.models
{
    public class SpadesSingleMatch : SingleMatch, ISpadesMatch
    {
        public int HighPoints { get ; set; }
        public int LowPoints { get; set; }
        public int BagsPanelty { get; set; }
        public int BagsCount { get; set; }
        public float BidDelay { get; set; }
        public PlayingMode Playing_mode { get ; set; }
        public PlayingVariant Variant { get; set ; }
        public PlayingSubVariant SubVariant { get ; set; }
        public BotsData BotsData { get; set; }
        public int Coins4PointsFactor { get; set; }
    }
}