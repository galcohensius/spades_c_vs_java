﻿namespace spades.models
{
    public class SpadesArenaMatch : ArenaMatch, ISpadesMatch
    {
        public int HighPoints { get; set; }
        public int LowPoints { get; set; }
        public int BagsPanelty { get; set; }
        public int BagsCount { get; set; }
        public float BidDelay { get; set; }
        public PlayingMode Playing_mode { get; set; }
        public PlayingVariant Variant { get; set; }
        public PlayingSubVariant SubVariant { get; set; }
        public BotsData BotsData { get; set; }
        public int Coins4PointsFactor { get; set; }

        public SpadesArenaMatch(int high_points, int low_points)
        {
            HighPoints = high_points;
            LowPoints = low_points;
        }

        public SpadesArenaMatch(SpadesArenaMatch arena_match)
        {
            HighPoints = arena_match.HighPoints;
            LowPoints = arena_match.LowPoints;
            BidDelay = arena_match.BidDelay;
        }

        public SpadesArenaMatch(int high_points, int low_points, float bid_delay, float turn_delay, int pay_out_coins)
        {
            HighPoints = high_points;
            LowPoints = low_points;
            BidDelay = bid_delay;
//            PlayDelay = turn_delay;
//            Payout_coins = pay_out_coins;

        }

        public SpadesArenaMatch()
        {
        }

        public override Arena Arena { get => base.Arena as SpadesArena; set => base.Arena = value; }
    }
}