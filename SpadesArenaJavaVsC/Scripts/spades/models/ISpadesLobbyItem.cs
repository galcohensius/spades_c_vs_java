﻿namespace spades.models
{
    public interface ISpadesLobbyItem : ILobbyItem
    {
        PlayingMode Playing_mode
        {
            get;
        }

        PlayingVariant Variant
        {
            get;
        }
    }
}