﻿namespace spades.models
{

    public class SpadesUserStats : UserStats
    {
        // SpadesSingleMatch related
        int matchWinsSolo;
        int matchLosesSolo;

        int matchWinsPartner;
        int matchLosesPartner;

        int exactBids;
        int greaterBids;
        int lesserBids;
        int nilBids;
        int nilTakes;

        public float MatchWinRatioSolo
        {
            get
            {
                if (MatchPlayedSolo == 0)
                    return 0;
                return (float)matchWinsSolo / MatchPlayedSolo;
            }
        }

        public int MatchPlayedSolo
        {
            get
            {
                return matchWinsSolo + matchLosesSolo;
            }

        }

        public int MatchWinsSolo
        {
            get
            {
                return this.matchWinsSolo;
            }
            set
            {
                matchWinsSolo = value;
            }
        }

        public int MatchLosesSolo
        {
            get
            {
                return this.matchLosesSolo;
            }
            set
            {
                matchLosesSolo = value;
            }
        }


        public int ExactBids
        {
            get
            {
                return this.exactBids;
            }
            set
            {
                exactBids = value;
            }
        }

        public float ExactBidsRatio
        {
            get
            {
                if (roundPlayed == 0)
                    return 0;
                return (float)exactBids / roundPlayed;
            }
        }

        public int GreaterBids
        {
            get
            {
                return this.greaterBids;
            }
            set
            {
                greaterBids = value;
            }
        }

        public int LesserBids
        {
            get
            {
                return this.lesserBids;
            }
            set
            {
                lesserBids = value;
            }
        }

        public int NilBids
        {
            get
            {
                return this.nilBids;
            }
            set
            {
                nilBids = value;
            }
        }

        public int NilTakes
        {
            get
            {
                return this.nilTakes;
            }
            set
            {
                nilTakes = value;
            }
        }

        public float NilTakesRatio
        {
            get
            {
                if (nilBids == 0)
                    return 0;
                return (float)nilTakes / nilBids;
            }
        }

        public float MatchWinRatioPartner
        {
            get
            {
                if (MatchPlayedPartner == 0)
                    return 0;
                return (float)matchWinsPartner / MatchPlayedPartner;
            }
        }

        public int MatchPlayedPartner
        {
            get
            {
                return matchWinsPartner + matchLosesPartner;
            }

        }

        public int MatchWinsPartner
        {
            get
            {
                return matchWinsPartner;
            }
            set
            {
                matchWinsPartner = value;
            }
        }

        public int MatchLosesPartner
        {
            get
            {
                return matchLosesPartner;
            }
            set
            {
                matchLosesPartner = value;
            }
        }

        public int TotalMatchesPlayed
        {
            get
            {
                return matchLosesPartner + matchLosesSolo +
                    matchWinsPartner + matchWinsSolo;
            }

        }

    }
}