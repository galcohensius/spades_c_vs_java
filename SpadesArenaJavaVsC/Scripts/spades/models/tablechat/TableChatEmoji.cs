namespace spades.models.tablechat
{
    [CreateAssetMenu]
    public class TableChatEmoji : ScriptableObject
    {
        [SerializeField] int MessageId;
        [SerializeField] Sprite m_emoji=null;

        public int ChatMessageId
        {
            get
            {
                return MessageId;
            }

            set
            {
                MessageId = value;
            }
        }

        public Sprite ChatMessageEmoji
        {
            get
            {
                return m_emoji;
            }

            set
            {
                m_emoji = value;
            }
        }

    }
}
