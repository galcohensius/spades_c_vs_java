using System.Collections.Generic;

namespace spades.models.tablechat
{
    [CreateAssetMenu]
    public class TableChatModel : ScriptableObject
    {
        [SerializeField] string type;
        [SerializeField] TableChatEmoji[] ChatEmoji;

        public enum SOLO_MESSAGE_ID
        {
            HELLO=1,
            GOOD_LUCK=2,
            THANKS=3,
            WOW=4,
            SURPRIZE=5,
            LOL=6,
            ZZZ=7,
            FCK=8,
            WELL_PLAYED=9,
            ALMOST_HAD=10,
            NICE_MOVE=11,
            SHOULD_HAVE=12,
            DONE=13,
            OOPS=14
        }

        public enum PARTNER_MESSAGE_ID
        {
            HELLO = 1,
            GOOD_LUCK = 2,
            THANKS = 3,
            WOW = 4,
            SURPRIZE = 5,
            LOL = 6,
            ZZZ = 7,
            FCK = 8,
            WELL_PLAYED = 9,
            ALMOST_HAD = 10,
            WHAT_ARE_U = 15,
            KIDDING = 16,
            GOT_IT = 17,
            GOOD_JOB = 18
        }

        private Dictionary<int, string> m_solo_texts = new Dictionary<int, string>() {
            { 1,"Hello!" },
            { 2,"Good luck" },
            {3,"Thanks" },
            {4,"WOW" },
            {5,"Surprise!" },
            {6,"LOL" },
            {7,"Oops..." },
            {8,"I'm done" },
            {9,"Well played!" },
            {10,"Almost had it" },
            {11,"Nice move" },
            {12,"I should have won" }
        };
        private Dictionary<int, string> m_partners_texts = new Dictionary<int, string>(){
            { 1,"Hello!" },
            { 2,"Good luck" },
            {3,"Thanks" },
            {4,"WOW" },
            {5,"Surprise!" },
            {6,"LOL" },
            {7,"Good cover" },
            {8,"Way to go, partner!" },
            {9,"Well played" },
            {10,"Almost had it" },
            {15,"What are you doing?!" },
            {16,"Are you kidding me?!" }
        };

        private Dictionary<int, int> m_userChatSoloToResponseTrigger = new Dictionary<int, int>()
        {
            {9,200},
            {1,201},
            {2,202},
            {5,206},
            {12,207}
        };

        private Dictionary<int, int> m_userChatPartnerToResponseTrigger = new Dictionary<int, int>()
        {
            {9,200},
            {1,201},
            {2,202},
            {5,206},
            {15,203},
            {7,204},
            {8,205}
        };

        private int[] ChatTextFavorites = new int[0]; // Should be initialized with an empty array
        private int[] ChatEmojiFavorites = new int[0];

        public Dictionary<int,string> ChatTextMessages
        {
            get
            {
                if (type == "Partners")
                    return m_partners_texts;
                else
                    return m_solo_texts;
            }
        }

        public TableChatEmoji[] ChatEmojiMessages
        {
            get
            {
                return ChatEmoji;
            }
        }

        public string FindMessageById(int id)
        {
            string retVal = "";

            if (type == "Partners")
            {
                m_partners_texts.TryGetValue(id, out retVal);
            }

            if (type == "Solo")
            {
                m_solo_texts.TryGetValue(id,out retVal);
            }

            return retVal;
        }

        public Sprite FindEmojiById(int id)
        {
            Sprite retVal = null;

            for (int i = 0; i < ChatEmoji.Length; i++)
            {
                if (ChatEmoji[i].ChatMessageId == id)
                {
                    retVal = ChatEmoji[i].ChatMessageEmoji;
                    break;
                }
            }

            return retVal;
        }

        public int[] ChatTextMessageFavorites
        {
            get
            {
                return ChatTextFavorites;
            }
            set
            {
                ChatTextFavorites = value;
            }
        }

        public int[] ChatEmojiMessageFavorites
        {
            get
            {
                return ChatEmojiFavorites;
            }
            set
            {
                ChatEmojiFavorites = value;
            }
        }

        // User Chat ID To AI Response Trigger Integration
        public int GetResponseTriggerForUserChat(int id, PlayingMode mode)
        {
            int triggerID = -1;

            if (mode == PlayingMode.Partners)
            {
                if (m_userChatPartnerToResponseTrigger.ContainsKey(id))
                {
                    triggerID = m_userChatPartnerToResponseTrigger[id];
                }
            }
            else
            {
                if (m_userChatSoloToResponseTrigger.ContainsKey(id))
                {
                    triggerID = m_userChatSoloToResponseTrigger[id];
                }
            }

            return triggerID;
        }
    }
}
