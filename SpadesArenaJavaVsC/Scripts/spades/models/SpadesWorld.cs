﻿using System.Collections.Generic;
using System;

namespace spades.models
{
    /// <summary>
    /// SpadesWorld implementation of World for Spades. same class name could be re-used for Gin under Gin namespace.
    /// </summary>
	public class SpadesWorld : World
	{
		Dictionary<PlayingMode,List<SpadesSingleMatch>>	m_single_matches = new Dictionary<PlayingMode,List<SpadesSingleMatch>>();	
		Dictionary<PlayingMode, SpadesArena> m_arenas = new Dictionary<PlayingMode, SpadesArena> ();
		Dictionary<string, SpadesArena> m_arenas_by_id = new Dictionary<string, SpadesArena>();

		public override int LowestUnlockLevel => throw new NotImplementedException();

        public SpadesWorld() : base()
        {
		}

		

		public void AddSingleMatch(SpadesSingleMatch single_match)
		{
			List<SpadesSingleMatch> curr_matches = new List<SpadesSingleMatch>();

			m_single_matches.TryGetValue (single_match.Playing_mode, out curr_matches);

			if (curr_matches != null)
				curr_matches.Add (single_match);
			else
			{
				curr_matches = new List<SpadesSingleMatch> ();
				curr_matches.Add (single_match);
			}
			
			m_single_matches [single_match.Playing_mode] = curr_matches;
		}

		public int GetSingleMatchCount()
		{
			return m_single_matches.Count;
		}



		public List<SpadesSingleMatch> GetSingleMatches(PlayingMode pm)
		{
			List<SpadesSingleMatch> single_matches = null;
            if (pm == PlayingMode.Both)
                pm = PlayingMode.Partners;

			m_single_matches.TryGetValue (pm, out single_matches);

			return single_matches;
		}

		public void AddArena(SpadesArena arena)
		{
			// Store the first arenas by playign mode
			if (!m_arenas.ContainsKey(arena.Playing_mode))
				m_arenas.Add(arena.Playing_mode, arena);

			// Store all arenas by id
			m_arenas_by_id.Add(arena.GetID(), arena);
		}

		public SpadesArena GetArena(PlayingMode pm)
		{
			SpadesArena arena = null;
			m_arenas.TryGetValue(pm, out arena);
			return arena;
		}

		public override Arena GetArenaByID(string id)
        {
			SpadesArena result = null;
			m_arenas_by_id.TryGetValue(id, out result);
			return result;
        }
    }
}