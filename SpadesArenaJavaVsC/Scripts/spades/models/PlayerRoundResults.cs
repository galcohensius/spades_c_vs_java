﻿namespace spades.models
{

	public class PlayerRoundResults
	{

		int	m_bids;
		int	m_takes;
		int	m_bags_round;
        int	m_round_score;
		int m_bags_total;
		int	m_bags_penalty;
		int	m_nil_bonus_or_penalty;
		int m_round_score_with_penalty;

	
		public PlayerRoundResults ()
		{
			m_bids = 0;
			m_takes = 0;
			m_bags_round = 0;
			m_round_score = 0;
			m_bags_total = 0;
			m_bags_penalty = 0;
			m_nil_bonus_or_penalty = 0;
			m_round_score_with_penalty = 0;

		}

		public PlayerRoundResults (int score,int bids, int takes)
		{
			m_round_score = score;
			m_round_score_with_penalty = score;
            m_bids = bids;
            m_takes = takes;
		}



		public int Bids {
			get {
				return m_bids;
			}
			set {
				m_bids = value;
			}
		}

		public int Takes {
			get {
				return m_takes;
			}
			set {
				m_takes = value;
			}
		}

		public int Bags_round {
			get {
				return m_bags_round;
			}
			set {
				m_bags_round = value;
			}
		}

		public int Round_score {
			get {
				return m_round_score;
			}
			set {
				m_round_score = value;
			}
		}

		public int Bags_total {
			get {
				return m_bags_total;
			}
			set {
				m_bags_total = value;
			}
		}

		public int Bags_penalty {
			get {
				return m_bags_penalty;
			}
			set {
				m_bags_penalty = value;
			}
		}

		public int Nil_bonus_or_penalty {
			get {
				return m_nil_bonus_or_penalty;
			}
			set {
				m_nil_bonus_or_penalty = value;
			}
		}

		public int Round_Score_With_Penalty {
			get {
				return m_round_score_with_penalty;
			}
			set { 
				m_round_score_with_penalty = value;
			}
		}


	}
}