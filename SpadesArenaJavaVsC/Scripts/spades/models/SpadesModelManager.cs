namespace spades.models
{
    public enum PlayingMode
    {
        Solo,
        Partners,
        Both
    }

    public enum PlayingVariant
    {
        Classic,
        Jokers,
        Mirror,
        Whiz,
        Suicide,
        Royale,
        CallBreak
    }

    public enum PlayingSubVariant
    {
        Regular,
        SingleRound,
        FastLane,
        GoBig,
        Jackpot,
        Turbo,
        BlindNil,
        NoBags,
        NoNil,
        Coins4Points,
        HiLo
    }

    /// <summary>
    /// Manages all the models and holds a reference to the parent Table model. Has Spades specific models and data.
    /// </summary>
//    public class SpadesModelManager : ModelManager
//    {
//        Dictionary<string, SpadesSingleMatch> m_cached_single_matches = new Dictionary<string, SpadesSingleMatch>(); // Holds single match data that was received on demand       
//
//        PlayingMode m_playing_mode;
//        PlayingMode m_initial_playing_mode;
//        PlayingVariant m_playing_variation;
//
//        public static new SpadesModelManager Instance;
//
//        
//        protected override void Awake()
//        {
//            base.Awake();
//            Instance = this;
//        }
//
//        private void OnDisable()
//        {
//            if (Instance.GetUser() != null)
//                LeaderboardItemModel.OnSetUserID -= Instance.GetUser().GetID;
//        }
//
//
//        public void SetMission(SpadesMission mission)
//        {
//            m_mission = mission;
//        }
//
//        public override void SetUser(User user)
//        {
//            m_user = (SpadesUser)user;
//            m_leaderboards = new LeaderboardsModel(m_user);
//            LeaderboardItemModel.OnSetUserID += Instance.GetUser().GetID;
//        }
//
//        public void SetUser(SpadesUser user)
//        {
//            m_user = user;
//            m_leaderboards = new LeaderboardsModel(m_user);
//            LeaderboardItemModel.OnSetUserID += Instance.GetUser().GetID;
//        }
//
//        public void AddActiveArena(SpadesActiveArena active_arena)
//        {
//            string temp_key = active_arena.GetArena().GetID();
//            m_active_arenas[temp_key] = active_arena;
//        }
//
//
//        public override SingleMatch GetSingleMatch(string match_id)
//        {
//            foreach (SpadesWorld world in m_worlds)
//            {
//                foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(PlayingMode.Partners))
//                {
//                    if (singleitem.GetID() == match_id)
//                    {
//                        return singleitem;
//                    }
//                }
//
//                foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(PlayingMode.Solo))
//                {
//                    if (singleitem.GetID() == match_id)
//                    {
//                        return singleitem;
//                    }
//                }
//            }
//
//            return null;
//        }
//
//        public override Arena GetArena(string arena_id)
//        {
//            Arena result = null;
//
//            foreach (SpadesWorld world in m_worlds)
//            {
//                if (world.WorldType != World.WorldTypes.Promo)
//                {
//                    result = world.GetArenaByID(arena_id);
//                    if (result != null)
//                        break;
//                }
//            }
//            return result;
//        }
//
//        public PlayingMode Playing_mode
//        {
//            get
//            {
//                return m_playing_mode;
//            }
//            set
//            {
//                m_playing_mode = value;
//
//                if (m_On_Mode_Changed != null)
//                    m_On_Mode_Changed();
//
//            }
//        }
//
//
//
//        public PlayingMode Initial_playing_mode
//        {
//            get
//            {
//                return m_initial_playing_mode;
//            }
//            set
//            {
//                m_initial_playing_mode = value;
//            }
//        }
//
//        public Dictionary<string, SpadesSingleMatch> Cached_single_matches
//        {
//            get => m_cached_single_matches;
//            set => m_cached_single_matches = value;
//        }
//
//        public new SpadesActiveArena GetActiveArena(string arena_id)
//        {
//            if (m_active_arenas.ContainsKey(arena_id))
//                return m_active_arenas[arena_id] as SpadesActiveArena;
//            else
//                return null;
//        }
//
//        public new SpadesActiveMatch GetActiveMatch()
//        {
//            return m_active_match as SpadesActiveMatch;
//        }
//
//        public SpadesWorld GetWorld(string key)
//        {
//            return base.GetWorldById(key) as SpadesWorld;
//        }
//    }

}
