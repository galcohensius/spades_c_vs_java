﻿using System;

namespace spades.models
{
    /// <summary>
    /// Spades with Spades' Challenges implementation.
    /// </summary>
    public class SpadesMission : Mission
    {
        /// <summary>
        /// don't use this. used only to promote base class to this class.
        /// </summary>
        public SpadesMission()
        {

        }

        public void AddChallenge(SpadesChallenge item)
        {
            m_challenges.Add(item);
        }

        public SpadesMission(DateTime dateTime) : base(dateTime)
        {

        }

        public void SetChallenge(int index, SpadesChallenge challenge)
        {
            m_challenges[index] = challenge;
        }
    }
}