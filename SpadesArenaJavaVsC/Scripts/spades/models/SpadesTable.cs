﻿using cardGames.models;

namespace spades.models
{
	/// <summary>
	/// Manages the table and holds a reference to the players models, to the thrown on the table cards and to all the bids.
	/// </summary>
	public class SpadesTable : Table
	{
		
		Player[] m_players = new Player[4];

		bool m_spades_broken = false;
		Card.SuitType? m_leading_suit=null;


		public override int ServerToLocalPos (int server_position)
		{
			return ((server_position - m_server_to_local_pos + 4) % 4);
		}

		public override int LocalToServerPos (int local_position)
		{
			return ((local_position + m_server_to_local_pos + 4) % 4);
		}

		public Card.SuitType? 	GetLeadingSuit ()
		{
			return	m_leading_suit;
		}

		public void SetLeadingSuit (Card.SuitType? suit)
		{
			m_leading_suit = suit;
		}

		public bool	GetSpadesBroken ()
		{
			return	m_spades_broken;
		}

		/// <summary>
		/// Sets the spades broken flag
		/// </summary>
		/// <param name="set_to">If set to <c>true</c> set to.</param>
		public void SetSpadesBroken (bool set_to)
		{
			m_spades_broken = set_to;
		}

	

		/// <summary>
		/// Deletes all player cards - used for restart
		/// </summary>
		public void DeleteAllPlayerCards ()
		{
			for (int i = 0; i < m_players.Length; i++)
				m_players [i].DeleteAllCards ();
		}

		public void SetPlayer (int index, Player player)
		{
			m_players [index] = player;
		}


		public Player GetPlayer (int index)
		{
			return m_players [index];
		}

		/// <summary>
		/// Adds the thrown card to table model - currently not trigerring the change in the table view as there are many values to pass as for position and 
		/// rotation - will be revised once animation is complete
		/// </summary>
		/// <param name="tablePos">SpadesTable position.</param>
		/// <param name="card">Card.</param>
		public void AddThrownCardToTable (int tablePos, Card card)
		{
			m_thrown_cards [tablePos] = card;
		}

        public void ResetThrownCardsList()
        {
            m_thrown_cards = new CardsList() { null, null, null, null };
        }

		/// <summary> WILL BE CONVERTED TO EVENT 
		/// send the message to Sorts the player cards.
		/// </summary>
		public override void SortPlayerCards ()	//sorts the cards in the players hand by suit and order
		{
			for (int i = 0; i < m_players.Length; i++)
				m_players [i].SortCardInHand ();
		}

		public void ClearAllBidsAndTakes ()
		{
			for (int i = 0; i < 4; i++) {
				m_players [i].SetBids (-1);
				m_players [i].SetTakes (0);
				m_players [i].SetBlind (false);
			}
		}


		public Player[] Players {
			get {
				return m_players;
			}
		}

	}

}