﻿using System.Collections.Generic;

namespace spades.models
{
    public class SpadesTableFilterData : TableFilterData
    {
        HashSet<PlayingVariant>      m_playingVariants = new HashSet<PlayingVariant>();
        HashSet<PlayingSubVariant>   m_playingSubVariants = new HashSet<PlayingSubVariant>();
        PlayingMode m_playingMode;

        public HashSet<PlayingVariant> PlayingVariants { get => m_playingVariants; set => m_playingVariants = value; }
        public HashSet<PlayingSubVariant> PlayingSubVariants { get => m_playingSubVariants; set => m_playingSubVariants = value; }
        public PlayingMode PlayingMode { get => m_playingMode; set => m_playingMode = value; }

        public bool IsVariantContained(PlayingVariant playingVariant)
        {
            if (m_playingVariants.Count == 0)
                return true;

            foreach (PlayingVariant variant in m_playingVariants)
            {
                if (playingVariant == variant)
                    return true;
            }
            return false;
        }

        public bool IsSubVariantContained(PlayingSubVariant playingSubVariant)
        {
            if (m_playingSubVariants.Count == 0)
                return true;

            foreach (PlayingSubVariant subVariant in m_playingSubVariants)
            {
                if (playingSubVariant == subVariant)
                    return true;
            }
            return false;
        }

        public bool IsPlayingModeContained(PlayingMode playingMode)
        {
            if (m_playingMode == PlayingMode.Both || playingMode == m_playingMode)
                return true;

            return false;
        }

        public override bool Equals(object obj)
        {
            SpadesTableFilterData other = (SpadesTableFilterData)obj;

            return base.Equals(obj) &&
                other.m_playingVariants.SetEquals(m_playingVariants) &&
                other.m_playingSubVariants.SetEquals(m_playingSubVariants) &&
                other.m_playingMode == m_playingMode;
        }

        public override int GetHashCode()
        {
            var hashCode = 307887542;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<HashSet<PlayingVariant>>.Default.GetHashCode(m_playingVariants);
            hashCode = hashCode * -1521134295 + EqualityComparer<HashSet<PlayingSubVariant>>.Default.GetHashCode(m_playingSubVariants);
            hashCode = hashCode * -1521134295 + m_playingMode.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(SpadesTableFilterData c1, SpadesTableFilterData c2)
        {
            return c1.Equals(c2);
        }
        public static bool operator !=(SpadesTableFilterData c1, SpadesTableFilterData c2)
        {
            return !c1.Equals(c2);
        }
    }
}



