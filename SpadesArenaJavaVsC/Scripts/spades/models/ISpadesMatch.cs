﻿namespace spades.models
{
    public interface ISpadesMatch
    {
        int HighPoints { get; set; }
        int LowPoints { get; set; }

        int BagsPanelty { get; set; }
        int BagsCount { get; set; }

        float BidDelay { get; set; }
        float PlayDelay { get; set; }
        PlayingMode Playing_mode { get; set; }

        PlayingVariant Variant { get; set; }

        PlayingSubVariant SubVariant { get; set; }

        BotsData BotsData { get; set; }

        int Coins4PointsFactor { get; set; }
        int Buy_in { get; set; }


    }
}