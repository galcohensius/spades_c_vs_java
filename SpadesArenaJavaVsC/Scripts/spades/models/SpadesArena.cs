namespace spades.models
{


	public class SpadesArena : Arena, ISpadesLobbyItem
    {
        public enum SpadesArenaType { Regular, Master, Jokers};

		PlayingMode		m_playing_mode;
	    PlayingVariant m_variant;
        PlayingSubVariant m_sub_variant;
		SpadesArenaType m_arenaType;
     

		public int GetAccumulatedPayoutCoinsByStage(int stage) {
			int result = 0;
			for (int i = 1; i <= stage; i++)
			{
				result += GetArenaMatch (i).GetPayOutCoins ();
			}
			return result;
		}

		public int GetPayoutCoinsByStage(int stage) 
		{
			return GetArenaMatch (stage).GetPayOutCoins ();
		}

		public PlayingMode Playing_mode {
			get {
				return m_playing_mode;
			}
			set{ 
				m_playing_mode = value;
			}
		}

	    public PlayingVariant Variant
        {
	        get
	        {
	            return m_variant;
	        }
	        set
	        {
	            m_variant = value;
	        }
	    }

        public PlayingSubVariant SubVariant
        {
            get
            {
                return m_sub_variant;
            }
            set
            {
                m_sub_variant = value;
            }
        }

        public SpadesArenaType ArenaType { get => m_arenaType; set => m_arenaType = value; }
    }
}
