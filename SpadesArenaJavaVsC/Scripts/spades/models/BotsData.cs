﻿using System.Collections.Generic;

namespace spades.models
{


    public class BotsData
    {
        int switchCardsNum;
        int roundSwitchCardsRatio;
        string switchType;


        Dictionary<int, BotTypeAndSkill> botTypes = new Dictionary<int, BotTypeAndSkill>();

        public int SwitchCardsNum
        {
            get
            {
                return switchCardsNum;
            }

            set
            {
                switchCardsNum = value;
            }
        }



        public int RoundSwitchCardsRatio
        {
            get
            {
                return roundSwitchCardsRatio;
            }

            set
            {
                roundSwitchCardsRatio = value;
            }
        }

        public Dictionary<int, BotTypeAndSkill> BotTypes
        {
            get
            {
                return botTypes;
            }
        }

        public string SwitchType
        {
            get
            {
                return switchType;
            }

            set
            {
                switchType = value;
            }
        }

        public struct BotTypeAndSkill
        {
            public string type;
            public int skill;
        }
    }
}