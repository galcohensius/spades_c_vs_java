﻿namespace spades.models
{

	public class SpadesActiveArena : ActiveArena 
	{
		//public SpadesActiveArena () : base()
		//{
		//}

        public SpadesActiveArena(Arena arena)
        {
            m_arena = arena;
        }

		public SpadesActiveArena(SpadesArena arena)
		{
			m_arena = arena;
		}


		public void SetArena(SpadesArena arena)
		{
			m_arena = arena;
		}

		public SpadesArenaMatch GetCurrArenaMatch()
		{
			return ((SpadesArena)m_arena).GetArenaMatch(m_curr_stage) as SpadesArenaMatch;
		}

	}
}