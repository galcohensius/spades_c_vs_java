﻿using System.Collections;
using cardGames.models;
using SpadesAI;

namespace spades.models
{

    /// <summary>
    /// Stores round and players data that is logged at the end of each round.
    /// </summary>
    public class SpadesRoundData : RoundData , IEnumerable
    {
        private SpadesPlayerRoundData[] playersData; // Player data by server position (0-3)

        public SpadesRoundData()
        {
            // Create empty players 
            playersData = new SpadesPlayerRoundData[4];
            for (int pos = 0; pos < 4; pos++)
            {
                playersData[pos] = new SpadesPlayerRoundData();
            }
        }


        public SpadesPlayerRoundData this[int pos]
        {
            get
            {
                return playersData[pos];
            }
        }

        public IEnumerator GetEnumerator()
        {
            return playersData.GetEnumerator();
        }
    }

    public class SpadesPlayerRoundData : PlayerRoundData
    {
 
        // Round data
        // Bid calc data
        // Bot data
        // Deal maker

        public override string ToString()
        {
            string ret_data = "hand: " + Hand;

            ret_data += "takes: " + Takes;
            ret_data += "bids: " + Bid;
            ret_data += "bags: " + Bags;
            ret_data += "points: " + Points;
            ret_data += "bagsPenalty: " + BagsPenalty;
            ret_data += "nilBonus: " + NilBonus;
            ret_data += "partnersBid: " + PartnersBid;
            ret_data += "partnersTakes: " + PartnersTakes;
            ret_data += "winPlace: " + WinPlace;
            ret_data += "initialBid: " + InitialBid;
            ret_data += "numericBid: " + NumericBid;
            ret_data += "nilValue: " + NilValue;
            ret_data += "botType: " + BotType;
            ret_data += "botLevel: " + BotLevel;
            ret_data += "cardsSwitch: " + CardsSwitch;
            ret_data += "forceSuitLength: " + ForceSuitLength;

            return ret_data;
        }

        public string Hand { get; set; } = "";
        public int Bid { get; set; }
        public int Takes { get; set; }
        public int Bags { get; set; }
        public int Points { get; set; }

        public int BagsPenalty { get; set; }
        public int NilBonus { get; set; }

        public int PartnersBid { get; set; }
        public int PartnersTakes { get; set; }
        public int PartnersBags { get; set; }

        public int BiddingPosition { get; set; }
        public float NilValue { get; set; }
        public float InitialBid { get; set; }
        public int NumericBid { get; set; }

        public int WinPlace { get; set; }
        public BrainType BotType { get; set; }
        public int BotLevel { get; set; }

        public int CardsSwitch { get; set; }
        public int ForceSuitLength { get; set; }

        public string Strategies { get; set; } = "";
    }

}