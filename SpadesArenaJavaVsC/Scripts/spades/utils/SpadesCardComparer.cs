﻿using System.Collections.Generic;
using cardGames.models;

namespace spades.utils
{
	/// <summary>
	/// Compares 2 cards according to Spades rules:
	/// Spades suit is always better than other cards.
	/// For non spades, highest rank is always better.
	/// </summary>
	public class SpadesCardComparer:IComparer<Card>
	{
		public int Compare (Card c1, Card c2)
		{
			if (c1.Suit == Card.SuitType.Spades && c2.Suit == Card.SuitType.Spades) {
				return c1.Rank - c2.Rank;
			} else if (c1.Suit == Card.SuitType.Spades) {
				return 1;
			} else if (c2.Suit == Card.SuitType.Spades) {
				return -1;
            } else if (c1.Rank == c2.Rank) {
                return c1.Suit - c2.Suit;
			} else {
				return c1.Rank - c2.Rank;
			}
		}
	}
}

