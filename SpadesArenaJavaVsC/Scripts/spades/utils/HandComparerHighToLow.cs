﻿using System.Collections.Generic;
using cardGames.models;

namespace spades.utils
{
	public class HandComparerHighToLow:IComparer<Card>
	{
		public int Compare (Card c1, Card c2)
		{
			// H, C, D, S
			if (c1.Suit == c2.Suit)
				return c2.Rank - c1.Rank;

			if (c1.Suit == Card.SuitType.Hearts)
				return -1;

			if (c1.Suit == Card.SuitType.Clubs &&
			    c2.Suit != Card.SuitType.Hearts)
				return -1;

			if (c1.Suit == Card.SuitType.Diamonds &&
				c2.Suit != Card.SuitType.Hearts && 
				c2.Suit != Card.SuitType.Clubs)
				return -1;
			

			return 1;
		}
	}
}

