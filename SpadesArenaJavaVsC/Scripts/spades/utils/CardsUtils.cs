﻿using cardGames.models;

namespace spades.utils
{
	public static class CardsUtils
	{
		/// <summary>
		/// Finds the winning card from a list of cards
		/// </summary>
		public static int FindWinningCardIndex(Card.SuitType? leadingSuit, CardsList cards) {

			Card winningCard = cards[0];
			int result = 0;

			for (int i = 1; i < cards.Count; i++) {
				if (winningCard.Suit == cards [i].Suit) {
					// Same suit check rank
					if (cards [i].Rank > winningCard.Rank) {
						result = i;
						winningCard = cards [i];
					}
				} else if (cards [i].Suit == Card.SuitType.Spades) {
					// Card is spades, must be better
					result = i;
					winningCard = cards [i];
				} else if (winningCard.Suit == Card.SuitType.Spades) {
					// current winning card is spades, must be better
				} else if (cards [i].Suit == leadingSuit) {
					result = i;
					winningCard = cards [i];
				} 
			}
			return result;
		}

	    /// <summary>
	    /// Finds the winning card from a list of cards
	    /// </summary>
	    public static Card FindWinningCard(Card.SuitType? leadingSuit, CardsList cards)
	    {
	        Card winningCard = cards[0];
	        int result = 0;

	        for (int i = 1; i < cards.Count; i++)
	        {
	            if (winningCard == null)
	            {
	                winningCard = cards[i];

	            }
	            else
	            {
	                if (cards[i] != null)
	                {
	                    if (winningCard.Suit == cards[i].Suit)
	                    {
	                        // Same suit check rank
	                        if (cards[i].Rank > winningCard.Rank)
	                        {
	                            result = i;
	                            winningCard = cards[i];
	                        }
	                    }
	                    else if (cards[i].Suit == Card.SuitType.Spades)
	                    {
	                        // Card is spades, must be better
	                        result = i;
	                        winningCard = cards[i];
	                    }
	                    else if (winningCard.Suit == Card.SuitType.Spades)
	                    {
	                        // current winning card is spades, must be better
	                    }
	                    else if (cards[i].Suit == leadingSuit)
	                    {
	                        result = i;
	                        winningCard = cards[i];
	                    }
	                }
                }
	        }
	        return winningCard;
	    }

        // x=0 is the highest card (BJ in Spades with jokers, Ace otherswise)
        public static Card GetXHighestCardInSuit(int x, Card.SuitType suit, string deck)
	    {
	        switch (deck)
	        {
                case "jokers":
                case "Jokers":
                    CardsList ansJokers = CardsListBuilder.FullDeckWithJokers().CardsBySuits(suit);
                    return ansJokers.Count > x ? ansJokers[x] : null;
	            case "regular":
	            default:
	                CardsList ans = CardsListBuilder.FullDeckNoJokers().CardsBySuits(suit);
	                return ans.Count > x ? ans[x] : null;    
	        }

	    }
        /// <summary>
        /// return the X lowest card.
        /// Classic decks start from 2, Jokers red suits start from 3
        /// </summary>
        /// <param name="x">x=0 is the lowest element</param>
        /// <param name="suit"></param>
        /// <param name="deck">Classic / Jokers</param>
        /// <returns></returns>
	    public static Card GetXLowestCardInSuit(int x, Card.SuitType suit, string deck)
	    {
	        switch (deck)
	        {
	            case "jokers":
	            case "Jokers":
	                CardsList ansJokers = CardsListBuilder.FullDeckWithJokers().CardsBySuits(suit);
	                return ansJokers.Count > x ? ansJokers[ansJokers.Count - x - 1] : null;
	            case "regular":
	            default:
	                CardsList ans = CardsListBuilder.FullDeckNoJokers().CardsBySuits(suit);
	                return ans.Count > x ? ans[ans.Count - x - 1] : null;
            }
	    }

	    public static int SuitLength(Card.SuitType suit, string deck)
	    {
	        switch (deck)
	        {
	            case "jokers":
	            case "Jokers":
	                switch (suit)
	                {
	                    case Card.SuitType.Spades: return 15;
	                    case Card.SuitType.Clubs: return 13;
	                    case Card.SuitType.Diamonds:
	                    case Card.SuitType.Hearts: return 12;
	                    default:
                        case Card.SuitType.Joker: return 0;
	                }
	            case "regular":
	            default:
	                return 13;
	        }
        }
    }
}
