﻿namespace spades.views

{
	public class CrownCollectibleItemView : MonoBehaviour 
	{
		[SerializeField]TMP_Text			m_title_text=null;
		[SerializeField]TMP_Text			m_crowns_counter_text=null;
		[SerializeField]Image				m_crown_image=null;
		[SerializeField]Image				m_progress_fill_image=null;

		[SerializeField]Image				m_gem_side_image_fill=null;
		[SerializeField]Image				m_gem_side_image_frame=null;

		[SerializeField]Image[]				m_gems_image_counter_fill=null;
		[SerializeField]Image[]				m_gems_image_counter_frame=null;

		Color								m_color;


		public void SetData(string title, int crowns_won_counter,int curr_step, Sprite crown_image,float progress_fill_amount, Color view_color, Sprite[] frames,Sprite curr_step_frame,Sprite curr_step_fill)
		{
			m_title_text.text = title;
			m_crowns_counter_text.text = crowns_won_counter.ToString();
			m_crown_image.sprite = crown_image;
			m_crown_image.SetNativeSize ();
			m_crown_image.gameObject.GetComponent<RectTransform> ().localScale = new Vector3 (1.5f, 1.5f, 1.5f);
			m_progress_fill_image.fillAmount = progress_fill_amount;
			m_color = view_color;

			if (crowns_won_counter==0)
				m_crown_image.color = new Color (0.1f, 0.1f, 0.1f);

			for (int i = 0; i < frames.Length; i++)
			{
				m_gems_image_counter_frame [i].sprite = frames [i];
			}

			for (int i = 0; i < m_gems_image_counter_fill.Length; i++)
				if(i<curr_step-1)
					m_gems_image_counter_fill [i].color = m_color;
			
			m_progress_fill_image.color = m_color;

			m_gem_side_image_fill.sprite = curr_step_fill;
			m_gem_side_image_fill.color = m_color;
			m_gem_side_image_frame.sprite = curr_step_frame;
		}

	}
}