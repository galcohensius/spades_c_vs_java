﻿using spades.controllers;

namespace spades.views
{

	public class GameView : MonoBehaviour 
	{

		public void TableOutAnimationDone()
		{
			//m_in_game_gui_canvas.alpha = 1f;
			//RoundController.Instance.TableInitDone();
			GetComponent<Animator>().SetBool("table_out_done",true);
		}

		public void TableInAnimationDone()
		{
			//m_in_game_gui_canvas.alpha = 1f;
			//RoundController.Instance.RandomizePlayersSittings();
			//GetComponent<Animator>().SetBool("table_out_done",true);
		}

		public void TableBouncePlayers()
		{
			//SoundsController.Instance.PlaySmack ();

			for (int i = 0; i < 4; i++)
			{
				//m_picture_views [i].TableHitBounce ();
			}
		}

		public void PlayWhooshSFX()
		{
            SpadesSoundsController.Instance.SpadesBrokenCardsMove();
		}

	}


}