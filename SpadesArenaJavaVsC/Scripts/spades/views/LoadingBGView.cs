﻿namespace spades.views
{

	public class LoadingBGView : MonoBehaviour 
	{
		[SerializeField]Sprite		m_cardface=null;
		[SerializeField]Sprite		m_cardback=null;
		[SerializeField]GameObject	m_card_obj=null;

		Image						m_card;

		void Awake()
		{
			m_card = m_card_obj.GetComponent<Image> ();
		}

		void OnEnable()
		{
			m_card.sprite = m_cardface;
			Rotate1 ();
		}
	
		void Rotate1()
		{
			iTween.RotateTo(m_card_obj, iTween.Hash("name","RotateCard","y", 90, "easeType", "linear", "time", .25,"onComplete","Rotate2","onCompleteTarget",this.gameObject));
		}

		void Rotate2()
		{
			m_card.sprite = m_cardback;
			iTween.RotateTo(m_card_obj, iTween.Hash("name","RotateCard","y", 180, "easeType", "linear", "time", .25,"onComplete","Rotate3","onCompleteTarget",this.gameObject));
		}

		void Rotate3()
		{
			//m_card.sprite = m_cardback;
			iTween.RotateTo(m_card_obj, iTween.Hash("name","RotateCard","y", 270, "easeType", "linear", "time", .25,"onComplete","Rotate4","onCompleteTarget",this.gameObject));
		}

		void Rotate4()
		{
			m_card.sprite = m_cardface;
			iTween.RotateTo(m_card_obj, iTween.Hash("name","RotateCard","y", 0, "easeType", "linear", "time", .25,"onComplete","Rotate1","onCompleteTarget",this.gameObject));
		}

	}
}