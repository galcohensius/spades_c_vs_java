﻿using System.Collections;
using System.Collections.Generic;

namespace spades.views
{
    public class SpadesLoaderView : MonoBehaviour
    {

        [SerializeField] List<Sprite> m_spades_loader = new List<Sprite>();
        [SerializeField] float m_flip_time = 0.15f;
        Image m_image_bg;
        int m_index_counter = 0;

        private void Awake()
        {
            m_image_bg = transform.GetChild(0).gameObject.GetComponent<Image>();
        }

        private void Start()
        {
            StartCoroutine(SwapImages());
        }

        IEnumerator SwapImages()
        {
            while (true)
            {
                m_index_counter++;

                if (m_index_counter >= m_spades_loader.Count)
                    m_index_counter = 0;

                m_image_bg.sprite = m_spades_loader[m_index_counter];

                yield return new WaitForSeconds(m_flip_time);
            }

        }
    }
}