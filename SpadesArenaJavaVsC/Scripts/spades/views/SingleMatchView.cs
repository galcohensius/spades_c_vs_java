﻿using	spades.models;

namespace spades.views
{

	public class SingleMatchView : MonoBehaviour 
	{
		public Text			m_cost_text;
		public GameObject	m_aligner;

		SpadesSingleMatch			m_match;

		public void SetMatchModel(SpadesSingleMatch match)
		{
			m_match = match;
			UpdateDisplay();
		}

		void UpdateDisplay()
		{
			m_cost_text.text = m_match.Buy_in.ToString();

			float icon_width = m_aligner.transform.Find("ButtonTextIcon").GetComponent<RectTransform>().rect.width-10;//this is because the image has edges

			float temp = (icon_width - m_cost_text.preferredWidth) /2;

			m_aligner.GetComponent<RectTransform>().localPosition = new Vector3(temp,0,0);

		}

	}
}