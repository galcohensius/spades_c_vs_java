﻿using System.Collections.Generic;
using System;

namespace spades.views.popups
{
    public class Last5GamesView : MonoBehaviour
    {

        [SerializeField] winLooseView[] m_winLooseViews = null;

        public void Init(List<int> state)
        {
            foreach (winLooseView view in m_winLooseViews)
            {
                view.Hide();
            }

            for (int i = 0; i < state.Count; i++)
            {
                m_winLooseViews[i].win = Convert.ToBoolean(state[i]);
            }
        }

    }
}