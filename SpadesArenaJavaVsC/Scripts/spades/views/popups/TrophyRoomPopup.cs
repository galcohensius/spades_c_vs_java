﻿using System.Collections;
using spades.models;
using common.views;
using common.controllers;
using cardGames.models;

namespace spades.views.popups
{

    public class TrophyRoomPopup : PopupBase
    {
        [SerializeField] GemItemViewTrophy[] m_gems_items = new GemItemViewTrophy[5];
        [SerializeField] GameObject[] m_gems_glows_light = new GameObject[5];
        [SerializeField] GameObject m_gems_stand_holder = null;

        [SerializeField] RectTransform m_gems_only_holder = null;
        [SerializeField] GameObject m_achievements_btn = null;


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            InitTrophiesData();
            StartCoroutine(ShowGems());

            // Show the achievements tab only if connected to game center and Android
            if (!GameCentersManager.Instance.IsLoggedIn() || Application.platform!=RuntimePlatform.Android)
            {
                m_achievements_btn.gameObject.SetActive(false);
            }
        }

		public override void OnBackButtonClicked()
		{
			CloseButtonClicked ();
		}

		IEnumerator ShowGems()
		{
			yield return new WaitForSeconds (0.125f);
			m_gems_only_holder.localPosition = new Vector3 ();
		}
	
		public void InitTrophiesData()
		{

			for (int i = 0; i < 5; i++)
			{                

				SpadesWorld world = ModelManager.Instance.GetWorldById ((i + 1).ToString ()) as SpadesWorld;

				int level_unlock = world.GetArena(PlayingMode.Solo).GetLevel();
								
				int progress = world.Arena_Gem_Curr_Step;
				int total_steps = world.Arena_Gem_Total_Steps;
				int wins = world.Arena_Gems_Won;
				
				m_gems_items [i].SetData (progress,total_steps, wins, world.GetName(), i,level_unlock,m_gems_glows_light[i] );
			}
		}

        public void bt_AchievementsBtnClick() {
            GameCentersManager.Instance.ShowNativeAchievementsScreen();
        }

		public override void CloseButtonClicked ()
		{
			
			m_gems_only_holder.gameObject.SetActive (false);

			m_manager.HidePopup ();
		}
			
	}
}