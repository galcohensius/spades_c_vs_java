﻿using System;

namespace spades.views.popups
{
    public class GemsView : MonoBehaviour
    {

        [SerializeField] GemView[] m_gems = null;

        public void Init(string gemStates)
        {
            string[] gems_count = gemStates.Split(',');

            for (int i = 0; i < gems_count.Length; i++)
            {
                m_gems[i].GemCount = Convert.ToInt32(gems_count[i]);
            }

        }


    }
}