﻿using cardGames.comm;
using cardGames.views.popups;
using common.ims.model;
using common.utils;
using spades.comm;
using spades.models;

namespace spades.views.popups
{
    public class SpadesIMSChallengePopup : IMSChallengesPopup
    {
        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            IMSDynamicContent dynamicContent = m_iZone.DynamicContent as IMSDynamicContent;

            if (dynamicContent.Content.Count == 0)
                return;

            // TODO: Find a more generic solotion
            SpadesSingleMatch match = (CardGamesCommManager.Instance.GetModelParser() as SpadesModelParser).ParseSingleMatch(dynamicContent.Content[0], null);

            Transform buyIn = transform.FindDeepChild("BuyIn");
            if (buyIn!=null)
                buyIn.GetComponent<TMP_Text>().text = "BET <sprite=1>" + FormatUtils.FormatBuyIn(match.Buy_in);

        }
    }
}