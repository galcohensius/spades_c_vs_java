using spades.controllers;
using spades.models;
using common.controllers;
using common.experiments;
using cardGames.models;
using cardGames.controllers;
using cardGames.views;

namespace spades.views.popups
{
    public class BiddingPopup : MonoBehaviour
    {
        public const string BIDDING_EXPERIMENT_ID = "6";

        private const float LOADER_LENGHT = 580f;
        private const float LEFT_EDGE = -250f;

        int m_selected_index;

        [SerializeField] Material white_font = null;
        [SerializeField] Material gray_font = null;

        [SerializeField] Sprite m_idle = null;
        [SerializeField] Sprite m_selected = null;
        [SerializeField] Sprite m_disabled = null;

        [SerializeField] RectTransform m_timer_fill_red = null;
        [SerializeField] RectTransform m_timer_fill_blue = null;

        [SerializeField] GameObject m_all_numbers_group = null;
        [SerializeField] GameObject m_blind_group = null;

        [SerializeField] GameObject m_bottom_bg = null;


        [SerializeField] GameObject m_bid_button = null;
        [SerializeField] TMP_Text m_bid_button_txt = null;

        [SerializeField] MAvatarView m_mavatar;

        public void Show(int roundNum, PopupClosedDelegate close_delegate = null)
        {
            //ExperimentsManager.Instance.OverrideValue(BIDDING_EXPERIMENT_ID, 2);

            m_selected_index = -1;

            gameObject.SetActive(true);
            m_all_numbers_group.SetActive(false);
            m_blind_group.SetActive(true);

            m_bid_button.GetComponent<Button>().interactable = false;
            m_bid_button_txt.text = "Bid";
            ManagerView.Instance.EnableDisableButton(m_bid_button.GetComponent<Button>());
            
            ISpadesMatch match = SpadesModelManager.Instance.GetActiveMatch().GetMatch();

            // Get the number of games played:
            int totalMatches = (ModelManager.Instance.GetUser().Stats as SpadesUserStats).TotalMatchesPlayed;

            // Show the blind nil popup window for the relevant variants, not on the first round.
            if (match.Playing_mode == PlayingMode.Partners &&
                roundNum > 1 &&
                match.Variant != PlayingVariant.Mirror &&
                match.Variant != PlayingVariant.Suicide &&
                match.Variant != PlayingVariant.Whiz &&
                match.SubVariant != PlayingSubVariant.NoNil &&
                totalMatches >= 6)
            {
                CreateKeyboard(blind_nil: true);
            }
            else
            {
                RoundController.Instance.ShowCards();
                CreateKeyboard(blind_nil: false);
            }
        }

        /// <summary>
        /// Create both bidding keyboard: first the blindNil popup (when needed), then the bidding [0-13], each variant/sub variant has different bidding options. 
        /// </summary>
        /// <param name="blind_nil"></param>
        public void CreateKeyboard(bool blind_nil)
        {
            if (blind_nil)
            {
                //show: Blind Nil / Show cards buttons
                m_all_numbers_group.SetActive(false);
                m_blind_group.SetActive(true);
                m_bottom_bg.SetActive(false);
            }
            else 
            {
                m_all_numbers_group.SetActive(true);
                m_blind_group.SetActive(false);
                m_bottom_bg.SetActive(true);

                int spadesNumber = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(3).GetCards().CardsBySuits(Card.SuitType.Spades).Count;
                int partnersBid = 0;
                if ((SpadesModelManager.Instance.GetActiveMatch().GetMatch().Playing_mode == PlayingMode.Partners))
                    partnersBid = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(1).GetBids();

                int i = 0;
                foreach (Transform T in m_all_numbers_group.transform.Find("Numbers").GetComponent<Transform>())
                {
                    Button curBtn = T.GetComponent<Button>();

                    switch (SpadesModelManager.Instance.GetActiveMatch().GetMatch().Variant)
                    {
                        case (PlayingVariant.Classic):
                        case (PlayingVariant.Jokers):
                        default:
                            {
                                if (SpadesModelManager.Instance.GetActiveMatch().GetMatch().SubVariant == PlayingSubVariant.NoNil)
                                {
                                    // SubVariant NoNil
                                    SetButtonState(i + partnersBid <= 13 && i > 0, curBtn);  // active button if (i + partnersBid <= 13 && no nil)
                                }
                                else
                                {
                                    SetButtonState(i + partnersBid <= 13, curBtn);  // active button if (i + partnersBid <= 13)
                                }
                                i++;
                                break;
                            }

                        case (PlayingVariant.Mirror):
                            SetButtonState(i == spadesNumber, curBtn); // active button if i==Spades#
                            i++;
                            break;

                        case (PlayingVariant.Whiz):
                            {
                                SetButtonState(i == 0 || i == spadesNumber, curBtn);  //activate: 0 or number of spades
                                i++;
                                break;
                            }

                        case (PlayingVariant.Suicide): // one partner must bid 0, other must bid 4 or more
                            {
                                if (partnersBid == -1) // first partner
                                {
                                    SetButtonState(i == 0 || i>=4 , curBtn);  //activate button
                                }
                                else // Second partner: hero must bid the other option that partner didn't bid 0/4+
                                {
                                    if (partnersBid == 0)
                                    {
                                        SetButtonState(i >= 4, curBtn); // 4+
                                    }
                                    else if (partnersBid >= 4)
                                    {
                                        SetButtonState(i == 0, curBtn);  // only nil
                                    }
                                }
                                i++;
                                break;
                            }
                    }
                }
            }


            // A/B testing
            RectTransform rectTransform = (RectTransform)transform;
            ExperimentsManager.Instance.With(BIDDING_EXPERIMENT_ID,
            () =>
            {
                m_bottom_bg.SetActive(true);
                rectTransform.sizeDelta = new Vector2(rectTransform.rect.width, 630);
            },
            () =>
            {
                m_bottom_bg.SetActive(false);
                rectTransform.sizeDelta = new Vector2(rectTransform.rect.width, 520);
            });
        }

        public Button SetButtonState(bool isActive, Button button)
        {
            if (isActive)
            {
                //activate button
                button.interactable = true;
                ManagerView.Instance.EnableDisableButton(button);

                button.image.sprite = m_idle;
                button.transform.Find("Text").GetComponent<TMP_Text>().fontMaterial = white_font;
                button.transform.Find("Text").GetComponent<TMP_Text>().color = Color.white;

            }
            else
            {
                //disable button
                button.interactable = false;
                ManagerView.Instance.EnableDisableButton(button);

                button.image.sprite = m_disabled;
                button.transform.Find("Text").GetComponent<TMP_Text>().fontMaterial = gray_font;
                button.transform.Find("Text").GetComponent<TMP_Text>().color = new Color(58f / 255f, 61f / 255f, 66f / 255f);
            }
            return button;
        }


        public void BT_BidNumberClicked(int value)
        {
            m_bid_button.GetComponent<Button>().interactable = true;
            ManagerView.Instance.EnableDisableButton(m_bid_button.GetComponent<Button>());
            CardGamesSoundsController.Instance.ButtonToggle();

            if (m_selected_index == -1)
            {
                m_all_numbers_group.transform.Find("Numbers").GetChild(value).GetComponent<Button>().image.sprite = m_selected;
            }
            else
            {
                m_all_numbers_group.transform.Find("Numbers").GetChild(m_selected_index).GetComponent<Button>().image.sprite = m_idle;
                m_all_numbers_group.transform.Find("Numbers").GetChild(value).GetComponent<Button>().image.sprite = m_selected;
            }

            m_selected_index = value;

            m_bid_button_txt.text = "Bid " + value;
            if (value == 0)
                m_bid_button_txt.text = "Bid Nil";

            ExperimentsManager.Instance.RunIf(BIDDING_EXPERIMENT_ID, 2, () =>
            {
                BT_BidButtonClicked();
            });
        }

        public void CloseBiddingKeyboard()
        {
            gameObject.SetActive(false);
        }


        public void BT_BidButtonClicked()
        {
            m_mavatar.ShowChat("I bid " + m_selected_index, 48);

            TableChatController.Instance.ToggleSingleMenu(true, false);
            TableChatController.Instance.ToggleChatMenuLock(true);

            CardGamesSoundsController.Instance.ButtonToggle();
            if (m_selected_index != -1)
            {
                RoundController.Instance.MakeBid(m_selected_index, false);
            }
        }

        /// <summary>
        /// Blinds the bidding keyboard clicked.
        /// </summary>
        /// <param name="value">Value. - 0 blind nil - value -1 show my cards</param>
        public void BT_BlindButtonsClicked(int value)
        {
            //StopCoroutine(m_bidding_timer);
            CardGamesSoundsController.Instance.ButtonClicked();
            RoundController.Instance.ShowCards();
            if (value == 1)
            {
                CreateKeyboard(false);
            }
            else
            {
                RoundController.Instance.MakeBid(value, true);
            }
        }

        public void SetTimerProgressValue(float progress_value)
        {
            m_timer_fill_blue.localPosition = new Vector3(-LOADER_LENGHT + progress_value * LOADER_LENGHT, 0, 0);
            m_timer_fill_red.localPosition = new Vector3(-LOADER_LENGHT + progress_value * LOADER_LENGHT, 0, 0);
        }

        public void SetTimerColor(bool blue)
        {
            m_timer_fill_blue.gameObject.SetActive(blue);
            m_timer_fill_red.gameObject.SetActive(!blue);

        }

        public int Selected_index
        {
            get
            {
                return m_selected_index;
            }
        }
    }
}
