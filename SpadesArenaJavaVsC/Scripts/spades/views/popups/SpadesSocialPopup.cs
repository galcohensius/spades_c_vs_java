using cardGames.controllers;
using cardGames.views.popups;
using common.controllers;
using common.facebook.views;
using cardGames.views.overlays;
using spades.controllers;

namespace spades.views.popups
{
    public class SpadesSocialPopup : SocialPopup
    {
        protected override void HandleClickConnectToFacebook()
        {
            LobbyController.Instance.FBLogin((bool success) => {

                if (success)
                {
                    SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
                }
                else
                {
                    m_facebookConnectBtn.SetMode(FacebookButton.ButtonMode.Connect);
                }
            });
        }

        //implementation could be even simplified but that needs re-doing OverlaysManager/SpadesOverlayManager, ManagerView and LobbyTopController.
        //not sure it worth the time but should be considered for future rummy implementation
        public override void FlyCoinsComet(int prize_value, GameObject start_game_object)
        {
            float scale_factor = 2f;
            float scale_timing = 0.3f;
            float scale_delay = 0.2f;

            //fly the coins - when flight is done - do the post send gift
            CometOverlay comets_overlay = SpadesOverlayManager.Instance.ShowCometOverlay();
            int num_coins_to_show = 3;

            if (start_game_object != null)
            {
                comets_overlay.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, start_game_object, CardGamesPopupManager.Instance.Lobby_flight_target, 0f, 0.15f, .75f,
                    CardGamesPopupManager.Instance.Lobby_coins_balance, prize_value, CardGamesPopupManager.Instance.Lobby_coins_animator, scale_factor, scale_timing, scale_delay, CometsDoneFlying, new Vector2(0.6f, 0.6f));
            }
            else
            {
                SpadesOverlayManager.Instance.HideOverlay(comets_overlay);
            }
        }
    }
}
