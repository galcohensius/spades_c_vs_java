﻿using spades.controllers;
using common.views;
using common.controllers;
using cardGames.models;
using common.facebook;

namespace spades.views.popups
{

	public class ConnectToFBPopup : PopupBase {

		[SerializeField]GameObject	m_spades_loading_parent=null;
		[SerializeField]GameObject	m_try_again_object=null;
		[SerializeField]GameObject m_close_button=null;

		[SerializeField]TMP_Text	m_body_text=null;

		public override void Show(PopupManager manager)
		{
			base.Show (manager);
			AttemptFBLogin ();

		}

		public void BT_ClosedButtonClicked()
		{

			m_manager.HidePopup ();
		}

		public void BT_TryAgainClicked()
		{
			AttemptFBLogin ();
		}

		private void AttemptFBLogin()
		{
			m_body_text.text = "Please Wait...";
			m_spades_loading_parent.SetActive (true);
			m_close_button.SetActive (false);
			m_try_again_object.SetActive (false);

			LobbyController.Instance.FBLogin ((bool success)=>{
				
				if(success)
				{
                    LobbyTopController.Instance.AvatarView.SetAvatarModel(ModelManager.Instance.GetUser().GetMAvatar());
					//ModelManager.Instance.GetUser().GetAvatar().Avatar_mode = OldAvatarModel.AvatarMode.FBImage;
					LobbyTopController.Instance.Progress_view.SetUserNameText(FacebookController.Instance.UserFacebookData.First_name);
					//LobbyTopController.Instance.AvatarView.CreateImage();
					m_spades_loading_parent.SetActive (false);
					m_body_text.text = "Connection Succesful...";
				}
				else
				{
					ShowTryAgain();
				}


			});
		}

		public void ShowTryAgain()
		{
			m_body_text.text = "";
			m_close_button.SetActive (true);
			m_spades_loading_parent.SetActive (false);
			m_try_again_object.SetActive (true);
		}

	}
}