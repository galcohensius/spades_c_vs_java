using spades.controllers;
using common.views;
using common.utils;

namespace spades.views
{
	public class ArenaEliminatedPopup : PopupBase
	{

		[SerializeField] TMP_Text m_top_text = null;
		[SerializeField] TMP_Text m_cost_text = null;
		[SerializeField] Button m_continue_button = null;

		int m_rebuy_cost = 0;
		RebuyRequested m_rebuy_requested_delegate;
		RebuyQuitRequested m_rebuy_quit_requested_delegate;

		public void SetData(int stage, int rebuy, RebuyRequested rebuy_requested_delegate, RebuyQuitRequested rebuy_quit_requested)
		{
			m_rebuy_requested_delegate = rebuy_requested_delegate;
			m_rebuy_quit_requested_delegate = rebuy_quit_requested;
			m_top_text.text = string.Format(m_top_text.text, stage);

			m_rebuy_cost = rebuy;


			if (m_rebuy_cost > 0)
				m_cost_text.text = "<sprite=1>" + FormatUtils.FormatBuyIn(m_rebuy_cost);

		}

		public override void OnBackButtonClicked()
		{
			GiveUpClicked();
		}

		public void ContinueClicked()
		{
			m_continue_button.interactable = false;
            ManagerView.Instance.EnableDisableButton(m_continue_button);

            m_rebuy_requested_delegate(() =>
			   {
				   m_continue_button.interactable = true;
                   ManagerView.Instance.EnableDisableButton(m_continue_button);
               });

		}

		public void GiveUpClicked()
		{
			m_manager.HidePopup();
			m_rebuy_quit_requested_delegate(false);
		}



	}
}
