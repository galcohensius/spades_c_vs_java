﻿using common.utils;

namespace spades.views.popups
{
    public class CoinsBoxView : MonoBehaviour
    {

        [SerializeField] TMP_Text m_balance_text = null;

        public void Init(int balance)
        {
            m_balance_text.text = FormatUtils.FormatBalance(balance);
        }
    }
}