using cardGames.comm;
using cardGames.controllers;
using cardGames.models;
using cardGames.views;
using common.controllers;
using common.facebook.views;
using common.models;
using common.utils;
using common.views;
using spades.controllers;
using spades.models;
using System;
using System.Collections.Generic;

namespace spades.views.popups
{
    public class NewPlayerProfilePopup : PopupBase
    {
        [SerializeField]
        private MAvatarView m_avatarView;

        [SerializeField] GameObject m_avatar_holder = null;
        [SerializeField] Last5GamesView m_last_games = null;
        [SerializeField] GameObject m_player_info = null;
        [SerializeField] TMP_Text m_player_name = null;
        [SerializeField] TMP_Text m_player_level = null;
        [SerializeField] TMP_Text m_partners_games_won = null;
        [SerializeField] TMP_Text m_partners_win_ratio = null;
        [SerializeField] TMP_Text m_solo_games_won = null;
        [SerializeField] TMP_Text m_solo_win_ratio = null;
        [SerializeField] TMP_Text m_total_won = null;
        [SerializeField] TMP_Text m_challenges_won = null;
        [SerializeField] TMP_Text m_rounds_played = null;
        [SerializeField] TMP_Text m_bids_met = null;
        [SerializeField] TMP_Text m_nil_bids_met = null;
        [SerializeField] TMP_Text m_balance_text = null;
        [SerializeField] Button m_edit_avatar_button = null;
        [SerializeField] Button m_trophies_button = null;
        [SerializeField] Button m_leaderboards_button = null;
        [SerializeField] GameObject m_data_holder = null;
        [SerializeField] GameObject m_spades_holder = null;

        [SerializeField] NameEditToolTipView m_nameEditTooltip = null;

        [SerializeField] GameObject m_editNameButton;
        [SerializeField] GameObject m_connectToFacebookBox;
        [SerializeField] FacebookButton m_facebookConnectButton;
        [SerializeField] GameObject m_nameEditBlackScreen;

        [SerializeField] GameObject m_facebookImageButton;
        [SerializeField] GameObject m_facebookZoomImage;

        [SerializeField] TMP_Text m_facebookConnectDescriptionText = null;

        private const string FACEBOOK_CONNECT_TEXT_MSG = "Connect with Facebook";
        private const string FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG = "Connect with Facebook & Get <color=#ffff0f>{0}</color> free coins!";

        private SpadesUser m_user = null;
        private bool m_isLeaderboard = false;
        private bool m_isEditable;

        //secret shortcut params
        int m_click_counter = 0;




        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            // When this popup is created, only the spades holder is shown
            m_spades_holder.SetActive(true);

            m_data_holder.SetActive(false);
            m_player_name.gameObject.SetActive(false);
            m_connectToFacebookBox.SetActive(false);


            ToggleEditableMode(false);

            if (m_user != null)
            {
                // If this popup was closed by another popup and reopen (like when pressing Edit Avatar), call SetData immediatly
                SetData(!m_isEditable, m_user, m_isLeaderboard);
            }
        }


        public void SetData(bool disableEdit, SpadesUser user = null, bool isLeaderboard = false)
        {
            if (this == null)
                return;

            if (user != null && user.GetID() != ModelManager.Instance.GetUser().GetID())
            {
                m_user = user;
            }
            else
            {
                m_user = ModelManager.Instance.GetUser() as SpadesUser;

            }

            ToggleEditableMode(!disableEdit);

            SetUserData();

            // TODO: fix this old issue:
            // ******************************
            //m_avatarView.SetFacebookImageLoadedListener(OnFacebookImageLoaded);
            // ******************************
            m_avatarView.SetAvatarModel(m_user.GetMAvatar());

            m_spades_holder.SetActive(false);

            m_data_holder.SetActive(true);
            m_player_name.gameObject.SetActive(true);

            if (!disableEdit)
            {
                // Facebook connect box stopped showing on version 1.23
                //ToggleConnectToFacebookBox(!FacebookController.Instance.IsLoggedIn());
            }

            if (m_user.GetLevel() < LeaderboardsModel.LEADERBOARD_MIN_LEVEL)
            {
                m_leaderboards_button.gameObject.SetActive(false);
            }
        }

        private void OnFacebookImageLoaded()
        {
            // COMMENT BY RAN (4/6/2019) - ITS NOT WORKING PROPERLY
            //if (ModelManager.Instance.GetUser().GetAvatar().Mode == AvatarModel.AvatarMode.FacebookAndAvatar)
            //{
            //    m_facebookImageButton.SetActive(true);
            //    m_facebookZoomImage.SetActive(true);
            //}
        }

        private void ToggleEditableMode(bool isEditable)
        {
            m_isEditable = isEditable;
            m_edit_avatar_button.gameObject.SetActive(isEditable);
            m_trophies_button.gameObject.SetActive(isEditable);
            m_leaderboards_button.gameObject.SetActive(isEditable);
            m_editNameButton.SetActive(isEditable);
        }

        private void ToggleConnectToFacebookBox(bool shouldDisplay)
        {
            m_connectToFacebookBox.SetActive(shouldDisplay);

            if (shouldDisplay)
            {
                SetFacebookConnectTextMessage();
            }
        }

        public void Btn_EditAvatarClicked()
        {
            if (!m_isEditable)
                return;

            m_close_delegate += () =>
            {
                CardGamesPopupManager.Instance.ShowMavatarCreationPopup(PopupManager.AddMode.ShowAndRemove);
            };

            m_manager.HidePopup();


        }

        public void Btn_TrophiesClicked()
        {
            (PopupManager.Instance as SpadesPopupManager).ShowTrophyRoomPopup(PopupManager.AddMode.ShowAndRemove);

        }

        public void Btn_LeaderboardsClicked()
        {
            m_close_delegate += () =>
            {
                LeaderboardsController.Instance.ShowLeaderboard(LeaderboardsModel.LeaderboardType.Friends);
            };

            m_manager.HidePopup();


        }

        private void SetUserData()
        {
            SpadesUserStats spadesUserStats = m_user.Stats as SpadesUserStats;

            m_player_name.text = GetNameWithEditableSuffix(m_user.GetMAvatar().NickName);

            TMPBidiHelper.MakeRTL(m_player_name);

            m_player_level.text = string.Format(m_player_level.text, m_user.GetLevel());

            m_balance_text.text = "<sprite=1>" + FormatUtils.FormatBalance(m_user.GetCoins());

            m_partners_games_won.text = spadesUserStats.MatchWinsPartner + " of " + (m_user.Stats as SpadesUserStats).MatchPlayedPartner;
            m_partners_win_ratio.text = string.Format("{0:P2}", spadesUserStats.MatchWinRatioPartner);

            m_solo_games_won.text = spadesUserStats.MatchWinsSolo + " of " + (m_user.Stats as SpadesUserStats).MatchPlayedSolo;
            m_solo_win_ratio.text = string.Format("{0:P2}", spadesUserStats.MatchWinRatioSolo);

            m_total_won.text = spadesUserStats.TotalMatchesPlayed.ToString();
            m_challenges_won.text = spadesUserStats.ArenaPlayed.ToString();
            m_rounds_played.text = spadesUserStats.RoundPlayed.ToString();
            m_bids_met.text = string.Format("{0:P2}", spadesUserStats.ExactBidsRatio);
            m_nil_bids_met.text = string.Format("{0:P2}", spadesUserStats.NilTakesRatio);

            //fake data incase there is no 5 games state history
            if (spadesUserStats.FiveGamesState == null || spadesUserStats.FiveGamesState.Count == 0)
            {
                spadesUserStats.FiveGamesState = new List<int>();

                for (int i = 0; i < Math.Min(spadesUserStats.TotalMatchesPlayed, 5); i++)
                {
                    spadesUserStats.FiveGamesState.Add(UnityEngine.Random.Range(0, 2));
                }
            }

            m_last_games.Init(spadesUserStats.FiveGamesState);
        }

        public void Button_SecretClicked()
        {
            m_click_counter++;

            if (m_click_counter >= 5)
            {
                //send log 
                SpadesPopupManager.Instance.ShowAutoMessagePopup("Log Sent", 1.5f);
                LoggerController.Instance.SendLogDataToServer("User", "User log sent");
                m_click_counter = 0;
            }
        }

        private string GetNameWithEditableSuffix(string name)
        {
            string editableImageSuffix = "";

            if (m_isEditable)
            {
                editableImageSuffix = "<sprite name=rename>";
            }

            return name + editableImageSuffix;
        }

        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }

        public void ToggleFacebookImageState(bool shouldActivate)
        {
            gameObject.GetComponent<Animator>().SetBool("ShouldActivate", shouldActivate);

            bool shouldDisplayZoomImage = !shouldActivate;

        }


        public void SetPlayerName(string name, bool wasAccepted)
        {
            m_nameEditBlackScreen.SetActive(false);

            if (wasAccepted)
            {
                m_player_name.text = GetNameWithEditableSuffix(name);
                m_player_name.gameObject.SetActive(true);
                TMPBidiHelper.MakeRTL(m_player_name);

                MAvatarModel model = ModelManager.Instance.GetUser().GetMAvatar();
                model.NickName = name;

                ModelManager.Instance.GetUser().GetMAvatar().FireOnModelChangedEvent();

                CardGamesCommManager.Instance.UpdateUserAvatar(model, name);
            }

            LobbyTopController.Instance.Progress_view.SetUserNameText(name);
        }

        private void OnUserNickNameUpdated(bool success, JSONNode responseData)
        {
            if (success)
            {
                LoggerController.Instance.Log("Server Updated");
            }
        }

        public void OpenNameEditConsole()
        {
            m_nameEditBlackScreen.SetActive(true);
            m_nameEditTooltip.Init(m_user.GetMAvatar().NickName, SetPlayerName);
        }

        public void ConnectToFacebook()
        {
            m_facebookConnectButton.SetMode(FacebookButton.ButtonMode.Waiting);

            m_edit_avatar_button.interactable = false;
            ViewUtils.EnableDisableButtonText(m_edit_avatar_button);
            m_trophies_button.interactable = false;
            ViewUtils.EnableDisableButtonText(m_trophies_button);

            LobbyController.Instance.FBLogin((bool success) =>
            {
                if (this != null)
                {
                    m_edit_avatar_button.interactable = true;
                    ViewUtils.EnableDisableButtonText(m_edit_avatar_button);
                    m_trophies_button.interactable = true;
                    ViewUtils.EnableDisableButtonText(m_trophies_button);

                    m_facebookConnectButton.SetMode(FacebookButton.ButtonMode.Connect);

                    if (success)
                    {
                        RefreshPopup();
                    }
                }
            });
        }

        private void SetFacebookConnectTextMessage()
        {
            FacebookBonus bonus = (FacebookBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.FacebookConnect);

            m_facebookConnectDescriptionText.text = string.Format(FACEBOOK_CONNECT_TEXT_MSG);

            if (bonus != null)
            {
                if (!bonus.IsEntitled)
                {
                    m_facebookConnectDescriptionText.text = string.Format(FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG, FormatUtils.FormatBalance(bonus.CoinsSingle));
                    m_facebookConnectButton.SetMode(FacebookButton.ButtonMode.Connect, true);
                }
            }
        }

        private void RefreshPopup()
        {
            SetData(false);
        }

        public bool IsEditable
        {
            get
            {
                return m_isEditable;
            }
        }
    }
}