﻿using System;
using cardGames.models;
using common.utils;
using common.views;
using spades.controllers;
using spades.models;

namespace spades.views.popups
{

    public class C4PQuitPopup : PopupBase
    {

        [SerializeField] TMP_Text m_yes_text = null;
        [SerializeField] TMP_Text m_no_text = null;
        [SerializeField] TMP_Text m_title = null;
        [SerializeField] TMP_Text m_buy_in = null;
        [SerializeField] TMP_Text m_penalty = null;
        [SerializeField] TMP_Text m_penaltyTitle = null;
        [SerializeField] GameObject m_C4P;

        Action yes_action;
        Action no_action;

        bool isClicking = false;

        public void SetTexts(string yes_text, string no_text)
        {
            m_yes_text.text = yes_text;
            m_no_text.text = no_text;

            m_C4P.SetActive(true);
            RectTransform rt = GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(1200, 720);

            int buyIn = ModelManager.Instance.GetActiveMatch().Match.Buy_in;
            int currentMoney = ModelManager.Instance.GetUser().GetCoins();

            

            ISpadesMatch spadesMatch = SpadesModelManager.Instance.GetActiveMatch().GetMatch();

            int factor = spadesMatch.Coins4PointsFactor;

            int minValuePenalty = Math.Min(ModelManager.Instance.GetUser().GetCoins(), factor * RoundController.Instance.C4pQuitPoints);

            if (spadesMatch.SubVariant == PlayingSubVariant.HiLo)
            {
                m_penaltyTitle.text = "HI-LO Quit Penalty:";
                minValuePenalty = Math.Min(ModelManager.Instance.GetUser().GetCoins(), factor * RoundController.Instance.HiLoQuitPoints);
            }
            
            m_title.text = "You'll lose a total of <sprite=1>" + FormatUtils.FormatBalance(buyIn + minValuePenalty);
            m_buy_in.text = "<sprite=1>" + FormatUtils.FormatBalance(buyIn);
            m_penalty.text = "<sprite=1>" + FormatUtils.FormatBalance(minValuePenalty);

        }

        public override void OnBackButtonClicked()
        {
            YesButtonClicked();
        }


        public virtual void YesButtonClicked()
        {
            if (!isClicking)
            {
                isClicking = true;
                yes_action?.Invoke();
                m_manager.HidePopup();
            }

        }


        public void NoButtonClicked()
        {
            if (!isClicking)
            {
                isClicking = true;
                no_action?.Invoke();
                m_manager.HidePopup();
            }
        }

        public Action Yes_action
        {
            get
            {
                return yes_action;
            }

            set
            {
                yes_action = value;
            }
        }

        public Action No_action
        {
            get
            {
                return no_action;
            }

            set
            {
                no_action = value;
            }
        }

    }
}
