﻿using common.views;
using common.controllers;
using common.models;
using common.utils;
using cardGames.models;
using cardGames.views.popups;
using cardGames.controllers;

namespace spades.views.popups
{
    public class FreeCoinsPopup : PopupBase
    {
        private const string MSG_INVITE_FRIENDS = "Invite your friends & get free coins for every friend that joins!";
        private const string MSG_REWARDED_VIDEO = "Watch a video & get";

        [SerializeField] Button m_rewardedVideoButton = null;
        [SerializeField] Button m_inviteFriendsButton = null;
        [SerializeField] TMP_Text m_descriptionText = null;
        [SerializeField] TMP_Text m_amountText = null;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            if (Application.platform!=RuntimePlatform.WebGLPlayer && ShouldDisplayRewardedVideoButton())
            {
                Bonus bonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.RewardedVideoBonus);

                m_descriptionText.text = MSG_REWARDED_VIDEO;
                m_amountText.text = "<sprite=1>"+FormatUtils.FormatPrice(bonus.CoinsSingle);
                m_rewardedVideoButton.gameObject.SetActive(true);
            }
            else
            {
                Bonus bonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Invite);

                m_descriptionText.text = MSG_INVITE_FRIENDS;
                m_amountText.text = "<sprite=1>" + FormatUtils.FormatPrice(bonus.CoinsSingle);
                m_rewardedVideoButton.gameObject.SetActive(false);
            }
        }

        private bool ShouldDisplayRewardedVideoButton()
        {
            bool shouldDisplay = false;

            bool wasAgentInitialized = VideoAdController.Instance.IsIronSourceAgentInitialized;
            bool isRewardedVideoCapped = VideoAdController.Instance.IsRewardedVideoCapped();

            if (wasAgentInitialized && !isRewardedVideoCapped)
            {
                shouldDisplay = true;
            }

            return shouldDisplay;
        }

        public void BT_RewardedVideoClick()
        {
            VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoBonus);
        }

        public void BT_InviteFriendsClick()
        {
            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
        }

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }
    }
}
