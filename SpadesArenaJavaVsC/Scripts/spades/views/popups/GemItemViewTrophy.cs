﻿using System.Collections;
using cardGames.models;

namespace spades.views.popups
{
    public class GemItemViewTrophy : MonoBehaviour
    {

        float[] m_delays = { 0f, .25f, .5f, .75f, 1f };

        [SerializeField] TMP_FontAsset m_blocked_font = null;

        [SerializeField] TMP_Text m_gem_counter_text = null;
        [SerializeField] TMP_Text m_gem_amt_text = null;
        [SerializeField] GameObject m_gemAmtBg = null;
        [SerializeField] TMP_Text m_gem_name_text = null;
        [SerializeField] TMP_Text m_gem_name_shadow_text = null;

        [SerializeField] TMP_Text m_level_unlock_text = null;
        [SerializeField] Image m_progress_image = null;

        [SerializeField] Image m_lock_image = null;
        [SerializeField] Image m_diamond_image = null;
        [SerializeField] Color[] m_colors;
        [SerializeField] Sprite[] m_diamonds;
        [SerializeField] Sprite[] m_locks;

        Animator m_animator = null;

        public void SetData(int progress, int total_steps, int total_gems_won, string title, int world_index, int unlock_level, GameObject over_light)
        {
            m_animator = GetComponentInParent<Animator>();

            int combined_index = world_index + 1;

            string trigger = "";

            if (total_gems_won > 0)
            {
                //m_gem_counter_text.gameObject.SetActive (true);
                //m_gem_counter_text.text = total_gems_won.ToString();

                trigger = "hover" + combined_index;
                over_light.SetActive(true);
                m_lock_image.gameObject.SetActive(false);
                m_gem_counter_text.gameObject.SetActive(true);
                m_diamond_image.sprite = m_diamonds[world_index];
                m_animator.SetTrigger(trigger);

                m_gem_amt_text.text = total_gems_won.ToString();
                m_gemAmtBg.SetActive(true);

            }
            else //total gems ==0
            {
                m_gemAmtBg.SetActive(false);
                trigger = "blink" + combined_index;

                if (unlock_level == -1)
                {
                    m_diamond_image.enabled = false;
                    m_lock_image.sprite = m_locks[world_index];
                    m_lock_image.enabled = true;
                    m_gem_counter_text.gameObject.SetActive(false);
                    m_level_unlock_text.text = "LOCKED";

                }
                else if (ModelManager.Instance.GetUser().GetLevel() >= unlock_level)
                {
                    m_diamond_image.sprite = m_diamonds[world_index];
                    m_diamond_image.enabled = true;
                    m_lock_image.gameObject.SetActive(false);
                    m_gem_counter_text.gameObject.SetActive(true);
                }
                else
                {
                    m_diamond_image.enabled = false;
                    m_lock_image.sprite = m_locks[world_index];
                    m_lock_image.enabled = true;
                    m_level_unlock_text.text = "LEVEL " + unlock_level;
                    m_gem_counter_text.gameObject.SetActive(false);
                }
                //m_animator.SetTrigger (trigger);
                StartCoroutine(PlayAnimDelay(m_delays[world_index], trigger));

            }
            //trigger = "blink" + combined_index;

            m_progress_image.fillAmount = (float)progress / (float)total_steps;
            m_progress_image.color = m_colors[world_index];
            m_gem_counter_text.text = progress.ToString() + "/" + total_steps.ToString();
            m_gem_name_text.text = title;
            m_gem_name_shadow_text.text = title;
            //m_gem_name_text.gameObject.transform.parent.GetComponent<RectTransform> ().localPosition = new Vector3 (x_title_offset, 0, 0);

            //m_progress_image.sprite = gem_image;
            //m_progress_image.SetNativeSize ();

        }

        private IEnumerator PlayAnimDelay(float delay, string trigger)
        {
            yield return new WaitForSecondsRealtime(delay);
            m_animator.SetTrigger(trigger);
        }

    }
}