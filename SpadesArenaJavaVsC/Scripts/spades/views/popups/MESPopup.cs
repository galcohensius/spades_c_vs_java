#pragma warning disable RECS0060 // Warns when a culture-aware 'IndexOf' call is used by default.

using System;
using common.views;
using System.Collections;
using common.mes;
using spades.controllers;
using common.utils;
using System.Collections.Generic;
using common.controllers;
using common.views.popups;

namespace spades.views.popups
{

    public class MESPopup : PopupBase
    {
        Dictionary<string, Transform>[] m_all_templates = new Dictionary<string, Transform>[5];

        Dictionary<string, Transform> m_templates1Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_templates2Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_templates3Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_templates4Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_bonustemplates = new Dictionary<string, Transform>();



        [SerializeField] Transform m_template_1_offer_parent;
        [SerializeField] Transform m_template_2_offer_parent;
        [SerializeField] Transform m_template_3_offer_parent;
        [SerializeField] Transform m_template_4_offer_parent;
        [SerializeField] Transform m_bonus_template_parent;

        [SerializeField] GameObject popupImage = null;
        [SerializeField] GameObject closeBtn = null;
        [SerializeField] GameObject noThanksBtn = null;
        [SerializeField] GameObject m_spades_holder = null;

        string m_template_Id = "";

        IEnumerator m_timer_routine = null;
        TMP_Text m_timer_text = null;

        float m_size_factor = 1.42857f; // Scale factor for Spades: 70% -> 100%

        private MESMaterial m_material;

        bool m_clicked = false; // To prevent double click
        bool m_shown;
        bool m_cantLoad=false;

        Coroutine m_timeoutCoroutine;

        PopupClosedDelegate m_cancel_clicked;

        Texture2D m_rawTexture;

        public void InitPopup(MESMaterial m, PopupClosedDelegate cancelClicked)
        {
            InitTemplates();

            m_material = m;
            m_cancel_clicked = cancelClicked;

            if (m_material.templateId == null)
                m_template_Id = "S1";
            else
                m_template_Id = m_material.templateId;


            if (m_template_Id.StartsWith("B"))
            {
                m_size_factor = 1.5f; // Scale factor for BG
            }

            m_spades_holder.SetActive(true);
            closeBtn.SetActive(false);


            LoggerController.Instance.Log("Loading promo popup: " + m_material.source);

            // Start a timeout check
            float popupLoadTimeout = LocalDataController.Instance.GetSettingAsInt("PromoPopupLoadTimeout", 0);
            if (popupLoadTimeout > 0)
                m_timeoutCoroutine = StartCoroutine(TimeoutCheck(popupLoadTimeout));

            RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, m_material.source, m_material.id, OnGraphicLoaded,
                null,false);


        }

        private void InitTemplates()
        {
            foreach (Transform template in m_template_1_offer_parent)
            {
                m_templates1Offer.Add(template.name, template);
            }

            foreach (Transform template in m_template_2_offer_parent)
            {
                m_templates2Offer.Add(template.name, template);
            }

            foreach (Transform template in m_template_3_offer_parent)
            {
                m_templates3Offer.Add(template.name, template);
            }

            foreach (Transform template in m_template_4_offer_parent)
            {
                m_templates4Offer.Add(template.name, template);
            }

            foreach (Transform template in m_bonus_template_parent)
            {
                m_bonustemplates.Add(template.name, template);
            }


            m_all_templates[0] = m_templates1Offer;
            m_all_templates[1] = m_templates2Offer;
            m_all_templates[2] = m_templates3Offer;
            m_all_templates[3] = m_templates4Offer;
            m_all_templates[4] = m_bonustemplates;


        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            m_clicked = false;
            m_shown = true;

            // Create the sprite here if the texture arrived before showing the popup
            if (m_rawTexture!=null)
            {
                Image popup_image = popupImage.GetComponent<Image>();
                popup_image.sprite = Sprite.Create(m_rawTexture, new Rect(0, 0, m_rawTexture.width, m_rawTexture.height), new Vector2(0.5f, 0.5f));
                popup_image.SetNativeSize();
            } else if (m_cantLoad)
            {
                manager.HidePopup();
            }

        }

        private void OnGraphicLoaded(Sprite popupSprite, Texture2D rawTexture, object userData)
        {
            if (this == null)
                return;

            if (m_timeoutCoroutine != null)
                StopCoroutine(m_timeoutCoroutine);

            if (rawTexture == null)
            {
                m_manager?.HidePopup();
                m_cantLoad = true;
                return;
            }

            m_rawTexture = rawTexture;


            m_spades_holder.SetActive(false);
            Image popup_image = popupImage.GetComponent<Image>();
            popup_image.GetComponent<RectTransform>().localScale = new Vector2(m_size_factor, m_size_factor);

            if (m_shown)
            {
                // Create the sprite here if the texture arrived after showing the popup
                popup_image.sprite = Sprite.Create(m_rawTexture, new Rect(0, 0, m_rawTexture.width, m_rawTexture.height), new Vector2(0.5f, 0.5f));
                popup_image.SetNativeSize();
            }

            closeBtn.SetActive(true);

            float image_width_factored = rawTexture.width * m_size_factor;

            closeBtn.GetComponent<RectTransform>().localPosition = new Vector3(image_width_factored / 2 - 46f,
                                                                                  rawTexture.height * m_size_factor / 2 - 46f);


            noThanksBtn.GetComponent<RectTransform>().localPosition = new Vector3(0,
                                                                                  -rawTexture.height * m_size_factor / 2 + 60);

            if (m_material.CloseButtonMode == "X")
            {
                closeBtn.SetActive(true);
                noThanksBtn.SetActive(false);
            }
            else if (m_material.CloseButtonMode == "W")
            {
                closeBtn.SetActive(false);
                noThanksBtn.SetActive(false);
            }
            else if (m_material.CloseButtonMode == "L")
            {
                // link
                noThanksBtn.SetActive(true);
                closeBtn.SetActive(false);
            }
            else
            {
                noThanksBtn.SetActive(false);
                closeBtn.SetActive(true);
            }


            if (m_material.actionId == MESBase.ACTION_CLAIM_DYNAMIC_BONUS)
            {
                InitBonusTemplate();

            }
            else
            {
                InitPOTemplates();
            }

            //FadeInAnimation();

        }

        private void FadeInAnimation()
        {
            gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.01f, 0.01f, 0.01f);
            this.gameObject.SetActive(true);
            UpdatePopupAlpha(0);
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 0, "x", 0, "easeType", iTween.EaseType.linear, "time", 0f));
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 1, "x", 1, "delay", 0.15, "easeType", iTween.EaseType.easeOutBack, "time", .4f, "onComplete", "OnEnterAnimDone", "onCompleteTarget", this.gameObject));
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "time", .4f, "onupdate", "UpdatePopupAlpha", "delay", .15f));
        }

        private IEnumerator TimeoutCheck(float popupLoadTimeout)
        {
            yield return new WaitForSecondsRealtime(popupLoadTimeout);

            m_manager.HidePopup();
        }

        public void onCloseClick()
        {
            m_cancel_clicked?.Invoke();

            m_manager.HidePopup();
        }


        private void InitBonusTemplate()
        {
            Transform template;
            if (m_all_templates[4].TryGetValue(m_template_Id, out template))
            {
                BonusTemplate bt = template.GetComponent<BonusTemplate>();
                bt.PopulateTemplate(m_material, onClick);
            }
        }

        private void InitPOTemplates()
        {
            //checking for multi offer promotion popup - needs to be both multi and  one of a few ActionID's (2,17,29) 
            //- if its not keeping the normal flow
            JSONArray apm = m_material.actionParam.AsArray;

            int num_offers = 1;

            if (apm != null && apm.Count > 0)
                num_offers = apm.Count;


            //based on count of apm - i can know which template to pick - but not its sub type

            Action<int> button_action = null;

            if (m_material.actionId == MESBase.ACTION_DIRECT_BUY)
                button_action = MultiOffer_DirectBuy;
            else if (m_material.actionId == MESBase.ACTION_OPEN_TABLE)
                button_action = MultiOffer_OpenTable;
            else if (m_material.actionId == MESBase.ACTION_OPEN_ARENA)
                button_action = MultiOffer_OpenArena;

            Transform template;
            if (!m_all_templates[num_offers - 1].TryGetValue(m_template_Id, out template))
            {
                LoggerController.Instance.Log("Missing data");
            }
            else
            {
                PromotionTemplate pt = template.GetComponent<PromotionTemplate>();
                m_timer_text = pt.GetTimer();

                if (apm == null)
                {
                    //single offer
                    pt.PopulateTemplate(m_material, null, onClick);
                }
                else
                {
                    pt.PopulateTemplate(m_material, button_action, onClick);
                }
            }


            ManageTimer();
        }

        
        public void onClick()
        {
            // Prevent double click
            if (!m_clicked)
            {
                m_clicked = true;
                (MESBase.Instance as SpadesMESController).ExecuteAction(m_material);
            }
        }


        public void MultiOffer_DirectBuy(int index)
        {
            // Prevent double click
            if (!m_clicked)
            {
                m_clicked = true;
                (MESBase.Instance as SpadesMESController).ExecuteMultiOfferDirectBuyAction(m_material, index);
            }
        }

        public void MultiOffer_OpenTable(int index)
        {
            // Prevent double click
            if (!m_clicked)
            {
                m_clicked = true;
                (MESBase.Instance as SpadesMESController).ExecuteMultiOfferOpenTable(m_material, index);
                m_manager.HidePopup();
            }
        }

        public void MultiOffer_OpenArena(int index)
        {

            // Prevent double click
            if (!m_clicked)
            {
                m_clicked = true;
                (MESBase.Instance as SpadesMESController).ExecuteMultiOfferOpenArena(m_material, index);
                m_manager.HidePopup();
            }
        }


        private void ManageTimer()
        {
            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (m_material.expired > 0)
            {

                m_timer_routine = POTimer((int)m_material.expired);
                StartCoroutine(m_timer_routine);
            }
            else if (m_material.interval > 0)
            {
                m_material.expired = DateUtils.DateToUnixTimestamp(DateTime.UtcNow + TimeSpan.FromSeconds(m_material.interval));
                m_timer_routine = POTimer((int)m_material.expired);
                StartCoroutine(m_timer_routine);
            }
            else
            {
                if (m_timer_text != null)
                    m_timer_text.gameObject.SetActive(false);
            }

            // Handle timer visibility flag
            if (m_material.timerVisibility == 0)
            {
                if (m_timer_text != null)
                    m_timer_text.gameObject.SetActive(false);
            }

        }



        IEnumerator POTimer(int expired)
        {
            m_timer_text.gameObject.SetActive(true);
            while (true)
            {
                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);

                m_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            m_manager.HidePopup();
        }

        private void OnDestroy()
        {
            if (popupImage != null)
                if (popupImage.GetComponent<Image>().sprite != null)
                    Destroy(popupImage.GetComponent<Image>().sprite.texture);
        }

      

        public override void OnBackButtonClicked()
        {
            if (m_material.CloseButtonMode == "X")
            {
                onCloseClick();
            }
            else if (m_material.CloseButtonMode == "W")
            {
                //do nothing, no close button
            }
            else if (m_material.CloseButtonMode == "L")
            {
                // link
                onCloseClick();
            }
            else
            {
                onCloseClick();
            }

        }


    }
}

