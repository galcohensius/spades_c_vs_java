﻿using System.Collections;
using spades.models;
using System;
using common.views;
using common.controllers;
using System.Collections.Generic;
using common.utils;
using common.experiments;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.popups
{

    public class RoundSummaryPartnerPopup : PopupBase
    {

        [SerializeField] TMP_Text ew_bids_takes = null;
        [SerializeField] TMP_Text ew_round_bags = null;
        [SerializeField] TMP_Text ew_round_points = null;
        [SerializeField] TMP_Text ew_total_bags = null;
        [SerializeField] TMP_Text ew_bags_penalty = null;
        [SerializeField] TMP_Text ew_bonus_turn = null;
        [SerializeField] TMP_Text ew_total_pts = null;

        [SerializeField] TMP_Text ns_bids_takes = null;
        [SerializeField] TMP_Text ns_round_bags = null;
        [SerializeField] TMP_Text ns_round_points = null;
        [SerializeField] TMP_Text ns_total_bags = null;
        [SerializeField] TMP_Text ns_bags_penalty = null;
        [SerializeField] TMP_Text ns_bonus_turn = null;
        [SerializeField] TMP_Text ns_total_pts = null;

        [SerializeField] TMP_Text m_round_numebr = null;

        [SerializeField] TMP_Text m_bags_penalty_title = null;

        [SerializeField] Vector3 m_position = new Vector3(0, 0, 0);

        [SerializeField] TMP_Text m_counter_text = null;

        [SerializeField] TMP_Text m_button_text = null;

        [SerializeField] GameObject m_all_avatars = null;
        [SerializeField] List<CanvasGroup> m_lines = new List<CanvasGroup>();
        [SerializeField] GameObject[] m_win_icons = null;

        [SerializeField] List<MAvatarView> m_newAvatars = null;
        [SerializeField] List<TMP_Text> m_avatars_names = null;

        [SerializeField] Sprite first_place_image = null;

        [SerializeField] GameObject m_navigation_buttons = null;
        int m_curr_round_showing;

        int m_ns_total_pts = 0;
        int m_ew_total_pts = 0;


        int round_number;

        bool m_match_ended;
        bool m_prev_results;
        bool m_menu_results;
        bool m_ns_win = false;

        Action<Action> m_next_clicked = null;

        [SerializeField] GameObject m_button = null;


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            PlayerRoundResults ew = new PlayerRoundResults();
            PlayerRoundResults ns = new PlayerRoundResults();

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

            ew = active_match.GetLastRoundResults().GetResults(SpadesRoundResults.Positions.West_East);
            ns = active_match.GetLastRoundResults().GetResults(SpadesRoundResults.Positions.North_South);
            round_number = active_match.GetLastRoundResults().GetRoundNumber();

            m_bags_penalty_title.text = active_match.GetMatch().BagsCount + " Bag Penalty";

            if (m_prev_results)
            {

                m_button_text.text = "Close";
            }
            else
            {

                m_button_text.text = "Next";

                ExperimentsManager.Instance.RunIf(BiddingPopup.BIDDING_EXPERIMENT_ID, 2, () =>
                {
                    // Start a countdown coroutine
                    StartCoroutine(ContinueBtnCountdown());
                });
            }

            ew_bids_takes.text = ew.Takes + "/" + ew.Bids;
            ns_bids_takes.text = ns.Takes + "/" + ns.Bids;


            ew_round_bags.text = ew.Bags_round.ToString();


            ew_round_points.text = ew.Round_score.ToString();
            if (ew.Round_score > 0)
                ew_round_points.color = ManagerView.Instance.ROUND_SUMMARY_GREEN_COLOR;
            else
                ew_round_points.color = ManagerView.Instance.ROUND_SUMMARY_RED_COLOR;

            ew_total_bags.text = ew.Bags_total.ToString();
            ew_bags_penalty.text = ew.Bags_penalty.ToString();
            ew_bonus_turn.text = ew.Nil_bonus_or_penalty.ToString();

            m_ew_total_pts = ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East);

            ew_total_pts.text = m_ew_total_pts.ToString();

            ns_round_bags.text = ns.Bags_round.ToString();

            //this is for not showing in the screen the round total including the bags - as there is an order in the view and the bags are under it
            /*
            int ns_total_combined_score = ns.Round_Score_With_Penalty - ns.Bags_penalty;

            if (ns_total_combined_score != ns.Score_round + ns.Nil_bonus_or_penalty)
                LoggerController.Instance.Log ("MIS MATCH");
*/
            ns_round_points.text = ns.Round_score.ToString();

            if (ns.Round_score > 0)
                ns_round_points.color = ManagerView.Instance.ROUND_SUMMARY_GREEN_COLOR;
            else
                ns_round_points.color = ManagerView.Instance.ROUND_SUMMARY_RED_COLOR;

            ns_total_bags.text = ns.Bags_total.ToString();
            ns_bags_penalty.text = ns.Bags_penalty.ToString();
            ns_bonus_turn.text = ns.Nil_bonus_or_penalty.ToString();

            m_ns_total_pts = ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South);

            ns_total_pts.text = m_ns_total_pts.ToString();




            if (((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East) > ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South))
                m_ns_win = false;
            else
                m_ns_win = true;

            for (int i = 0; i < 4; i++)
            {
                m_newAvatars[i].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i).GetMAvatarModel());
                m_avatars_names[i].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i).GetMAvatarModel().NickName);
            }
            m_win_icons[0].gameObject.SetActive(false);
            m_win_icons[1].gameObject.SetActive(false);
            m_win_icons[2].gameObject.SetActive(false);
            m_win_icons[3].gameObject.SetActive(false);

            m_counter_text.text = "";



            if (m_match_ended)
                m_round_numebr.text = "Final score";
            else
                m_round_numebr.text = "Round " + round_number;

            if (!m_menu_results)
            {
                StartCoroutine(ShowLines());
                CardGamesSoundsController.Instance.SummaryScreen();
            }
            else
            {
                for (int i = 0; i < m_lines.Count; i++)
                    m_lines[i].alpha = 1;

                ShowEndPart();
            }

        }

        public override void Close()
        {
            base.Close();
            StopAllCoroutines();
        }

        public void SetMatchEnded(bool match_ended, bool prev_results, bool menu_results = false)
        {
            m_match_ended = match_ended;
            m_prev_results = prev_results;
            m_menu_results = menu_results;

            if (menu_results)
            {
                m_navigation_buttons.SetActive(true);

            }
            else
            {
                m_navigation_buttons.SetActive(false);

            }

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            m_curr_round_showing = active_match.GetLastRoundResults().GetRoundNumber();
            SetNavigationButtons();
        }

        public void NavigationButtonClicked(bool up)
        {

            if (up)
            {
                m_curr_round_showing++;
                SetNavigationButtons();
                ShowNavigationRoundResults();
            }
            else
            {
                m_curr_round_showing--;
                SetNavigationButtons();
                ShowNavigationRoundResults();
            }

        }

        private void SetNavigationButtons()
        {

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

            int num_rounds = active_match.GetRoundsCount();


            if (m_curr_round_showing >= num_rounds)
            {

                m_navigation_buttons.transform.Find("Up").gameObject.SetActive(false);

            }
            else
            {
                m_navigation_buttons.transform.Find("Up").gameObject.SetActive(true);

            }

            if (m_curr_round_showing <= 1)
            {

                m_navigation_buttons.transform.Find("Down").gameObject.SetActive(false);
            }

            else
            {
                m_navigation_buttons.transform.Find("Down").gameObject.SetActive(true);

            }



        }


        private void ShowNavigationRoundResults()
        {
            PlayerRoundResults ew = new PlayerRoundResults();
            PlayerRoundResults ns = new PlayerRoundResults();

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

            ew = active_match.GetRoundResult(m_curr_round_showing).GetResults(SpadesRoundResults.Positions.West_East);
            ns = active_match.GetRoundResult(m_curr_round_showing).GetResults(SpadesRoundResults.Positions.North_South);
            round_number = active_match.GetRoundResult(m_curr_round_showing).GetRoundNumber();

            m_bags_penalty_title.text = active_match.GetMatch().BagsCount + " Bag Penalty";

            m_button_text.text = "Close";

            ew_bids_takes.text = ew.Takes + "/" + ew.Bids;
            ns_bids_takes.text = ns.Takes + "/" + ns.Bids;

            ew_round_bags.text = ew.Bags_round.ToString();



            ew_round_points.text = ew.Round_score.ToString();

            if (ew.Round_score > 0)
                ew_round_points.color = ManagerView.Instance.ROUND_SUMMARY_GREEN_COLOR;
            else
                ew_round_points.color = ManagerView.Instance.ROUND_SUMMARY_RED_COLOR;


            ew_total_bags.text = ew.Bags_total.ToString();
            ew_bags_penalty.text = ew.Bags_penalty.ToString();
            ew_bonus_turn.text = ew.Nil_bonus_or_penalty.ToString();

            m_ew_total_pts = ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East, m_curr_round_showing + 1);

            ew_total_pts.text = m_ew_total_pts.ToString();

            ns_round_bags.text = ns.Bags_round.ToString();

            //this is for not showing in the screen the round total including the bags - as there is an order in the view and the bags are under it


            ns_round_points.text = ns.Round_score.ToString();
            if (ns.Round_score > 0)
                ns_round_points.color = ManagerView.Instance.ROUND_SUMMARY_GREEN_COLOR;
            else
                ns_round_points.color = ManagerView.Instance.ROUND_SUMMARY_RED_COLOR;


            ns_total_bags.text = ns.Bags_total.ToString();
            ns_bags_penalty.text = ns.Bags_penalty.ToString();
            ns_bonus_turn.text = ns.Nil_bonus_or_penalty.ToString();

            m_ns_total_pts = ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South, m_curr_round_showing + 1);

            ns_total_pts.text = m_ns_total_pts.ToString();


            if (((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East, m_curr_round_showing + 1) > ((SpadesActiveMatch)ModelManager.Instance.GetActiveMatch()).GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South, m_curr_round_showing + 1))
                m_ns_win = false;
            else
                m_ns_win = true;

            for (int i = 0; i < 4; i++)
            {
                m_newAvatars[i].SetAvatarModel((ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(i).GetMAvatarModel());
                m_avatars_names[i].text = TMPBidiHelper.MakeRTL((ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(i).GetMAvatarModel().NickName);
            }
            m_win_icons[0].gameObject.SetActive(false);
            m_win_icons[1].gameObject.SetActive(false);
            m_win_icons[2].gameObject.SetActive(false);
            m_win_icons[3].gameObject.SetActive(false);


            m_counter_text.text = "";

            m_round_numebr.text = "Round " + m_curr_round_showing;


            for (int i = 0; i < m_lines.Count; i++)
                m_lines[i].alpha = 1;

            m_button.SetActive(true);

        }


        public override Vector3 GetPosition()
        {
            return m_position;
        }


        private void ShowEndPart()
        {
            float delay = 5f;

            if (!m_match_ended && !m_menu_results)
            {
                m_button.SetActive(false);
                StartCoroutine(DelayClose(delay));
            }
            else
            {
                m_button.SetActive(true);
                if (!m_menu_results)
                    ShowWinnersIndications();

            }
        }

        private void ShowWinnersIndications()
        {
            //find winners and second place 
            int index = 1;

            if (m_ns_win)
                index = 0;

            if (m_ns_total_pts == m_ew_total_pts)
                index = 2;

            if (index == 2)
            {
                m_win_icons[0].GetComponent<Image>().sprite = first_place_image;
                m_win_icons[0].SetActive(true);
                iTween.ScaleFrom(m_win_icons[0], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));

                m_win_icons[1].GetComponent<Image>().sprite = first_place_image;
                m_win_icons[1].SetActive(true);
                iTween.ScaleFrom(m_win_icons[1], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));

                m_win_icons[2].GetComponent<Image>().sprite = first_place_image;
                m_win_icons[2].SetActive(true);
                iTween.ScaleFrom(m_win_icons[2], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));

                m_win_icons[3].GetComponent<Image>().sprite = first_place_image;
                m_win_icons[3].SetActive(true);
                iTween.ScaleFrom(m_win_icons[3], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));
            }
            else
            {
                if (index == 0)
                {
                    m_win_icons[0].GetComponent<Image>().sprite = first_place_image;
                    m_win_icons[0].SetActive(true);
                    iTween.ScaleFrom(m_win_icons[0], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));
                    m_win_icons[1].GetComponent<Image>().sprite = first_place_image;
                    m_win_icons[1].SetActive(true);
                    iTween.ScaleFrom(m_win_icons[1], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));
                }
                else
                {
                    m_win_icons[2].GetComponent<Image>().sprite = first_place_image;
                    m_win_icons[2].SetActive(true);
                    iTween.ScaleFrom(m_win_icons[2], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));
                    m_win_icons[3].GetComponent<Image>().sprite = first_place_image;
                    m_win_icons[3].SetActive(true);
                    iTween.ScaleFrom(m_win_icons[3], iTween.Hash("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));
                }


            }

        }

        IEnumerator ShowLines()
        {
            float delay = 0.1f;
            float appear_time = 0.15f;
            float initial_delay = 0.5f;

            yield return new WaitForSeconds(initial_delay);

            for (int i = 0; i < m_lines.Count; i++)
            {
                StartCoroutine(FadeLine(i, appear_time));
                yield return new WaitForSeconds(delay);
            }

        }

        IEnumerator FadeLine(int line_index, float appear_time)
        {
            float start_time = CurTime();
            float time_counter = 0;
            while (CurTime() < start_time + appear_time)
            {
                time_counter = (CurTime() - start_time) / appear_time;

                m_lines[line_index].alpha = time_counter;
                yield return null;
            }
            m_lines[line_index].alpha = 1f;


            if (line_index >= m_lines.Count - 1)
                ShowEndPart();

        }

        IEnumerator DelayClose(float value)
        {
            float f_start_time = CurTime();
            float time_to_move = value;
            float time_counter = 0;
            while (time_counter < 1f)
            {
                time_counter = (CurTime() - f_start_time) / time_to_move;

                int total_sec = Math.Max(0, Convert.ToInt32(value - (CurTime() - f_start_time)));

                m_counter_text.text = "Next round starts in " + total_sec + " sec.";

                yield return new WaitForSeconds(1f);
            }




            if ((ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(3).Master)
            {
                NextClicked();
            }
            else
            {
                LoggerController.Instance.Log("slave trying to close his round summary popup");
                PopupManager.Instance.HidePopup();
            }
        }

        IEnumerator ContinueBtnCountdown()
        {
            int delay = 10;
            while (delay > 0)
            {
                m_button_text.text = string.Format("Continue... {0}", delay);

                yield return new WaitForSecondsRealtime(1);

                delay--;
            }
            NextClicked();
        }


        public void NextClicked()
        {
            StopCoroutine(ContinueBtnCountdown());

            if (m_next_clicked != null)
            {
                //disable the button 
                //show spades holder

                m_button.GetComponent<Button>().interactable = false;
                ManagerView.Instance.EnableDisableButton(m_button.GetComponent<Button>());
                //first child is the moving spades - 
                m_button.transform.GetChild(0).gameObject.SetActive(true);
                //second child is the text on the button
                m_button.transform.GetChild(1).gameObject.SetActive(false);


                m_next_clicked(() =>
                {
                    // resetting data for the mavatars between the rounds
                    // **************************************************
                     SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
                     for (int i = 0; i < table.Players.Length; i++)
                     {
                         Player player = table.GetPlayer(i);
                         PlayerView playerView = ManagerView.Instance.Table.GetComponent<TableView>().Player_views[i];
                         playerView.SetModel(player);
                         playerView.SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i).GetMAvatarModel());
                     }

                    // handles the avatars in game between the rounds
                    MAvatarController.Instance.OnAvatarShownInHierarchy?.Invoke();
                    // **************************************************

                    m_all_avatars.SetActive(false);
                    PopupManager.Instance.HidePopup();
                });

            }
            else
            {
                m_all_avatars.SetActive(false);
                PopupManager.Instance.HidePopup();
            }
        }


        /// <summary>
        /// Returns the current time according to the platform.
        /// </summary>     
        private float CurTime()
        {
#if UNITY_WEBGL
            return Time.realtimeSinceStartup;
#else
            return Time.time;
#endif
        }

        public Action<Action> Next_clicked
        {
            get
            {
                return m_next_clicked;
            }
            set
            {
                m_next_clicked = value;
            }
        }
    }
}