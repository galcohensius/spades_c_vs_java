﻿using cardGames.models;
using cardGames.views.popups;
using spades.controllers;
using cardGames.controllers;
using cardGames.views.overlays;

namespace spades.views.popups
{
    public class SpadesLeaderboardRewardPopup : LeaderboardRewardPopup
    {
        protected override void HandleCollectClick()
        {
            ManagerView.Instance.EnableDisableButton(m_collectButton);
        }

        protected override void HandleLeaderboardClaimAwardSuccess(bool success, int coinsWon, int newBalance)
        {
            ModelManager.Instance.GetUser().SetCoins(newBalance);

            //flyCoins
            float scale_factor = 3f;
            float scale_timing = 0.3f;
            float scale_delay = 0.2f;

            CometOverlay coins_comet = SpadesOverlayManager.Instance.ShowCometOverlay();
            int num_coins_to_show = 5;

            coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_reward.gameObject, CardGamesPopupManager.Instance.Lobby_flight_target, 0f, 0.15f, .75f,
                CardGamesPopupManager.Instance.Lobby_coins_balance, coinsWon, CardGamesPopupManager.Instance.Lobby_coins_animator, scale_factor, scale_timing, scale_delay, TrailCompleted,
                                   new Vector2(0.7f, 0f));
        }
    }

}