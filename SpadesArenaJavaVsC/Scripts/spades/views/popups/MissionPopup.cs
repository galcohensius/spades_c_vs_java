﻿using System.Collections;
using common.views;
using spades.models;
using common.controllers;
using System;
using spades.views;
using common.utils;
using cardGames.models;
using cardGames.comm;
using cardGames.controllers;
using common.views.popups;

namespace spades.controllers
{
    public class MissionPopup : PopupBase
    {

        public const float ITEM_WIDTH = 60f;

        [SerializeField] VerticalLayoutGroup m_verticalLayoutGroup;
        [SerializeField] GameObject m_active_group;
        [SerializeField] GameObject m_over_group;

        [SerializeField] GameObject m_over_timer_group;
        [SerializeField] TMP_Text m_over_timer_text;

        [SerializeField] GameObject m_confirm_swap;

        [SerializeField] TMP_Text m_active_timer_text;
        [SerializeField] TMP_Text m_desc_title_text;
        [SerializeField] TMP_Text m_desc_body_text;

        [SerializeField] TMP_Text m_wade_group_text;
        [SerializeField] GameObject m_wade_group_progress_holder;
        [SerializeField] GameObject m_wade_group_progress_prefab;

        [SerializeField] Sprite m_wade_group_progress_empty;
        [SerializeField] Sprite m_wade_group_progress_full;


        [SerializeField] GameObject m_suitcase_effect = null;
        [SerializeField] Image m_wade_image = null;

        [SerializeField] Image m_progressBar_image;

        [SerializeField] ChallengeMissionView[] m_challenges;
        [SerializeField] InfoBubbleContainer m_infoBubbleContainer;

        [SerializeField] Image m_black_bloack_image;
        [SerializeField] Image m_swap_black_bloack_image;

        SpadesMission m_mission;
        Coroutine m_timer_routine;

        public override void Start()
        {
            base.Start();

            Transform close_button = transform.FindDeepChild("CloseButton2");

            if (close_button != null)
                close_button.gameObject.GetComponent<Button>().onClick.AddListener(CloseButtonClicked);
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            //this is to have the vertical layout properly calcuate its challenges location - its a problem when you have another popup before it in the popups list
            m_verticalLayoutGroup.gameObject.SetActive(false);
            m_verticalLayoutGroup.gameObject.SetActive(true);

            m_mission = ModelManager.Instance.GetMission() as SpadesMission;

            if (m_mission != null)
            {
                SpadesStateController.Instance.SetLastSeenMissionId(m_mission.Mission_id);
            }

            UpdateMissionData();
            CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.show_mission_popup);
            gameObject.GetComponent<Animator>().SetTrigger("play");

            ManagerView.Instance.DelayAction(1.1f, () =>
             {
                 if (m_suitcase_effect != null)
                     m_suitcase_effect.SetActive(true);
             });


        }

        public void ShowHideInfoBlackBlocker(bool show)
        {
            m_black_bloack_image.enabled = show;

            if (!show)
            {
                for (int i = 0; i < m_challenges.Length - 1; i++)
                    m_challenges[i].HideInfoBox();
            }
        }

        private void UpdateMissionData()
        {
            m_mission = ModelManager.Instance.GetMission() as SpadesMission;

            if (m_mission != null)
            {

                UpdateMegaPrizeText(m_mission.GetNumChallenges());
                //need to check if all the challenges are complete - if so need to show the over screen with timer for next mission
                bool all_challenges_complete = true;
                bool all_challenges_locked = true;

                for (int i = 0; i < m_mission.GetNumChallenges(); i++)
                {
                    if (m_mission.GetChallenge(i).Status != MissionController.ChallengeStatus.Completed)
                        all_challenges_complete = false;

                    if (m_mission.GetChallenge(i).Status != MissionController.ChallengeStatus.Locked)
                        all_challenges_locked = false;
                }

                //m_suitcase_effect.SetActive(true);
                // m_wade_image.sprite = SpadesMissionController.Instance.Suitcase_open;


                m_active_group.SetActive(!all_challenges_complete);
                m_over_group.SetActive(all_challenges_complete);

                //check if the current mission is in the past (because we clicked to show the popup and show it before we get answer for GetChallenges
                if ((m_mission.End_date - DateTime.Now).TotalSeconds < 0)
                {
                    ShowOverTimerOrHideGroup();
                }
                else if (all_challenges_complete)
                {
                    SetTimer(m_over_timer_text);
                }
                else if (all_challenges_locked)
                {
                    m_suitcase_effect.SetActive(false);
                    //m_wade_image.sprite = SpadesMissionController.Instance.Suitcase_closed;
                    SetTimer(m_over_timer_text);
                }
                else
                {
                    SetTimer(m_active_timer_text);
                    InitData2();
                }

                BuildAndSetWadeChallengeProgress();

            }
            else
            {
                //check if there is ETA for future mission
                ShowOverTimerOrHideGroup();
            }
        }

        private void UpdateMegaPrizeText(int num_challenges)
        {
            if (num_challenges == 1)
            {
                m_wade_group_text.text = "Complete the challenge to get the additional prize!";
            }
            else if (num_challenges == 2)
            {
                m_wade_group_text.text = "Complete both challenges to get the additional prize!";
            }
            else if (num_challenges == 3)
            {
                m_wade_group_text.text = "Complete all 3 challenges to get the additional prize!";
            }
            else if (num_challenges == 4)
            {
                m_wade_group_text.text = "Complete all 4 challenges to get the additional prize!";
            }
            else
            {
                m_wade_group_text.text = "";
            }

        }

        private void ShowOverTimerOrHideGroup()
        {
            if (SpadesMissionController.Instance.Next_mission_date > 0)
            {
                TimeSpan timeDiff = TimeSpan.FromSeconds(DateUtils.TotalSeconds((int)(MissionController.Instance.Next_mission_date)));

                DateTime end_date = new DateTime(timeDiff.Ticks + DateTime.Now.Ticks);

                TimeSpan delta = end_date - DateTime.Now;

                m_over_timer_group.SetActive(true);
                m_active_group.SetActive(false);
                m_over_group.SetActive(true);


                m_timer_routine = StartCoroutine(RunTimer(delta, m_over_timer_text));


            }
            else
            {
                //this is for the case of no timer - no mission
                m_over_timer_group.SetActive(false);
                m_active_group.SetActive(false);
                m_over_group.SetActive(true);
            }
        }

        private void SetTimer(TMP_Text curr_timer_text)
        {
            TimeSpan delta = m_mission.End_date - DateTime.Now;

            curr_timer_text.gameObject.SetActive(true);

            if (delta.TotalSeconds > 0)
            {
                m_timer_routine = StartCoroutine(RunTimer(delta, curr_timer_text));
            }
            else
            {
                m_over_timer_group.SetActive(false);

            }

        }



        public void Button_Confirm_Swap_Clicked(bool clicked_yes)
        {
            if (clicked_yes)
                PerformSwap();
            else
                m_swap_black_bloack_image.enabled = false;

            m_confirm_swap.SetActive(false);

        }

        public void SwapMission()
        {
            //need to check if we need to show confirm window or just do the swap
            int active_mission_index = m_mission.Active_challenge_index;

            float active_challenge_view_y = m_challenges[active_mission_index].GetComponent<RectTransform>().localPosition.y;
            m_confirm_swap.GetComponent<RectTransform>().localPosition = new Vector3(-87.5f, active_challenge_view_y + 230f, 0);
            m_confirm_swap.SetActive(true);

            m_swap_black_bloack_image.enabled = true;
        }

        private void PerformSwap()
        {
            CardGamesPopupManager.Instance.ShowWaitingIndication(true);

            CardGamesCommManager.Instance.GetChallengesMission(true, (bool success, long next_mission_date, Mission mission) =>
            {
                m_swap_black_bloack_image.enabled = false;
                if (success)
                {
                    //swapped
                    LoggerController.Instance.Log("Swapped");
                    ModelManager.Instance.SetMission(mission);
                    MissionController.Instance.Next_mission_date = next_mission_date;
                    UpdateMissionData();
                }

                CardGamesPopupManager.Instance.ShowWaitingIndication(false);

            });
        }

        public override void CloseButtonClicked()
        {
            ClosePopup();
        }

        public override void OnBackButtonClicked()
        {
            ClosePopup();
        }

        private void ClosePopup()
        {
            if (m_mission != null)
            {
                for (int i = 0; i < m_mission.GetNumChallenges(); i++)
                    m_challenges[i].HideChallengeEffects();
            }


            m_suitcase_effect.SetActive(false);
            m_confirm_swap.SetActive(false);
            m_manager.HidePopup();

        }

        private void InitData2()
        {
            for (int i = 0; i < m_mission.GetNumChallenges(); i++)
                m_challenges[i].SetChallengeData(i, m_mission.GetChallenge(i) as SpadesChallenge, CloseButtonClicked, SwapMission, m_infoBubbleContainer.InfoBubbles[i], ShowHideInfoBlackBlocker);
        }

        public void InitData(SpadesMission mission)
        {
            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            TimeSpan delta = mission.End_date - DateTime.Now;

            if (delta.TotalSeconds > 0)
            {
                m_timer_routine = StartCoroutine(RunTimer(delta, m_active_timer_text));
            }
            else
            {
                //this means the challenge has expired - we can show the next one will come in delta+24H time.
                m_timer_routine = StartCoroutine(RunTimer(delta + TimeSpan.FromHours(24), m_active_timer_text));
            }
        }

        private void BuildAndSetWadeChallengeProgress()
        {
            //clear the holder
            foreach (Transform item in m_wade_group_progress_holder.transform)
            {
                Destroy(item.gameObject);
            }

            int num_steps = m_mission.GetNumChallenges();


            for (int i = 0; i < num_steps; i++)
            {
                GameObject gameObject = (GameObject)Instantiate(m_wade_group_progress_prefab);

                float pos_x = (i - num_steps / 2) * ITEM_WIDTH + ((num_steps + 1) % 2) * ITEM_WIDTH / 2;

                gameObject.transform.SetParent(m_wade_group_progress_holder.transform);

                gameObject.GetComponent<RectTransform>().localPosition = new Vector2(pos_x, 0);
                gameObject.GetComponent<RectTransform>().localScale = new Vector2(1f, 1f);

                if (m_mission.GetChallenge(i).Status == MissionController.ChallengeStatus.Completed)
                    gameObject.GetComponent<Image>().sprite = m_wade_group_progress_full;

            }
        }

        private IEnumerator RunTimer(TimeSpan ts, TMP_Text timer)
        {
            float startTime = Time.realtimeSinceStartup;

            bool have_days = false;

            if (ts.Days > 0)
                have_days = true;

            while (ts.TotalSeconds >= 0)
            {

                timer.text = DateUtils.TimespanToDateMission(ts);


                yield return new WaitForSecondsRealtime(1f);

                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }


            //if (m_active_group.activeSelf)
            CloseButtonClicked();
            MissionController.Instance.MissionPopupTimerFinished();
        }

    }
}


