using cardGames.views.popups;

namespace spades.views.popups
{

    public class SpadesRedeemCodePopup : RedeemCodePopup
    {
        protected override void HandleEnableRedeemButton()
        {
            ManagerView.Instance.EnableDisableButton(m_redeem_btn);
        }
    }
}
