﻿namespace spades.views.popups
{
    public class winLooseView : MonoBehaviour
    {

        [SerializeField] Image m_win = null;
        [SerializeField] Image m_loose = null;

        private bool m_is_win = false;

        public bool win
        {
            get
            {
                return m_is_win;
            }
            set
            {
                m_is_win = value;

                if (m_is_win)
                {
                    m_win.enabled = true;
                    m_loose.enabled = false;
                }
                else
                {
                    m_win.enabled = false;
                    m_loose.enabled = true;
                }
            }
        }

        public void Hide()
        {
            m_win.enabled = false;
            m_loose.enabled = false;
        }
    }
}