﻿using spades.controllers;
using common.views;
using common.controllers;
using System;
using common.models;
using common.utils;
using System.Collections.Generic;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;
using cardGames.views.overlays;

namespace spades.views.popups
{
	public class InviteBonusPopup : PopupBase
	{
		private const string DESCRIPTION_TEXT_TEMPLATE_ONE = "Your friend is here and you get a <b>BONUS!</b>";
		private const string DESCRIPTION_TEXT_TEMPLATE_FEW = "Your friends are here and you get a <b>BONUS!</b>";
		private const string DESCRIPTION_TEXT_TEMPLATE_MANY = "And {0} other friends are here and you get a <b>BONUS!</b>";

		private const string MULTIPLAYER_TEXT_TEMPLATE_ONE = "{0} friend x {1}";
		private const string MULTIPLAYER_TEXT_TEMPLATE_MANY = "{0} friends x {1}";

		[SerializeField] TMP_Text m_desc_field = null;
		[SerializeField] TMP_Text m_multiplayer_field = null;
		[SerializeField] TMP_Text m_prize_field = null;
        [SerializeField] GameObject m_avatars_holder = null;

        [SerializeField]
        private List<MAvatarView> m_newAvatarViewList;

		[SerializeField] GameObject m_button_spades = null;
		[SerializeField] GameObject m_button_text = null;
		[SerializeField] Button m_button = null;
		[SerializeField] TMP_Text[] m_player_names = null;

		private InviteBonus inviteBonusModel = null;

		public override void Show(PopupManager manager)
		{
			base.Show(manager);

			m_player_names[0].gameObject.SetActive(false);
            m_player_names[1].gameObject.SetActive(false);
            m_player_names[2].gameObject.SetActive(false);
            m_newAvatarViewList[0].gameObject.SetActive(false);
            m_newAvatarViewList[1].gameObject.SetActive(false);
            m_newAvatarViewList[2].gameObject.SetActive(false);

            inviteBonusModel = (InviteBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Invite);

            if (inviteBonusModel != null)
            {

                for (int userIdx = 0; userIdx < inviteBonusModel.Inviters.Count; userIdx++)
                {
                    if (userIdx >= 3)
                    {
                        break;
                    }

                    AddAvatar(userIdx, (inviteBonusModel.Inviters[userIdx] as User).GetMAvatar());
                }
            }

            if (inviteBonusModel.Inviters.Count==2)
            {
                // Update the avatars X pos
                m_player_names[0].gameObject.transform.localPosition = new Vector2(180, -5);
                m_player_names[1].gameObject.transform.localPosition = new Vector2(-180, -5);
                m_newAvatarViewList[0].gameObject.transform.localPosition = new Vector2(180, 22);
                m_newAvatarViewList[1].gameObject.transform.localPosition = new Vector2(-180, 22);
            }

            string formatedPrizePerInvitee = FormatUtils.FormatBalance(inviteBonusModel.CoinsSingle);

			switch (inviteBonusModel.Inviters.Count)
			{
				case 1:
					m_desc_field.text = DESCRIPTION_TEXT_TEMPLATE_ONE;
					m_multiplayer_field.text = String.Format(MULTIPLAYER_TEXT_TEMPLATE_ONE, inviteBonusModel.Inviters.Count, formatedPrizePerInvitee);
					break;
				case 2:
					m_desc_field.text = DESCRIPTION_TEXT_TEMPLATE_FEW;
					m_multiplayer_field.text = String.Format(MULTIPLAYER_TEXT_TEMPLATE_MANY, inviteBonusModel.Inviters.Count, formatedPrizePerInvitee);
					break;
				case 3:
					m_desc_field.text = DESCRIPTION_TEXT_TEMPLATE_FEW;
					m_multiplayer_field.text = String.Format(MULTIPLAYER_TEXT_TEMPLATE_MANY, inviteBonusModel.Inviters.Count, formatedPrizePerInvitee);
					break;
				default:
					m_desc_field.text = String.Format(DESCRIPTION_TEXT_TEMPLATE_MANY, inviteBonusModel.Inviters.Count - 3);
					m_multiplayer_field.text = String.Format(MULTIPLAYER_TEXT_TEMPLATE_MANY, inviteBonusModel.Inviters.Count, formatedPrizePerInvitee);
					break;
			}

			m_prize_field.text = "<sprite=1>" + FormatUtils.FormatBalance(inviteBonusModel.Inviters.Count * inviteBonusModel.CoinsSingle);
		}

        private void AddAvatar(int index, MAvatarModel avatarData)
        {
            m_newAvatarViewList[index].gameObject.SetActive(true);
            m_newAvatarViewList[index].SetAvatarModel(avatarData);

            m_player_names[index].gameObject.SetActive(true);
            m_player_names[index].text = TMPBidiHelper.MakeRTL((inviteBonusModel.Inviters[index] as User).GetMAvatar().NickName);

        }


        public void BT_ClaimClick()
		{
			m_button_text.SetActive(false);
			m_button_spades.SetActive(true);
			m_button.interactable = false;
            ManagerView.Instance.EnableDisableButton(m_button);

            BonusController.Instance.ClaimBonus(Bonus.BonusTypes.Invite, 0, 0, null, 0, OnBonusClaimedCallback);
		}

		private void OnBonusClaimedCallback(bool success, BonusAwarded bonusAwarded)
		{
            m_button.gameObject.SetActive(false);

			if (success)
			{

				float scale_factor = 2f;
				float scale_timing = 0.25f;
				float scale_delay = 0.1f;

				//claim bonuses done - can start flying coins and when its done - can close the window
				CometOverlay coins_comet = SpadesOverlayManager.Instance.ShowCometOverlay();
				int num_coins_to_show = 5;

				coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_prize_field.gameObject, CardGamesPopupManager.Instance.Lobby_flight_target, 0f, 0.15f, .75f,
                    CardGamesPopupManager.Instance.Lobby_coins_balance, inviteBonusModel.Inviters.Count * inviteBonusModel.CoinsSingle, CardGamesPopupManager.Instance.Lobby_coins_animator, scale_factor, scale_timing, scale_delay, OnClosePopup, new Vector2(0.7f, 0f));
			}
			else
			{
				OnClosePopup();
			}

		}

		public void OnClosePopup()
		{
			PopupManager.Instance.HidePopup();
		}

       
		public override void OnBackButtonClicked()
		{
			BT_ClaimClick();
		}
	}
}

