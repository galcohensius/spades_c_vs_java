using System.Collections;
using common;
using spades.models;
using common.views;
using common.controllers;
using System.Collections.Generic;
using System;
using common.utils;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.popups
{

    public class SearchingForPlayersPopup : PopupBase
    {

        public delegate void SearchingPopupCancelClicked();

        const float MAX_APPEAR_DELAY = 1f;
        const float MIN_APPEAR_DELAY = 0.3f;

        [SerializeField] TMP_Text m_title_text = null;
        [SerializeField] float m_bg_darkness = 0;
        [SerializeField] GameObject m_cancel_button = null;
        [SerializeField] GameObject m_vs_title_object = null;

        [SerializeField] Sprite m_blue_bg = null;


        bool m_partner_shown = false;
        PlayingMode m_match_type = PlayingMode.Partners;

        int m_players_ready = 1;

        private SearchingPopupCancelClicked m_cancel_clicked_delegate = null;

        bool m_cancel_clicked = false;

        bool m_show_cancel_button = true;

        Match m_match;

        //used internally in adding players to window one by one
        Action m_search_finished;

        Action m_searchingPopupDone;

        bool m_isRematch;

        [SerializeField]
        private List<MAvatarView> m_playerNewAvatarViews;

        [SerializeField]
        private List<GameObject> m_emptyCircles;

        [SerializeField]
        private List<TMP_Text> m_player_names;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);
            GeneratePictureViewsAndLinks();

            m_title_text.text = "Searching For Players";
            SetPlayersPostions(m_match);

            User user = ModelManager.Instance.GetUser();
            m_playerNewAvatarViews[3].SetAvatarModel(user.GetMAvatar());
            m_player_names[3].text = TMPBidiHelper.MakeRTL(user.GetMAvatar().NickName);

            if (m_isRematch)
            {

                SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;

                if (table != null)
                    PreAssignPartnerForRematch(table.GetPlayer(1).User.GetMAvatar());

                // Hide the cancel button
                m_cancel_button.SetActive(false);
            }
            else
                m_cancel_button.SetActive(m_show_cancel_button);

        }

        public override void OnBackButtonClicked()
        {
            if (m_cancel_button != null && m_cancel_button.activeSelf && m_show_cancel_button)
                CancelClicked();
        }

        private void GeneratePictureViewsAndLinks()
        {
            for (int i = 0; i < m_playerNewAvatarViews.Count; i++)
            {
                if (i == 3)
                {
                    m_playerNewAvatarViews[i].gameObject.SetActive(true);
                }
                else
                {
                    m_playerNewAvatarViews[i].gameObject.SetActive(false);
                }
            }
        }



        public void SearchOtherPlayers(Action search_finished)
        {
            if (this == null)
                return;

            System.Random m_random = new System.Random();

            bool firstGame = true;// ModelManager.Instance.GetUser().NeverPlayed;

            m_search_finished = search_finished;
            if (m_players_ready < 4)
            {
                int empty_sits = 4 - m_players_ready;
                int num_players_appear = UnityEngine.Random.Range(1, empty_sits + 1);


                float appear_delay = (float)m_random.NextTriangular(0.3f, 1.3f, 0.5f);
                if (firstGame)
                    appear_delay = 0.2f;


                if (!m_cancel_clicked)
                    StartCoroutine(AssignPlayers(num_players_appear, appear_delay, m_players_ready));
            }
            else
            {
                m_title_text.text = "Starting Match";
                if (firstGame)
                    StartCoroutine(FinishSearchWithDelay(0.1f));
                else
                    StartCoroutine(FinishSearchWithDelay(0.5f));

            }

        }

        private void PreAssignPartnerForRematch(MAvatarModel previousPartner)
        {
            m_partner_shown = true;
            m_emptyCircles[1].SetActive(false);
            m_playerNewAvatarViews[1].SetAvatarModel(previousPartner);
            m_player_names[1].text = TMPBidiHelper.MakeRTL(previousPartner.NickName);

            m_playerNewAvatarViews[1].gameObject.SetActive(true);
            iTween.ScaleFrom(m_playerNewAvatarViews[1].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
            m_players_ready++;
        }

        public IEnumerator AssignPlayers(int num_players, float delay, int curr_ready_players, bool fill_all_mode = false)
        {
            yield return new WaitForSeconds(delay);

            CardGamesSoundsController.Instance.BobbleJoin();

            if (fill_all_mode)
            {// this is used for instant filling of the page to close for when the Master client is ready
                num_players = 4 - m_players_ready;
                curr_ready_players = m_players_ready;
            }
            if (m_match_type == PlayingMode.Solo)// 0-yellow 1-red 2-green 3-blue
            {
                for (int i = 0; i < num_players; i++)
                {
                    Player curr_player = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i + curr_ready_players - 1);
                    m_emptyCircles[i + curr_ready_players - 1].SetActive(false);
                   
                    m_playerNewAvatarViews[i + curr_ready_players - 1].SetAvatarModel(curr_player.GetMAvatarModel());
                    m_playerNewAvatarViews[i + curr_ready_players - 1].gameObject.SetActive(true);
                    m_player_names[i + curr_ready_players - 1].text = TMPBidiHelper.MakeRTL(curr_player.GetMAvatarModel().NickName);

                    iTween.ScaleFrom(m_playerNewAvatarViews[i + curr_ready_players - 1].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                    m_players_ready++;
                }
            }
            else //partners
            {


                if (curr_ready_players == 1)
                {
                    if (num_players == 1)
                    {

                        m_partner_shown = true;
                        m_emptyCircles[1].SetActive(false);
                        m_playerNewAvatarViews[1].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(1).GetMAvatarModel());
                        m_playerNewAvatarViews[1].gameObject.SetActive(true);

                        m_player_names[1].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(1).GetMAvatarModel().NickName);

                        iTween.ScaleFrom(m_playerNewAvatarViews[1].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                    }
                    else if (num_players == 2)
                    {
                        m_emptyCircles[0].SetActive(false);
                        m_playerNewAvatarViews[0].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(0).GetMAvatarModel());

                        m_player_names[0].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(0).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[0].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[0].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                        m_emptyCircles[2].SetActive(false);
                        m_playerNewAvatarViews[2].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(2).GetMAvatarModel());

                        m_player_names[2].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(2).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[2].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[2].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                    }
                    else if (num_players == 3)
                    {
                        for (int i = 0; i < num_players; i++)
                        {
                            Player curr_player = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i + curr_ready_players - 1);
                            m_emptyCircles[i + curr_ready_players - 1].SetActive(false);
                            m_playerNewAvatarViews[i + curr_ready_players - 1].SetAvatarModel(curr_player.GetMAvatarModel());

                            m_player_names[i + curr_ready_players - 1].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i + curr_ready_players - 1).GetMAvatarModel().NickName);

                            m_playerNewAvatarViews[i + curr_ready_players - 1].gameObject.SetActive(true);
                            iTween.ScaleFrom(m_playerNewAvatarViews[i + curr_ready_players - 1].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                            m_players_ready++;
                        }
                    }
                }
                else if (curr_ready_players == 2)
                {
                    if (num_players == 1)
                    {
                        m_emptyCircles[0].SetActive(false);
                        m_playerNewAvatarViews[0].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(0).GetMAvatarModel());

                        m_player_names[0].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(0).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[0].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[0].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                    }
                    else if (num_players == 2)
                    {
                        m_emptyCircles[0].gameObject.SetActive(false);
                        m_playerNewAvatarViews[0].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(0).GetMAvatarModel());

                        m_player_names[0].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(0).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[0].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[0].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                        m_emptyCircles[2].SetActive(false);
                        m_playerNewAvatarViews[2].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(2).GetMAvatarModel());

                        m_player_names[2].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(2).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[2].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[2].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                    }
                }
                else if (curr_ready_players == 3)
                {
                    if (m_partner_shown)
                    {
                        m_emptyCircles[2].SetActive(false);
                        m_playerNewAvatarViews[2].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(2).GetMAvatarModel());

                        m_player_names[2].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(2).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[2].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[2].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                    }
                    else
                    {
                        m_partner_shown = true;
                        m_emptyCircles[1].SetActive(false);
                        m_playerNewAvatarViews[1].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(1).GetMAvatarModel());

                        m_player_names[1].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(1).GetMAvatarModel().NickName);

                        m_playerNewAvatarViews[1].gameObject.SetActive(true);
                        iTween.ScaleFrom(m_playerNewAvatarViews[1].transform.parent.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeOutBack", "time", .1));
                        m_players_ready++;
                    }
                }

            }

            if (!fill_all_mode)
            {
                ButtonsOverView.ForceNormalCursor();
                m_cancel_button.SetActive(false);

                if (!m_cancel_clicked)
                    SearchOtherPlayers(m_search_finished);
            }

        }

        private IEnumerator FinishSearchWithDelay(float delay)
        {

            if (!m_cancel_clicked)
                m_searchingPopupDone?.Invoke();

            yield return new WaitForSecondsRealtime(delay);

            if (m_search_finished != null)
            {
                m_search_finished();

            }

        }

        public override void Close()
        {
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
            iTween.ScaleTo(this.gameObject, iTween.Hash("x", 0, "y", 0, "easeType", "easeInBack", "time", .3, "onComplete", "OnExitAnimDone", "onCompleteTarget", this.gameObject));
            //iTween.ScaleTo(this.gameObject, iTween.Hash("from", 1,"to",0, "time", .25f,"onupdate","UpdatePopupAlpha", "easeType", "easeOutCubic"));

            

        }

        private void SetPlayersPostions(Match match)
        {
            ISpadesMatch spadesMatch = match as ISpadesMatch;

            if (spadesMatch.Playing_mode == PlayingMode.Solo)
            {
                m_emptyCircles[0].GetComponent<Image>().sprite = m_blue_bg;
                m_emptyCircles[1].GetComponent<Image>().sprite = m_blue_bg;
                m_emptyCircles[2].GetComponent<Image>().sprite = m_blue_bg;
            }

            for (int i = 0; i < 3; i++)                     //creating players
            {

                if (spadesMatch.Playing_mode == PlayingMode.Solo)//overiding color for opposite player to also be red in solo
                {


                    m_match_type = PlayingMode.Solo;
                    m_vs_title_object.SetActive(false);

                    m_playerNewAvatarViews[1].transform.parent.GetComponent<RectTransform>().localPosition = new Vector3(190f, 0, 0);
                    m_playerNewAvatarViews[0].transform.parent.GetComponent<RectTransform>().localPosition = new Vector3(-190f, 0, 0);

                }
                else
                {
                    m_emptyCircles[1].GetComponent<Image>().sprite = m_blue_bg;

                    m_match_type = PlayingMode.Partners;

                    //swap tables for player 0 and player 1
                    Vector3 temp_position = m_playerNewAvatarViews[0].transform.parent.GetComponent<RectTransform>().localPosition;
                    m_playerNewAvatarViews[0].transform.parent.GetComponent<RectTransform>().localPosition = m_playerNewAvatarViews[1].GetComponent<RectTransform>().localPosition;
                    m_playerNewAvatarViews[1].transform.parent.GetComponent<RectTransform>().localPosition = temp_position;

                    m_playerNewAvatarViews[0].transform.parent.GetComponent<RectTransform>().localPosition = new Vector3(270f, 0, 0);
                    m_playerNewAvatarViews[1].transform.parent.GetComponent<RectTransform>().localPosition = new Vector3(-270f, 0, 0);

                    m_vs_title_object.SetActive(true);
                }

            }       //assigning players to table
        }

        public override float GetBGDarkness()
        {
            return m_bg_darkness;
        }

        public void CancelClicked()
        {
            //need to notify that all the win\lose screens can click again

            LoggerController.Instance.Log("Searching For players Cancel button clicked");

            m_cancel_clicked = true;



            if (m_cancel_clicked_delegate != null)
                m_cancel_clicked_delegate();

            m_manager.HidePopup();
        }



        public SearchingPopupCancelClicked Cancel_clicked_delegate
        {
            get
            {
                return m_cancel_clicked_delegate;
            }
            set
            {
                m_cancel_clicked_delegate = value;
            }
        }

        public int Players_ready
        {
            get
            {
                return m_players_ready;
            }
        }

        public Match Match
        {
            get
            {
                return m_match;
            }

            set
            {
                m_match = value;
            }
        }

        public bool IsRematch
        {
            get
            {
                return m_isRematch;
            }

            set
            {
                m_isRematch = value;
            }
        }

        public bool Show_cancel_button
        {
            get
            {
                return m_show_cancel_button;
            }

            set
            {
                m_show_cancel_button = value;
            }
        }

        public Action SearchingPopupDone { get => m_searchingPopupDone; set => m_searchingPopupDone = value; }
    }
}
