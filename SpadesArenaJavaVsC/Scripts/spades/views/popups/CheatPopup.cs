using System;
using common.views;
using spades.controllers;
using common.controllers;

namespace spades.views.popups
{
	public class CheatPopup:PopupBase
	{
        [SerializeField] private TMP_InputField m_bids;
        [SerializeField] private TMP_InputField m_takes;

        int m_add_bags = 0;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            m_bids.text = "1";
            m_takes.text = "1";
        }

        public void ToggleChange(bool value)
        {
            if (value)
                m_add_bags = 1;
            else
                m_add_bags = 0;
        }

        public void WinRoundClick() {
            int bids = ParseInput(m_bids);
            int takes = ParseInput(m_takes);
            RoundController.Instance.Spades_engine.EndRoundNowCheat(1,1,bids,takes);
            CloseButtonClicked ();
		}
		public void LoseRoundClick() {
            int bids = ParseInput(m_bids);
            int takes = ParseInput(m_takes);
            RoundController.Instance.Spades_engine.EndRoundNowCheat(0,1, bids, takes);
            CloseButtonClicked ();
		}
		public void WinGameClick() {
            int bids = ParseInput(m_bids);
            int takes = ParseInput(m_takes);
            RoundController.Instance.Spades_engine.EndRoundNowCheat(1,200+ m_add_bags, bids, takes);
            CloseButtonClicked ();

		}

        public void WinGame400Click()
        {
            int bids = ParseInput(m_bids);
            int takes = ParseInput(m_takes);
            RoundController.Instance.Spades_engine.EndRoundNowCheat(1, 40+ m_add_bags, bids, takes);
            CloseButtonClicked();

        }

		public void LoseGameClick() {
            int bids = ParseInput(m_bids);
            int takes = ParseInput(m_takes);
            RoundController.Instance.Spades_engine.EndRoundNowCheat(0,200+ m_add_bags, bids, takes);
            CloseButtonClicked ();

		}
		public void WinSecondClick() {
            int bids = ParseInput(m_bids);
            int takes = ParseInput(m_takes);
            RoundController.Instance.Spades_engine.EndRoundNowCheat(2,200+ m_add_bags, bids, takes);
            CloseButtonClicked ();

		}

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }

        public override void OnBackButtonClicked()
		{
            CloseButtonClicked ();
		}

        private int ParseInput(TMP_InputField input) {
            string txt = input.text;
            if (string.IsNullOrEmpty(txt))
                return 0;
            
            return Convert.ToInt32(txt);
        }
	}
}

