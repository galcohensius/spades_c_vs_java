﻿using common.controllers;
using common.views;
using spades.controllers;
using System;
using System.Collections;

namespace spades.views.popups
{

    public class AgeVerificationPopup : PopupBase
    {
        public const int MIN_TO_WAIT = 60*4;

        private const string ENTER_AGE_TEXT = "Please enter your age below:";
        private const string TRY_AGAIN_TEXT = "Please try again in:";
        private const string DONT_MEET_AGE_TEXT = "We're sorry,\nyou don't meet the minimum age requirements to play this game.";
        [SerializeField] Sprite m_idle_image=null;
        [SerializeField] Sprite m_selected_image= null;
        [SerializeField] TMP_Text m_title_text= null;
        [SerializeField] TMP_Text m_timer_text= null;
        [SerializeField] TMP_InputField m_age_input = null;

        Action m_age_verified_callback;

        int m_selected_index = -1;


        [SerializeField] Button m_continue_button = null;
        //[SerializeField] Transform m_buttons_holder = null;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            m_continue_button.interactable = false;
            ManagerView.Instance.EnableDisableButton(m_continue_button);

            m_age_input.text = "";

            string timer_set = SpadesStateController.Instance.GetAgeVerifictionTimer();

            if(timer_set==null)
            {
                //there wasnt any timer set - start fresh
            }
            else
            {
                DateTime saved_time = Convert.ToDateTime(timer_set);

                TimeSpan ts = saved_time - DateTime.Now;

                if(ts.TotalSeconds < 0)
                {
                    //timer already expired
                    m_title_text.text = ENTER_AGE_TEXT;
                    m_timer_text.gameObject.SetActive(false);
                }
                else
                {
                    //need to start a counter of the timer with the TS that is left
                    //m_buttons_holder.gameObject.SetActive(false);
                    m_title_text.text = DONT_MEET_AGE_TEXT+" "+TRY_AGAIN_TEXT;
                    m_timer_text.text = "";
                    m_timer_text.gameObject.SetActive(true);
                    m_continue_button.gameObject.SetActive(false);
                    m_age_input.gameObject.SetActive(false);

                    StartCoroutine(ShowTimer(ts));
                }
            }
            
        }

        public void OnAgeValueChanged() {

            m_continue_button.interactable = false;
            ManagerView.Instance.EnableDisableButton(m_continue_button);

            string age = m_age_input.text.Trim();
            if (!string.IsNullOrEmpty(age)) {
                m_continue_button.interactable = true;
                ManagerView.Instance.EnableDisableButton(m_continue_button);
            }
        }

        private IEnumerator ShowTimer(TimeSpan ts)
        {
            float startTime = Time.realtimeSinceStartup;

            while (ts.TotalSeconds>=0)
            {
                m_timer_text.text = string.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);

                yield return new WaitForSecondsRealtime(1f);

                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }

            m_title_text.text = ENTER_AGE_TEXT;
            m_timer_text.gameObject.SetActive(false);
            m_continue_button.gameObject.SetActive(true);
            m_age_input.gameObject.SetActive(true);

            /*
            m_buttons_holder.gameObject.SetActive(true);
            foreach (Transform t in m_buttons_holder)
            {
                t.gameObject.GetComponent<Image>().sprite = m_idle_image;
            }
            */

        }



        public void SetCallback(Action age_confrimed)
        {
            m_age_verified_callback = age_confrimed;
        }

        /*
        public void Button_Age_Clicked(int button_index)
        {
            if (m_selected_index != -1)
                m_buttons_holder.GetChild(m_selected_index).GetComponent<Image>().sprite = m_idle_image;

            if(button_index !=m_selected_index)
                m_selected_index = button_index;

            m_buttons_holder.GetChild(button_index).GetComponent<Image>().sprite = m_selected_image;

            m_continue_button.SetActive(true);
        }
        */

        public void Button_Continue_Clicked()
        {
            int age = 0;
            try {
                age = Convert.ToInt32(m_age_input.text);
            } catch(Exception) {
            }

            if(age<18)
            {
                SpadesStateController.Instance.IncrementAgeVerificationFailed();

                //need to check if its first or second time - this should be saved in State_Controller
                if(SpadesStateController.Instance.GetAgeVerificationFailed()==1)
                {
                    //show first message
                    PopupManager.Instance.ShowMessagePopup(DONT_MEET_AGE_TEXT, PopupManager.AddMode.ShowAndKeep, true, () =>
                    {
                        //do nothing
                    }, "CLOSE");
                }
                else
                {
                    //show second message
                    PopupManager.Instance.ShowMessagePopup(DONT_MEET_AGE_TEXT+ "\nPlease try again in 4 hours.", PopupManager.AddMode.ShowAndKeep, true, () =>
                    {
                        //set
                        SpadesStateController.Instance.IncrementAgeVerificationFailed(true);
                        SpadesStateController.Instance.SetAgeVerificationTimer(MIN_TO_WAIT);
                    }, "CLOSE");
                }
            }
            else
            {
                if (m_age_verified_callback != null)
                {
                    m_manager.HidePopup();
                    m_age_verified_callback();
                }
                    

            }
        }

        public override float GetBGDarkness()
        {
            return 1;
        }

    }
}
