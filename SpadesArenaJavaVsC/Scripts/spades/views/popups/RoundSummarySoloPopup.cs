﻿using System.Collections;
using spades.models;
using System;
using common.views;
using common.controllers;
using System.Collections.Generic;
using common.utils;
using common.experiments;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.popups
{

	public class RoundSummarySoloPopup : PopupBase
	{
		
		[SerializeField] TMP_Text[] m_bids_takes = null;
		[SerializeField] TMP_Text[] m_round_bags = null;
		[SerializeField] TMP_Text[] m_nil_bonus = null;
		[SerializeField] TMP_Text[] m_round_points = null;
		[SerializeField] TMP_Text[] m_total_bags = null;
		[SerializeField] TMP_Text[] m_bags_penalty = null;
		[SerializeField] TMP_Text[] m_total_pts = null;
		[SerializeField] GameObject[]	m_win_icons = null;

		[SerializeField] TMP_Text m_bags_penalty_title = null;

		[SerializeField]GameObject m_all_avatars = null;
		[SerializeField]List<CanvasGroup>	m_lines = new List<CanvasGroup> ();

        [SerializeField] List<MAvatarView> m_newAvatars = null;
        [SerializeField] List<TMP_Text> m_avatars_names = null;

        [SerializeField]Sprite first_place_image = null;
		[SerializeField]Sprite second_place_image = null;


		[SerializeField] TMP_Text m_round_number_txt = null;


		[SerializeField] Vector3	m_position = new Vector3 (0, 0, 0);

		[SerializeField]TMP_Text m_counter_text = null;

        [SerializeField]TMP_Text m_button_text= null;

        [SerializeField]GameObject m_navigation_buttons= null;

		Action<Action>					m_next_clicked=null;

		int m_curr_round_showing;
		int	round_number;

		bool m_match_ended;
		bool m_prev_results;
		bool m_menu_results;

		[SerializeField]	GameObject	m_button = null;

        public override void Show(PopupManager manager)
        {


            base.Show(manager);



            PlayerRoundResults[] player_round_results = new PlayerRoundResults[4];

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

            for (int i = 0; i < 4; i++)
                player_round_results[i] = new PlayerRoundResults();

            for (int i = 0; i < 4; i++)
                player_round_results[i] = active_match.GetLastRoundResults().GetResults((SpadesRoundResults.Positions)(3 - i));

            round_number = active_match.GetLastRoundResults().GetRoundNumber();

            m_bags_penalty_title.text = active_match.GetMatch().BagsCount + " Bag Penalty";

            if (m_prev_results)
            {

                m_button_text.text = "Close";
            }
            else
            {

                m_button_text.text = "Next";

                ExperimentsManager.Instance.RunIf(BiddingPopup.BIDDING_EXPERIMENT_ID, 2, () =>
                {
                    // Start a countdown coroutine
                    StartCoroutine(ContinueBtnCountdown());
                });
            }


            for (int i = 0; i < 4; i++)
            {
                m_bids_takes[i].text = player_round_results[i].Takes + "/" + player_round_results[i].Bids;
                m_round_bags[i].text = player_round_results[i].Bags_round.ToString();


                if (player_round_results[i].Round_score > 0)
                {
                    m_round_points[i].text = player_round_results[i].Round_score.ToString();
                    m_round_points[i].color = ManagerView.Instance.ROUND_SUMMARY_GREEN_COLOR;

                }

                else
                {
                    m_round_points[i].text = player_round_results[i].Round_score.ToString();
                    m_round_points[i].color = ManagerView.Instance.ROUND_SUMMARY_RED_COLOR;
                }

                m_total_bags[i].text = player_round_results[i].Bags_total.ToString();
                m_bags_penalty[i].text = player_round_results[i].Bags_penalty.ToString();
                m_nil_bonus[i].text = player_round_results[i].Nil_bonus_or_penalty.ToString();

                int combined = 3 - i;

                m_total_pts[i].text = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)(combined)).ToString();

                m_newAvatars[i].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(combined).GetMAvatarModel());
                m_avatars_names[i].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(combined).GetMAvatarModel().NickName);

                m_win_icons[i].gameObject.SetActive(false);
            }

            m_counter_text.text = "";


            if (m_match_ended)
                m_round_number_txt.text = "Final score";
            else
                m_round_number_txt.text = "Round " + round_number;

            if (!m_menu_results)
            {
                StartCoroutine(ShowLines());
                CardGamesSoundsController.Instance.SummaryScreen();
            }
            else
            {
                for (int i = 0; i < m_lines.Count; i++)
                    m_lines[i].alpha = 1;

                ShowEndPart();
            }

        }

        public void SetMatchEnded (bool match_ended, bool prev_results, bool menu_results = false)
		{
			m_match_ended = match_ended;
			m_prev_results = prev_results;
			m_menu_results = menu_results;

			if (menu_results)
			{
				m_navigation_buttons.SetActive (true);
                m_round_number_txt.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(-475, m_round_number_txt.gameObject.GetComponent<RectTransform>().localPosition.y, 0);

            }
			else
			{
				m_navigation_buttons.SetActive (false);
				
			}
			SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch ();
			m_curr_round_showing = active_match.GetLastRoundResults ().GetRoundNumber ();
			SetNavigationButtons ();
		}

		public void NavigationButtonClicked (bool up)
		{

			if (up)
			{
				m_curr_round_showing++;
				SetNavigationButtons ();
				ShowNavigationRoundResults ();
			}
			else
			{
				m_curr_round_showing--;
				SetNavigationButtons ();
				ShowNavigationRoundResults ();
			}

		}

        private void SetNavigationButtons()
        {

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

            int num_rounds = active_match.GetRoundsCount();


            if (m_curr_round_showing >= num_rounds)
            {
                
                m_navigation_buttons.transform.Find("Up").gameObject.SetActive(false);

            }
            else
            {
                m_navigation_buttons.transform.Find("Up").gameObject.SetActive(true);
                
            }

            if (m_curr_round_showing <= 1)
            {
               
                m_navigation_buttons.transform.Find("Down").gameObject.SetActive(false);
            }

            else
            {
                m_navigation_buttons.transform.Find("Down").gameObject.SetActive(true);
               
            }



        }


        private void ShowNavigationRoundResults ()
		{
			PlayerRoundResults[] player_round_results = new PlayerRoundResults[4];

			SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

			for (int i = 0; i < 4; i++)
				player_round_results [i] = new PlayerRoundResults ();

			for (int i = 0; i < 4; i++)
				player_round_results [i] = active_match.GetRoundResult (m_curr_round_showing).GetResults ((SpadesRoundResults.Positions)(3 - i));

			round_number = active_match.GetLastRoundResults ().GetRoundNumber ();

			m_bags_penalty_title.text = active_match.GetMatch ().BagsCount + " Bag Penalty";

			if (m_prev_results)
				m_button_text.text = "Close";
			else
				m_button_text.text = "Next";

			for (int i = 0; i < 4; i++)
			{
				m_bids_takes [i].text = player_round_results [i].Takes + "/" + player_round_results [i].Bids;
				m_round_bags [i].text = player_round_results [i].Bags_round.ToString ();

                if (player_round_results[i].Round_score > 0)
                {
                    m_round_points[i].text = player_round_results[i].Round_score.ToString();
                    m_round_points[i].color = ManagerView.Instance.ROUND_SUMMARY_GREEN_COLOR;

                }

                else
                {
                    m_round_points[i].text =player_round_results[i].Round_score.ToString();
                    m_round_points[i].color = ManagerView.Instance.ROUND_SUMMARY_RED_COLOR;
                }
                    


				m_total_bags [i].text = player_round_results [i].Bags_total.ToString ();
				m_bags_penalty [i].text = player_round_results [i].Bags_penalty.ToString ();
				m_nil_bonus [i].text = player_round_results [i].Nil_bonus_or_penalty.ToString ();

				int combined = 3 - i;

				m_total_pts [i].text = active_match.GetCalculatedTotalResults ((SpadesRoundResults.Positions)(combined), m_curr_round_showing + 1).ToString ();

                m_newAvatars[i].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(combined).GetMAvatarModel());
                m_avatars_names[i].text = TMPBidiHelper.MakeRTL(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(combined).GetMAvatarModel().NickName);

                m_win_icons [i].gameObject.SetActive (false);
			}

			m_counter_text.text = "";

			m_round_number_txt.text = "Round " + m_curr_round_showing;


			for (int i = 0; i < m_lines.Count; i++)
				m_lines [i].alpha = 1;

			m_button.SetActive (true);

		}

		public override Vector3 GetPosition ()
		{
			return m_position;
		}


		private void ShowEndPart ()
		{
			
			if (!m_match_ended && !m_menu_results)
			{
				m_button.SetActive (false);
				StartCoroutine (DelayClose (5));
			}
			else
			{
				m_button.SetActive (true);
				if (!m_menu_results)
					ShowWinnersIndications ();


			}
		}

		IEnumerator ShowLines ()
		{
			float delay = 0.1f;
			float appear_time = 0.15f;
			float initial_delay = 0.5f;

			yield return new WaitForSeconds (initial_delay);

			for (int i = 0; i < m_lines.Count; i++)
			{
				StartCoroutine (FadeLine (i, appear_time));
				yield return new WaitForSeconds (delay);
			}

		}

		IEnumerator FadeLine (int line_index, float appear_time)
		{
            float start_time = CurTime();
			float time_counter = 0;
            while (CurTime() < start_time + appear_time)
			{
                time_counter = (CurTime() - start_time) / appear_time;

				m_lines [line_index].alpha = time_counter;
				yield return null;
			}
			m_lines [line_index].alpha = 1f;


			if (line_index >= m_lines.Count - 1)
				ShowEndPart ();

		}

		private List<int> GetWinner (int[]	data)
		{
			int highestscore = -50000;
			List<int>	winners = new List<int> ();

			for (int i = 0; i < 4; i++)//finding out what was the highest score
				if (data [i] > highestscore)
				{
					highestscore = data [i];
				}

			for (int i = 0; i < 4; i++)//finding out if there are multiple winners
			{
				if (data [i] == highestscore)
					winners.Add (i);
			}

			return winners;
		}

		private void ShowWinnersIndications ()
		{
			//find winners and second place 

			int[] total_scores = new int[4];
			List<int>	winners = new List<int> ();
			SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

			for (int i = 0; i < 4; i++)
				total_scores [i] = active_match.GetCalculatedTotalResults ((SpadesRoundResults.Positions)(3 - i));
			
			winners = GetWinner (total_scores);

			for (int i = 0; i < winners.Count; i++)
			{
				m_win_icons [winners [i]].GetComponent<Image> ().sprite = first_place_image;
				m_win_icons [winners [i]].SetActive (true);

				iTween.ScaleFrom (m_win_icons [winners [i]], iTween.Hash ("x", 3, "y", 3, "easeType", "easeInQuad", "time", .15));

			}
			///

			if (winners.Count == 1)
			{
				for (int i = 0; i < 4; i++)
					if (total_scores [i] == total_scores [winners [0]])
						total_scores [i] = -10000;

				winners.Clear ();
				winners = GetWinner (total_scores);

				for (int i = 0; i < winners.Count; i++)
				{
					m_win_icons [winners [i]].GetComponent<Image> ().sprite = second_place_image;
					m_win_icons [winners [i]].SetActive (true);
				}
			}

		}



		IEnumerator DelayClose (float value)
		{
            float f_start_time = CurTime();
			float time_to_move = value;	
			float time_counter = 0;
			while (time_counter < 1f)
			{
                time_counter = (CurTime() - f_start_time) / time_to_move; 

                int total_sec = Math.Max(0,Convert.ToInt32 (value - (CurTime() - f_start_time)));

				m_counter_text.text = "Next round starts in " + total_sec + " sec.";

				yield return new WaitForSeconds (1f);
			}

			if (((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(3).Master)
			{
				LoggerController.Instance.Log ("Master did play next");
				NextClicked ();
			}
			else
			{
				LoggerController.Instance.Log ("slave trying to close his round summary popup");
				PopupManager.Instance.HidePopup ();
			}
		}

        IEnumerator ContinueBtnCountdown()
        {
            int delay = 10;
            while (delay > 0)
            {
                m_button_text.text = string.Format("Continue... {0}", delay);

                yield return new WaitForSecondsRealtime(1);

                delay--;
            }
            NextClicked();
        }

		public void NextClicked ()
		{

			if (m_next_clicked != null)
			{
				//disable the button 
				//show spades holder

				m_button.GetComponent<Button> ().interactable = false;
                ManagerView.Instance.EnableDisableButton(m_button.GetComponent<Button>());

                //first child is the moving spades - 
                m_button.transform.GetChild (0).gameObject.SetActive (true);
				//second child is the text on the button
				m_button.transform.GetChild (1).gameObject.SetActive (false);


				m_next_clicked (()=>{

					// resetting data for the mavatars between the rounds
					// **************************************************
					SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
					for (int i = 0; i < table.Players.Length; i++)
					{
						Player player = table.GetPlayer(i);
						PlayerView playerView = ManagerView.Instance.Table.GetComponent<TableView>().Player_views[i];
						playerView.SetModel(player);
						playerView.SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i).GetMAvatarModel());
					}

					// handles the avatars in game between the rounds
					MAvatarController.Instance.OnAvatarShownInHierarchy?.Invoke();
					// **************************************************

					m_all_avatars.SetActive (false);
					PopupManager.Instance.HidePopup ();

				});



			}
			else
			{
				m_all_avatars.SetActive (false);
				PopupManager.Instance.HidePopup ();
			}
		}

       

        /// <summary>
        /// Returns the current time according to the platform.
        /// </summary>     
        private float CurTime()
        {
#if UNITY_WEBGL
            return Time.realtimeSinceStartup;
#else
            return Time.time;
#endif
        }

		public Action<Action> Next_clicked {
			get {
				return m_next_clicked;
			}
			set{
				m_next_clicked = value;
			}
		}

		
	}
}