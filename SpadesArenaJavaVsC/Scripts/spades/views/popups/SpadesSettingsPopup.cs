﻿using cardGames.models;
using cardGames.views.popups;
using common.apple;
using common.controllers;
using common.facebook;
using common.facebook.views;
using spades.controllers;

namespace spades.views.popups
{

    public class SpadesSettingsPopup : SettingsPopup
    {

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

#if UNITY_IOS
            m_BG_image.sizeDelta = new Vector2(1200, 910);
#else
            m_BG_image.sizeDelta = new Vector2(1200, 820);
#endif

        }


        protected override void ReLoginAfterAppleSignIn(AppleData appleData)
        {
            if (appleData != null && appleData.SignedIn)
            {
                 LobbyController.Instance.LoginWithApple(appleData);
            }
        }

        public override void FacebookButtonClicked()
        {
            if (m_loginProcessActive) return;

            m_loginProcessActive = true;

            if (FacebookController.Instance.IsLoggedIn())
            {
                m_facebook_button.SetMode(FacebookButton.ButtonMode.Waiting);
                LobbyController.Instance.FBLogOut((bool success) =>
                {
                    ModelManager.Instance.Social_model.AppRequestsChanged.Invoke(0, false);

                    if (this == null)
                        return;

                    UpdateFBButton();
                    SetButtons();

                    m_loginProcessActive = false;
                });
            }
            else
            {
                m_facebook_button.SetMode(FacebookButton.ButtonMode.Waiting);

                LobbyController.Instance.FBLogin((bool success) =>
                {
                    if (this == null)
                        return;

                    if (success)
                    {
                        ManagePicAndName();
                        SetButtons();
                    }
                    UpdateFBButton();

                    m_loginProcessActive = false;

                });
            }
        }

        public override void LoginAfterQAEnvChanged()
        {
            m_manager.HidePopup();
            LobbyController.Instance.LoginAfterSessionTimeout();
        }
    }
}
