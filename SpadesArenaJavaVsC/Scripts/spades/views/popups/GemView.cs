﻿namespace spades.views.popups
{
    public class GemView : MonoBehaviour
    {

        [SerializeField] Image m_active_gem = null;
        [SerializeField] Image m_disabled_gem = null;

        [SerializeField] TMP_Text m_gem_count_text = null;
        [SerializeField] Image m_gem_bg = null;

        private int m_gem_count = 0;



        public int GemCount
        {
            get
            {
                return m_gem_count;
            }
            set
            {
                m_gem_count = value;

                if (m_gem_count > 0)
                {
                    m_active_gem.enabled = true;
                    m_disabled_gem.enabled = false;
                    m_gem_count_text.enabled = true;
                    m_gem_count_text.text = m_gem_count.ToString();
                    m_gem_bg.enabled = true;
                }
                else
                {
                    m_gem_count_text.enabled = false;
                    m_active_gem.enabled = false;
                    m_disabled_gem.enabled = true;
                    m_gem_bg.enabled = false;
                }
            }
        }
    }
}