﻿using System.Collections;
using cardGames.models;

namespace spades.views
{

	public class SocialButtonView : MonoBehaviour {

		[SerializeField]TMP_Text		m_counter=null;	

		Coroutine						m_punch_counter_coroutine=null;
		bool							m_running=false;

		public void Start()
		{
			
			ModelManager.Instance.Social_model.AppRequestsChanged += CounterValueChanged;
		}

		private void CounterValueChanged(int new_value, bool added)
		{
			if (new_value <= 0)
			{
				m_counter.transform.parent.gameObject.SetActive (false);

                // Not used for now (Ran 30/8/18)
                //SpadesNotificationController.Instance.CancelPendingNotifications(SpadesNotificationController.FORGOT_GIFT_NOTIF_ID);
            }
            else
            {
                m_counter.text = new_value.ToString();
                m_counter.transform.parent.gameObject.SetActive (true);

                // Not used for now (Ran 30/8/18)
                //SpadesNotificationController.Instance.ScheduleForgetGiftNotification();

			}
		}

		public void OnEnable()
		{
			
			m_punch_counter_coroutine = StartCoroutine (PunchCounter());
		}

		public void OnDisable()
		{
			StopCoroutine (m_punch_counter_coroutine);
		}

		IEnumerator PunchCounter()
		{
			m_running = true;

			while (true)
			{
				
				iTween.PunchRotation(m_counter.transform.parent.gameObject, iTween.Hash("z", 40,"islocal",true));
				yield return new WaitForSecondsRealtime (3);
			}


		}

	}
}