﻿using spades.models;
using common.utils;
using cardGames.models;

namespace spades.views

{
    //TODO: generalize this to cardGames namespace to be used derived for Spades and Gin.
    //NOTE: since there's a relying on structure and Spades specific user stats this may remain as is.
	public class StatsView : MonoBehaviour {


		[SerializeField] GameObject 			m_stats_parent=null;

		public void DisplayStats(UserStats user_stats)
		{
            if (!(user_stats is SpadesUserStats))
            {
                throw new System.Exception("UserStats of wrong type parsed to Spades StatsView");
            }
			SpadesUserStats m_user_stats = user_stats as SpadesUserStats;

			string winRatioFormattedSolo = string.Format ("{0:P1}", m_user_stats.MatchWinRatioSolo);
			string winRatioFormattedPartner = string.Format ("{0:P1}", m_user_stats.MatchWinRatioPartner);

			string bids_met = string.Format ("{0:P1}", m_user_stats.ExactBidsRatio);

			string nil_bids_met = string.Format ("{0:P1}", m_user_stats.NilTakesRatio);

			m_stats_parent.transform.GetChild (0).GetComponent<TMP_Text> ().text = m_user_stats.MatchPlayedPartner.ToString();
			m_stats_parent.transform.GetChild (1).GetComponent<TMP_Text> ().text = m_user_stats.MatchWinsPartner.ToString ();
			m_stats_parent.transform.GetChild (2).GetComponent<TMP_Text> ().text = winRatioFormattedPartner;
			m_stats_parent.transform.GetChild (3).GetComponent<TMP_Text> ().text = m_user_stats.ArenaPlayed.ToString ();
			m_stats_parent.transform.GetChild(4).GetComponent<TMP_Text>().text = FormatUtils.FormatPrice(m_user_stats.CoinsWon);
			m_stats_parent.transform.GetChild(5).GetComponent<TMP_Text>().text = bids_met;

			m_stats_parent.transform.GetChild(6).GetComponent<TMP_Text>().text = m_user_stats.MatchPlayedSolo.ToString();
			m_stats_parent.transform.GetChild(7).GetComponent<TMP_Text>().text = m_user_stats.MatchWinsSolo.ToString();
			m_stats_parent.transform.GetChild(8).GetComponent<TMP_Text>().text = winRatioFormattedSolo;
			m_stats_parent.transform.GetChild(9).GetComponent<TMP_Text>().text = m_user_stats.RoundPlayed.ToString ();
			m_stats_parent.transform.GetChild(10).GetComponent<TMP_Text>().text = FormatUtils.FormatPrice(m_user_stats.BiggestCoinsWin);
			m_stats_parent.transform.GetChild(11).GetComponent<TMP_Text>().text = nil_bids_met;

		}
	}
}