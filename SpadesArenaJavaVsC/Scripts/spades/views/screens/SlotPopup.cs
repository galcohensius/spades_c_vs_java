using System.Collections;
using System.Collections.Generic;
using common.views;
using common.controllers;
using spades.controllers;
using common.models;
using System;
using common;
using common.facebook;
using common.utils;
using common.facebook.views;
using common.facebook.models;
using cardGames.models;
using cardGames.controllers;
using cardGames.views.popups;
using cardGames.views.overlays;
using common.ims;

namespace spades.views.screens
{

    public class SlotPopup : PopupBase
    {

        public const string SLOT_VIDEO_BANNER = "V1";

        const string SHARE_ID = "300";

        float m_wheels_start_delay = 0.1f;
        float m_wheels_stop_delay = 0.75f;

        const int NUM_ICONS = 7;

        List<int> m_pay_table = new List<int>();
        int m_winner;
        int m_day;

        DailyBonus m_bonus;

        [SerializeField] Sprite m_curr_day_on = null;
        [SerializeField] Sprite m_curr_day_off = null;
        [SerializeField] Sprite m_curr_friends_on = null;
        [SerializeField] Sprite m_curr_friends_off = null;
        [SerializeField] Sprite m_pay_table_on = null;
        [SerializeField] Sprite m_pay_table_off = null;

        [SerializeField] List<TMP_Text> m_pay_lines = new List<TMP_Text>();
        [SerializeField] List<Image> m_pay_lines_bg = new List<Image>();
        [SerializeField] List<Image> m_pay_lines_bg_lit = new List<Image>();
        [SerializeField] List<GameObject> m_days_bg = new List<GameObject>();
        [SerializeField] List<GameObject> m_friends_bg = new List<GameObject>();

        [SerializeField] Animator m_slot_animator = null;
        [SerializeField] Animator m_summary_animator = null;
        [SerializeField] Animator m_paytable_animator = null;

        [SerializeField] List<Animator> m_wheels_animator = new List<Animator>();
        [SerializeField] List<GameObject> m_wheels_objects = new List<GameObject>();

        [SerializeField] List<GameObject> m_item_wheels = new List<GameObject>();
        [SerializeField] List<Sprite> m_icons_sprite = new List<Sprite>();

        [SerializeField] TMP_Text m_win_text = null;
        [SerializeField] TMP_Text m_friends_text = null;
        [SerializeField] TMP_Text m_day_text = null;
        [SerializeField] TMP_Text m_total_text = null;



        [SerializeField] GameObject m_close_button = null;

        [SerializeField] TMP_Text m_bonus_timer_text = null;
        [SerializeField] List<TMP_Text> m_daily_multiplyer_text = new List<TMP_Text>();
        [SerializeField] List<TMP_Text> m_friends_value_text = new List<TMP_Text>();

        [SerializeField] Button m_visual_button = null;
        [SerializeField] Button m_hidden_button = null;


        [SerializeField] FacebookShareView m_fb_share_view = null;
        [SerializeField] GameObject m_coins_emmiter = null;

        [SerializeField] GameObject m_slot_electricity_effect_object = null;

        [SerializeField] FacebookButton m_facebook_connect_btn = null;


        [SerializeField] GameObject m_invite_button = null;
        [SerializeField] GameObject m_showVideo_button = null;
        [SerializeField] TMP_Text m_showVideoAmountText = null;

        [SerializeField] GameObject m_slot_back_glow = null;
        [SerializeField] GameObject m_slot_back_glitter = null;


        [SerializeField] TMP_Text m_facebookConnectDescriptionText = null;

        private const string FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG = "Get <color=#ffff0f>{0}</color> coins!";


        int m_curr_day_to_show;
        int m_curr_friends_to_show;
        bool m_answer_from_server = false;
        bool m_spin_enabled = false;
        float m_day_factor;
        int m_friends_factor;
        int m_total_awarded_coins;

        float m_xp_icon_width = 42f;

        //both a flag and counter - if 0 - no video - if bigger - coins amount
        int m_showVideo = 0;

        IEnumerator[] m_pay_lines_blink_coroutine = new IEnumerator[10];

        private IEnumerator m_counter_coroutine;

        [SerializeField] GameObject m_click_to_continue = null;

        public override void Start()
        {
            base.Start();
            m_facebook_connect_btn.GetComponent<Button>().onClick.AddListener(delegate { BT_ConnectClicked(); });
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
            GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);

            CardGamesSoundsController.Instance.StartSlotIntro(true);

            m_slot_back_glow.SetActive(true);
            m_slot_back_glitter.SetActive(true);

        }





        public void InitMachine()
        {
            int showVideo = 0;

            // Check if the IMS banner exists - if so need to init the slot in special way
            if (IMSController.Instance.BannersByLocations.ContainsKey(SLOT_VIDEO_BANNER) && Application.platform!=RuntimePlatform.WebGLPlayer)
            {
                Bonus bonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.RewardedSlotVideoBonus);

                if (bonus != null)
                {
                    showVideo = bonus.CoinsSingle;

                    // Init VideoAdController
                    VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(ModelManager.Instance.GetUser());
                }
            }


            m_showVideo = showVideo;

            m_bonus = (DailyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Daily);


            HandleBonusCounter();
            SetPayTableAndDay();
            SetFakeItems();
            m_fb_share_view.InitFBShare(SHARE_ID);
            m_fb_share_view.Hide();

            BlinkPayLines();
        }


        private void SetPayTableAndDay()
        {


            for (int i = 0; i < m_bonus.Pay_table.Count; i++)
            {
                m_pay_lines[i].text = FormatUtils.FormatBalance(m_bonus.Pay_table[i]);
                m_pay_table.Add(m_bonus.Pay_table[i]);
            }

            for (int i = 0; i < m_bonus.ConsecutiveDaysFactor.Count; i++)
            {

                m_daily_multiplyer_text[i].text = FormatUtils.FormatBalance(m_bonus.ConsecutiveDaysFactor[i]);

                m_friends_value_text[i].text = FormatUtils.FormatBalance(m_bonus.FriendsFactor[i]);

            }

            m_curr_day_to_show = m_bonus.NextStep;



            m_day_factor = m_bonus.ConsecutiveDaysFactor[m_curr_day_to_show - 1];

        }



        public void HandleClicked()
        {
            if (m_spin_enabled)
            {
                Spin();
            }

        }

        private void Spin()
        {
            m_slot_electricity_effect_object.SetActive(true);
            m_hidden_button.interactable = false;
            m_visual_button.interactable = false;

            ManagerView.Instance.EnableDisableButton(m_hidden_button);
            ManagerView.Instance.EnableDisableButton(m_visual_button);

            m_visual_button.GetComponent<ButtonsOverView>().Disabled = true;

            m_close_button.SetActive(false);

            CardGamesSoundsController.Instance.PullSlotHandle();
            m_spin_enabled = false;
            m_answer_from_server = false;

            m_slot_animator.SetTrigger("lights_out");
            StartCoroutine(StartSpinning(0.15f));

            // Get the facebook friends if connected to facebook
            FacebookController.Instance.GetUserAppFriends(FacebookFriendsReceived);
        }

        private void FacebookFriendsReceived(List<FacebookData> friends)
        {
            int numOfFriends = friends.Count;

            if (numOfFriends >= 250)
                m_curr_friends_to_show = 4;
            if (numOfFriends >= 99)
                m_curr_friends_to_show = 3;
            else if (numOfFriends >= 25)
                m_curr_friends_to_show = 2;
            else if (numOfFriends >= 10)
                m_curr_friends_to_show = 1;
            else if (numOfFriends >= 1)
                m_curr_friends_to_show = 0;
            else if (numOfFriends == 0)
                m_curr_friends_to_show = -1;



            m_days_bg[m_curr_day_to_show - 1].GetComponent<Image>().sprite = m_curr_day_on;

            if (m_curr_friends_to_show > -1)
                m_friends_bg[m_curr_friends_to_show].GetComponent<Image>().sprite = m_curr_friends_on;

            if (m_curr_friends_to_show > -1)
            {

                m_friends_factor = m_bonus.FriendsFactor[m_curr_friends_to_show];
            }

            BonusController.Instance.ClaimBonus(Bonus.BonusTypes.Daily, 0, friends.Count, null, 0, BonusClaimed);
        }

        private void BonusClaimed(bool success, BonusAwarded bonusAwarded)
        {
            if (success)
            {
                IMSController.Instance.TriggerEvent(IMSController.EVENT_BONUS_AWARD,true);

                m_winner = bonusAwarded.GoodsIndex;

                m_answer_from_server = true;

                m_total_awarded_coins = bonusAwarded.GoodsList.CoinsValue;

                NotificationController.Instance.ScheduleDailyNotification();

            }
            else
            {
                PopupManager.Instance.ShowMessagePopup("This bonus can no longer be collected.");
            }
        }

        private void HandleSharing(Action SharingDone)
        {
            //added also check for answer_from_server - so only if we actually did a spin we should publish to FB
            if (SpadesStateController.Instance.IsFBShare(SHARE_ID) && m_fb_share_view.gameObject.activeSelf && m_answer_from_server)
            {
                FacebookShareManager.Instance.Share(SHARE_ID, (bool succ) =>
                {

                    SharingDone();

                }, null, null);
            }
            else
            {
                SharingDone();
            }

        }

        private void BlinkPayLines()
        {
            int num_objects = m_pay_lines_bg.Count / 2;

            float fade_time = 1f;
            float items_delay = 0.4f;
            float additional_delay = 1.5f;
            float on_time = 0.1f;
            float off_time = .5f;

            for (int i = 0; i < num_objects; i++)
            {
                m_pay_lines_blink_coroutine[i] = PayLineFadeInOut(1000, m_pay_lines_bg[i], m_pay_lines_bg_lit[i], fade_time, additional_delay + i * items_delay, on_time, off_time);

                StartCoroutine(m_pay_lines_blink_coroutine[i]);

                m_pay_lines_blink_coroutine[i + num_objects] = PayLineFadeInOut(1000, m_pay_lines_bg[i + num_objects], m_pay_lines_bg_lit[i + num_objects], fade_time, additional_delay + i * items_delay, on_time, off_time);

                StartCoroutine(m_pay_lines_blink_coroutine[i + num_objects]);

            }

        }

        public void StopPayLineBlink()
        {
            for (int i = 0; i < m_pay_lines_bg.Count; i++)
            {
                StopCoroutine(m_pay_lines_blink_coroutine[i]);
                StartCoroutine(PayLineFadeToOn(false, m_pay_lines_bg[i], m_pay_lines_bg_lit[i], .75f, 0, null));

            }
        }

        IEnumerator BlinkImage(int num_blinks, float fade_time, Image image)
        {
            float f_start_time = CurTime();

            int counter = 0;

            while (counter < num_blinks)
            {
                CardGamesSoundsController.Instance.PlaySlotWinIndicator();

                f_start_time = CurTime();

                while ((CurTime() < f_start_time + fade_time))
                {
                    float time_left = fade_time - (CurTime() - f_start_time);

                    float timer_value = 1f - time_left / fade_time;


                    image.color = new Color(1, 1, 1, timer_value);

                    yield return new WaitForSecondsRealtime(0.05f);
                }

                f_start_time = CurTime();

                while ((CurTime() < f_start_time + fade_time))
                {
                    float time_left = fade_time - (CurTime() - f_start_time);

                    float timer_value = time_left / fade_time;


                    image.color = new Color(1, 1, 1, timer_value);

                    yield return new WaitForSecondsRealtime(0.05f);
                }

                counter++;

            }
        }

        IEnumerator PayLineFadeInOut(int num_blinks, Image image1, Image image2, float fade_time, float initial_delay, float blink_on_time, float blink_off_time, bool sfx = false)
        {
            yield return new WaitForSecondsRealtime(initial_delay);

            float f_start_time = CurTime();

            int counter = 0;

            while (counter < num_blinks)
            {
                if (sfx)
                    CardGamesSoundsController.Instance.PlaySlotWinIndicator();

                f_start_time = CurTime();

                while ((CurTime() < f_start_time + fade_time))
                {
                    float time_left = fade_time - (CurTime() - f_start_time);

                    float timer_value = time_left / fade_time;

                    //image1.color = new Color (1, 1, 1, timer_value);
                    image2.color = new Color(1, 1, 1, 1 - timer_value);


                    yield return new WaitForSeconds(0.05f);
                }

                yield return new WaitForSeconds(blink_on_time);

                f_start_time = CurTime();

                while ((CurTime() < f_start_time + fade_time))
                {
                    float time_left = fade_time - (CurTime() - f_start_time);

                    float timer_value = time_left / fade_time;

                    image2.color = new Color(1, 1, 1, timer_value);


                    yield return new WaitForSeconds(0.05f);
                }

                yield return new WaitForSeconds(blink_off_time);

                counter++;

            }

        }

        IEnumerator PayLineFadeToOn(bool fade_in, Image image1, Image image2, float fade_time, float initial_delay, Action fade_done = null)
        {
            yield return new WaitForSecondsRealtime(initial_delay);

            float f_start_time = CurTime();

            while ((CurTime() < f_start_time + fade_time))
            {
                float time_left = fade_time - (CurTime() - f_start_time);

                float timer_value = time_left / fade_time;

                if (fade_in)
                {
                    image1.color = new Color(1, 1, 1, timer_value);
                    image2.color = new Color(1, 1, 1, image2.color.a - timer_value);
                }
                else
                {
                    image1.color = new Color(1, 1, 1, image1.color.a + (1 - timer_value));
                    image2.color = new Color(1, 1, 1, image2.color.a - (1 - timer_value));
                }

                yield return new WaitForSeconds(0.05f);
            }


            if (fade_done != null)
                fade_done();


        }

        public void BT_CloseButtonClicked()
        {
            CloseSlotScreen();
        }

        private void CloseSlotScreen(Action close_share_done = null)
        {
            // The GameObject might be destroyed when connecting to facebook and this callback is invoked
            if (this == null)
                return;


            CardGamesSoundsController.Instance.SpinLoop(true);
            m_slot_back_glitter.SetActive(false);
            m_slot_back_glow.SetActive(false);
            CardGamesSoundsController.Instance.StartSlotIntro(false);

            HandleSharing(() =>
            {

                m_manager.HidePopup();
                ButtonsOverView.ForceNormalCursor();

                if (close_share_done != null)
                    close_share_done();
            });
        }




        private void HandleBonusCounter()
        {
            DailyBonus bonus = (DailyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Daily);
            if (bonus.Interval <= 0)
            {
                m_spin_enabled = true;

                m_bonus_timer_text.text = "Free Spin";
            }
            else
            {
                //m_bonus_timer_text.gameObject.SetActive (true);
                m_spin_enabled = false;
                m_counter_coroutine = StartBonusTimer(bonus.NextBonusTime);
                StartCoroutine(m_counter_coroutine);
            }
        }


        IEnumerator StartBonusTimer(DateTime nextBonusTime)
        {
            TimeSpan span = nextBonusTime - DateTime.Now;

            float startTime = Time.realtimeSinceStartup;

            while (span.TotalSeconds > 0)
            {
                m_bonus_timer_text.text = string.Format("Next Free {0:0}:{1:00}:{2:00}", span.Hours, span.Minutes, span.Seconds);

                yield return new WaitForSeconds(1);

                span = span.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;

            }

            m_spin_enabled = true;
            //m_bonus_timer_text.gameObject.SetActive (false);
            m_bonus_timer_text.text = "Free Spin";

        }

        IEnumerator StartSpinning(float delay)
        {
            StopPayLineBlink();

            yield return new WaitForSeconds(delay);

            CardGamesSoundsController.Instance.SpinLoop();

            for (int i = 0; i < m_wheels_animator.Count; i++)
            {
                m_item_wheels[i].SetActive(false);
                m_wheels_objects[i].SetActive(true);
                m_wheels_animator[i].SetBool("play", true);
                yield return new WaitForSeconds(m_wheels_start_delay);
            }

            float start_time = Time.realtimeSinceStartup;
            float min_spin_time = 2f;

            while (!m_answer_from_server || Time.realtimeSinceStartup < start_time + min_spin_time)
            {
                yield return new WaitForSeconds(0.1f);
            }

            WinnerCallback(m_winner);

        }

        private void SlowStopWheels(int i)
        {
            CardGamesSoundsController.Instance.SpinStop(i);
            float delay = 0.15f;
            iTween.MoveTo(m_item_wheels[i].transform.GetChild(0).gameObject, iTween.Hash("y", -60, "islocal", true, "easeType", "easeOutcirc", "time", delay));
            iTween.MoveTo(m_item_wheels[i].transform.GetChild(0).gameObject, iTween.Hash("y", 0, "islocal", true, "easeType", "easeOutBack", "time", delay * 1.5f, "delay", delay));
        }

        IEnumerator StopSpinning()
        {


            for (int i = 0; i < m_wheels_animator.Count; i++)
            {
                m_wheels_animator[i].SetBool("play", false);
                m_wheels_objects[i].SetActive(false);
                m_item_wheels[i].SetActive(true);
                SlowStopWheels(i);
                yield return new WaitForSeconds(m_wheels_stop_delay);
            }

            yield return new WaitForSeconds(0.25f);


            StartCoroutine(GenerateWinnerReport());
            yield return new WaitForSeconds(0.5f);

            m_slot_electricity_effect_object.SetActive(false);

            yield return new WaitForSeconds(1.5f);


        }

        private void WinnerCallback(int winner)
        {
            m_winner = winner;

            PrepareWinner();
            CardGamesSoundsController.Instance.SpinLoop(true);
            StartCoroutine(StopSpinning());

        }

        IEnumerator GenerateWinnerReport()
        {

            //add a delay to show the winning pay line
            //m_pay_lines_bg [m_winner].sprite = m_pay_table_on;
            //m_pay_lines [m_winner].color = Color.yellow;

            float total_blink_time = 3.0f;

            StartCoroutine(BlinkImage(5, 0.3f, m_pay_lines_bg_lit[m_winner]));

            yield return new WaitForSecondsRealtime(total_blink_time);

            //post blink

            CardGamesSoundsController.Instance.PlaySlotSidePanels();

            m_slot_animator.SetTrigger("friends_in");

            m_paytable_animator.SetTrigger("paytable_out");

            m_summary_animator.SetTrigger("summary_in");

            yield return new WaitForSecondsRealtime(1f);

            // Slots win
            CardGamesSoundsController.Instance.SlotTotalCount();
            CardGamesSoundsController.Instance.SlotXPUpdate();
            int win_value = Convert.ToInt32(m_pay_table[m_winner]);
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", win_value, "time", 1, "onupdate", "UpdateWinValue"));
            //post slot counter delay
            yield return new WaitForSecondsRealtime(1f);

            CardGamesSoundsController.Instance.SlotXPUpdate();
            StartCoroutine(BlinkImage(m_days_bg[m_curr_day_to_show - 1].GetComponent<Image>(), .1f, 1f, m_curr_day_on, m_curr_day_off));
            // Days factor
            int day_value = (int)(m_day_factor);
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", day_value, "time", 1, "onupdate", "UpdateDayValue"));
            yield return new WaitForSecondsRealtime(0.75f);


            //friends 
            if (m_curr_friends_to_show > -1)//this is for no friends or not connected to FB at all
            {
                CardGamesSoundsController.Instance.SlotXPUpdate();
                StartCoroutine(BlinkImage(m_friends_bg[m_curr_friends_to_show].GetComponent<Image>(), .1f, 1f, m_curr_friends_on, m_curr_friends_off));
                iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", m_friends_factor, "time", 1, "onupdate", "UpdateFriendValue"));
                yield return new WaitForSecondsRealtime(0.75f);
            }


            m_days_bg[m_curr_day_to_show - 1].GetComponent<Image>().sprite = m_curr_day_on;

            // Total
            CardGamesSoundsController.Instance.SlotXPUpdate();
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", m_total_awarded_coins, "time", 1, "onupdate", "UpdateTotalValue"));
            //m_total_text.text = "<sprite=1>" +FormatUtils.FormatPrice(m_total_awarded_coins);

            yield return new WaitForSecondsRealtime(1f);

            if (m_curr_friends_to_show > -1)//this is for no friends or not connected to FB at all
            {
                m_friends_bg[m_curr_friends_to_show].GetComponent<Image>().sprite = m_curr_friends_on;
            }
            //placing the coins emitter in the right position based on the win value
            float pos_x2 = m_total_text.gameObject.GetComponent<RectTransform>().localPosition.x - m_total_text.preferredWidth / 2 + m_xp_icon_width;
            m_coins_emmiter.GetComponent<RectTransform>().localPosition = new Vector3(pos_x2, m_coins_emmiter.GetComponent<RectTransform>().localPosition.y, 0);

            m_fb_share_view.Show();

            HandleBonusCounter();

            CollectReward();

            CardGamesSoundsController.Instance.PlaySlotTrail();

        }

        private void CollectRewardDone()
        {
            m_close_button.SetActive(true);

            if (FacebookController.Instance.IsLoggedIn())
            {
                m_invite_button.SetActive(true);
            }
            else
            {
                m_facebook_connect_btn.gameObject.SetActive(true);
                SetFacebookConnectTextMessage();
            }

#if !UNITY_WEBGL

            if (m_showVideo > 0)
            { 
                m_showVideoAmountText.text = "<sprite=1>" + FormatUtils.FormatBuyIn(m_showVideo);
                m_invite_button.SetActive(false);
                m_facebook_connect_btn.gameObject.SetActive(false);
                m_facebookConnectDescriptionText.gameObject.SetActive(false);
                m_showVideo_button.SetActive(true);
            }
#endif
        }

        private void SetFacebookConnectTextMessage()
        {
            FacebookBonus bonus = (FacebookBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.FacebookConnect);

            m_facebookConnectDescriptionText.gameObject.SetActive(false);
            m_facebook_connect_btn.SetMode(FacebookButton.ButtonMode.Connect);

            if (bonus != null)
            {
                if (!bonus.IsEntitled)
                {
                    m_facebookConnectDescriptionText.gameObject.SetActive(true);
                    m_facebookConnectDescriptionText.text = string.Format(FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG, FormatUtils.FormatBalance(bonus.CoinsSingle));
                    m_facebook_connect_btn.SetMode(FacebookButton.ButtonMode.Connect, true);
                }
            }
        }

        public void BT_ConnectClicked()
        {
            m_facebook_connect_btn.SetMode(FacebookButton.ButtonMode.Waiting);
            LobbyController.Instance.FBLogin((bool success) =>
            {
                CloseSlotScreen(null);
            });
        }

        public void BT_ShowVideoClicked()
        {
            Debug.Log("Show video clicked");

            VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedSlotVideoBonus);
            CloseSlotScreen();
        }

        public void BT_InviteClicked()
        {
            CloseSlotScreen(() =>
               {
                   SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
               });

        }

        private void UpdateDayValue(int value)
        {
            m_day_text.text = FormatUtils.FormatBalance(value);
        }

        private void UpdateWinValue(int value)
        {
            m_win_text.text = FormatUtils.FormatBalance(value);
        }

        private void UpdateFriendValue(int value)
        {
            m_friends_text.text = FormatUtils.FormatBalance(value);
        }

        private void UpdateTotalValue(int value)
        {
            m_total_text.text = "<sprite=1>" + FormatUtils.FormatBalance(value);
        }

        public void CollectReward()
        {
            PlayCoinsFlightForHourlyBonus();
        }

        public void PlayCoinsFlightForHourlyBonus()
        {
            CometOverlay comet = SpadesOverlayManager.Instance.ShowCometOverlay();

            int bonus_amount = m_total_awarded_coins;
            int num_coins_to_show = Mathf.Min(5, bonus_amount);

            comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_coins_emmiter, CardGamesPopupManager.Instance.Lobby_flight_target
                , 0f, 0.15f, 1.5f, CardGamesPopupManager.Instance.Lobby_coins_balance, bonus_amount, CardGamesPopupManager.Instance.Lobby_coins_animator, 3, 1.0f, 0.5f
                , TrailCompleted, new Vector2(0f, 0f), new Vector2(0.8f, 0.4f));
            //StartCoroutine (HideCometOverlay (comet));
        }

        private void TrailCompleted()
        {
            CollectRewardDone();

        }


        private IEnumerator BlinkImage(Image blink_image, float blink_delay, float total_blink_time, Sprite on_image, Sprite off_image)
        {
            int num_blinks = (int)(total_blink_time / blink_delay);

            for (int i = 0; i < num_blinks; i++)
            {
                if (i % 2 == 0)
                    blink_image.sprite = on_image;
                else
                    blink_image.sprite = off_image;

                yield return new WaitForSecondsRealtime(blink_delay);
            }
        }


        private void AmountUpdateFinishAnim(GameObject go)
        {
            iTween.ScaleTo(go, iTween.Hash("x", 1.2f, "y", 1.3, "time", 0.2f));
            iTween.ScaleTo(go, iTween.Hash("x", 1f, "y", 1, "time", 0.2f, "delay", 0.2f));
        }

        private void SetFakeItems()
        {
            for (int i = 0; i < 3; i++)
            {
                Sprite top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                Sprite mid = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                Sprite bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[i].GetComponent<WheelView>().SetWheel(top, mid, bottom);
            }
        }

        /// <summary>
        /// Returns the current time according to the platform.
        /// </summary>     
        private float CurTime()
        {
#if UNITY_WEBGL
			return Time.realtimeSinceStartup;
#else
            return Time.time;
#endif
        }

        private void PrepareWinner()
        {
            if (m_winner == 0)//single cherry
            {
                int wheel_with_cherry = UnityEngine.Random.Range(0, 3);

                //winner wheel 
                Sprite top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                Sprite mid = m_icons_sprite[0];
                Sprite bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[wheel_with_cherry].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                //no cherry wheel 
                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[UnityEngine.Random.Range(3, NUM_ICONS)];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(wheel_with_cherry + 1) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                //no cherry wheel 
                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[UnityEngine.Random.Range(3, NUM_ICONS)];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(wheel_with_cherry - 1 + 3) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);

            }
            else if (m_winner == 1) //2 cherry 
            {
                int wheel_with_cherry = UnityEngine.Random.Range(0, 3);

                //no cherry wheel 
                Sprite top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                Sprite mid = m_icons_sprite[UnityEngine.Random.Range(3, NUM_ICONS)];
                Sprite bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[wheel_with_cherry].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                //cherry wheel 
                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[0];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(wheel_with_cherry + 1) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                //cherry wheel 
                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[0];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(wheel_with_cherry - 1 + 3) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);
            }
            else if (m_winner == 8) // J + Q + K
            {
                int random_wheel = UnityEngine.Random.Range(0, 3);

                //J 
                Sprite top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                Sprite mid = m_icons_sprite[4];
                Sprite bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[random_wheel].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                //Q
                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[5];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(random_wheel + 1) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                //K 
                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[6];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(random_wheel - 1 + 3) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);
            }
            else if (m_winner == 9) // SSS
            {
                int random_wheel = UnityEngine.Random.Range(0, 3);

                Sprite top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                Sprite mid = m_icons_sprite[0];
                Sprite bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[random_wheel].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[0];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(random_wheel + 1) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);

                top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                mid = m_icons_sprite[0];
                bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                m_item_wheels[(random_wheel - 1 + 3) % 3].GetComponent<WheelView>().SetWheel(top, mid, bottom);
            }
            else//full 3 lines - (2-7)
            {
                for (int i = 0; i < 3; i++)
                {
                    Sprite top = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                    Sprite mid = m_icons_sprite[m_winner - 1];
                    Sprite bottom = m_icons_sprite[UnityEngine.Random.Range(2, NUM_ICONS)];
                    m_item_wheels[i].GetComponent<WheelView>().SetWheel(top, mid, bottom);
                }

            }
        }

        public override void OnBackButtonClicked()
        {
            if (m_close_button.activeSelf)
                CloseSlotScreen();
        }

        public override void Close()
        {
            base.Close();
            CardGamesSoundsController.Instance.StartSlotIntro(false);
        }
    }


}
