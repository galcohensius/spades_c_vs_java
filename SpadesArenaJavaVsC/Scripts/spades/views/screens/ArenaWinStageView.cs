using common.utils;
using System;

namespace spades.views.screens
{
    public class ArenaWinStageView : MonoBehaviour
    {
        [SerializeField] TMP_Text m_price = null;

        public void SetPrice(int price)
        {
            m_price.text = String.Format("<sprite=1>{0}", FormatUtils.FormatBuyIn(price));
        }

        public void PlayWinAnimation()
        {
            

        }
    }
}
