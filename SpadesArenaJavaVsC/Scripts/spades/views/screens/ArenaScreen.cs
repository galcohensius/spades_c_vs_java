using System.Collections;
using System.Collections.Generic;
using common.views;
using spades.controllers;
using spades.models;
using System;
using common.utils;
using spades.views.overlays;
using common;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;
using cardGames.views.popups;

namespace spades.views.screens
{
    public class ArenaScreen : ScreenBase
    {
        
        [SerializeField] GameObject m_leftSideJumpingAvatar = null;
        [SerializeField] ArenaStageView m_challenge_stage_prefab = null;
        [SerializeField] ArenaWinStageView m_win_stage_prefab = null;
        [SerializeField] ArenaGemView m_challenge_gem_view = null;
        [SerializeField] GameObject m_stage_container = null;
        [SerializeField] GameObject m_controls_container = null;
        [SerializeField] Button m_play_button = null;
        [SerializeField] TMP_Text m_play_button_text = null;
        [SerializeField] TMP_Text m_inner_title_text = null;
        [SerializeField] TMP_Text m_challenge_type_text = null;
        [SerializeField] TMP_Text m_challenge_title_text = null;
        [SerializeField] TMP_Text m_points_text = null;
        [SerializeField] TMP_Text m_bags_count_text = null;
        [SerializeField] TMP_Text m_bags_penalty_text = null;

        [SerializeField] GameObject m_first_stage_info = null;
        [SerializeField] TMP_Text m_bags_text = null;
        [SerializeField] TMP_Text m_bags_value_text = null;
        [SerializeField] TMP_Text m_title_points_text = null;

        [SerializeField] Material m_color_mat = null;
        [SerializeField] Material m_center_panel_bg_mat = null;
        [SerializeField] Material m_unpressed_mat = null;
        [SerializeField] Material m_pressed_mat = null;
        [SerializeField] Material m_start_point_mat = null;
        [SerializeField] Material m_vertical_div_mat = null;
        [SerializeField] Material m_hor_div_mat = null;
        [SerializeField] Material m_progress_mat = null;
        [SerializeField] Material m_stand_mat = null;
        [SerializeField] ControlsContainerView m_controls = null;
        [SerializeField] GameObject m_bottom_panel;
        [SerializeField] RectTransform m_central_panel = null;
        [SerializeField] List<Color> m_world_primary_colors = new List<Color>();
        [SerializeField] List<Color> m_world_secondary_colors = new List<Color>();
        [SerializeField] GameObject m_spadesHolder;

        [SerializeField] Image m_bg_material_shader;

        [SerializeField] Sprite m_standardBG;
        [SerializeField] Sprite m_specialRingMasterBG;
        [SerializeField] Sprite m_specialRingJokersBG;

        List<Color> m_hsb_plugin = new List<Color>();

        private SpadesArena m_arena = null;
        private int m_num_stages = 0;
        private Color m_world_primary_color = new Color();
        private Color m_world_secondary_color = new Color();
        private List<Color> m_colors = new List<Color>();
        private bool isRebuy = false;
        private ArenaStageView m_currentStageView;
        private ArenaStageView m_firstStageView;
        private Action m_buyAnimCompleted;
        private bool isClose;
        private BackFromReason m_current_reason;
        private Vector3 m_avatarTweenToPos;

        private const int STAGES_CONTAINER_HEIGHT = 650;
        private const int STAGES_CONTAINER_POS = -200;
        private const int STAGES_CONTAINER_BUY_POS = -220;
        [SerializeField] Image bgGlow = null;


        public enum BackFromReason
        {
            win,
            lose,
            stay,
            big_win,
            lobby,
            cancel,
            noRebuy,
            winPlayNext,
            big_win_with_gem,
            second_place
        }

        public void Awake()
        {
            FillPluginHSBValues();

            // Start with hidden, will be shown later according to state
            m_leftSideJumpingAvatar.SetActive(false);
        }

        public override void Show(ScreensManager manager, ScreenClosedDelegate close_delegate)
        {

            SetAvatarPrefabOverrideParams(m_leftSideJumpingAvatar, new Vector3(0.6f, 0.6f, 0.6f));

            base.Show(manager, close_delegate);
            LobbyController.Instance.ShowLobby(false);
            LobbyTopController.Instance.ShowTop(false);

            m_leftSideJumpingAvatar.GetComponent<MAvatarView>().AddArenaAvatarStartJumpListener(StartTweenMoveJumpNext);
            m_leftSideJumpingAvatar.GetComponent<MAvatarView>().AddArenaAvatarDroppedListener(OnPlayFall);

            // Force the cursor to normal, but wait a single frame so the mouse up handling will not update back
            UnityMainThreadDispatcher.Instance.DelayedCall(0.1f, () =>
            {
                ButtonsOverView.ForceNormalCursor();
            });
        }

        private void FillPluginHSBValues()
        {
            m_hsb_plugin.Clear();

            m_hsb_plugin.Add(new Color(48f, .396f, 1f));//emerald
            m_hsb_plugin.Add(new Color(196f, .707f, 1f));//ruby
            m_hsb_plugin.Add(new Color(0f, 0.702f, 1f));//saphirre
            m_hsb_plugin.Add(new Color(304f, 0.293f, 1f));//onyx
            m_hsb_plugin.Add(new Color(237f, 0.493f, 1f));//amteyst


            //virtual arena #6 
            m_hsb_plugin.Add(new Color(237f, 0.493f, 1f));//amteyst

        }

        private void SetShaderParams(int index)
        {
            Color curr_color = m_hsb_plugin[index];

            m_bg_material_shader.material.SetFloat("_ColorHSV_Hue_1", curr_color.r);
            m_bg_material_shader.material.SetFloat("_ColorHSV_Saturation_1", curr_color.g);
            m_bg_material_shader.material.SetFloat("_ColorHSV_Brightness_1", curr_color.b);
        }

        private void SetAvatarPrefabOverrideParams(GameObject targetAvatarObject, Vector3 localScale)
        {
            targetAvatarObject.transform.localScale = localScale;
        }

        private void CenterPanelContent(bool show)
        {
            m_stage_container.SetActive(show);
            m_controls_container.SetActive(false);
            m_inner_title_text.gameObject.SetActive(show);
            m_play_button.gameObject.SetActive(show);
        }

        public void SetArena(SpadesArena arena, BackFromReason back_reason)
        {
            m_current_reason = back_reason;
            RemoveChallengeStages();

            m_arena = arena;
            SpadesActiveArena active_arena = SpadesModelManager.Instance.GetActiveArena(m_arena.GetID());

            m_first_stage_info.SetActive(!(active_arena != null));
            if (active_arena == null)
            {

                SpadesArenaMatch spadesArenaMatch = m_arena.Arena_matches[0] as SpadesArenaMatch;

                m_bags_text.text = spadesArenaMatch.BagsCount.ToString();
                m_bags_value_text.text = spadesArenaMatch.BagsPanelty.ToString();
                m_points_text.text = spadesArenaMatch.LowPoints + "/" + spadesArenaMatch.HighPoints;
                m_title_points_text.text = m_points_text.text;

            }

            m_world_primary_color = m_world_primary_colors[Convert.ToInt32(m_arena.World.Index)];
            m_world_secondary_color = m_world_secondary_colors[Convert.ToInt32(m_arena.World.Index)];

            m_colors.Clear();
            m_colors.Add(m_world_secondary_color);
            m_colors.Add(m_world_primary_color);

            m_num_stages = m_arena.GetNumStages();
            m_inner_title_text.text = String.Format("WIN {0} GAMES &", m_num_stages);


            SetShaderParams(m_arena.World.Index);

            //m_challenge_type_text.text = "<sprite={0}> {1}";
            if (m_arena.Playing_mode == PlayingMode.Partners)
            {
                m_challenge_title_text.text = "<sprite=5> PARTNERS";
            }
            else
            {
                m_challenge_title_text.text = "<sprite=6> SOLO";
            }

            if (m_arena.ArenaType == SpadesArena.SpadesArenaType.Master)
            {
                //special case for fifth arena
                m_challenge_title_text.text += "\n<size=90>MASTER ARENA";
                m_first_stage_info.transform.GetChild(1).GetComponent<TMP_Text>().text = "MASTER ARENA";
                
                m_central_panel.gameObject.GetComponent<Image>().sprite = m_specialRingMasterBG;
                m_central_panel.gameObject.GetComponent<Image>().material = null;
                m_central_panel.gameObject.GetComponent<Image>().color = Color.white;


            }
            else if (m_arena.ArenaType == SpadesArena.SpadesArenaType.Jokers)
            {
                //special case for 6 arena
                m_challenge_title_text.text += "\n<size=90>JOKERS ARENA";
                m_first_stage_info.transform.GetChild(1).GetComponent<TMP_Text>().text = "JOKERS ARENA";

                m_central_panel.gameObject.GetComponent<Image>().sprite = m_specialRingJokersBG;
                m_central_panel.gameObject.GetComponent<Image>().material = null;
                m_central_panel.gameObject.GetComponent<Image>().color = Color.white;


            }
            else
            {
                m_challenge_title_text.text += "\n<size=90>" + m_arena.GetName() + " arena";
                m_central_panel.gameObject.GetComponent<Image>().sprite = m_standardBG;
                m_central_panel.gameObject.GetComponent<Image>().material = m_color_mat;
            }

            


            if (back_reason != BackFromReason.cancel)
            {
                BuildStages(active_arena, false, back_reason);
            }

            m_challenge_gem_view.Init(m_arena, back_reason);

            m_center_panel_bg_mat.color = m_world_primary_color;

            m_unpressed_mat.color = m_world_secondary_color;
            m_pressed_mat.color = m_world_secondary_color;

            m_start_point_mat.color = m_world_secondary_color;

            m_vertical_div_mat.SetColor("_EmissionColor", m_world_primary_color);
            m_hor_div_mat.SetColor("_EmissionColor", m_world_primary_color);

            m_stand_mat.color = m_world_primary_color;

            bgGlow.color = m_world_primary_color;

            if (m_currentStageView == null)
                HideAvatar();



            //set center object size to 75% of screen width
            Vector2 crp_size = m_central_panel.sizeDelta;
            m_central_panel.sizeDelta = new Vector2(ManagerView.Instance.Actual_width * .75f, crp_size.y);
        }

        public void OnCancelClick()
        {
            SpadesActiveArena active_arena = SpadesModelManager.Instance.GetActiveArena(m_arena.GetID());

            if (m_currentStageView != null)
            {
                StartCoroutine(ShowAvatar(BackFromReason.cancel));
            }
            else
            {
                HideAvatar();
            }
        }

        private void HideAvatar()
        {
            m_leftSideJumpingAvatar.SetActive(false);
        }

        private IEnumerator ShowAvatar(BackFromReason back_reason)
        {
            Vector3 toPos;
            yield return new WaitForEndOfFrame();
            m_current_reason = back_reason;

            float currentStageCenterPos = m_currentStageView.GetComponent<RectTransform>().anchoredPosition.x - m_central_panel.GetComponent<RectTransform>().offsetMax.x / 2;

            float currentStageYPos = 795 - m_currentStageView.transform.position.y * (-13.95f);

            if (back_reason == BackFromReason.lobby)
            {
                // reset avatar position

                m_controls_container.transform.localPosition = m_leftSideJumpingAvatar.transform.localPosition + new Vector3(0, 470, 0);


                m_leftSideJumpingAvatar.GetComponent<RectTransform>().anchoredPosition = new Vector3(currentStageCenterPos, -437, 0);

                m_leftSideJumpingAvatar.SetActive(true);
                SetAvatarModel();
                m_leftSideJumpingAvatar.GetComponent<Animator>().Play("Arena Drop");
            }

            if (back_reason == BackFromReason.noRebuy)
            {
                HideAvatar();
            }

            if (back_reason == BackFromReason.win)
            {
                toPos = m_currentStageView.transform.position + new Vector3(0, 2, 0);
                m_avatarTweenToPos = toPos;
                m_controls_container.GetComponent<RectTransform>().anchoredPosition = new Vector3(currentStageCenterPos, currentStageYPos, 0);
                m_controls_container.SetActive(false);

                SetAvatarModel();
                m_leftSideJumpingAvatar.GetComponent<Animator>().Play("Arena jump next");

            }
            if (back_reason == BackFromReason.winPlayNext)
            {
                m_play_button.interactable = true;
                ManagerView.Instance.EnableDisableButton(m_play_button);

                m_play_button.enabled = false;
                toPos = m_currentStageView.transform.position + new Vector3(0, 2, 0);
                m_avatarTweenToPos = toPos;
                m_controls_container.GetComponent<RectTransform>().anchoredPosition = new Vector3(currentStageCenterPos, 70, 0);
                m_controls_container.SetActive(false);

                SetAvatarModel();
                m_leftSideJumpingAvatar.GetComponent<Animator>().Play("Arena jump next");

            }
            if (back_reason == BackFromReason.cancel)
            {
                toPos = m_currentStageView.transform.position + new Vector3(0, 2, 0);
                m_controls_container.transform.localPosition = m_leftSideJumpingAvatar.transform.localPosition + new Vector3(0, 470, 0);
                m_controls_container.SetActive(true);
            }

            if (back_reason == BackFromReason.lose && m_currentStageView != null)
            {
                toPos = m_currentStageView.transform.position + new Vector3(0, 2, 0);

                m_controls_container.SetActive(true);
                m_controls_container.transform.localPosition = m_leftSideJumpingAvatar.transform.localPosition + new Vector3(0, 470, 0);
            }
            else if (back_reason == BackFromReason.lose && m_currentStageView == null)
            {
                m_controls_container.SetActive(false);
                ScaleDownStages(true);
            }

            if (back_reason == BackFromReason.stay)
            {
                yield return new WaitForFixedUpdate();

                toPos = m_currentStageView.transform.position + new Vector3(0, 2, 0);
            }

            if (back_reason == BackFromReason.second_place)
            {
                yield return new WaitForFixedUpdate();

                toPos = m_currentStageView.transform.position + new Vector3(0, 2, 0);
            }

            if (back_reason == BackFromReason.big_win)
            {
                m_controls_container.SetActive(false);
            }

            if (back_reason == BackFromReason.big_win_with_gem)
            {
                m_controls_container.SetActive(false);
            }
        }

        private void SetAvatarModel()
        {
            MAvatarModel model = ModelManager.Instance.GetUser().GetMAvatar();

            m_leftSideJumpingAvatar.GetComponent<MAvatarView>().SetAvatarModel(model);
        }

        public void StartTweenMoveJumpNext()
        {
            iTween.MoveTo(m_leftSideJumpingAvatar, iTween.Hash("x", m_avatarTweenToPos.x,
                                                               "time", 0.9f,
                                                               "oncomplete", "OnMoveAvatarComplete",
                                                               "oncompletetarget", this.gameObject,
                                                           "easeType", iTween.EaseType.easeOutQuad));

            CardGamesSoundsController.Instance.ArenaJump();
        }

        public void OnAnimateStages(float tweenVal)
        {
            RectTransform rt = m_stage_container.GetComponent<RectTransform>();
            rt.localScale = new Vector3(0.8f + 0.2f * tweenVal, 0.8f + 0.2f * tweenVal, 1f);

            rt.anchoredPosition = new Vector2(rt.anchoredPosition.x, STAGES_CONTAINER_BUY_POS + STAGES_CONTAINER_POS * tweenVal);
        }

        public void ScaleUpStages(bool animate)
        {
            RectTransform rt = m_stage_container.GetComponent<RectTransform>();

            if (animate)
            {
                iTween.ValueTo(this.gameObject, iTween.Hash("from", 0,
                                                            "to", 1,
                                                            "onupdatetarget", this.gameObject,
                                                            "oncompletetarget", this.gameObject,
                                                            "onupdate", "OnAnimateStages",
                                                            "oncomplete", "OnStagesAnimComplete",
                                                            "time", 0.6,
                                                            "easeType", iTween.EaseType.easeInCubic));

            }
            else
            {
                rt.localScale = new Vector3(1f, 1f, 1f);
                rt.offsetMin = new Vector2(0, STAGES_CONTAINER_BUY_POS + STAGES_CONTAINER_POS);
                rt.offsetMax = new Vector2(0, STAGES_CONTAINER_HEIGHT + STAGES_CONTAINER_POS);

                OnStagesAnimComplete();
            }
        }

        private void ScaleDownStages(bool animate)
        {
            HideAvatar();


            RectTransform rt = m_stage_container.GetComponent<RectTransform>();

            if (animate)
            {
                iTween.ValueTo(this.gameObject, iTween.Hash("from", 1,
                                                               "to", 0,
                                                               "onupdatetarget", this.gameObject,
                                                               "oncompletetarget", this.gameObject,
                                                               "onupdate", "OnAnimateStages",
                                                               "oncomplete", "ShowBottomPanel",
                                                               "time", 0.6,
                                                               "easeType", iTween.EaseType.easeInCubic));

            }
            else
            {
                rt.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                rt.offsetMin = new Vector2(0, STAGES_CONTAINER_BUY_POS);
                rt.offsetMax = new Vector2(0, STAGES_CONTAINER_HEIGHT);
                ShowBottomPanel();
            }
        }

        public void ShowBottomPanel()
        {
            m_bottom_panel.SetActive(true);

            m_play_button.interactable = true;
            ManagerView.Instance.EnableDisableButton(m_play_button);

            m_play_button_text.enabled = true;
            m_spadesHolder.SetActive(false);
        }


        public void HideBottomPanel()
        {
            m_bottom_panel.SetActive(false);
        }

        public void OnShowControls()
        {
            m_controls_container.transform.localPosition = m_leftSideJumpingAvatar.transform.localPosition + new Vector3(0, 470, 0);
            m_controls_container.SetActive(true);
        }

        private void OnMoveAvatarComplete()
        {
            m_play_button.interactable = m_play_button.enabled = true;
            ManagerView.Instance.EnableDisableButton(m_play_button);

            m_play_button_text.enabled = true;
            m_spadesHolder.SetActive(false);

            if (m_current_reason == BackFromReason.winPlayNext)
            {
                SpadesArenaController.Instance.PlayNextArena(OnCancelClicked);
            }
            else
            {
                OnShowControls();
            }
        }

        private void OnCancelClicked()
        {
            m_controls_container.SetActive(true);
            m_controls_container.transform.localPosition = m_leftSideJumpingAvatar.transform.localPosition + new Vector3(0, 470, 0);
        }

        private void OnPlayFall()
        {
            CardGamesSoundsController.Instance.ArenaFall();
            OnShowControls();
        }

        public void ShowRebuy(int? rebuy, int stage, RebuyRequested rebuy_requested_delegate, RebuyQuitRequested rebuy_quit_requested_delegate)
        {
            isRebuy = true;
            m_controls.setState(ControlsContainerView.ControlsContainerState.REBUY_MODE, rebuy, rebuy_requested_delegate, rebuy_quit_requested_delegate, null);
        }

        public void OnStagesAnimComplete()
        {
            StartCoroutine(ShowAvatar(m_current_reason));
        }

        public void OnBuyAnimComplete()
        {
            m_buyAnimCompleted();
        }

        public void UpdateStageContainerPosition(float val)
        {
            RectTransform rt = m_stage_container.GetComponent<RectTransform>();
            Vector3 pos = rt.position;
            pos.y = val;
        }

        private void BuildStages(SpadesActiveArena active_arena, bool arenaBought, BackFromReason reason)
        {
            m_first_stage_info.SetActive(!(active_arena != null));

            //create stages
            for (int stageIdx = 0; stageIdx < m_num_stages; stageIdx++)
            {
                ArenaStageView stageView = (ArenaStageView)Instantiate(m_challenge_stage_prefab);
                stageView.transform.SetParent(m_stage_container.transform);
                stageView.transform.localPosition = new Vector3();
                stageView.transform.localScale = new Vector3(1f, 1f, 1f);

                stageView.Init(active_arena, m_arena.Arena_matches[stageIdx].GetPayOutCoins(), stageIdx, m_num_stages, m_colors, m_arena.World.GetID());

                if (stageView.IsCurrent)
                {
                    m_currentStageView = stageView;
                }
            }
            //return;

            // create final win stage
            ArenaWinStageView winStageView = (ArenaWinStageView)Instantiate(m_win_stage_prefab);
            winStageView.SetPrice(m_arena.Arena_matches[m_num_stages - 1].GetPayOutCoins());
            winStageView.transform.SetParent(m_stage_container.transform);
            winStageView.transform.localPosition = new Vector3();
            winStageView.transform.localScale = new Vector3(1f, 1f, 1f);

            m_play_button_text.text = String.Format("START <sprite=1>{0}", FormatUtils.FormatBuyIn(m_arena.GetBuyInCoins()));
        
            if (active_arena != null)
            {
                //arena ready to play
                ScaleUpStages(arenaBought);

                HideBottomPanel();
                m_controls.gameObject.SetActive(false);

                switch (reason)
                {
                    case BackFromReason.stay:
                        m_controls.setState(ControlsContainerView.ControlsContainerState.PLAY_MODE, null, null, null, active_arena);

                        isClose = false;
                        m_controls_container.SetActive(true);

                        break;
                    case BackFromReason.second_place:
                        m_controls.setState(ControlsContainerView.ControlsContainerState.PLAY_MODE, null, null, null, active_arena);
                        m_controls_container.SetActive(true);

                        break;
                    case BackFromReason.lose:
                        //do nothing
                        break;
                    default:
                        m_controls.setState(ControlsContainerView.ControlsContainerState.PLAY_MODE, null, null, null, active_arena);
                        break;
                }
            }
            else
            {
                if (reason == BackFromReason.big_win_with_gem)
                {
                    m_leftSideJumpingAvatar.SetActive(false);

                    //Show win overlay here
                    CenterPanelContent(false);

                    NewArenaWinOverlay winOverlay = SpadesOverlayManager.Instance.ShowmArenaWinOverlay(() =>
                    {
                        SpadesOverlayManager.Instance.ShowGemOverlay(m_arena, () =>
                        {
                            CenterPanelContent(true);
                            m_challenge_gem_view.UpdateGemsWonCount(0, false);
                        });
                    });

                    winOverlay.SetData(m_arena.Arena_matches[m_num_stages - 1].GetPayOutCoins(), () =>
                    {
                        m_challenge_gem_view.UpdateGemsWonCount(m_arena.World.Arena_Gem_Total_Steps, true);
                    });

                    ScaleDownStages(false);


                    m_controls.gameObject.SetActive(false);
                }

                if (reason == BackFromReason.big_win)
                {
                    m_leftSideJumpingAvatar.SetActive(false);

                    //Show win overlay here

                    CenterPanelContent(false);


                    NewArenaWinOverlay winOverlay = SpadesOverlayManager.Instance.ShowmArenaWinOverlay(() =>
                    {
                        CenterPanelContent(true);
                    });

                    winOverlay.SetData(m_arena.Arena_matches[m_num_stages - 1].GetPayOutCoins(), () => { m_challenge_gem_view.UpdateGemsWonCount(); });
                    ScaleDownStages(false);

                    m_controls.gameObject.SetActive(false);
                }

                if (reason == BackFromReason.lobby)
                {
                    ScaleDownStages(false);


                    m_controls.gameObject.SetActive(false);
                }

                if (reason == BackFromReason.lose)
                {
                    ScaleDownStages(true);

                    m_controls.gameObject.SetActive(false);
                }

                if (reason == BackFromReason.noRebuy)
                {
                    ScaleDownStages(true);
                    m_controls.gameObject.SetActive(false);

                }
            }
        }

        public void UnlockBuyButton()
        {
            m_play_button.interactable = true;
            ManagerView.Instance.EnableDisableButton(m_play_button);

            m_play_button_text.enabled = true;
            // m_play_button.transform.GetChild(0).gameObject.SetActive(false);
            m_spadesHolder.SetActive(false);
        }

        public void PlayButtonClicked()
        {
            m_play_button.interactable = false;
            ManagerView.Instance.EnableDisableButton(m_play_button);

            m_play_button_text.enabled = false;
            m_spadesHolder.SetActive(true);

            SpadesArenaController.Instance.Enter_Arena();
        }

        public override void CloseButtonClicked()
        {
            if (this.gameObject != null)
                iTween.Stop(this.gameObject);

            m_leftSideJumpingAvatar.GetComponent<MAvatarView>().RemoveAllArenaAvatarStartJumpListeners();


            //check if any progress
            SpadesActiveArena active_arena = SpadesModelManager.Instance.GetActiveArena(m_arena.GetID());

            if (active_arena != null && isRebuy)
            {
                isClose = true;
                ArenaBuyWarningPopup arenaBuyWarning = SpadesPopupManager.Instance.ShowArenaBuyWarning();
                arenaBuyWarning.SetData(SpadesArenaController.Instance.GetCurrentRebuy(), () =>
                {
                    SpadesArenaController.Instance.QuitRebuyRequested(true);
                    isClose = false;
                    isRebuy = false;

                    RemoveChallengeStages();

                    m_manager.HideScreen();

                    LobbyController.Instance.ShowLobby(true, true);

                    if (RoundController.Instance.Table_view != null)
                        RoundController.Instance.Table_view.ShowGameUI(false);
                }, () => { SpadesArenaController.Instance.RebuyRequested(() => { }); });

            }
            else
            {
                m_leftSideJumpingAvatar.SetActive(false);

                isRebuy = false;
                RemoveChallengeStages();

                m_manager.HideScreen();

                LobbyController.Instance.ShowLobby(true, true);

                if (RoundController.Instance.Table_view != null)
                    RoundController.Instance.Table_view.ShowGameUI(false);

            }
        }

        public override void OnBackButtonClicked()
        {
            CloseButtonClicked();
        }

        public void ArenaBought(Action buyAnimCompleded)
        {

            m_buyAnimCompleted = buyAnimCompleded;
            m_current_reason = BackFromReason.lobby;
            RemoveChallengeStages();

            SpadesActiveArena active_arena = SpadesModelManager.Instance.GetActiveArena(m_arena.GetID());
            BuildStages(active_arena, true, BackFromReason.lobby);
        }

        public void Eliminated()
        {

        }

        public void RebuildArenaStages(bool bought, bool isClose = false)
        {


            if (bought)
            {
                isRebuy = false;
                SetArena(m_arena, BackFromReason.stay);
            }
            else
            {
                isRebuy = false;
                if (!isClose)
                    SetArena(m_arena, BackFromReason.noRebuy);
            }
        }

        private void RemoveChallengeStages()
        {
            foreach (Transform T in m_stage_container.transform)
            {
                Destroy(T.gameObject);
            }
        }
    }
}
