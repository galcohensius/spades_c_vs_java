﻿using System;

namespace spades.views.screens
{
    public class ProgressBar : MonoBehaviour
    {

        [SerializeField] Image m_fill = null;
        [SerializeField] TMP_Text m_progress_text = null;

        public void setBar(int current, int total, Color color)
        {
            m_progress_text.text = String.Format("{0}/{1}", current, total);
            m_fill.fillAmount = (float)current / (float)total;
            m_fill.color = color;
        }
    }
}