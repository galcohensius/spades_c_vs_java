using System.Collections.Generic;
using spades.models;
using System;
using common.utils;

namespace spades.views.screens
{
    public class ArenaStageView : MonoBehaviour
    {

        public enum ChallengeStageState
        {
            Empty,
            PressedGlow,
            Pressed,
            UnpressedGlow,
            Unpressed,
            LastStatePressedGlow,
            LastPressed,
        }

        [SerializeField] GameObject m_pressed_image = null;
        [SerializeField] GameObject m_unpressed_image = null;
        [SerializeField] GameObject m_start_point_image = null;
        [SerializeField] ArenaStageEffect m_regular_effect = null;
        [SerializeField] TMP_Text m_payout_text = null;
        [SerializeField] GameObject m_payout_container = null;
        [SerializeField] GameObject m_avatarStand = null;

        [SerializeField] Sprite[] m_regularStageImages = null;
        [SerializeField] Sprite[] m_pressedStageImages = null;

        private bool m_isCurrent = false;
        private const float APPEAR_DELAY = 0.5f;
        private int m_total_stages_count = 0;
        private int m_current_stage_pos = 0;



        public void Init(SpadesActiveArena activeArena, int payout, int stagePos, int totalStages, List<Color> color, String worldId)
        {
            m_total_stages_count = totalStages;
            m_current_stage_pos = stagePos;

            m_regular_effect.Init(color, payout > 0, stagePos == totalStages - 1);
            // m_regular_effect.transform.localScale=new Vector3(1.4f,1,0.5f);
            m_payout_text.enabled = false;
            m_payout_text.text = String.Format("<sprite=1>{0}", FormatUtils.FormatBuyIn(payout));

            //if(playEnterAnim)
            //    AppearAnimation();

            int worldIdx = Convert.ToInt32(worldId) - 1;

            m_unpressed_image.GetComponent<Image>().sprite = m_regularStageImages[worldIdx];
            m_pressed_image.GetComponent<Image>().sprite = m_pressedStageImages[worldIdx];

            if (activeArena == null)
            {
                m_pressed_image.SetActive(false);
                m_unpressed_image.SetActive(true);
                m_start_point_image.SetActive(false);
                m_regular_effect.Display(ArenaStageEffect.DISPLAY_SELECTOR.REGULAR);
                m_isCurrent = false;
                return;
            }
            else
            {
                SpadesArenaMatch currentArenaMatch = activeArena.GetCurrArenaMatch();
                if (activeArena.GetCurrStage() == stagePos + 1)
                {
                    m_pressed_image.SetActive(false);
                    m_unpressed_image.SetActive(true);
                    m_start_point_image.SetActive(false);
                    m_regular_effect.Display(ArenaStageEffect.DISPLAY_SELECTOR.NONE);
                    m_isCurrent = true;
                }
                else if (activeArena.GetCurrStage() >= stagePos + 1)
                {
                    m_pressed_image.SetActive(true);
                    m_unpressed_image.SetActive(false);
                    m_start_point_image.SetActive(false);
                    m_regular_effect.Display(ArenaStageEffect.DISPLAY_SELECTOR.NONE);
                    m_isCurrent = false;
                }
                else
                {
                    m_isCurrent = false;
                    m_regular_effect.Display(ArenaStageEffect.DISPLAY_SELECTOR.REGULAR);
                }
            }
        }

        // private void AppearAnimation()
        // {
        //     iTween.ScaleFrom(gameObject, iTween.Hash("scale",new Vector3(1,0,1),"delay",APPEAR_DELAY/m_total_stages_count*m_current_stage_pos,"time",0.2f));
        // }

        public bool IsCurrent
        {
            get { return m_isCurrent; }
        }

        public GameObject AvatarStand
        {
            get
            {
                return m_avatarStand;
            }


        }
    }


}