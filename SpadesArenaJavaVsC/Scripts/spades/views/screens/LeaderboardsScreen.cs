using cardGames.controllers;
using cardGames.views.popups;
using common;
using common.controllers;
using common.models;
using common.views;
using spades.controllers;

namespace spades.views.screens
{

    public class LeaderboardsScreen : ScreenBase
    {
        [SerializeField] GameObject m_topPanel = null;
        [SerializeField] GameObject m_rightPanel = null;
        [SerializeField] LeaderboardAlerts m_alerts = null;
        [SerializeField] LeaderboardsItemsPanelView m_ItemsPanel = null;
        [SerializeField] GameObject m_ItemsPanelHeader = null;

        private LeaderboardsTopPanel m_topPanelLogic;
        private LeaderboardsRightPanel m_rightPanelLogic;


        public override void Show(ScreensManager manager, ScreenClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);

            UpdatePanels();


            m_topPanelLogic = m_topPanel.GetComponent<LeaderboardsTopPanel>();


            m_rightPanelLogic = m_rightPanel.GetComponent<LeaderboardsRightPanel>();
            m_rightPanelLogic.Show(false);

            m_rightPanelLogic.SwitchTab(LeaderboardsModel.LeaderboardType.GlobalCurrent);

            //m_rightPanelLogic.SwitchTab(LeaderboardsModel.LeaderboardType.HallOfFame);

            LobbyTopController.Instance.ShowTop(false);

            m_topPanelLogic.switchToCurrent();
            m_topPanelLogic.StopTimer();
        }

        public override void Close()
        {
            base.Close();

            m_topPanelLogic.StopTimer();
            Alerts.hideAll();
            ClearItems();
            LobbyTopController.Instance.ShowTop(true, false, true);
        }

        public void ClearItems()
        {
            if (m_ItemsPanel != null)
            {
                m_ItemsPanel.ClearItems();
            }
        }

        public void ShowItems(LeaderboardsModel.LeaderboardType lt)
        {
            m_rightPanelLogic.Show(true);
            m_ItemsPanel.ShowItems(lt);

            if (lt == LeaderboardsModel.LeaderboardType.GlobalCurrent)
                m_topPanelLogic.StartTimer();
        }

        public void TurnOnWaiting()
        {
            //lock tabs
            m_topPanelLogic.LockAllTabs();

            m_ItemsPanel.Sapdes_holder.SetActive(true);
            m_ItemsPanel.ClearItems();
        }

        public void TurnOffWaiting()
        {
            m_topPanelLogic.UnLockAllTabs();
            m_ItemsPanel.Sapdes_holder.SetActive(false);
        }

        public void ShowItemsHeader(bool show)
        {
            m_ItemsPanelHeader.SetActive(show);
        }

        public void BT_ConnectFacebook()
        {
            LobbyController.Instance.FBLogin((bool success) =>
            {
                if (success)
                {
                    LeaderboardsController.Instance.HideScreen();

                }
            });
        }

        public override void CloseButtonClicked()
        {
            LeaderboardsController.Instance.HideScreen();
        }

        public override void OnBackButtonClicked()
        {
            LeaderboardsController.Instance.HideScreen();
        }


        public void OnTabClick(int id)
        {
            //SoundsController.Instance.TabsSFX();
            ClearItems();
            LeaderboardsController.Instance.ShowLeaderboard((LeaderboardsModel.LeaderboardType)id);
        }

        public void SwitchTab(LeaderboardsModel.LeaderboardType tabId)
        {
            if (tabId == LeaderboardsModel.LeaderboardType.GlobalCurrent)
            {
                m_rightPanel.SetActive(true);
            }
            CardGamesSoundsController.Instance.TabsSFX();
            m_topPanelLogic.SwitchTab(tabId);
            m_rightPanelLogic.SwitchTab(tabId);

            UpdatePanels(tabId);
        }

        public void SwitchToCurrentPrevious(bool isCurrent)
        {

            ClearItems();
            LeaderboardsController.Instance.switchCurrentPrevious(isCurrent);

            if (isCurrent)
                UpdatePanels(LeaderboardsModel.LeaderboardType.GlobalCurrent);
            else
                UpdatePanels(LeaderboardsModel.LeaderboardType.GlobalPrevious);

            ButtonsOverView.ForceNormalCursor();
        }




        public void UpdateRanksView(LeaderboardsModel.LeaderboardType lt)
        {
            m_rightPanelLogic.UpdateRanksView(lt);
        }

        public void OnInfoButtonClick()
        {
            LeaderboardInfoPopup popup = (PopupManager.Instance as SpadesPopupManager).ShowLeaderboardInfoPopup();
            popup.Init();
        }


        public void DisablePrevious()
        {
            m_topPanelLogic.DisablePrevious();
        }

        private void UpdatePanels(LeaderboardsModel.LeaderboardType tabId = LeaderboardsModel.LeaderboardType.GlobalCurrent)
        {
            if (m_topPanelLogic == null)
                return;

            m_topPanelLogic.ShowItemsTitle(null);
            m_topPanelLogic.StopTimer();

            switch (tabId)
            {
                case LeaderboardsModel.LeaderboardType.GlobalCurrent:
                    m_topPanelLogic.switchToCurrent();

                    if (m_ItemsPanel.HasItems)
                        m_topPanelLogic.StartTimer();
                    return;

                case LeaderboardsModel.LeaderboardType.GlobalPrevious:
                    m_topPanelLogic.switchToPrevious();
                    break;

                case LeaderboardsModel.LeaderboardType.Friends:
                    m_topPanelLogic.hideButtons();
                    m_topPanelLogic.ShowItemsTitle("TOP SPADES ROYALE FACEBOOK FRIENDS");
                    break;

                case LeaderboardsModel.LeaderboardType.HallOfFame:
                    m_topPanelLogic.hideButtons();
                    m_topPanelLogic.ShowItemsTitle("TOP SPADES ROYALE PLAYERS OF ALL TIME");
                    break;
            }
        }

        public LeaderboardsTopPanel GetTopPanel()
        {
            return m_topPanelLogic;
        }

        public void OpenInviteFriends()
        {
            //m_manager.HideScreen();

            Close();
            LeaderboardsController.Instance.HideScreen();
            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndKeep, SocialPopup.SocialTab.Invite);
        }

        public LeaderboardAlerts Alerts
        {
            get
            {
                return m_alerts;
            }
        }
    }
}

