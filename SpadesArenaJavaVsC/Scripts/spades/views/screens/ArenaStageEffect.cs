using System.Collections.Generic;

namespace spades.views.screens
{
    public class ArenaStageEffect : MonoBehaviour
    {

        public enum DISPLAY_SELECTOR
        {
            NONE, SWIRL_ONLY, REGULAR
        }

        [SerializeField] ParticleSystem m_lightPS;
        [SerializeField] ParticleSystem m_flickersPS;

        private UnityEngine.ParticleSystem.ColorOverLifetimeModule m_lightColt;
        private UnityEngine.ParticleSystem.ColorOverLifetimeModule m_flickerColt;
        private UnityEngine.ParticleSystem.MinMaxGradient m_color;

        public void Init(List<Color> color, bool keyPointStage, bool finalStage)
        {
            m_lightColt = m_lightPS.colorOverLifetime;
            m_flickerColt = m_flickersPS.colorOverLifetime;

            Gradient gradient = new Gradient();
            gradient.mode = GradientMode.Blend;

            GradientAlphaKey[] alphaKeys = new GradientAlphaKey[3];

            alphaKeys[0].alpha = 0f;
            alphaKeys[0].time = 0f;

            alphaKeys[1].alpha = 0.73f;
            alphaKeys[1].time = 0.11f;

            alphaKeys[2].alpha = 0f;
            alphaKeys[2].time = 1f;

            GradientColorKey[] colorKeys = new GradientColorKey[2];
            colorKeys[0].color = Color.white;
            colorKeys[0].time = 0f;

            colorKeys[1].color = color[1];
            colorKeys[1].time = 0.54f;

            gradient.SetKeys(colorKeys, alphaKeys);

            m_color = new ParticleSystem.MinMaxGradient(gradient);
        }

        public void Display(DISPLAY_SELECTOR display)
        {
            m_lightColt.color = m_color;
            m_flickerColt.color = m_color;

            switch (display)
            {
                case DISPLAY_SELECTOR.NONE:
                    m_lightPS.gameObject.SetActive(false);
                    m_flickersPS.gameObject.SetActive(false);

                    break;

                case DISPLAY_SELECTOR.REGULAR:
                    m_lightPS.gameObject.SetActive(true);
                    m_flickersPS.gameObject.SetActive(true);
                    break;
                default:
                    m_lightPS.Clear(true);
                    m_flickersPS.Clear(true);

                    break;
            }

        }
    }
}