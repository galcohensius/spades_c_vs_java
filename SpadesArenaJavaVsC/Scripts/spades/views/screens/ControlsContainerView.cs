using spades.models;
using spades.controllers;
using common.utils;
using System;

namespace spades.views.screens
{
    public class ControlsContainerView : MonoBehaviour
    {
        [SerializeField] GameObject m_loseState = null;
        [SerializeField] GameObject m_playState = null;
        [SerializeField] TMP_Text m_points_text = null;
        [SerializeField] TMP_Text m_bags_count_text = null;
        [SerializeField] TMP_Text m_bags_penalty_text = null;
        [SerializeField] TMP_Text m_cost_text = null;
        [SerializeField] GameObject m_spades_wait = null;
        [SerializeField] GameObject m_rebuy_button_text = null;
        RebuyRequested m_RebuyRequested;
        RebuyQuitRequested m_RebuyQuitRequested;

        public enum ControlsContainerState
        {
            PLAY_MODE,
            REBUY_MODE
        }

        public void setState(ControlsContainerState state, int? rebuy, RebuyRequested rebuy_requested_delegate, RebuyQuitRequested rebuy_quit_requested_delegate,SpadesActiveArena arena)
        {
            m_RebuyRequested = rebuy_requested_delegate;
            m_RebuyQuitRequested = rebuy_quit_requested_delegate;

            if(state== ControlsContainerState.PLAY_MODE)
            {
                m_playState.SetActive(true);
                m_loseState.SetActive(false);

                m_points_text.text = arena.GetCurrArenaMatch().LowPoints + "/" + arena.GetCurrArenaMatch().HighPoints;
                m_bags_count_text.text = arena.GetCurrArenaMatch().BagsCount.ToString();
                m_bags_penalty_text.text = arena.GetCurrArenaMatch().BagsPanelty.ToString();

            }
            else
            {
                m_playState.SetActive(false);
                m_loseState.SetActive(true);
                m_cost_text.text = String.Format("<sprite=1> {0}", FormatUtils.FormatBuyIn(rebuy));
            }
           

        }

        public void OnRebuyClick()
        {
            //show spades here
            m_spades_wait.SetActive(true);
            m_rebuy_button_text.SetActive(false);

            m_RebuyRequested(()=> { m_spades_wait.SetActive(false);
                                    m_rebuy_button_text.SetActive(true); });
        }

        public void OnRebuyQuitClick()
        {
            m_RebuyQuitRequested(false);
        }
    }
}
