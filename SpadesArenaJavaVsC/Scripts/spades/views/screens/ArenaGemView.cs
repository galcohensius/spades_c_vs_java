using System.Collections.Generic;
using spades.models;
using spades.controllers;
using System;
using cardGames.models;
using cardGames.controllers;
using cardGames.views;

namespace spades.views.screens
{
    public class ArenaGemView : MonoBehaviour
    {
        [SerializeField] Image m_gem = null;
        [SerializeField] List<GameObject> m_gems_objects = null;
        [SerializeField] GameObject m_gemEffectHolder = null;
        [SerializeField] GameObject m_rightSidePanelAvatar = null;

        [SerializeField] TMP_Text m_desc_text = null;
        [SerializeField] TMP_Text m_gems_total_count_text = null;
        [SerializeField] TMP_Text m_gems_current_count_text = null;
        private SpadesWorld m_world = null;


        public void Init(SpadesArena arena, ArenaScreen.BackFromReason reason)
        {
            m_world = (SpadesWorld)arena.World;

            //set object size to 20% of screen width
            Vector2 size = gameObject.GetComponent<RectTransform>().sizeDelta;
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(ManagerView.Instance.Actual_width * .25f, size.y);

            //clean
            foreach (Transform T in m_gemEffectHolder.transform)
            {
                Destroy(T.gameObject);
            }

            MAvatarModel newModel = new MAvatarModel();
            MAvatarController.Instance.CopyMavatarModel(ModelManager.Instance.GetUser().GetMAvatar(), newModel);
            // TODO: what should be here?
            //newModel = SpadesAvatarCreationController.Instance.CreateAvatarModelCopyWithSpecifiedGemBaseKey(newModel, arena.World.GetID(), true);
            switch (arena.World.GetID())
            {
                case "1":
                    // assetbundle 1 is the basic bundle for the avatars
                    if (arena.World.Arena_Gems_Won == 0)
                    {
                        // 1 stone
                        newModel.Trophy = new MavatarItem("1_t01");
                    }
                    else if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 2 stones
                        newModel.Trophy = new MavatarItem("1_t02");
                    }
                    else if (arena.World.Arena_Gems_Won >= 2)
                    {
                        // 3 stones
                        newModel.Trophy = new MavatarItem("1_t03");
                    }
                    break;

                case "2":
                    if (arena.World.Arena_Gems_Won == 0)
                    {
                        // 1 stone
                        newModel.Trophy = new MavatarItem("1_t04");
                    }
                    else if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 2 stones
                        newModel.Trophy = new MavatarItem("1_t05");
                    }
                    else if (arena.World.Arena_Gems_Won >= 2)
                    {
                        // 3 stones
                        newModel.Trophy = new MavatarItem("1_t06");
                    }
                    break;

                case "3":
                    if (arena.World.Arena_Gems_Won == 0)
                    {
                        // 1 stone
                        newModel.Trophy = new MavatarItem("1_t07");
                    }
                    else if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 2 stones
                        newModel.Trophy = new MavatarItem("1_t08");
                    }
                    else if (arena.World.Arena_Gems_Won >= 2)
                    {
                        // 3 stones
                        newModel.Trophy = new MavatarItem("1_t09");
                    }
                    break;

                case "4":
                    if (arena.World.Arena_Gems_Won == 0)
                    {
                        // 1 stone
                        newModel.Trophy = new MavatarItem("1_t10");
                    }
                    else if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 2 stones
                        newModel.Trophy = new MavatarItem("1_t11");
                    }
                    else if (arena.World.Arena_Gems_Won >= 2)
                    {
                        // 3 stones
                        newModel.Trophy = new MavatarItem("1_t12");
                    }

                    break;
                case "5":
                    // assetbundle 1 is the basic bundle for the avatars
                    // t17 is the id of the master ring
                    newModel.Trophy = new MavatarItem("1_t17");
                    break;
                case "6":
                    // assetbundle 1 is the basic bundle for the avatars
                    // t18 is the id of the joker ring
                    newModel.Trophy = new MavatarItem("1_t18");
                    break;
            }

            m_rightSidePanelAvatar.GetComponent<MAvatarView>().SetAvatarModel(newModel);
            GameObject gem = m_gems_objects[arena.World.Index];
            GameObject m_gem_object = (GameObject)Instantiate(gem);
            m_gem_object.transform.SetParent(m_gemEffectHolder.transform);
            m_gem_object.GetComponent<RectTransform>().localPosition = Vector3.zero;// LobbyController.Instance.Lobby_view.Gems_objects_positions[m_arena.SpadesWorld.Index];
            m_gem_object.GetComponent<RectTransform>().localScale = new Vector3(.5f, .5f, .5f);

            m_gem.sprite = LobbyController.Instance.Lobby_view.Gems_lobby_sprite[arena.World.Index];

            m_gem.SetNativeSize();

            if (arena.World.Index >= 4)
            {
                m_desc_text.text = String.Format("Complete the arena {0} {2}\n time{1} to get the ring", m_world.Arena_Gem_Total_Steps - m_world.Arena_Gem_Curr_Step, m_world.Arena_Gem_Total_Steps - m_world.Arena_Gem_Curr_Step > 1 ? "s" : "", m_world.Arena_Gem_Curr_Step == 0 ? "" : "more");
                gameObject.transform.GetChild(1).GetChild(2).GetComponent<TMP_Text>().text = "GET THE RING";
                m_gem.gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.7f, 0.7f, 0.7f);
            }
            else
            {
                m_desc_text.text = String.Format("Complete the arena {0} {2}\n time{1} to get the gem", m_world.Arena_Gem_Total_Steps - m_world.Arena_Gem_Curr_Step, m_world.Arena_Gem_Total_Steps - m_world.Arena_Gem_Curr_Step > 1 ? "s" : "", m_world.Arena_Gem_Curr_Step == 0 ? "" : "more");
                gameObject.transform.GetChild(1).GetChild(2).GetComponent<TMP_Text>().text = "GET THE GEM";
                m_gem.gameObject.GetComponent<RectTransform>().localScale = Vector3.one;

            }

            if (reason == ArenaScreen.BackFromReason.big_win)
            {
                m_gems_current_count_text.text = (m_world.Arena_Gem_Curr_Step - 1).ToString();
            }
            else if (reason == ArenaScreen.BackFromReason.big_win_with_gem)
            {
                m_gems_current_count_text.text = (m_world.Arena_Gem_Total_Steps - 1).ToString();
            }
            else
            {
                m_gems_current_count_text.text = m_world.Arena_Gem_Curr_Step.ToString();
            }

            m_gems_total_count_text.text = "/" + m_world.Arena_Gem_Total_Steps.ToString();
        }

        public void UpdateGemsWonCount()
        {
            iTween.PunchScale(m_gems_current_count_text.gameObject, new Vector3(1.2f, 1.2f, 1), 0.8f);
            m_gems_current_count_text.text = m_world.Arena_Gem_Curr_Step.ToString();
        }

        public void UpdateGemsWonCount(int step, bool punchScale)
        {
            if (punchScale)
                iTween.PunchScale(m_gems_current_count_text.gameObject, new Vector3(1.2f, 1.2f, 1), 0.8f);

            m_gems_current_count_text.text = step.ToString();
        }
    }
}
