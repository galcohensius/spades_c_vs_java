﻿using spades.controllers;

namespace spades.views
{

	public class StageFX_SFX_View : MonoBehaviour {

		public void PlaySFX3pipes()
		{
            SpadesSoundsController.Instance.StageFX3Pipes ();
		}

		public void PlaySFX1pipe()
		{
            SpadesSoundsController.Instance.StageFX1Pipe ();
		}

		public void PlayStageFireWorks()
		{
            SpadesSoundsController.Instance.PlayFireWorksSFX (true);
		}

		public void StopStageFireWorks()
		{
            SpadesSoundsController.Instance.PlayFireWorksSFX (false);
		}

		public void PlayChampainge()
		{
            SpadesSoundsController.Instance.Champaigne ();
		}

		public void PlayConfettiSFX()
		{
            SpadesSoundsController.Instance.PlayPopSounds ();
		}
	}
}