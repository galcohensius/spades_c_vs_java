﻿using System.Collections.Generic;

namespace spades.views {
    
	public class BotsLogView : MonoBehaviour {
		
		
		[SerializeField ]TMP_Text logText = null;
		
		List<string> texts = new List<string>();

        public void Start()
        {
#if DEV_MODE || QA_MODE
            CommonAI.Logger.OnDebugLog += AppendDebugLogMsg;
#endif
        }
		
		public void ClearLog() {
			texts.Clear();
			UpdateTexts();
		}
		
		private void AppendDebugLogMsg(string msg)
		{
			texts.Add(msg);
			
			// Max lines are 23
			if (texts.Count > 23) {
				texts.RemoveRange(0,texts.Count-23);
			}
			
			UpdateTexts();
		}
		
		private void UpdateTexts()
		{
			logText.text = "";
			foreach (var text in texts)
			{
				logText.text += text + "\n";
			}
		}
		
	}
	
}