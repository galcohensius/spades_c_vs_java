using common;
using spades.controllers;
using common.mes;
using common.models;
using common.controllers;
using common.utils;
using cardGames.models;
using cardGames.controllers;
using common.ims;

namespace spades.views
{
    public class LobbyButtonsGroupView : MonoBehaviour
    {
        public const string VIDEO_BUTTON_BANNER_LOCATION = "F5";

        [SerializeField] Button m_contestsButton = null;
        [SerializeField] GameObject m_contestsLock = null;
        [SerializeField] TMP_Text m_contestLockText = null;
        [SerializeField] Button m_leaderboardButton = null;
        [SerializeField] GameObject m_leaderboardLock = null;
        [SerializeField] TMP_Text m_leaderboardLockText = null;
        [SerializeField] Button m_promoRoomButton = null;
        [SerializeField] Image m_promoRoomBannerClosed = null;
        [SerializeField] TMP_Text m_promoRoomButtonTitle;

        [SerializeField] Button m_casinoButton = null;
        [SerializeField] GameObject m_CasinoLock = null;
        [SerializeField] TMP_Text m_CasinoLockText = null;


        [SerializeField] GameObject m_inviteFriendsButton;
        [SerializeField] GameObject m_rewardedVideoButton;
        [SerializeField] TMP_Text m_inviteBonusText;
        [SerializeField] TMP_Text m_videoBonusText;
        [SerializeField] Image m_videoButtonIcon = null;


        private bool m_isContestsReleased;
        bool m_promoRoomClicked;

        private MESMaterial m_promoRoomMaterial = null;

        private void Start()
        {
            // Set the special room title
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_promoRoomButtonTitle.text = ldc.GetSetting("SpecialRoomTitle", "Jokers Room");

                m_videoBonusText.gameObject.SetActive(ldc.GetSettingAsBool("ShowVideoBonusAmount", true));
                // show the contests or leaderboard button in the lobby footer
                m_isContestsReleased = ldc.GetSettingAsBool("ContestsReleased", true);
                ActivateContestsButton();


                // If no IMS, show invite friends
                if (!SpadesIMSController.Instance.HasEvents)
                {
                    ShowInviteFriendsButton();

                    ShowCasinoButtonClosedOrLocked();
                }

            };

            IMSController.Instance.IMSEventDataChanged += IMSDataArrived;
            IMSDataArrived();

        }

        private void IMSDataArrived()
        {
            // Default show the invite
            ShowInviteFriendsButton();

            // Check if we need to show the video instead
            if (IMSController.Instance.BannersByLocations.ContainsKey(VIDEO_BUTTON_BANNER_LOCATION))
            {
                if (IMSController.Instance.BannersByLocations[VIDEO_BUTTON_BANNER_LOCATION].Count > 0)
                {
                    int imsZoneActionID = IMSController.Instance.BannersByLocations[VIDEO_BUTTON_BANNER_LOCATION][0].Action.Id;

                    if (imsZoneActionID == MESBase.ACTION_SHOW_REWARDED_VIDEO)
                    {
                        ShowVideoButtonIfNotCapped();
                    }

                }
            }
        }

        private void OnEnable()
        {
            m_promoRoomClicked = false;
        }


        public void PromoRoomActivate()
        {
            m_promoRoomBannerClosed.gameObject.SetActive(false);
            m_promoRoomButton.interactable = true;
        }

        public void PromoRoomDeactivate()
        {
            m_promoRoomBannerClosed.gameObject.SetActive(true);
            m_promoRoomButton.interactable = false;

        }

        public void ShowLeaderboardsLock()
        {
            m_leaderboardButton.gameObject.GetComponent<CanvasGroup>().alpha = 1f;
            m_leaderboardButton.gameObject.GetComponent<ButtonsOverView>().Disabled = true;
            m_leaderboardLockText.text = "LVL " + LeaderboardsModel.LEADERBOARD_MIN_LEVEL.ToString();
            m_leaderboardLock.SetActive(true);
        }

        public void HideLeaderboardsLock()
        {
            m_leaderboardButton.gameObject.GetComponent<CanvasGroup>().alpha = 1f;
            m_leaderboardButton.gameObject.GetComponent<ButtonsOverView>().Disabled = false;
            m_leaderboardLock.SetActive(false);
        }


        public void ShowInviteFriendsButton()
        {
            // Show invite friends button 

            // Get the invite friends bonus amount
            Bonus inviteBonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Invite);
            m_inviteBonusText.text = string.Format("<sprite=1>{0}", FormatUtils.FormatPrice(inviteBonus?.CoinsSingle));

            m_rewardedVideoButton.SetActive(false);
            m_inviteFriendsButton.SetActive(true);
        }

        public void ShowVideoButtonIfNotCapped()
        {
            // Show video button if not capped

            // Init VideoAdController
            VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(ModelManager.Instance.GetUser());

            VideoAdController.Instance.WaitOnVideoPlacementCapReady((isCapped) =>
            {
                if (!isCapped)
                {
                    // Get the video bonus amount
                    Bonus rewardedVideoBonus = ModelManager.Instance.FindBonus(Bonus.BonusTypes.RewardedVideoBonus);
                    m_videoBonusText.text = string.Format("<sprite=1>{0}", FormatUtils.FormatPrice(rewardedVideoBonus?.CoinsSingle));

                    m_rewardedVideoButton.SetActive(true);
                    m_inviteFriendsButton.SetActive(false);
                } else
                {
                    ShowInviteFriendsButton();
                }
            });
        }

        public void ShowCasinoButtonClosedOrLocked()
        {
            // Delay added here to make sure the user is updated after login and that the min casion level is updated.
            UnityMainThreadDispatcher.Instance.DelayedCall(0.5f, () =>
            {
                m_CasinoLock.SetActive(true);
              //  m_casinoButton.interactable = false;

                if (ModelManager.Instance.GetUser().GetLevel() >= SideGamesController.Instance.MinCasinoLevel)
                    m_CasinoLockText.text = "Closed";
                else
                    m_CasinoLockText.text = "LVL " + SideGamesController.Instance.MinCasinoLevel;
            });
        }

        /// <summary>
        /// Invoked from IMSBanner event
        /// </summary>
        public void ShowCasinoButtonOpenOrLocked()
        {
            // Delay added here to make sure the user is updated after login and that the min casion level is updated.
            UnityMainThreadDispatcher.Instance.DelayedCall(0.5f, () =>
            {
                if (ModelManager.Instance.GetUser().GetLevel() >= SideGamesController.Instance.MinCasinoLevel)
                {
                    m_CasinoLock.SetActive(false);
                //    m_casinoButton.interactable = true;
                }
                else
                {
                    m_CasinoLock.SetActive(true);
                    m_CasinoLockText.text = "LVL " + SideGamesController.Instance.MinCasinoLevel;
                }
            });

        }


        public void ActivateContestsButton()
        {
            if (ModelManager.Instance.ContestsLists.AllContests.Count == 0 && !m_isContestsReleased)
            {
                m_leaderboardButton.gameObject.SetActive(true);
                m_contestsButton.gameObject.SetActive(false);
            }
            else
            {
                // Contests button
                m_leaderboardButton.gameObject.SetActive(false);
                m_contestsButton.gameObject.SetActive(true);

                int contestMinLevel = ContestsController.Instance.Min_contests_level;
                if (ModelManager.Instance.GetUser().GetLevel() < contestMinLevel)
                {
                    m_contestsLock.SetActive(true);
                //    m_contestsButton.interactable = false;
                    m_contestLockText.text = "LVL " + contestMinLevel;
                }
                else
                {
                    m_contestsLock.SetActive(false);
                //    m_contestsButton.interactable = true;
                }
            }
        }
    }
}

