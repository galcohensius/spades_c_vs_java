﻿using cardGames.views;
using common.controllers;
using spades.controllers;

namespace spades.views
{
    public class SpadesLeftControlPanelView : LeftControlPanelView
    {
        protected override void InitTabButtons(int activeTabButtonIndex)
        {
            foreach (GameObject tabButtonObject in m_tabButtonsList)
            {
                tabButtonObject.transform.parent.GetComponent<Image>().sprite = SpadesPopupManager.Instance.Tab_Off_sprite;
                tabButtonObject.transform.parent.GetComponent<Image>().SetNativeSize();
                tabButtonObject.transform.GetChild(0).GetComponent<TMP_Text>().fontMaterial = SpadesPopupManager.Instance.Tab_Off_Material;
            }

            if (m_tabButtonsList[activeTabButtonIndex] != null)
            {

                m_tabButtonsList[activeTabButtonIndex].transform.parent.GetComponent<Image>().sprite = SpadesPopupManager.Instance.Tab_On_sprite;
                m_tabButtonsList[activeTabButtonIndex].transform.parent.GetComponent<Image>().SetNativeSize();
                m_tabButtonsList[activeTabButtonIndex].transform.GetChild(0).GetComponent<TMP_Text>().fontMaterial = SpadesPopupManager.Instance.Tab_On_Material;

                //m_tabButtonsList[activeTabButtonIndex].transform.SetAsLastSibling();
            }
            else
            {
                LoggerController.Instance.LogError("Missing Tab Button / Link Inspector");
            }
        }
    }
}