﻿using System;

namespace spades.views
{

	public class NetworkErrorView : MonoBehaviour {


		[SerializeField] TMP_Text 			m_message_text=null;
		[SerializeField] TMP_Text 			m_caption_button=null;
		Action								m_button_action=null;

		public string Message
		{
			set
			{
				m_message_text.text = value;
			}
		}


		public void BT_Button_Clicked()
		{
            gameObject.SetActive(false);

			if (m_button_action != null)
			{
				m_button_action ();
			}
			
		}


		public Action Button_action {
			get {
				return m_button_action;
			}
			set {
				m_button_action = value;
			}
		}


		public TMP_Text Caption_button {
			get {
				return m_caption_button;
			}
			set {
				m_caption_button = value;
			}
		}
	}
}
