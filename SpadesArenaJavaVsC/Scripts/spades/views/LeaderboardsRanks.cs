﻿using common.models;
using cardGames.models;

namespace spades.views
{
	public class LeaderboardsRanks: MonoBehaviour
	{
        [SerializeField] GameObject[] rankRows= null;

		public void UpdateRanksView (LeaderboardsModel.LeaderboardType lt)
		{
			
			for (int i = 0; i < rankRows.Length; i++) {
				rankRows [i].SetActive (false);
			}

			if (ModelManager.Instance.Leaderboards.GetLeaderboard(lt) != null && ModelManager.Instance.Leaderboards.GetLeaderboard(lt).LeaderboardMetaData != null) {
				LeaderboardMeta ldMeta = ModelManager.Instance.Leaderboards.GetLeaderboard (lt).LeaderboardMetaData;
                for (int i = 0; i < ldMeta.UserRanks.Count; i++) {
					LeaderboardTopRankRow rowScript = rankRows [i].GetComponent<LeaderboardTopRankRow> ();
					rowScript.Coins = ldMeta.UserPrizes[i];
					rowScript.gameObject.SetActive (true);
				}

                for (int i = 0; i < ldMeta.UserRanks.Count; i++) {
					LeaderboardTopRankRow rowScript = rankRows [i].GetComponent<LeaderboardTopRankRow> ();
					rowScript.setPlace(formatPlaces(ldMeta.UserRanks[i]));
				}

			}
		}

		private string formatPlaces(string place)
		{
            // If the min and max place are the same, return a single number
            string[] tokens = place.Split('-');
            if (tokens.Length > 1 && tokens[0] == tokens[1])
                return tokens[0];

            return place;
		}
			
	}
}

