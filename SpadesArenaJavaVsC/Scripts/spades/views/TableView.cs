using System.Collections;
using System.Collections.Generic;
using spades.controllers;
using spades.models;
using common;
using System;
using common.utils;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views
{
    /// <summary>
    /// Table view visualization - currnetly sitting on the table in the scene - holds the references to the players view and cards thrown on the table
    /// </summary>
    public class TableView : MonoBehaviour
    {
        // cards positioning params - to modify the hand curve
        public const float CURVE = 0.3f;
        public const float Y_ELEVATION = -1.5f;
        public const float X_OFFSET = 130f;
        public const float CARD_THROW_TIME = 0.5f;
        public const float DEAL_INDICATION_MOVE_TIME = 0.7f;

        public const float CARD_SCALE_DOWN = .7f;

        public const int NUM_CARDS_TO_DEAL = 13;

        public const float CENTER_OFFSET_X = 200f;
        public const float TOP_CENTER_OFFSET_Y = 60f;
        public const float SIDES_CENTER_OFFSET_Y = -60f;
        public const float CENTER_ROTATE_RANGE = 0f;


        List<GameObject> m_thrown_cards = new List<GameObject>();   //this will hold the cards thrown on the table 

        Vector3[] m_dealerChipPositions = new Vector3[4];
        Vector3[] m_cardEndPositions = new Vector3[4]; // Used for cards movements
        Vector3 m_cardScale;

        [SerializeField] List<PlayerView> m_player_views = null;

        Vector3 m_thrown_cards_org_position;
        [SerializeField] GameObject m_roundCardsObject = null;
        [SerializeField] GameObject m_highCardRoot = null;

        [SerializeField] GameObject m_dealer_chip = null;

        [SerializeField] GameObject m_game_table_object = null;
        [SerializeField] GameObject m_spades_broken_object = null;

        [SerializeField] GameObject m_game_ui_object = null;

        [SerializeField] GameObject m_contest_panel = null;

        [SerializeField] bool m_players_created = false;

        List<GameObject> m_cards_view = new List<GameObject>();

        [SerializeField] Image m_table_image = null;

        [SerializeField] CardView[] m_spades_broken_card = null;

        [SerializeField] GameObject m_onboard_object = null;
        [SerializeField] TMP_Text m_onboard_text = null;

        [SerializeField] GameObject m_playerHand = null;

        [SerializeField] GameObject m_partners_points_table = null;
        [SerializeField] TMP_Text m_your_team_points_text = null;
        [SerializeField] TMP_Text m_rival_team_points_text = null;
        [SerializeField] TMP_Text m_your_team_bags_text = null;
        [SerializeField] TMP_Text m_rival_team_bags_text = null;

        [SerializeField] TMP_Text m_coins4PointsFactorText = null;

        IEnumerator m_add_dummy_cards = null;

        private void Awake()
        {
            m_dealerChipPositions[0] = new Vector3(-597f, 183f, 0f);
            m_dealerChipPositions[1] = new Vector3(174f, 255f, 0f);
            m_dealerChipPositions[2] = new Vector3(716f, -21f, 0f);
            m_dealerChipPositions[3] = new Vector3(-521f, -210f, 0f);

            m_cardEndPositions[0] = new Vector3(-600, 0, 0);
            m_cardEndPositions[1] = new Vector3(0, 250, 0);
            m_cardEndPositions[2] = new Vector3(600, 0, 0);
            m_cardEndPositions[3] = new Vector3(0, -300, 0);

            m_cardScale = new Vector3(3.5f, 3.5f, 3.5f);
        }

        public void BounceAllHeads()
        {
            //m_player_views[0].GetComponent<PlayerView>().DynamicAvatarView.Bounce();
        }


        public void TableInit(PlayingMode playing_mode)
        {
            if (ManagerView.Instance.Loaded_table != null)
                m_table_image.sprite = ManagerView.Instance.Loaded_table;

            m_game_table_object.SetActive(true);
            m_game_ui_object.SetActive(true);
            m_contest_panel.SetActive(true);

            for (int i = 0; i < m_player_views.Count; i++)
            {
                Player player = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i);

                //m_player_views[i].name = "Player" + i;
                m_player_views[i].SetModel(player);
                m_player_views[i].SetAvatarModel(((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(i).GetMAvatarModel());

                ResetPlayerIndications();
                HideTimers();
                ShowDealerIndication(0, true, false);

            }

            AdjustPlayersPositions();


            foreach (Transform child in transform.Find("RoundCards").transform)
                Destroy(child.gameObject);

            m_players_created = true;


            if (ManagerView.Instance.Aspect43)
            {
                // Move the player hand
                m_playerHand.transform.localPosition = new Vector2(0, -100);
            }

            // Coins 4 points / High Low
            ISpadesMatch spadesMatch = SpadesModelManager.Instance.GetActiveMatch().GetMatch();

            if ((spadesMatch.SubVariant == PlayingSubVariant.Coins4Points || spadesMatch.SubVariant == PlayingSubVariant.HiLo)
                && spadesMatch.Coins4PointsFactor > 0)
            {
                m_coins4PointsFactorText.text = "<sprite=1>" + FormatUtils.FormatBalance(spadesMatch.Coins4PointsFactor);
            }
            else
            {
                m_coins4PointsFactorText.text = "";
            }

        }

        public void ToggleAvatarObjectsActiveState(bool isActive)
        {
            foreach (PlayerView playerView in m_player_views)
            {
                playerView.gameObject.SetActive(isActive);
            }

            TableChatController.Instance.CloseEmojiMenu();
        }





        public void HideTimers()
        {
            for (int i = 0; i < 4; i++)
            {
                m_player_views[i].GetComponent<PlayerView>().ShowHideTimer(false);
            }
        }


        public void ShowGameUI(bool show)
        {
            m_game_table_object.SetActive(show);
            m_game_ui_object.SetActive(show);
            m_contest_panel.SetActive(true);

            if (!show)
            {
                m_onboard_object.SetActive(false);
                m_contest_panel.GetComponent<ContestPanelView>().StopTweensAndExitPanel();
            }
        }

        public void ResetPlayerIndications()
        {
            foreach (PlayerView playerView in m_player_views)
            {
                playerView.ResetPlayerIndications();
            }
        }

        public void SetCardsSizes()
        {
            for (int i = 0; i < 4; i++)
            {
                m_player_views[i].SetCardsSize(m_cardScale);
            }
        }

        /// <summary>	WILL BE CONVERTED TO AN EVENT
        /// triggers the reveal card in the player 
        /// </summary>
        public void RevealCards()
        {

            m_player_views[3].GetComponent<PlayerView>().RevealCards(false, true);

            /*
            GameObject hand = m_player_views[3].GetComponent<PlayerView>().Hand;

            float new_pos_x = hand.GetComponent<RectTransform>().localPosition.x - 50;

            iTween.MoveTo(hand, iTween.Hash("x", new_pos_x, "islocal", true, "easeType", "linear", "time", .2f, "delay", 0.1f));
            */

        }

        private void AdjustPlayersPositions()
        {

            m_player_views[3].gameObject.SetActive(true);

            m_thrown_cards_org_position = new Vector3(-6f, 111f, 0);

            //m_thrown_cards_object.GetComponent<RectTransform>().localPosition = m_thrown_cards_org_position;
        }


        /// <summary> WILL BE CONVERTED TO AN EVENT
        /// triggers the Arranges the hand cards curve.
        /// </summary>
        public void ArrangeHandCardsCurve()
        {
            m_player_views[3].GetComponent<PlayerView>().MoveCardsToPositions(false);
        }

        /// <summary>
        /// Deletes all player cards visually - used for restart
        /// </summary>
        public void DeleteAllPlayerCards()
        {
            for (int i = 0; i < m_player_views.Count; i++)
            {
                m_player_views[i].DeleteAllCards();
                m_player_views[i].HideBidSpeechBubble();
            }
        }


        public void HideBidSignsAndBubbles()
        {
            foreach (var playerView in m_player_views)
            {
                playerView.HideBidSpeechBubble();

            }
        }


        /// <summary>
        /// Clears the table thrown objects - used for restart
        /// </summary>
        public void ClearTable()
        {
            foreach (Transform Child in m_roundCardsObject.transform)
            {
                Destroy(Child.gameObject);
            }

            foreach (Transform Child in m_highCardRoot.transform)
            {
                Destroy(Child.gameObject);
            }

            m_thrown_cards.Clear();

            m_roundCardsObject.GetComponent<RectTransform>().localPosition = m_thrown_cards_org_position;

            CanvasGroup CardColorFade = m_roundCardsObject.GetComponent<CanvasGroup>();
            CardColorFade.alpha = 1f;

        }

        public PlayerView GetPlayerView(int index)
        {
            return m_player_views[index];
        }

        /// <summary>
        /// Adds the card to table visually - animation will be added thats why also table pos needed to start the animation from the right place
        /// </summary>
        public void AddCardToTable(Vector3 position, Vector3 rotation, Card cardmodel, int location_index)
        {
            m_thrown_cards.Add((GameObject)Instantiate(ManagerView.Instance.Card_prefab));  //adding a card and setting its image
            m_thrown_cards[m_thrown_cards.Count - 1].transform.SetParent(m_roundCardsObject.transform);
            m_thrown_cards[m_thrown_cards.Count - 1].transform.SetAsLastSibling();
            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<RectTransform>().localScale = m_cardScale * CARD_SCALE_DOWN;
            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<RectTransform>().localPosition = position;
            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<ButtonsOverView>().enabled = false;
            Vector3 temp_rotation = new Vector3(0, rotation.y, rotation.z);
            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<RectTransform>().localEulerAngles = temp_rotation;

            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<CardView>().SetCardModel(cardmodel);
            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<CardView>().RevealCard();
            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<CardView>().SetInteractable(false);
            m_thrown_cards[m_thrown_cards.Count - 1].name = "Card" + location_index;

            m_thrown_cards[m_thrown_cards.Count - 1].GetComponent<CardView>().ShowHideShadow(false);

        }

        public void PlaySpadesBroken()
        {
            m_spades_broken_object.GetComponent<Animator>().SetTrigger("broken");
        }



        public void ResetSpadesBrokenAnimations(bool reset)
        {
            m_game_table_object.GetComponent<Animator>().SetBool("Exit", reset);
        }

        public IEnumerator ComputerThrowSpadesBorkenCard(int location, Card card_model, Action anim_done_callback)
        {
            //remove card from player model - which will also handle the view 
            ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(location).DeleteCard(card_model);
            RoundController.Instance.Table_view.GetPlayerView(location).MoveCardsToPositions();

            CardGamesSoundsController.Instance.CardThrown();

            Vector3 temp_rotation;
            float temp_rotation_value = 0;
            float temp_rotation_end_value = 0;


            if (location == 0)
            {
                temp_rotation_value = -90f;
                temp_rotation_end_value = 0f;
            }
            else if (location == 1)
            {
                temp_rotation_value = 180f;
                temp_rotation_end_value = 0f;
            }
            else if (location == 2)
            {
                temp_rotation_value = 90f;
                temp_rotation_end_value = 0f;
            }
            else if (location == 3)
            {
                temp_rotation_value = 0f;
                temp_rotation_end_value = 180f;
            }

            temp_rotation = new Vector3(0, 0, temp_rotation_value);

            float target_x, target_y, temp_target_x, temp_target_y;

            target_x = 0f;
            target_y = 0f;

            ManagerView.Instance.Table.GetComponent<TableView>().Spades_broken_card[location].SetCardModel(card_model);
            ManagerView.Instance.Table.GetComponent<TableView>().Spades_broken_card[location].SpadesBrokenRevealCard();

            if (location == 0)//east
            {
                target_x = -1 * CENTER_OFFSET_X;
                target_y = SIDES_CENTER_OFFSET_Y;
                m_game_table_object.GetComponent<Animator>().SetTrigger("spades_broken_left0");
            }
            else if (location == 1)//top
            {
                target_y = TOP_CENTER_OFFSET_Y;
                m_game_table_object.GetComponent<Animator>().SetTrigger("spades_broken_top1");
            }
            else if (location == 2)//west
            {
                target_x = CENTER_OFFSET_X;
                target_y = SIDES_CENTER_OFFSET_Y;
                m_game_table_object.GetComponent<Animator>().SetTrigger("spades_broken_right");
            }

            temp_target_x = target_x;
            temp_target_y = target_y;

            //CanvasGroup	CardColorFade = m_thrown_card_view.GetComponent<CanvasGroup>();

            yield return new WaitForSeconds(1.5f);

            //play sapdes broken
            PlaySpadesBroken();

            yield return new WaitForSeconds(.5f);

            temp_rotation = new Vector3(ManagerView.Instance.Table.GetComponent<RectTransform>().localEulerAngles.x, temp_rotation.y, temp_rotation_end_value);

            //adding card to the table 
            AddCardToTable(new Vector3(temp_target_x, temp_target_y, 0), temp_rotation, card_model, location);
            //removing the temp card that was made for throwing

            //adding card to table model - this is were we check the score against
            ((SpadesTable)ModelManager.Instance.GetTable()).AddThrownCardToTable(location, card_model);

            yield return new WaitForSeconds(1f);

            RoundController.Instance.Cards_finished_throw_anim++;
            anim_done_callback();
        }

        //this will be the animatino to generate a card throw from player

        public IEnumerator ComputerThrowCard(int pos, Card card_model, Action anim_done_callback)
        //currently computers throw their cards from the center of the player - as they will not have a visual deck 
        {
            //remove card from player model - which will also handle the view 
            ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(pos).DeleteCard(card_model);

            RoundController.Instance.Table_view.GetPlayerView(pos).MoveCardsToPositions();

            CardGamesSoundsController.Instance.CardThrown();

            float f_start_time = Time.time;
            float time_to_move = CARD_THROW_TIME;           //card contraction time

            Vector3 org_location = new Vector3(0, 0, 0);
            Vector3 temp_rotation;
            float temp_rotation_value = 0;
            float temp_rotation_end_value = 0;


            if (pos == 0)
            {
                temp_rotation_value = -90f;
                temp_rotation_end_value = 0f;
            }
            else if (pos == 1)
            {
                temp_rotation_value = 180f;
                temp_rotation_end_value = 0f;
            }
            else if (pos == 2)
            {
                temp_rotation_value = 90f;
                temp_rotation_end_value = 0f;
            }
            else if (pos == 3)
            {
                temp_rotation_value = 0f;
                temp_rotation_end_value = 180f;
            }



            temp_rotation_end_value += -1 * CENTER_ROTATE_RANGE / 2 + UnityEngine.Random.Range(0f, CENTER_ROTATE_RANGE);

            temp_rotation = new Vector3(0, 0, temp_rotation_value);

            GameObject m_thrown_card = ((GameObject)Instantiate(ManagerView.Instance.Card_prefab));

            m_thrown_card.GetComponent<ButtonsOverView>().enabled = false;

            CardView m_thrown_card_view = m_thrown_card.GetComponent<CardView>();

            float temp_scale_factor = m_cardScale.x * CARD_SCALE_DOWN;
            Vector3 temp_scale = new Vector3(temp_scale_factor * 0.8f, temp_scale_factor * 0.8f, temp_scale_factor * 0.8f);

            m_thrown_card_view.SetCardModel(card_model);

            m_thrown_card_view.RevealCard();

            m_thrown_card.transform.SetParent(m_roundCardsObject.transform);

            m_thrown_card.GetComponent<RectTransform>().localScale = temp_scale;

            org_location = m_cardEndPositions[pos];

            m_thrown_card.GetComponent<RectTransform>().localPosition = org_location;

            Transform T = m_thrown_card_view.GetComponent<RectTransform>();

            float start_pos_x = T.localPosition.x;
            float start_pos_y = T.localPosition.y;

            float target_x, target_y, temp_target_x, temp_target_y;

            target_x = 0f;
            target_y = 0f;



            if (pos == 0)//east
            {
                target_x = -1 * CENTER_OFFSET_X;
                target_y = SIDES_CENTER_OFFSET_Y;
            }
            else if (pos == 1)//top
            {
                target_y = TOP_CENTER_OFFSET_Y;
            }
            else if (pos == 2)//west
            {
                target_x = CENTER_OFFSET_X;
                target_y = SIDES_CENTER_OFFSET_Y;
            }

            temp_target_x = target_x;
            temp_target_y = target_y;


            float timeForRot = 0;

            m_thrown_card.GetComponent<RectTransform>().localPosition = new Vector3(start_pos_x, start_pos_y, 0);
            m_thrown_card.GetComponent<RectTransform>().localEulerAngles = temp_rotation;
            m_thrown_card.GetComponent<RectTransform>().localScale = temp_scale;

            //m_thrown_card.GetComponent<Shadow> ().enabled = false;

            m_thrown_card.GetComponent<CardView>().ShowHideShadow(false);

            iTween.MoveTo(m_thrown_card, iTween.Hash("y", target_y, "x", target_x, "islocal", true, "easeType", "easeOutCubic", "time", time_to_move));

            iTween.RotateTo(m_thrown_card, iTween.Hash("z", temp_rotation_end_value, "islocal", true, "easeType", "easeOutCubic", "time", time_to_move));

            iTween.ScaleTo(m_thrown_card, iTween.Hash("x", temp_scale_factor, "y", temp_scale_factor, "z", temp_scale_factor, "islocal", true, "easeType", "easeOutCubic", "time", time_to_move));

            while (timeForRot < time_to_move)
            {
                timeForRot = (Time.time - f_start_time) / time_to_move;

                yield return new WaitForSeconds(0.2f);
            }

            temp_rotation = new Vector3(ManagerView.Instance.Table.GetComponent<RectTransform>().localEulerAngles.x, temp_rotation.y, temp_rotation_end_value);
            //adding card to the table 
            AddCardToTable(new Vector3(temp_target_x, temp_target_y, 0), temp_rotation, card_model, pos);
            //removing the temp card that was made for throwing

            if (m_thrown_card_view != null)
                m_thrown_card_view.DeleteCard();

            RoundController.Instance.Cards_finished_throw_anim++;


            anim_done_callback();
        }

        public IEnumerator HighCardAnim(int pos, Card card_model, bool isDealer, Action highCardAnimFinished)
        {

            GameObject m_thrown_card = ((GameObject)Instantiate(ManagerView.Instance.Card_prefab));
            m_thrown_card.GetComponent<ButtonsOverView>().enabled = false;
            CardView m_thrown_card_view = m_thrown_card.GetComponent<CardView>();

            float scale_factor = m_cardScale.x * CARD_SCALE_DOWN * 0.8f;

            m_thrown_card_view.SetCardModel(card_model);

            m_thrown_card.transform.SetParent(m_highCardRoot.transform);

            m_thrown_card.GetComponent<RectTransform>().localScale = new Vector3(0, 0, 0);

            float target_x = 0, target_y = 0;

            if (pos == 0)//east
            {
                target_x = -470;
                target_y = 100;
            }
            else if (pos == 1)//top
            {
                target_x = 0;
                target_y = 180;
            }
            else if (pos == 2)//west
            {
                target_x = 470;
                target_y = 100;
            }
            else if (pos == 3)//south
            {
                target_x = 0;
                target_y = -200;
            }

            m_thrown_card.GetComponent<RectTransform>().localPosition = new Vector3(target_x, target_y, 0);
            m_thrown_card.GetComponent<CardView>().ShowHideShadow(false);

            string name = "Rot" + pos.ToString();

            yield return new WaitForSeconds(.5f);
            iTween.ScaleTo(m_thrown_card, iTween.Hash("x", scale_factor, "y", scale_factor, "z", scale_factor, "easeType", "easeOutBack", "time", 0.4f));
            yield return new WaitForSeconds(.75f);
            iTween.RotateTo(m_thrown_card, iTween.Hash("y", 130, "easeType", "linear", "time", .15));
            yield return new WaitForSeconds(0.15f);
            m_thrown_card_view.GetComponent<CardView>().RevealCard();
            m_thrown_card.GetComponent<RectTransform>().localScale = new Vector3(-scale_factor, scale_factor, 0);
            iTween.RotateTo(m_thrown_card, iTween.Hash("y", 180, "easeType", "linear", "time", .15));

            yield return new WaitForSeconds(1);

            if (isDealer)
            {
                SpadesSoundsController.Instance.HighCard();
                if (m_thrown_card != null)
                {
                    iTween.ScaleTo(m_thrown_card, iTween.Hash("name", name, "x", -scale_factor * 1.5f, "y", scale_factor * 1.5f, "islocal", true, "easeType", "easeOutBack", "time", 0.1f));
                    iTween.PunchRotation(m_thrown_card, iTween.Hash("name", name, "z", 40f, "time", 1.4f, "delay", 0.1));
                }
            }
            else
            {
                if (m_thrown_card != null)
                {
                    iTween.ScaleTo(m_thrown_card, iTween.Hash("name", name, "x", -scale_factor * .8f, "y", scale_factor * .8f, "islocal", true, "easeType", "easeInSine", "time", 0.2f));
                    m_thrown_card.transform.Find("Card").GetComponent<Image>().color = new Color(0.85f, 0.85f, 0.85f);
                }
            }
            yield return new WaitForSeconds(2f);

            //if (winner)
            //iTween.Stop (m_thrown_card);
            if (m_thrown_card != null)
                iTween.ScaleTo(m_thrown_card, iTween.Hash("x", 0, "y", 0, "z", 0, "islocal", true, "easeType", "easeOutQuad", "time", 0.15f));

            yield return new WaitForSeconds(.35f);


            if (isDealer)
            {
                //show dealer indication
                //ShowDealerIndication(pos, true, true);
                highCardAnimFinished();
            }

        }

        public void ShowDealerIndication(int next, bool first_round, bool show)
        {
            if (first_round)//this is special flag only for the first round
                next++;

            Vector3 StartPos, endPos;

            int curr = next - 1;
            curr = (curr + 4) % 4;

            next = next % 4;

            StartPos = m_dealerChipPositions[curr];
            endPos = m_dealerChipPositions[next];

            m_dealer_chip.SetActive(show);
            m_dealer_chip.GetComponent<RectTransform>().localPosition = StartPos;

            if (!first_round)
            {
                iTween.MoveTo(m_dealer_chip, iTween.Hash("position", endPos, "islocal", true, "easeType", iTween.EaseType.easeInOutCubic, "time", DEAL_INDICATION_MOVE_TIME));

            }

        }

        //currently computers throw their cards from the center of the player - as they will not have a visual deck 
        public IEnumerator ExitAnimation(int pos, Action exit_anim_done)
        {
            RectTransform obj1, obj2, obj3;
            float start_target_z_1, start_target_z_2, start_target_z_3, end_target_z_1, end_target_z_2, end_target_z_3;
            float end_pos_x_1, end_pos_x_2, end_pos_x_3, start_pos_x_1, start_pos_x_2, start_pos_x_3;
            float end_pos_y_1, end_pos_y_2, end_pos_y_3, start_pos_y_1, start_pos_y_2, start_pos_y_3;

            int obj1_index, obj2_index, obj3_index;

            obj1_index = (pos + 5) % 4;    //left of selected winner
            obj2_index = (pos + 3) % 4;    //right of selected winner
            obj3_index = (pos + 2) % 4;    //opposite to selected winner

            CardGamesSoundsController.Instance.CardDragged();

            float f_start_time = Time.time;
            float time_to_move = RoundController.EXIT_ANIM_TIME;          //card contraction time

            Vector3 org_location = new Vector3();
            Vector3 temp_position = new Vector3();

            Transform T = m_roundCardsObject.GetComponent<RectTransform>();

            //for the middle cards to spin
            obj1 = T.Find("Card" + obj1_index).GetComponent<RectTransform>();
            obj2 = T.Find("Card" + obj2_index).GetComponent<RectTransform>();
            obj3 = T.Find("Card" + obj3_index).GetComponent<RectTransform>();

            start_target_z_1 = obj1.localEulerAngles.z;
            start_target_z_2 = obj2.localEulerAngles.z;
            start_target_z_3 = obj3.localEulerAngles.z;

            end_target_z_1 = start_target_z_1;
            end_target_z_2 = start_target_z_2;
            end_target_z_3 = start_target_z_3;

            start_pos_x_1 = obj1.localPosition.x;
            start_pos_x_2 = obj2.localPosition.x;
            start_pos_x_3 = obj3.localPosition.x;
            end_pos_x_1 = 0;
            end_pos_x_2 = 0;
            end_pos_x_3 = 0;

            start_pos_y_1 = obj1.localPosition.y;
            start_pos_y_2 = obj2.localPosition.y;
            start_pos_y_3 = obj3.localPosition.y;
            end_pos_y_1 = 0;
            end_pos_y_2 = 0;
            end_pos_y_3 = 0;


            float movesides = 210f;

            float movedown2 = 170f;


            float movedown = 300f;

            if (pos == 0)
            {
                end_pos_y_1 = start_pos_y_1 - movesides / 2 + UnityEngine.Random.Range(-10f, 10f);
                end_pos_y_2 = start_pos_y_2 + movesides / 2 + UnityEngine.Random.Range(-10f, 10f);

                end_pos_x_1 = start_pos_x_1 - movedown2 + UnityEngine.Random.Range(-10f, 10f);
                end_pos_x_2 = start_pos_x_2 - movedown2 + UnityEngine.Random.Range(-10f, 10f);

                end_pos_x_3 = start_pos_x_3 - movedown + UnityEngine.Random.Range(-15f, 15f);
                end_pos_y_3 = start_pos_y_3 + UnityEngine.Random.Range(-15f, 15f);
            }
            else if (pos == 1)
            {
                end_pos_x_1 = start_pos_x_1 - movesides + UnityEngine.Random.Range(-10f, 10f);
                end_pos_x_2 = start_pos_x_2 + movesides + UnityEngine.Random.Range(-10f, 10f);

                end_pos_y_1 = start_pos_y_1 + movedown2 + UnityEngine.Random.Range(-10f, 10f);
                end_pos_y_2 = start_pos_y_2 + movedown2 + UnityEngine.Random.Range(-10f, 10f);

                end_pos_y_3 = start_pos_y_3 + movedown - 100 + UnityEngine.Random.Range(-15f, 15f);
                end_pos_x_3 = start_pos_x_3 + UnityEngine.Random.Range(-15f, 15f);
            }
            else if (pos == 2)
            {
                end_pos_y_1 = start_pos_y_1 + movesides / 2 + UnityEngine.Random.Range(-10f, 10f);
                end_pos_y_2 = start_pos_y_2 - movesides / 2 + UnityEngine.Random.Range(-10f, 10f);

                end_pos_x_1 = start_pos_x_1 + movedown2 + UnityEngine.Random.Range(-10f, 10f);
                end_pos_x_2 = start_pos_x_2 + movedown2 + UnityEngine.Random.Range(-10f, 10f);

                end_pos_x_3 = start_pos_x_3 + movedown + UnityEngine.Random.Range(-15f, 15f);
                end_pos_y_3 = start_pos_y_3 + UnityEngine.Random.Range(-15f, 15f);
            }
            else if (pos == 3)
            {
                end_pos_x_1 = start_pos_x_1 + movesides + UnityEngine.Random.Range(-10f, 10f);
                end_pos_x_2 = start_pos_x_2 - movesides + UnityEngine.Random.Range(-10f, 10f);

                end_pos_y_1 = start_pos_y_1 - movedown2 + 50f + UnityEngine.Random.Range(-10f, 10f);
                end_pos_y_2 = start_pos_y_2 - movedown2 + 50f + UnityEngine.Random.Range(-10f, 10f);

                end_pos_y_3 = start_pos_y_3 - movedown + 100 + UnityEngine.Random.Range(-15f, 15f);
                end_pos_x_3 = start_pos_x_3 + UnityEngine.Random.Range(-15f, 15f);
            }


            T.localPosition = m_thrown_cards_org_position;

            float start_pos_x = T.localPosition.x;
            float start_pos_y = T.localPosition.y;

            float target_x, target_y;

            org_location = m_cardEndPositions[pos];

            if (pos % 2 == 0)  //fixing the exit drag animation for the two side players - 
                org_location += m_thrown_cards_org_position;

            target_x = org_location.x;
            target_y = org_location.y;

            CanvasGroup CardColorFade = m_roundCardsObject.GetComponent<CanvasGroup>();

            float timeForRot = 0;

            float obj1_rot = 0;
            float obj2_rot = 0;
            float obj3_rot = 0;

            float temp_scale_parent = m_roundCardsObject.GetComponent<RectTransform>().localScale.x * 0.8f;

            //iTween.ScaleTo(m_thrown_cards_object, iTween.Hash("x", temp_scale_parent, "y", temp_scale_parent, "z", temp_scale_parent, "islocal", true, "easeType", "linear", "time", time_to_move));


            GetPlayerView(pos).DynamicAvatarView.PlayAnimation(MAvatarView.AvatarAnimationStateTrigger.TrickTake);
            while (timeForRot < 1f)
            {
                timeForRot = (Time.time - f_start_time) / time_to_move;

                temp_position = new Vector3(Mathf.SmoothStep(start_pos_x, target_x, timeForRot), Mathf.SmoothStep(start_pos_y, target_y, timeForRot), 0f);

                T.localPosition = temp_position;

                CardColorFade.alpha = (0.85f - timeForRot);

                obj1.localPosition = new Vector3(Mathf.SmoothStep(start_pos_x_1, end_pos_x_1, timeForRot), Mathf.SmoothStep(start_pos_y_1, end_pos_y_1, timeForRot * 2), 0f);
                obj2.localPosition = new Vector3(Mathf.SmoothStep(start_pos_x_2, end_pos_x_2, timeForRot), Mathf.SmoothStep(start_pos_y_2, end_pos_y_2, timeForRot * 2), 0f);
                obj3.localPosition = new Vector3(Mathf.SmoothStep(start_pos_x_3, end_pos_x_3, timeForRot), Mathf.SmoothStep(start_pos_y_3, end_pos_y_3, timeForRot * 2), 0f);

                obj1_rot = Mathf.SmoothStep(start_target_z_1, end_target_z_1, timeForRot * 2);
                obj2_rot = Mathf.SmoothStep(start_target_z_2, end_target_z_2, timeForRot * 2);
                obj3_rot = Mathf.SmoothStep(start_target_z_3, end_target_z_3, timeForRot * 2);

                obj1.localEulerAngles = new Vector3(0, 0, obj1_rot);
                obj2.localEulerAngles = new Vector3(0, 0, obj2_rot);
                obj3.localEulerAngles = new Vector3(0, 0, obj3_rot);

                yield return null;
            }

            //removing the cards from it - and reseting it - no need to touch the model as they were never added to it
            //for (int i = 0; i < m_thrown_cards.Count; i++)
            //  m_thrown_cards[i].GetComponent<CardView>().DeleteCard();

            foreach (Transform item in m_roundCardsObject.transform)
            {
                //item.gameObject.GetComponent<CardView> ().DeleteCard ();

                Destroy(item.gameObject);
            }

            m_thrown_cards.Clear();

            CardColorFade.alpha = 1f;
            T.localPosition = m_thrown_cards_org_position;

            exit_anim_done();
        }


        public IEnumerator DealCardsAnimation(int dealerPos, Action animEnded, CardsList player_cards)
        {
            yield return new WaitForSeconds(DEAL_INDICATION_MOVE_TIME);

            SetCardsSizes();

            bool quit = false;
            float f_start_time = Time.time;
            float time_to_move = 0.3f;   //card contraction time
            float time_to_move_single_card = time_to_move / 3;   //card contraction time

            int cards_to_deal = 7;

            Vector3 org_location = new Vector3();

            float temp_rotation_value, temp_rotation_end_value;

            Transform T = m_roundCardsObject.GetComponent<RectTransform>();

            T.localPosition = m_thrown_cards_org_position;

            float start_pos_x = T.localPosition.x;
            float start_pos_y = T.localPosition.y;

            org_location = m_cardEndPositions[dealerPos];

            start_pos_x = org_location.x;
            start_pos_y = org_location.y;

            float target_x, target_y;
            int j;

            GameObject[] tempCards = new GameObject[3];
            CanvasGroup[] CardColorFaders = new CanvasGroup[3];

            tempCards[0] = (GameObject)Instantiate(ManagerView.Instance.Card_for_deal_prefab);
            CardColorFaders[0] = tempCards[0].GetComponent<CanvasGroup>();
            tempCards[1] = (GameObject)Instantiate(ManagerView.Instance.Card_for_deal_prefab);
            CardColorFaders[1] = tempCards[1].GetComponent<CanvasGroup>();
            tempCards[2] = (GameObject)Instantiate(ManagerView.Instance.Card_for_deal_prefab);
            CardColorFaders[2] = tempCards[2].GetComponent<CanvasGroup>();


            tempCards[0].transform.SetParent(T);
            tempCards[0].GetComponent<RectTransform>().localScale = m_cardScale * CARD_SCALE_DOWN * 0.8f;
            tempCards[1].transform.SetParent(T);
            tempCards[1].GetComponent<RectTransform>().localScale = m_cardScale * CARD_SCALE_DOWN * 0.8f;
            tempCards[2].transform.SetParent(T);
            tempCards[2].GetComponent<RectTransform>().localScale = m_cardScale * CARD_SCALE_DOWN * 0.8f;

            CardColorFaders[0].alpha = 0f;
            CardColorFaders[1].alpha = 0f;
            CardColorFaders[2].alpha = 0f;

            float total_time = cards_to_deal * time_to_move;

            m_add_dummy_cards = m_player_views[3].GetComponent<PlayerView>().AddDummyCards(total_time * 0.9f, player_cards);

            StartCoroutine(m_add_dummy_cards);

            int i = dealerPos;
            j = 0;
            while (Time.time - f_start_time < total_time && !quit)
            {
                i = i % 4;

                if (i != dealerPos)
                {

                    CardGamesSoundsController.Instance.CardDealt();

                    temp_rotation_value = 180;

                    if (i % 2 == 0) //west or east player - start rotation 90
                        temp_rotation_end_value = -90f;
                    else
                        temp_rotation_end_value = 0f;

                    if (tempCards[j] != null)
                    {
                        tempCards[j].GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);
                        CardColorFaders[j].alpha = 0f;
                        org_location = m_cardEndPositions[i];

                        target_x = org_location.x;
                        target_y = org_location.y;

                        StartCoroutine(ThrowSingleCard(time_to_move, tempCards[j].GetComponent<RectTransform>(), start_pos_x, target_x, start_pos_y, target_y, temp_rotation_value, temp_rotation_end_value, CardColorFaders[j]));

                    }
                    else
                    {

                        StopCoroutine(m_add_dummy_cards);
                        quit = true;

                    }


                    yield return new WaitForSeconds(time_to_move_single_card);


                    j++;
                    j = j % 3;
                }
                i++;
            }


            yield return new WaitForSeconds(0.25f);
            // GetPlayer(3).GetComponent<PlayerView>().GetCardsList().Clear();

            yield return new WaitForSeconds(1f);


            Destroy(tempCards[0].gameObject);
            Destroy(tempCards[1].gameObject);
            Destroy(tempCards[2].gameObject);

            animEnded();
        }


        IEnumerator ThrowSingleCard(float duration, Transform rect, float start_x, float end_x, float start_y, float end_y, float start_rot, float end_rot, CanvasGroup fader)
        {
            float timeForRot = 0;
            float f_start_time = Time.time;
            Vector3 temp_position, temp_rotation;

            while (timeForRot < 1f && rect != null)
            {

                timeForRot = (Time.time - f_start_time) / duration;

                temp_position = new Vector3(Mathf.SmoothStep(start_x, end_x, timeForRot), Mathf.SmoothStep(start_y, end_y, timeForRot), 0f);

                rect.localPosition = temp_position;

                temp_rotation = new Vector3(0, 0, Mathf.SmoothStep(start_rot, end_rot, timeForRot));

                rect.localEulerAngles = temp_rotation;

                fader.alpha = (1f - timeForRot + 0.33f);

                yield return null;
            }

            if (fader != null)
                fader.alpha = 0f;
        }


        public void ShowAllPlayerNames()
        {
            foreach (var playerView in m_player_views)
            {
                playerView.ShowPlayerName();
            }
        }



        public void SetOnboardText(string str)
        {
            m_onboard_text.text = str;
        }

        public void Timeline_TableBouncePlayers()
        {
            //SoundsController.Instance.PlaySmack ();

            for (int i = 0; i < 4; i++)
            {
                //m_picture_views [i].TableHitBounce ();
            }
        }

        public void UpdatePartnersPointsTable()
        {
            SpadesActiveMatch aMatch = SpadesModelManager.Instance.GetActiveMatch();

            ISpadesMatch spadesMatch = aMatch.GetMatch();

            if (spadesMatch.Playing_mode == PlayingMode.Partners)
            {
                SpadesRoundResults round_results = aMatch.GetLastRoundResults();

                m_partners_points_table.SetActive(true);

                int rival_bags = 0;
                int your_bags = 0;

                if (round_results != null)
                {
                    your_bags = round_results.GetResults(SpadesRoundResults.Positions.North_South).Bags_total;
                    rival_bags = round_results.GetResults(SpadesRoundResults.Positions.West_East).Bags_total;
                }


                m_your_team_bags_text.text = your_bags.ToString();
                m_rival_team_bags_text.text = rival_bags.ToString();

                m_your_team_points_text.text = string.Format("{0} Pt.", aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South));
                m_rival_team_points_text.text = string.Format("{0} Pt.", aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East));
            }
            else
            {
                m_partners_points_table.SetActive(false);
            }
        }


        public bool Players_created
        {
            get
            {
                return m_players_created;
            }
            set
            {
                m_players_created = value;
            }
        }

        public GameObject Game_ui_object
        {
            get
            {
                return m_game_ui_object;
            }
        }

        public Vector3 CardSize
        {
            get
            {
                return m_cardScale;
            }
            set
            {
                m_cardScale = value;
            }
        }


        public CardView[] Spades_broken_card
        {
            get
            {
                return m_spades_broken_card;
            }
        }

        public GameObject Game_table_object
        {
            get
            {
                return m_game_table_object;
            }
        }



        public GameObject Onboard_text
        {
            get
            {
                return m_onboard_object;
            }
        }


        public GameObject Thrown_cards_object
        {
            get
            {
                return m_roundCardsObject;
            }
            set
            {
                m_roundCardsObject = value;
            }
        }

        public List<PlayerView> Player_views { get => m_player_views; set => m_player_views = value; }
    }
}
