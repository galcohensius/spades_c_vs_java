using System.Collections;
using System;
using spades.models;
using spades.controllers;
using common;
using System.Collections.Generic;
using common.controllers;
using cardGames.comm;
using cardGames.models;
using common.models;
using common.views.popups;
using cardGames.controllers;

namespace spades.views
{
    /// <summary>
    /// Manager view - manages all other views - holds all the prefabs to be duplicated in run time - and links to other game objects in the scene 
    /// </summary>
    public class ManagerView : MonoBehaviour
    {

        public static ManagerView Instance;
        public const string TABLE_PATH = "tableGraphics/v2/";

        public Color ROUND_SUMMARY_RED_COLOR = new Color(207f / 255f, 24f / 255f, 41f / 255f);
        public Color ROUND_SUMMARY_GREEN_COLOR = new Color(0, 218f / 255f, 174f / 255f);

        public Color DISABLE_BUTTON_COLOR = new Color(207f / 255f, 24f / 255f, 41f / 255f);

        [SerializeField] Sprite[] m_deck_images = null;
        //link to images of cards


        Sprite m_trophy_image = null;
        Sprite m_single_panel_image = null;
        Sprite m_arena_panel_image = null;
        Sprite m_front_BG_image = null;
        Sprite m_bg_image = null;
        Sprite m_glow_image = null;
        Sprite m_next_world = null;
        Sprite m_prev_world = null;

        Sprite m_arena_banner = null;
        Sprite m_arena_banner_glow = null;

        List<Sprite> m_collectables = new List<Sprite>();
        List<Sprite> m_dark_collectables = new List<Sprite>();

        [SerializeField] GameObject m_card_prefab = null;
        [SerializeField] GameObject m_card_for_deal_prefab = null;
        [SerializeField] GameObject m_card_for_table_prefab = null;
        [SerializeField] GameObject m_imsBanner_prefab = null;

        [SerializeField] GameObject m_table = null;


        [SerializeField] GameObject m_Game_Table_object = null;
        [SerializeField] GameObject m_Lobby_canvas_landscape = null;


        bool m_attempt_match_type = false;

        [SerializeField] GameObject m_InGameGui = null;

        [SerializeField] GameObject m_xp_trails_prefab = null;

        [SerializeField] GameObject m_slow_connection = null;
        [SerializeField] GameObject m_network_error_object = null;

        [SerializeField]
        private Serialized_KeyStringValueSprite_Dictionary m_table_graphics = null;

        [SerializeField]
        private Sprite m_default_facebook_image;

        [SerializeField] GameObject m_gameTableHolder = null;
        [SerializeField] GameObject m_voucher_prefab;

        //asset loaded table
        Sprite loaded_table = null;

        //drag helpers
        float m_scaler_x;
        float m_scaler_y;
        float m_scaler_width;
        float m_scaler_height;
        float m_actual_width;
        float m_actual_height;


        bool aspect43 = false;
        bool aspect43_correction_done = false;



        bool m_showErrorPopup = true;

        bool m_show_avatar_selection_in_session = false;

        int m_networkRetriesNumber;


        private void Awake()
        {
            Instance = this;
        }



        public void Start()
        {
            CardGamesCommManager.Instance.Rest_Client.SlowConnectionCallback += ShowSlowConnection;
            CardGamesCommManager.Instance.Rest_Client.NetworkErrorCallback += NetworkErrorHandler;
            CardGamesCommManager.Instance.Rest_Client.ConnectionTimeoutCallback += ConnectionTimeoutHandler;
            CardGamesCommManager.Instance.Rest_Client.DuplicateLoginCallback += DuplicateLoginHandler;
            CardGamesCommManager.Instance.Rest_Client.MaintainanceProcedureCallback += ShowMaintenancePopup;
            CardGamesCommManager.Instance.Rest_Client.BannedUserCallback += ShowBannedPopup;
        }

       

        private void ShowMaintenancePopup()
        {
            PopupManager.Instance.ShowMessagePopup("System Message\nPlease restart the game to continue playing", common.controllers.PopupManager.AddMode.ShowAndRemove, true, OnMaintenancePopupClose, "RESTART", 32000);
        }

        private void OnMaintenancePopupClose()
        {
            BackgroundController.Instance.ForceSessionTimeout();
        }

        void DuplicateLoginHandler()
        {
            //ShowNetworkError (LobbyController.Instance.Restart, "You are already connected on another device.\nClick OK to reconnect.","OK");

            LoggerController.Instance.Log("DuplicateLogin");

            ShowNetworkError(DuplicateLoginResolve, "Another device is connected using your account.\nClick to reconnect.", "RECONNECT");
        }

        private void DuplicateLoginResolve()
        {
            if (m_Game_Table_object.activeSelf)
            {
                RoundController.Instance.LeaveTable(false, true);
                SpadesPopupManager.Instance.ShowWaitingIndication(false);

            }
            BackgroundController.Instance.ForceSessionTimeout();
        }

        public void ShowSlowConnection(bool show)
        {
            SlowConnectionView slowConnectionView = m_slow_connection.GetComponent<SlowConnectionView>();

            if (show)
            {
                slowConnectionView.Show();
                LoggerController.Instance.Log("Slow Connection");
            }

            else
                slowConnectionView.Hide();

        }

        private void ShowBannedPopup()
        {
            SpadesPopupManager.Instance.ShowBanUserPopup(PopupManager.AddMode.ShowAndRemove, CommonUser.AccountStatus.BanFromLogin);
        }

        public Coroutine DelayAction(float delay, Action action)
        {
            return StartCoroutine(PerformDelayAction(delay, action));
        }

        IEnumerator PerformDelayAction(float delay, Action action)
        {
            yield return new WaitForSecondsRealtime(delay);

            if (action != null)
                action();
        }

        public void NetworkErrorHandler(string command, long httpCode=200, string httpError=null)
        {
            if (m_networkRetriesNumber < 20)
            {
                m_networkRetriesNumber++;

                string message = "There seems to be a problem reaching our servers right now.\nPlease check your internet connection and try again.";

                ShowNetworkError(CardGamesCommManager.Instance.Rest_Client.SendRequestAgain, message, "TRY AGAIN");

            }
            else
            {
                m_networkRetriesNumber = 0;
                PopupManager.Instance.ShowMessagePopup("There seems to be a problem reaching our servers right now.\nPlease check your internet connection.",
                    close_delegate: () =>
                    {
                        BackgroundController.Instance.ForceSessionTimeout();
                    });

            }

            TrackingManager.Instance.LogFirebaseEvent("NetworkError", new Dictionary<string, string>
            {
                ["command"]=command,
                ["httpCode"]=httpCode.ToString(),
                ["httpError"]=httpError
            });
        }

        private void ConnectionTimeoutHandler(string command)
        {
            NetworkErrorHandler(command, 408 /*RequestTimeout*/, "Request timeout after " + RestClient.CONN_TIMEOUT_SEC + " sec");
        }

        private void ShowNetworkError(Action button_action, string message, string button_caption)
        {
            //LoggerController.Instance.SendLogDataToServer("NetworkError", "Show Network Error: " +message);

            m_network_error_object.SetActive(true);
            m_network_error_object.GetComponent<NetworkErrorView>().Message = message;
            m_network_error_object.GetComponent<NetworkErrorView>().Caption_button.text = button_caption;
            m_network_error_object.GetComponent<NetworkErrorView>().Button_action = button_action;

            SpadesPopupManager.Instance.ShowWaitingIndication(false);
        }

        public void ClearAllWorldAssets()
        {
            m_front_BG_image = null;
            m_trophy_image = null;
            m_arena_panel_image = null;
            m_single_panel_image = null;
            m_bg_image = null;
            m_collectables.Clear();
            m_dark_collectables.Clear();
        }

        public void LoadTableSprite(Match match, Action<Sprite> OnTableLoaded)
        {

            string tableKey = GetTableSpriteKey(match);

            // Try the local cache first
            Sprite result = null;
            m_table_graphics.TryGetValue(tableKey, out result);
            if (result != null)
            {
                ManagerView.Instance.Loaded_table = result;
                OnTableLoaded(result);
                return;
            }

            string cachePostfix = LocalDataController.Instance.GetSetting($"{tableKey}_cachePostfix", "");

            // Use the RemoteAssetManager
            string table_image_path = $"{TABLE_PATH}{tableKey}{cachePostfix}.jpg";

            LoadingAssetPopup loadingAssetPopup = null;
            if (!RemoteAssetManager.Instance.IsImageCached(table_image_path))
            {
                loadingAssetPopup = (CardGamesPopupManager.Instance as SpadesPopupManager).ShowLoadingAssetPopup(table_image_path, () =>
                 {
                     GetTableImage(table_image_path, OnTableLoaded, loadingAssetPopup.UpdateProgress);
                 });
            }
            else
            {
                GetTableImage(table_image_path, OnTableLoaded, null);
            }
        }

        private void GetTableImage(string table_image_path, Action<Sprite> OnTableLoaded, Action<float> loadProgress)
        {
            RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, table_image_path,
                 null, (sprite, rawTexture, userData) =>
                 {
                     PopupManager.Instance.HidePopup();
                     if (sprite != null)
                     {
                         ManagerView.Instance.Loaded_table = sprite;
                         OnTableLoaded(sprite);
                     }
                     else
                     {
                         PopupManager.Instance.ShowMessagePopup("Cannot open table, please try again later.", PopupManager.AddMode.ShowAndRemove);
                         OnTableLoaded(null);
                     }


                 }, loadProgress);
        }



        /// <summary>
        /// Returns the correct table sprite key for a specific lobby item according to world and playing variation
        /// or the graphics id if provided
        /// This method returns the provided tables only, or null if not found and should be fetches from a remote location
        /// </summary>
        private string GetTableSpriteKey(Match match)
        {
            if (!string.IsNullOrEmpty(match.Graphics_path))
                return match.Graphics_path;

            ISpadesMatch spadesMatch = match as ISpadesMatch;
            string tableKey = "";
            switch (spadesMatch.Variant)
            {
                case PlayingVariant.Jokers:
                    tableKey = "Jokers";
                    if (spadesMatch.SubVariant == PlayingSubVariant.Coins4Points)
                        tableKey = "Coins4Points_Joker";
                    break;
                case PlayingVariant.Mirror:
                    tableKey = "Mirror";
                    break;
                case PlayingVariant.Suicide:
                    tableKey = "Suicide";
                    break;
                case PlayingVariant.Whiz:
                    tableKey = "Whiz";
                    break;
                case PlayingVariant.CallBreak:
                    tableKey = "CallBreak";
                    break;
                default:
                    switch (spadesMatch.SubVariant)
                    {
                        case PlayingSubVariant.FastLane:
                            tableKey = "FastLane";
                            break;
                        case PlayingSubVariant.GoBig:
                            tableKey = "GoBig";
                            break;
                        case PlayingSubVariant.SingleRound:
                            tableKey = "SingleRound";
                            break;
                        case PlayingSubVariant.Coins4Points:
                            tableKey = "Coins4Points_v2";
                            break;
                        case PlayingSubVariant.NoNil:
                            tableKey = "NoNil";
                            break;
                        case PlayingSubVariant.HiLo:
                            tableKey = "HiLo";
                            break;

                        default:
                            {
                                if (spadesMatch is SpadesSingleMatch)
                                    tableKey = "Room-" + ((SpadesSingleMatch)match).World_id;
                                else if (spadesMatch is SpadesArenaMatch)
                                    tableKey = "Room-" + ((SpadesArenaMatch)match).GetArena().World.GetID();
                                break;
                            }
                    }

                    break;
            }

            return tableKey;

        }

        [Obsolete("Use ViewUtils.EnableDisableButtonText instead")]
        public void EnableDisableButton(Button button)
        {
            TMP_Text button_text = button.transform.FindDeepChildTmp();

            if (button_text != null)
            {
                Color curr_color = button_text.color;

                if (button.interactable)
                    button_text.color = new Color(curr_color.r, curr_color.g, curr_color.b, 1f);
                else
                    button_text.color = new Color(curr_color.r, curr_color.g, curr_color.b, 0.5f);
            }

        }

        public Sprite GetBGImage()
        {
            return m_bg_image;
        }

        public Sprite GetGlowImage()
        {
            return m_glow_image;
        }

        public void SetBGImage(Sprite image)
        {
            m_bg_image = image;
        }

        public Sprite GetFrontBGImage()
        {
            return m_front_BG_image;
        }

        public void SetFrontBGImage(Sprite front_image)
        {
            m_front_BG_image = front_image;
        }

        public Sprite GetThropyImage()
        {
            return m_trophy_image;
        }

        public void SetTrophyImage(Sprite trophy)
        {
            m_trophy_image = trophy;
        }

        public Sprite GetPanelImage(bool arena)
        {
            if (arena)
                return m_arena_panel_image;
            else
                return m_single_panel_image;
        }

        public void SetPanels(Sprite arena, Sprite single)
        {
            m_arena_panel_image = arena;
            m_single_panel_image = single;
        }

        public void SetArenaTitleAndGlow(Sprite banner, Sprite glow)
        {
            m_arena_banner = banner;
            m_arena_banner_glow = glow;
        }

        public void SetNextPrevWorldsImages(Sprite next, Sprite prev)
        {
            m_next_world = next;
            m_prev_world = prev;
        }


        public Sprite GetNextPrevWorldImage(bool next)
        {
            if (next)
                return m_next_world;
            else
                return m_prev_world;
        }

        public Sprite GetDeckImage(int index)
        {
            return m_deck_images[index];
        }

        public void SetCollectibleImage(Sprite image)
        {
            m_collectables.Add(image);
        }

        public void SetDarkCollectibleImage(Sprite image)
        {
            m_dark_collectables.Add(image);
        }

        public Sprite GetCollectableImage(int index)
        {
            return m_collectables[index];
        }


        public Sprite GetDarkCollectableImage(int index)
        {
            return m_dark_collectables[index];
        }

        public void SetMatchAttemptType(bool value)
        {
            m_attempt_match_type = value;
        }

        public bool GetMatchAttemptType()
        {
            return m_attempt_match_type;
        }

        public void ReadAndSetScalerInfo()
        {
            CanvasScaler scaler = m_Lobby_canvas_landscape.GetComponent<CanvasScaler>();

            // Fix the scaler for wide screens
            // In WebGL, do not fix since might not work on full screen mode
#if !UNITY_WEBGL
            if (Camera.main.aspect > 16 / 9f)
            {
                scaler.matchWidthOrHeight = 1f;
            }
#endif

            if (Camera.main.aspect <= 4 / 3f + 0.02f && Camera.main.aspect >= 1) // The second condition is since I suspect we sometimes get 0 here..
            {
                aspect43 = true;
                m_gameTableHolder.transform.localPosition = new Vector2(0, 130);
            }

            m_scaler_x = scaler.referenceResolution.x;
            m_scaler_y = scaler.referenceResolution.y;

            m_scaler_width = Screen.width;
            m_scaler_height = Screen.height;

            m_actual_width = m_Lobby_canvas_landscape.GetComponent<RectTransform>().rect.width;
            m_actual_height = m_Lobby_canvas_landscape.GetComponent<RectTransform>().rect.height;

            // For wide screens, we change the canvas scaler mode in runtime. For this reason, 
            // the actual height and width must be adjusted            
            if (scaler.matchWidthOrHeight == 1)
            {
                m_actual_width = 1080 * m_actual_width / m_actual_height;
                m_actual_height = 1080;
            }

            // Set the pixelDragThreshold to control the click sensitivity in scroll views
            UpdateDragThreshold();

        }

        private void UpdateDragThreshold()
        {
            int defaultValue = EventSystem.current.pixelDragThreshold;
            EventSystem.current.pixelDragThreshold =
                    Mathf.Max(
                         defaultValue,
                         (int)(defaultValue * Screen.dpi / 80f));
        }

        public GameObject InGameGui
        {
            get
            {
                return m_InGameGui;
            }
        }

        public GameObject Table
        {
            get
            {
                return m_table;
            }
        }



        public float Scaler_x
        {
            get
            {
                return m_scaler_x;
            }
        }


        public float Scaler_y
        {
            get
            {
                return m_scaler_y;
            }
        }


        public float Scaler_width
        {
            get
            {
                return m_scaler_width;
            }
        }

        public float Scaler_height
        {
            get
            {
                return m_scaler_height;
            }
        }

        public float Actual_width
        {
            get
            {
                return m_actual_width;
            }
        }

        public float Actual_height
        {
            get
            {
                return m_actual_height;
            }
        }

        public GameObject Lobby_canvas_landscape
        {
            get
            {
                return m_Lobby_canvas_landscape;
            }
        }

        public GameObject Card_prefab
        {
            get
            {
                return m_card_prefab;
            }
        }


        public GameObject Game_Table
        {
            get
            {
                return m_Game_Table_object;
            }
        }

        public GameObject Card_for_deal_prefab
        {
            get
            {
                return m_card_for_deal_prefab;
            }
        }

        public GameObject Card_for_table_prefab
        {
            get
            {
                return m_card_for_table_prefab;
            }
        }



        void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        void OnDisable()
        {
            // Remove callback when object goes out of scope
            Application.logMessageReceived -= HandleLog;
        }

        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            try
            {

                if (m_showErrorPopup && (type == LogType.Error || type == LogType.Exception))
                {
                    if (logString.ToLower().Contains("bad request") ||
                        logString.ToLower().Contains("download failed") ||
                        logString.ToLower().Contains("same files is already loaded"))
                        return;

                    // Write to logger
                    LoggerController.Instance.Log("ERROR: " + logString);
                    LoggerController.Instance.Log("STACK TRACE: " + stackTrace);

#if DEV_MODE || QA_MODE
                    SpadesPopupManager.Instance.ShowErrorMessagePopup (logString, stackTrace);
#endif
                    if (RoundController.Instance.Is_game_active)
                    {
                        // Do not send if the problem is related to the dynamic contest list
                        if (!stackTrace.Contains("ParseDynamicDataList"))
                            LoggerController.Instance.SendLogDataToServer("GameException", "Game exception: " + logString);
                    }
                }

            }
            catch
            {
                // Do nothing
            }
        }

        public Sprite Glow_image
        {
            get
            {
                return m_glow_image;
            }
            set
            {
                m_glow_image = value;
            }
        }

        public GameObject Xp_trails_prefab
        {
            get
            {
                return m_xp_trails_prefab;
            }
        }


        public Sprite Arena_banner
        {
            get
            {
                return m_arena_banner;
            }
        }

        public Sprite Arena_banner_glow
        {
            get
            {
                return m_arena_banner_glow;
            }
        }

        public bool ShowErrorPopup
        {
            get
            {
                return this.m_showErrorPopup;
            }
            set
            {
                m_showErrorPopup = value;
            }
        }


        public bool Aspect43
        {
            get
            {
                return aspect43;
            }
        }

        public bool Aspect43_correction_done
        {
            get
            {
                return aspect43_correction_done;
            }
            set
            {
                aspect43_correction_done = value;
            }
        }



        public bool Show_avatar_selection_in_session
        {
            get
            {
                return m_show_avatar_selection_in_session;
            }

            set
            {
                m_show_avatar_selection_in_session = value;
            }
        }

        public Sprite Loaded_table
        {
            get
            {
                return loaded_table;
            }

            set
            {
                loaded_table = value;
            }
        }

        public Sprite Default_facebook_image
        {
            get
            {
                return m_default_facebook_image;
            }
        }

        public GameObject Voucher_prefab { get => m_voucher_prefab; set => m_voucher_prefab = value; }
        public GameObject ImsBanner_prefab { get => m_imsBanner_prefab; set => m_imsBanner_prefab = value; }
    }
}
