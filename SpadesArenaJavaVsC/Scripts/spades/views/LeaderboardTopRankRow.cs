﻿using common.utils;

namespace spades.views
{
	public class LeaderboardTopRankRow : MonoBehaviour {
		
		[SerializeField] TMP_Text m_coins_text=null;
        [SerializeField] TMP_Text m_place_text= null;
        [SerializeField] bool isPedestal=false;

		public int Coins
		{
			set{
				m_coins_text.text = (isPedestal?"<sprite=1>":"") + FormatUtils.FormatBalance (value);
			}
		}

		public void setPlace(string val)
		{
			if (m_place_text!=null)
				m_place_text.text = val;

		}

	}
}