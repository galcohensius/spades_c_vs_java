namespace spades.views
{
    public class GemUnlockAnimTapHandler : MonoBehaviour
    {
        [SerializeField] TMP_Text m_text = null;
       
        public void ShowTapText(int id)
        {
            Animator animator = GetComponent<Animator>();

            if(animator != null && animator.GetBool("BasePart") && id==2)
                iTween.ValueTo(m_text.gameObject, iTween.Hash("from", 0, "to", 1, "time", .4f, "onupdate", "UpdateAlphaOn", "onupdatetarget", this.gameObject));

            if(animator != null && !animator.GetBool("BasePart") && id == 1)
                iTween.ValueTo(m_text.gameObject, iTween.Hash("from", 0, "to", 1, "time", .4f, "onupdate", "UpdateAlphaOn", "onupdatetarget", this.gameObject));
        }

        public void UpdateAlphaOn(float value)
        {
            m_text.gameObject.GetComponent<CanvasGroup>().alpha = value;
        }
    }
}
