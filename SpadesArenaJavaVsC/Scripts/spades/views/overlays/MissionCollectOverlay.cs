﻿using spades.controllers;
using common.controllers;
using common.utils;
using common.models;
using System;
using cardGames.models;
using cardGames.controllers;
using cardGames.comm;
using cardGames.views.overlays;
using cardGames.views;

namespace spades.views.overlays
{

    public class MissionCollectOverlay : OverlayBase
    {
        //flight related

        [SerializeField] GameObject m_flight_source = null;

        Animator m_anim = null;
        int m_new_balance = 0;

        bool m_total_claims_done = false;

        [SerializeField] TMP_Text m_prize_text = null;

        public void Start()
        {
            m_anim = GetComponent<Animator>();
        }

        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);

            if (MissionController.Instance.To_claim_mission.Count > 0)
            {
                ClaimMission(MissionController.Instance.To_claim_mission[0]);
                CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.mission_complete);
            }
            else
            {
                PopupManager.Instance.ShowMessagePopup("This bonus can no longer be collected.");
                SpadesOverlayManager.Instance.HideOverlay(this);
            }
        }


        private void ClaimMission(SpadesMissionController.ClaimMissionData mission_to_claim)
        {
            m_prize_text.text = "<sprite=3>" + FormatUtils.FormatPrice(Convert.ToInt32(mission_to_claim.Mission_prize));
            CardGamesCommManager.Instance.ClaimMissionReward(mission_to_claim.Mission_id, BonusClaimed);
        }


        private void BonusClaimed(bool success, int new_balance, BonusAwarded bonusAwarded)
        {
            //m_total_claims_done = true;

            if (success)
            {
                m_anim.SetTrigger("play_anim");
                PlayCoinsFlightForHourlyBonus(bonusAwarded.GoodsList.CoinsValue);
                m_new_balance = new_balance;

                MissionController.Instance.To_claim_mission.RemoveAt(0);


            }
            else
            {
                PopupManager.Instance.ShowMessagePopup("This bonus can no longer be collected.");
                m_manager.HideOverlay(this);
            }
        }

        public void BT_ClickToContinueClicked()
        {


            if (MissionController.Instance.To_claim_mission.Count > 0)
            {
                //rewind the animation to start
                m_anim.SetTrigger("continue_clicked");
                //keep claiming
                ClaimMission(MissionController.Instance.To_claim_mission[0]);

            }
            else
            {
                //done claiming all missions
                //m_total_claims_done = true;
                LobbyController.Instance.Lobby_view.HandleLobbyChallengeButtonAndClaimIndication();
                SpadesOverlayManager.Instance.HideOverlay(this);
            }
        }

        public void PlayCoinsFlightForHourlyBonus(int bonus_amount)
        {
            CometOverlay comet = SpadesOverlayManager.Instance.ShowCometOverlay();


            int num_coins_to_show = Mathf.Min(5, bonus_amount);

            comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_flight_source, CardGamesPopupManager.Instance.Lobby_flight_target
                , 0f, 0.15f, 1.5f, CardGamesPopupManager.Instance.Lobby_coins_balance, bonus_amount, CardGamesPopupManager.Instance.Lobby_coins_animator, 3, 1.0f, 0.5f
                , TrailCompleted, new Vector2(0f, 0f), new Vector2(0.8f, 0.4f));
            //StartCoroutine (HideCometOverlay (comet));
        }

        private void TrailCompleted()
        {
            ModelManager.Instance.GetUser().SetCoins(m_new_balance);

        }

    }



}
