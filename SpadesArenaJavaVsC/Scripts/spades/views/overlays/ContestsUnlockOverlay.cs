﻿using cardGames.views;
using spades.controllers;


namespace spades.views.overlays
{
    public class ContestsUnlockOverlay : OverlayBase
    {
        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);


        }


        public void OnTapToContinue()
        {
            SpadesOverlayManager.Instance.HideOverlay(this);
        }
    }
}