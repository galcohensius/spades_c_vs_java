﻿using spades.models;
using spades.controllers;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

    public class ArenaUnlockOverlay : OverlayBase
    {

        ArenaItemView m_arena_item_view;

        [SerializeField] Canvas m_image_canvas = null;
        [SerializeField] GameObject m_gem_item_holder = null;


        [SerializeField] GameObject m_arena = null;

        [SerializeField] TMP_Text m_title_text = null;


        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);

            CardGamesSoundsController.Instance.TableUnlocked();
        }

        public void SetData(SpadesArena arena)
        {
            SpadesWorld world = (SpadesWorld)arena.World;

            Sprite un_locked = LobbyController.Instance.Lobby_view.Arena_panels [world.Index];
            Sprite locked = LobbyController.Instance.Lobby_view.Arena_panels[world.Index];

            GameObject gem = LobbyController.Instance.Lobby_view.Gems_objects[world.Index];
            Sprite gem_sprite = LobbyController.Instance.Lobby_view.Gems_lobby_sprite[world.Index];

            m_arena_item_view = m_arena.GetComponent<ArenaItemView>();
            m_arena_item_view.SetFullArena(arena, un_locked, gem, gem_sprite,true);

            m_gem_item_holder.SetActive(false);
            m_image_canvas.sortingOrder = 104;

            m_title_text.text = "ARENA";

            m_arena.SetActive(true);

        }


        public void ButtonClicked()
        {
            if (this != null)
                SpadesOverlayManager.Instance.HideOverlay(this);
        }

       
    }
}
