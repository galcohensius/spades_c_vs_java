﻿using spades.models;
using spades.controllers;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

    public class TableUnlockOverlay : OverlayBase
    {

        SingleItemView m_single_item_view;


        [SerializeField] GameObject m_single = null;


        [SerializeField] TMP_Text m_title_text = null;


        public void SetData(SpadesSingleMatch single_match)
        {
            CardGamesSoundsController.Instance.TableUnlocked();


            SpadesWorld world = (SpadesWorld)single_match.World;


            Sprite locked = LobbyController.Instance.Lobby_view.Locked_single[world.Index];
            Sprite un_locked = LobbyController.Instance.Lobby_view.Single_panels[world.Index];

            m_single_item_view = m_single.GetComponent<SingleItemView>();

            //Sprite locked = SpadesModelManager

            m_single_item_view.SetInitialData(single_match, locked, un_locked);

            //m_title_text.gameObject.GetComponent<WarpText> ().curveScale = 8f;
            m_title_text.text = "TABLE";



            m_single.SetActive(true);


        }


        public void ButtonClicked()
        {
            if (this != null)
                SpadesOverlayManager.Instance.HideOverlay(this);
        }

       
    }
}
