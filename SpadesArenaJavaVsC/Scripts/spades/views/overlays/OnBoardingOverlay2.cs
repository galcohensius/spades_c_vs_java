﻿using spades.controllers;
using spades.models;
using cardGames.views;

namespace spades.views.overlays
{

    public class OnBoardingOverlay2 : OverlayBase
    {
        [SerializeField] SingleItemView m_item = null;

        SpadesSingleMatch m_single_match = null;

        public void SetLobbyItemData(SpadesSingleMatch single_match,WorldItemView worldItemView)
        {
            m_single_match = single_match;
            m_item.SetInitialData(single_match,
                                  LobbyController.Instance.Lobby_view.Locked_single[0],
                                  LobbyController.Instance.Lobby_view.Single_panels[0],
                                  worldItemView);
            if (ManagerView.Instance.Aspect43)
            {

                // Find a fix for 4:3 sizing, where the lobby item is scaled on Y axis, and the onboarding item
                // needs to be scaled as well
                m_item.Panel_BG.GetComponent<RectTransform>().localScale = new Vector3(1f, 1.15f, 1f);

                AddDeltaY(-35, m_item.Mission_indication);
                AddDeltaY(10, m_item.Buy_in_object);
                AddDeltaY(-10, m_item.Points_text.gameObject);
                AddDeltaY(-25, m_item.Bags_object);
                AddDeltaY(20, m_item.Playing_mode_text.gameObject);

            }

        }


        private void AddDeltaY(float delta, GameObject go)
        {
            RectTransform temp_rt = go.GetComponent<RectTransform>();
            go.GetComponent<RectTransform>().localPosition = new Vector3(temp_rt.localPosition.x, temp_rt.localPosition.y + delta, temp_rt.localPosition.z);
        }

        public void SingleItemClick() {
            SingleMatchController.Instance.StartSingleMatch(m_single_match, null, null);
        }

    }
}