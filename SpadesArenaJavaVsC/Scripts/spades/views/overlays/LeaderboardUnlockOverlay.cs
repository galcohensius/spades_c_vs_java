using spades.controllers;
using cardGames.views;

namespace spades.views.overlays
{
	public class LeaderboardUnlockOverlay : OverlayBase
	{

        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);

           
        }


        public void OnTapToContinue()
		{
			SpadesOverlayManager.Instance.HideOverlay (this);
		}
	}
}
