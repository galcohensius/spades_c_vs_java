using System.Collections;
using spades.models;
using spades.controllers;
using System;
using common.views;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

	public class WinArenaStage2ndPlaceOverlay : OverlayBase
	{

		[SerializeField]WinLoseView m_win_lose_view = null;
		[SerializeField]Image m_table_image = null;

		[SerializeField] BalanceCoinTextbox m_user_coins_text = null;

		bool m_ff_animation = false;

        [SerializeField]
        private MAvatarView m_soloAvatarView;

        private MAvatarView m_currentFallingAvatar = null;
        private MAvatarView.AvatarAnimationStateTrigger m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropLeftJump;

		private MAvatarModel m_userAvatarModel;

		public override void Show (OverlaysManager manager, OverlayClosedDelegate close_delegate)
		{
			base.Show (manager, close_delegate);
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);
			CardGamesSoundsController.Instance.GameWin ();

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            ISpadesMatch match = active_match.GetMatch();

            StartCoroutine(DelayAvatarDrop(1f));


            m_table_image.sprite = ManagerView.Instance.Loaded_table;
        }

        private IEnumerator DelayAvatarDrop(float wantedDelayTime)
        {
            yield return new WaitForSeconds(wantedDelayTime);

            m_currentFallingAvatar = m_soloAvatarView;

			m_soloAvatarView.gameObject.SetActive(true);
			m_soloAvatarView.SetAvatarModel(m_userAvatarModel);

		}

        private void AnimateTargetAvatar(GameObject targetAvatar)
        {
           //targetAvatar.GetComponent<MAvatarView>().PlayAnimation(m_currentAnimationTrigger);
        }

        public void SetWinScreenData(int gap, int coins_payout, int xpPayout, Action LobbyClicked, Action NewGameClicked, Action AnimationDone)
        {
          
            m_user_coins_text = LobbyTopController.Instance.Coins_text;

			m_win_lose_view.Is_arena = true;
            m_win_lose_view.SetScreenData(true, false, gap, coins_payout,0, xpPayout, NewGameClicked, "Try Again",null, null, "SECOND PLACE", LobbyClicked);

			// Hide avatars and set model
			MAvatarModel model = ModelManager.Instance.GetUser().GetMAvatar();
			m_soloAvatarView.gameObject.SetActive(false);
			m_userAvatarModel = model;
		}

		private void TL_PlayTrickAnim()
		{
			//StartCoroutine (PlayBobbleAnims ());
		}

		public IEnumerator PlayBobbleAnims()
		{
			//m_player1_avatar.PlayTrickTakingAnim ();

			yield return new WaitForSecondsRealtime (0.5f);

			//if(m_player2_object.activeSelf)
				//m_player2_avatar.PlayTrickTakingAnim ();
		}

		/// <summary>
		/// CALLED FROM TIMELINE
		/// </summary>
		private void StartProgressAnimation ()
		{
			m_win_lose_view.StartProgressAnimation ();
		}

		/// <summary>
		/// CALLED FROM TIMELINE
		/// </summary>
		private void UpdateCoinsOverlay ()
		{
			m_win_lose_view.UpdateCoins (m_user_coins_text, PostCoinsFlight, m_ff_animation);
		}

		private void PostCoinsFlight ()
		{

		}



		public bool FF_animation {
			get {
				return m_ff_animation;
			}

		}
	}
}