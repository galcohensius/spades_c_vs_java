using System.Collections;
using spades.models;
using spades.controllers;
using System;
using common.views;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

    public class WinArenaStageOverlay : OverlayBase
    {

        [SerializeField] WinLoseView m_win_lose_view = null;
        [SerializeField] Image m_table_image = null;

        [SerializeField] BalanceCoinTextbox m_user_coins_text = null;

        [SerializeField]
        private MAvatarView m_soloAvatarView;

        [SerializeField]
        private MAvatarView m_partnerAvatarView_1;

        [SerializeField]
        private MAvatarView m_partnerAvatarView_2;

        private MAvatarView m_currentFallingAvatar = null;
        private MAvatarView.AvatarAnimationStateTrigger m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropLeftJump;
        private int m_fallingAvatarCount = 0;

        bool m_ff_animation = false;

        private MAvatarModel m_userAvatarModel;
        private MAvatarModel m_partnerAvatarModel;

        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);
            GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            CardGamesSoundsController.Instance.GameWin();


            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            ISpadesMatch match = active_match.GetMatch();

            if (match.Playing_mode == PlayingMode.Partners)
            {
                StartCoroutine(AnimatePartners());
            }
            else
            {
                StartCoroutine(DelayAvatarDrop(1f));
            }

            m_table_image.sprite = ManagerView.Instance.Loaded_table;
        }

        private IEnumerator AnimatePartners()
        {
            yield return new WaitForSeconds(1f);

            m_partnerAvatarView_1.gameObject.SetActive(true);
            m_partnerAvatarView_2.gameObject.SetActive(true);

            m_partnerAvatarView_1.SetAvatarModel(m_userAvatarModel);
            m_partnerAvatarView_2.SetAvatarModel(m_partnerAvatarModel);

            m_currentFallingAvatar = m_partnerAvatarView_1;
            //m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropLeftJump;
            //AnimateTargetAvatar(m_partnerAvatarView_1.gameObject);

            yield return new WaitForSeconds(1f);

            m_currentFallingAvatar = m_partnerAvatarView_2;
            //m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropRightJump;
            //AnimateTargetAvatar(m_partnerAvatarView_2.gameObject);
        }

        private IEnumerator DelayAvatarDrop(float wantedDelayTime)
        {
            yield return new WaitForSeconds(wantedDelayTime);

            m_currentFallingAvatar = m_soloAvatarView;

            m_soloAvatarView.gameObject.SetActive(true);
            m_soloAvatarView.SetAvatarModel(m_userAvatarModel);

            //m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.Drop;
            //AnimateTargetAvatar(m_soloAvatarView.gameObject);
        }

        private void AnimateTargetAvatar(GameObject targetAvatar)
        {
            //targetAvatar.GetComponent<MAvatarView>().PlayAnimation(m_currentAnimationTrigger);

            //m_table_image.sprite = LobbyController.Instance.Lobby_view.GetTableSprite(ModelManager.Instance.GetActiveMatch().GetMatch());

            m_table_image.sprite = ManagerView.Instance.Loaded_table;
        }


        public void SetWinScreenData(int coins_payout, int xpPayout, Action LobbyClicked, Action NewGameClicked, Action AnimationDone)
        {
            m_user_coins_text = LobbyTopController.Instance.Coins_text;

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            ISpadesMatch match = active_match.GetMatch();

            m_win_lose_view.Is_arena = true;
            int gap = 0;
            m_win_lose_view.SetScreenData(true, false, gap, coins_payout, 0, xpPayout, NewGameClicked, "Next Stage", null, null, "YOU WIN", LobbyClicked);

            // Hide avatars and set model
            MAvatarModel model = ModelManager.Instance.GetUser().GetMAvatar();
            m_partnerAvatarView_1.gameObject.SetActive(false);
            m_partnerAvatarView_2.gameObject.SetActive(false);
            m_soloAvatarView.gameObject.SetActive(false);

            m_userAvatarModel = model;
            m_partnerAvatarModel = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(1).GetMAvatarModel();


        }

        private void TL_PlayHappyAnim()
        {
            //StartCoroutine (PlayBobbleAnims ());
        }

        public IEnumerator PlayBobbleAnims()
        {
            //m_player1_avatar.PlayChatAnimation (AvatarPictureView.ChatType.Happy);

            yield return new WaitForSecondsRealtime(1f);

            //if(m_player2_object.activeSelf)
            //m_player2_avatar.PlayChatAnimation (AvatarPictureView.ChatType.Happy);
        }

        /// <summary>
        /// CALLED FROM TIMELINE
        /// </summary>
        private void StartProgressAnimation()
        {
            m_win_lose_view.StartProgressAnimation();
        }

        /// <summary>
        /// CALLED FROM TIMELINE
        /// </summary>
        private void UpdateCoinsOverlay()
        {
            m_win_lose_view.UpdateCoins(m_user_coins_text, PostCoinsFlight, m_ff_animation);
        }

        private void PostCoinsFlight()
        {

        }




        public bool FF_animation
        {
            get
            {
                return m_ff_animation;
            }

        }
    }
}