using spades.controllers;
using common.utils;
using System;
using cardGames.views;

namespace spades.views.overlays
{
    public class NewArenaWinOverlay : OverlayBase
    {
        [SerializeField] TMP_Text m_text = null;
        private Action m_onWinCountUpdate;

        public void SetData(int price,Action OnWinCountUpdate)
        {
            m_onWinCountUpdate = OnWinCountUpdate;
            m_text.text = "<sprite=1>" + FormatUtils.FormatBalance(price);

        }

        public void OnAnimationFineshed() //called from animation
        {
            SpadesOverlayManager.Instance.HideOverlay(this);
        }

        public void OnAnimationArenaWinCountUpdate()
        {
            if (m_onWinCountUpdate != null)
                m_onWinCountUpdate();
        }

      
    }


}
