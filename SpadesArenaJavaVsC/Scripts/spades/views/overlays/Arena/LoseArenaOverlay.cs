using cardGames.models;
using cardGames.views;
using common.views;
using spades.controllers;
using System;

namespace spades.views

{

    public class LoseArenaOverlay : OverlayBase {
		
		[SerializeField] WinLoseView		m_win_lose_view=null;
		[SerializeField]Image				m_table_image=null;

		BalanceCoinTextbox						m_user_coins_text=null;


		bool 								m_button_clicked=false;
		bool 								m_ff_animation=false;

		public override void Show (OverlaysManager manager, OverlayClosedDelegate close_delegate)
		{
			base.Show (manager, close_delegate);
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);

		}

		public void StartProgressAnimation()
		{
			//m_progress_bar_view.StartProgressAnimation ();
		}


		public void SetData(int gap, int stage,string name,int coins, int xp, Action LobbyClicked, Action NewGameClicked, Action AnimationDone)
		{
            m_table_image.sprite = ManagerView.Instance.Loaded_table;

            User user = ModelManager.Instance.GetUser ();

			m_user_coins_text = LobbyTopController.Instance.Coins_text;

			m_user_coins_text.Amount = user.GetCoins ();

            m_win_lose_view.Is_arena = true;
			m_win_lose_view.SetScreenData (true, false, gap, coins, 0,xp,null,null,null,null,"MATCH OVER", LobbyClicked);

		}

		public void UpdateCoinsDiamonds()
		{
			m_win_lose_view.UpdateCoins (m_user_coins_text,null,m_ff_animation);
		}



		public void LobbyClickedLose()
		{
			
			if (!m_button_clicked)
			{
				m_button_clicked = true;
				SpadesArenaController.Instance.LobbyClicked ();

			}

		}


		public override void OnBackButtonClicked ()
		{
			LobbyClickedLose ();
		}

		
		

	}
}