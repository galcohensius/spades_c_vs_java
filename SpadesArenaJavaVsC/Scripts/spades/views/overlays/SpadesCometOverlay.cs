using cardGames.views.overlays;

namespace spades.views.overlays
{
    public class SpadesCometOverlay : CometOverlay
    {
        protected override float GetLandscapeScale()
        {
            return ManagerView.Instance.Lobby_canvas_landscape.GetComponent<RectTransform>().localScale.x;
        }

        protected override Vector2 GetLandscapeSize()
        {
            float size_x = ManagerView.Instance.Lobby_canvas_landscape.GetComponent<RectTransform>().rect.width;
            float size_y = ManagerView.Instance.Lobby_canvas_landscape.GetComponent<RectTransform>().rect.height;
            return new Vector2(size_x, size_y);
        }
    }
}
