using spades.models;
using spades.controllers;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

    public class RoomUnlockedOverlay : OverlayBase
    {

        [SerializeField] SingleItemView m_single_item_view1;
        [SerializeField] SingleItemView m_single_item_view2;
        [SerializeField] SingleItemView m_single_item_view3;

        [SerializeField] Image m_room_name;
        [SerializeField] Image m_room_gem;


        public void SetData(SpadesWorld world)
        {
            CardGamesSoundsController.Instance.TableUnlocked();

            int world_index = world.Index;

            m_single_item_view1.SetInitialData(world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[0], LobbyController.Instance.Lobby_view.Locked_single[world_index], LobbyController.Instance.Lobby_view.Single_panels[world_index]);
            m_single_item_view2.SetInitialData(world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[1], LobbyController.Instance.Lobby_view.Locked_single[world_index], LobbyController.Instance.Lobby_view.Single_panels[world_index]);
            m_single_item_view3.SetInitialData(world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[2], LobbyController.Instance.Lobby_view.Locked_single[world_index], LobbyController.Instance.Lobby_view.Single_panels[world_index]);

            m_room_gem.sprite = LobbyController.Instance.Lobby_view.Gems_lobby_sprite[world_index];
            m_room_name.sprite = LobbyController.Instance.Lobby_view.Room_name_images[world_index];


        }


        public void ButtonClicked()
        {
            if (this != null)
                SpadesOverlayManager.Instance.HideOverlay(this);
        }
    }
}
