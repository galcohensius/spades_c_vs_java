﻿using spades.controllers;
using cardGames.views;

namespace spades.views.overlays
{
    public class MissionsUnlockOverlay : OverlayBase
    {

        public void ButtonClicked()
        {
            if (this != null)
                SpadesOverlayManager.Instance.HideOverlay(this);
        }
    }

}