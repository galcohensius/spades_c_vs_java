using spades.controllers;
using spades.models;
using System;
using common.controllers;
using cardGames.models;
using cardGames.controllers;
using cardGames.views;

namespace spades.views.overlays
{

    public class GemUnlockedOverlay : OverlayBase
    {
        const string SHARE_ID = "400";

        [SerializeField] Color[] m_light_colors = new Color[5];
        [SerializeField] Color[] m_heavy_colors = new Color[5];

        [SerializeField] Image m_gem_image = null;

        [SerializeField] ParticleSystem burst1 = null;
        [SerializeField] ParticleSystem burst2 = null;
        [SerializeField] ParticleSystem burst3 = null;
        [SerializeField] ParticleSystem burst4 = null;

        //[SerializeField]GameObject m_avatarHolder = null;
        [SerializeField] Animator m_animator = null;
        [SerializeField] TMP_Text m_text_line = null;
        [SerializeField] FacebookShareView m_fb_share_view = null;

        Action m_close_action = null;
        SpadesArena m_arena;

        [SerializeField] MAvatarView m_avatar = null;


        public void SetLevel(SpadesArena arena, OverlayClosedDelegate close_action)
        {
            m_avatar.SetAvatarModel(ModelManager.Instance.GetUser().GetMAvatar());
            bool isBaseUpdated = false;

            m_arena = arena;

            int index = arena.World.Index;

            //set gem sprite
            Sprite gem = LobbyController.Instance.Lobby_view.Gems_lobby_sprite[index];
            m_gem_image.sprite = gem;
            m_gem_image.SetNativeSize();
            var model_manager = ModelManager.Instance as SpadesModelManager;
            switch (arena.World.GetID())
            {
                case "1":
                    if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 1 stone
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t01");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_1_1");
                    }
                    else if (arena.World.Arena_Gems_Won == 2)
                    {
                        // 2 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t02");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_1_2");
                    }
                    else if (arena.World.Arena_Gems_Won == 3)
                    {
                        // 3 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t03");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_1_3");
                    }
                    m_fb_share_view.InitFBShare(SHARE_ID);
                    m_text_line.text = arena.World.GetName() + " GEM WON";
                    break;

                case "2":
                    if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 1 stone
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t04");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_2_1");
                    }
                    else if (arena.World.Arena_Gems_Won == 2)
                    {
                        // 2 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t05");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_2_2");
                    }
                    else if (arena.World.Arena_Gems_Won == 3)
                    {
                        // 3 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t06");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_2_3");
                    }
                    m_fb_share_view.InitFBShare(SHARE_ID);
                    m_text_line.text = arena.World.GetName() + " GEM WON";
                    break;

                case "3":
                    if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 1 stone
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t07");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_3_1");
                    }
                    else if (arena.World.Arena_Gems_Won == 2)
                    {
                        // 2 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t08");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_3_2");
                    }
                    else if (arena.World.Arena_Gems_Won == 3)
                    {
                        // 3 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t09");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_3_3");
                    }
                    m_fb_share_view.InitFBShare(SHARE_ID);
                    m_text_line.text = arena.World.GetName() + " GEM WON";
                    break;

                case "4":
                    if (arena.World.Arena_Gems_Won == 1)
                    {
                        // 1 stone
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t10");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_4_1");
                    }
                    else if (arena.World.Arena_Gems_Won == 2)
                    {
                        // 2 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t11");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_4_2");
                    }
                    else if (arena.World.Arena_Gems_Won == 3)
                    {
                        // 3 stones
                        m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t12");
                        m_avatar.MAvatarModel.FireOnModelChangedEvent();

                        model_manager.Inventory.AddItem($"gem_4_3");
                    }
                    m_fb_share_view.InitFBShare(SHARE_ID);
                    m_text_line.text = arena.World.GetName() + " GEM WON";
                    break;

                case "5":
                    m_text_line.text = "MASTER RING WON";
                    m_fb_share_view.gameObject.SetActive(false);
                    // assetbundle 1 is the basic bundle for the avatars
                    // t17 is the id of the master ring
                    m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t17");
                    m_avatar.MAvatarModel.FireOnModelChangedEvent();

                    model_manager.Inventory.AddItem($"gem_5_1");

                    break;

                case "6":
                    m_text_line.text = "JOKERS RING WON";
                    m_fb_share_view.gameObject.SetActive(false);
                    m_avatar.MAvatarModel.Trophy = new MavatarItem("1_t18");
                    m_avatar.MAvatarModel.FireOnModelChangedEvent();

                    model_manager.Inventory.AddItem($"gem_6_1");
                    break;

                default:
                    m_fb_share_view.InitFBShare(SHARE_ID);
                    m_text_line.text = arena.World.GetName() + " GEM WON";
                    break;
            }

            ModelManager.Instance.GetUser().GetMAvatar().Trophy = new MavatarItem(m_avatar.MAvatarModel.Trophy);
            ModelManager.Instance.GetUser().GetMAvatar().FireOnModelChangedEvent();
            MAvatarController.Instance.UpdateAvatarInfo();

            if (arena.World.Arena_Gems_Won < 4)
            {
                isBaseUpdated = true;
                m_animator.SetBool("BasePart", isBaseUpdated);
            }
            else
            {
                isBaseUpdated = false;
            }

            //play update base anim or not
            if (isBaseUpdated)
            {
                // MAvatarModel newModel = ModelManager.Instance.GetUser().GetMAvatar();
                // TODO: fix this
                //newModel = SpadesAvatarCreationController.Instance.CreateAvatarModelCopyWithSpecifiedGemBaseKey(newModel, arena.World.GetID());

                // m_newAvatar.GetComponent<MAvatarView>().SetAvatarModel(newModel);
                // ModelManager.Instance.GetUser().SetMAvatar(newModel);
                // TODO: fix this
                //AvatarController.Instance.SaveAvatar(newModel);
            }


            if (burst1 != null)
            {
                UnityEngine.ParticleSystem.MainModule main0 = burst1.main;
                main0.startColor = m_light_colors[index];
            }

            if (burst2 != null)
            {
                UnityEngine.ParticleSystem.MainModule main1 = burst2.main;
                main1.startColor = m_heavy_colors[index];
            }

            if (burst3 != null)
            {
                UnityEngine.ParticleSystem.MainModule main2 = burst3.main;
                main2.startColor = m_heavy_colors[index];
            }
            if (burst4 != null)
            {
                UnityEngine.ParticleSystem.MainModule main3 = burst4.main;
                main3.startColor = m_light_colors[index];
            }


        }



        private void HandleSharing(Action SharingDone)
        {
            if (m_arena.World.Index == 4 || m_arena.World.Index == 5)
            {
                // No FB share for rings
                SharingDone?.Invoke();
                return;
            }

            if (SpadesStateController.Instance.IsFBShare(SHARE_ID))
            {
                string[] desc_tokens = new string[2];
                string[] title_tokens = new string[1];

                title_tokens[0] = m_arena.World.GetName().ToUpper();
                //desc_tokens[0] = FormatUtils.FormatPrice(m_coins_won);
                desc_tokens[1] = title_tokens[0];

                FacebookShareManager.Instance.Share((int.Parse(SHARE_ID) + int.Parse(m_arena.World.GetID())) + "", (bool succ) =>
                {

                    SharingDone();

                }, title_tokens, desc_tokens);
            }
            else
            {
                SharingDone();
            }

        }

        public void OnClickToContinue()
        {
            HandleSharing(() =>
            {
                if (this != null)
                    SpadesOverlayManager.Instance.HideOverlay(this);
            });
        }
    }
}
