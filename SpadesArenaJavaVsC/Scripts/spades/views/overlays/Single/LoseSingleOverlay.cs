using cardGames.models;
using cardGames.views;
using common.views;
using spades.controllers;
using System;

namespace spades.views.overlays

{

    public class LoseSingleOverlay : OverlayBase
    {


        [SerializeField] WinLoseView m_win_lose_view = null;
        [SerializeField] Image m_table_image = null;

        BalanceCoinTextbox m_user_coins_text = null;

        bool m_button_clicked = false;
        bool m_ff_animation = false;



        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);
            GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            //SetUser ();
        }

        public void SetData(bool wasCanceled, int gap, int coins, int coins_for_points_payout, int xp, Action LobbyClicked, Action NewGameClicked, Action AnimationDone)
        {

            m_table_image.sprite = ManagerView.Instance.Loaded_table;


            m_button_clicked = false;

            User user = ModelManager.Instance.GetUser();

            m_user_coins_text = LobbyTopController.Instance.Coins_text;

            //m_user_coins_text.Amount = user.GetCoins ();

            m_win_lose_view.SetScreenData(false, wasCanceled, gap, coins, coins_for_points_payout, xp, NewGameClicked, "New Game", null, null, "MATCH OVER", LobbyClicked);

        }


        /// <summary>
        /// Time line function
        /// </summary>

        public void UpdateCoinsDiamonds()
        {
            //we comment this line as the lose doesnt give money anymore - 22/04/18 Alon and Ran
            m_win_lose_view.UpdateCoins(m_user_coins_text, null, m_ff_animation);
        }

        public WinLoseView Win_lose_view
        {
            get
            {
                return m_win_lose_view;
            }
        }
    }
}