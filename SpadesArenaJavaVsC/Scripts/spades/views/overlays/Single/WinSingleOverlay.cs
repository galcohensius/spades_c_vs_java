using System.Collections;
using spades.models;
using spades.controllers;
using System;
using common.views;
using common.controllers;
using common.utils;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

    public class WinSingleOverlay : OverlayBase
    {

        const string SHARE_ID = "100";

        const string BUBBLE_TEXT_LETS_PLAY = "Let's partner again!";
        const string BUBBLE_TEXT_GO = "Sorry, gotta go!";
        const string BUBBLE_TEXT_HMMM = "Hmmmmm...";
        const string BUBBLE_TEXT_NO = "I can't right now...";
        const string BUBBLE_TEXT_YES = "Sure thing!";


        [SerializeField] WinLoseView m_win_lose_view = null;
        [SerializeField] Image m_table_image = null;

        [SerializeField] BalanceCoinTextbox m_user_coins_text = null;

        [SerializeField]
        private MAvatarView m_soloAvatarView;

        [SerializeField]
        private MAvatarView m_partnerAvatarView_1;

        [SerializeField]
        private MAvatarView m_partnerAvatarView_2;

        private MAvatarView m_currentFallingAvatar = null;
        private MAvatarView.AvatarAnimationStateTrigger m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropLeftJump;

        //particle systems
        [SerializeField] GameObject m_ballon_system = null;
        [SerializeField] GameObject m_confetti_system = null;
        [SerializeField] GameObject m_fireworks_system = null;
        [SerializeField] GameObject m_champaigne_system = null;
        [SerializeField] GameObject m_stageFX_system = null;

        [SerializeField] CanvasGroup m_avatarGroup;
        [SerializeField] FacebookShareView m_fb_share_view = null;

        bool m_ff_animation = false;
        int m_coins_payout;
        bool m_level_up;


        [SerializeField] GameObject m_player_rematch_bubble = null;
        [SerializeField] TMP_Text m_player_rematch_text = null;
        [SerializeField] GameObject m_partner_rematch_bubble = null;
        [SerializeField] TMP_Text m_partner_rematch_text = null;

        // Used to round-robin the effects
        static int m_effect_index = -1;

        private MAvatarModel m_userAvatarModel;
        private MAvatarModel m_partnerAvatarModel;

        public override void Show(OverlaysManager manager = null, OverlayClosedDelegate close_delegate = null)
        {
            base.Show(manager, close_delegate);

            GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
            CardGamesSoundsController.Instance.GameWin();

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            ISpadesMatch match = active_match.GetMatch();

            if (match.Playing_mode == PlayingMode.Partners)
            {
                StartCoroutine(AnimatePartners());
            }
            else
            {
                StartCoroutine(DelayAvatarDrop(1f));
            }

            m_table_image.sprite = ManagerView.Instance.Loaded_table;
        }

        public override void Close()
        {
            StopAllCoroutines();

            // Stop the fireworks sounds. For some reason, they are not always stopped automatically - RAN (28/6/18)
            SpadesSoundsController.Instance.PlayFireWorksSFX(false);

            base.Close();
        }

        private IEnumerator AnimatePartners()
        {
            yield return new WaitForSeconds(1f);

            m_partnerAvatarView_1.gameObject.SetActive(true);
            m_partnerAvatarView_2.gameObject.SetActive(true);

            m_partnerAvatarView_1.SetAvatarModel(m_userAvatarModel);
            m_partnerAvatarView_2.SetAvatarModel(m_partnerAvatarModel);




            m_currentFallingAvatar = m_partnerAvatarView_1;
            //StartCoroutine(FadeAvatars());
            //m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropLeftJump;
            //AnimateTargetAvatar(m_partnerAvatarView_1.gameObject);

            yield return new WaitForSeconds(1f);

            m_currentFallingAvatar = m_partnerAvatarView_2;
            //StartCoroutine(FadeAvatars());
            //m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.DropRightJump;
            //AnimateTargetAvatar(m_partnerAvatarView_2.gameObject);
        }

       
        private IEnumerator DelayAvatarDrop(float wantedDelayTime)
        {
            yield return new WaitForSeconds(wantedDelayTime);
            m_currentFallingAvatar = m_soloAvatarView;

            m_soloAvatarView.gameObject.SetActive(true);
            m_soloAvatarView.SetAvatarModel(m_userAvatarModel);


            //m_currentAnimationTrigger = MAvatarView.AvatarAnimationStateTrigger.Drop;
            //AnimateTargetAvatar(m_soloAvatarView.gameObject);
        }

        private void AnimateTargetAvatar(GameObject targetAvatar)
        {
            targetAvatar.GetComponent<MAvatarView>().PlayAnimation(m_currentAnimationTrigger);
        }

        private void InitAvatarPosition(GameObject targetAvatar)
        {
            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();
            ISpadesMatch match = active_match.GetMatch();

            if (match.Playing_mode == PlayingMode.Partners)
            {
                targetAvatar.transform.localPosition = new Vector3(0, -184, 0);
            }
            else
            {
                targetAvatar.transform.localPosition = new Vector3(0, -224, 0);
            }
        }

        private void OnPlayFall()
        {
            CardGamesSoundsController.Instance.ArenaJump();
        }

        public void SetWinScreenData(int gap, int coins_payout, int coins_for_points_payout, int xpPayout, bool level_up, bool show_rematch, bool cheat)
        {

            m_level_up = level_up;

            m_coins_payout = coins_payout;

            m_user_coins_text = LobbyTopController.Instance.Coins_text;

            SpadesActiveMatch active_match = SpadesModelManager.Instance.GetActiveMatch();

            ISpadesMatch match = new SpadesSingleMatch();
            match.Playing_mode = PlayingMode.Solo;

            if (!cheat)
                match = active_match.GetMatch();
            else
            {
                int curr_xp = ModelManager.Instance.GetUser().GetXPInLevel();
                int curr_level = ModelManager.Instance.GetUser().GetLevel();
                int curr_thresh = ModelManager.Instance.GetUser().GetLevelTotalXP();

                LobbyController.Instance.SetOldXP(curr_xp);
                LobbyController.Instance.SetOldLevel(curr_level);
                LobbyController.Instance.Old_threshold = curr_thresh;
            }

            if (show_rematch)
            {
                m_win_lose_view.SetScreenData(false, false, gap, coins_payout, coins_for_points_payout, xpPayout, PreShareNewGameClicked, "New Game", PreShareRematchClicked, "play again", "YOU WIN", PreShareLobbyClicked);

            }
            else
            {
                m_win_lose_view.SetScreenData(false, false, gap, coins_payout, coins_for_points_payout, xpPayout, PreShareNewGameClicked, "New Game", null, null, "YOU WIN", PreShareLobbyClicked);
            }

            // Hide avatars and set model
            MAvatarModel model = ModelManager.Instance.GetUser().GetMAvatar();
            m_partnerAvatarView_1.gameObject.SetActive(false);
            m_partnerAvatarView_2.gameObject.SetActive(false);
            m_soloAvatarView.gameObject.SetActive(false);

            m_userAvatarModel = model;
            m_partnerAvatarModel = (ModelManager.Instance.GetTable() as SpadesTable).GetPlayer(1).GetMAvatarModel();


            m_fb_share_view.InitFBShare(SHARE_ID);
        }

        private void PreShareRematchClicked()
        {
            SingleMatchController.Instance.RematchClicked();
        }

        private void PreShareLobbyClicked()
        {
            HandleSharing(SingleMatchController.Instance.LobbyClicked);
        }

        private void PreShareNewGameClicked()
        {
            HandleSharing(SingleMatchController.Instance.NewGameClicked);
        }

        /// <summary>
        /// CALLED FROM TIMELINE
        /// </summary>
        private void StartProgressAnimation()
        {
            m_win_lose_view.StartProgressAnimation();
        }

        /// <summary>
        /// CALLED FROM TIMELINE
        /// </summary>
        private void UpdateCoinsOverlay()
        {
            m_win_lose_view.UpdateCoins(m_user_coins_text, PostCoinsFlight, m_ff_animation);
        }

        private void PostCoinsFlight()
        {

        }

        private void TL_PlayHappyAnim()
        {
        }

        private void StartConfetti()
        {
            if (!m_level_up)
            {
                // round robin the effects, randomize on first win
                if (m_effect_index == -1)
                    m_effect_index = UnityEngine.Random.Range(0, 4);

                //m_effect_index = 1;

                if (!m_ff_animation)
                {
                    switch (m_effect_index)
                    {
                        case (0):
                            SpadesSoundsController.Instance.PlayPopSounds();
                            m_confetti_system.SetActive(true);
                            break;
                        case (1):
                            m_champaigne_system.SetActive(true);
                            break;
                        case (2):
                            m_stageFX_system.SetActive(true);
                            break;
                        case (3):
                            m_ballon_system.SetActive(true);
                            break;
                        case (4):
                            m_fireworks_system.SetActive(true);
                            break;
                    }


                    m_effect_index++;
                    if (m_effect_index == 5)
                        m_effect_index = 0;
                }
            }

        }

        private void HandleSharing(Action SharingDone)
        {
            // Stop all coroutines here to cancel the fake play again flow if exists
            StopAllCoroutines();

            if (SpadesStateController.Instance.IsFBShare(SHARE_ID) && m_win_lose_view.Should_show_fb_share)
            {
                string[] desc_tokens = new string[1];

                desc_tokens[0] = FormatUtils.FormatPrice(m_coins_payout);

                FacebookShareManager.Instance.Share(SHARE_ID, (bool succ) =>
                {

                    SharingDone();

                }, null, desc_tokens);
            }
            else
            {
                SharingDone();
            }

        }

        public void PlayerInitiatedRematch()
        {
            m_win_lose_view.SetLeftBtnInteractable(false);
            m_win_lose_view.SetRightBtnInteractable(false);

            // Show the player's bubble
            ShowPlayerBubbleText(BUBBLE_TEXT_LETS_PLAY, 2);

            // Show partner's bubble
            StartCoroutine(ShowPartnerBubbleTextDelayed(BUBBLE_TEXT_HMMM, 1, 0.5f));

            // Disable button
            m_win_lose_view.SetRightBtnInteractable(false);

        }

        public void PlayerAcceptedRematch()
        {
            // Disable button
            m_win_lose_view.SetRightBtnInteractable(false);
            m_win_lose_view.SetLeftBtnInteractable(false);

            ShowPlayerBubbleText(BUBBLE_TEXT_YES);
        }


        public void PartnerAcceptedRematch()
        {
            ShowPartnerBubbleText(BUBBLE_TEXT_YES);
        }

        public void PartnerRejectedRematch(bool leave)
        {
            if (leave)
                ShowPartnerBubbleText(BUBBLE_TEXT_GO);
            else
                ShowPartnerBubbleText(BUBBLE_TEXT_NO, 2);

            m_win_lose_view.SetRightBtnInteractable(false);
            m_win_lose_view.SetLeftBtnInteractable(true);
        }

        public void PartnerInitatedRematch()
        {
            // Show the player's bubble
            ShowPartnerBubbleText(BUBBLE_TEXT_LETS_PLAY, 2);


            // Change button text
            m_win_lose_view.ChangeRightButtonLabel("accept");

        }

        public void CancelPartnerRematch()
        {
            if (m_player_rematch_bubble != null)
                m_player_rematch_bubble.SetActive(false);

            ShowPartnerBubbleText(BUBBLE_TEXT_GO);

            m_win_lose_view.SetRightBtnInteractable(false);
            m_win_lose_view.SetLeftBtnInteractable(true);
        }


        private void ShowPlayerBubbleText(string text, int lines = 1)
        {
            m_player_rematch_bubble.SetActive(true);
            m_player_rematch_text.text = text;
            if (lines == 1)
            {
                ((RectTransform)m_player_rematch_bubble.transform).sizeDelta = new Vector2(309, 104);
            }
            else
            {
                ((RectTransform)m_player_rematch_bubble.transform).sizeDelta = new Vector2(309, 140);
            }
            CardGamesSoundsController.Instance.ChatBubble();
        }

        private void ShowPartnerBubbleText(string text, int lines = 1)
        {
            if (m_partner_rematch_bubble != null)
            {
                m_partner_rematch_bubble.SetActive(true);
                if (lines == 1)
                {
                    ((RectTransform)m_partner_rematch_bubble.transform).sizeDelta = new Vector2(309, 94);
                }
                else
                {
                    ((RectTransform)m_partner_rematch_bubble.transform).sizeDelta = new Vector2(309, 140);
                }
            }

            if (m_partner_rematch_text != null)
                m_partner_rematch_text.text = text;


            CardGamesSoundsController.Instance.ChatBubble();
        }

        private IEnumerator ShowPartnerBubbleTextDelayed(string text, int lines, float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            ShowPartnerBubbleText(text, lines);
        }


        private IEnumerator ShowPlayerBubbleTextDelayed(string text, int lines, float delay)
        {
            yield return new WaitForSecondsRealtime(delay);
            ShowPlayerBubbleText(text, lines);
        }


        public bool FF_animation
        {
            get
            {
                return m_ff_animation;
            }

        }

        public WinLoseView Win_lose_view
        {
            get
            {
                return m_win_lose_view;
            }
        }
    }
}