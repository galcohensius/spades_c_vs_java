﻿using spades.controllers;
using cardGames.views;

namespace spades.views.overlays
{
	public class CollectableOverlay : OverlayBase {

		[SerializeField] Image		m_image=null;

		// Use this for initialization
		public void SetImage(int single_index)
		{
			m_image.sprite = ManagerView.Instance.GetCollectableImage (single_index);
			//StartCoroutine (DelayClose (2));
		}
	

		public void ButtonClicked()
		{
			SpadesOverlayManager.Instance.HideOverlay (this);
		}

		public void ShakeScreen()
		{
			LobbyController.Instance.Lobby_view.CameraShake ();
		}

		public void PlaySFX()
		{
            SpadesSoundsController.Instance.SpadesBrokenCardsMove ();
		}

	}
}