using spades.controllers;
using common.utils;
using common.models;
using System;
using common.controllers;
using cardGames.models;
using cardGames.views;

namespace spades.views.overlays
{

	public class LevelUpOverlay : OverlayBase {

		const string SHARE_ID = "200";

		[SerializeField] TMP_Text			m_level_text=null;

		[SerializeField] TMP_Text 			m_pay_coins_text=null;

		[SerializeField] TMP_Text 			m_hourly_bouns_text=null;

		[SerializeField]FacebookShareView	m_fb_share_view=null;

		[SerializeField]Image				m_table_image=null;

        [SerializeField] TMP_Text m_bottomText;
        [SerializeField] GameObject m_rewardGroup;
        [SerializeField] GameObject m_prizeGroup;
        [SerializeField] GameObject m_videoGroup;




        int m_level;

		bool m_clicked;


		public void SetLevel(int level,int hourly_bouns=0)
		{
			m_level = level;
			m_level_text.text = level.ToString();

			gameObject.AddComponent<Canvas> ();
			gameObject.AddComponent<GraphicRaycaster> ();

			GetComponent<Canvas> ().overrideSorting = true;
			GetComponent<Canvas> ().sortingOrder = 310;
		
			if (ModelManager.Instance.Last_levelup_bonus_awarded != null)
			{

				int coin = ModelManager.Instance.Last_levelup_bonus_awarded.GoodsList.CoinsValue;

				m_pay_coins_text.text = "<sprite=8>" + FormatUtils.FormatPrice (coin);
			
				if (hourly_bouns > 0)
				{
					m_hourly_bouns_text.gameObject.SetActive (true);
					m_hourly_bouns_text.text = "Hourly Bonus Increased to <sprite=8>" + FormatUtils.FormatPrice (hourly_bouns);
				}
				else
				{
					m_hourly_bouns_text.gameObject.SetActive (false);
				}
			}
			else
			{
				m_pay_coins_text.text = "";
				m_hourly_bouns_text.gameObject.SetActive (false);

			}

            m_videoGroup.SetActive(false);

#if   !UNITY_WEBGL
            if (SpadesIMSController.Instance.BannersByLocations.ContainsKey("V3"))
                Invoke(nameof(ShowVideoButton), 6);
#endif

            m_table_image.sprite = ManagerView.Instance.Loaded_table;


            m_fb_share_view.InitFBShare (SHARE_ID);
		}

        public void ShowVideoButton()
        {
            RewardedVideoBonus videoBonus = (RewardedVideoBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.RewardedVideoLevelUp);

            string data = "No Data";

            if (videoBonus != null)
                data = "<sprite=1>" + FormatUtils.FormatBuyIn(videoBonus.CoinsSingle);

            m_videoGroup.transform.FindDeepChild("watch_Text").GetComponent<TMP_Text>().text = data;

            m_prizeGroup.SetActive(false);
            m_rewardGroup.SetActive(false);
            m_videoGroup.SetActive(true);
            m_bottomText.text = "No Thanks";
        }

        public void WatchVideoClicked()
        {
            VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoLevelUp);
        }

		public void ButtonClicked()
		{
			if (m_clicked)
				return;

			m_clicked = true;
			HandleSharing (() => {

				SpadesOverlayManager.Instance.HideOverlay (this);
				
                //prompt user to change the avatar when reaching level2 - if mobile - only if you are on the defualt mobile - or if you are on WebGL 
                //added limitation to show the popup only once a session
                if (!ManagerView.Instance.Show_avatar_selection_in_session && ModelManager.Instance.GetUser().GetLevel()>=2)
                {
                    ManagerView.Instance.Show_avatar_selection_in_session = true;
                }
				
			});


		}

		private void HandleSharing (Action SharingDone)
		{
			
			if (SpadesStateController.Instance.IsFBShare (SHARE_ID))
			{
				string[]	desc_tokens = new string[1];

				desc_tokens [0] = m_level.ToString ();

				FacebookShareManager.Instance.Share (SHARE_ID, (bool succ) => {

					SharingDone ();

				}, null, desc_tokens);
			}
			else
			{
				SharingDone ();
			}

		}

		public override void Close ()
		{
			base.Close ();
			LobbyTopController.Instance.UpdateCoinsText ();
		}


    }
}
