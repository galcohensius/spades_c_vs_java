﻿using System.Collections;
using cardGames.views;

namespace spades.views.overlays
{

	public class FastForwardOverlay : OverlayBase {

		[SerializeField]TMP_Text	m_text=null;
		[SerializeField]Image		m_bg_image=null;

		Coroutine					m_blink_routine=null;
		Animator					m_animator=null;

		const float					BLINK_SPEED=0.25f;

		public override void Show (OverlaysManager manager, OverlayClosedDelegate close_delegate)
		{
			base.Show (manager, close_delegate);

			m_animator = GetComponent<Animator> ();

			m_blink_routine = StartCoroutine (BlinkText ());

		}

		IEnumerator	BlinkText()
		{
			while (true)
			{
				yield return new WaitForSecondsRealtime (BLINK_SPEED*3);

				m_text.gameObject.SetActive (false);

				yield return new WaitForSecondsRealtime (BLINK_SPEED);

				m_text.gameObject.SetActive (true);
			}
		}

		public Image Bg_image {
			get {
				return m_bg_image;
			}
			set {
				m_bg_image = value;
			}
		}

		public Animator Animator {
			get {
				return m_animator;
			}
		}
	}
}