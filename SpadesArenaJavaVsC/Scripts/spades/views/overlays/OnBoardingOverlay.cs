﻿using spades.controllers;
using spades.models;
using common.facebook;
using System;
using common.experiments;
using cardGames.models;
using cardGames.views;
using cardGames.controllers;

namespace spades.views.overlays
{

    public class OnBoardingOverlay : OverlayBase
    {


        [SerializeField] SingleItemView m_item = null;
        [SerializeField] GameObject m_facebook_panel_parent = null;
        [SerializeField] GameObject m_facebook_button_object = null;
        [SerializeField] GameObject m_FB_button_loading_indication = null;
        [SerializeField] TMP_Text m_facebook_button_text = null;
        //[SerializeField] GameObject m_single_item_holder = null;

        SpadesSingleMatch m_single_match = null;
        Action m_beforeLoginAction = null;

        GameObject m_wade, m_sign; // For Experiment

        bool m_connecting_to_fb;

        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);

            m_facebook_panel_parent.transform.localPosition = new Vector2(ManagerView.Instance.Actual_width / 2, -550);

            // If the user is already logged in (WegGL), do not show the facebook connect panel
            if (FacebookController.Instance.IsLoggedIn() 
                || ExperimentsManager.Instance.GetValue(LobbyController.ONBOARDING_EXPERIMENT_ID) == 3)
            {
                m_facebook_panel_parent.SetActive(false);
            }
            CardGamesSoundsController.Instance.PlayOnBoarding();

            ExperimentsManager.Instance.RunIf(LobbyController.ONBOARDING_EXPERIMENT_ID, 3, () =>
            {
                m_wade = gameObject.transform.Find("Wade").gameObject;
                m_sign = gameObject.transform.Find("Sign").gameObject;
                m_wade.SetActive(false);
                m_sign.SetActive(false);
            });
        }

        public void SetLobbyItemData(SpadesSingleMatch single_match)
        {
            m_single_match = single_match;
            m_item.SetInitialData(single_match, LobbyController.Instance.Lobby_view.Locked_single[0], LobbyController.Instance.Lobby_view.Single_panels[0]);

            if (ManagerView.Instance.Aspect43)
            {

                // TODO: Find a fix for 4:3 sizing, where the lobby item is scaled on Y axis, and the onboarding item
                // needs to be scaled as well

                //m_single_item_holder.transform.localScale = new Vector2(1, 1.1f);
            }
        }

        public void FacebookButtonClicked()
        {
            m_facebook_button_object.GetComponent<Button>().interactable = false;

            ManagerView.Instance.EnableDisableButton(m_facebook_button_object.GetComponent<Button>());

            m_FB_button_loading_indication.SetActive(true);
            m_facebook_button_text.gameObject.SetActive(false);

            m_connecting_to_fb = true;

            LobbyController.Instance.FBLogin((bool success) =>
            {
                if (success)
                {
                    //need to check if you are new player
                    if (!ModelManager.Instance.GetUser().NeverPlayed)
                    {
                        //old player remove the overlay
                        SpadesOverlayManager.Instance.HideAllOverlays();

                        //this is used from OnBoardingTypeB - need to clear the game and show lobby before trigger login to MES
                        if (m_beforeLoginAction != null)
                            m_beforeLoginAction();
                    }
                    else
                    {
                        // New Player - show overlay - hide FB button
                        m_facebook_panel_parent.SetActive(false);
                    }


                }
                else
                {
                    m_facebook_button_object.GetComponent<Button>().interactable = true;
                    ManagerView.Instance.EnableDisableButton(m_facebook_button_object.GetComponent<Button>());

                    m_FB_button_loading_indication.SetActive(false);
                    m_facebook_button_text.gameObject.SetActive(true);
                }

                m_connecting_to_fb = false;

            });

        }

        public void ButtonDown()
        {
            if (m_connecting_to_fb)
                return;

         //   LobbyController.Instance.Onboarding_tracker.LogEvent("Starting_first_game");
            
            CardGamesSoundsController.Instance.ButtonClicked();
            SingleMatchController.Instance.StartSingleMatch(m_single_match, SearchingCancelled, SearchingDone);

            gameObject.SetActive(false);

        }

        public void ButtonUp()
        {
        }

        private void SearchingCancelled()
        {
            gameObject.SetActive(true);
        }

        private void SearchingDone()
        {
            SpadesOverlayManager.Instance.HideOverlay(this);
        }

     

        public Action BeforeLoginAction
        {
            get
            {
                return m_beforeLoginAction;
            }
            set
            {
                m_beforeLoginAction = value;
            }
        }
    }
}