using common.views;
using common.controllers;
using System;
using cardGames.views;

namespace spades.views.overlays
{
		
	public class WelcomeOverlay :  OverlayBase
	{


		[SerializeField]GameObject			m_try_again_object=null;

        [SerializeField] DynamicLoader m_loader = null;


        public override void Show(OverlaysManager manager, OverlayClosedDelegate close_delegate)
        {
            base.Show(manager, close_delegate);
            LoggerController.Instance.Log("Start Welcome Screen");
            m_loader.UpdateImmediate(0.5f);
            
        }

        public void UpdateProgress(float progress,Action OnUpdateComplete=null) {
            m_loader.UpdateWithAnim(progress,OnUpdateComplete);
        }

        public void TryAgainClicked()
		{
            // NOT USED (RAN 7/2/2019)
            // SEE LobbyController:189
		}


		public void ShowTryAgain()
		{
			//m_try_again_object.SetActive (true);
		}

        public override void OnBackButtonClicked()
        {
           // do nothing
        }

	}
}
