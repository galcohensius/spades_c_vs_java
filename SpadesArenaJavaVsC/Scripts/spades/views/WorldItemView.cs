using System.Collections;
using System.Collections.Generic;
using spades.models;
using spades.controllers;
using System;
using cardGames.models;
using cardGames.controllers;
using cardGames.models.contests;
using cardGames.views.contests;
using static cardGames.models.World;
using common.ims.views;
using common.ims;

namespace spades.views
{

    public class WorldItemView : MonoBehaviour
    {
        private Action m_rotate_complete_delegate;

        [SerializeField] SingleItemView[] m_singles_view = null;
        [SerializeField] ArenaItemView m_arena_view = null;

        LobbyView m_lobby_view = null;

        SpadesWorld m_world;
        public const float SPIN_TIME = .55f;
        public const float DELAY_BETWEEN_PANELS_SPIN = 0.13f;

        bool m_is_rotating = false;

        int m_num_singles = 3;
        bool[] m_single_did_flip = new bool[3];
        bool m_arena_did_flip = false;

        List<ContestHatView> m_contest_indicators = new List<ContestHatView>();
        [SerializeField] GameObject m_contest_indication_holder;

        GameObject m_potentialBanner = null;


        private void Start()
        {

            UpdateContestsHats();
            ModelManager.Instance.ContestsLists.OnContestDataChanged += UpdateContestsHats;

        }


        public void RotatePanels(bool rotate_left, Action rotate_complete, bool instant = false)
        {
            m_is_rotating = true;
            m_rotate_complete_delegate = rotate_complete;
            float delay = 0;
            int spin_direction = -1;
            int i = 0;

            float actual_spin_time = SPIN_TIME;
            float actual_delay_time = DELAY_BETWEEN_PANELS_SPIN;

            if (instant)
            {
                actual_spin_time = 0.1f;
                actual_delay_time = 0;
            }

            if (rotate_left)
            {
                spin_direction = 1;
                delay = 3 * actual_delay_time;
            }
            for (i = 0; i < m_singles_view.Length; i++)
            {
                m_single_did_flip[i] = false;
                iTween.ScaleTo(m_singles_view[i].gameObject, iTween.Hash("x", 0.8, "y", 0.8, "easeType", iTween.EaseType.linear, "time", actual_spin_time / 4, "delay", delay));
                iTween.ScaleTo(m_singles_view[i].gameObject, iTween.Hash("x", 1, "y", 1, "easeType", iTween.EaseType.linear, "time", actual_spin_time / 4, "delay", delay + actual_spin_time / 4));
                iTween.RotateAdd(m_singles_view[i].gameObject, iTween.Hash("y", spin_direction * 180, "easeType", iTween.EaseType.easeOutBack, "time", actual_spin_time, "delay", delay, "onstart", "PlaySound", "onstarttarget", this.gameObject, "onupdate", "FlipInTime", "onupdatetarget", this.gameObject, "onupdateparams", i));

                if (rotate_left)
                    delay -= actual_delay_time;
                else
                    delay += actual_delay_time;

            }

            m_arena_did_flip = false;
            iTween.ScaleTo(m_arena_view.gameObject, iTween.Hash("x", 0.8, "y", 0.8, "easeType", iTween.EaseType.linear, "time", actual_spin_time / 4, "delay", delay));
            iTween.ScaleTo(m_arena_view.gameObject, iTween.Hash("x", 1, "y", 1, "easeType", iTween.EaseType.linear, "time", actual_spin_time / 4, "delay", delay + actual_spin_time / 4));
            iTween.RotateAdd(m_arena_view.gameObject, iTween.Hash("y", spin_direction * 180, "easeType", iTween.EaseType.easeOutBack, "time", actual_spin_time, "delay", delay, "onstart", "PlaySound", "onstarttarget", this.gameObject, "onupdate", "FlipInTimeArena", "onupdatetarget", this.gameObject));


            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "onupdatetarget", this.gameObject, "onupdate", "DummyUpdate", "time", 3 * actual_delay_time + actual_spin_time, "oncompletetarget", this.gameObject, "oncomplete", "RotateLastPanelDone"));

        }

        private void RotateLastPanelDone()
        {
            m_is_rotating = false;

            m_rotate_complete_delegate?.Invoke();

            ForceFlipAllPanels();
        }

        public void ForceFlipAllPanels()
        {

            ForceFlipArena();

            for (int i = 0; i < m_num_singles; i++)
            {
                ForceFlipInTime(i);
            }
        }

        private void PlaySound()
        {
            int index = ModelManager.Instance.Worlds.FindIndex(w => w == m_world);
            if (index == LobbyController.Instance.Lobby_view.GetSwipe().Screen_index - 1)
                CardGamesSoundsController.Instance.PlayPanelFlip();
        }

        private void FlipInTime(int index)
        {

            PlayingMode initial_pm = SpadesModelManager.Instance.Initial_playing_mode;

            float angle = m_singles_view[index].gameObject.GetComponent<RectTransform>().localEulerAngles.y;

            int temp_holder_mode = Mathf.CeilToInt((angle - GetPanelFlipAngle(index)) / 180);

            if (temp_holder_mode == 0 && (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners && m_singles_view[index].Single_match.Playing_mode == PlayingMode.Solo))
            {
                if (initial_pm == PlayingMode.Solo)
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 0f, 0);

                m_singles_view[index].SwitchModel(m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[index]);

                m_single_did_flip[index] = true;
            }
            else if (temp_holder_mode == 1 && SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo && m_singles_view[index].Single_match.Playing_mode == PlayingMode.Partners)
            {
                if (initial_pm == PlayingMode.Partners)
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 0f, 0);

                m_singles_view[index].SwitchModel(m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[index]);

                m_single_did_flip[index] = true;
            }

        }

        private void FlipInTimeArena()
        {

            PlayingMode initial_pm = SpadesModelManager.Instance.Initial_playing_mode;

            float angle = m_arena_view.gameObject.GetComponent<RectTransform>().localEulerAngles.y;

            int temp_holder_mode = Mathf.CeilToInt((angle - GetPanelFlipAngle(3)) / 180);

            if (temp_holder_mode == 0 && (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners && m_arena_view.Arena.Playing_mode == PlayingMode.Solo))
            {
                if (initial_pm == PlayingMode.Solo)
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 0f, 0);

                m_arena_view.SetModel(m_world.GetArena(SpadesModelManager.Instance.Playing_mode));
                m_arena_did_flip = true;
            }
            else if (temp_holder_mode == 1 && (SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo && m_arena_view.Arena.Playing_mode == PlayingMode.Partners))
            {
                if (initial_pm == PlayingMode.Partners)
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 0f, 0);

                m_arena_view.SetModel(m_world.GetArena(SpadesModelManager.Instance.Playing_mode));
                m_arena_did_flip = true;
            }

        }

        private void ForceFlipArena()
        {

            PlayingMode initial_pm = SpadesModelManager.Instance.Initial_playing_mode;

            if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
            {
                if (initial_pm == PlayingMode.Solo)
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 0f, 0);


            }
            else if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo)
            {
                if (initial_pm == PlayingMode.Partners)
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_arena_view.Holder.localEulerAngles = new Vector3(0, 0f, 0);

            }

            // PromoRoom Changes
            if (m_world.WorldType == WorldTypes.Regular)        
                m_arena_view.SetModel(m_world.GetArena(SpadesModelManager.Instance.Playing_mode));

        }

        private void ForceFlipInTime(int index)
        {
            PlayingMode initial_pm = SpadesModelManager.Instance.Initial_playing_mode;

            if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
            {
                if (initial_pm == PlayingMode.Solo)
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 0f, 0);
            }
            else if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo)
            {
                if (initial_pm == PlayingMode.Partners)
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 180f, 0);
                else
                    m_singles_view[index].Holder.localEulerAngles = new Vector3(0, 0f, 0);

            }

            // PromoRoom Changes
            if (m_world.WorldType == WorldTypes.Regular)
                m_singles_view[index].SwitchModel(m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[index]);
        }


        public void SetWorldModel(SpadesWorld world, LobbyView lobby_view)
        {

            m_lobby_view = lobby_view;
            m_world = world;

            ModelManager.Instance.On_Mode_Changed += ModeChanged;

            

            SetWorldPanels();
        }

        public void AddIMSBannerIfNeeded()
        {
            if (m_potentialBanner != null)
                Destroy(m_potentialBanner);


            if (m_world.Index == 5)
            {

                if (IMSController.Instance.BannersByLocations.ContainsKey("L3"))
                {
                    m_potentialBanner = Instantiate(ManagerView.Instance.ImsBanner_prefab, transform);
                    RectTransform rect = m_potentialBanner.GetComponent<RectTransform>();
                    RectTransform arenaPos = m_arena_view.gameObject.GetComponent<RectTransform>();

                    m_arena_view.gameObject.SetActive(false);

                    rect.localPosition = arenaPos.localPosition;
                    rect.localScale = new Vector3(1f, 1f, 1f);
                    IMSBannerView iMSBannerView = m_potentialBanner.GetComponent<IMSBannerView>();
                    iMSBannerView.Location = "L3";
                    iMSBannerView.UpdateModel();
                }
            }

        }


        private void BuildAndSetData(int contestHatPanel, Vector3 pos, int contest_id)
        {
            GameObject gameObject = (GameObject)Instantiate(ContestsController.Instance.Contest_lobby_hats[contestHatPanel - 1]);
            gameObject.transform.SetParent(m_contest_indication_holder.transform);
            if (contestHatPanel == 1)
                gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.9f, 0.9f, 1f);
            else
                gameObject.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            gameObject.GetComponent<RectTransform>().localPosition = pos;
            ContestHatView contestHatView = gameObject.GetComponent<ContestHatView>();
            contestHatView.InitAndLink(ModelManager.Instance.ContestsLists.GetContestByID(contest_id));
            contestHatView.PlayAnim(true);
            m_contest_indicators.Add(contestHatView);
        }

        IEnumerator DeleteContestIndications(bool animate)
        {
            if (this == null) yield break;
            if (animate)
            {
                foreach (Transform item in m_contest_indication_holder.transform)
                {
                    item.gameObject.GetComponent<ContestHatView>().PlayAnim(false);
                }
                yield return new WaitForSeconds(0.25f);
            }

            foreach (Transform item in m_contest_indication_holder.transform)
            {
                Destroy(item.gameObject);
            }
            m_contest_indicators.Clear();
            yield return null;
        }


        public void UpdateContestsHats()
        {

            if (this==null || !this.gameObject.activeInHierarchy)
                return;

          //  LoggerController.Instance.Log("UpdateContestsHats world " + m_world.GetID());

            StartCoroutine(DeleteContestIndications(false));

            //need to understand what if any of the tables in this world are part of the contest
            //only after you see the entire list of tables in contest you can build the hats on top of them

            if (ModelManager.Instance.ContestsLists != null && ModelManager.Instance.ContestsLists.OngoingContests.Count > 0)
            {
                int curr_level = ModelManager.Instance.GetUser().GetLevel();

                /*SingleMatch first_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[0];
                SingleMatch second_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[1];
                SingleMatch third_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[2];
                */

                SingleMatch Min_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[0];
                SingleMatch Max_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[2];

                //the entire world is locked
                if (Min_singleMatch.Min_level > curr_level)
                    return;

                //max single is locked - switching max to middle one
                if (Max_singleMatch.Min_level > curr_level)
                {
                    Max_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[1];

                    //middle one is also locked
                    if (Max_singleMatch.Min_level > curr_level)
                        Max_singleMatch = m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode)[0];
                }

                int min = Min_singleMatch.Buy_in;
                int max = Max_singleMatch.Buy_in;
                List<Contest> contests_current = ContestsController.Instance.GetOnGoingContestsInRange(min, max);

                //holds the data about the singles in this current playing mode - the string is the ID of the contest that this table is participating in;
                int[] tables_contests = new int[3] { -1, -1, -1 };

                int counter = 0;

                foreach (SingleMatch item in m_world.GetSingleMatches(SpadesModelManager.Instance.Playing_mode))
                {
                    SpadesSingleMatch spadesSingleMatch = (SpadesSingleMatch)item;

                    if (item.Is_special || item.Min_level > curr_level)
                        continue;

                    foreach (Contest curr_contest in contests_current)
                    {
                        if (item.Buy_in >= curr_contest.TableFilterData.MinBuyIn && item.Buy_in <= curr_contest.TableFilterData.MaxBuyIn
                            && curr_contest.TableFilterData.SpecialOnly==false 
                            && (curr_contest.TableFilterData as SpadesTableFilterData).IsVariantContained(spadesSingleMatch.Variant)
                            && (curr_contest.TableFilterData as SpadesTableFilterData).IsSubVariantContained(spadesSingleMatch.SubVariant))

                            tables_contests[counter] = curr_contest.Id;
                    }
                    counter++;
                }



                //check for one big hat XXX
                if (tables_contests[0] == tables_contests[1] && tables_contests[0] == tables_contests[2] && tables_contests[0] != -1)
                {
                    BuildAndSetData(3, new Vector3(35f, 0, 0), tables_contests[0]);
                }
                // XXO 
                else if (tables_contests[0] == tables_contests[1] && tables_contests[0] != -1)
                {
                    Vector3 pos = new Vector3(-145f, 0, 0);
                    BuildAndSetData(2, pos, tables_contests[0]);
                    //XXY
                    if (tables_contests[2] != -1)
                        BuildAndSetData(1, new Vector3(m_singles_view[2].GetComponent<RectTransform>().localPosition.x + 214, 0, 0), tables_contests[2]);
                }
                // OXX 
                else if (tables_contests[1] == tables_contests[2] && tables_contests[1] != -1)
                {
                    Vector3 pos = new Vector3(213f, 0, 0);
                    BuildAndSetData(2, pos, tables_contests[1]);
                    //YXX
                    if (tables_contests[0] != -1)
                        BuildAndSetData(1, new Vector3(m_singles_view[0].GetComponent<RectTransform>().localPosition.x + 214, 0, 0), tables_contests[0]);
                }
                else
                {
                    //check all the single cases
                    for (int i = 0; i < 3; i++)
                    {
                        if (tables_contests[i] != -1)
                            BuildAndSetData(1, new Vector3(m_singles_view[i].GetComponent<RectTransform>().localPosition.x+214, 0, 0), tables_contests[i]);
                    }
                }
            }
        }

        internal void RescaleWorldToIpad(float scale_y, float delta_y_panel_pos, float delta_y_buy_in, float delta_y_points, float delta_y_bags, float delta_y_playing_mode_and_lock, float arena_delta_y_panel_pos, float arena_delta_y_buy_in, float arena_delta_y_challenge, float arena_delta_y_gem, float arena_delta_y_gem_image, float delta_y_mission_indication)
        {

            float curr_panel_y = m_singles_view[0].gameObject.GetComponent<RectTransform>().localPosition.y;

            //setting the singles
            for (int i = 0; i < m_singles_view.Length; i++)
            {
                m_singles_view[i].Panel_BG.GetComponent<RectTransform>().localScale = new Vector3(1f, scale_y, 1f);
                AddDeltaY(delta_y_panel_pos, m_singles_view[i].gameObject);
                AddDeltaY(delta_y_buy_in, m_singles_view[i].Buy_in_object);
                AddDeltaY(delta_y_points, m_singles_view[i].Points_text.gameObject);
                AddDeltaY(delta_y_bags, m_singles_view[i].Bags_object);
                AddDeltaY(delta_y_playing_mode_and_lock, m_singles_view[i].Playing_mode_text.gameObject);
                AddDeltaY(delta_y_mission_indication, m_singles_view[i].Mission_indication);
            }

            //setting the arena
            m_arena_view.Panel_BG.GetComponent<RectTransform>().localScale = new Vector3(1f, scale_y, 1f);


            m_arena_view.BG_Image.GetComponent<RectTransform>().localScale = new Vector3(1f, scale_y, 1f);
            m_arena_view.BG_Mask.GetComponent<RectTransform>().localScale = new Vector3(1f, scale_y, 1f);

            AddDeltaY(arena_delta_y_panel_pos, m_arena_view.gameObject);
            AddDeltaY(arena_delta_y_buy_in, m_arena_view.Buy_in_object);
            AddDeltaY(arena_delta_y_challenge, m_arena_view.Arena_challenge_text.gameObject);
            AddDeltaY(arena_delta_y_gem, m_arena_view.Gem_holder);
            AddDeltaY(arena_delta_y_gem_image, m_arena_view.Gem_BG.gameObject);


            AddDeltaY(arena_delta_y_gem_image, m_arena_view.TinyGems[0].gameObject);
            AddDeltaY(arena_delta_y_gem_image, m_arena_view.TinyGems[1].gameObject);
            AddDeltaY(arena_delta_y_gem_image, m_arena_view.TinyGems[2].gameObject);


            AddDeltaY(-15, m_contest_indication_holder);

        }

        private void AddDeltaY(float delta, GameObject go)
        {
            RectTransform temp_rt = go.GetComponent<RectTransform>();
            go.GetComponent<RectTransform>().localPosition = new Vector3(temp_rt.localPosition.x, temp_rt.localPosition.y + delta, temp_rt.localPosition.z);
        }


        private void ModeChanged()
        {
            bool is_curr_world = false;
            bool is_solo = SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo;

            int pos = ModelManager.Instance.Worlds.FindIndex(w => w == m_world);
            if (pos == m_lobby_view.GetSwipe().Screen_index - 1)//there is an offset of index between Step_swipe - and the world indexes
                is_curr_world = true;

            RotatePanels(is_solo, UpdateContestsHats, !is_curr_world);

            StartCoroutine(DeleteContestIndications(is_curr_world)); // Animate only if current world

        }



        public void SetWorldPanels()
        {
            PlayingMode pm = SpadesModelManager.Instance.Playing_mode;

            //public void SetFullArena(Arena arena, Sprite bg_image_unlocked, Sprite bg_image_locked, GameObject gem_object, Sprite gem)
            m_arena_view.World_item_view = this;

            // PromoRoom Changes
            if (IsRegularRoom())
            {
                m_arena_view.SetFullArena(m_world.GetArena(pm), m_lobby_view.Arena_panels[m_world.Index], m_lobby_view.Gems_objects[m_world.Index], m_lobby_view.Gems_lobby_sprite[m_world.Index]);

                for (int i = 0; i < 3; i++)
                    m_singles_view[i].SetInitialData(m_world.GetSingleMatches(pm)[i], m_lobby_view.Locked_single[m_world.Index], m_lobby_view.Single_panels[m_world.Index], this);
            }
        }

        // PromoRoom Changes
        public bool IsRegularRoom()
        {
            return m_world.WorldType == WorldTypes.Regular;
        }

        public void UpdateAllItemsDisplay()
        {
            if (m_world.WorldType == WorldTypes.Regular)
            {
                foreach (SingleItemView item in m_singles_view)
                {
                    item.UpdateDisplay();
                }
                m_arena_view.UpdateDisplay();
            }

            

        }

        private float GetPanelFlipAngle(int panel_index)
        {
            if (panel_index == 0)
                return 65.35f;
            else if (panel_index == 1)
                return 83.35f;
            else if (panel_index == 2)
                return 102.8f;
            else if (panel_index == 3)
                return 124.7f;
            else
                return 0;
        }



        public void StartPulsing(int singleItemIndex)
        {
            iTween.ScaleTo(m_singles_view[singleItemIndex].gameObject, iTween.Hash("x", 1.05, "y", 1.05, "easeType", iTween.EaseType.easeInCubic, "time", .5, "looptype", "pingPong"));

        }

        public void StopPulsing(int singleItemIndex)
        {
            iTween.Stop(m_singles_view[singleItemIndex].gameObject);

        }

   
        public void PanelBGDown()
        {
            LobbyController.Instance.Lobby_view.GetSwipe().StartSwipe();
        }

        public void PanelBGUp()
        {
            LobbyController.Instance.Lobby_view.GetSwipe().EndSwipe();
            
        }

        public bool Is_rotating
        {
            get
            {
                return m_is_rotating;
            }
        }

        public ArenaItemView Arena_view
        {
            get
            {
                return m_arena_view;
            }
        }

        public SingleItemView[] Singles_view { get => m_singles_view; }
        public SpadesWorld World { get => m_world;}
    }
}
