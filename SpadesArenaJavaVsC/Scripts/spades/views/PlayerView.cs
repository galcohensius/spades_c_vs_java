﻿using System.Collections;
using System.Collections.Generic;
using spades.models;
using spades.controllers;
using common;
using System;
using common.utils;
using cardGames.models;
using spades.views.popups;
using cardGames.views;
using cardGames.controllers;
using cardGames.comm;
using common.controllers;

namespace spades.views
{
    /// <summary>
    /// Player view - visualization of the player - holds the references to the card views
    /// </summary>
    public class PlayerView : MonoBehaviour
    {

        
        private const double CARD_FLIP_TIME = .1;
        [SerializeField] MAvatarView m_dynamicAvatarView = null;
        [SerializeField] PlayerIndicationView m_player_indication = null;
        [SerializeField] GameObject m_player_type_indication = null;
        [SerializeField] GameObject m_hand = null;
        [SerializeField] GameObject m_playerName = null;
        [SerializeField] TMP_Text m_playerNameText = null;

        //link to my player model   
        Player m_playerModel;
        List<GameObject> m_cards_view = new List<GameObject>();

        protected static Dictionary<int, Player> m_player_cache = new Dictionary<int, Player>();

        protected NewPlayerProfilePopup m_oppoProfilePopup;


        private void OnEnable()
        {
            bool is_player = false;

            if (m_player_indication.gameObject.transform.parent.gameObject.name == "PlayerSouth")
                is_player = true;

            m_playerName.GetComponent<CanvasGroup>().alpha = 0;

            m_player_indication.SetGameMode(SpadesModelManager.Instance.GetActiveMatch().GetMatch().Playing_mode == PlayingMode.Solo,is_player);
            m_player_indication.SetTimer(0);
        }

        public void SetAvatarModel(MAvatarModel avatarModel)
        {
            m_dynamicAvatarView.SetAvatarModel(avatarModel);
        }

        /// <summary>
        /// Sets the model and creates the events
        /// </summary>
        /// <param name="player">Player.</param>
        public void SetModel(Player player)
        {
            // Remove old events if exists
            if (m_playerModel != null)
            {
                m_playerModel.OnTakesChange -= HandleTakesChange;
                m_playerModel.OnCardRemoved -= HandleCardRemoved;
            }


            m_playerModel = player;

            // Register on new events
            m_playerModel.OnTakesChange += HandleTakesChange;
            m_playerModel.OnCardRemoved += HandleCardRemoved;

            m_playerNameText.text = player.GetMAvatarModel().NickName;
            TMPBidiHelper.MakeRTL(m_playerNameText);
        }

        public void SetCardState(bool enabled, int index)
        {
            m_cards_view[index].GetComponent<CardView>().SetCardButton(enabled);
        }

        /// <summary>
        /// Enables specific playable cards
        /// </summary>
        public void EnableCards(CardsList playableCards)
        {
            if (playableCards == null)
            {
                // All cards are enabled
                foreach (var card_view in m_cards_view)
                {
                    card_view.GetComponent<CardView>().SetCardButton(true);
                }
            }
            else
            {

                foreach (var card_view_go in m_cards_view)
                {
                    CardView cardView = card_view_go.GetComponent<CardView>();
                    // If the model exists in the playable list, set to enable
                    if (playableCards.Contains(cardView.GetCardModel()))
                    {
                        cardView.SetCardButton(true);
                    }
                    else
                    {
                        cardView.SetCardButton(false);
                    }
                }
            }
        }

        public List<GameObject> GetCardsList()
        {
            return m_cards_view;
        }





        public void ShowHideTimer(bool show)
        {
            m_player_indication.TogglePlayerHighlight(show);
        }

        /// <summary>
        /// hides hand in invisible mode
        /// </summary>
        public void InvisibleMode()
        {
            //m_hand_canvas.alpha=0;
            m_hand.SetActive(false);
        }



        public void ShowBidBubbleAndSign()
        {
            CardGamesSoundsController.Instance.ChatBubble();

            string bid_text;
            string sign_text;

            if (m_playerModel.GetBlind())
            {
                bid_text = "Blind nil!";
                sign_text = "Blind \nNil";
            }
            else if (m_playerModel.GetBids() == 0)
            {
                bid_text = "I bid nil";
                sign_text = "Nil";
            }
            else
            {
                bid_text = "I bid " + m_playerModel.GetBids().ToString();
                sign_text = m_playerModel.GetBids().ToString();
            }

            m_dynamicAvatarView.ShowChat(bid_text, 48);
            m_player_indication.SetBidAndTakes(m_playerModel.GetBidsStr(), m_playerModel.GetTakes());

          //  m_dynamicAvatarView.PlayBidChatBubble(bid_text);

        }

        public void HideBidSpeechBubble()
        {
            //m_dynamicAvatarView.HideMessage();
        }

        /// <summary>
        /// Deletes the card both from hand and also visually.
        /// </summary>
        /// <param name="card_index">Card index.</param>
        public void DeleteCard(int card_index)
        {
            m_cards_view[card_index].GetComponent<CardView>().DeleteCard();
            m_cards_view.RemoveAt(card_index);
        }

        /// <summary>
        /// Deletes all cards - used for restart
        /// </summary>
        public void DeleteAllCards()
        {
            for (int i = 0; i < m_cards_view.Count; i++)
                m_cards_view[i].GetComponent<CardView>().DeleteCard();

            m_cards_view.Clear();
        }

        public IEnumerator AddDummyCards(float total_time, CardsList cards)
        {

            int total_cards_dealt = 0;
            float delay = total_time / 13f;
            float start_time = Time.time;


            while (Time.time - start_time < total_time)
            {
                yield return new WaitForSeconds(delay);


                if (total_cards_dealt < TableView.NUM_CARDS_TO_DEAL)
                    AddSingleCard(cards[total_cards_dealt]);

                total_cards_dealt++;
            }

            while (total_cards_dealt < TableView.NUM_CARDS_TO_DEAL)
            {

                AddSingleCard(cards[total_cards_dealt]);

                total_cards_dealt++;
            }

        }


        /// <summary>
        /// Adds cards to player's hand
        /// </summary>
        public void AddCards(CardsList cards, bool enabled = false)
        {
            foreach (var card in cards)
            {
                m_cards_view.Add((GameObject)Instantiate(ManagerView.Instance.Card_prefab));    //adding a card and setting its image
                m_cards_view[m_cards_view.Count - 1].transform.SetParent(m_hand.transform);
                m_cards_view[m_cards_view.Count - 1].transform.SetAsFirstSibling();
                m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);    //i do this directly - as there is no later need
                //m_cards_view[m_cards_view.Count - 1].transform.Find("Card").GetComponent<RectTransform>().localPosition = new Vector3(8f, 2f, 0f);
                m_cards_view[m_cards_view.Count - 1].GetComponent<CardView>().SetCardModel(card);
                m_cards_view[m_cards_view.Count - 1].GetComponent<Collider2D>().enabled = enabled;
                m_cards_view[m_cards_view.Count - 1].GetComponent<ButtonsOverView>().enabled = enabled;

            }
        }

        public void AddSingleCard(Card card)
        {
            m_cards_view.Add((GameObject)Instantiate(ManagerView.Instance.Card_prefab));
            //adding a card and setting its image
            m_cards_view[m_cards_view.Count - 1].GetComponent<ButtonsOverView>().enabled = false;
            m_cards_view[m_cards_view.Count - 1].transform.SetParent(m_hand.transform);
            m_cards_view[m_cards_view.Count - 1].transform.SetAsFirstSibling();
            m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0); //i do this directly - as there is no later need
            m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localEulerAngles = new Vector3(0f, 0f, 0); //i do this directly - as there is no later need

            //m_cards_view[m_cards_view.Count - 1].transform.Find("Card").GetComponent<RectTransform>().localPosition = new Vector3(8f, 2f, 0f);
            m_cards_view[m_cards_view.Count - 1].GetComponent<RectTransform>().localScale = new Vector3(1.1f, 1.1f, 1); //i do this directly - as there is no later need
            m_cards_view[m_cards_view.Count - 1].GetComponent<CardView>().SetCardModel(card);
            m_cards_view[m_cards_view.Count - 1].GetComponent<Collider2D>().enabled = enabled;
            m_cards_view[m_cards_view.Count - 1].GetComponent<ButtonsOverView>().enabled = enabled;
            MoveCardsToPositions(true,true);
        }



        public void SetCardsSize(Vector3 size)
        {
            for (int i = 0; i < m_cards_view.Count; i++)
                m_cards_view[i].GetComponent<CardView>().SetScale(size);
        }


        /// <summary>
        /// Sets the hand position offset from the player center
        /// </summary>
        /// <param name="pos">Position.</param>
        public void SetHandPosition(Vector3 pos)
        {
            m_hand.GetComponent<RectTransform>().localPosition = pos;
        }



        /// <summary>
        /// flips all the cards.
        /// </summary>
        public void RevealCards(bool force_immediate_reveal, bool play_sound)//this flag is for when we have a cheat showing the bot cards - but not having their sfx play
        {
            if (play_sound)
                CardGamesSoundsController.Instance.CardsFlip();


            StartCoroutine(RevealCardsDelay(force_immediate_reveal));
        }

        IEnumerator RevealCardsDelay(bool force_immediate_reveal)
        {


            if (!force_immediate_reveal)
            {
                while (m_cards_view.Count != 13)
                    yield return new WaitForSecondsRealtime(0.3f);
            }
            else
            {
                //some short delay for the cards to be created so that all of them will spin properly
                yield return new WaitForSecondsRealtime(0.25f);
            }


            float angle = 104.5f;

            for (int i = m_cards_view.Count - 1; i > -1; i--)
            {
                if (m_cards_view!=null && m_cards_view[i] != null)
                {
                    iTween.RotateTo(m_cards_view[i].transform.GetChild(0).gameObject, iTween.Hash("y", angle, "easeType", "linear", "islocal", true, "time", CARD_FLIP_TIME));
                    iTween.RotateTo(m_cards_view[i].transform.GetChild(1).gameObject, iTween.Hash("y", angle, "easeType", "linear", "islocal", true, "time", CARD_FLIP_TIME, "onComplete", "Rotate2", "onCompleteTarget", this.gameObject, "oncompleteparams", i));
                }
                    

                yield return new WaitForSeconds(.07f);

                angle -= 2.23f;
            }
        }


        void Rotate2(int i)
        {
            
            //iTween.MoveBy(m_cards_view[i], iTween.Hash("x", -50, "easeType", "linear", "time", .1));

            m_cards_view[i].GetComponent<CardView>().RevealCard();
            m_cards_view[i].GetComponent<RectTransform>().localScale = new Vector3(3.5f, 3.5f, 0);

            RectTransform card_rect2 = m_cards_view[i].transform.GetChild(1).gameObject.GetComponent<RectTransform>();
            card_rect2.localScale = new Vector3(-1, 1, 0);

            m_cards_view[i].transform.SetAsFirstSibling();
            //m_cards_view[i].transform.Find("Card").GetComponent<RectTransform>().localPosition = new Vector3(8f, 2f, 0);
            iTween.RotateTo(m_cards_view[i].transform.GetChild(1).gameObject, iTween.Hash("y", 180, "easeType", "linear", "islocal", true, "time", CARD_FLIP_TIME));
            iTween.RotateTo(m_cards_view[i].transform.GetChild(0).gameObject, iTween.Hash("y", 180, "easeType", "linear", "islocal", true, "time", CARD_FLIP_TIME, "onComplete", "RotateDone", "onCompleteTarget", this.gameObject, "oncompleteparams", i));

        }

        void RotateDone(int i)
        {
            RectTransform card_rect = m_cards_view[i].GetComponent<RectTransform>();
            card_rect.localEulerAngles = new Vector3(0, 0, card_rect.localEulerAngles.z);

            RectTransform card_rect2 = m_cards_view[i].transform.GetChild(1).gameObject.GetComponent<RectTransform>();
            card_rect2.localEulerAngles = new Vector3(0, 0, card_rect2.localEulerAngles.z);
            card_rect2.localScale = new Vector3(1, 1, 0);

            //m_cards_view[i].GetComponent<RectTransform>().localScale = new Vector3(3.5f, 3.5f, 0);
        }

        public int GetPlayerImageIndex() => m_playerModel.GetTablePos();

        /// <summary>
        /// Gets the index of the selected card when the player throws a card
        /// </summary>
        /// <returns>The card index.</returns>
        /// <param name="selected_card">Selected card.</param>
        public int GetCardIndex(GameObject selected_card)
        {
            return m_cards_view.FindIndex(GameObject => GameObject.gameObject == selected_card); // index of the card last clicked
        }

        public GameObject GetCardObject(Card card)
        {
            return m_cards_view.Find(card_object =>
                card_object.GetComponent<CardView>().GetCardModel() == card); // index of the card last clicked
        }

        /// <summary> 
        /// Handles the card removed.
        /// </summary>
        /// <param name="card_index">Card index.</param>
        void HandleCardRemoved(int card_index)
        {
            DeleteCard(card_index);
        }




        /// <summary>
        /// Handles the takes change.
        /// </summary>
        /// <param name="takes">Takes.</param>
        void HandleTakesChange(int takes)
        {
            if (takes > 0)
                StartCoroutine(ShowBidsChangeDelay(takes));
        }

        IEnumerator ShowBidsChangeDelay(int takes)
        {
            yield return new WaitForSeconds(0.75f);
            m_player_indication.SetBidAndTakes(m_playerModel.GetBidsStr(), m_playerModel.GetTakes());
        }

        /// <summary>
        /// sorts the hand in a fan like shape
        /// </summary>
        ///

        public void MoveCardsToPositions(bool animate = true, bool is_deal=false)
        {
            Vector2 center = new Vector2(0, 0);

            float radius = 4500f;//this is the diameter of the big circle

            int number_of_cards = m_cards_view.Count;

            float spread = 20f;//this is the part of the huge circle that we align our cards on - this is in angle units

            //this makes the hand smalled when we have more cards

            if(is_deal)
            {
                spread = number_of_cards * 20/13;
                
            }   
            else 
            {
                if (number_of_cards <= 2)
                    spread = 3f;
                else if (number_of_cards < 10)
                    spread = number_of_cards * 2;
                else
                    spread = 20f;
            }
                       

            for (int i = 0; i < number_of_cards; i++)
            {
                float new_y_pos = 0, new_x_pos = 0, new_curve = 0;

                if (m_cards_view[i] != null)
                    m_cards_view[i].name = "Card " + i;

                float curr_angle = 0;

                if (number_of_cards > 1)//to avoid division by 0
                    curr_angle = (spread / (number_of_cards - 1)) * i - spread / 2;

                curr_angle += 90f;

                new_y_pos = Mathf.Sin(curr_angle * Mathf.Deg2Rad) * radius - radius;

                new_x_pos = Mathf.Cos(curr_angle * Mathf.Deg2Rad) * radius;

                new_curve = curr_angle - 90f;

                try
                {
                    //the Z depth is to improve the selection of correct card
                    Vector3 endPos = new Vector3(new_x_pos, new_y_pos, i * 0.1f);
                    StartCoroutine(ArrangeHandAnimation(m_cards_view[number_of_cards - 1 - i].GetComponent<CardView>(), endPos, new_curve, 0.2f, animate));
                }
                catch (Exception e)
                {
                    // This coroutine sometimes throws an exception
                    LoggerController.Instance.LogError(LoggerController.Module.Game, "MoveCardsToPositions: " + e);
                }

            }
        }



        /// <summary>
        /// moves a card to a new location with minimal rotation change
        /// </summary>
        /// <returns>The hand animation.</returns>
        /// <param name="index">Index.</param>
        /// <param name="endPosition">End position.</param>
        /// <param name="endRotation">End rotation.</param>
        public IEnumerator ArrangeHandAnimation(CardView card, Vector3 endPosition, float endRotation, float move_time, bool animate = true)  //used to re arrange the ahnd after a card was thrown
        {
            float f_start_time = Time.time;
            float time_to_move = move_time;         //card contraction time

            Vector3 temp_position;
            Vector3 temp_scale;
            Vector3 temp_rotation;
            CardView temp_cardview;

            temp_cardview = card;

            float curr_angle = temp_cardview.GetRotation().z;
            float curr_angle_x = temp_cardview.GetRotation().x;


            float start_pos_x = temp_cardview.GetPosition().x;
            float start_pos_y = temp_cardview.GetPosition().y;
            float start_pos_z = temp_cardview.GetPosition().z;

            float start_scale = temp_cardview.GetScale();


            float target_x = endPosition.x;
            float target_y = endPosition.y;
            float target_z = endPosition.z;

            if (animate)
            {

                float time_counter = 0;
                while (time_counter < 1f)
                {
                    float vel2 = 0f;

                    float vel3 = 2f;

                    time_counter = (Time.time - f_start_time) / time_to_move;

                    temp_position = new Vector3(Mathf.SmoothStep(start_pos_x, target_x, time_counter), Mathf.SmoothStep(start_pos_y, target_y, time_counter), Mathf.SmoothStep(start_pos_z, target_z, time_counter));

                    temp_scale = new Vector3(Mathf.SmoothStep(start_scale, 3.5f, time_counter), Mathf.SmoothStep(start_scale, 3.5f, time_counter), 1f);

                    temp_cardview.SetScale(temp_scale);

                    temp_cardview.SetPosition(temp_position);

                    curr_angle = Mathf.SmoothDampAngle(curr_angle, endRotation, ref vel2, 0.1f);

                    curr_angle_x = Mathf.SmoothDampAngle(curr_angle_x, 0, ref vel3, 0.1f);

                    temp_rotation = new Vector3(curr_angle_x, 0, curr_angle);

                    temp_cardview.SetRotation(temp_rotation);

                    yield return null;
                }

            }

            temp_cardview.SetRotation(new Vector3(0, 0, endRotation));               //backup only to ensure ending in the right location even with timing issues
            temp_cardview.SetPosition(new Vector3(target_x, target_y, target_z));
            temp_cardview.SetScale(new Vector3(3.5f, 3.5f, 1f));

        }

       
        public IEnumerator PlayerThrowCardAnimation(Card card, Action anim_done_callback)
        {

            RoundController.Instance.Card_thrown_processed = false;

            CardGamesSoundsController.Instance.CardThrown();
            float f_start_time = Time.time;
            float time_to_move = TableView.CARD_THROW_TIME;         //card animation time
            Vector3 temp_position;


            GameObject new_card;

            CardView cardview = GetCardObject(card).GetComponent<CardView>();

            cardview.transform.SetParent(ManagerView.Instance.Table.GetComponent<TableView>().Thrown_cards_object.transform);

            new_card = ((GameObject)Instantiate(ManagerView.Instance.Card_prefab));  //adding a card and setting its image

            new_card.transform.SetParent(ManagerView.Instance.Table.GetComponent<TableView>().Thrown_cards_object.transform);
            new_card.GetComponent<RectTransform>().localScale = cardview.GetComponent<RectTransform>().localScale;
            new_card.GetComponent<RectTransform>().localPosition = cardview.GetComponent<RectTransform>().localPosition;


            new_card.GetComponent<ButtonsOverView>().enabled = false;
            Vector3 temp_rotation = new Vector3(0, cardview.GetComponent<RectTransform>().localEulerAngles.y, cardview.GetComponent<RectTransform>().localEulerAngles.z);
            new_card.GetComponent<RectTransform>().localEulerAngles = temp_rotation;

            new_card.GetComponent<CardView>().SetCardModel(cardview.GetComponent<CardView>().GetCardModel());
            new_card.GetComponent<CardView>().RevealCard();
            new_card.GetComponent<CardView>().SetInteractable(false);
            new_card.name = "Card" + 3;

            new_card.GetComponent<CardView>().ShowHideShadow(false);


            cardview.DeleteCard();
            m_cards_view.Remove(cardview.gameObject);

            MoveCardsToPositions();

            float start_pos_x = cardview.GetPosition().x;
            float start_pos_y = cardview.GetPosition().y;
            float start_pos_z = cardview.GetPosition().z;


            float start_scale = cardview.GetScale();

            float target_x, target_y, target_z;
            //player location offset
            target_x = 0f;
            target_y = -200f;
            target_z = 0f;

            float curr_angle_z = cardview.GetRotation().z;
            float end_angle_z = 0f;


            float startAngle_z = curr_angle_z;


            float curr_angle_x = cardview.GetRotation().x;
            float end_angle_x = 0f;//curr_angle_x;
            float startAngle_x = curr_angle_x;



            new_card.GetComponent<RectTransform>().localPosition = new Vector3(start_pos_x, start_pos_y, start_pos_z);
            new_card.GetComponent<RectTransform>().localEulerAngles = new Vector3(startAngle_x, 0, startAngle_z);
            new_card.GetComponent<RectTransform>().localScale = new Vector3(start_scale, start_scale, start_scale);

            new_card.GetComponent<CardView>().ShowHideShadow(false);

            float mid_x = (start_pos_x + target_x) / 2;
            float mid_y = target_y + 50f;
            float mid_z = -150f;
            Vector3[] path = new Vector3[3];
            path[0] = new Vector3(start_pos_x, start_pos_y, start_pos_z);
            path[1] = new Vector3(mid_x, mid_y, mid_z);
            path[2] = new Vector3(target_x, target_y, target_z);

            iTween.MoveTo(new_card, iTween.Hash("path", path, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.easeOutCirc, "time", time_to_move));

            iTween.RotateTo(new_card, iTween.Hash("z", end_angle_z, "x", end_angle_x, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", time_to_move));

            iTween.ScaleTo(new_card, iTween.Hash("y", 2.45f, "x", 2.45f, "z", 2.45f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", time_to_move));

            yield return new WaitForSeconds(time_to_move);



            temp_position = new Vector3(target_x, target_y, target_z);

            if (new_card != null)
            {
                new_card.GetComponent<CardView>().SetPosition(temp_position);
                //new_card.GetComponent<CardView> ().DeleteCard ();
            }

            RoundController.Instance.Cards_finished_throw_anim++;
            anim_done_callback();

        }


        public IEnumerator PlayerThrowCardForBreakingSpades(Card card, Action anim_done_callback)
        {
            //Time.timeScale = 0.5f;

            float f_start_time = Time.time;
            float time_to_move = TableView.CARD_THROW_TIME; //card animation time


            CardView cardview = GetCardObject(card).GetComponent<CardView>();

            float start_pos_x = cardview.GetPosition().x;
            float start_pos_y = cardview.GetPosition().y;
            float start_pos_z = cardview.GetPosition().z;


            float start_scale = cardview.GetScale();

            float target_x, target_y;
            //player location offset
            target_x = 0f;
            target_y = 451f;

            float curr_angle_z = cardview.GetRotation().z;
            float end_angle_z = 0;


            float startAngle_z = curr_angle_z;


            float curr_angle_x = cardview.GetRotation().x;
            float end_angle_x = curr_angle_x;
            float startAngle_x = curr_angle_x;


            end_angle_x = -10f;

            cardview.GetComponent<RectTransform>().localPosition = new Vector3(start_pos_x, start_pos_y, start_pos_z);
            cardview.GetComponent<RectTransform>().localEulerAngles = new Vector3(startAngle_x, 0, startAngle_z);
            cardview.GetComponent<RectTransform>().localScale = new Vector3(start_scale, start_scale, start_scale);

            //temp_cardview.GetComponent<Shadow> ().enabled = false;

            cardview.GetComponent<CardView>().ShowHideShadow(false);

            float mid_x = (start_pos_x + target_x) / 2;
            float mid_y = (target_y - start_pos_y) / 3 * 2;
            Vector3[] path = new Vector3[3];
            path[0] = new Vector3(start_pos_x, start_pos_y, start_pos_z);
            path[1] = new Vector3(mid_x, mid_y, 150f);
            path[2] = new Vector3(target_x, target_y, 0);

            ManagerView.Instance.Table.GetComponent<TableView>().Spades_broken_card[3].SetCardModel(card);
            ManagerView.Instance.Table.GetComponent<TableView>().Spades_broken_card[3].RevealCard();
            ManagerView.Instance.Table.GetComponent<TableView>().Spades_broken_card[3].ShowHideShadow(false);
            ManagerView.Instance.Table.GetComponent<TableView>().Game_table_object.GetComponent<Animator>().SetTrigger("spades_broken");

            iTween.MoveTo(cardview.GetGameObject(), iTween.Hash("x", target_x, "y", target_y, "islocal", true, "movetopath", false, "easeType", iTween.EaseType.easeInCubic, "time", time_to_move));
            iTween.RotateTo(cardview.GetGameObject(), iTween.Hash("z", end_angle_z, "islocal", true, "easeType", iTween.EaseType.easeInCubic, "time", time_to_move));
            //iTween.MoveTo(cardview.GetGameObject(), iTween.Hash("path", path, "islocal", true, "movetopath", false, "easeType", "linear", "time", time_to_move));
            //iTween.RotateTo(cardview.GetGameObject(), iTween.Hash("z", end_angle_z / 2, "x", -end_angle_x * 3, "islocal", true, "easeType", "easeOutSine", "time", time_to_move / 3));
            //iTween.RotateTo(cardview.GetGameObject(), iTween.Hash("z", end_angle_z, "x", -45, "islocal", true, "easeType", "easeOutSine", "delay", time_to_move / 3, "time", 2 * time_to_move / 3));

            //iTween.ScaleTo(cardview.GetGameObject(), iTween.Hash("y", end_scale, "x", end_scale, "z", end_scale, "islocal", true, "easeType", "easeOutSine", "time", time_to_move));

            yield return new WaitForSeconds(time_to_move);

            cardview.DeleteCard();
            m_cards_view.Remove(cardview.gameObject);
            MoveCardsToPositions();

            ManagerView.Instance.Table.GetComponent<TableView>().Game_table_object.GetComponent<Animator>().SetTrigger("card_flight");

            yield return new WaitForSeconds(1f);

            ManagerView.Instance.Table.GetComponent<TableView>().PlaySpadesBroken();

            yield return new WaitForSeconds(0.3f);

            Vector3 table_pos_vector = new Vector3(0, -200f, 0);
            Vector3 table_rot_vector = new Vector3(end_angle_x, 0, end_angle_z);

            ManagerView.Instance.Table.GetComponent<TableView>().AddCardToTable(table_pos_vector, table_rot_vector, card, 3);

            yield return new WaitForSeconds(2f);

            RoundController.Instance.Cards_finished_throw_anim++;

            anim_done_callback();

            ChatAIController.Instance.TriggerAIChatEvent((int)BotResponseTrigger.PlayerBreaksSpade, SpadesModelManager.Instance.Playing_mode);
        }

        public void ShowPlayerName()
        {
            iTween.ValueTo(m_playerName, iTween.Hash("from", 0, "to", 1, "time", .5f, "onupdate", "UpdateNameAlpha", "Onupdatetarget", this.gameObject));
            iTween.ValueTo(m_playerName, iTween.Hash("from", 1, "to", 0, "time", .3f, "onupdate", "UpdateNameAlpha", "Onupdatetarget", this.gameObject, "delay", 3));
        }

        public void UpdateNameAlpha(float value)
        {
            m_playerName.GetComponent<CanvasGroup>().alpha = value;
        }


        public void PlayerViewClicked()
        {
            if (m_playerModel.GetTablePos() == 3)
            {
                SpadesPopupManager.Instance.ShowProfilePopup(true);
                return;
            }


            m_oppoProfilePopup = SpadesPopupManager.Instance.ShowOtherProfilePopup();


            int tablePos = m_playerModel.GetTablePos();
            if (m_player_cache.ContainsKey(tablePos))
            {
                OnPlayerDataArrived(true, m_player_cache[tablePos].User.Stats as UserStats, m_player_cache[tablePos].User.GetXPInLevel(),
                                    m_player_cache[tablePos].User.GetLevel(), m_player_cache[tablePos].User.GetLevelTotalXP(),
                                    m_player_cache[tablePos].User.GetCoins());
            }
            else
            {
                if (m_playerModel.Player_Type == Player.PlayerType.Bot || m_playerModel.Player_Type == Player.PlayerType.Zombie)
                {
                    SpadesActiveMatch match = SpadesModelManager.Instance.GetActiveMatch();

                    string key = "s" + (match.GetMatch() as Match).GetID();
                    if (!match.IsSingleMatch)
                    {
                        key = "a" + (match.GetMatch() as Match).GetID();
                    }

                    int botLevelByMapping = 1;
                    if (ModelManager.unlockLevelMap!=null && ModelManager.unlockLevelMap.ContainsKey(key))
                    {
                        botLevelByMapping = ModelManager.unlockLevelMap[key];
                    }

                    int botLevel = Math.Max((match.GetMatch() as Match).Min_level, botLevelByMapping);

                    CardGamesCommManager.Instance.RequestUserStats(botLevel, 0, OnPlayerDataArrived);
                }
                else
                {
                    if (m_playerModel.Player_Type == Player.PlayerType.Human)
                    {
                        CardGamesCommManager.Instance.RequestUserStats(-1, m_playerModel.User.GetID(), OnPlayerDataArrived);
                    }
                }
            }
        }

        //user_stats must be of SpadesUserStats? If so then this is wrong and needs investigation
        protected void OnPlayerDataArrived(bool success, UserStats user_stats, int xp, int level, int total_level_xp, int coins_balance)
        {
            if (!success)
            {
                return;
            }

            m_playerModel.User.SetLevel(level, total_level_xp, xp);
            m_playerModel.User.Stats = user_stats;
            m_playerModel.User.SetCoins(coins_balance);

            if (!m_player_cache.ContainsKey(m_playerModel.GetTablePos()))
            {
                m_player_cache.Add(m_playerModel.GetTablePos(), m_playerModel);
            }

            m_oppoProfilePopup.SetData(true, m_playerModel.User);
        }

        public static void ClearPlayerCache(bool skipPartner=false)
        {
            m_player_cache.Remove(0);
            m_player_cache.Remove(2);
            if (!skipPartner)
                m_player_cache.Remove(1);
        }


        internal void ResetPlayerIndications()
        {
            m_player_indication.SetInitialBidsAndTakes();

            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch();

            if (activeMatch.GetMatch().Playing_mode == PlayingMode.Solo)
            {
                int bags = 0;
                int points = 0;
                SpadesRoundResults roundResults = activeMatch.GetLastRoundResults();
                if (roundResults != null)
                {
                    bags = roundResults.Players_round_results[m_playerModel.GetTablePos()].Bags_total;
                    points = activeMatch.GetCalculatedTotalResults((SpadesRoundResults.Positions)m_playerModel.GetTablePos());

                }
                m_player_indication.SetPointsAndBags(points, bags);
            }
        }

        public MAvatarView DynamicAvatarView
        {
            get
            {
                return m_dynamicAvatarView;
            }
        }

        public GameObject Hand
        {
            get
            {
                return m_hand;
            }
        }


        public PlayerIndicationView Player_indication
        {
            get
            {
                return m_player_indication;
            }

        }
    }
}
