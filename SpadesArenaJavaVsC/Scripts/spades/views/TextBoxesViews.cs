﻿using spades.models;
using cardGames.models;

namespace spades.views
{
    public class TextBoxesViews : MonoBehaviour
    {


        [SerializeField] Text ns_total_pts = null;
        [SerializeField] Text ew_total_pts = null;

        [SerializeField] Text m_p0_score_text = null;
        [SerializeField] Text m_p1_score_text = null;
        [SerializeField] Text m_p2_score_text = null;
        [SerializeField] Text m_p3_score_text = null;

        [SerializeField] Text m_p0_name_text = null;
        [SerializeField] Text m_p1_name_text = null;
        [SerializeField] Text m_p2_name_text = null;
        [SerializeField] Text m_p3_name_text = null;

        [SerializeField] GameObject m_partners = null;
        [SerializeField] GameObject m_solo = null;



        public void SetData(SpadesActiveMatch active_match)
        {
            ISpadesMatch match = active_match.GetMatch() as ISpadesMatch;

            if (match.Playing_mode == PlayingMode.Partners)
            {
                m_solo.SetActive(false);
                m_partners.SetActive(true);

                ns_total_pts.text = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions.North_South)).ToString();
                ew_total_pts.text = active_match.GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East).ToString();
            }
            else
            {

                //find winner to sort boxes by winner

                int[] totals = new int[4];
                int[] order = new int[4];
                int temp = 0;
                bool notDone = true;

                for (int i = 0; i < 4; i++)
                {
                    order[i] = i;
                    totals[i] = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)i);
                }

                while (notDone)
                {
                    notDone = false;
                    for (int i = 0; i < 3; i++)
                    {
                        if (totals[i] < totals[i + 1])
                        {
                            notDone = true;
                            temp = totals[i];
                            totals[i] = totals[i + 1];
                            totals[i + 1] = temp;

                            temp = order[i];
                            order[i] = order[i + 1];
                            order[i + 1] = temp;
                        }
                    }
                }


                m_solo.SetActive(true);
                m_partners.SetActive(false);

                m_p0_name_text.text = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(order[0]).GetName();
                m_p1_name_text.text = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(order[1]).GetName();
                m_p2_name_text.text = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(order[2]).GetName();
                m_p3_name_text.text = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(order[3]).GetName();


                m_p0_score_text.text = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)order[0]).ToString();
                m_p1_score_text.text = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)order[1]).ToString();
                m_p2_score_text.text = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)order[2]).ToString();
                m_p3_score_text.text = active_match.GetCalculatedTotalResults((SpadesRoundResults.Positions)order[3]).ToString();

                m_p0_name_text.color = Color.red;
                m_p1_name_text.color = Color.red;
                m_p2_name_text.color = Color.red;
                m_p3_name_text.color = Color.red;

                if (order[0] == 3)
                    m_p0_name_text.color = Color.blue;
                else if (order[1] == 3)
                    m_p1_name_text.color = Color.blue;
                else if (order[2] == 3)
                    m_p2_name_text.color = Color.blue;
                else if (order[3] == 3)
                    m_p3_name_text.color = Color.blue;

            }
        }
    }
}
