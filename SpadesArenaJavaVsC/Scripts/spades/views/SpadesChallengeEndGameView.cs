﻿using System.Collections;
using spades.models;
using System;
using spades.controllers;
using common.utils;
using cardGames.models;
using cardGames.controllers;
using cardGames.views;
using common.controllers;
using cardGames.views.overlays;

namespace spades.views

{
    public class SpadesChallengeEndGameView : ChallengeEndGameView
    {
        const float ALTERNTATE_INTERVAL = 2f;
        const float ALTERNTATE_SWITCH = .5f;

        [SerializeField] GameObject m_claim_group;
        [SerializeField] GameObject m_active_group;

        [SerializeField] GameObject m_progress_group;
        [SerializeField] GameObject m_Bigprize_group;
        [SerializeField] GameObject m_body_group;


        [SerializeField] GameObject m_alternate_group;
        [SerializeField] GameObject m_timer_group;
        [SerializeField] GameObject m_prize_group;
        [SerializeField] TMP_Text m_alternate_prize_text;

        [SerializeField] TMP_Text m_body_text;
        [SerializeField] TMP_Text m_timer_text;
        [SerializeField] TMP_Text m_desc_title_text;

        [SerializeField] TMP_Text m_prize_text;
        [SerializeField] TMP_Text m_progress_text;

        [SerializeField] Image m_progressBar_image;

        [SerializeField] GameObject m_challenge_avail_group;
        [SerializeField] GameObject m_expired_group;

        [SerializeField] GameObject m_start_coin_trail_object;
        [SerializeField] GameObject m_complete_icon_object;
        [SerializeField] GameObject m_complete_bg_object;

        [SerializeField] GameObject m_more_info_button;

        Action m_panel_anim_in_done = null;
        Action m_panel_anim_out_done = null;

        CanvasGroup m_timer_canvas;
        CanvasGroup m_prize_canvas;
        CanvasGroup m_info_canvas;

        IEnumerator m_alternate_routine = null;

        Coroutine m_timer_routine;

        private void Awake()
        {
            m_timer_canvas = m_timer_group.GetComponent<CanvasGroup>();
            m_prize_canvas = m_prize_group.GetComponent<CanvasGroup>();
        }

        public void SetChallengeData(SpadesChallenge before_update_challenge, SpadesChallenge post_update_challenge, Action anim_done)
        {

            m_claim_group.SetActive(false);
            m_active_group.SetActive(true);

            m_body_group.SetActive(false);
            m_progress_group.SetActive(true);
            m_Bigprize_group.SetActive(true);

            gameObject.SetActive(true);

            m_prize_text.text = "<sprite=1>" + FormatUtils.FormatBalance(post_update_challenge.Prize);
            m_desc_title_text.text = MissionController.Instance.GetTitleFromChallenge(post_update_challenge);
            int prize = post_update_challenge.Prize;

            int target_value = post_update_challenge.Progress;
            int total_value = post_update_challenge.TotalProgress;
            int start_value = post_update_challenge.Progress;

            bool start_complete = false;

            if (before_update_challenge != null)
                start_value = before_update_challenge.Progress;

            bool challenge_complete = post_update_challenge.Status == SpadesMissionController.ChallengeStatus.Completed;

            if (before_update_challenge == null && challenge_complete)
                start_complete = true;


            if (start_complete)
            {
                m_progressBar_image.fillAmount = 1f;
                PanelIn(() =>
                {
                    RemoveAlternateTimerGroup();
                    StartCoroutine(CompleteAndCoinFlight(1.5f, prize, () =>
                    {
                        PanelOut(() =>
                        {
                            SetAdditionalChallengeData();
                            PanelIn(() =>
                            {
                                anim_done();
                            });

                        });
                    }));

                });
            }
            else //start progress
            {

                PanelIn(() =>
                {

                    StartCoroutine(IncreaseProgress(start_value, target_value, total_value, prize, () =>
                    {

                        if (challenge_complete)
                        {
                            StartCoroutine(CompleteAndCoinFlight(1.5f, prize, () =>
                            {
                                PanelOut(() =>
                                {
                                    SetAdditionalChallengeData();
                                    PanelIn(() =>
                                    {
                                        anim_done();
                                    });
                                });
                            }));
                        }
                        else
                        {
                            anim_done();
                        }
                            
                    }));
                });
            }

                StartAlternateTimer("<sprite=1>" + FormatUtils.FormatBalance(post_update_challenge.Prize));


        }


        public void SetAdditionalChallengeData()
        {
            if (m_timer_routine != null)
                StopCoroutine(m_timer_routine);

            gameObject.SetActive(true);
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            m_complete_icon_object.GetComponent<CanvasGroup>().alpha = 0;
            m_complete_bg_object.GetComponent<CanvasGroup>().alpha = 0;
            m_complete_icon_object.GetComponent<RectTransform>().localScale = new Vector3();

            if (mission != null && mission.GetNumChallenges() > 0)
            {
                SpadesChallenge curr_challenge = mission.GetActiveChallenge() as SpadesChallenge;
                if (curr_challenge != null && curr_challenge.Status != SpadesMissionController.ChallengeStatus.Completed)
                {
                    if (MissionController.Instance.IsMissionExpired())
                    {
                        ShowExpired();
                    }
                    //need to check if the current active challenge is in this table and show the normal progress or its in another table and then need to write that stuff 
                    else if (MissionController.Instance.IsChallengeActiveInActiveMatch(curr_challenge))
                    {
                        m_desc_title_text.text = MissionController.Instance.GetTitleFromChallenge(curr_challenge);
                        m_progress_group.SetActive(true);
                        m_progress_group.GetComponent<RectTransform>().localPosition = new Vector3(85f, 0, 0);
                        m_progress_group.GetComponent<CanvasGroup>().alpha = 1f;
                        m_progressBar_image.fillAmount = 0;
                        m_progress_text.text = 0 + "/" + FormatUtils.FormatBalance(curr_challenge.TotalProgress);
                        m_body_group.SetActive(false);

                        StartAlternateTimer("<sprite=1>" + FormatUtils.FormatBalance(curr_challenge.Prize));

                    }
                    else
                    {
                        if (m_alternate_routine != null)
                            StopCoroutine(m_alternate_routine);
                        m_alternate_group.SetActive(true);

                        //the challenge is active in a different table
                        m_challenge_avail_group.SetActive(true);
                        m_desc_title_text.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(95f, 15f, 0);
                        m_desc_title_text.text = MissionController.Instance.GetTitleFromChallenge(curr_challenge);
                        m_progress_group.SetActive(false);
                        m_Bigprize_group.SetActive(false);
                        m_body_group.SetActive(false);
                        m_more_info_button.SetActive(true);
                        SetTimer("");
                    }
                }
                else
                {
                    //no active challenge - means all the challenges are complete - we need to show timer for next mission

                    m_claim_group.SetActive(true);
                    gameObject.GetComponent<Animator>().SetTrigger("end_claim");
                    m_active_group.SetActive(false);
                    SetTimer("");
                }
            }

        }

        private void StartAlternateTimer(string prize)
        {
            if (m_alternate_routine != null)
                StopCoroutine(m_alternate_routine);

            m_alternate_prize_text.text = prize;
            m_Bigprize_group.SetActive(false);
            m_alternate_group.SetActive(true);
            m_alternate_routine = AlternatePrize_Timer();
            StartCoroutine(m_alternate_routine);
            SetTimer("MISSION ENDS IN: ");
        }

        public void StartEndAnimation()
        {
            gameObject.GetComponent<Animator>().SetTrigger("end_claim");
        }

        public void BT_ClaimButtonClicked()
        {
            LoggerController.Instance.Log("Claim Clicked");
            SpadesOverlayManager.Instance.HideAllOverlays();
            RoundController.Instance.Table_view.ShowGameUI(false);
            LobbyController.Instance.ShowLobby(true, true, false, true);
            MissionController.Instance.MissionButtonClicked();
        }

        public void Button_MoreInfoClicked()
        {
            LoggerController.Instance.Log("more info Clicked");
            SpadesOverlayManager.Instance.HideAllOverlays();
            RoundController.Instance.Table_view.ShowGameUI(false);
            LobbyController.Instance.ShowLobby(true, true, false, true);
            MissionController.Instance.MissionButtonClicked();


        }



        private void PanelIn(Action anim_done)
        {
            Show(true, anim_done);
        }

        private void PanelOut(Action anim_done)
        {
            Show(false, anim_done);
        }

        public void Show(bool panel_in, Action panel_anim_done)
        {
            float actual_width = ManagerView.Instance.Actual_width;

            if (panel_in)
            {
                CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.slider_open);
                m_panel_anim_in_done = panel_anim_done;
                iTween.MoveTo(gameObject, iTween.Hash("x", 20f + actual_width / 2, "islocal", true, "easeType", "easeInQuad", "time", .2, "onComplete", "PanelAnimInDone", "onCompleteTarget", this.gameObject));

            }

            else
            {
                CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.slider_open);
                m_panel_anim_out_done = panel_anim_done;
                iTween.MoveTo(gameObject, iTween.Hash("x", 700f + actual_width / 2, "islocal", true, "easeType", "easeInQuad", "time", .2, "onComplete", "PanelAnimOutDone", "onCompleteTarget", this.gameObject));
            }

        }

        private void PanelAnimInDone()
        {
            if (m_panel_anim_in_done != null)
            {
                m_panel_anim_in_done();
                m_panel_anim_in_done = null;
            }
        }

        private void PanelAnimOutDone()
        {
            if (m_panel_anim_out_done != null)
            {
                m_panel_anim_out_done();
                m_panel_anim_out_done = null;
            }
        }

        public void UpdateProgresBar(float value)
        {
            m_progressBar_image.fillAmount = value;

        }

        IEnumerator IncreaseProgress(int start_value, int target_value, int total_value, int prize, Action anim_done)
        {

            target_value = Math.Min(target_value, total_value);

            if (start_value > target_value)
                start_value = target_value;

            float start_val = start_value / (float)total_value;
            float end_val = target_value / (float)total_value;


            m_progressBar_image.fillAmount = start_val;

            m_progress_text.text = FormatUtils.FormatBalance(start_value) + "/" + FormatUtils.FormatBalance(total_value);

            yield return new WaitForSeconds(0.5f);

            iTween.ValueTo(gameObject, iTween.Hash("from", start_val, "to", end_val, "time", .5f, "onupdate", "UpdateProgresBar", "onupdatetarget", this.gameObject));
            CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.slider_barFull);

            m_progress_text.text = FormatUtils.FormatBalance(target_value) + "/" + FormatUtils.FormatBalance(total_value);


            if (anim_done != null)
                anim_done();
        }

        IEnumerator CompleteAndCoinFlight(float initial_delay, int prize, Action anim_done)
        {
            m_progress_text.text = "COMPLETE";
            ShowCompleteIcon();

            yield return new WaitForSecondsRealtime(initial_delay);

            CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.challenge_complete);

            yield return new WaitForSecondsRealtime(0.35f);

            SwitchCompletedAndReward();

            yield return new WaitForSecondsRealtime(0.4f);


            CometOverlay coins_comet = SpadesOverlayManager.Instance.ShowCometOverlay();
            int num_coins_to_show = Mathf.Min(5, prize);

            coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_start_coin_trail_object, LobbyTopController.Instance.Coins_box_icon, 0.05f, 0.15f, 1.25f, LobbyTopController.Instance.Coins_text, prize, LobbyTopController.Instance.Coins_text.Coin_box_animator, () =>
            {
                if (anim_done != null)
                    anim_done();
            }, null);



        }

        private void SwitchCompletedAndReward()
        {
            //put prize group to start location and dim its alpha to 0 - the progress group should already be there 


            iTween.MoveTo(m_progress_group, iTween.Hash("x", 40f, "islocal", true, "easeType", "easeInQuad", "time", .35));
            iTween.ValueTo(m_progress_group, iTween.Hash("from", 1, "to", 0, "time", .35f, "onupdate", "UpdateProgressAlpha", "Onupdatetarget", this.gameObject));

            m_Bigprize_group.GetComponent<RectTransform>().localPosition = new Vector3(170f, -45f, 0);
            m_Bigprize_group.GetComponent<CanvasGroup>().alpha = 0;


            iTween.MoveTo(m_Bigprize_group, iTween.Hash("x", 120f, "islocal", true, "easeType", "easeInQuad", "time", .35));
            iTween.ValueTo(m_Bigprize_group, iTween.Hash("from", 0, "to", 1, "time", .35f, "onupdate", "UpdatePrizeAlpha", "Onupdatetarget", this.gameObject));

        }

        private void ShowCompleteIcon()
        {

            iTween.ScaleTo(m_complete_icon_object, iTween.Hash("x", 1f, "y", 1f, "time", .35f, "easeType", iTween.EaseType.easeOutBounce));
            iTween.ValueTo(m_complete_icon_object, iTween.Hash("from", 0, "to", 1, "time", .35f, "onupdate", "UpdateIconAlpha", "Onupdatetarget", this.gameObject));
        }

        public void UpdateIconAlpha(float value)
        {
            //m_manager.CanvasGroup.alpha = value;
            m_complete_icon_object.GetComponent<CanvasGroup>().alpha = value;
            m_complete_bg_object.GetComponent<CanvasGroup>().alpha = value;
        }

        public void UpdateProgressAlpha(float value)
        {
            //m_manager.CanvasGroup.alpha = value;
            m_progress_group.GetComponent<CanvasGroup>().alpha = value;
        }

        public void UpdatePrizeAlpha(float value)
        {
            //m_manager.CanvasGroup.alpha = value;
            m_Bigprize_group.GetComponent<CanvasGroup>().alpha = value;
        }


        private void SetTimer(string timer_text_prefix)
        {
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            TimeSpan delta = mission.End_date - DateTime.Now;

            if (delta.TotalSeconds > 0)
            {
                m_timer_text.gameObject.SetActive(true);

                m_timer_routine = StartCoroutine(RunTimer(delta, m_timer_text, timer_text_prefix));
            }
            else
            {
                ShowExpired();
            }

        }


        private void ShowExpired()
        {
            m_timer_text.text = "EXPIRED";

            m_expired_group.SetActive(true);

            RemoveAlternateTimerGroup();

            if (m_timer_routine != null)
                StopCoroutine(m_timer_routine);


            m_challenge_avail_group.SetActive(false);
            m_more_info_button.SetActive(false);


            m_progress_group.SetActive(false);
            m_body_group.SetActive(false);
            m_Bigprize_group.SetActive(false);
            m_desc_title_text.gameObject.SetActive(false);

        }

        private void RemoveAlternateTimerGroup()
        {
            if (m_alternate_routine != null)
                StopCoroutine(m_alternate_routine);

            m_alternate_group.SetActive(false);
        }

        private IEnumerator RunTimer(TimeSpan ts, TMP_Text timer, string timer_text_prefix)
        {
            float startTime = Time.realtimeSinceStartup;

            timer_text_prefix += "<color=#FF4855>";

            while (ts.TotalSeconds >= 0)
            {
                yield return new WaitForSecondsRealtime(1f);
               // timer.text = string.Format("{0}{1:00}:{2:00}:{3:00}", timer_text_prefix, ts.Hours, ts.Minutes, ts.Seconds);
                timer.text = timer_text_prefix + DateUtils.TimespanToDateMission(ts);

                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }

            SetTimer("");
        }

        IEnumerator AlternatePrize_Timer()
        {

            RectTransform prize_rect = m_prize_group.GetComponent<RectTransform>();
            RectTransform timer_rect = m_timer_group.GetComponent<RectTransform>();


            m_timer_canvas.alpha = 0f;
            m_prize_canvas.alpha = 1f;
            prize_rect.localPosition = new Vector3(50f, 0, 0);



            timer_rect.localPosition = new Vector3(0, 0, 0);

            bool group_active = true;

            while (group_active)
            {
                yield return new WaitForSeconds(ALTERNTATE_INTERVAL);

                timer_rect.localPosition = new Vector3(0, 0, 0);

                iTween.MoveTo(m_prize_group, iTween.Hash("x", -50f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(m_prize_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdatePrize2Alpha", "Onupdatetarget", this.gameObject));


                iTween.MoveTo(m_timer_group, iTween.Hash("x", -50f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(m_timer_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateTimerAlpha", "Onupdatetarget", this.gameObject));

                yield return new WaitForSeconds(ALTERNTATE_SWITCH + ALTERNTATE_INTERVAL);


                prize_rect.localPosition = new Vector3(250f, 0, 0);
                iTween.MoveTo(m_prize_group, iTween.Hash("x", 50f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(m_prize_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdatePrize2Alpha", "Onupdatetarget", this.gameObject));



                iTween.MoveTo(m_timer_group, iTween.Hash("x", -100f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(m_timer_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateTimerAlpha", "Onupdatetarget", this.gameObject));

                yield return new WaitForSeconds(ALTERNTATE_SWITCH);
            }

        }



        public void UpdateTimerAlpha(float value)
        {
            m_timer_canvas.alpha = value;
        }

        public void UpdatePrize2Alpha(float value)
        {
            m_prize_canvas.alpha = value;
        }

    }
}


