﻿using spades.controllers;
using cardGames.models;
using common.controllers;

namespace spades.views
{

    /// <summary>
    /// Card view - sits on the card prefab - used for visualization of a card and receiving click events on it 
    /// </summary>
    public class CardView : MonoBehaviour
    {


        GameObject m_card;

        RectTransform m_rect_transform;

        Card m_card_model;

        bool m_drag = false;
        bool m_active = true;
        bool m_interactable = true;

        Vector3 m_startPos = new Vector3(0, 0, 0);
        Vector3 m_org_pos = new Vector3(0, 0, 0);
        float m_org_z = 0;

        float m_scaler_x;
        float m_scaler_y;

        float m_scaler_width;
        float m_scaler_height;


        float combinedX;
        float combinedY;


        int m_my_org_order = 0;
		bool card_thrown=false;


        public void SetInteractable(bool value)
        {
            m_interactable = value;
        }
        // Use this for initialization
        void Awake()
        {
            m_rect_transform = GetComponent<RectTransform>();
            m_card = this.gameObject;

        }

        public void ReadScalarData()
        {
            m_scaler_x = ManagerView.Instance.Scaler_x;
            m_scaler_y = ManagerView.Instance.Scaler_y;
            m_scaler_width = ManagerView.Instance.Scaler_width;
            m_scaler_height = ManagerView.Instance.Scaler_height;

        }

        public float GetScale()
        {
            return m_rect_transform.localScale.x;
        }

        public void SetScale(Vector3 size)
        {
			if(m_rect_transform!=null)
           		m_rect_transform.localScale = size;
        }

        public void SetPosition(Vector3 pos)
        {
			if(m_rect_transform!=null)
            	m_rect_transform.localPosition = pos;
        }

        public GameObject GetGameObject()
        {
            return m_card;
        }

        public Vector3 GetPosition()
        {
            return m_rect_transform.localPosition;
        }

        public Vector3 GetRotation()
        {
            return m_rect_transform.localEulerAngles;
        }

        public Card GetCardModel()
        {
            return m_card_model;
        }

        public void SetRotation(Vector3 new_rotation)
        {
			if(m_rect_transform!=null)
            	m_rect_transform.localEulerAngles = new_rotation;
        }



        public void SetCardModel(Card card_model)
        {
            m_card_model = card_model;
        }

        public void SetCardButton(bool enable)
        {
            //GetComponent<Button>().interactable = enable;

            if (enable)

                transform.Find("Card").GetComponent<Image>().color = new Color(1f, 1f, 1f);
            else
                transform.Find("Card").GetComponent<Image>().color = new Color(0.65f, 0.65f, 0.65f);

            m_active = enable;
        }

        /// <summary>
        /// flips the card.
        /// </summary>
        public void RevealCard()
        {

            transform.Find("Card").GetComponent<Image>().sprite = ManagerView.Instance.GetDeckImage(CardImageIndex);
        }

        public void SpadesBrokenRevealCard()
        {
            GetComponent<Image>().sprite = ManagerView.Instance.GetDeckImage(CardImageIndex);
        }

        public void ShowHideShadow(bool show)
        {
            //LoggerController.Instance.Log ("Disabling shadow");
            //GetComponent<Image>().enabled = show;
            transform.Find("Card").GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        }

        /// <summary>
        /// called from the card button and moves to the card controller
        /// </summary>
        public void CardWasClicked()
        {
            //RoundController.Instance.CardWasClicked(m_card);
        }

        /// <summary>
        /// Deletes the card game object (visuallY)
        /// </summary>
        public void DeleteCard()
        {
            if (gameObject != null)
                Destroy(gameObject);
        }

        public void OnMouseDown()
        {
			if (!card_thrown && !RoundController.Instance.Player_bid_or_played)
			{
				if (m_interactable)
				{
					if (m_active)
					{
						m_org_pos = m_rect_transform.localPosition;// + temp;
						m_org_z = m_rect_transform.localEulerAngles.z;
						RoundController.Instance.PlayerClickedCard (m_card);
					}
				}
			}
			else
			{
				LoggerController.Instance.Log ("RoundController.Instance.Player_bid_or_played: " + RoundController.Instance.Player_bid_or_played);
			}
        }

        public void StartDrag()
        {
            if (m_interactable)
            {
                RoundController.Instance.Selected_card_view = this;
                ReadScalarData();


                combinedX = m_scaler_x / m_scaler_width;
                combinedY = m_scaler_y / m_scaler_height;

#if (!UNITY_EDITOR && !UNITY_WEBGL)
                if (Input.touchCount > 0) {
    				m_rect_transform.localScale*=1.3f;
    				m_drag=true;
    				m_my_org_order = m_rect_transform.GetSiblingIndex();
    				m_rect_transform.SetAsLastSibling();
    				m_startPos = new Vector3(Input.GetTouch(0).position.x*combinedX,Input.GetTouch(0).position.y*combinedY,0);
                }
#else
                iTween.MoveTo(gameObject, iTween.Hash("position", m_rect_transform.localPosition + m_rect_transform.up.normalized * 50f, "islocal", true, "easeType", "easeInQuad", "time", .2));

                m_my_org_order = m_rect_transform.GetSiblingIndex();
                m_startPos = new Vector3(Input.mousePosition.x * combinedX, Input.mousePosition.y * combinedY, 0);

#endif
            }
        }

        public void ClickThrowCard()
        {
            RoundController.Instance.DragEnded(200f, m_org_pos, GetComponent<CardView>(), m_org_z);
            m_drag = false;
        }

        public void MoveCardBackDown()
        {
            iTween.MoveTo(gameObject, iTween.Hash("position", m_rect_transform.localPosition - m_rect_transform.up.normalized * 50f, "islocal", true, "easeType", "easeInQuad", "time", .2));
        }

        public void SetMyOrderBack()
        {
            m_rect_transform.SetSiblingIndex(m_my_org_order);
        }

        public void ScaleMeBackToNormal()
        {
            m_rect_transform.localScale /= 1.3f;
        }

        public void ForceEndDrag()
        {

            RoundController.Instance.DragEnded(301, m_org_pos, GetComponent<CardView>(), m_org_z);
            m_drag = false;
        }

        void Update()
        {

            if (m_interactable)
            {
                if (m_drag)
                {

#if (!UNITY_EDITOR && !UNITY_WEBGL)
                    if (Input.touchCount > 0) {
                        m_rect_transform.localPosition -= m_startPos-new Vector3(Input.GetTouch(0).position.x*combinedX,Input.GetTouch(0).position.y*combinedY,0);
    					m_startPos =new Vector3(Input.GetTouch(0).position.x*combinedX,Input.GetTouch(0).position.y*combinedY,0);

    					float card_height = GetComponent<RectTransform>().localPosition.y;

    					if((card_height>160f))
    					{
    						RoundController.Instance.DragEnded(m_startPos.y,m_org_pos,GetComponent<CardView>(),m_org_z);
    						m_drag=false;
    					}
                    }
#else
                    m_rect_transform.localPosition -= m_startPos - new Vector3(Input.mousePosition.x * combinedX, Input.mousePosition.y * combinedY, 0);
                    m_startPos = new Vector3(Input.mousePosition.x * combinedX, Input.mousePosition.y * combinedY, 0);

                    float card_height = GetComponent<RectTransform>().localPosition.y;

                    if (card_height > 160f)
                    {
                        RoundController.Instance.DragEnded(card_height, m_org_pos, GetComponent<CardView>(), m_org_z);
                        m_drag = false;

                    }

#endif
                }

                if (m_drag)
                {
#if (!UNITY_EDITOR && !UNITY_WEBGL)
                    if (Input.touchCount > 0) {
    					Touch	touch = Input.GetTouch(0);

    					if(touch.phase == TouchPhase.Ended)
    					{
    						RoundController.Instance.DragEnded(m_startPos.y,m_org_pos,GetComponent<CardView>(),m_org_z);
    						m_drag=false;
    					}
                    }
#else
                    if (Input.GetMouseButtonUp(0))
                    {
                        RoundController.Instance.DragEnded(m_startPos.y, m_org_pos, GetComponent<CardView>(), m_org_z);
                        m_drag = false;
                    }
#endif
                }
            }
        }

        private int CardImageIndex
        {
            get
            {
                switch (m_card_model.Suit)
                {
                    case Card.SuitType.Diamonds:
                        return (int)m_card_model.Rank - 2;
                    case Card.SuitType.Clubs:
                        return (int)m_card_model.Rank - 2 + 13;
                    case Card.SuitType.Hearts:
                        return (int)m_card_model.Rank - 2 + 26;
                    case Card.SuitType.Spades:
                        return (int)m_card_model.Rank - 2 + 39;
                    default:
                        return 0;
                }
            }
        }


        public bool Card_thrown
        {
            get
            {
                return card_thrown;
            }
            set
            {
                card_thrown = value;
            }
        }

    }
}