using spades.controllers;

namespace spades.views.tablechat
{
    public class TableChatText : MonoBehaviour
    {
        [SerializeField] TMP_Text m_text=null;

        private int m_id;
        private OnTextItemClick m_OnTextItemClick;

        public delegate void OnTextItemClick(int id);

        public void Init(int id,string text, OnTextItemClick onTextItemClick)
        {
            m_id = id;

            m_OnTextItemClick = onTextItemClick;
            m_text.text = text;
        }

        public void OnButtonClick()
        {
            if (m_OnTextItemClick != null)
            {
                ///TableChatController.Instance.HideChatPanel();
               // TableChatController.Instance.CloseEmojiMenu();
                //m_OnTextItemClick(m_id);
                //
                TableChatController.Instance.HideChatPanel();
                TableChatController.Instance.CloseEmojiMenu();
                TableChatController.Instance.PlayChatText(m_id);
                TableChatController.Instance.SendAvatatChat(m_id, m_text.text);
            }
        }

    }
}
