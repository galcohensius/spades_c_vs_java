﻿using spades.controllers;

namespace spades.views.tablechat
{
    public class EmojiChatRadialView : MonoBehaviour
    {
        [SerializeField]
        private Animator m_emojiChatRadialAnimator;

        [SerializeField]
        private GameObject m_radialOpenButton;

        [SerializeField]
        private GameObject m_radialCloseButton;

        private bool m_shouldRadialOpen = true;
        private bool m_isRadialCurrentlyOpen = false;

        GameObject m_player_indication;

        public bool IsRadialCurrentlyOpen
        {
            get
            {
                return m_isRadialCurrentlyOpen;
            }
        }

        public void ToggleEmojiRadial(bool shouldOpen)
        {
            m_radialCloseButton.SetActive(shouldOpen);
            m_radialOpenButton.SetActive(!shouldOpen);
            m_emojiChatRadialAnimator.SetBool("IsRadialOpen", shouldOpen);

        }

        public void ToggleRadialStatus()
        {
            m_shouldRadialOpen = !m_shouldRadialOpen;

            MovePlayerScoreIndication(m_shouldRadialOpen);
        }


        public void MovePlayerScoreIndication(bool move)
        {
            if (move)
                iTween.MoveTo(m_player_indication, iTween.Hash("x", -480, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", .25));
            else
                iTween.MoveTo(m_player_indication, iTween.Hash("x", -360, "islocal", true, "easeType", iTween.EaseType.easeInCirc, "time", .25));
        }

        public void Reset()
        {
            gameObject.SetActive(false);
            gameObject.SetActive(true);
            m_shouldRadialOpen = true;
            MovePlayerScoreIndication(true);
        }

        public void Start()
        {
            m_player_indication = RoundController.Instance.Table_view.GetPlayerView(3).Player_indication.gameObject;
        }
    }
}
