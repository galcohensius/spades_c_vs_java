using spades.controllers;
using cardGames.views;

namespace spades.views.tablechat
{
    public class TableChatEmojiView : MonoBehaviour
    {
        [SerializeField] Image m_image = null;

        private int m_id;
        private OnEmojiItemClick m_OnEmojiItemClick;

        public delegate void OnEmojiItemClick(int id);

        public void Init(int id, Sprite emoji, OnEmojiItemClick onEmojiItemClick)
        {
            m_id = id;

            m_OnEmojiItemClick = onEmojiItemClick;
            m_image.sprite = emoji;
            m_image.SetNativeSize();
        }

        public void OnButtonClick(int animTriggerName)
        {
            TableChatController.Instance.CloseEmojiMenu();
            /*if (m_OnEmojiItemClick != null)
                m_OnEmojiItemClick(m_id);*/

            MAvatarView.AvatarAnimationStateTrigger animTrigger = (MAvatarView.AvatarAnimationStateTrigger)animTriggerName;

            TableChatController.Instance.PlayNewAvatarAnimation(animTrigger);
            //TableChatController.Instance.ToggleSingleMenu(true, false);
        }
    }
}
