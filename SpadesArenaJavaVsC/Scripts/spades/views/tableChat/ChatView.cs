﻿using spades.controllers;
using spades.models.tablechat;
using System.Collections.Generic;

namespace spades.views.tablechat
{
    public class ChatView : MonoBehaviour
    {
        [SerializeField] GameObject m_TextChatMessagesContainer = null;
        [SerializeField] GameObject m_TextChatMessagePrefab = null;
        [SerializeField] TableChatText[] m_TextFavorites = null;

        [SerializeField]
        private Animator m_tableChatAnimator=null;

        [SerializeField]
        private GameObject m_openChatButton = null;

        [SerializeField]
        private GameObject m_navIndicatorTop = null;

        [SerializeField]
        private GameObject m_navIndicatorBottom = null;

        [SerializeField] ScrollRect m_scroll_rect = null;

        private bool m_isExpanded = false;

        void Awake()
        {
            m_scroll_rect.onValueChanged.AddListener(OnScroll);
        }

        void OnDestroy()
        {
            m_scroll_rect.onValueChanged.RemoveListener(OnScroll);
        }

        private void OnScroll(Vector2 vec)
        {
            m_navIndicatorTop.SetActive(false);
            m_navIndicatorBottom.SetActive(false);

            if ((m_TextChatMessagesContainer.transform as RectTransform).rect.height <= ManagerView.Instance.Actual_height)
                return;

            if (m_scroll_rect.verticalNormalizedPosition >= 1)
            {
                m_navIndicatorBottom.SetActive(true);
            }
            else if (m_scroll_rect.verticalNormalizedPosition <= 0)
            {
                m_navIndicatorTop.SetActive(true);
            }
        }

        public bool IsExpanded
        {
            get
            {
                return m_isExpanded;
            }
        }

        public void Show(TableChatModel model)
        {
            UpdateTextFavorites(model);

            AddChatTextMessages(model);

            ToggleTextChatMenu(true);
        }

        public void Hide()
        {
            ToggleTextChatMenu(false);
        }

        public void ToggleTextChatMenu(bool isOpen)
        {
            m_tableChatAnimator.SetBool("IsOpen", isOpen);

            if (isOpen)
            {
                m_openChatButton.SetActive(!isOpen);
            }

            /*if (!isOpen)
            {
                m_tableChatAnimator.SetBool("IsExpanded", false);
                m_isExpanded = false;
                m_moreChatButton.SetActive(true);
                m_lessChatButton.SetActive(false);
            }*/
        }

        public void ToggleTextChatMenuExpanded(bool isExpanded)
        {
            m_tableChatAnimator.SetBool("IsExpanded", isExpanded);
            m_isExpanded = isExpanded;

            if (isExpanded)
            {
                TableChatController.Instance.ToggleSingleMenu(true, false);
                //m_moreChatButton.SetActive(false);
                //m_lessChatButton.SetActive(true);
            }
            else
            {
                //m_moreChatButton.SetActive(true);
                //m_lessChatButton.SetActive(false);
            }
        }

        public void UpdateTextFavorites(TableChatModel model)
        {
            for (int i = 0; i < m_TextFavorites.Length; i++)
            {
                m_TextFavorites[i].Init(model.ChatTextMessageFavorites[i], model.FindMessageById(model.ChatTextMessageFavorites[i]), OnFavoriteTextItemClick);
            }
        }

        private void OnFavoriteTextItemClick(int id)
        {
            ToggleChatMenuLock(true);
            TableChatController.Instance.SendText(id);

            if (m_isExpanded)
            {
                ToggleTextChatMenuExpanded(false);
            }
        }

        private void AddChatTextMessages(TableChatModel model)
        {
            foreach (Transform C in m_TextChatMessagesContainer.transform)
            {
                Destroy(C.gameObject);
            }

            foreach (KeyValuePair<int, string> item in model.ChatTextMessages)
            {
                GameObject chatTextMessage = Instantiate(m_TextChatMessagePrefab, m_TextChatMessagesContainer.transform, false);
                TableChatText tct = chatTextMessage.GetComponent<TableChatText>();
                tct.Init(item.Key, item.Value, OnTextItemClick);
            }
        }

        private void OnTextItemClick(int id)
        {
            //ToggleChatMenuLock(true);
            TableChatController.Instance.SendText(id);

            //add to favorites
            TableChatController.Instance.AddTextToFavorites(id);
            //ToggleTextChatMenuExpanded(false);
        }

        void OnEnable()
        {
            //TableChatController.Instance.ShowChatPanel();
            //m_openChatButton.SetActive(true);
        }

        public void ToggleChatMenuLock(bool shouldLockMenu)
        {
            //m_chatMenuLock.blocksRaycasts = shouldLockMenu;
        }

        public void TL_OnChatPanelClosed()
        {
            m_openChatButton.SetActive(true);
        }
    }
}
