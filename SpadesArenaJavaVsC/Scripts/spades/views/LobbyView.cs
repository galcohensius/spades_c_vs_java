using System.Collections.Generic;
using common;
using common.controllers;
using common.models;
using spades.controllers;
using spades.models;
using spades.views.overlays;
using System;
using cardGames.models;
using cardGames.controllers;
using cardGames.views.popups;
using cardGames.views;
using static cardGames.models.World;
using common.ims;
using common.ims.model;

namespace spades.views
{

    public class LobbyView : MonoBehaviour
    {
        [SerializeField] Image m_room_name_image = null;
        [SerializeField] List<Sprite> m_room_name_images = new List<Sprite>();

        Dictionary<string,Sprite> m_promo_room_name_images; // Promo rooms title images by location

        [SerializeField] List<WorldItemView> m_world_items = new List<WorldItemView>();

        [SerializeField] List<Sprite> m_single_panels = new List<Sprite>();
        [SerializeField] List<Sprite> m_arena_panels = new List<Sprite>();

        [SerializeField] List<TMP_ColorGradient> m_room_title_gradient = new List<TMP_ColorGradient>();

        [SerializeField] List<Sprite> m_world_bgs = new List<Sprite>();
        List<Sprite> m_world_bgs_with_promo;
        [SerializeField] List<Sprite> m_locked_single = null;
        [SerializeField] List<GameObject> m_gems_objects = null;
        [SerializeField] List<Vector3> m_gems_objects_positions = null;

        [SerializeField] List<Sprite> m_gems_lobby_sprite = null;
        [SerializeField] List<Sprite> m_tiny_gems_on_lobby_sprite = null;
        [SerializeField] List<Sprite> m_tiny_gems_off_lobby_sprite = null;

        [SerializeField] List<Color> m_lobby_highlight_colors = null;

        [SerializeField] GameObject m_world_item_prefab = null;
        [SerializeField] GameObject m_items = null;

        [SerializeField] GameObject m_left_arrow = null;
        [SerializeField] GameObject m_right_arrow = null;

        [SerializeField] GameObject m_side_arrows = null;

        [SerializeField] ProgressView m_progress_bar_view = null;

        [SerializeField] GameObject m_loading_screen = null;

        [SerializeField] StepSwipe m_swipe = null;

        [SerializeField] GameObject m_lobby_banner = null;

        [SerializeField] GameObject m_challenge_collect_ready = null;
        [SerializeField] GameObject m_challenge_will_unlock = null;
        [SerializeField] TMP_Text m_challenge_unlock_text = null;

        [SerializeField] GameObject m_contests_collect_ready = null;

        [SerializeField] RectTransform m_imsBanner = null;

        int m_effect_index = 0;

        //links to top panel objects
        [SerializeField] GameObject m_top_panel_holder = null;
        [SerializeField] GameObject m_bottom_panel_holder = null;

        [SerializeField] GameObject m_bottom_bonus_text = null;

        [SerializeField] ButtonsOverView m_banner_overview = null;
        [SerializeField] ModeButtonsView m_mode_buttons_view = null;

        [SerializeField] RectTransform m_footer_left;
        [SerializeField] RectTransform m_footer_right;

        [SerializeField] LobbyTopController m_lobby_top_controller;
        [SerializeField] HourlyBonusView m_hourly_bonus_view;

        [SerializeField] GameObject m_promoRoom_item_prefab;


        void Start()
        {

            ContestsController.Instance.On_ClaimDataAvailble += NewContestClaimDataArrived;

            VoucherController.Instance.OnVouchersAdded += () => UpdateVouchers();
        }

#if UNITY_EDITOR
        //use for debug only
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (SpadesCommManager.Instance.m_contestsFakeData)
                {
                    ImmediateFakeCollectClaim();
                }
            }
        }

        //currently not called, NewContestClaimDataArrived emulates this in the editor and can also be done immediately with ImmediateFakeCollectClaim()
        public IEnumerator FakeCollectClaim()
        {
            yield return new WaitForSecondsRealtime(17f);
            ContestsController.Instance.To_claim_contests.Add(new ContestsController.ClaimContestData(1, 100, 4));
            ContestsController.Instance.To_claim_contests.Add(new ContestsController.ClaimContestData(2, 100, 99));
            NewContestClaimDataArrived();
        }

        private void ImmediateFakeCollectClaim()
        {
            ContestsController.Instance.To_claim_contests.Add(new ContestsController.ClaimContestData(1, 100, 4));
            ContestsController.Instance.To_claim_contests.Add(new ContestsController.ClaimContestData(2, 100, 99));
            NewContestClaimDataArrived();
        }
#endif

        private void NewContestClaimDataArrived()
        {
#if UNITY_EDITOR
            if (SpadesCommManager.Instance.m_contestsFakeData)
            {
                ContestsController.Instance.To_claim_contests.Add(new ContestsController.ClaimContestData(1, 100, 4));
                ContestsController.Instance.To_claim_contests.Add(new ContestsController.ClaimContestData(2, 100, 99));
            }
#endif

            if (ModelManager.Instance.GetUser().GetLevel() >= ContestsController.Instance.Min_contests_level)
                m_contests_collect_ready.SetActive(ContestsController.Instance.To_claim_contests.Count > 0);
        }

        public StepSwipe GetSwipe()
        {
            return m_swipe;
        }

        public void CameraShake(float delay = 0)
        {
            iTween.ShakeRotation(this.gameObject, iTween.Hash("z", 4, "easeType", "easeOutCubic", "delay", delay, "time", .5));
            iTween.ShakePosition(this.gameObject, iTween.Hash("x", 3, "y", 3, "easeType", "easeOutCubic", "delay", delay, "time", .5));
        }


        public void LimitReached(int limit)
        {
            //LoggerController.Instance.Log ("Limit: " + limit);
            if (limit == 0)
            {
                ShowHideLeftArrow(false);
                ShowHideRightArrow(true);
                ButtonsOverView.ForceNormalCursor();
            }
            else if (limit == 1)
            {
                ShowHideRightArrow(false);
                ShowHideLeftArrow(true);
                ButtonsOverView.ForceNormalCursor();
            }
            else
            {
                ShowHideLeftArrow(true);
                ShowHideRightArrow(true);
            }
        }

        public void ShowHideLeftArrow(bool show)
        {
            if (m_left_arrow.activeSelf != show)
                m_left_arrow.SetActive(show);
        }

        public void ShowHideRightArrow(bool show)
        {
            if (m_right_arrow.activeSelf != show)
                m_right_arrow.SetActive(show);
        }

        public void ShowHideLobbyGemEffects(int index)
        {
            //hide all the effects - we are moving
            foreach (WorldItemView item in m_world_items)
            {
                if (item != null)
                    item.Arena_view.ShowHideMyEffect(false);
            }
            if (index == -1)
            {
                //hide all the effects - we are moving
                foreach (WorldItemView item in m_world_items)
                {
                    if (item != null)
                        item.Arena_view.ShowHideMyEffect(false);
                }
            }
            else
            {
                //we stopped - show only the relevant effect 
                if (m_world_items[index] != null)
                    m_world_items[index].Arena_view.ShowHideMyEffect(true);
            }
        }

        public void SetUser()
        {

            User user = (ModelManager.Instance as SpadesModelManager).GetUser();

            user.OnXPChange += HandleXPChange;

            HandleXPChange();

            m_swipe.OnReachLimit += LimitReached;

            m_progress_bar_view.ShowInitialData(user.GetXPInLevel(), user.GetLevelTotalXP(), user.GetLevel());

        }

        public void ForceSpinAllPanels()
        {
            for (int i = 0; i < m_world_items.Count; i++)
            {
                m_world_items[i].ForceFlipAllPanels();
            }
        }

        private void HandleXPChange()
        {
            User user = (ModelManager.Instance as SpadesModelManager).GetUser();

            m_progress_bar_view.ShowInitialData(user.GetXPInLevel(), user.GetLevelTotalXP(), user.GetLevel());
        }

        public void CreateLobbyWorlds(List<World> worlds)
        {

            //reading the value again even that its in manager view - because of read timing
            float actual_pixel_width = 1920f - StepSwipe.SCREEN_MASK_OFFSET;

            float screen_width = ManagerView.Instance.Actual_width;

            //clean the holder from older children
            foreach (Transform child in m_items.transform)
            {
                Destroy(child.gameObject);
            }
            m_world_items.Clear();
            

            for (int i = 0; i < worlds.Count; i++)
            {
                SpadesWorld world = worlds[i] as SpadesWorld;

                GameObject world_item = null;

                float curr_pos = (i + 1) * actual_pixel_width - actual_pixel_width;

                if (world.WorldType == WorldTypes.Regular)
                {
                    world_item = Instantiate(m_world_item_prefab, m_items.transform);

                    WorldItemView world_item_view = world_item.GetComponent<WorldItemView>();

                    m_world_items.Add(world_item_view);
                    world_item_view.SetWorldModel(world, this);
                }

                else if (world.WorldType == WorldTypes.Promo)
                {
                    world_item = Instantiate(m_promoRoom_item_prefab, m_items.transform);
                    PromoRoomItemView promoRoomItemView = world_item.GetComponent<PromoRoomItemView>();
                    promoRoomItemView.SetWorld(world, OnPromoRoomInjectionEnd);
                }
                else
                {
                    // Virtual world
                    continue;
                }

                world_item.GetComponent<RectTransform>().localScale = Vector3.one;
                world_item.GetComponent<RectTransform>().localPosition = new Vector3(i * actual_pixel_width, 0, 0);
                world_item.GetComponent<RectTransform>().offsetMin = new Vector2(curr_pos, 0);
                world_item.GetComponent<RectTransform>().offsetMax = new Vector2(curr_pos, 0);
            }

            GetSwipe().OnMoveRoomComplete = OnMoveRoomComplete;
            GetSwipe().OnSwipeUpdate = OnSwipeUpdate;
            Loading_screen.SetActive(false);


            m_promo_room_name_images = new Dictionary<string, Sprite>();

            // Create the world bg colors list
            m_world_bgs_with_promo = new List<Sprite>(m_world_bgs);
            int numOfPromoRooms = worlds.FindAll(w => w.WorldType == WorldTypes.Promo).Count;
            if (numOfPromoRooms>=1)
            {
                int leftToPromoRoomIndex = Math.Max(worlds.FindIndex(w => w.WorldType == WorldTypes.Promo)-1,0);
                m_world_bgs_with_promo.Insert(leftToPromoRoomIndex, m_world_bgs[leftToPromoRoomIndex]);
            }
            if (numOfPromoRooms==2)
            {
                int rightToPromoRoomIndex = Math.Min(worlds.FindLastIndex(w => w.WorldType == WorldTypes.Promo) + 1, worlds.Count-1);
                m_world_bgs_with_promo.Insert(rightToPromoRoomIndex-1, m_world_bgs[Math.Max(0,rightToPromoRoomIndex-numOfPromoRooms-1)]);
            }
             

            if (ManagerView.Instance.Aspect43 && !ManagerView.Instance.Aspect43_correction_done)
                AdjustSizeForIpad();

            UpdateMissionIcons();
        }

       

        public void RegisterToMissionEvents(Mission mission)
        {
            mission.ModelChanged += UpdateMissionIcons;
        }

        private void UpdateMissionIcons()
        {
            MissionController.Instance.UpdateMissionLobbyIcons();
        }

        private void OnPromoRoomInjectionEnd(string location, Sprite promoRoomNameImage)
        {
            m_promo_room_name_images[location] = promoRoomNameImage;

            // Since the injection end can end later than OnSwipeUpdate if the promo room is the initial room
            // we need to refresh the title after injection.
            // Use ManageBGColors() for invoking the Swipe event
            m_swipe.ManageBGColors();
        }

        private void OnSwipeUpdate(float xpos, float screenWidth)
        {
            float val = Math.Abs(xpos / screenWidth - (int)(xpos / screenWidth));
            if (val > 0.5)
            {
                m_room_name_image.color = new Color(1, 1, 1, (val - 0.5f) * 2);
            }
            else
            {
                m_room_name_image.color = new Color(1, 1, 1, 1 - val * 2);
            }

            int worldIndex = -Convert.ToInt16(xpos / screenWidth);
            World world = ModelManager.Instance.Worlds[worldIndex];

            if (world.WorldType == WorldTypes.Regular)
                m_room_name_image.sprite = m_room_name_images[world.Index];
            else
            {
                if (m_promo_room_name_images.ContainsKey(world.Promo_iZone.Location))
                    m_room_name_image.sprite = m_promo_room_name_images[world.Promo_iZone.Location];
            }

            m_room_name_image.SetNativeSize();


            Mode_buttons_view.Draw(SpadesModelManager.Instance.Playing_mode, LobbyController.Instance.Lobby_view.Lobby_highlight_colors[world.Index]);

            if (world.WorldType == World.WorldTypes.Promo)
            {
                m_mode_buttons_view.HideModeButtons();
            }

        }

        private void OnMoveRoomComplete(int index)
        {
            World world = ModelManager.Instance.Worlds[index];

            if(world.WorldType == WorldTypes.Regular)
            {
                m_mode_buttons_view.ShowModeButtons();
                Mode_buttons_view.Draw(SpadesModelManager.Instance.Playing_mode, LobbyController.Instance.Lobby_view.Lobby_highlight_colors[world.Index]);
            } else
            {
                m_mode_buttons_view.HideModeButtons();
            }

        }

        private void AdjustSizeForIpad()
        {
            ManagerView.Instance.Aspect43_correction_done = true;

            float world_delta_y = 150;

            // Lobby panels
            foreach (WorldItemView worldItem in m_world_items)
            {
                float curr_value_x = worldItem.gameObject.GetComponent<RectTransform>().offsetMax.x;

                worldItem.gameObject.GetComponent<RectTransform>().offsetMax = new Vector2(curr_value_x, world_delta_y);

                worldItem.RescaleWorldToIpad(1.15f, -70f, 10f, -10f, -25f, 20f, -70f, -20f, -5f, 5f, 10f, -35f);
            }

            //setting side arrows
            float side_arrows_delta_y = 75;
            AddDeltaY(side_arrows_delta_y, LobbyController.Instance.Lobby_view.Side_arrows);
            AddDeltaY(side_arrows_delta_y, LobbyController.Instance.Lobby_view.Mode_buttons_view.gameObject);
            AddDeltaY(side_arrows_delta_y, LobbyController.Instance.Lobby_view.m_room_name_image.gameObject);

            // Footer
            m_bottom_panel_holder.GetComponent<RectTransform>().localPosition = new Vector3(0f, -40f, 0f);
            m_footer_left.sizeDelta = new Vector2(920f, 150f);
            m_footer_right.sizeDelta = new Vector2(920f, 150f);
            m_hourly_bonus_view.AdjustSizeForIpad();

            // Header
            m_lobby_top_controller.AdjustSizeForIpad();

            // Left and right mask lines
            m_swipe.AdjustSizeForIpad();


            // Snow
            m_imsBanner.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_imsBanner.GetComponent<RectTransform>().anchoredPosition.x, 320f);
        }

        private void AddDeltaY(float delta, GameObject go)
        {
            RectTransform temp_rt = go.GetComponent<RectTransform>();
            go.GetComponent<RectTransform>().localPosition = new Vector3(temp_rt.localPosition.x, temp_rt.localPosition.y + delta, temp_rt.localPosition.z);
        }

        /// <summary>
        /// Opens the highest table and show table unlock if needed.
        /// </summary>
        /// <returns><c>true</c> if table unlock was played</returns>
		public bool OpenHighestTable(bool showTableUnlock = false, bool goToHighestTable = false, OverlayClosedDelegate unlock_anim_done = null)
        {
            bool result = false;

            ILobbyItem lobby_item = LobbyController.Instance.FindHighestOpenLobbyItem();

            int curr_open_level = lobby_item.GetUnlockLevel();

            LoggerController.Instance.Log("Last Opened Level: " + SpadesStateController.Instance.GetLastOpenItemLevel() + " Current open level: " + curr_open_level);

            if (showTableUnlock && curr_open_level > SpadesStateController.Instance.GetLastOpenItemLevel())
            {
                goToHighestTable = true;
                string world_id = lobby_item.World.GetID();
                int item_level = lobby_item.GetLevel();

                if (lobby_item.IsSingleMatch())
                {
                    SingleMatch single_match = null;
                    foreach (SingleMatch singleitem in (SpadesModelManager.Instance.GetWorld(world_id) as SpadesWorld).GetSingleMatches((ModelManager.Instance as SpadesModelManager).Playing_mode))
                    {
                        if (singleitem.GetLevel() == item_level && !singleitem.Is_special)
                        {
                            single_match = singleitem;
                            break; // Find the first open table, so the table unlock will show the lowest one
                        }
                    }

                    //here need to add a check that if the world ID is over 1 we dont show single (table) unlcok but instead show World unlock
                    if (single_match != null && !SpadesScreensManager.Instance.IsScreenShown)
                    {
                        if (SpadesModelManager.Instance.GetWorld(single_match.World_id).Index < 1)
                        {
                            TableUnlockOverlay unlock_overlay = SpadesOverlayManager.Instance.ShowTableUnlockedOverlay(unlock_anim_done);
                            unlock_overlay.SetData(single_match as SpadesSingleMatch);
                        }
                        else
                        {
                            RoomUnlockedOverlay unlock_overlay = SpadesOverlayManager.Instance.ShowRoomUnlockedOverlay(unlock_anim_done);
                            unlock_overlay.SetData(SpadesModelManager.Instance.GetWorld(single_match.World_id) as SpadesWorld);
                        }

                        //updae mission icons
                        MissionController.Instance.UpdateMissionLobbyIcons();
                    }

                    result = true;
                }
                else
                {
                    if (!SpadesScreensManager.Instance.IsScreenShown)
                    {
                        ArenaUnlockOverlay unlock_overlay = SpadesOverlayManager.Instance.ShowArenaUnlockedOverlay(unlock_anim_done);
                        unlock_overlay.SetData(((ModelManager.Instance as SpadesModelManager).GetWorld(world_id) as SpadesWorld).GetArena(SpadesModelManager.Instance.Playing_mode));
                    }
                }
            }

            SpadesStateController.Instance.SetLastOpenItemLevel(curr_open_level);

            for (int i = 0; i <= lobby_item.World.Index; i++)
            {
                m_world_items[i].UpdateAllItemsDisplay();
            }

            // Only move to highest world if wanted
            if (goToHighestTable)
            {
                if (ModelManager.Instance.InitialWorldId != null && !LobbyController.Instance.AreAllTablesLocked(ModelManager.Instance.InitialWorldId))
                {
                    // Find if there is a promo room with initial world flag
                    World initialPromo = ModelManager.Instance.Worlds.Find(w => w.WorldType == WorldTypes.Promo && w.Promo_iZone.OptionalParam(IMSController.OPTIONAL_PARAM_INITIAL_ROOM) == "1");

                    if (initialPromo!=null)
                    {
                        // Going to promo room world id
                        int pos = ModelManager.Instance.Worlds.FindIndex(w => w.GetID() == initialPromo.GetID());
                        m_swipe.InitSwipe(pos + 1);
                    }
                    else
                    {
                        // Going to initial world id
                        int pos = ModelManager.Instance.Worlds.FindIndex(w => w.GetID() == ModelManager.Instance.InitialWorldId);
                        m_swipe.InitSwipe(pos + 1);
                    }

                }
                else
                {
                    LoggerController.Instance.Log("Going to Highest table with world index: " + lobby_item.World.Index);
                    m_swipe.InitSwipe(lobby_item.World.Index + 1);
                }
            }

            // TODO: This is not good since it overrides the InitialWorldId mechanism. Should go to voucher only after purchasing a voucher!
            //if there is a voucher - go to the table with this voucher (as long as its not locked)
            //CheckVouchersAndGoToHighest();

            return result;

        }

        
        public void CheckVouchersAndGoToHighest()
        {
            SpadesSingleMatch spadesSingleMatch = null;
            //we need to sort all the vouchers by values - and starting from the highest value one - check if the level is not locked to us - and if so go to that table in that world.
            if (ModelManager.Instance.Vouchers.Count > 0)
            {
                List<VouchersGroup> vouchersGroups = ModelManager.Instance.Vouchers;

                // sort list
                vouchersGroups.Sort((x, y) => x.VouchersValue.CompareTo(y.VouchersValue));
                for (int i = vouchersGroups.Count - 1; i >= 0; i--)
                {
                    if (vouchersGroups[i].Count == 0)
                        continue;

                    int value = vouchersGroups[i].VouchersValue;
                    PlayingMode playingMode = (vouchersGroups[i].TableFilterData as SpadesTableFilterData).PlayingMode;
                    //check if the highest voucher is linked to a table that its level is still locked to the user - if not - this is the table to go to
                    spadesSingleMatch = LobbyController.Instance.FindUnlockedTableByBuyIn(value, playingMode);

                    if (spadesSingleMatch != null)
                        break;
                }
            }

            if (spadesSingleMatch != null)
            {
                //we found a voucher that we need to go to
                m_swipe.InitSwipe(spadesSingleMatch.World.Index + 1);
                m_mode_buttons_view.ModeClicked(spadesSingleMatch.Playing_mode == PlayingMode.Solo);
            }
        }

       


        public void UpdateVouchers()
        {
            for (int i = 0; i < m_world_items.Count; i++)
            {
                if (m_world_items[i] != null)
                {
                    for (int k = 0; k < m_world_items[i].Singles_view.Length; k++)
                    {
                        m_world_items[i].Singles_view[k].UpdateVouchers();
                    }
                }
            }
        }

        public void UpdateContestsHats()
        {
            foreach (WorldItemView worldItemView in m_world_items)
            {
                if (worldItemView != null)
                    worldItemView.UpdateContestsHats();
            }
        }

        public void UpdateLeaderboardLock()
        {

            if (SpadesModelManager.Instance.GetUser().GetLevel() < LeaderboardsModel.LEADERBOARD_MIN_LEVEL)
            {
                ShowLeaderboardsLock();
                SpadesStateController.Instance.SetShouldShowLeaderboardsUnlocked();
            }
            else
            {
                HideLeaderboardsLock();
            }

            //check and show leaderboards unlock overlay
            /*if (SpadesModelManager.Instance.GetUser().GetLevel() >= LeaderboardsModel.LEADERBOARD_MIN_LEVEL &&
                SpadesStateController.Instance.GetShouldShowLeaderboardsUnlocked() &&
                !SpadesScreensManager.Instance.IsScreenShown &&
                !SpadesOverlayManager.Instance.IsOverlayShown)
            {

                SpadesStateController.Instance.ClearShouldShowLeaderboardsUnlocked();

                SpadesOverlayManager.Instance.ShowUnlockLeaderboardsOverlay();
            }*/
        }

        public void UpdateContestsLock(OverlayClosedDelegate unlock_anim_done = null)
        {
            if (SpadesModelManager.Instance.GetUser().GetLevel() < ContestsController.Instance.Min_contests_level)
            {
                SpadesStateController.Instance.SetShouldShowContestsUnlocked();
            }
            //check and show leaderboards unlock overlay
            if (SpadesModelManager.Instance.GetUser().GetLevel() >= ContestsController.Instance.Min_contests_level &&
                SpadesStateController.Instance.GetShouldShowContestsUnlocked() &&
                !SpadesScreensManager.Instance.IsScreenShown &&
                !SpadesOverlayManager.Instance.IsOverlayShown)
            {

                SpadesStateController.Instance.ClearShouldShowContestsUnlocked();

                SpadesOverlayManager.Instance.ShowContestsUnlockedOverlay(unlock_anim_done);
            }
        }

        public bool ShouldShowMissionUnlockOverlay()
        {
            int curr_level = SpadesModelManager.Instance.GetUser().GetLevel();

            if (curr_level < MissionController.Instance.Min_mission_level)
            {
                SpadesStateController.Instance.SetShouldShowMissionUnlockOverlay(true);
                return false;
            }
            else
            {
                //we are above the min level - check if user ever saw the popup
                if (SpadesStateController.Instance.GetShouldShowMissionUnlockedOverlay() && !SpadesScreensManager.Instance.IsScreenShown) //&& !SpadesOverlayManager.Instance.IsOverlayShown)
                {
                    SpadesStateController.Instance.SetShouldShowMissionUnlockOverlay(false);
                    return true;
                }
            }

            return false;
        }


        public void HandleLobbyChallengeButtonAndClaimIndication()
        {
            bool lobby_button_locked = !MissionController.Instance.UserOverMissionMinLevel();

            //m_Challenge_button.interactable = lobby_button_active;
            m_challenge_will_unlock.SetActive(lobby_button_locked);

            m_challenge_unlock_text.text = "LVL " + MissionController.Instance.Min_mission_level;

            bool m_show_challenge_collect_indication = MissionController.Instance.ShowMissionCollectIndication();

            m_challenge_collect_ready.SetActive(m_show_challenge_collect_indication);

        }

        public void BT_MiniGameClicked()
        {
            if (ModelManager.Instance.GetUser().GetLevel() < SideGamesController.Instance.MinCasinoLevel)
            {
                PopupManager.Instance.ShowMessagePopup("CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + SideGamesController.Instance.MinCasinoLevel + "!");
                return;
            }

            CardGamesSoundsController.Instance.ButtonClicked();
            SideGamesController.Instance.StartMiniGame(SideGamesController.SideGames.Slot3);

            TrackingManager.Instance.LogFirebaseEvent("FooterBtn", new Dictionary<string, string> { ["button"] = "RoyalSlots" });
        }

        public void BT_ChallengesClick()
        {

            CardGamesSoundsController.Instance.ButtonClicked();

            MissionController.Instance.MissionButtonClicked();

            TrackingManager.Instance.LogFirebaseEvent("FooterBtn", new Dictionary<string, string>{["button"] = "Missions"});
        }

        public void BT_ContestsClick()
        {
            CardGamesSoundsController.Instance.ButtonClicked();
            // Level check is in contests controller
            m_contests_collect_ready.SetActive(false);
            ContestsController.Instance.ShowContestsPopup();

            TrackingManager.Instance.LogFirebaseEvent("FooterBtn", new Dictionary<string, string> { ["button"] = "Contests" });
        }

        public void BT_LeaderboardClick()
        {
            CardGamesSoundsController.Instance.ButtonClicked();
            StopAllCoroutines();

            if (SpadesModelManager.Instance.GetUser().GetLevel() < LeaderboardsModel.LEADERBOARD_MIN_LEVEL)
            {
                //  PopupManager.Instance.ShowMessagePopup("CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + LeaderboardsModel.LEADERBOARD_MIN_LEVEL + "!", PopupManager.AddMode.ShowAndRemove, true);
            }
            else
            {
                StartCoroutine(LeaderboardsController.Instance.StartLeaderboardCacheTimer());
                LeaderboardsController.Instance.ShowLeaderboard(LeaderboardsModel.LeaderboardType.GlobalCurrent);
            }
        }


        public void SettingsClicked()
        {
            CardGamesSoundsController.Instance.ButtonClicked();

            SpadesPopupManager.Instance.ShowSettingsPopup();
        }

        public void BT_RewardedVideoClick()
        {
            // Get the iZone for the video banner
            IMSInteractionZone videoBanner = null;
            if (IMSController.Instance.BannersByLocations.ContainsKey(LobbyButtonsGroupView.VIDEO_BUTTON_BANNER_LOCATION))
            {
                videoBanner = IMSController.Instance.BannersByLocations[LobbyButtonsGroupView.VIDEO_BUTTON_BANNER_LOCATION][0];
            }

            CardGamesSoundsController.Instance.ButtonClicked();
            VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedVideoBonus, videoBanner);
        }

        public void BT_InviteFriendsClick()
        {
            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndRemove, SocialPopup.SocialTab.Invite);
        }

        public void BT_SocialClick()
        {
            CardGamesSoundsController.Instance.ButtonClicked();
            SocialController.Instance.ShowSocialPopup(PopupManager.AddMode.ShowAndKeep);

            TrackingManager.Instance.LogFirebaseEvent("FooterBtn", new Dictionary<string, string> { ["button"] = "Social" });
        }

        private void OnEnable()
        {
            if (AndroidBackButtonListener.Instance != null)
                AndroidBackButtonListener.Instance.BackButtonClicked += OnBackButtonClicked;
        }

        private void OnDisable()
        {
            if (AndroidBackButtonListener.Instance != null)
                AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
        }

        private void OnBackButtonClicked()
        {
#if !UNITY_WEBGL
            if (!PopupManager.Instance.IsPopupShown &&
                !SpadesOverlayManager.Instance.IsOverlayShown &&
                !SpadesScreensManager.Instance.IsScreenShown)
            {
                SpadesPopupManager.Instance.ShowQuestionPopup(PopupManager.AddMode.ShowAndRemove, null, "Are you sure you want to leave Spades Royale?", "Stay", "Leave", null,
                                                              () =>
                                                              {
                                                                  Application.Quit();
                                                              }
                                                              );
            }
#endif
        }

        public void ShowLeaderboardsLock()
        {
            LobbyButtonsGroups.ShowLeaderboardsLock();
        }

        public void HideLeaderboardsLock()
        {
            LobbyButtonsGroups.HideLeaderboardsLock();
        }

        public LobbyButtonsGroupView LobbyButtonsGroups
        {
            get
            {
                return m_bottom_panel_holder.GetComponent<LobbyButtonsGroupView>();
            }
        }

        public GameObject Loading_screen
        {
            get
            {
                return m_loading_screen;
            }
        }

        public ProgressView Progress_bar_view
        {
            get
            {
                return m_progress_bar_view;
            }
            set
            {
                m_progress_bar_view = value;
            }
        }

        public GameObject Items
        {
            get
            {
                return m_items;
            }
            set
            {
                m_items = value;
            }
        }

        public int Effect_index
        {
            get
            {
                return m_effect_index;
            }
        }

        public List<WorldItemView> World_items
        {
            get
            {
                return m_world_items;
            }
        }

        public List<Sprite> Single_panels
        {
            get
            {
                return m_single_panels;
            }
        }

        public List<Sprite> Arena_panels
        {
            get
            {
                return m_arena_panels;
            }
        }


        public List<Sprite> Locked_single
        {
            get
            {
                return m_locked_single;
            }
        }


        public GameObject PromoRoom_item_prefab
        {
            get
            {
                return m_promoRoom_item_prefab;
            }
        }

        public List<Sprite> World_bgs
        {
            get
            {
                return m_world_bgs_with_promo;
            }
        }


        public List<GameObject> Gems_objects
        {
            get
            {
                return m_gems_objects;
            }
        }

        public List<Vector3> Gems_objects_positions
        {
            get
            {
                return m_gems_objects_positions;
            }
        }

        public GameObject Lobby_banner
        {
            get
            {
                return m_lobby_banner;
            }
        }

        public GameObject Side_arrows
        {
            get
            {
                return m_side_arrows;
            }
        }

        public List<Sprite> Gems_lobby_sprite
        {
            get
            {
                return m_gems_lobby_sprite;
            }
        }

        public List<TMP_ColorGradient> Room_title_gradient
        {
            get
            {
                return m_room_title_gradient;
            }
        }

        public List<Color> Lobby_highlight_colors
        {
            get
            {
                return m_lobby_highlight_colors;
            }
        }

        public ModeButtonsView Mode_buttons_view
        {
            get
            {
                return m_mode_buttons_view;
            }
        }

        public GameObject Contests_collect_ready { get => m_contests_collect_ready; set => m_contests_collect_ready = value; }
        public List<Sprite> Room_name_images { get => m_room_name_images; }
        public List<Sprite> Tiny_gems_on_lobby_sprite { get => m_tiny_gems_on_lobby_sprite; }
        public List<Sprite> Tiny_gems_off_lobby_sprite { get => m_tiny_gems_off_lobby_sprite;  }
    }
}
