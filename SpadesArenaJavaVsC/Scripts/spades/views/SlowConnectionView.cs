﻿namespace spades.views

{

	public class SlowConnectionView : MonoBehaviour 
	{
		[SerializeField]GameObject		m_spinning_spade=null;
		bool 							m_showing=false;

		float m_orig_y;


        private void Start()
        {
            this.gameObject.SetActive(false);
        }

        public void Show()
		{

			// Reset the starting position, since iTween has problems animating anchored objects
			m_orig_y = -ManagerView.Instance.Actual_height / 2 - 60;
			gameObject.transform.localPosition = new Vector3(0,m_orig_y);

			if (m_showing)
				return;

			this.gameObject.SetActive (true);
			

            iTween.ScaleTo (m_spinning_spade, iTween.Hash ("x", 1.1,"y", 1.1, "easeType", "easeOutCubic", "time", .5, "looptype", "pingPong"));
			iTween.MoveTo (this.gameObject, iTween.Hash ("y", m_orig_y+80, "easeType", "easeInQuad", "time", .2,"islocal",true));

			m_showing = true;
		}

		public void Hide()
		{
			iTween.MoveTo (this.gameObject, iTween.Hash ("y", m_orig_y, "easeType", "easeOutQuad","time", .2,"islocal", true, "oncomplete", "HideDone","Oncompletetarget",this.gameObject));
		}

		public void HideDone()
		{
			m_showing = false;
			this.gameObject.SetActive (false);
		}

	}
}