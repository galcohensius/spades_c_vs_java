using common.models;
using cardGames.models;

namespace spades.views
{
	public class LeaderboardsRightPanel : MonoBehaviour
	{

        [SerializeField] GameObject m_ranks = null;
        [SerializeField] GameObject m_hof = null;
        [SerializeField] GameObject m_rewards = null;
        [SerializeField] FriendsBannerView m_friends_banner = null;
		[SerializeField] GameObject m_flayer = null;
		[SerializeField] LeaderboarRightPanelTitle m_rightPanelTitle = null;

      
		public void SwitchTab (LeaderboardsModel.LeaderboardType lt)
		{
			if ((LeaderboardsModel.LeaderboardType)lt == LeaderboardsModel.LeaderboardType.HallOfFame) {
				m_friends_banner.gameObject.SetActive (false);
				m_rewards.SetActive (false);
				m_hof.SetActive (true);
               
				m_flayer.SetActive (false);
				return;
			} else {
				gameObject.SetActive (true);
				m_hof.SetActive (false);
				
				if (lt == LeaderboardsModel.LeaderboardType.Friends) {
					m_friends_banner.gameObject.SetActive (true);
					m_friends_banner.Init ();

					m_rewards.SetActive (false);
					m_flayer.SetActive (false);
				} else {
					m_friends_banner.gameObject.SetActive (false);

					if (ModelManager.Instance.Leaderboards.GetLeaderboard (lt).LeaderboardMetaData == null) {
						m_rewards.SetActive (false);
						m_flayer.SetActive (false);
					} else {
						m_rewards.SetActive (true);
						m_flayer.SetActive (true);
					}
				}
			}
		}

		public void UpdateRanksView (LeaderboardsModel.LeaderboardType lt)
		{

			if (lt != LeaderboardsModel.LeaderboardType.GlobalCurrent && lt != LeaderboardsModel.LeaderboardType.GlobalPrevious)
				return;
			
			m_rewards.SetActive (true);
			m_flayer.SetActive (true);

			LeaderboardsRanks ranksScript = m_ranks.GetComponent<LeaderboardsRanks> ();
			ranksScript.UpdateRanksView (lt);

			m_rightPanelTitle.SetTitle (lt);
			
		}


		public void Show(bool show)
		{
			gameObject.GetComponent<CanvasGroup> ().alpha = show?1:0;
		}
	}
}
