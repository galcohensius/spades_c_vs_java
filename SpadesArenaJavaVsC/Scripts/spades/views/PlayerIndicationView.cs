﻿using spades.models;

namespace spades.views
{
    public class PlayerIndicationView : MonoBehaviour
    {

        [SerializeField] GameObject m_bid_bg_object = null;
        [SerializeField] TMP_Text m_partnerBidText = null;
        [SerializeField] TMP_Text m_soloBidText = null;

        [SerializeField] GameObject m_solo_mode = null;
        [SerializeField] GameObject m_partner_mode = null;

        [SerializeField] TMP_Text m_solo_bags_text = null;
        [SerializeField] TMP_Text m_solo_pts_text = null;

        [SerializeField] Image m_timer = null;

        [SerializeField] GameObject m_playerSoloHighlight = null;
        [SerializeField] GameObject m_playerPartnerHighlight = null;
        [SerializeField] GameObject m_playerTimer = null;
        [SerializeField] GameObject m_playerGlow = null;

        [SerializeField] Sprite m_timerImage = null;
        [SerializeField] Sprite m_timerFinishedImage = null;


        public void SetGameMode(bool solo, bool is_player)
        {
            m_solo_mode.SetActive(solo);
            m_partner_mode.SetActive(!solo);

            // Set the timer pos
            if (solo)
                m_playerTimer.transform.localPosition = new Vector2(0, -92);
            else
                m_playerTimer.transform.localPosition = new Vector2(0, -13);
        }

        public void TogglePlayerHighlight(bool isActive)
        {
            m_playerTimer.SetActive(isActive);
            //m_playerGlow.SetActive(isActive);

            if (SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo)
            {
                m_playerSoloHighlight.SetActive(isActive);
            }
            else
            {
                m_playerPartnerHighlight.SetActive(isActive);
            }
        }

        public void SetTimer(float value)
        {
            m_timer.fillAmount = value;
        }

        public void SetStartTimerImage()
        {
            m_timer.sprite = m_timerImage;
        }

        public void SetEndTimerImage()
        {
            m_timer.sprite = m_timerFinishedImage;
        }


        public void SetPointsAndBags(int points, int bags)
        {
            m_solo_pts_text.text = string.Format("{0} Pt.", points);
            m_solo_bags_text.text = bags.ToString();
        }

        public void SetBidAndTakes(string bid,int takes)
        {
            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch();
            if (activeMatch.GetMatch().Playing_mode == PlayingMode.Partners)
            {
                m_partnerBidText.text = string.Format("{0}/{1}", takes, bid);
                SetAlpha(m_partnerBidText, 1);
            }
            else
            {
                m_soloBidText.text = string.Format("{0}/{1}", takes, bid);
                SetAlpha(m_soloBidText, 1);
            }
        }

        public void SetInitialBidsAndTakes()
        {
            SpadesActiveMatch activeMatch = SpadesModelManager.Instance.GetActiveMatch();
            if (activeMatch.GetMatch().Playing_mode == PlayingMode.Partners)
            {
                m_partnerBidText.text = "-/-";
                SetAlpha(m_partnerBidText, 0.5f);
            }
            else
            {
                m_soloBidText.text = "-/-";
                SetAlpha(m_soloBidText, 0.5f);
            }
        }

        private void SetAlpha(TMP_Text text,float alpha)
        {
            Color color = text.color;
            color.a = alpha;
            text.color = color;
        }

    }
}
