﻿using cardGames.controllers;
using cardGames.models;
using common;
using common.controllers;
using common.models;
using common.utils;
using common.views;
using spades.controllers;
using spades.models;

namespace spades.views
{
    public class SingleItemView : MonoBehaviour
    {

        Color LOCKED_COLOR = new Color(90 / 255f, 90 / 255f, 90 / 255f, 1f);
        Color OPEN_COLOR = new Color(1f, 1f, 1f, 1f);


        [SerializeField] GameObject m_mission_indication = null;
        [SerializeField] TMP_Text m_buyin_text = null;
        [SerializeField] TMP_Text m_buyin_title_text = null;
        [SerializeField] GameObject m_locked_object = null;
        [SerializeField] Image m_panel_BG = null;
        [SerializeField] GameObject m_buy_in_object = null;



        [SerializeField] TMP_Text m_points_text = null;
        [SerializeField] TMP_Text m_playing_mode_text = null;

        [SerializeField] TMP_Text m_bags_penatly_text = null;
        [SerializeField] TMP_Text m_bags_count_text = null;
        [SerializeField] GameObject m_bags_object = null;


        [SerializeField] RectTransform m_holder = null;
        [SerializeField] ButtonsOverView m_button_overview = null;
        [SerializeField] VoucherView m_voucherView;

        SpadesSingleMatch m_single_match;
        Sprite m_unlocked_panel;
        Sprite m_locked_panel;

        WorldItemView m_world_item_view = null;

        // Use this for initialization
        public void SetInitialData(SpadesSingleMatch single_match, Sprite locked_panel, Sprite unlocked_panel, WorldItemView world_item_view = null)
        {
            if (world_item_view != null)
                m_world_item_view = world_item_view;

            m_locked_panel = locked_panel;
            m_unlocked_panel = unlocked_panel;
            m_single_match = single_match;


            ResetPanel();

            SwitchModel(m_single_match);

            m_voucherView.gameObject.SetActive(false);
        }

        //panel pointer up and down "eat" the mouse events before ButtonOverView - and so they give information about the clicks to the button overview if it exists
        public void PanelPointerDown()
        {
            if (Input.GetMouseButtonDown(0) && m_world_item_view.Is_rotating == false)
            {

                LobbyController.Instance.Lobby_view.GetSwipe().StartSwipe(StartSignleGame);

                if (m_button_overview != null)
                    m_button_overview.OnPointerClick(null);
            }
        }

        //panel pointer up and down "eat" the mouse events before ButtonOverView - and so they give information about the clicks to the button overview if it exists
        public void PanelPointerUp()
        {
            if (Input.GetMouseButtonUp(0))
            {
                LobbyController.Instance.Lobby_view.GetSwipe().EndSwipe();

                if (m_button_overview != null)
                    m_button_overview.OnPointerClickUp();
            }
        }

        public void SwitchModel(SingleMatch single_match)
        {

            m_single_match = single_match as SpadesSingleMatch;

            UpdateDisplay();

        }

        public void SwitchModel(SpadesSingleMatch single_match)
        {

            m_single_match = single_match;

            UpdateDisplay();

        }





        private void ResetPanel()
        {
            m_holder.localEulerAngles = new Vector3();
            GetComponent<RectTransform>().localEulerAngles = new Vector3();
        }

        public void UpdateChallengeIndication()
        {
            if (m_mission_indication != null)
                m_mission_indication.SetActive(m_single_match.Active_in_challenge && !IsLocked());

        }



        public void UpdateDisplay()
        {
            if (m_single_match.Playing_mode == PlayingMode.Partners)
                m_playing_mode_text.text = "<sprite=5> PARTNERS";
            else
                m_playing_mode_text.text = "<sprite=6> SOLO";

            UpdateChallengeIndication();

            m_buyin_text.text = "<sprite=3>" + FormatUtils.FormatBuyIn(m_single_match.Buy_in);

            m_points_text.text = m_single_match.LowPoints + "/" + m_single_match.HighPoints;
            m_bags_count_text.text = m_single_match.BagsCount.ToString();
            m_bags_penatly_text.text = m_single_match.BagsPanelty + " pt.";

            User user = ModelManager.Instance.GetUser();

            SetLocked(IsLocked());

            UpdateVouchers();
        }

        private bool IsLocked()
        {
            if (ModelManager.Instance.GetUser().GetLevel() < m_single_match.GetLevel() || m_single_match.GetLevel() == -1)
                return true;
            else
                return false;
        }

        private void SetLocked(bool locked)
        {
            if (locked)
            {
                m_points_text.color = LOCKED_COLOR;

                m_bags_count_text.color = LOCKED_COLOR;
                m_bags_penatly_text.color = LOCKED_COLOR;
                m_buyin_title_text.color = LOCKED_COLOR;

                m_playing_mode_text.color = LOCKED_COLOR;

                //m_playing_mode_text.gameObject.SetActive (true);

                if (m_single_match != null && m_buyin_text != null)
                {

                    m_buyin_text.color = LOCKED_COLOR;
                    m_buyin_text.text = "<sprite=4>" + FormatUtils.FormatBuyIn(m_single_match.Buy_in);
                    m_panel_BG.sprite = m_locked_panel;
                    m_locked_object.SetActive(true);

                    if (m_single_match.GetLevel() >= 0)
                        m_locked_object.transform.GetChild(0).GetComponent<TMP_Text>().text = "Level " + m_single_match.GetLevel();
                    else
                        m_locked_object.transform.GetChild(0).GetComponent<TMP_Text>().text = "Locked";
                }

                //disables the button overview
                if (m_button_overview != null)
                    m_button_overview.Disabled = true;

            }
            else
            {

                m_panel_BG.sprite = m_unlocked_panel;

                m_points_text.color = Color.white;

                m_bags_count_text.color = Color.white;
                m_bags_penatly_text.color = Color.white;
                m_buyin_title_text.color = Color.white;

                m_playing_mode_text.color = Color.white;

                if (m_single_match != null && m_buyin_text != null)
                {

                    m_buyin_text.color = Color.white;
                    m_buyin_text.gameObject.SetActive(true);
                    m_buyin_text.text = "<sprite=3>" + FormatUtils.FormatBuyIn(m_single_match.Buy_in);
                }
                m_locked_object.SetActive(false);

                //disables the button overview
                if (m_button_overview != null)
                    m_button_overview.Disabled = false;

            }



        }

        public void StartSignleGame()
        {
            if (m_world_item_view != null)
            {
                if (!m_world_item_view.Is_rotating)
                {
                    if (m_locked_object.activeSelf)
                    {
                        string message_to_pass = "CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + m_single_match.GetLevel() + "!";
                        if (m_single_match.GetLevel() == -1)
                            message_to_pass = "This table is in the oven...\nCome back soon to join the fun!";

                        PopupManager.Instance.ShowMessagePopup(message_to_pass);
                    }
                    else
                    {
                        CardGamesSoundsController.Instance.ButtonClicked();
                        SingleMatchController.Instance.StartSingleMatch(m_single_match);
                    }
                }
            }

        }

        public void UpdateVouchers()
        {
            // TODO: There are some single item views which are detatched from the prefab and they don't have the voucher view
            if (m_voucherView == null)
                return;
            // Check if there are vouchers for the connected match
            VouchersGroup voucher = SpadesVoucherController.Instance.GetVouchersBySpadesMatch(m_single_match);
            if (voucher != null && voucher.Count > 0 && ModelManager.Instance.GetUser().GetLevel() >= m_single_match.Min_level)
            {
                m_voucherView.gameObject.SetActive(true);
                m_voucherView.SetData(voucher);

                voucher.OnVoucherUpdate -= UpdateVouchers;
                voucher.OnVoucherUpdate += UpdateVouchers;
            }
            else
            {
                m_voucherView.gameObject.SetActive(false);
            }
        }

        //this is for the unlocking animation
        public void HideLock()
        {
            m_locked_object.SetActive(false);

        }

        public void SetToLocked()
        {


        }

        public SpadesSingleMatch Single_match
        {
            get
            {
                return m_single_match;
            }
            set
            {
                m_single_match = value;
            }
        }



        public RectTransform Holder
        {
            get
            {
                return m_holder;
            }
        }


        public Image Panel_BG
        {
            get
            {
                return m_panel_BG;
            }

        }

        public TMP_Text Points_text
        {
            get
            {
                return m_points_text;
            }

        }

        public TMP_Text Playing_mode_text
        {
            get
            {
                return m_playing_mode_text;
            }
        }

        public GameObject Buy_in_object
        {
            get
            {
                return m_buy_in_object;
            }
        }

        public GameObject Bags_object
        {
            get
            {
                return m_bags_object;
            }
        }

        public GameObject Mission_indication { get => m_mission_indication; }
    }
}