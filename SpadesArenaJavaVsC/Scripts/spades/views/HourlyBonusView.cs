using System.Collections;
using common.models;
using spades.controllers;
using common.controllers;
using System;
using common.utils;
using cardGames.models;
using cardGames.controllers;
using cardGames.views.overlays;
using common.ims;

namespace spades.views
{
    public class HourlyBonusView : MonoBehaviour
    {
        public const string HOURLY_VIDEO_BANNER = "V2";

        

        [SerializeField] GameObject m_bg = null;
        [SerializeField] GameObject m_text_bg = null;

        [SerializeField] GameObject m_particle_system = null;
        [SerializeField] GameObject m_bounce_particle_system = null;

        //we have 2 prefabs because they are already in animation so we cannot give them a new parent and hide both with one parent.
        [SerializeField] GameObject m_bonus_glow_coins = null;
        [SerializeField] GameObject m_bonus_glow_coins1 = null;



        //bonus related
        [SerializeField] TMP_Text m_button_text = null;

        [SerializeField] GameObject m_claim_button = null;
        [SerializeField] GameObject m_free_spin_button = null;

        [SerializeField] GameObject m_comet_starting_point = null;

        [SerializeField] GameObject m_coins_stack = null;
        [SerializeField] GameObject m_coins_stack_holder = null;

        [SerializeField] GameObject m_free_spin_timer = null;
        [SerializeField] TMP_Text m_free_spin_timer_text = null;

        [SerializeField] GameObject m_free_spin_ready_image = null;


        [SerializeField] GameObject m_video_button = null;
        [SerializeField] TMP_Text m_video_button_amount_text = null;

        float m_xp_icon_width = 42f;

        Coroutine m_bonus_coroutine = null;
        Coroutine m_free_spin_timer_coroutine = null;

        int m_slotTimerPreviewInterval; // How long to show the daily bonus timer before the daily bonus is available
        int m_hourlyVideoBonusButtonInterval; // How long to show the video button after the hourly is claimed

        bool m_hourlyBonusTaken = false;

        void Awake()
        {
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_slotTimerPreviewInterval = LocalDataController.Instance.GetSettingAsInt("slotTimerIntervalTimeSec", 60 * 60 * 2);
                m_hourlyVideoBonusButtonInterval = LocalDataController.Instance.GetSettingAsInt("hourlyVideoBonusButtonIntervalTimeSec", 60 * 10);
            };
        }


        public void UpdateBonusView()
        {
            if (m_bonus_coroutine != null)
            {
                StopCoroutine(m_bonus_coroutine);
                m_bonus_coroutine = null;
            }
            if (m_free_spin_timer_coroutine != null)
            {
                StopCoroutine(m_free_spin_timer_coroutine);
                m_free_spin_timer_coroutine = null;
            }

            // Get the bonuses
            HourlyBonus bonus = (HourlyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Hourly);
            DailyBonus dailyBonus = (DailyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Daily);

            // Hide the different parts
            m_free_spin_button.SetActive(false);
            m_free_spin_timer.SetActive(false);
            m_free_spin_ready_image.SetActive(false);

            m_video_button.SetActive(false);

            // Handle daily bonus
            if (dailyBonus.IsEntitled)
            {
                // Show the claim button
                m_claim_button.SetActive(false);
                m_free_spin_button.SetActive(true);
                m_free_spin_ready_image.SetActive(true);
                m_button_text.text = "free daily spin";
                MoveCoinsUp();

                return;
            }
            else
            {
                // Timer
                if (isActiveAndEnabled)
                    m_free_spin_timer_coroutine = StartCoroutine(StartDailyBonusTimer(dailyBonus));
            }

            if (bonus.NextBonusTime < DateTime.Now)
            {
                //Hourly bonus is availiable
                m_button_text.color = Color.white;

                //set notification for forgot to collect
                NotificationController.Instance.ScheduleForgotHourlyBonusNotification();

                ShowClaimButtonHideCounter(true, "Collect <sprite=1>" + FormatUtils.FormatPrice(bonus.CoinsSingle));

            }
            else
            {

                ShowClaimButtonHideCounter(false);
                if (isActiveAndEnabled)
                    m_bonus_coroutine = StartCoroutine(StartBonusTimer(bonus.NextBonusTime));

            }
        }

        private void ShowClaimButtonHideCounter(bool show, string bonus_text = "")
        {

            if (LobbyController.Instance.Lobby_view.gameObject.activeSelf)
            {
                if (show)
                {
                    m_button_text.text = bonus_text;

                    //replacing the icon sign in the shadow text with an empty image
                }

            }
            m_claim_button.SetActive(show);

            if (!show)
            {
                //means they need to go down in animation
                MoveCoinsDown();
            }
            else
            {
                MoveCoinsUp();

            }

        }

        private void MoveCoinsUp()
        {
            m_particle_system.SetActive(true);
            m_bounce_particle_system.SetActive(true);
            iTween.MoveTo(m_coins_stack, iTween.Hash("y", -10f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", .5));
        }


        private void MoveCoinsDown()
        {
            m_particle_system.SetActive(false);
            m_bounce_particle_system.SetActive(false);
            iTween.MoveTo(m_coins_stack, iTween.Hash("y", -300f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", 0.5));

        }


        IEnumerator StartBonusTimer(DateTime nextBonusTime)
        {
            TimeSpan span = nextBonusTime - DateTime.Now;

            float startTime = Time.realtimeSinceStartup;

            HourlyBonus bonus = (HourlyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Hourly);
            RewardedVideoBonus videoBonus = (RewardedVideoBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.RewardedHourlyVideoBonus);
            bool shouldShowVideo = IMSController.Instance.BannersByLocations.ContainsKey(HOURLY_VIDEO_BANNER) &&
                videoBonus!=null && videoBonus.IsEntitled && m_hourlyBonusTaken && Application.platform != RuntimePlatform.WebGLPlayer;

            while (span.TotalSeconds >= 0)
            {
                if (shouldShowVideo && (BonusController.HOURLY_BONUS_INTERVAL - span).TotalSeconds<m_hourlyVideoBonusButtonInterval)
                {
                    // Show the video bonus button instead
                    if (!m_video_button.activeSelf)
                    {
                        ShowHourlyVideoBonusButton(videoBonus);

                        // Init VideoAdController
                        VideoAdController.Instance.InitIronSourceAgentForRewardedVideo(ModelManager.Instance.GetUser());
                    }

                } else
                {
                    m_hourlyBonusTaken = false;
                    m_video_button.SetActive(false);

                    m_button_text.text = string.Format("<sprite=1>{0} in {1:hh\\:mm\\:ss}", bonus.CoinsSingle, span);
                }

                yield return new WaitForSeconds(0.1f); // Wait for less than a second so the two timers look synchronized

                span = span.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;

            }

            UpdateBonusView();
        }

        

        IEnumerator StartDailyBonusTimer(DailyBonus dailyBonus)
        {

            int timeToNextBonus = (int)(dailyBonus.NextBonusTime - DateTime.Now).TotalSeconds;

            // check delay until timer should be shown
            int delay = Math.Max(timeToNextBonus - m_slotTimerPreviewInterval, 0);
            if (delay > 0)
                yield return new WaitForSecondsRealtime(delay);


            m_free_spin_timer.SetActive(true);

            // Start the clock
            TimeSpan span = dailyBonus.NextBonusTime - DateTime.Now;

            float startTime = Time.realtimeSinceStartup;

            while (span.TotalSeconds >= 0)
            {

                m_free_spin_timer_text.text = string.Format("free spin: {0:hh\\:mm\\:ss}", span);

                yield return new WaitForSeconds(0.1f); // Wait for less than a second so the two timers look synchronized

                span = span.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;

            }

            // Manually set entitled 
            dailyBonus.IsEntitled = true;

            UpdateBonusView();

        }

        private void ShowHourlyVideoBonusButton(RewardedVideoBonus videoBonus)
        {
            m_video_button.SetActive(true);

            // Set the bonus amount
            int amount = videoBonus.CoinsSingle;
            m_video_button_amount_text.text = "<sprite=1>" + FormatUtils.FormatBuyIn(amount);
        }

        public void ClaimHourlyBonusButtonClicked()
        {
            //m_particle_system.SetActive (true);
            //m_bounce_particle_system.SetActive (false);
            UnityMainThreadDispatcher.Instance.DelayedCall(3.5f, () =>
            {
                IMSController.Instance.TriggerEvent(IMSController.EVENT_BONUS_AWARD,true);
            });

            CardGamesSoundsController.Instance.CollectBonus();

            //do some particle effect from that box

            m_claim_button.SetActive(false);

            HourlyBonus bonus = (HourlyBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.Hourly);
            m_button_text.text = "<sprite=1>" + FormatUtils.FormatPrice(bonus.CoinsSingle) + " Collected";

            float pos_x2 = m_button_text.gameObject.GetComponent<RectTransform>().localPosition.x - m_button_text.preferredWidth / 2 + 25f;

            m_comet_starting_point.GetComponent<RectTransform>().localPosition = new Vector3(pos_x2, 55f, 0);

            BonusController.Instance.ClaimBonus(Bonus.BonusTypes.Hourly, 0, 0, null, 0, BonusClaimed);

            m_hourlyBonusTaken = true;
        }

        public void HourlyVideoBonusButtonClicked()
        {
            VideoAdController.Instance.ShowRewardedVideo(Bonus.BonusTypes.RewardedHourlyVideoBonus);

            m_hourlyBonusTaken = false;

            UpdateBonusView();
        }

        private void BonusClaimed(bool success, BonusAwarded bonusAwarded)
        {
            if (success)
            {
                PlayCoinsFlightForHourlyBonus();

                // Schedule the next notification
                NotificationController.Instance.ScheduleHourlyNotification();

                // Cancel the forgot notification
                NotificationController.Instance.CancelPendingNotifications(NotificationController.FORGOT_HOURLY_BONUS_NOTIF_ID);

            }
        }

        public void PlayCoinsFlightForHourlyBonus()
        {
            CometOverlay comet = SpadesOverlayManager.Instance.ShowCometOverlay();

            int bonus_amount = ModelManager.Instance.FindBonus(Bonus.BonusTypes.Hourly).CoinsSingle;
            int num_coins_to_show = Mathf.Min(5, bonus_amount);

            comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_comet_starting_point, CardGamesPopupManager.Instance.Lobby_flight_target
                , 0f, 0.15f, 1.5f, CardGamesPopupManager.Instance.Lobby_coins_balance, bonus_amount, CardGamesPopupManager.Instance.Lobby_coins_animator, 3, 1.0f, 0.5f
                , TrailCompleted, new Vector2(-0.25f, -0.5f), new Vector2(0.8f, 0.4f));
        }

        private void TrailCompleted()
        {
            UpdateBonusView();
        }

        public void Btn_FreeSpinBonusClicked()
        {
            SpadesPopupManager.Instance.ShowSlotPopup(() =>
            {
                UpdateBonusView();
            });
        }


        public void AdjustSizeForIpad()
        {

            (m_bg.transform as RectTransform).sizeDelta = new Vector2(712, 170);
            (m_text_bg.transform as RectTransform).sizeDelta = new Vector2(512, 154);

            m_coins_stack_holder.transform.localPosition = new Vector2(0, 40);
            m_free_spin_ready_image.transform.localPosition = new Vector2(0, m_free_spin_ready_image.transform.localPosition.y + 40);
            m_free_spin_timer.transform.localPosition = new Vector2(0, m_free_spin_timer.transform.localPosition.y + 40);

            (m_claim_button.transform as RectTransform).sizeDelta = new Vector2(512, 154);
            (m_free_spin_button.transform as RectTransform).sizeDelta = new Vector2(512, 154);

            (m_video_button.transform as RectTransform).sizeDelta = new Vector2(512, 154);

            m_button_text.transform.localPosition = new Vector2(0, 55);


        }
    }

}
