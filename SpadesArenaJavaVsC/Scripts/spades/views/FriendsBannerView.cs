﻿using common.utils;
using common.models;
using System;
using cardGames.models;
using common.controllers;

namespace spades.views
{
	public class FriendsBannerView : MonoBehaviour
	{
		[SerializeField] TMP_Text m_price_field = null;

		private Bonus m_bonus_model;

		public void Init()
		{
			m_bonus_model = ModelManager.Instance.FindBonus (Bonus.BonusTypes.Invite, 0);

			if (m_bonus_model != null) {
				m_price_field.text = String.Format ("<sprite=1> {0}", FormatUtils.FormatBalance(m_bonus_model.CoinsSingle));
			} else {
				LoggerController.Instance.Log("FriendsBannerView -> No Bonus Model");
			}
		}

	}
}
