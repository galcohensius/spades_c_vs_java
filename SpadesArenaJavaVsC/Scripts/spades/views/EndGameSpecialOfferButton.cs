using System.Collections;
using common.mes;
using System;
using common.utils;
using spades.controllers;
using common.models;
using common.ims.model;
using common.ims;
using cardGames.controllers;
using common.controllers;

namespace spades.views
{
    public class EndGameSpecialOfferButton : MonoBehaviour
    {
        [SerializeField] Image m_bg;
        [SerializeField] TMP_Text m_timer_text = null;
        [SerializeField] TMP_Text m_cost_text = null;
        [SerializeField] TMP_Text m_quantity_text = null;
        [SerializeField] Button m_buyButton = null;

        IEnumerator m_timer_routine = null;
        private MESMaterial m_material = null;
        private Action m_timer_end;
        private Action m_OnPurchaseComplete;


        private IMSInteractionZone m_iZone;

        public void Init(MESMaterial material, Action OnTimerEnd, Action OnPurchaseComplete)
        {
            TimeSpan Total_time_to_wait;
            m_timer_end = OnTimerEnd;
            m_OnPurchaseComplete = OnPurchaseComplete;
            if (material.ChildMaterials.Count > 0)
            {
                m_material = material.ChildMaterials[0];
            }
            else
            {
                m_material = material;
            }

            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (m_material.dynamicContent != null && m_material.dynamicContent.AsArray.Count > 0)
            {
                int coins = m_material.dynamicContent[0]["fQy"].AsInt;
                string price = m_material.dynamicContent[0]["cot"];

                m_quantity_text.text = "GET <sprite=1> " + FormatUtils.FormatBalance(coins);
                m_cost_text.text = "$" + price;

                m_buyButton.onClick.RemoveAllListeners();
                m_buyButton.onClick.AddListener(() =>
                {
                    CashierItem item = new CashierItem();

                    // material can contain multiple action params if there are multiple offers
                    if (m_material.actionParam.IsArray)
                    {
                        JSONNode offer = m_material.actionParam[0];
                        item.Id = offer["pId"];
                        item.External_id = offer["ePId"];

                    }
                    else
                    {
                        item.Id = m_material.actionParam["pId"];
                        item.External_id = m_material.actionParam["ePId"];
                    }


                    LoggerController.Instance.Log("Trying to direct buy item: " + item);

                    CashierController.Instance.BuyPackage(item.Id);
                });

                // Handle the timer
                if (m_material.expired > 0 && m_material.timerVisibility == 1)
                {
                    double diff = DateUtils.TotalSeconds((int)m_material.expired);

                    Total_time_to_wait = TimeSpan.FromSeconds(diff);
                    m_timer_routine = POTimer(Total_time_to_wait);
                    StartCoroutine(m_timer_routine);

                }
                else
                {
                    m_timer_text.gameObject.SetActive(false);
                }
            }

        }

        public void Init(IMSInteractionZone iZone, Sprite sprite, Action OnTimerEnd, Action OnPurchaseComplete)
        {
            this.m_iZone = iZone;

            TimeSpan Total_time_to_wait;
            m_timer_end = OnTimerEnd;
            m_OnPurchaseComplete = OnPurchaseComplete;


            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            int coins = iZone.DynamicContent.Content[0]["gds"].AsInt;
            string price = iZone.DynamicContent.Content[0]["cot"];

            m_quantity_text.text = "GET <sprite=1> " + FormatUtils.FormatBalance(coins);
            m_cost_text.text = "$" + price;

            m_buyButton.onClick.RemoveAllListeners();
            m_buyButton.onClick.AddListener(() =>
            {
                (IMSController.Instance as SpadesIMSController).ExecuteAction(iZone);
            });

            // Handle custom graphics
            if (sprite!=null)
            {
                m_bg.sprite = sprite;
            }

            // Handle the timer
            if(m_iZone.ViewLimit!=null && m_iZone.ViewLimit.Expires > 0)
            {
                double diff = DateUtils.TotalSeconds((int)m_iZone.ViewLimit.Expires);

                Total_time_to_wait = TimeSpan.FromSeconds(diff);
                m_timer_routine = POTimer(Total_time_to_wait);

            }
            else
            {
                m_timer_text.gameObject.SetActive(false);
            }


        }


        private void OnEnable()
        {
            CashierController.Instance.PurchaseCompleted += HandlePurchaseComplete;

            if (m_timer_routine!=null)
            {
                StartCoroutine(m_timer_routine);
            }
        }

        private void OnDisable()
        {
            CashierController.Instance.PurchaseCompleted -= HandlePurchaseComplete;
        }

        private void HandlePurchaseComplete(string packageId,IMSGoodsList goodsList)
        {
            m_OnPurchaseComplete();
        }


        IEnumerator POTimer(TimeSpan timeRemain)
        {

            while (timeRemain.TotalSeconds > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, 1);
                timeRemain = timeRemain.Subtract(ts);

                m_timer_text.text = "ENDS IN: " + timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);
            }

            //switch back to single buy when timer expires
            m_timer_end();
        }

        public bool HasSpecialOffer()
        {
            return m_iZone != null;
        }

        public void ResetSpecialOffer()
        {
            m_iZone = null;
        }
    }
}
