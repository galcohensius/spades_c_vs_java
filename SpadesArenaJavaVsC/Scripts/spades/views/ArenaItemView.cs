using spades.controllers;
using spades.models;
using common.controllers;
using common;
using common.utils;
using cardGames.models;
using cardGames.controllers;
using common.experiments;
using static cardGames.models.World;

namespace spades.views
{
    public class ArenaItemView : MonoBehaviour
    {


        [SerializeField] TMP_Text m_prize_text = null;
        [SerializeField] TMP_Text m_prize_title_text = null;
        [SerializeField] TMP_Text m_arena_challenge_text = null;

        [SerializeField] GameObject m_buy_in_object = null;

        [SerializeField] TMP_Text m_playing_mode_text = null;

        SpadesArena m_arena;

        [SerializeField] Image m_panel_BG = null;
        [SerializeField] Image m_Gem_BG = null;
        [SerializeField] Image m_BG_Image = null;
        [SerializeField] GameObject m_BG_Mask = null;
        Sprite m_panel_sprite = null;
        [SerializeField] GameObject m_locked_text_object = null;

        [SerializeField] RectTransform m_holder = null;
        [SerializeField] RectTransform m_voucher_place_holder = null;

        Vector2 m_startDragPos = new Vector2();
        Vector2 m_endDragPos = new Vector2();

        [SerializeField] GameObject m_gem_holder = null;
        GameObject m_gem_object = null;

        [SerializeField] ButtonsOverView m_button_overview = null;
        [SerializeField] GameObject m_bg_image_mask = null;

        [SerializeField] Image[] m_tinyGems = null;

        Color LOCKED_COLOR = new Color(90 / 255f, 90 / 255f, 90 / 255f, 1f);
        Color OPEN_COLOR = new Color(1f, 1f, 1f, 1f);

        bool m_locked = false;

        private bool m_showAllGems = false;

        WorldItemView m_world_item_view = null;

        //panel pointer up and down "eat" the mouse events before ButtonOverView - and so they give information about the clicks to the button overview if it exists
        public void PanelPointerDown()
        {
            if (Input.GetMouseButtonDown(0) && m_world_item_view.Is_rotating == false)
            {
                LobbyController.Instance.Lobby_view.GetSwipe().StartSwipe(EnterArenaClicked);

#if (!UNITY_EDITOR && !UNITY_WEBGL)  //this deals with the snapping back on clicking which results in screen moving after closing arena lobby 
                m_startDragPos = Input.touches[0].position;
#else
                m_startDragPos = Input.mousePosition;
#endif

                if (m_button_overview != null)
                    m_button_overview.OnPointerClick(null);
            }
        }

        //panel pointer up and down "eat" the mouse events before ButtonOverView - and so they give information about the clicks to the button overview if it exists
        public void PanelPointerUp()
        {
            if (Input.GetMouseButtonUp(0))
            {
#if (!UNITY_EDITOR && !UNITY_WEBGL)//this deals with the snapping back on clicking which results in screen moving after closing arena lobby 
                m_endDragPos = Input.touches[0].position;
#else
                m_endDragPos = Input.mousePosition;
#endif

                float gestureDist = ((Vector2)m_endDragPos - m_startDragPos).magnitude;

                if (gestureDist < 10f)
                    LobbyController.Instance.Lobby_view.GetSwipe().EndSwipe(true);
                else
                    LobbyController.Instance.Lobby_view.GetSwipe().EndSwipe();

                if (m_button_overview != null)
                    m_button_overview.OnPointerClickUp();
            }
        }


        public void UpdateDisplay()
        {
            m_panel_BG.sprite = m_panel_sprite;

            //TODO: if arena payout 

            m_prize_text.text = "<size=100><sprite=3></size>" + FormatUtils.FormatBuyIn(m_arena.GetPayOutCoins());

            if ( m_arena.World.WorldType == WorldTypes.Regular )
            {
                for (int i = 0; i < 3; i++)
                {
                    if (i <= m_arena.World.Arena_Gems_Won - 1 || m_showAllGems)
                       TinyGems[i].sprite = LobbyController.Instance.Lobby_view.Tiny_gems_on_lobby_sprite[m_arena.World.Index];
                    else
                       TinyGems[i].sprite = LobbyController.Instance.Lobby_view.Tiny_gems_off_lobby_sprite[m_arena.World.Index];

                    //TODO: if world is 6 - arena is locked 
                    if(m_world_item_view.World.Index == 5)
                        TinyGems[i].sprite = LobbyController.Instance.Lobby_view.Tiny_gems_off_lobby_sprite[m_arena.World.Index];

                    TinyGems[i].SetNativeSize();
                }
            }

            //TODO: if world is 6 - arena is locked 

            m_locked = ModelManager.Instance.GetUser().GetLevel() < m_arena.GetLevel() || m_arena.GetLevel() == -1 || 
                m_world_item_view.World.Index == 5;

            SetLocked();

            ManageArenaViewExperiment();

        }

        private void ManageArenaViewExperiment()
        {
            string sprite_id = "<sprite=3>";

            if (m_locked)
                sprite_id = "<sprite=4>";

            //id=1 means show the BUYIN instead of prize
            if (ExperimentsManager.Instance.GetValue(LobbyController.ARENA_PANEL_EXPERIMENT_ID) == 2)
            {
                //TODO: if world is 6 - arena needs to be locked
                if (m_arena.GetBuyInCoins()==0)
                {
                    m_prize_title_text.text = "BUY IN";
                    m_prize_text.text = "<size=100>" + sprite_id + "</size>" + FormatUtils.FormatBuyIn(20000000);
                }
                else
                {
                    m_prize_title_text.text = "BUY IN";
                    m_prize_text.text = "<size=100>" + sprite_id + "</size>" + FormatUtils.FormatBuyIn(m_arena.GetBuyInCoins());
                }

            }
            else
            {
                m_prize_title_text.text = "PRIZE";
                m_prize_text.text = "<size=100>" + sprite_id + "</size>" + FormatUtils.FormatBuyIn(m_arena.GetPayOutCoins());
            }
        }

        private void SetLocked()
        {
            if (m_locked)
            {
                m_bg_image_mask.SetActive(false);


                m_prize_text.color = LOCKED_COLOR;
                m_prize_title_text.color = LOCKED_COLOR;
                m_arena_challenge_text.color = LOCKED_COLOR;

                BG_Image.color = LOCKED_COLOR;
                m_panel_BG.color = LOCKED_COLOR;
                m_Gem_BG.color = LOCKED_COLOR;

                m_playing_mode_text.color = LOCKED_COLOR;

                //m_prize_text.text = "<sprite=4>" + FormatUtils.FormatBuyIn(m_arena.GetPayOutCoins());

                if (m_arena.Playing_mode == PlayingMode.Partners)
                    m_playing_mode_text.text = "<sprite=9> PARTNERS";
                else
                    m_playing_mode_text.text = "<sprite=10> SOLO";


                m_panel_BG.SetNativeSize();

                m_locked_text_object.SetActive(true);
                if (m_arena.GetLevel() >= 0)
                    m_locked_text_object.transform.GetChild(0).GetComponent<TMP_Text>().text = "Level " + m_arena.GetLevel();
                else
                    m_locked_text_object.transform.GetChild(0).GetComponent<TMP_Text>().text = "Locked";

                //TODO: make arena in world 6 locked 
                if(m_world_item_view.World.Index == 5)
                    m_locked_text_object.transform.GetChild(0).GetComponent<TMP_Text>().text = "Locked";

                m_gem_holder.SetActive(false);

                //disables the button overview
                if (m_button_overview != null)
                    m_button_overview.Disabled = true;
            }
            else
            {
                m_locked_text_object.SetActive(false);


                m_prize_text.color = OPEN_COLOR;
                m_prize_title_text.color = OPEN_COLOR;
                m_arena_challenge_text.color = OPEN_COLOR;

                BG_Image.color = OPEN_COLOR;

                m_panel_BG.color = OPEN_COLOR;

                m_Gem_BG.color = OPEN_COLOR;

                m_playing_mode_text.color = OPEN_COLOR;

                //m_prize_text.text = "<sprite=3>" + FormatUtils.FormatBuyIn(m_arena.GetPayOutCoins());


                if (m_arena.Playing_mode == PlayingMode.Partners)
                    m_playing_mode_text.text = "<sprite=5> PARTNERS";
                else
                    m_playing_mode_text.text = "<sprite=6> SOLO";


                //m_gem_holder.SetActive(true);

                //disables the button overview
                if (m_button_overview != null)
                    m_button_overview.Disabled = false;
            }
        }



        public void SetModel(SpadesArena arena)
        {
            m_arena = arena;
            UpdateDisplay();

        }

        public void EnterArenaClicked()
        {
            //disable mouse over 
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);

            if (m_locked_text_object.activeSelf)
            {
                string message_to_pass = "CONTENT LOCKED\nThis feature will unlock as soon as you reach Level " + m_arena.GetLevel() + "!";
                if (m_arena.GetLevel() == -1)
                    message_to_pass = "This arena is in the oven...\nCome back soon to join the fun!";

                PopupManager.Instance.ShowMessagePopup(message_to_pass);
            }
            else
            {
                CardGamesSoundsController.Instance.ButtonClicked();
                SpadesArenaController.Instance.Arena_Selected(m_arena);
                LobbyController.Instance.ShowLobby(false);
                LobbyTopController.Instance.ShowHideIndicationPoints(false);
            }


        }

        public void SetModel(Arena arena)
        {
            m_arena = arena as SpadesArena;
            UpdateDisplay();
        }

        public void SetFullArena(SpadesArena arena, Sprite bg_image_unlocked, GameObject gem_object, Sprite gem, bool showAllGems = false)
        {
            //TODO: this is special valentines master ring thing

            m_arena = arena;



            m_showAllGems = showAllGems;
            m_panel_sprite = bg_image_unlocked;

            //set the gem effect
            foreach (Transform T in m_gem_holder.transform)
            {
                Destroy(T.gameObject);
            }

            /*
            m_gem_object = (GameObject)Instantiate(gem_object);

            m_gem_object.transform.SetParent(m_gem_holder.transform);

            m_gem_object.GetComponent<RectTransform>().localPosition = LobbyController.Instance.Lobby_view.Gems_objects_positions[m_arena.World.Index];
            m_gem_object.GetComponent<RectTransform>().localScale = new Vector3(.5f, .5f, .5f);
            */
            m_Gem_BG.sprite = gem;


            m_Gem_BG.gameObject.transform.SetAsLastSibling();

            m_Gem_BG.SetNativeSize();

            m_gem_holder.SetActive(false);

            ResetPanel();
            SetModel(m_arena);

           
            if (m_arena.ArenaType == SpadesArena.SpadesArenaType.Master)  
            {
                //hide gems, move gem to -35f - make the arena text to MasterArena
                foreach (Image tinyGem in m_tinyGems)
                {
                    tinyGem.gameObject.SetActive(false);
                }

                m_Gem_BG.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, -35f, 0);
                m_arena_challenge_text.text = "Master Arena";
                m_arena_challenge_text.fontSize = 58f;
            }
        }

        private void ResetPanel()
        {
            m_holder.localEulerAngles = new Vector3();
            GetComponent<RectTransform>().localEulerAngles = new Vector3();
        }

        public void ShowHideMyEffect(bool show)
        {
            if (show && m_locked == false)
                m_gem_holder.SetActive(true);
            else
                m_gem_holder.SetActive(false);
        }

        //this is for the unlocking animation
        public void SetTolocked(int level)
        {
            m_locked_text_object.SetActive(false);

        }

        public SpadesArena Arena
        {
            get
            {
                return m_arena;
            }
        }

        public Image Panel_BG
        {
            get
            {
                return m_panel_BG;
            }
        }


        public RectTransform Holder
        {
            get
            {
                return m_holder;
            }
        }


        public GameObject Buy_in_object
        {
            get
            {
                return m_buy_in_object;
            }
        }

        public TMP_Text Arena_challenge_text
        {
            get
            {
                return m_arena_challenge_text;
            }
        }

        public TMP_Text Playing_mode_text
        {
            get
            {
                return m_playing_mode_text;
            }
        }

        public GameObject Gem_holder
        {
            get
            {
                return m_gem_holder;
            }
        }

        public Image Gem_BG
        {
            get
            {
                return m_Gem_BG;
            }
        }

        public Image BG_Image
        {
            get
            {
                return m_BG_Image;
            }

            set
            {
                m_BG_Image = value;
            }
        }

        public GameObject BG_Mask
        {
            get
            {
                return m_BG_Mask;
            }

            set
            {
                m_BG_Mask = value;
            }
        }

        public Image[] TinyGems
        {
            get
            {
                return m_tinyGems;
            }

            set
            {
                m_tinyGems = value;
            }
        }

        public WorldItemView World_item_view
        {
            get
            {
                return m_world_item_view;
            }

            set
            {
                m_world_item_view = value;
            }
        }
    }
}