﻿using common.facebook;
using spades.controllers;

namespace spades.views
{

    public class FacebookShareView : MonoBehaviour
    {

        [SerializeField] Toggle m_check_sign = null;
        [SerializeField] GameObject m_toggle_check_icon = null;

        string m_share_id;

        public void InitFBShare(string share_id)
        {
            m_share_id = share_id;

            // Currently not active for iOS
            if (!FacebookController.Instance.IsLoggedIn() || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                gameObject.SetActive(false);
                m_check_sign.isOn = false;
                BT_FB_Share(m_check_sign.isOn);
            }
            else
            {
                gameObject.SetActive(true);

                if (SpadesStateController.Instance.IsFBShare(m_share_id))
                    m_check_sign.isOn = true;
                else
                    m_check_sign.isOn = false;

            }
        }

		public void BT_FB_Share(bool selected)
		{
			m_toggle_check_icon.SetActive(selected);

			SpadesStateController.Instance.SetFBShare(m_share_id, selected);
		}

		public void Show() {
			// Show if connected to facebook and not iOS
			if (FacebookController.Instance.IsLoggedIn() && Application.platform != RuntimePlatform.IPhonePlayer) {
				gameObject.SetActive(true);
			}
		}

		public void Hide() {
			gameObject.SetActive(false);
		}

	}
}