using spades.models;
using spades.controllers;
using spades.views.popups;
using common.utils;
using spades.comm;
using common.controllers;
using cardGames.models;
using common.commands;

namespace spades.views
{
    public class InGameUIView : MonoBehaviour
    {

        SpadesActiveMatch m_active_match;
        ISpadesMatch m_spades_match;
        [SerializeField] GameObject m_game_id_indication = null;

        [SerializeField] TMP_Text m_target_text = null;
        [SerializeField] TMP_Text m_prize_text = null;
        [SerializeField] TMP_Text m_bet_text = null;

        [SerializeField] TMP_Text m_game_mode_text = null;
        [SerializeField] TMP_Text m_bags_title_text = null;
        [SerializeField] TMP_Text m_bags_text = null;
        [SerializeField] TMP_Text m_round_number_text = null;

        [SerializeField] GameObject m_menu_button = null;
        [SerializeField] GameObject m_closeButtonSettingsPanel = null;
        [SerializeField] Sprite m_menu_button_idle = null;
        [SerializeField] Sprite m_menu_button_close = null;

        [SerializeField] Animator m_settingsPanelAnimator = null;

        GameObject[] m_players_table_ui_object = new GameObject[4];

        [SerializeField] MenuView m_menu;

        bool m_quit_initiated_popup = false;


        public void SetActiveMatch(SpadesActiveMatch active_match)
        {
            m_spades_match = active_match.GetMatch() as ISpadesMatch;

            m_active_match = active_match;

            SpadesRoundResults round_results = m_active_match.GetLastRoundResults();

            m_game_id_indication.SetActive(false);

            //in arena we show stage number
            //in single we show Bet and buy in
            if (m_active_match.IsSingleMatch)
            {
                int prize = (m_spades_match as SpadesSingleMatch).Buy_in;
                m_bet_text.text = "bet";
                m_prize_text.text = "<sprite=1>" + FormatUtils.FormatBuyIn(prize);
            }
            else
            {
                m_bet_text.text = "STAGE " + (((SpadesArenaMatch)m_spades_match).Stage_num + 1);
                m_prize_text.text = "";

            }




            m_target_text.text = m_spades_match.LowPoints.ToString() + "/" + m_spades_match.HighPoints.ToString();

            Color light_red = new Color(255f / 255f, 71f / 255f, 90f / 255f);
            Color light_blue = new Color(56f / 255f, 163f / 255f, 252f / 255f);


            if (m_spades_match.Playing_mode == PlayingMode.Partners)
            {
                switch (m_spades_match.Variant)
                {
                    case PlayingVariant.Classic:
                        switch (m_spades_match.SubVariant)
                        {
                            case PlayingSubVariant.FastLane:
                                m_game_mode_text.text = "<sprite=5> Fast Lane";
                                break;
                            case PlayingSubVariant.GoBig:
                                m_game_mode_text.text = "<sprite=5> Go Big";
                                break;
                            case PlayingSubVariant.SingleRound:
                                m_game_mode_text.text = "<sprite=5> 1 Round";
                                break;
                            case PlayingSubVariant.Coins4Points:
                                m_game_mode_text.text = "<sprite=5> Coins4Pts";
                                break;
                            default:
                                m_game_mode_text.text = "<sprite=5> Partners";
                                break;
                        }
                        break;
                    case PlayingVariant.Jokers:
                        m_game_mode_text.text = "<sprite=12> Jokers";
                        break;
                    case PlayingVariant.Mirror:
                        m_game_mode_text.text = "<sprite=14> Mirror";
                        break;
                    case PlayingVariant.Suicide:
                        m_game_mode_text.text = "<sprite=13> Suicide";
                        break;
                    case PlayingVariant.Whiz:
                        m_game_mode_text.text = "<sprite=11> Whiz";
                        break;
                }

            }
            else
            {
                m_game_mode_text.text = "<sprite=6> Solo";

                switch (m_spades_match.SubVariant)
                {
                    case PlayingSubVariant.HiLo:
                        m_game_mode_text.text = "<sprite=6> Hi-Lo";
                        break;
                }
            }
            m_bags_title_text.text = m_spades_match.BagsCount.ToString();

            m_bags_text.text = m_spades_match.BagsPanelty.ToString() + " Pt.";

            m_round_number_text.text = (m_active_match.GetRoundsCount() + 1).ToString();


            m_active_match.OnScoreChange += OnScoreChanged;


        }



        public void SetGameTransId(int gameTransId)
        {
            m_game_id_indication.SetActive(true);
            m_game_id_indication.GetComponent<TMP_Text>().text = "ID: " + gameTransId;

            m_quit_initiated_popup = false;

        }


        private Color GetWorldsColor()
        {
            Color solo_color = new Color(0, 0, 0);

            switch ((m_active_match.GetMatch() as Match).World_id)
            {
                case ("1"):
                    solo_color = new Color(64f / 255f, 243f / 255f, 182f / 255f);
                    break;
                case ("2"):
                    solo_color = new Color(255f / 255f, 120f / 255f, 79f / 255f);
                    break;
                case ("3"):
                    solo_color = new Color(106f / 255f, 194f / 255f, 255f / 255f);
                    break;
                case ("4"):
                    solo_color = new Color(171f / 255f, 152f / 255f, 255f / 255f);
                    break;
                case ("5"):
                    solo_color = new Color(230f / 255f, 181f / 255f, 65f / 255f);
                    break;
            }

            return solo_color;
        }


        void OnScoreChanged(SpadesRoundResults round_results)
        {

            m_round_number_text.text = (m_active_match.GetRoundsCount() + 1).ToString();
            if (m_spades_match.Playing_mode == PlayingMode.Partners)
            {

                int rival_bags = 0;
                int your_bags = 0;

                if (round_results != null)
                {
                    your_bags = round_results.GetResults(SpadesRoundResults.Positions.North_South).Bags_total;
                    rival_bags = round_results.GetResults(SpadesRoundResults.Positions.West_East).Bags_total;
                }
            }
        }

        public void LogButtonClicked()
        {
            Transform GameLogMenuItem = transform.FindDeepChild("GameLogMenuItem");
            bool a = true;
            if (GameLogMenuItem != null)
                a = GameLogMenuItem.GetComponent<Button>().interactable;
            if (!a)
                return;
            m_menu.CloseMenuPanel();

            GameObject popup = null;

            if (m_spades_match.Playing_mode == PlayingMode.Partners)
            {
                popup = (GameObject)Instantiate((PopupManager.Instance as SpadesPopupManager).Round_summary_partners);
                popup.GetComponent<RoundSummaryPartnerPopup>().SetMatchEnded(false, true, true);
            }
            else
            {
                popup = (GameObject)Instantiate((PopupManager.Instance as SpadesPopupManager).Round_summary_solo);
                popup.GetComponent<RoundSummarySoloPopup>().SetMatchEnded(false, true, true);

            }


            PopupManager.Instance.AddPopup(popup, PopupManager.AddMode.ShowAndRemove);
        }

        public void ExitButtonClicked()
        {
            m_menu.CloseMenuPanel();
            ShowQuitQuestion();
        }

        private void ShowQuitQuestion()
        {
            if (!m_quit_initiated_popup)
            {
                m_quit_initiated_popup = true;

                string question_text;

                SpadesActiveMatch activeMatch = (SpadesActiveMatch)ModelManager.Instance.GetActiveMatch();
                if (activeMatch.IsSingleMatch)
                {
                    if (activeMatch.UsesVoucher)
                    {
                        question_text = "Are you sure you want to quit?\n\nYou'll lose the game!";
                    }
                    else
                    {
                        if (ModelManager.Instance.GetUser().NeverPlayed)
                        {
                            question_text = "Are you sure you want to quit?\n\nYou'll lose the game!";
                        }
                        else
                        {
                            int buyin = (activeMatch.GetMatch() as SpadesSingleMatch).Buy_in;
                            question_text = "Are you sure you want to quit?\n\nYou'll lose <sprite=1>" + FormatUtils.FormatBuyIn(buyin) + "!";
                        }
                    }
                }
                else
                {
                    question_text = "Are you sure you want to quit?\n\nYou'll be eliminated from the arena!";
                }


                if (m_spades_match.Coins4PointsFactor == 0)
                {
                    SpadesPopupManager.Instance.ShowQuestionPopup(PopupManager.AddMode.ShowAndRemove, null, question_text, "Stay", "Quit", () =>
                     {
                         m_quit_initiated_popup = false;
                     }, () =>
                     {
                         SpadesPopupManager.Instance.ShowWaitingIndication(true);
                         SpadesGameServerManager.Instance.Send(CommandsBuilder.Instance.BuildQuitCommand(ModelManager.Instance.GetTable().Game_id));
                     });
                }
                else
                {

                    SpadesPopupManager.Instance.ShowC4PQuitPopup(PopupManager.AddMode.ShowAndRemove, "Stay", "Quit", () =>
                    {
                        m_quit_initiated_popup = false;
                    }, () =>
                    {
                        SpadesPopupManager.Instance.ShowWaitingIndication(true);
                        SpadesGameServerManager.Instance.Send(CommandsBuilder.Instance.BuildQuitCommand(ModelManager.Instance.GetTable().Game_id));
                    });
                }
            }
        }

        public void TL_MenuOpened()
        {
            m_menu_button.transform.GetChild(0).gameObject.SetActive(true);
            m_menu_button.GetComponent<Image>().sprite = m_menu_button_close;
        }

        public void TL_MenuClosed()
        {
            m_menu_button.transform.GetChild(0).gameObject.SetActive(false);
            m_menu_button.GetComponent<Image>().sprite = m_menu_button_idle;
        }

        private void OnEnable()
        {
            if (AndroidBackButtonListener.Instance != null)
                AndroidBackButtonListener.Instance.BackButtonClicked += OnBackButtonClicked;

        }

        private void OnDisable()
        {
            if (AndroidBackButtonListener.Instance != null)
                AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;

            m_quit_initiated_popup = false;
        }

        private void OnBackButtonClicked()
        {
            ShowQuitQuestion();
        }

        public void CheatButtonClick()
        {
#if DEV_MODE || QA_MODE && !NO_CHEAT
            SpadesPopupManager.Instance.ShowCheatPopup();
#endif
        }

        public void DisconnectButtonClick()
        {
        }



    }
}
