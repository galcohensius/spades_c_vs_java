﻿using System.Collections.Generic;

namespace spades.views

{

	public class WheelView : MonoBehaviour {

		[SerializeField]List<Image>	m_icons=new List<Image>();


		public void SetWheel(Sprite sprite1,Sprite sprite2,Sprite sprite3)
		{
			m_icons [0].sprite = sprite1;
			m_icons [0].SetNativeSize ();
			m_icons [0].GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);

			m_icons [1].sprite = sprite2;
			m_icons [1].SetNativeSize ();
			m_icons [1].GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);

			m_icons [2].sprite = sprite3;
			m_icons [2].SetNativeSize ();
			m_icons [2].GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);
		}

	}
}