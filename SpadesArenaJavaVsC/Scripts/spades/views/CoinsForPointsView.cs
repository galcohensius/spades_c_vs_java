﻿using cardGames.controllers;
using common.utils;
using cardGames.views.overlays;
using spades.controllers;
using System.Collections;
using common.controllers;

namespace spades.views
{
    public class CoinsForPointsView : MonoBehaviour
    {
        float m_coin_icon_width = 30f;

        [SerializeField] TMP_Text m_title;
        [SerializeField] TMP_Text m_pt;
        [SerializeField] TMP_Text m_pt_title;
        [SerializeField] TMP_Text m_total;
        [SerializeField] Image m_bg_image_win;
        [SerializeField] Image m_bg_image_lose;
        [SerializeField] GameObject m_prize_coins_object_aligner = null;
        [SerializeField] Material m_material_med = null;
        int m_c4pCapping;
        //win_bg is there by defualt
        [SerializeField] Sprite m_lose_bg;

        private void Start()
        {

        }

        private void Awake()
        {
            // C4PCapping
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_c4pCapping = ldc.GetSettingAsInt("C4PCapping", 200);
            };
        }

        public void SetData(bool wasCancel, int gap, int pt_worth, int total, bool win)
        {
            gameObject.SetActive(true);

            gap = gap >= 0 ? Mathf.Min(m_c4pCapping, gap) : Mathf.Max(-m_c4pCapping, gap); 

            if (wasCancel)
                m_title.text = "QUIT PENALTY";
            else
                m_title.text = "POINT GAP " + gap + " PT";

            //float pos_x = m_prize_coins_text.gameObject.GetComponent<RectTransform> ().localPosition.x - m_prize_coins_text.preferredWidth / 2 + m_coin_icon_width;
            if (win)
            {
                m_total.text = "<sprite=1>" + FormatUtils.FormatPrice(total);
                m_pt_title.text = "1pt = ";
                m_pt.text = "<sprite=1> +" + pt_worth;

                m_bg_image_win.gameObject.SetActive(true);
                m_bg_image_lose.gameObject.SetActive(false);

                float pos_x = -m_total.preferredWidth / 2 + m_coin_icon_width / 2;

                m_prize_coins_object_aligner.GetComponent<RectTransform>().localPosition = new Vector3(pos_x, 0, 0);

                float scale_factor = 3f;
                float scale_timing = 1.2f;
                float scale_delay = 0.3f;

                StartCoroutine(DelayFlyComet(gap, total, scale_factor, scale_timing, scale_delay));
            }
            else
            {
                m_total.text = "<sprite=1>" + FormatUtils.FormatPrice(total);

                m_pt_title.text = "1pt = ";
                m_pt.text = "<sprite=1> -" + pt_worth;
                m_bg_image_win.gameObject.SetActive(false);
                m_bg_image_lose.gameObject.SetActive(true);
                Color color;
                if (ColorUtility.TryParseHtmlString("#b9b8b8", out color))
                {
                    m_title.color = color;
                    m_pt_title.color = color;
                }
                m_pt.fontSharedMaterial = m_material_med;
                m_total.fontSharedMaterial = m_material_med;
                m_title.fontSharedMaterial = m_material_med;
            }
        }

        IEnumerator DelayFlyComet(int gap, int total, float scale_factor, float scale_timing, float scale_delay)
        {
            yield return new WaitForSecondsRealtime(1f);
            CometOverlay coins_comet = SpadesOverlayManager.Instance.ShowCometOverlay();
            int num_coins_to_show = Mathf.Min(5, gap);
            coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_prize_coins_object_aligner, CardGamesPopupManager.Instance.Lobby_flight_target, 0.05f, 0.15f, 1.25f,
            CardGamesPopupManager.Instance.Lobby_coins_balance, total, CardGamesPopupManager.Instance.Lobby_coins_animator, scale_factor, scale_timing, scale_delay, CoinsFlightDone, new Vector2(0.4f, 0f));
        }
        private void CoinsFlightDone()
        {

        }


    }
}
