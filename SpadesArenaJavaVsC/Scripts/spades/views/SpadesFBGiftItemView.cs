﻿using cardGames.views;

namespace spades.views
{
    public class SpadesFBGiftItemView : FBGiftItemView
    {
        protected override void HandleButtonClick()
        {
            ManagerView.Instance.EnableDisableButton(m_button);
        }
    }
}