using System.Collections.Generic;
using common.models;
using cardGames.models;

namespace spades.views
{
    public class LeaderboarRightPanelTitle : MonoBehaviour
    {
        [SerializeField] TMP_Text m_flayerTitle = null;
        [SerializeField] TMP_Text m_flayerLevels = null;
        [SerializeField] TMP_Text m_flayerBatch = null;
        [SerializeField] Material m_material_gold = null;
        [SerializeField] Material m_material_silver = null;
        [SerializeField] Material m_material_bronze = null;
        [SerializeField] Material m_material_platinum = null;

        [SerializeField] List<Sprite> m_titleImageMap = null;
        [SerializeField] Image m_titleImage = null;

        private Dictionary<string, int> m_groupToImageMap = new Dictionary<string, int>() {
        { "A1",0 },
        { "A2",1 },
        { "A3",2 },
        { "B1",3 },
        { "B2",4 },
        { "B3",5 },
        { "C1",6 },
        { "C2",7 },
        { "C3",8 },
        { "D1",9 },
        { "D2",10 },
        { "D3",11 }
    };


        public void SetTitle(LeaderboardsModel.LeaderboardType lt)
        {
            //Flayer only settings
            LeaderboardModel lb = ModelManager.Instance.Leaderboards.GetLeaderboard(lt);
            string group = lb.LeaderboardMetaData.GroupId;

            string color = ModelManager.Instance.Leaderboards.GetColorFromGroup(group);
            m_flayerTitle.text = ModelManager.Instance.Leaderboards.GetTitleFromGroup(group);
            m_flayerLevels.text = "LEVELS " + lb.LeaderboardMetaData.MinLvl + "-" + lb.LeaderboardMetaData.MaxLvl;

            if (group != null && m_groupToImageMap.ContainsKey(group))
            {
                m_titleImage.sprite = m_titleImageMap[m_groupToImageMap[group]];
            }
            /*
            switch (color)
            {
                case "GOLD":
                    m_flayerTitle.fontMaterial = m_material_gold;
                    break;
                case "SILVER":
                    m_flayerTitle.fontMaterial = m_material_silver;
                    break;
                case "BRONZE":
                    m_flayerTitle.fontMaterial = m_material_bronze;
                    break;
                case "PLATINUM":
                    m_flayerTitle.fontMaterial = m_material_platinum;
                    break;
            }*/
            string leaderboardId = lb.LeaderboardMetaData.Id.ToString();
            m_flayerBatch.text = "#" + leaderboardId.Substring(leaderboardId.Length - 3);

        }
    }
}
