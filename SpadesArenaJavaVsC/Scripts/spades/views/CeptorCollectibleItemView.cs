﻿namespace spades.views
{
		
	public class CeptorCollectibleItemView : MonoBehaviour
	{
		const int STEP_SPACE = 3;
		const int STEP_WIDTH = 61;
		const int EXTRA_MARGINS = 20;

		[SerializeField]TMP_Text 	m_title_text = null;
		[SerializeField]TMP_Text	m_ceptors_counter_text = null;
		[SerializeField]Image 		m_ceptor_image = null;

		[SerializeField]GameObject 	Step_prefab = null;

		[SerializeField]GameObject	m_step_progress_bar=null;


		public void SetData (string title, int ceptors_won_counter, int curr_step, int total_steps, Sprite ceptor_image)
		{
			m_title_text.text = title;
			m_ceptors_counter_text.text = ceptors_won_counter.ToString();
			m_ceptor_image.sprite = ceptor_image;
			//m_ceptor_image.SetNativeSize ();

			if (ceptors_won_counter==0)
				m_ceptor_image.color = new Color (0.1f, 0.1f, 0.1f);

			CreateStepsProgressBar (total_steps, curr_step);
		}


		public void CreateStepsProgressBar (int total_step,int curr_step)
		{

			foreach (Transform child in m_step_progress_bar.transform)
			{
				Destroy (child.gameObject);
			}

			float progress_bar_width = STEP_WIDTH * total_step + STEP_SPACE * (total_step - 1) + EXTRA_MARGINS;
			m_step_progress_bar.GetComponent<RectTransform> ().sizeDelta = new Vector2 (progress_bar_width, 45f);

			for (int i = 0; i < total_step; i++)
			{
				GameObject step = (GameObject)Instantiate (Step_prefab);
				step.transform.SetParent (m_step_progress_bar.transform);
				step.GetComponent<RectTransform> ().localPosition = new Vector3 (EXTRA_MARGINS / 2 + i * STEP_WIDTH + i * STEP_SPACE - progress_bar_width / 2 + STEP_WIDTH / 2, 0, 0);
				step.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1f);

				if(i>curr_step-1)
					step.SetActive (false);
				
			}
		}


	}
}