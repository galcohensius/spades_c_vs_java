using System;
using System.Collections;
using cardGames.controllers;
using cardGames.models;
using common.controllers;
using common.utils;
using common.views;
using cardGames.views.overlays;
using spades.controllers;
using spades.models;
using common.ims;
using cardGames.piggy.controllers;

namespace spades.views
{

    public class WinLoseView : MonoBehaviour
    {

        private const float IMS_BANNER_IN_POS = 740f;
        private const float IMS_BANNER_OUT_POS = 1600f;

        private const float END_GAME_POPUPS_DELAY = 1f;


        [SerializeField] GameObject m_end_game_challenge_object = null;
        SpadesChallengeEndGameView m_end_game_challenge_view = null;

        [SerializeField] Sprite m_arena_title_bg = null;
        [SerializeField] Image m_title = null;

        [SerializeField] TMP_Text m_title_text;
        [SerializeField] TMP_Text m_prize_coins_text = null;
        [SerializeField] TMP_Text m_prize_xp_text = null;

        [SerializeField] TMP_Text m_sentence_text = null;

        [SerializeField] ProgressView m_progress_bar_view = null;

        [SerializeField] GameObject m_prize_coins_object_aligner = null;
        [SerializeField] GameObject m_prize_xp_object_aligner = null;

        [SerializeField] GameObject m_prize_coins_group = null;
        [SerializeField] GameObject m_prize_xp_group = null;


        [SerializeField] GameObject m_left_button = null;
        [SerializeField] GameObject m_right_button = null;

        [SerializeField] GameObject m_close_button = null;

        [SerializeField] RectTransform m_bg = null;

        [SerializeField] CoinsForPointsView m_coinsForPointsView;
        [SerializeField] HighLowView m_highLowView;

        [SerializeField] GameObject m_imsBannerHolder;
        [SerializeField] GameObject m_imsBannerCloseButton;

        int m_prize_coins;
        int m_prize_xp;
        float m_coin_icon_width = 38f;
        float m_xp_icon_width = 66f;

        bool m_ff_animation = false;

        Action m_left_button_clicked;
        Action m_right_button_clicked;

        Action m_close_button_clicked;

        bool m_end_event_done = false;
        bool m_levelup_shown;
        bool m_is_arena;

        bool m_isCoinsForPoints = false;

        bool m_should_show_fb_share = true;

        Coroutine m_left_btn_countdown_coroutine;
        Coroutine m_endGameIMSEventCoroutine;

        private void Awake()
        {
            m_end_game_challenge_view = m_end_game_challenge_object.GetComponent<SpadesChallengeEndGameView>();
        }

        public void SetScreenData(bool arena, bool wasCancel, int gap, int prize_coins, int prize_coins_for_points, int prize_xp, Action leftBtnClick, string left_button_text, Action rightBtnClick,
                                   string right_button_text, string title_text, Action closeBtnClick)
        {
            //used to close everything when mini game is triggered
            SpadesPopupManager.Instance.Mini_game_initiated += OnMiniGameInitiated;

            m_end_game_challenge_object.SetActive(MissionController.Instance.ShouldShowMissionButtonInGame());
            //if (arena)
            //    m_title.sprite = m_arena_title_bg;

            Transform close_button = transform.FindDeepChild("CloseButton");

            if (close_button != null)
                close_button.gameObject.GetComponent<Button>().onClick.AddListener(BT_CloseClick);



            LobbyTopController.Instance.ShowTop(true, false, false, true);

            m_left_button.transform.GetChild(0).GetComponent<TMP_Text>().text = left_button_text;
            m_right_button.transform.GetChild(0).GetComponent<TMP_Text>().text = right_button_text;

            m_left_button.SetActive(false);
            m_right_button.SetActive(false);


            m_progress_bar_view = LobbyTopController.Instance.Progress_view;

            m_left_button_clicked = leftBtnClick;
            m_right_button_clicked = rightBtnClick;
            m_close_button_clicked = closeBtnClick;

            m_close_button.SetActive(false);

            m_title_text.text = title_text;


            m_prize_coins = prize_coins;
            m_prize_xp = prize_xp;

            //LoggerController.Instance.Log("Width:"+m_prize_xp_text.preferredWidth);



            int old_xp = LobbyController.Instance.GetOldXP();
            int thresh = LobbyController.Instance.Old_threshold;
            int old_level = LobbyController.Instance.GetOldLevel();
            m_progress_bar_view.ShowInitialData(old_xp, thresh, old_level);

            ISpadesMatch spadesMatch = SpadesModelManager.Instance.GetActiveMatch().GetMatch();

            if (spadesMatch.Coins4PointsFactor > 0)
            {
                m_isCoinsForPoints = true;

                int pt_worth = spadesMatch.Coins4PointsFactor;
                if (spadesMatch.SubVariant == PlayingSubVariant.Coins4Points && spadesMatch.Coins4PointsFactor > 0)
                {
                    m_coinsForPointsView.SetData(wasCancel, gap, pt_worth, prize_coins_for_points, prize_coins_for_points > 0);
                    m_prize_coins = m_prize_coins - prize_coins_for_points;
                }
                else if (spadesMatch.SubVariant == PlayingSubVariant.HiLo && spadesMatch.Coins4PointsFactor > 0)
                {
                    m_highLowView.SetData(wasCancel, gap, pt_worth, prize_coins_for_points, prize_coins_for_points >= 0);
                    m_prize_coins = m_prize_coins - prize_coins_for_points;
                }
            }


            m_prize_coins_text.text = "<sprite=1>" + FormatUtils.FormatPrice(m_prize_coins);
            m_prize_xp_text.text = "<sprite=7>" + FormatUtils.FormatPrice(m_prize_xp);

            if (m_prize_xp > 0)
            {
                m_prize_xp_text.text = "<sprite=7>" + FormatUtils.FormatPrice(m_prize_xp);

            }
            else
            {

                m_prize_xp_group.SetActive(false);

            }


            float no_coins_y_offset = 0;

            if (m_prize_coins > 0)
            {

                m_prize_coins_text.text = "<sprite=1>" + FormatUtils.FormatPrice(m_prize_coins);

                //float pos_x = m_prize_coins_text.gameObject.GetComponent<RectTransform> ().localPosition.x - m_prize_coins_text.preferredWidth / 2 + m_coin_icon_width;

                float pos_x = -m_prize_coins_text.preferredWidth / 2 + m_coin_icon_width / 2;

                m_prize_coins_object_aligner.GetComponent<RectTransform>().localPosition = new Vector3(pos_x, 0, 0);

                m_bg.sizeDelta = new Vector2(1050f, 590f);

                no_coins_y_offset = -100f;
            }
            else
            {
                m_prize_coins_group.SetActive(false);
                m_prize_xp_text.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

                m_bg.sizeDelta = new Vector2(1050f, 540f);

                //need to hide the entire prize text group and move on the Y the entire XP group


            }

            m_prize_xp_text.text = "<sprite=7>" + FormatUtils.FormatPrice(m_prize_xp);

            float pos_x2 = -m_prize_xp_text.preferredWidth / 2 + m_xp_icon_width / 2;

            float y_pos = m_prize_xp_object_aligner.GetComponent<RectTransform>().localPosition.y + no_coins_y_offset;

            m_prize_xp_object_aligner.GetComponent<RectTransform>().localPosition = new Vector3(pos_x2, 0, 0);

            if (m_prize_coins > 0)
                m_prize_xp_group.GetComponent<RectTransform>().localPosition = new Vector3(120, 7f + no_coins_y_offset, 0);
            else
                m_prize_xp_group.GetComponent<RectTransform>().localPosition = new Vector3(120, -12f + no_coins_y_offset, 0);
        }





        public void Button_Side_Panel_Clicked()
        {
            if (m_close_button.activeSelf && !m_levelup_shown && !PopupManager.Instance.IsPopupShown)
            {

                m_should_show_fb_share = false;

                BT_CloseClick();

                LeaderboardsController.Instance.ShowLeaderboard(common.models.LeaderboardsModel.LeaderboardType.GlobalCurrent);

            }


        }

        private void OnEnable()
        {

            if (AndroidBackButtonListener.Instance != null)
                AndroidBackButtonListener.Instance.BackButtonClicked += OnBackButtonClicked;

        }

        private void OnDisable()
        {
            if (AndroidBackButtonListener.Instance != null)
                AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;

            if (m_left_btn_countdown_coroutine != null)
                StopCoroutine(m_left_btn_countdown_coroutine);

        }

        private void OnBackButtonClicked()
        {
            // If the close button is shown, click it
            if (m_close_button.activeSelf && !m_levelup_shown && !PopupManager.Instance.IsPopupShown)
                BT_CloseClick();
        }


        /// <summary>
        /// CALLED FROM TIMELINE
        /// </summary>
        public void StartProgressAnimation()
        {
            //m_progress_bar_view.StartProgressAnimation (m_ff_animation);
        }

        public void FF_Event()
        {
            m_ff_animation = true;
            m_progress_bar_view.StopCoroutineAndShowEnd();
        }

        public void UpdateCoins(BalanceCoinTextbox coins_text, Action post_flight_action = null, bool ff_animation = false)
        {

            float scale_factor = 3f;
            float scale_timing = 1.2f;
            float scale_delay = 0.3f;

            m_ff_animation = ff_animation;

            float delay = 0;

            if (m_prize_coins > 0)
            {
                delay = 1f;

                if (m_prize_xp > 0)
                {
                    delay += 0.5f;
                }
            }

            if (m_prize_coins > 0)
            {
                CometOverlay coins_comet = SpadesOverlayManager.Instance.ShowCometOverlay();
                int num_coins_to_show = Mathf.Min(5, m_prize_coins);

                coins_comet.FlyComets(CardGamesPopupManager.Instance.Coins_trails_prefab, num_coins_to_show, m_prize_coins_object_aligner, CardGamesPopupManager.Instance.Lobby_flight_target, 0.05f, 0.15f, 1.25f,
                CardGamesPopupManager.Instance.Lobby_coins_balance, m_prize_coins, CardGamesPopupManager.Instance.Lobby_coins_animator, scale_factor, scale_timing, scale_delay, post_flight_action, new Vector2(0.4f, 0f));
            }
            else
            {
                // Update the balance view - UGLY
                CardGamesPopupManager.Instance.Lobby_coins_balance.Amount = ModelManager.Instance.GetUser().GetCoins();
            }

            if (m_prize_xp > 0)
            {

                CometOverlay xp_comet = SpadesOverlayManager.Instance.ShowCometOverlay();
                int num_coins_to_show = Mathf.Min(5, m_prize_xp);

                xp_comet.FlyComets(ManagerView.Instance.Xp_trails_prefab, num_coins_to_show, m_prize_xp_object_aligner, LobbyTopController.Instance.Xp_icon_object, 0.05f, 0.15f, 1.25f,
                    null, 0, null, scale_factor, scale_timing, scale_delay, post_flight_action, new Vector2(-0.5f, 0f));


                StartCoroutine(ShowProgressView(1.3f));
            }

            if (m_ff_animation)
                delay = 0;

            PiggyController.Instance.OnEndGame?.Invoke();

        }



        private void TriggerEndGameMESEvent()
        {

            if (!m_end_event_done)
            {
                m_end_event_done = true;

                // Register to get the MES action if executed
                SpadesMESController.Instance.OnActionExecuted += OnMesActionExecuted;


                 m_endGameIMSEventCoroutine = ManagerView.Instance.DelayAction(END_GAME_POPUPS_DELAY, () =>
                 {
                     // Trigger end game event - IMS
                     SpadesIMSController.Instance.TriggerEvent(SpadesIMSController.EVENT_END_GAME,true);

                     // Trigger end game event
                     (SpadesMESController.Instance as SpadesMESController).Execute(SpadesMESController.TRIGGER_END_GAME);
                 });


            }
        }

        private void OnMesActionExecuted(int actionId)
        {
            // If the action is openning a new screen, we have to get back to the lobby
            if (actionId == SpadesMESController.ACTION_OPEN_ARENA || actionId == SpadesMESController.ACTION_OPEN_LEADERBOARD)
            {
                BT_CloseClick();

            }
        }

        private void ShowChallengeAndContestsPanels()
        {

            // call contest win animations here...
            ContestPanelController.Instance.TriggerEndGameAnimation();

            if (m_end_game_challenge_view != null)
                MissionController.Instance.ShowChallengeEndGamePanel(m_end_game_challenge_view);
        }

        private void ProgressBarFillDone(bool shouldLevelUp)
        {
            //shouldLevelUp = true;

            if (shouldLevelUp)
            {
                m_levelup_shown = true;
                // Remove the back button functionality for now
                SpadesOverlayManager.Instance.ShowLevelUpOverlay(ModelManager.Instance.GetUser().GetLevel(), () =>
                 {

                     m_levelup_shown = false;

                     // MES end game event only for Single games for now
                     if (!m_is_arena)
                         TriggerEndGameMESEvent();


                     ShowChallengeAndContestsPanels();

                 });
            }
            else
            {
                // MES end game event only for Single games for now
                if (!m_is_arena)
                    TriggerEndGameMESEvent();

                ShowChallengeAndContestsPanels();
            }

            if (m_right_button_clicked == null)
            {
                // Show a single button in the middle
                m_left_button.transform.localPosition = new Vector2(0, m_left_button.transform.localPosition.y);
                m_right_button.SetActive(false);

                if (m_left_button_clicked != null)
                    m_left_button.SetActive(true);
            }
            else
            {
                if (m_left_button != null)
                    m_left_button.SetActive(true);

                if (m_right_button != null)
                    m_right_button.SetActive(true);
            }



            if (IMSController.Instance.BannersByLocations.ContainsKey("E2"))
            {
                //bringing in the IMS banner
                //need to check if this is a c4P game - and if so - add a close button
                m_imsBannerCloseButton.SetActive(m_isCoinsForPoints);
            
                iTween.MoveTo(m_imsBannerHolder, iTween.Hash("x", IMS_BANNER_IN_POS, "islocal", true, "easeType", "easeInQuad", "time", .2));
            }
            

            m_close_button.SetActive(true);

        }

        public void IMSBannerCloseClicked()
        {
            iTween.MoveTo(m_imsBannerHolder, iTween.Hash("x", IMS_BANNER_OUT_POS, "islocal", true, "easeType", "easeInQuad", "time", .2));
        }

        private IEnumerator ShowProgressView(float delay)
        {
            yield return new WaitForSeconds(delay);

            m_progress_bar_view.StartProgressAnimation(m_ff_animation, ProgressBarFillDone);

            //Need to check if we need to show the OnBoarding overlay
            /*
			if (LobbyController.Instance.ShouldShowOnboardingAfterGame)
			{
                LobbyController.Instance.ShouldShowOnboardingAfterGame = false;
				SpadesOverlayManager.Instance.ShowOnboardingOverlay (null,()=>
					{
						RoundController.Instance.Table_view.ShowGameUI(false);
						LobbyController.Instance.ShowLobby (true);
					});
			}
			*/

        }

        private void OnMiniGameInitiated()
        {
            m_should_show_fb_share = false;
            BT_CloseClick();
        }


        public void BT_CloseClick()
        {
            if (m_endGameIMSEventCoroutine != null)
                StopCoroutine(m_endGameIMSEventCoroutine);

            m_progress_bar_view.StopCoroutineAndShowEnd();

            if (m_left_btn_countdown_coroutine != null)
                StopCoroutine(m_left_btn_countdown_coroutine);

            if (m_close_button_clicked != null)
                m_close_button_clicked();

            ContestPanelController.Instance.Contest_panel_view.SetOpenPanel(false);
        }

        public void BT_LeftButtonClick()
        {
            m_progress_bar_view.StopCoroutineAndShowEnd();

            if (m_endGameIMSEventCoroutine!=null)
                StopCoroutine(m_endGameIMSEventCoroutine);

            if (m_left_btn_countdown_coroutine != null)
                StopCoroutine(m_left_btn_countdown_coroutine);

            if (m_left_button_clicked != null)
                m_left_button_clicked();

            ContestPanelController.Instance.Contest_panel_view.SetOpenPanel(false);
        }


        public void BT_RightButtonClick()
        {
            if (m_endGameIMSEventCoroutine != null)
                StopCoroutine(m_endGameIMSEventCoroutine);

            m_progress_bar_view.StopCoroutineAndShowEnd();

            if (m_left_btn_countdown_coroutine != null)
                StopCoroutine(m_left_btn_countdown_coroutine);

            if (m_right_button_clicked != null)
                m_right_button_clicked();

            ContestPanelController.Instance.Contest_panel_view.SetOpenPanel(false);

        }

        public void SetRightBtnInteractable(bool interactable)
        {
            if (this != null)
            {
                m_right_button.GetComponent<Button>().interactable = interactable;
                ViewUtils.EnableDisableButtonText(m_right_button.GetComponent<Button>());
            }
        }

        public void SetLeftBtnInteractable(bool interactable)
        {
            if (this != null)
            {
                m_left_button.GetComponent<Button>().interactable = interactable;
                ViewUtils.EnableDisableButtonText(m_left_button.GetComponent<Button>());
            }
        }

        public void ChangeRightButtonLabel(string text)
        {
            if (this != null && m_right_button != null && m_right_button.transform.GetChild(0) != null && m_right_button.transform.GetChild(0).GetComponent<TMP_Text>() != null)
                m_right_button.transform.GetChild(0).GetComponent<TMP_Text>().text = text;
        }

        private void OnDestroy()
        {
            // Unregister from events
            SpadesMESController.Instance.OnActionExecuted -= OnMesActionExecuted;
            SpadesPopupManager.Instance.Mini_game_initiated -= OnMiniGameInitiated;
        }

        public bool Is_arena
        {
            get
            {
                return m_is_arena;
            }

            set
            {
                m_is_arena = value;
            }
        }

        public bool Should_show_fb_share
        {
            get
            {
                return m_should_show_fb_share;
            }

        }

        public SpadesChallengeEndGameView End_game_challenge_view
        {
            get
            {
                return m_end_game_challenge_view;
            }
        }
    }
}
