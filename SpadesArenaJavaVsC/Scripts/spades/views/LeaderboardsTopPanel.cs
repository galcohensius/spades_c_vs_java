using System.Collections;
using common.models;
using common;
using System;
using common.utils;
using spades.controllers;
using cardGames.models;

namespace spades.views
{

	public class LeaderboardsTopPanel : MonoBehaviour
	{

		[SerializeField] Image m_friends = null;
		[SerializeField] Image m_global = null;
		[SerializeField] Image m_hof = null;

		[SerializeField] GameObject m_previous_selected = null;
		[SerializeField] GameObject m_previous_button = null;

		[SerializeField] GameObject m_current_selected = null;
		[SerializeField] GameObject m_current_button = null;
		[SerializeField] TMP_Text m_items_title = null;

        [SerializeField] GameObject m_timerObj = null;
        [SerializeField] TMP_Text m_timer_text = null;

		private LeaderboardsModel.LeaderboardType currentTab = LeaderboardsModel.LeaderboardType.GlobalCurrent;
        [Header("basic shared general")]
        public Color TITLE_ON_COLOR = new Color(181f / 255f, 253f / 255f, 247f / 255f, 1f);
        public Color TITLE_OFF_COLOR = new Color(75f / 255f, 113f / 255f, 136f / 255f, 1f);

        public void SwitchTab(LeaderboardsModel.LeaderboardType tab)
		{
            Button btn;
			if (currentTab != tab)
			{
				TurnTabOnOff(currentTab, SpadesPopupManager.Instance.Tab_Off_sprite, TITLE_OFF_COLOR, SpadesPopupManager.Instance.Tab_Off_Material);
                if (currentTab == LeaderboardsModel.LeaderboardType.Friends)
                {
                    btn = m_friends.GetComponentInChildren<Button>();
                    btn.enabled = true;
                }

                if (currentTab == LeaderboardsModel.LeaderboardType.GlobalCurrent || currentTab == LeaderboardsModel.LeaderboardType.GlobalPrevious)
                {
                    btn = m_global.GetComponentInChildren<Button>();
                    btn.enabled = true;
                }

                if (currentTab == LeaderboardsModel.LeaderboardType.HallOfFame)
                {
                    btn = m_hof.GetComponentInChildren<Button>();
                    btn.enabled = true;
                }
            }

			currentTab = tab;
			TurnTabOnOff(currentTab, SpadesPopupManager.Instance.Tab_On_sprite, TITLE_ON_COLOR, SpadesPopupManager.Instance.Tab_On_Material);


            //disable click on selected tab
            if(currentTab == LeaderboardsModel.LeaderboardType.Friends)
            {
                btn = m_friends.GetComponentInChildren<Button>();
                btn.enabled = false;
            }

            if (currentTab == LeaderboardsModel.LeaderboardType.GlobalCurrent || currentTab == LeaderboardsModel.LeaderboardType.GlobalPrevious)
            {
                btn = m_global.GetComponentInChildren<Button>();
                btn.enabled = false;
            }

            if (currentTab == LeaderboardsModel.LeaderboardType.HallOfFame)
            {
                btn = m_hof.GetComponentInChildren<Button>();
                btn.enabled = false;
            }

        }


        public void LockAllTabs()
        {
            m_friends.GetComponentInChildren<Button>().enabled = false;
            m_global.GetComponentInChildren<Button>().enabled = false;
            m_hof.GetComponentInChildren<Button>().enabled = false;
            m_previous_button.GetComponent<Button>().enabled = false;
            m_current_button.GetComponent<Button>().enabled = false;
        }

        private void TurnTabOnOff(LeaderboardsModel.LeaderboardType tab, Sprite sprite, Color text_color, Material font_material)
		{
			switch (tab)
			{
				case LeaderboardsModel.LeaderboardType.Friends:
					m_friends.sprite = sprite;
                    m_friends.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().color = text_color;
                    m_friends.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().fontMaterial = font_material;
                    m_friends.SetNativeSize();
					break;

				case LeaderboardsModel.LeaderboardType.GlobalCurrent:
					m_global.sprite = sprite;
                    m_global.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().color = text_color;
                    m_global.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().fontMaterial = font_material;
                    m_global.SetNativeSize();
					break;
				case LeaderboardsModel.LeaderboardType.GlobalPrevious:
					m_global.sprite = sprite;
                    m_global.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().color = text_color;
                    m_global.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().fontMaterial = font_material;
                    m_global.SetNativeSize();
					break;
				case LeaderboardsModel.LeaderboardType.HallOfFame:
					m_hof.sprite = sprite;
                    m_hof.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().color = text_color;
                    m_hof.transform.GetChild(0).GetChild(0).GetComponent<TMP_Text>().fontMaterial = font_material;
                    m_hof.SetNativeSize();
					break;

			}
		}

		public void switchToPrevious()
		{
			m_previous_selected.SetActive(true);
			m_previous_button.SetActive(false);

			m_current_selected.SetActive(false);
			m_current_button.SetActive(true);
		}

		public void switchToCurrent()
		{
			m_previous_selected.SetActive(false);
			m_previous_button.SetActive(true);

			m_current_selected.SetActive(true);
			m_current_button.SetActive(false);
		}

		public void hideButtons()
		{
			m_previous_selected.SetActive(false);
			m_previous_button.SetActive(false);

			m_current_selected.SetActive(false);
			m_current_button.SetActive(false);
		}

		public void DisablePrevious()
		{
			m_previous_button.GetComponent<Button>().interactable = false;
            ManagerView.Instance.EnableDisableButton(m_previous_button.GetComponent<Button>());


            m_previous_button.GetComponent<ButtonsOverView>().Disabled = true;

			// Change font color
			m_previous_button.transform.GetChild(0).gameObject.GetComponent<TMP_Text>().color = new Color(1, 1, 1, 0.3f);

		}

		public void ShowItemsTitle(string title)
		{
			if (title!=null) {
				m_items_title.gameObject.SetActive(true);
				m_items_title.text = title;
			} else {
				m_items_title.gameObject.SetActive(false);
			}
                
        }

        public LeaderboardsModel.LeaderboardType GetCurrentTab()
        {
            return currentTab;
        }

        public void UnLockAllTabs()
        {
            if (currentTab == LeaderboardsModel.LeaderboardType.Friends)
            {
                m_global.GetComponentInChildren<Button>().enabled = true;
                m_friends.GetComponentInChildren<Button>().enabled = false;
                m_hof.GetComponentInChildren<Button>().enabled = true;
            }

            if (currentTab == LeaderboardsModel.LeaderboardType.GlobalCurrent || currentTab == LeaderboardsModel.LeaderboardType.GlobalPrevious)
            {
                m_global.GetComponentInChildren<Button>().enabled = false;
                m_friends.GetComponentInChildren<Button>().enabled = true;
                m_hof.GetComponentInChildren<Button>().enabled = true;

                if (currentTab==LeaderboardsModel.LeaderboardType.GlobalCurrent) {
                    m_previous_button.GetComponent<Button>().enabled = true;
                } else {
                    m_current_button.GetComponent<Button>().enabled = true;
                }
            }

            if (currentTab == LeaderboardsModel.LeaderboardType.HallOfFame)
            {
                m_global.GetComponentInChildren<Button>().enabled = true;
                m_friends.GetComponentInChildren<Button>().enabled = true;
                m_hof.GetComponentInChildren<Button>().enabled = false;
            }
        }

        public void StartTimer()
        {
            m_timerObj.SetActive(true);

            LeaderboardModel ldrModel = ModelManager.Instance.Leaderboards.GetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalCurrent);
            if (ldrModel.LeaderboardMetaData!=null) {
                TimeSpan timeDiff = TimeSpan.FromSeconds(DateUtils.TotalSeconds(ldrModel.LeaderboardMetaData.TimeToEnd));
                
                StopAllCoroutines();
                
                StartCoroutine(updateMinuteTimer(timeDiff));
            }
        }

        public void StopTimer()
        {
            m_timerObj.SetActive(false);
            StopAllCoroutines();
        }


        IEnumerator updateMinuteTimer(TimeSpan timeRemain)
        {
            while (timeRemain.Days > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, 1);
                timeRemain = timeRemain.Subtract(ts);
                m_timer_text.text = "Ends in: " + timeRemain.Days.ToString() + "d " + timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");

                yield return new WaitForSecondsRealtime(1f);
            }

            StopAllCoroutines();
            StartCoroutine(updateSecondsTimer(timeRemain));
        }


        IEnumerator updateSecondsTimer(TimeSpan timeRemain)
        {
            while (timeRemain.TotalSeconds > 0)
            {
                TimeSpan ts = new TimeSpan(0, 0, 1);
                timeRemain = timeRemain.Subtract(ts);

                m_timer_text.text = "Ends in: " + timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);
            }


            // TODO: This is not the place for those

            //clear cache when the leaderboard ends
            ModelManager.Instance.Leaderboards.ClearCache(true);

            //close leaderboard when timer ends
            LeaderboardsController.Instance.HideScreen();
        }

    }


}
