﻿using cardGames.controllers;
using cardGames.models;
using common.utils;
using spades.controllers;
using System;
using System.Collections;

namespace spades.views
{

    public class ProgressView : MonoBehaviour
    {

        const float TOTAL_FILL_TIME = 2f;

        [SerializeField] TMP_Text m_progress_text = null;
        [SerializeField] TMP_Text m_name_text = null;
        [SerializeField] TMP_Text m_level_text = null;
        [SerializeField] RectTransform m_fill = null;

        [SerializeField] GameObject m_xp_object = null;

        private IEnumerator m_progress_bar_coroutine = null;
        bool m_stop_coroutine_flag = false;

        Action<bool> m_done_progress = null;

        float m_empty_value = -302f;

        int m_level = 0;//for format

        bool m_leveled_up;


        public void ShowInitialData(int xp, int total_xp, int level)
        {
            m_level = level;

            if (xp == 0 && total_xp == 0)
            {
                m_progress_text.text = "";
                m_fill.localPosition = new Vector3(m_empty_value, 0, 0);
            }
            else
            {
                m_progress_text.text = FormatProgress((float)xp * 100 / (float)total_xp);
                if (m_progress_text.text == FormatProgress(100f))
                    m_progress_text.text = "99.9%";
                //m_text.text = (int)xp*100/total_xp+"%";

                float fill_position = Mathf.Lerp(m_empty_value, 0, (float)xp / (float)total_xp);
                m_fill.localPosition = new Vector3(fill_position, 0, 0);
            }


            if (m_level_text != null)
            {
                if (level == 0)
                    m_level_text.text = "";
                else
                    m_level_text.text = "LVL <b>" + level + "</b>";
            }

            SetUserNameText(ModelManager.Instance.GetUser().GetMAvatar().NickName);

        }


        public void StartProgressAnimation(bool ff_animation = false, Action<bool> done_progress = null)
        {
            m_stop_coroutine_flag = false;
            m_done_progress = done_progress;
            m_leveled_up = false;

            if (!ff_animation)
            {

                User user = ModelManager.Instance.GetUser();
                int old_level = LobbyController.Instance.GetOldLevel();
                int old_threshold = LobbyController.Instance.Old_threshold;
                int old_xp = LobbyController.Instance.GetOldXP();

                if (old_threshold == 0)
                    old_threshold = user.GetLevelTotalXP();

                int level_xp_total = user.GetLevelTotalXP();

                if (old_level == user.GetLevel())
                {
                    //Level was not changed 
                    m_progress_text.text = FormatProgress((float)old_xp * 100 / (float)level_xp_total);
                    if (m_progress_text.text == FormatProgress(100f))
                        m_progress_text.text = "99.9%";

                    float new_value = (float)user.GetXPInLevel() / (float)level_xp_total;
                    m_progress_bar_coroutine = ProgressBarUpdate(new_value, old_xp, user.GetXPInLevel() - old_xp, level_xp_total);
                    StartCoroutine(m_progress_bar_coroutine);
                }
                else
                {
                    //Level CHANGED INCREASED
                    m_progress_text.text = FormatProgress((float)old_xp * 100 / (float)old_threshold);
                    if (m_progress_text.text == FormatProgress(100f))
                        m_progress_text.text = "99.9%";

                    m_progress_bar_coroutine = ProgressBarUpdate(1f, old_xp, old_threshold - old_xp, old_threshold);
                    StartCoroutine(m_progress_bar_coroutine);

                    LobbyController.Instance.SetOldLevel(user.GetLevel());
                    LobbyController.Instance.Old_threshold = user.GetLevelTotalXP();
                    LobbyController.Instance.SetOldXP(user.GetXPInLevel());
                }
            }
            else
                StopCoroutineAndShowEnd();
        }





        public void StopCoroutineAndShowEnd()
        {

            m_stop_coroutine_flag = true;

            if (m_progress_bar_coroutine != null)
                StopCoroutine(m_progress_bar_coroutine);

            User user = ModelManager.Instance.GetUser();
            int level_xp_total = user.GetLevelTotalXP();
            float fill_ratio = user.GetXPInLevel() / (float)level_xp_total;

            m_progress_text.text = FormatProgress((float)user.GetXPInLevel() * 100 / (float)level_xp_total);
            if (m_progress_text.text == FormatProgress(100f))
                m_progress_text.text = "99.9%";

            m_level_text.text = "LVL <b>" + user.GetLevel() + "</b>";

            float fill_position = Mathf.Lerp(m_empty_value, 0, fill_ratio);
            m_fill.localPosition = new Vector3(fill_position, 0, 0);

            //	if (m_done_progress != null)
            // m_done_progress(false);

        }

        IEnumerator ProgressBarUpdate(float fill_ratio, float start_value, int amount, float thresh)
        {
            if (!m_stop_coroutine_flag && amount > 0)
                CardGamesSoundsController.Instance.XPUpdate();

            float f_start_time = Time.realtimeSinceStartup;

            float time_counter = 0;
            float start_progress;

            float fill_position = Mathf.InverseLerp(m_empty_value, 0, m_fill.localPosition.x);
            start_progress = fill_position;

            float ratio = fill_ratio - start_progress;

            //LoggerController.Instance.Log ("Amount to fill: " + ratio);

            float time_to_move = TOTAL_FILL_TIME * ratio;

            //LoggerController.Instance.Log ("Time To Move: " + time_to_move);

            while (time_counter < time_to_move && !m_stop_coroutine_flag)
            {

                //LoggerController.Instance.Log ("Time Counter: " + time_counter);

                time_counter = (Time.realtimeSinceStartup - f_start_time);// / time_to_move; 

                m_progress_text.text = FormatProgress((float)start_value * 100 / thresh + (amount * Mathf.Min(time_counter / time_to_move, 1f) * 100 / thresh));
                if (m_progress_text.text == FormatProgress(100f))
                    m_progress_text.text = "99.9%";

                fill_position = Mathf.Lerp(m_empty_value, 0, Mathf.Lerp(start_progress, fill_ratio, time_counter / time_to_move));
                m_fill.localPosition = new Vector3(fill_position, 0, 0);

                yield return new WaitForSecondsRealtime(0.02f);
            }

            if (fill_ratio >= 1 && !m_stop_coroutine_flag)
            {
                m_level_text.text = "LVL <b>" + ModelManager.Instance.GetUser().GetLevel() + "</b>";

                m_leveled_up = true;

                m_fill.localPosition = new Vector3(m_empty_value, 0, 0);

                LevelUpOverlayClosed();



            }
            else
            {

                if (time_to_move < TOTAL_FILL_TIME)
                    yield return new WaitForSecondsRealtime(TOTAL_FILL_TIME - time_to_move);

                if (m_done_progress != null)
                    m_done_progress(m_leveled_up);
            }

        }

        private void LevelUpOverlayClosed()
        {

            User user = ModelManager.Instance.GetUser();
            int level_xp_total = user.GetLevelTotalXP();
            float fill_ratio = (float)user.GetXPInLevel() / (float)level_xp_total;

            float start_value = (float)user.GetXPInLevel() / (float)level_xp_total;

            m_progress_text.text = (int)start_value + "%";
            m_progress_bar_coroutine = ProgressBarUpdate(fill_ratio, 0, user.GetXPInLevel(), level_xp_total);
            StartCoroutine(m_progress_bar_coroutine);

        }


        public string FormatProgress(float value)
        {
            if (m_level > 20)
            {
                return String.Format("{0:F1}%", value);
            }
            else
            {
                return String.Format("{0:F0}%", value);
            }
        }

        public void SetUserNameText(string name)
        {
            m_name_text.text = name;
            TMPBidiHelper.MakeRTL(m_name_text);
        }


        public GameObject Xp_object
        {
            get
            {
                return m_xp_object;
            }
        }

    }
}
