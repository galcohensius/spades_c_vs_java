using common.models;
using common.utils;
using spades.controllers;
using common.facebook.views;
using cardGames.models;

/// <summary>
/// used to be in common.views however is deep ingrined in spades.
/// in use and awaits refactoring
/// </summary>

namespace spades.views
{
    public class LeaderboardAlerts : MonoBehaviour
    {
        //const int NUM_FRIENDS_TO_UNLOCK = 1; Currently fixed at 1

        [SerializeField] TMP_Text m_fb_frends_text = null;
        [SerializeField] GameObject m_no_friends_obj = null;
        [SerializeField] GameObject m_no_facebook_obj = null;
        [SerializeField] GameObject m_blackScreenBlock = null;
        [SerializeField] FacebookButton m_fb_connect_button = null;

        [SerializeField] TMP_Text m_facebookConnectDescriptionText = null;

        private const string FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG = "Get <color=#ffff0f>{0}</color> free coins!";

        public enum LeaderboardAlertType
        {
            NO_FACEBOOK_ACCOUNT,
            NO_FACEBOOK_FRIENDS
        }

        public void ShowTextMessage(LeaderboardAlertType type)
        {
            hideAll();

            switch (type)
            {
                case LeaderboardAlertType.NO_FACEBOOK_ACCOUNT:
                    m_no_facebook_obj.SetActive(true);
                    SetFacebookConnectTextMessage();
                    break;

                case LeaderboardAlertType.NO_FACEBOOK_FRIENDS:
                    m_no_friends_obj.SetActive(true);
                    break;
            }
        }

        private void SetFacebookConnectTextMessage()
        {
            FacebookBonus bonus = (FacebookBonus)ModelManager.Instance.FindBonus(Bonus.BonusTypes.FacebookConnect);

            m_facebookConnectDescriptionText.gameObject.SetActive(false);
            m_fb_connect_button.SetMode(FacebookButton.ButtonMode.Connect);

            if (bonus != null)
            {
                if (!bonus.IsEntitled)
                {
                    m_facebookConnectDescriptionText.gameObject.SetActive(true);
                    m_facebookConnectDescriptionText.text = string.Format(FACEBOOK_CONNECT_BONUS_ENTITLED_TEXT_MSG, FormatUtils.FormatBalance(bonus.CoinsSingle));
                    m_fb_connect_button.SetMode(FacebookButton.ButtonMode.Connect, true);
                }
            }
        }


        public void BT_ConnectToFBClicked()
        {
            m_fb_connect_button.SetMode(FacebookButton.ButtonMode.Waiting);
            LobbyController.Instance.FBLogin((bool success) =>
            {

                if (success)
                {
                    //LeaderboardsController.Instance.ShowLeaderboard(LeaderboardsModel.LeaderboardType.Friends);
                    LeaderboardsController.Instance.HideScreen();

                }
                else
                {
                    m_fb_connect_button.SetMode(FacebookButton.ButtonMode.Connect);
                }
            });
        }


        public void hideAll()
        {
            m_no_friends_obj.SetActive(false);
            m_no_facebook_obj.SetActive(false);
            m_blackScreenBlock.SetActive(false);
        }
    }
}