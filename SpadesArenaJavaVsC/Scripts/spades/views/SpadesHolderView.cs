﻿using System.Collections;

namespace spades.views

{

	public class SpadesHolderView : MonoBehaviour {


		private const float					DELAY = 0.15f;
		[SerializeField]GameObject[]		m_spades_loading=null;

		bool								m_running=true;

		void OnEnable()
		{
			

			for (int i=0;i<5;i++)
				m_spades_loading [i].GetComponent<RectTransform> ().localScale = new Vector3 (0.7f,0.7f,0.7f);

			m_running = true;
			StartCoroutine (AnimateSpades ());
		}

		void OnDisable()
		{
			m_running = false;
		}

		IEnumerator AnimateSpades()
		{
			
			while (m_running)
			{

				for (int i = 0; i < 5; i++)
				{
					iTween.ScaleTo (m_spades_loading [i], iTween.Hash ("y", 1, "x", 1, "easeType", "linear","time", DELAY, "delay", i * DELAY));
					iTween.ScaleTo (m_spades_loading [i], iTween.Hash ("y", .7f, "x", .7f, "easeType", "linear","time", DELAY, "delay", i * DELAY+DELAY));
				}

				//this will make the routine wait for each spade to do its animation + empty one - so total is 5 spades + 1 space = 6
				yield return new WaitForSeconds (DELAY*6);
			}
		}


	}
}