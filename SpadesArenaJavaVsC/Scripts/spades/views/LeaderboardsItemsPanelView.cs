﻿using System.Collections;
using System.Collections.Generic;
using common.models;
using spades.controllers;
using cardGames.models;
using cardGames.views;

namespace spades.views
{
	
	public class LeaderboardsItemsPanelView : MonoBehaviour
	{
		[SerializeField] GameObject items_container = null;
		[SerializeField] GameObject me_row = null;
        [SerializeField] ScrollRect m_scroll_rect = null;

		private bool isMeRow = false;
		private Vector3 meRowPosition = new Vector3 ();
		private GameObject selfRowGridRef = null;
		private List<LeaderboardItemModel> items;
		private Coroutine m_showItems;
		private bool isCurrent = true;

		[SerializeField]GameObject	m_spades_holder = null;

		public void ShowItems (LeaderboardsModel.LeaderboardType lt)
		{
			m_spades_holder.SetActive (false);

            m_scroll_rect.onValueChanged.RemoveListener(onScroll);

            ClearItems ();

			if (lt == LeaderboardsModel.LeaderboardType.GlobalPrevious)
				isCurrent = false;
			else
				isCurrent = true;
			
			LeaderboardModel lbModel = ModelManager.Instance.Leaderboards.GetLeaderboard (lt);

			if (lbModel != null) {
				items = lbModel.Items;

				m_showItems = StartCoroutine (ShowItemsCourutine ());
			}


		}

		private IEnumerator ShowItemsCourutine ()
		{
			selfRowGridRef = null;
			int c = 0;

			if (items != null)
            {
                foreach (LeaderboardItemModel item in items)
                {

                    GameObject leaderboardItem = Instantiate(LeaderboardsController.Instance.LeaderboardItemRow);
                    leaderboardItem.transform.SetParent(items_container.transform);

                    LeaderboardItem itemLogic = leaderboardItem.GetComponent<LeaderboardItem>();

                    //if (ModelManager.Instance.Leaderboards.m_leaderboardType == LeaderboardsModel.LeaderboardType.HallOfFame) {

                    if (isCurrent)
                        itemLogic.Init(item, LeaderboardsModel.LeaderboardType.GlobalCurrent);
                    else
                        itemLogic.Init(item, LeaderboardsModel.LeaderboardType.GlobalPrevious);

                    RectTransform rt = leaderboardItem.GetComponent<RectTransform>();
                    rt.localScale = new Vector3(1f, 1f, 1f);
                    rt.localPosition = new Vector3(rt.localPosition.x, rt.localPosition.y, 0);

                    if (item.UserId == ModelManager.Instance.GetUser().GetID())
                    {
                        isMeRow = true;

                        meRowPosition = rt.position;

                        //me_rowLogic is floating item
                        LeaderboardItem me_rowLogic = me_row.GetComponent<LeaderboardItem>();

                        if (isCurrent)
                            me_rowLogic.Init(item, LeaderboardsModel.LeaderboardType.GlobalCurrent, true);
                        else
                            me_rowLogic.Init(item, LeaderboardsModel.LeaderboardType.GlobalPrevious, true);

                        me_rowLogic.setMeRowBg(isCurrent);

                        itemLogic.setMeRowBg(isCurrent); //set me row in items list
                                                         //me_row.GetComponent<Canvas> ().sortingOrder = 42;

                        selfRowGridRef = leaderboardItem;

                    }

                    c++;

                    if (c % 10 == 0)
                        yield return null;

                }

                yield return null;
                onScroll(Vector2.one);
            }
            yield return null; //this is here so there is another yield when c%10 doesnt make a yield
            m_scroll_rect.onValueChanged.AddListener(onScroll);

        }

		public void onScroll (Vector2 vec)
		{
			if (selfRowGridRef == null || items_container.transform.childCount == 0) {
				me_row.SetActive (false);
				return;
			}
			float bottom_limit = 350f - ManagerView.Instance.Lobby_canvas_landscape.GetComponent<RectTransform> ().rect.height;

			if (items_container.transform.localPosition.y + selfRowGridRef.transform.localPosition.y > bottom_limit &&
                items_container.transform.localPosition.y + selfRowGridRef.transform.localPosition.y <= -160) {
				me_row.SetActive (false);
			} else if (isMeRow && items_container.transform.localPosition.y + selfRowGridRef.transform.localPosition.y < bottom_limit) {
                // Me row at the bottom
				me_row.SetActive (true);
                me_row.transform.localPosition = new Vector3 (me_row.transform.localPosition.x, bottom_limit - 105, 0);
			} else if (isMeRow && items_container.transform.localPosition.y + selfRowGridRef.transform.localPosition.y > -110) {
                // Me row at the top
				me_row.SetActive (true);
                me_row.transform.localPosition = new Vector3 (me_row.transform.localPosition.x, -200, 0);
			}

		}

		public void ClearItems ()
		{
            StopAllCoroutines ();
			items = null;
			foreach (Transform C in items_container.transform) {
				Destroy (C.gameObject);
			}
				
			selfRowGridRef = null;
			me_row.SetActive (false);
			// Reset the scroll
			items_container.transform.localPosition = new Vector2 (0, 0);

            
		}

		public GameObject Sapdes_holder {
			get {
				return m_spades_holder;
			}
		}

        public bool HasItems {
            get {
                return items!=null;
            }
        }
	}

}
	