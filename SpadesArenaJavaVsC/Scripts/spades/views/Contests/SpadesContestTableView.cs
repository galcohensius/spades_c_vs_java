﻿using cardGames.models.contests;
using cardGames.views;
using common.views.popups;
using spades.comm;
using spades.controllers;
using spades.models;

namespace spades.views.contests
{
    public class SpadesContestTableView : ContestTableView
    {
        public override void SetData(Contest contest)
        {
            base.SetData(contest);

            string subVariat = SpadesModelParser.HandleSubVariants(contest.TableFilterData);
            string variation = SpadesModelParser.HandleVariants(contest.TableFilterData, subVariat);

            SpadesTableFilterData spadesTableFilterData = contest.TableFilterData as SpadesTableFilterData;

            string title = SpadesContestsController.Instance.FormatAvailable(contest);
            string description = "";

            if (subVariat == "Regular")
                subVariat = string.Empty;

            if (variation == string.Empty && subVariat == string.Empty)
                title = "<b>Available at all tables</b>";

            else if (subVariat != string.Empty)
                description = variation + subVariat + " only";

            else
                description = variation + " only";

            m_infoBubbleContainer.SetData(title + "\n" + description, InfoBubbleContainer.Placement.RightUp);
        }
    }
}
