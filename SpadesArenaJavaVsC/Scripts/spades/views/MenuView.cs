﻿using cardGames.controllers;
using cardGames.models;
using common.models;
using spades.controllers;

namespace spades.views
{
    public class MenuView : MonoBehaviour
    {
        [SerializeField] GameObject m_menu_button = null;
        [SerializeField] GameObject m_closeButtonSettingsPanel = null;

        [SerializeField] Button m_fullScreenBtn = null;
        [SerializeField] Button m_gameLogBtn = null;
        [SerializeField] Image m_gameLogBtnIcon = null;

        [SerializeField] Animator m_settingsPanelAnimator = null;

        [SerializeField] GameObject m_support_button = null;

        [SerializeField] Button m_clearCloseButton;

        [SerializeField] TMP_Text m_version;

        private void Start()
        {
            m_version.text = "Version: " + Application.version;
        }

        public void OpenSettingsPopop()
        {
            CloseMenuPanel();
            CardGamesSoundsController.Instance.ButtonClicked();
            if (LobbyController.Instance.Lobby_view.gameObject.activeSelf && !SideGamesController.Instance.IsMiniGameOpen())
                SpadesPopupManager.Instance.ShowSettingsPopup(common.controllers.PopupManager.AddMode.DontShowIfPopupShown, false);
            else
                SpadesPopupManager.Instance.ShowSettingsPopup(common.controllers.PopupManager.AddMode.DontShowIfPopupShown, true);
        }

        public void Button_Help()
        {
            string url = "https://bbumgameshelp.freshdesk.com/support/home";
            WebExternalController.Instance.OpenHelpPage(url);
        }

        public void Button_Support()
        {
            WebExternalController.Instance.OpenSupportPage();
        }

        public void Button_Privacy()
        {
            string url = "https://bbumgameshelp.freshdesk.com/support/solutions/articles/44001711371-privacy-policy";
            WebExternalController.Instance.OpenHelpPage(url);
        }

        // opens the menu from the inspector
        public void CloseMenuPanel()
        {
            m_clearCloseButton.gameObject.SetActive(false);
            m_clearCloseButton.interactable = false;
            m_closeButtonSettingsPanel.GetComponent<Button>().interactable = false;
            m_clearCloseButton.GetComponent<Image>().enabled = false;
            m_settingsPanelAnimator.SetBool("IsOpen", false);
            m_menu_button.GetComponent<Button>().interactable = true;
        }

        // closes the menu from the inspector
        public void OpenMenuPanel()
        {
            m_version.text = "Version: " + Application.version;

            m_clearCloseButton.gameObject.SetActive(true);
            m_clearCloseButton.interactable = true;
            m_clearCloseButton.GetComponent<Image>().enabled = true;
            m_menu_button.GetComponent<Button>().interactable = false;
            m_settingsPanelAnimator.SetBool("IsOpen", true);
            m_closeButtonSettingsPanel.GetComponent<Button>().interactable = true;

            if (LobbyController.Instance.Lobby_view.gameObject.activeSelf)
            {
                if (m_support_button == null)
                    return;

                InitMenuButtonsInLobby();
                if (ModelManager.Instance.GetUser().User_type == CommonUser.UserType.NonPaying)
                {
                    m_support_button.SetActive(false);
                }
                else
                {
                    m_support_button.SetActive(true);
                }
            }
            else
            {
                InitMenuButtonsInGame();
                if (m_support_button == null)
                    return;
                m_support_button.SetActive(false);
            }
        }

        private void InitMenuButtonsInLobby()
        {
#if !UNITY_WEBGL
            //De-Activate Full Screen Button
            m_fullScreenBtn.gameObject.SetActive(false);
#endif
        }

        private void InitMenuButtonsInGame()
        {
#if !UNITY_WEBGL
            //De-Activate Full Screen Button
            m_fullScreenBtn.gameObject.SetActive(false);
#endif
            // Round history button
            if (ModelManager.Instance.GetTable().GetRoundNumber() <= 1)
            {
                m_gameLogBtn.interactable = false;
                ManagerView.Instance.EnableDisableButton(m_gameLogBtn);

                m_gameLogBtnIcon.color = Color.gray;
            }
            else
            {
                m_gameLogBtn.interactable = true;
                ManagerView.Instance.EnableDisableButton(m_gameLogBtn);

                m_gameLogBtnIcon.color = Color.white;
            }
        }
    }
}