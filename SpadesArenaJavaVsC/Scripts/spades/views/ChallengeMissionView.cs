﻿using spades.models;
using System;
using spades.controllers;
using common.utils;
using cardGames.models;
using cardGames.controllers;
using common.views.popups;
using spades.comm;

namespace spades.views
{
    public class ChallengeMissionView : MonoBehaviour
    {
        InfoBubble m_smart_Info_Box_View;
        //Smart_Info_Box_View m_smart_Info_Box_View;
        [SerializeField] InfoBubbleContainer m_infoBubbleContainer;
        [SerializeField] RectTransform m_main;
        [SerializeField] RectTransform m_active_group;
        [SerializeField] RectTransform m_locked_group;

        [SerializeField] TMP_Text m_play_at_value_text;
        [SerializeField] TMP_Text m_play_at_value_bottom_text;

        [SerializeField] TMP_Text m_short_description_text;
        [SerializeField] TMP_Text m_active_description_text;
        [SerializeField] TMP_Text m_locked_description_text;
        [SerializeField] TMP_Text m_prize_text;
        [SerializeField] TMP_Text m_progress_text;
        [SerializeField] Image m_challenge_image;
        [SerializeField] Image m_locked_completed_image;
        [SerializeField] Image m_completed_v_sign;
        [SerializeField] Image m_locked_completed_bg_image;
        [SerializeField] GameObject m_swap_icon;

        [SerializeField] Sprite m_complete_v_sign_image;

        [SerializeField] Image m_progressBar_image;

        [SerializeField] Button m_action_button;

        GameObject m_info_box;

        Action m_close_popup;
        Action m_swap_mission_action = null;
        SpadesChallenge m_challenge;

        int m_challenge_index = 0;
        Action<bool> m_info_clicked;

        public void SetChallengeData(int challenge_index, SpadesChallenge challenge, Action Close_popup, Action Swap_Mission, InfoBubble smart_Info_Box_View, Action<bool> info_clicked)
        {
            m_challenge_index = challenge_index;

            m_smart_Info_Box_View = smart_Info_Box_View;
            m_info_box = smart_Info_Box_View.gameObject;
            m_info_clicked = info_clicked;

            m_swap_mission_action = Swap_Mission;
            m_close_popup = Close_popup;
            m_challenge = challenge;

            SetProgress();

            if (challenge.Status == SpadesMissionController.ChallengeStatus.Completed || challenge.Status == SpadesMissionController.ChallengeStatus.Locked)
                SetActiveLocked(false);
            else
                SetActiveLocked(true);
        }

        private void SetActiveLocked(bool active)
        {
            m_active_group.gameObject.SetActive(active);
            m_locked_group.gameObject.SetActive(!active);

            if (active)
                m_main.sizeDelta = new Vector2(0, SpadesMissionController.OPEN_HEIGHT);
            else
                m_main.sizeDelta = new Vector2(0, SpadesMissionController.DONE_HEIGHT);
        }

        public void UpdateIconAlpha(float value)
        {
            //m_manager.CanvasGroup.alpha = value;
            gameObject.GetComponent<CanvasGroup>().alpha = value;
        }

        private void GoToWorld(int world_index, PlayingMode playingMode)
        {
            if (m_close_popup != null)
                m_close_popup();

            LobbyController.Instance.Lobby_view.GetSwipe().InitSwipe(world_index + 1);
            LobbyController.Instance.Lobby_view.Mode_buttons_view.ModeClicked(playingMode == PlayingMode.Solo);
        }

        public void Button_SwapMissionCLicked()
        {
            if (m_swap_mission_action != null)
            {
                m_swap_mission_action();
                //m_info_clicked(m_info_box.activeSelf);
            }
        }

        public void BTN_InfoCLicked()
        {
            m_infoBubbleContainer.Button_clicked();
           // m_infoBubbleContainer.SetPlacement(InfoBubbleContainer.Placement.LeftDown);
        }

        public void Button_InfoCLicked()
        {
            if (m_info_box.activeSelf)
                m_info_box.SetActive(false);
            else
                m_info_box.SetActive(true);

            m_info_clicked(m_info_box.activeSelf);
        }

        public void HideInfoBox()
        {
            if (m_info_box != null && m_info_box.activeSelf)
                m_info_box.SetActive(false);
        }

        //used for hiding the effects before closing the entire popup
        public void HideChallengeEffects()
        {
            m_challenge_image.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            m_challenge_image.gameObject.transform.GetChild(1).gameObject.SetActive(false);
        }

        public void Button_InfoBoxOkClicked()
        {
            m_info_box.SetActive(false);
        }

        private void SetProgress()
        {
            int world_index = 0;
            if (m_challenge.Bet_operator == SpadesMissionController.BetOperator.Min)
            {
                //go to highest open table
                world_index = LobbyController.Instance.GetWorldIndexFromBuyIn(m_challenge.Bet);
            }
            else
            {
                int bet = m_challenge.Bet;
                world_index = -1;
                //go to exact room
                //same behaviour for exat and Max
                foreach (SpadesWorld world in ModelManager.Instance.Worlds)
                {
                    if (world.WorldType != World.WorldTypes.Regular)
                        continue;

                    //find highest level 
                    foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(PlayingMode.Solo))
                    {

                        if (!singleitem.Is_special && singleitem.Buy_in >= bet && world_index == -1)
                        {
                            world_index = world.Index;
                            continue;
                        }
                    }

                    foreach (SpadesSingleMatch singleitem in world.GetSingleMatches(PlayingMode.Partners))
                    {

                        if (!singleitem.Is_special && singleitem.Buy_in >= bet && world_index == -1)
                        {
                            world_index = world.Index;
                            continue;
                        }
                    }
                    if (world.GetArena(PlayingMode.Solo).GetBuyInCoins() >= bet && world_index == -1)
                        world_index = world.Index;

                    if (world.GetArena(PlayingMode.Partners).GetBuyInCoins() >= bet && world_index == -1)
                        world_index = world.Index;
                }
            }

            m_action_button.onClick.AddListener(() =>
            {

                PlayingMode playingMode = PlayingMode.Partners;

                if (m_challenge.SpadesTableFilterData.PlayingMode == PlayingMode.Solo)
                    playingMode = PlayingMode.Solo;

                GoToWorld(world_index, playingMode);
            });

            if (m_challenge.Status == SpadesMissionController.ChallengeStatus.Locked)
            {
                m_locked_description_text.text = "CHALLENGE LOCKED";
                m_locked_description_text.color = SpadesMissionController.Instance.MISSION_LOCKED_COLOR;
                m_locked_completed_bg_image.sprite = SpadesMissionController.Instance.Locked_bg;
                m_locked_completed_image.sprite = SpadesMissionController.Instance.GetMissionIcon(m_challenge);
            }
            else if (m_challenge.Status == SpadesMissionController.ChallengeStatus.Completed)
            {
                m_locked_description_text.text = "CHALLENGE COMPLETE";
                m_completed_v_sign.sprite = m_complete_v_sign_image;
                m_locked_completed_bg_image.sprite = MissionController.Instance.Completed_bg;
                m_locked_completed_image.sprite = MissionController.Instance.GetMissionIcon(m_challenge);
            }
            else//this is a InProgress or Swapped or Init - which are all similar and are like InProgress
            {
                m_active_description_text.text = MissionController.Instance.GetDescriptionFromChallenge(m_challenge);
            }

            m_challenge_image.sprite = MissionController.Instance.GetMissionIcon(m_challenge);

            m_prize_text.text = "<sprite=1>" + FormatUtils.FormatBalance(m_challenge.Prize);

            m_short_description_text.text = MissionController.Instance.GetTitleFromChallenge(m_challenge);

            m_play_at_value_bottom_text.text = "And above";


            if (m_challenge.Bet_operator == SpadesMissionController.BetOperator.Exact)
                m_play_at_value_bottom_text.text = "";
            else if (m_challenge.Bet_operator == SpadesMissionController.BetOperator.Max)
                m_play_at_value_bottom_text.text = "And below";

            HandleAvailableOnData();

            m_play_at_value_text.text = "Bet " + FormatUtils.FormatBuyIn(m_challenge.Bet);// + " " + game_mode_text;

            int progress = m_challenge.Progress;

            int total = m_challenge.TotalProgress;

            m_progressBar_image.fillAmount = (float)progress / (float)total;

            m_progress_text.text = FormatUtils.FormatBalance(progress) + "/" + FormatUtils.FormatBalance(total);

            m_swap_icon.SetActive(m_challenge.Swap_left > 0);
        }

        void HandleAvailableOnData()
        {
            string gameMode = "";
            if (m_challenge.SpadesTableFilterData.PlayingMode == PlayingMode.Solo)
                gameMode += "<b>Available at </b>Solo tables";

            else if (m_challenge.SpadesTableFilterData.PlayingMode == PlayingMode.Partners)
                gameMode += "<b>Available at </b>Partners tables";

            else
                gameMode += "<b>Available at </b>Partners & Solo tables";

            string description = string.Empty;
            string subVariants = SpadesModelParser.HandleSubVariants(m_challenge.SpadesTableFilterData);
            string variants = SpadesModelParser.HandleVariants(m_challenge.SpadesTableFilterData, subVariants);

            if (subVariants == string.Empty && variants == string.Empty)
                description = "";

            else if(subVariants != string.Empty)
                description = variants + subVariants + " only";

            else if (variants != string.Empty)
                description = variants;

            m_infoBubbleContainer.SetData(gameMode + "\n" + description, InfoBubbleContainer.Placement.LeftDown);
        }

        string HandleVariants(string subVariat)
        {
            string variation = "";
            int numOfVariants = 0;
            bool containsClassic = false;

            foreach (PlayingVariant item in m_challenge.SpadesTableFilterData.PlayingVariants)
            {
                if (item == PlayingVariant.Classic)
                    containsClassic = true;
                numOfVariants++;
            }

            int counter = 0;
            if (containsClassic)
            {
                if (numOfVariants == 1)
                    variation += PlayingVariant.Classic;
                else
                    variation += PlayingVariant.Classic + ", ";
            }

            foreach (PlayingVariant item in m_challenge.SpadesTableFilterData.PlayingVariants)
            {
                if (item == PlayingVariant.Classic)
                {
                    counter++;
                    continue;
                }
                string name = SpadesModelParser.VariantEnumToString(item);

                if (numOfVariants > 1)
                {
                    if (counter == numOfVariants - 1 && subVariat == string.Empty)
                        variation += " and " + name + "";
                    else if (counter == 0)
                        variation += name;
                    else
                        variation += ", " + name;
                }
                else
                {
                    variation += name;
                }

                counter++;
            }
            variation = PlayingVariant.Classic.ToString();
            return variation;
        }

        string HandleSubVariants()
        {
            string subVariant = "";
            int numOfSubVariants = 0;
            foreach (PlayingSubVariant item in m_challenge.SpadesTableFilterData.PlayingSubVariants)
                numOfSubVariants++;

            int counter = 0;
            foreach (PlayingSubVariant item in m_challenge.SpadesTableFilterData.PlayingSubVariants)
            {

                string name = SpadesModelParser.SubVariantEnumToString(item);
                if (item == PlayingSubVariant.Regular)
                    continue;

                if (counter == numOfSubVariants - 1)
                    subVariant += " and " + name + "";
                else if (counter == 0)
                    subVariant += name;
                else
                    subVariant += ", " + name;

                counter++;

            }
            subVariant = PlayingSubVariant.Regular.ToString();

            return subVariant;
        }
    }
}

