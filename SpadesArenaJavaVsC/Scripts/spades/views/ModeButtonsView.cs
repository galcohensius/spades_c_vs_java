using System.Collections;
using spades.models;
using spades.controllers;
using cardGames.controllers;

namespace spades.views

{

    public class ModeButtonsView : MonoBehaviour
    {

        bool m_is_rotating = false;
        [SerializeField] Sprite m_solo_bg = null;
        [SerializeField] Sprite m_partners_bg = null;

        [SerializeField] Image m_panel_bg = null;
        [SerializeField] RectTransform m_back_bg = null;
        [SerializeField] RectTransform m_highlight = null;

        [SerializeField] TMP_Text m_partner_text = null;
        [SerializeField] TMP_Text m_solo_text = null;

        private Color m_color;

        public void HideModeButtons()
        {
            gameObject.SetActive(false);
        }

        public void ShowModeButtons()
        {
            gameObject.SetActive(true);

        }

        public void Draw(PlayingMode pm, Color color)
        {
            m_color = color;
            if (pm == PlayingMode.Solo)
            {
                m_panel_bg.sprite = m_solo_bg;

                m_back_bg.localPosition = new Vector3(583f, m_back_bg.localPosition.y, 0);
                m_highlight.localPosition = new Vector3(305f, m_highlight.localPosition.y, 0);

                m_solo_text.fontSize = 45;
                m_partner_text.fontSize = 42;

                m_solo_text.color = new Color(m_solo_text.color.r, m_solo_text.color.g, m_solo_text.color.b, 1f);
                m_partner_text.color = new Color(m_partner_text.color.r, m_partner_text.color.g, m_partner_text.color.b, .6f);

            }
            else
            {
                m_panel_bg.sprite = m_partners_bg;

                m_back_bg.localPosition = new Vector3(146.1f, m_back_bg.localPosition.y, 0);
                m_highlight.localPosition = new Vector3(630f, m_highlight.localPosition.y, 0);

                m_solo_text.fontSize = 42;
                m_partner_text.fontSize = 45;

                m_solo_text.color = new Color(m_solo_text.color.r, m_solo_text.color.g, m_solo_text.color.b, .6f);
                m_partner_text.color = new Color(m_partner_text.color.r, m_partner_text.color.g, m_partner_text.color.b, 1f);
            }

            m_highlight.gameObject.GetComponent<Image>().color = m_color;
        }

        public void ModeClicked(bool solo_clicked)
        {
            if (!m_is_rotating)
            {

                CardGamesSoundsController.Instance.TabsSFX();
                if (solo_clicked && SpadesModelManager.Instance.Playing_mode == PlayingMode.Partners)
                {
                    //click the solo
                    m_is_rotating = true;
                    SpadesModelManager.Instance.Playing_mode = PlayingMode.Solo;
                }
                else if (!solo_clicked && SpadesModelManager.Instance.Playing_mode == PlayingMode.Solo)
                {
                    m_is_rotating = true;
                    SpadesModelManager.Instance.Playing_mode = PlayingMode.Partners;
                }

                if (m_is_rotating)
                    StartCoroutine(WaitAndUnlockMode());

                SpadesStateController.Instance.SetPlayingMode(SpadesModelManager.Instance.Playing_mode);
                Draw(SpadesModelManager.Instance.Playing_mode, m_color);
            }

        }

        private IEnumerator WaitAndUnlockMode()
        {
            float total_time = WorldItemView.DELAY_BETWEEN_PANELS_SPIN * 4 + WorldItemView.SPIN_TIME;

            yield return new WaitForSeconds(total_time);

            m_is_rotating = false;


        }


        private void OnEnable()
        {
            m_is_rotating = false;
        }
    }
}
