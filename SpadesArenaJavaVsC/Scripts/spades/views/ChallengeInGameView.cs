using System.Collections;
using spades.models;
using System;
using spades.controllers;
using common.utils;
using cardGames.models;
using cardGames.controllers;
using common.controllers;

namespace spades.views

{
    public class ChallengeInGameView : MonoBehaviour
    {

        const float ALTERNTATE_INTERVAL = 2f;
        const float ALTERNTATE_SWITCH = .5f;

        [SerializeField] GameObject m_progress_group;
        [SerializeField] GameObject m_mission_button;

        [SerializeField] GameObject m_prize_timer_alternate_group;

        [SerializeField] GameObject m_prize_group;
        [SerializeField] GameObject m_timer_group;
        [SerializeField] GameObject m_info_group;

        [SerializeField] GameObject m_challenge_avail_group;
        [SerializeField] GameObject m_expired_group;

        [SerializeField] GameObject m_body_group;

        [SerializeField] TMP_Text m_body_text;
        [SerializeField] TMP_Text m_timer_text;
        [SerializeField] TMP_Text m_desc_title_text;

        [SerializeField] TMP_Text m_prize_text;
        [SerializeField] TMP_Text m_progress_text;
        [SerializeField] TMP_Text m_info_text;

        [SerializeField] Image m_progressBar_image;

        [SerializeField] GameObject m_active_effect_Object;
        [SerializeField] GameObject m_burst_effect_holder;

        [SerializeField] GameObject m_active_group_bg;

        CanvasGroup m_timer_canvas;
        CanvasGroup m_prize_canvas;
        CanvasGroup m_info_canvas;

        RectTransform m_rect;

        Action m_panel_anim_done;

        Coroutine m_timer_routine;
        IEnumerator m_alternate_routine = null;
        IEnumerator m_auto_play_out = null;


        bool m_challenge_panel_showing = false;
        float x_pos_in = 0;
        float x_pos_out = 0;
        float y_pos = 0;
        float screen_actual_width;
        float panel_actual_width;

        float screen_actual_height;
        float panel_actual_height;

        private void Awake()
        {
            m_timer_canvas = m_timer_group.GetComponent<CanvasGroup>();
            m_prize_canvas = m_prize_group.GetComponent<CanvasGroup>();
            m_info_canvas = m_info_group.GetComponent<CanvasGroup>();

            //position the parent relative to the screen aspect as alignment of the rectTransform causes problems for the iTween
            m_rect = GetComponent<RectTransform>();


        }

        private void CalcPositions()
        {
            screen_actual_width = ManagerView.Instance.Actual_width / 2;
            panel_actual_width = 480f;// m_rect.rect.width;

            screen_actual_height = ManagerView.Instance.Actual_height / 2;
            panel_actual_height = 300f;// m_rect.rect.height;

            y_pos = screen_actual_height - panel_actual_height + 210f;

            x_pos_out = screen_actual_width + panel_actual_width - 90f;
            x_pos_in = x_pos_out - panel_actual_width - 130f;
        }

        private void Start()
        {

            CalcPositions();

            m_rect.localPosition = new Vector3(x_pos_out, y_pos, 0);
        }

        private void OpenPanel(bool panel_in, Action panel_anim_done)
        {
            if (x_pos_in == 0 && x_pos_out == 0)
                CalcPositions();

            CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.slider_open);

            if (!panel_in)
            {
                m_panel_anim_done = panel_anim_done;
                Burst_effect_holder.SetActive(false);

                iTween.MoveTo(gameObject, iTween.Hash("x", x_pos_out, "islocal", true, "easeType", "easeInQuad", "time", .2));
                iTween.MoveTo(m_mission_button, iTween.Hash("x", -470f, "islocal", true, "easeType", "easeInQuad", "time", .2, "onComplete", "PanelAnimDone", "onCompleteTarget", this.gameObject));
                m_challenge_panel_showing = false;
            }

            else
            {
                m_panel_anim_done = panel_anim_done;
                Burst_effect_holder.SetActive(true);
                Burst_effect_holder.GetComponent<Animator>().SetTrigger("burst");
                iTween.MoveTo(gameObject, iTween.Hash("x", x_pos_in, "islocal", true, "easeType", "easeInQuad", "time", .2));
                iTween.MoveTo(m_mission_button, iTween.Hash("x", -235f, "islocal", true, "easeType", "easeInQuad", "time", .2, "onComplete", "PanelAnimDone", "onCompleteTarget", this.gameObject));
                m_challenge_panel_showing = true;
            }

        }

        public void ShowChallengePanel(bool show)
        {
            gameObject.SetActive(show);
        }

        private void PanelAnimDone()
        {
            if (m_panel_anim_done != null)
            {
                m_panel_anim_done();
                m_panel_anim_done = null;
            }
        }

        public void TakePanelOut()
        {
            // m_rect can be null on leave table calls that happen before it is initialized (like disconnection due to 
            // another device)
            if (m_rect != null)
            {
                m_mission_button.GetComponent<RectTransform>().localPosition = new Vector3(-470f, 0, 0);
                m_rect.localPosition = new Vector3(x_pos_out, y_pos, 0);

                gameObject.SetActive(false);
            }



        }

        public void ShowActiveChallenge(bool auto_play)
        {
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            ResetAllVisualComponents();

            SpadesChallenge curr_challenge = mission.GetActiveChallenge() as SpadesChallenge;

            if (curr_challenge == null)
            {
                //no active challenge - means all the challenges are complete - we need to show timer for next mission
                m_desc_title_text.gameObject.SetActive(true);
                return;
            }

            SetTimer("MISSION ENDS IN: ");

            if (MissionController.Instance.IsChallengeActiveInActiveMatch(curr_challenge))
            {
                if (MissionController.Instance.IsMissionExpired())
                {
                    ResetAllVisualComponents();

                    m_expired_group.SetActive(true);
                    m_active_group_bg.SetActive(false);
                    //hiding the active BG

                }
                //the challenge is active - check if we need to animate from previous value
                else if (MissionController.Instance.Curr_challenge_before_update != null)
                {
                    int start_value = MissionController.Instance.Curr_challenge_before_update.Progress;
                    int target_value = curr_challenge.Progress;
                    int total_value = (MissionController.Instance.Curr_challenge_before_update as SpadesChallenge).TotalProgress;
                    ShowChallengeInTable(curr_challenge);
                    StartCoroutine(IncreaseProgress(start_value, target_value, total_value));
                }
                else
                {
                    ShowChallengeInTable(curr_challenge);
                    SetProgress(curr_challenge);
                }

            }
            else
            {
                //NOT USED
                //ShowChallengeInOtherTable(curr_challenge);
            }

            if (auto_play)
            {
                m_auto_play_out = AutoShowPanel();
                StartCoroutine(m_auto_play_out);
            }

        }

        IEnumerator AutoShowPanel()
        {
            yield return new WaitForSeconds(0.5f);
            OpenPanel(true, null);
            yield return new WaitForSeconds(SpadesMissionController.AUTO_SHOW_CHALLENGE_TIME);
            OpenPanel(false, null);
        }

        private void ShowChallengeInTable(SpadesChallenge curr_challenge)
        {
            m_desc_title_text.gameObject.SetActive(true);
            m_desc_title_text.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(50f, 45f, 0);

            m_body_group.SetActive(true);
            m_body_text.text = MissionController.Instance.GetDescriptionFromChallenge(curr_challenge);

            m_progress_group.SetActive(true);
            m_active_effect_Object.SetActive(true);
            m_prize_timer_alternate_group.SetActive(true);
            m_alternate_routine = AlternatePrize_Timer(true);
            StartCoroutine(m_alternate_routine);


        }

        private void ShowChallengeInOtherTable(SpadesChallenge curr_challenge)
        {
            m_desc_title_text.text = MissionController.Instance.GetTitleFromChallenge(curr_challenge);
            m_challenge_avail_group.SetActive(true);

            m_desc_title_text.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(50f, 0, 0);
            m_desc_title_text.gameObject.SetActive(true);

            //starts empty for the case of Both tables are ok
            string game_mode_text = "";
            //TODO: fix with varients 
            /*
            if (curr_challenge.Game_mode == SpadesMissionController.GameMode.Solo)
                game_mode_text = "SOLO";
            else if (curr_challenge.Game_mode == SpadesMissionController.GameMode.Partner)
                game_mode_text = "PARTNERS";
                */
            m_info_text.text = "ON " + GetTitle(curr_challenge) + ": " + FormatUtils.FormatBuyIn(curr_challenge.Bet) + " " + game_mode_text;


            m_active_effect_Object.SetActive(true);
            m_prize_timer_alternate_group.SetActive(true);

            m_alternate_routine = AlternatePrize_Timer(false);
            StartCoroutine(m_alternate_routine);
        }

        private void ResetAllVisualComponents()
        {
            if (m_alternate_routine != null)
                StopCoroutine(m_alternate_routine);

            m_timer_text.gameObject.SetActive(false);
            m_prize_timer_alternate_group.SetActive(false);
            m_progress_group.SetActive(false);
            m_desc_title_text.gameObject.SetActive(false);
            m_expired_group.SetActive(false);
            m_active_group_bg.SetActive(true);
            m_active_effect_Object.SetActive(false);
            m_challenge_avail_group.SetActive(false);

            m_timer_group.SetActive(false);
            m_prize_group.SetActive(false);
            m_info_group.SetActive(false);

        }



        private void SetTimer(string timer_text_prefix)
        {
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            TimeSpan delta = mission.End_date - DateTime.Now;

            if (delta.TotalSeconds > 0)
            {
                m_timer_text.gameObject.SetActive(true);
                m_expired_group.SetActive(false);
                m_timer_routine = StartCoroutine(RunTimer(delta, m_timer_text, timer_text_prefix));
            }
            else
            {
                //m_timer_text.text = "EXPIRED";
                ResetAllVisualComponents();
                m_expired_group.SetActive(true);
                m_active_group_bg.SetActive(false);
            }

        }

        private void SetProgress(SpadesChallenge challenge)
        {
            SpadesMission mission = ModelManager.Instance.GetMission() as SpadesMission;

            m_desc_title_text.text = MissionController.Instance.GetTitleFromChallenge(challenge);
            m_prize_text.text = "<sprite=1>" + FormatUtils.FormatBalance(challenge.Prize);

            int total = challenge.TotalProgress;
            int counter = challenge.Progress;

            m_progressBar_image.fillAmount = counter / (float)challenge.TotalProgress;

            if (counter >= total)
                m_progress_text.text = "COMPLETE";
            else
                m_progress_text.text = FormatUtils.FormatBalance(counter) + "/" + FormatUtils.FormatBalance(total);
        }

        IEnumerator IncreaseProgress(int start_value, int target_value, int total_value)
        {

            target_value = Math.Min(target_value, total_value);

            if (start_value > target_value)
                start_value = target_value;

            float start_val = start_value / (float)total_value;
            float end_val = target_value / (float)total_value;


            m_progressBar_image.fillAmount = start_val;

            m_progress_text.text = start_value + "/" + total_value;

            yield return new WaitForSeconds(0.5f);

            iTween.ValueTo(gameObject, iTween.Hash("from", start_val, "to", end_val, "time", .5f, "onupdate", "UpdateProgresBar", "onupdatetarget", this.gameObject));
            CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.slider_barFull);

            m_progress_text.text = target_value + "/" + total_value;

            yield return new WaitForSecondsRealtime(1.5f);

            if (m_progressBar_image.fillAmount == 1f)
            {
                CardGamesSoundsController.Instance.PlayMissionSFX(SoundsController.MissionSFX.challenge_complete);
                m_progress_text.text = "COMPLETE";
            }


        }

        public void UpdateProgresBar(float value)
        {
            m_progressBar_image.fillAmount = value;

        }

        private IEnumerator RunTimer(TimeSpan ts, TMP_Text timer, string timer_text_prefix)
        {
            float startTime = Time.realtimeSinceStartup;

            timer_text_prefix += "<color=#FF4855>";

            while (ts.TotalSeconds >= 0)
            {
                yield return new WaitForSecondsRealtime(1f);
                timer.text = timer_text_prefix + DateUtils.TimespanToDateMission(ts);
             //   timer.text = string.Format("{0}{1:00}:{2:00}:{3:00}", timer_text_prefix, ts.Hours, ts.Minutes, ts.Seconds);

                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }

            SetTimer("");
            //timer.text = "";
        }

        IEnumerator AlternatePrize_Timer(bool prize)
        {

            RectTransform prize_rect = m_prize_group.GetComponent<RectTransform>();
            RectTransform timer_rect = m_timer_group.GetComponent<RectTransform>();
            RectTransform info_rect = m_info_group.GetComponent<RectTransform>();

            m_timer_canvas.alpha = 0f;
            m_prize_canvas.alpha = 0f;
            m_info_canvas.alpha = 0f;

            m_timer_group.SetActive(true);

            if (prize)
            {
                m_prize_group.SetActive(true);
                m_prize_canvas.alpha = 1f;
                prize_rect.localPosition = new Vector3(-50f, 0, 0);
            }

            else
            {
                m_info_group.SetActive(true);
                m_info_canvas.alpha = 1f;
                info_rect.localPosition = new Vector3(-50f, 0, 0);
            }

            timer_rect.localPosition = new Vector3(0, 0, 0);

            bool group_active = true;

            while (group_active)
            {
                yield return new WaitForSeconds(ALTERNTATE_INTERVAL);

                timer_rect.localPosition = new Vector3(0, 0, 0);
                if (prize)
                {
                    iTween.MoveTo(m_prize_group, iTween.Hash("x", -100f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                    iTween.ValueTo(m_prize_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdatePrizeAlpha", "Onupdatetarget", this.gameObject));
                }
                else
                {
                    iTween.MoveTo(m_info_group, iTween.Hash("x", -100f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                    iTween.ValueTo(m_info_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateInfoAlpha", "Onupdatetarget", this.gameObject));
                }

                iTween.MoveTo(m_timer_group, iTween.Hash("x", -50f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(m_timer_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateTimerAlpha", "Onupdatetarget", this.gameObject));

                yield return new WaitForSeconds(ALTERNTATE_SWITCH + ALTERNTATE_INTERVAL);

                if (prize)
                {
                    prize_rect.localPosition = new Vector3(0, 0, 0);
                    iTween.MoveTo(m_prize_group, iTween.Hash("x", -50f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                    iTween.ValueTo(m_prize_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdatePrizeAlpha", "Onupdatetarget", this.gameObject));
                }
                else
                {
                    info_rect.localPosition = new Vector3(0, 0, 0);
                    iTween.MoveTo(m_info_group, iTween.Hash("x", -50f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                    iTween.ValueTo(m_info_group, iTween.Hash("from", 0, "to", 1, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateInfoAlpha", "Onupdatetarget", this.gameObject));
                }


                iTween.MoveTo(m_timer_group, iTween.Hash("x", -100f, "islocal", true, "easeType", iTween.EaseType.easeOutCirc, "time", ALTERNTATE_SWITCH, "Onupdatetarget", this.gameObject));
                iTween.ValueTo(m_timer_group, iTween.Hash("from", 1, "to", 0, "time", ALTERNTATE_SWITCH, "onupdate", "UpdateTimerAlpha", "Onupdatetarget", this.gameObject));

                yield return new WaitForSeconds(ALTERNTATE_SWITCH);
            }

        }



        private string GetTitle(SpadesChallenge challenge)
        {
            if (challenge.Bet_operator == MissionController.BetOperator.Exact)
                return "PLAY AT";
            else if (challenge.Bet_operator == MissionController.BetOperator.Min)
                return "MIN BET";
            else if (challenge.Bet_operator == MissionController.BetOperator.Max)
                return "MAX BET";
            else
                return "";
        }



        public void UpdatePrizeAlpha(float value)
        {
            m_prize_canvas.alpha = value;
        }

        public void UpdateTimerAlpha(float value)
        {
            m_timer_canvas.alpha = value;
        }

        public void UpdateInfoAlpha(float value)
        {
            m_info_canvas.alpha = value;
        }

        public void Button_ShowMissionClicked()
        {
            OpenPanel(!m_challenge_panel_showing, null);
        }


        public GameObject Burst_effect_holder
        {
            get
            {
                return m_burst_effect_holder;
            }

            set
            {
                m_burst_effect_holder = value;
            }
        }



    }
}


