using common.models;
using System.Collections.Generic;
using common.utils;
using System;
using System.Linq;
using cardGames.models;
using common.controllers.purchase;
using cardGames.controllers;
using common.ims;
using common.ims.model;

namespace spades.views
{
    public class EndGameBoostOfferButton : MonoBehaviour
    {
        [SerializeField] TMP_Text m_cost_text = null;
        [SerializeField] TMP_Text m_quantity_text = null;
        [SerializeField] TMP_Text m_more_text = null;
        [SerializeField] Button m_buyButton = null;

        CashierItem m_cashier_item = null;
        private Action m_OnPurchaseComplete;

        public void Init(int short_amount, Action OnPurchaseComplete)
        {
            m_OnPurchaseComplete = OnPurchaseComplete;
            bool found_item = false;
            m_cashier_item = null;
            Cashier cashier = ModelManager.Instance.GetCashier();

            ICollection<CashierItem> items_of_group = cashier.Cashier_packages;

            foreach (CashierItem item in items_of_group)
            {
                if (!found_item)
                {
                    if (item.GoodsList.CoinsValue > short_amount)
                    {
                        found_item = true;
                        m_cashier_item = item;
                    }
                }
            }

            if (m_cashier_item == null)
            {
                // Show the biggest offer available
                m_cashier_item = items_of_group.Last();
            }


            // m_missing_amount_text.text = "You need at least <color=yellow><sprite=1>" + FormatUtils.FormatBalance(short_amount) + "</color> more to play.";
            m_quantity_text.text = "GET <sprite=1> " + FormatUtils.FormatBalance(m_cashier_item.GoodsList.CoinsValue);
            m_cost_text.text = "$" + m_cashier_item.Price;

            if (m_cashier_item.Bonus == 0)
            {
                m_more_text.text = "";
            }
            else
            {
                m_more_text.text = "GET " + m_cashier_item.Bonus + "% MORE COINS";
            }

            m_buyButton.onClick.RemoveAllListeners();
            m_buyButton.onClick.AddListener(() =>
            {
                m_buyButton.enabled = false;

                // Create the tracking data
                PurchaseTrackingData purchaseTrackingData = new PurchaseTrackingData()
                {
                    imsMetaData = ModelManager.Instance.GetCashier().ImsMetadata,
                    imsActionValue = m_cashier_item.ImsActionValue,
                    clientMetaData = IMSController.CLIENT_EVENT_NEED_BOOST
                };

                CashierController.Instance.BuyPackage(m_cashier_item.Id, purchaseTrackingData);
            });
        }

        private void OnEnable()
        {
            CashierController.Instance.PurchaseCompleted += HandlePurchaseComplete;
            CashierController.Instance.PurchaseCanceled += HandlePurchaseCanceled;
        }

        private void OnDisable()
        {
            CashierController.Instance.PurchaseCompleted -= HandlePurchaseComplete;
            CashierController.Instance.PurchaseCanceled -= HandlePurchaseCanceled;
        }


        private void HandlePurchaseComplete(string packageId, IMSGoodsList goodsList)
        {
            m_buyButton.enabled = true;
            m_OnPurchaseComplete();
        }

        private void HandlePurchaseCanceled(string packageId)
        {
            m_buyButton.enabled = true;
        }

    }
}
