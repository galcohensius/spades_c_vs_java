﻿using cardGames.comm;
using cardGames.models;
using spades.models;


namespace spades.comm
{
    /// <summary>
    /// actually no really hard-coupled with Spades. CardGamesBonusModelParser implements this without any dependancy on Spades. Use that one instead.
    /// Use this only if extending the CardGames version for Spades specific stuff. Only difference here is using SpadesWorld and not World.
    /// </summary>
    public class SpadesBonusModelParser : CardGamesBonusModelParser
    {
        protected override User CreateUser()
        {
            return new SpadesUser();
        }
    }
}