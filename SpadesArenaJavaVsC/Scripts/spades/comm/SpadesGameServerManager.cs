﻿using System;
using common.comm;
using common.commands;
using spades.commands;

namespace spades.comm
{
	public class SpadesGameServerManager : MonoBehaviour
	{
        protected CommandsManager m_commands_manager;

        SendDataOfflineHandler offlineHandler;

        internal void Connect(Action<bool> onConnected)
        {
            
        }

        public static new SpadesGameServerManager Instance;

		protected void Awake()
        {
            Instance = this;
        }


		protected void Start ()
		{
            m_commands_manager = new CommandsManager();
            RegisterCommands();

            offlineHandler = new SendDataOfflineHandler (m_commands_manager);

		}

    
      

        protected void RegisterCommands ()
		{
         

			Command send_data_command = new SendDataCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.SEND_DATA_CODE, send_data_command);

			Command game_craeted_command = new GameCreatedCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.GAME_CREADTED_CODE, game_craeted_command);

			Command player_status_command = new PlayerStatusCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.PLAYER_STATUS_CODE, player_status_command);

			Command quit_command = new QuitCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.QUIT_CODE, quit_command);

			Command reconnect_command = new ReconnectCommand ();
			m_commands_manager.RegisterCommand (SpadesCommandsBuilder.RECONNECT_CODE, reconnect_command);

		}

		


		public void Send (MMessage message)
		{

			offlineHandler.HandleCommand (message);
			
		}


		public void GoOffline ()
		{
		}

        public void Disconnect()
        {

        }
    }

}

