using cardGames.models;
using common.comm;
using common.models;
using System.Collections.Generic;
using spades.controllers;
using spades.models;
using cardGames.comm;
using cardGames.controllers;
using cardGames.models.contests;
using common.controllers;

namespace spades.comm
{
    /// <summary>
    /// Comm manager handles all commands that are sent to the user server.
    /// </summary>
    public class SpadesCommManager : CardGamesCommManager
    {
        public delegate void EndMatchSingleCompleted(bool success, int coinsPayout, int coinsForPointsPayout, int xpPayout, int newCoinsBalance, int newDiamondsBalance,
            int newXpBalance, LevelData levelData, int newLeaderboardId, float minBotFactor, float maxBotFactor, SpadesChallenge missionUpdate, Cashier cashier,
            Contest contest);

        //if bots data can be abstracted this can move to cardGames
        public delegate void StartMatchCompleted(bool success, int matchTransId, int newCoinsBalance, BotsData botsData, bool show_missions, Contest contest, string voucher_id);

        protected StartMatchCompleted m_start_match_completed;


        EndMatchSingleCompleted m_end_match_single_completed;

        private SpadesModelParser modelParser;
        private SpadesBonusModelParser bonusModelParser;
        public new static SpadesCommManager Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;
            }
        }

        protected override void Start()
        {
            base.Start();
            modelParser = new SpadesModelParser();
            bonusModelParser = new SpadesBonusModelParser();
        }

        //and then use casting since we created the real type
        public override ModelParser GetModelParser()
        {
            if (modelParser == null)
                modelParser = new SpadesModelParser();

            return modelParser;
        }

        public SpadesModelParser GetSpadesModelParser()
        {
            return (SpadesModelParser)GetModelParser();
        }

        protected override CardGamesBonusModelParser GetBonusModelParser()
        {
            if (bonusModelParser == null)
                bonusModelParser = new SpadesBonusModelParser();

            return bonusModelParser;
        }



        protected override void HandleAchievements(JSONNode responseData)
        {
            JSONNode user_achievement_data = responseData["uAs"];
            AchievementsManager.Instance.DecodeUserAchievements(user_achievement_data);
        }

        /// <summary>
        /// Command for ending round (Spades only)
        /// </summary>
        public override void EndRound(string tableId, RoundData rData, EndRoundCompleted onComplete)
        {
            m_end_round_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["tId"] = rData.GameTransId;
            requestData["rNm"] = rData.RoundNum;
            requestData["gmId"] = rData.GameId;
            requestData["gTId"] = tableId;

            JSONArray userArr = new JSONArray();
            for (int pos = 0; pos < 4; pos++)
            {
                SpadesPlayerRoundData prData = ((SpadesRoundData)rData)[pos];

                JSONNode playerDataJSON = new JSONObject();
                playerDataJSON["uky"].AsInt = prData.UserId;
                playerDataJSON["uPn"].AsInt = pos;
                playerDataJSON["bid"].AsInt = prData.Bid;
                playerDataJSON["tas"].AsInt = prData.Takes;
                playerDataJSON["rBs"].AsInt = prData.Bags;
                playerDataJSON["rPs"].AsInt = prData.Points;
                playerDataJSON["iBd"].AsFloat = prData.InitialBid;
                playerDataJSON["bFt"].AsFloat = prData.NilValue;
                playerDataJSON["fRe"].AsInt = prData.NumericBid;
                playerDataJSON["nVn"].AsInt = prData.BiddingPosition;
                playerDataJSON["bPy"].AsInt = prData.BagsPenalty;
                playerDataJSON["nBs"].AsInt = prData.NilBonus;
                playerDataJSON["bSLe"].AsInt = prData.BotLevel;
                playerDataJSON["cSh"].AsInt = prData.CardsSwitch;
                playerDataJSON["fSLh"].AsInt = 0;
                playerDataJSON["pRBs"].AsInt = prData.PartnersBid;
                playerDataJSON["pRTs"].AsInt = prData.PartnersTakes;
                playerDataJSON["pRg"].AsInt = prData.PartnersBags;
                playerDataJSON["eRPe"].AsInt = prData.WinPlace;
                playerDataJSON["had"] = prData.Hand;
                playerDataJSON["sty"] = prData.Strategies;

                if (prData.UserId > 0)
                {
                    playerDataJSON["bTe"] = "H";
                }
                else
                {
                    switch (prData.BotType)
                    {
                        case SpadesAI.BrainType.ClassicV1:
                            playerDataJSON["bTe"] = "A";
                            break;
                        case SpadesAI.BrainType.ClassicV2:
                            playerDataJSON["bTe"] = "S";
                            break;
                        case SpadesAI.BrainType.ClassicV3:
                            playerDataJSON["bTe"] = "P";
                            break;
                        case SpadesAI.BrainType.ClassicV4Peeking:
                            playerDataJSON["bTe"] = "K";
                            break;
                        case SpadesAI.BrainType.ClassicV4:
                            playerDataJSON["bTe"] = "G";
                            break;

                        case SpadesAI.BrainType.GGP:
                            playerDataJSON["bTe"] = "U";
                            break;
                        case SpadesAI.BrainType.Jokers:
                            playerDataJSON["bTe"] = "J";
                            break;
                        case SpadesAI.BrainType.Suicide:
                            playerDataJSON["bTe"] = "C";
                            break;
                        case SpadesAI.BrainType.Whiz:
                            playerDataJSON["bTe"] = "W";
                            break;
                        case SpadesAI.BrainType.Mirror:
                            playerDataJSON["bTe"] = "M";
                            break;
                        case SpadesAI.BrainType.CallBreak:
                            playerDataJSON["bTe"] = "I";
                            break;

                        // AI2:
                        case SpadesAI.BrainType.Jokers2020:
                            playerDataJSON["bTe"] = "Z";
                            break;
                        case SpadesAI.BrainType.V3_LessObvious:
                            playerDataJSON["bTe"] = "O";
                            break;
                        case SpadesAI.BrainType.RoyaleBot: 
                            playerDataJSON["bTe"] = "R";
                            break;
                    }
                }
                userArr.Add(playerDataJSON);
            }

            requestData["uRIo"] = userArr;



            m_client.SendRequest("endRound", requestData, OnEndRoundResponse);
        }


        private void OnEndRoundResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                SpadesChallenge challenge_update = null;

                if (responseData["che"] != null)
                    challenge_update = modelParser.ParseMissionUpdate(responseData["che"]) as SpadesChallenge;


                // contest data will arrive here as well
                // should be:
                JSONNode miniLeaderBoardJSON;
#if UNITY_EDITOR
                if (m_contestsFakeData)
                {
                    // but for fake reasons:
                    miniLeaderBoardJSON = JSON.Parse(ContestsController.Instance.Mini_leaderboard_contests_data.text);
                }
                else
                {
                    miniLeaderBoardJSON = responseData["mLBs"][0];
                }
#else
                miniLeaderBoardJSON = responseData["mLBs"][0];
#endif
                Contest contest = null;
                if (miniLeaderBoardJSON != null)
                {
                    contest = ModelManager.Instance.ContestsLists.GetContestByID(miniLeaderBoardJSON["ctId"]);
                    if (contest != null)
                    {
                        contest.Contest_players_mini_leaderboard_rank = GetSpadesModelParser().ParseContestPlayers(miniLeaderBoardJSON, "pLs");
                        contest.Contest_players_mini_leaderboard_top3 = GetSpadesModelParser().ParseContestPlayers(miniLeaderBoardJSON, "tOp3");
                    }
                    Debug.Log("Response OnEndRoundResponse contest data: " + miniLeaderBoardJSON.ToString());

                }
                // add a contest parameter, if no contest, return null in the callback

                m_end_round_completed(challenge_update, contest);
            }
            else
            {
                LoggerController.Instance.LogError("End round error received:\n" + responseData.ToString());
            }
        }


        /// <summary>
        /// Command for ending the single match. spades only for now, will be probably refactored later for Gin
        /// </summary>
        public void EndSingleMatch(string tableId, int matchTransId, int numOfRounds,
                                    int points, int opponentsPointsForPartners, bool isQuit, int winPlace, int num_winners,
                                    SpadesUserStats userStats, string match_history, JSONObject user_achievements, EndMatchSingleCompleted onComplete)
        {
            m_end_match_single_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["gTId"] = tableId;
            requestData["tId"].AsInt = matchTransId;
            requestData["gNRs"].AsInt = numOfRounds;
            requestData["pts"].AsInt = points;
            requestData["oPs"].AsInt = opponentsPointsForPartners;
            requestData["ple"].AsInt = winPlace;
            requestData["nWs"].AsInt = num_winners;
            requestData["eGe"] = isQuit ? "Q" : "R";
            requestData["uSs"] = UserStatsController.Instance.SerializeUserStats(userStats);

            if (match_history != null)
                requestData["gHD"] = match_history;

            requestData["uAs"] = user_achievements;

            m_client.SendRequest("endSingleMatch", requestData, OnEndSingleMatchResponse, true);
        }

        private void OnEndSingleMatchResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                int coinsPayout = responseData["cGs"].AsInt;
                int xpPayout = responseData["xPt"].AsInt;
                int newCoinsBalance = responseData["cBe"].AsInt;
                int newXpBalance = responseData["xp"].AsInt;
                int leaderboardId = -1;
                int coinsForPointsPayout = 0;

                if (responseData["c4PPt"] != null)
                    coinsForPointsPayout = responseData["c4PPt"].AsInt;

                //default values
                float minBotFactor = 0.5f;
                float maxBotFactor = 1.5f;

                JSONNode leaderboardData = responseData["led"];

                if (leaderboardData != null && leaderboardData["lId"] != null)
                    leaderboardId = leaderboardData["lId"].AsInt;

                if (leaderboardData != null && leaderboardData["bLFr"] != null)
                    minBotFactor = leaderboardData["bLFr"].AsFloat;

                if (leaderboardData != null && leaderboardData["bUFr"] != null)
                    maxBotFactor = leaderboardData["bUFr"].AsFloat;

                JSONNode levelUpJson = responseData["lUp"];
                LevelData levelData = null;
                if (levelUpJson != null)
                {
                    levelData = ParseLevelData(levelUpJson);
                }

                // Cashier data - IMS (have higher priority)
                Cashier cashier = SpadesCommManager.Instance.GetSpadesModelParser().ParseCashier(responseData["ims"]["bPe"]);

                // Cashier data - MES
                if (cashier == null)
                    cashier = SpadesCommManager.Instance.GetSpadesModelParser().ParseCashier(responseData["pSr"]);

                SpadesChallenge mission_update = null;

                if (responseData["che"] != null)
                    mission_update = modelParser.ParseMissionUpdate(responseData["che"]) as SpadesChallenge;

                JSONNode mesEvents = responseData["evs"];
                LoggerController.Instance.Log("MES JSON END GAME: " + mesEvents.ToString());
                (SpadesMESController.Instance as SpadesMESController).ParseMESJson(mesEvents);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log("IMS JSON END GAME: " + imsEvents.ToString());
                SpadesIMSController.Instance.ParseEvents(imsEvents);

                // contest data will arrive here as well
                JSONNode miniLeaderBoardJSON;
#if UNITY_EDITOR
                if (m_contestsFakeData)
                {
                    // for fake reasons:
                    miniLeaderBoardJSON = JSON.Parse(ContestsController.Instance.Mini_leaderboard_contests_data_end.text);
                }
                else
                {
                    miniLeaderBoardJSON = responseData["mLBs"][0];
                }
#else
                miniLeaderBoardJSON = responseData["mLBs"][0];
#endif
                Contest contest = null;
                if (miniLeaderBoardJSON != null)
                {
                    contest = ModelManager.Instance.ContestsLists.GetContestByID(miniLeaderBoardJSON["ctId"]);
                    if (contest != null)
                    {
                        contest.Contest_players_mini_leaderboard_rank = SpadesCommManager.Instance.GetSpadesModelParser().ParseContestPlayers(miniLeaderBoardJSON, "pLs");
                        contest.Contest_players_mini_leaderboard_top3 = SpadesCommManager.Instance.GetSpadesModelParser().ParseContestPlayers(miniLeaderBoardJSON, "tOp3");
                    }
                    LoggerController.Instance.Log("Response endSingleMatch contest data: " + miniLeaderBoardJSON.ToString());
                }
                m_end_match_single_completed(true, coinsPayout, coinsForPointsPayout, xpPayout, newCoinsBalance, 0, newXpBalance, levelData, leaderboardId, minBotFactor, maxBotFactor, mission_update, cashier, contest);
            }
            else
            {

                m_end_match_single_completed(false, 0, 0, 0, 0, 0, 0, null, 0, 0, 0, null, null, null);

            }
        }




        /// <summary>
        /// Command for ending an Arena stage.
        /// </summary>
        public void EndArenaStage(string arenaId, int gameServerMatchId, int matchTransId, int numOfRounds,
                                   int points, bool isQuit, int winPlace, int num_winners, SpadesUserStats userStats, string match_history, JSONObject user_achievements, EndMatchArenaCompleted onComplete)
        {

            m_end_match_arena_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["aId"] = arenaId;
            requestData["gmId"].AsInt = gameServerMatchId;
            requestData["tId"].AsInt = matchTransId;
            requestData["gNRs"].AsInt = numOfRounds;
            requestData["pts"].AsInt = points;
            requestData["eGe"] = isQuit ? "Q" : "R";
            requestData["ple"].AsInt = winPlace;
            requestData["nWs"].AsInt = num_winners;
            requestData["uSs"] = UserStatsController.Instance.SerializeUserStats(userStats);

            if (match_history != null)
                requestData["gHD"] = match_history;

            requestData["uAs"] = user_achievements;

            m_client.SendRequest("endArenaStage", requestData, OnEndArenaStageResponse, true);
        }

        private void OnEndArenaStageResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                int newCoinsBalance = responseData["cBe"].AsInt;
                int coinsPayout = responseData["cGs"].AsInt;
                int xpPayout = responseData["xPt"].AsInt;
                int newXpBalance = responseData["xp"].AsInt;
                int leaderboardId = -1;


                //defaut values
                float minBotFactor = 0.5f;
                float maxBotFactor = 1.5f;

                JSONNode leaderboardData = responseData["led"];

                if (leaderboardData != null && leaderboardData["lId"] != null)
                    leaderboardId = leaderboardData["lId"].AsInt;

                if (leaderboardData != null && leaderboardData["bLFr"] != null)
                    minBotFactor = leaderboardData["bLFr"].AsFloat;

                if (leaderboardData != null && leaderboardData["bUFr"] != null)
                    maxBotFactor = leaderboardData["bUFr"].AsFloat;


                JSONNode trophyNode = responseData["uTs"];

                int currStep = -1; // -1 indicates that this field was not updated
                int totalGemsWon = 0;

                if (trophyNode != null)
                {
                    currStep = trophyNode["cSp"].AsInt;
                    totalGemsWon = trophyNode["tCAd"].AsInt;
                }


                JSONNode levelUpJson = responseData["lUp"];
                LevelData levelData = null;
                if (levelUpJson != null)
                {
                    levelData = ParseLevelData(levelUpJson);
                }

                int nextArenaStage = responseData["aSId"].AsInt;

                JSONNode mesEvents = responseData["evs"];
                LoggerController.Instance.Log("MES JSON END ARENA: " + mesEvents.ToString());
                (SpadesMESController.Instance as SpadesMESController).ParseMESJson(mesEvents);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log("IMS JSON END ARENA: " + imsEvents.ToString());
                SpadesIMSController.Instance.ParseEvents(imsEvents);


                // Cashier data - IMS (have higher priority)
                Cashier cashier = SpadesCommManager.Instance.GetSpadesModelParser().ParseCashier(responseData["ims"]["bPe"]);

                // Cashier data - MES
                if (cashier == null)
                    cashier = SpadesCommManager.Instance.GetSpadesModelParser().ParseCashier(responseData["pSr"]);


                m_end_match_arena_completed(true, coinsPayout, xpPayout, newCoinsBalance, 0, newXpBalance, levelData, nextArenaStage, currStep, totalGemsWon, leaderboardId, minBotFactor, maxBotFactor, cashier);

            }
            else
            {
                m_end_match_arena_completed(false, 0, 0, 0, 0, 0, null, 0, 0, 0, 0, 0, 0, null);
            }
        }


        private LevelData ParseLevelData(JSONNode levelData)
        {
            LevelData result = new LevelData();
            result.Level = levelData["lel"].AsInt;
            result.XpThreshold = levelData["xTd"].AsInt;

            // Level bonus awarded
            result.LevelBonus = BonusModelParser.ParseBonusAwarded(levelData["bAd"]);

            // New bonus data (might change after level up)
            List<Bonus> bonuses = new List<Bonus>();
            JSONArray bonusesData = levelData["bDa"].AsArray;
            foreach (JSONNode bonusData in bonusesData)
            {
                Bonus bonus = bonusModelParser.ParseBonus(bonusData, null);
                if (bonus != null)
                    bonuses.Add(bonus);
            }
            result.NewBonuses = bonuses;

            if (result.Level > ModelManager.Instance.GetUser().GetLevel())
                ContestsController.Instance.HandleLevelUp();

            return result;
        }

        protected override UserStats CreateUserStats()
        {
            return new SpadesUserStats();
        }

        protected override void HandleEmptyUserStats(UserStats user_stats)
        {
            ((SpadesUserStats)user_stats).ExactBids = 0;
            ((SpadesUserStats)user_stats).GreaterBids = 0;
            ((SpadesUserStats)user_stats).LesserBids = 0;
            ((SpadesUserStats)user_stats).MatchLosesSolo = 0;
            ((SpadesUserStats)user_stats).MatchWinsSolo = 0;
            ((SpadesUserStats)user_stats).NilBids = 0;
            ((SpadesUserStats)user_stats).NilTakes = 0;
            ((SpadesUserStats)user_stats).MatchWinsPartner = 0;
            ((SpadesUserStats)user_stats).MatchLosesPartner = 0;
        }

        /*
        protected override void OnSendLoggingResponse(bool success)
        {


            if (success)
                LoggerController.Instance.Log("LogDataSent");

            if(m_get_send_data_completed!=null)
                m_get_send_data_completed(success);
        }*/


        protected override void OnGetTableResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                SpadesSingleMatch singleMatch = (GetModelParser() as SpadesModelParser).ParseSingleMatch(responseData, null);
                m_get_table_data_completed(success, singleMatch);
            }
            else
            {
                m_get_table_data_completed(success, null);
            }
        }




        /// <summary>
        /// Command for starting a new single match.
        /// </summary>
        public void StartSingleMatch(string tableId, string gameId, bool useVoucher, StartMatchCompleted onComplete)
        {
            m_start_match_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["gTId"] = tableId;
            requestData["gmId"] = gameId;


            requestData["uVr"] = useVoucher ? 1 : 0;


            m_client.SendRequest("startSingleMatch", requestData, OnStartSingleMatchResponse);
        }


        private void OnStartSingleMatchResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                int matchTransId = responseData["tId"].AsInt;
                int newCoinsBalance = responseData["cBe"].AsInt;

                // contest data will arrive here as well
                JSONNode miniLeaderBoardJSON;
#if UNITY_EDITOR
                if (m_contestsFakeData)
                {
                    // for fake reasons:
                    miniLeaderBoardJSON = JSON.Parse(ContestsController.Instance.Mini_leaderboard_contests_data.text);
                }
                else
                {
                    miniLeaderBoardJSON = responseData["mLBs"][0];
                }
#else
                miniLeaderBoardJSON = responseData["mLBs"][0];
#endif
                Contest contest = null;
                if (miniLeaderBoardJSON != null)
                {
                    contest = ModelManager.Instance.ContestsLists.GetContestByID(miniLeaderBoardJSON["ctId"]);
                    if (contest != null)
                    {
                        contest.Contest_players_mini_leaderboard_rank = GetSpadesModelParser().ParseContestPlayers(miniLeaderBoardJSON, "pLs");
                        contest.Contest_players_mini_leaderboard_top3 = GetSpadesModelParser().ParseContestPlayers(miniLeaderBoardJSON, "tOp3");
                    }

                    LoggerController.Instance.Log("Response startSingleMatch contest data: " + miniLeaderBoardJSON.ToString());
                }

                BotsData botsData = GetSpadesModelParser().ParseBotsData(responseData["tBs"]);

                bool show_missions = responseData["mCe"].AsInt == 1;
                string voucher_id = responseData["vId"];

                // Cashier data - IMS 
                Cashier cashier = SpadesCommManager.Instance.GetSpadesModelParser().ParseCashier(responseData["ims"]["bPe"]);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log("IMS JSON START GAME: " + imsEvents.ToString());
                SpadesIMSController.Instance.ParseEvents(imsEvents);

                m_start_match_completed(true, matchTransId, newCoinsBalance, botsData, show_missions, contest, voucher_id);
            }
            else
            {
                m_start_match_completed(false, 0, 0, null, false, null, null);
            }
        }



        /// <summary>
        /// Command for starting a new arena stage.
        /// </summary>
        public void StartArenaStage(string arenaId, string gameId, StartMatchCompleted onComplete)
        {
            m_start_match_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["aId"] = arenaId;
            requestData["gmId"] = gameId;

            m_client.SendRequest("startArenaStage", requestData, OnStartArenaStageResponse);
        }

        private void OnStartArenaStageResponse(bool success, JSONNode responseData)
        {
            if (success)
            {
                int matchTransId = responseData["tId"].AsInt;

                BotsData botsData = modelParser.ParseBotsData(responseData["tBs"]);

                m_start_match_completed(true, matchTransId, 0, botsData, false, null, null);
            }
            else
            {
                m_start_match_completed(false, 0, 0, null, false, null, null);
            }
        }

    }




}
