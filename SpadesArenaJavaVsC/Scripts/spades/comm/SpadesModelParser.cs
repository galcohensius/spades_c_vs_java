﻿using System.Collections.Generic;
using spades.models;
using spades.controllers;
using common.models;
using System;
using cardGames.models;
using cardGames.controllers;
using cardGames.comm;
using cardGames.models.contests;

namespace spades.comm
{
    /// <summary>
    /// Utility class for parsing models from JSON as received from the server
    /// </summary>
    public class SpadesModelParser : CardGamesModelParser
    {
        public override User ParseUser(JSONNode jsonNode, List<World> worlds)
        {
            SpadesUser result = new SpadesUser();
            ParseUserCommonData(result, jsonNode, worlds);

            result.Stats = new SpadesUserStats();
            return result;
        }

        public override World ParseWorld(JSONNode jsonNode, JSONNode collectiblesNode, JSONNode trophyNode)
        {
            SpadesWorld result = new SpadesWorld();
            ParseWorldCommonData(result, jsonNode, collectiblesNode, trophyNode);

            // Single matches
            JSONArray singlesNode = jsonNode["sMs"].AsArray;

            if (singlesNode!=null)
            {
                foreach (JSONNode node in singlesNode)
                {
                    result.AddSingleMatch(ParseSingleMatch(node, result));
                }
            }

            // Arenas
            JSONArray arenaJsonArr = jsonNode["ara"].AsArray;
            if (arenaJsonArr!=null)
            {
                foreach (JSONNode arenaJson in arenaJsonArr)
                {
                    result.AddArena(ParseArena(arenaJson, result));
                }
            }


            return result as SpadesWorld;
        }



        protected override Arena CreateArena()
        {
            return new SpadesArena();
        }

        protected override Challenge CreateChallenge(int order_id, int progress, int prize, MissionController.ChallengeStatus status)
        {
            return new SpadesChallenge(order_id, progress, prize, status);
        }

        protected override Mission CreateMission(DateTime dateTime)
        {
            return new SpadesMission(dateTime);
        }

        protected override Challenge CreateChallenge(MissionController.ChallengeStatus status, int[] thresholds, int order, int prize, int num_swapped, int bet, MissionController.BetOperator betOperator, int progress)
        {
            return new SpadesChallenge(SpadesMissionController.ChallengeType.Bid_Num_Spades_In_Hand, status, thresholds, order, prize, num_swapped, bet, betOperator, progress);
        }

        protected override ActiveArena CreateActiveArena(Arena arena)
        {
            return new SpadesActiveArena(arena);
        }

        public SpadesSingleMatch ParseSingleMatch(JSONNode jsonNode, SpadesWorld world)
        {
            SpadesSingleMatch result = new SpadesSingleMatch();

            result.SetID(jsonNode["gTId"]);
            result.Min_level = jsonNode["mLl"].AsInt;
            result.Buy_in = jsonNode["bIn"].AsInt;
            result.SetPayOutCoins(jsonNode["pOCs"].AsInt);

            result.HighPoints = jsonNode["hPs"].AsInt;
            result.LowPoints = jsonNode["lPs"].AsInt;
            result.BidDelay = jsonNode["bDy"].AsFloat;
            result.PlayDelay = (jsonNode["tDy"].AsFloat);
            result.BagsCount = jsonNode["nBs"].AsInt;
            result.BagsPanelty = jsonNode["bPy"].AsInt;
            //result.Seating_mode = jsonNode["sMe"].Value;
            //result.Max_humans = jsonNode["mHs"].AsInt;
            result.Is_special = jsonNode["iSl"].AsInt == 1;

            int unlockLevel = 0;
            string key = "s" + result.GetID();
            ModelManager.unlockLevelMap.TryGetValue(key, out unlockLevel);
            result.SetUnlockLevel(unlockLevel);

            result.Playing_mode = GetPlayingMode(jsonNode["gMe"].Value);
            result.Variant = GetPlayingVariant(jsonNode["gVt"].Value);
            result.SubVariant = GetPlayingSubVariant(jsonNode["sGVt"].Value);

            result.Graphics_path = jsonNode["gPh"];

            if (world != null)
            {
                result.World_id = world.GetID();
                result.World = world;
            }
            else
            {
                result.World_id = jsonNode["wId"];
            }

            result.Coins4PointsFactor = jsonNode["c4Ps"].AsInt;

            // ------------ Comment out after Debug: -------  
//            if (key == "s1")
//            {
//                result.Playing_mode = PlayingMode.Solo;
//                result.Variant = PlayingVariant.Jokers;
//                result.SubVariant = PlayingSubVariant.Regular;
//            }
            return result;
        }

        public override Arena ParseArena(JSONNode jsonNode, World world)
        {
            SpadesArena result = base.ParseArena(jsonNode, world) as SpadesArena;
            string playing_mode_str = jsonNode["gMe"].Value;

            int unlockLevel = 0;
            string key = "a" + result.GetID();
            ModelManager.unlockLevelMap.TryGetValue(key, out unlockLevel);
            result.SetUnlockLevel(unlockLevel);

            if (playing_mode_str == "P")
                result.Playing_mode = PlayingMode.Partners;
            else
                result.Playing_mode = PlayingMode.Solo;

            result.Variant = GetPlayingVariant(jsonNode["gVt"].Value);
            result.SubVariant = GetPlayingSubVariant(jsonNode["sGVt"].Value);

            // Matches
            int totalCoinsPayOut = 0;
            int counter = 0;
            JSONArray matchesNode = jsonNode["aSs"].AsArray;
            foreach (JSONNode matchNode in matchesNode)
            {
                SpadesArenaMatch match = ParseArenaMatch(matchNode, result);

                match.World_id = world.GetID();
                result.Arena_matches.Add(match);
                totalCoinsPayOut += match.GetPayOutCoins();

                match.Stage_num = counter;

                counter++;

            }

            result.SetPayOutCoins(totalCoinsPayOut);

            return result;
        }

        public SpadesArena ParseArena(JSONNode jsonNode, SpadesWorld world)
        {
            SpadesArena result = new SpadesArena();
            result.SetId(jsonNode["aId"]);
            result.SetName(jsonNode["aNe"]);
            result.SetLevel(jsonNode["mLl"].AsInt);

            int mc = jsonNode["bICs"].AsInt;


            result.SetBuyInMulti(mc);

            result.Curr_rebuy_num = jsonNode["uNrs"].AsInt;

            result.World = world;

            string playing_mode_str = jsonNode["gMe"].Value;

            if (playing_mode_str == "P")
                result.Playing_mode = PlayingMode.Partners;
            else
                result.Playing_mode = PlayingMode.Solo;

            result.Variant = GetPlayingVariant(jsonNode["gVt"].Value);
            result.SubVariant = GetPlayingSubVariant(jsonNode["sGVt"].Value);


            // Matches
            int totalCoinsPayOut = 0;
            int counter = 0;
            JSONArray matchesNode = jsonNode["aSs"].AsArray;
            foreach (JSONNode matchNode in matchesNode)
            {
                SpadesArenaMatch match = ParseArenaMatch(matchNode, result);

                match.World_id = world.GetID();
                result.Arena_matches.Add(match);
                totalCoinsPayOut += match.GetPayOutCoins();

                match.Stage_num = counter;
                match.Variant = result.Variant;
                match.SubVariant = result.SubVariant;
                counter++;
            }
            

            result.SetPayOutCoins(totalCoinsPayOut);

            // Arena type
            if (world.GetID() == "5")
                result.ArenaType = SpadesArena.SpadesArenaType.Master;
            else if (world.GetID() == "6")
                result.ArenaType = SpadesArena.SpadesArenaType.Jokers;

            return result;
        }

        public SpadesArenaMatch ParseArenaMatch(JSONNode jsonNode, SpadesArena arena)
        {
            SpadesArenaMatch result = new SpadesArenaMatch();
            result.SetID(jsonNode["aSId"]);
            result.Payout_coins = jsonNode["pOCs"].AsInt;
            result.HighPoints = jsonNode["hPs"].AsInt;
            result.LowPoints = jsonNode["lPs"].AsInt;
            result.BidDelay = (jsonNode["bDy"].AsFloat);
            result.PlayDelay = (jsonNode["tDy"].AsFloat);
            result.BagsCount = (jsonNode["nBs"].AsInt);
            result.BagsPanelty = (jsonNode["bPy"].AsInt);
            result.Payout_coins = jsonNode["pOCs"].AsInt;
            result.Graphics_path = jsonNode["gPh"];
            result.Playing_mode = arena.Playing_mode;
            // Rebuys
            JSONArray rebuysArr = jsonNode["rCs"].AsArray;
            foreach (JSONNode rebuy in rebuysArr)
            {
                result.Rebuys.Add(Convert.ToInt32(rebuy.Value));
            }

            result.SetArena(arena);

            return result;
        }

        protected override TableFilterData CreateTableFilterdata()
        {
            return new SpadesTableFilterData();
        }

        public override Challenge ParseChallengePackage(JSONNode jsonNode)
        {
            SpadesMissionController.ChallengeType ch_type = (SpadesMissionController.ChallengeType)jsonNode["tId"].AsInt;
            PlayingMode playingMode = PlayingMode.Solo;
            HashSet<PlayingSubVariant> subVariant;
            HashSet<PlayingVariant> variant;

            SpadesTableFilterData spadesTableFilterData = new SpadesTableFilterData();

            string game_mode_value = jsonNode["gMe"].Value;

            if (game_mode_value == "B")
            {
                playingMode = PlayingMode.Both;
            }
            else if (game_mode_value == "P")
            {
                playingMode = PlayingMode.Partners;
            }
            else
            {
                playingMode = PlayingMode.Solo;
            }

            spadesTableFilterData.MinBuyIn = jsonNode["bet"].AsInt;
            spadesTableFilterData.MaxBuyIn = jsonNode["bet"].AsInt;
            spadesTableFilterData.PlayingMode = playingMode;

            JSONArray subVariantString = jsonNode["sGVt"].AsArray;
            subVariant = GetPlayingSubVariants(subVariantString);
            spadesTableFilterData.PlayingSubVariants = subVariant;

            JSONArray variantString = jsonNode["gVt"].AsArray;
            variant = GetPlayingVariants(variantString);
            spadesTableFilterData.PlayingVariants = variant;


            return new SpadesChallenge(base.ParseChallengePackage(jsonNode), ch_type, spadesTableFilterData);
        }
        PlayingVariant FormatSubVariant(string value)
        {
            switch (value)
            {
                case "C":
                    return PlayingVariant.Classic;

            }
            return PlayingVariant.Classic;
        }
        public SpadesActiveArena ParseActiveArena(JSONNode jsonNode, SpadesArena arena)
        {
            SpadesActiveArena result = new SpadesActiveArena(arena);

            result.SetBought(true);
            result.SetCurrStage(jsonNode["aSId"].AsInt);

            return result;
        }

        public BotsData ParseBotsData(JSONNode jsonNode)
        {
            BotsData result = new BotsData();

            result.SwitchCardsNum = jsonNode["cSNm"].AsInt;
            result.RoundSwitchCardsRatio = jsonNode["rCSRo"].AsInt;
            if (jsonNode.HasChild("cSTe"))
                result.SwitchType = jsonNode["cSTe"].Value;

            JSONArray botSkillsJson = jsonNode["nUs"].AsArray;
            foreach (JSONNode botSkillJson in botSkillsJson)
            {
                BotsData.BotTypeAndSkill typeAndSkill = new BotsData.BotTypeAndSkill
                {
                    skill = botSkillJson["sLl"].AsInt,
                    type = botSkillJson["bTe"].Value
                };

                result.BotTypes[botSkillJson["pon"].AsInt] = typeAndSkill;
            }

            return result;
        }

        public override TableFilterData ParseTableFilterData(JSONObject json)
        {
            SpadesTableFilterData result = new SpadesTableFilterData();
            result.PlayingMode = GetPlayingMode(json["gMe"]);

            JSONArray voucher_vairant_arr = json["gVt"].AsArray;
            JSONArray voucher_sub_vairant_arr = json["gSVt"].AsArray;

            foreach (JSONNode item in voucher_vairant_arr)
                result.PlayingVariants.Add(GetPlayingVariant(item.Value));

            foreach (JSONNode item in voucher_sub_vairant_arr)
                result.PlayingSubVariants.Add(GetPlayingSubVariant(item.Value));

            return result;

        }

        public override Contest ParseContest(JSONNode node, int index, Contest.ContestStatus contestStatus)
        {
            Contest contest = base.ParseContest(node, index, contestStatus);

            JSONArray subVariant = node["sGVt"].AsArray;

            
            SpadesTableFilterData tableFilterData = contest.TableFilterData as SpadesTableFilterData;
            tableFilterData.PlayingSubVariants = GetPlayingSubVariants(subVariant);

            JSONArray gameVariant = node["gVt"].AsArray;
            tableFilterData.PlayingVariants = GetPlayingVariants(gameVariant);

            JSONNode playingMode = node["gMe"];
            tableFilterData.PlayingMode = GetPlayingMode(playingMode);

            contest.TableFilterData = tableFilterData;
            return contest;
        }

        private HashSet<PlayingVariant> GetPlayingVariants(JSONArray gameVar)
        {
            HashSet<PlayingVariant> result = new HashSet<PlayingVariant>();
            if (gameVar.Count == 0)
            {
                return result;
            }
            foreach (JSONNode item in gameVar)
            {
                result.Add(GetPlayingVariant(item));
            }
            return result;
        }

        private HashSet<PlayingSubVariant> GetPlayingSubVariants(JSONArray gameSubVar)
        {
            HashSet<PlayingSubVariant> result = new HashSet<PlayingSubVariant>();
            if (gameSubVar.Count == 0)
            {
                return result;
            }
            foreach (JSONNode item in gameSubVar)
            {
                result.Add(GetPlayingSubVariant(item));
            }
            return result;
        }

        private PlayingVariant GetPlayingVariant(string gameVar)
        {
            switch (gameVar)
            {
                case "J":
                    return PlayingVariant.Jokers;
                case "M":
                    return PlayingVariant.Mirror;
                case "W":
                    return PlayingVariant.Whiz;
                case "S":
                    return PlayingVariant.Suicide;
                case "R":
                    return PlayingVariant.Royale;
                case "I":
                    return PlayingVariant.CallBreak;
                case "C":
                default:
                    return PlayingVariant.Classic;
            }
        }

        private PlayingSubVariant GetPlayingSubVariant(string subVar)
        {
            switch (subVar)
            {
                case "F":
                    return PlayingSubVariant.FastLane;
                case "S":
                    return PlayingSubVariant.SingleRound;
                case "B":
                    return PlayingSubVariant.GoBig;
                case "J":
                    return PlayingSubVariant.Jackpot;
                case "G":
                    return PlayingSubVariant.NoBags;
                case "N":
                    return PlayingSubVariant.NoNil;
                case "L":
                    return PlayingSubVariant.BlindNil;
                case "T":
                    return PlayingSubVariant.Turbo;
                case "P":
                    return PlayingSubVariant.Coins4Points;
                case "H":
                    return PlayingSubVariant.HiLo;
                case "R":
                default:
                    return PlayingSubVariant.Regular;

            }
        }

        private PlayingMode GetPlayingMode(string mode)
        {
            switch (mode)
            {
                case "S":
                    return PlayingMode.Solo;
                case "P":
                    return PlayingMode.Partners;
                default:
                    return PlayingMode.Both;
            }
        }

        public static string VariantEnumToString(PlayingVariant variant)
        {
            switch (variant)
            {
                case PlayingVariant.Classic:
                    return "Classic";
                case PlayingVariant.CallBreak:
                    return "CallBreak";
                case PlayingVariant.Jokers:
                    return "Jokers";
                case PlayingVariant.Mirror:
                    return "Mirror";
                case PlayingVariant.Royale:
                    return "Royale";
                case PlayingVariant.Suicide:
                    return "Suicide";
                case PlayingVariant.Whiz:
                    return "Whiz";
                default:
                    return "Classic";
            }
        }

        public static string SubVariantEnumToString(PlayingSubVariant subVariant)
        {
            switch (subVariant)
            {
                case PlayingSubVariant.Regular:
                    return "Regular";
                case PlayingSubVariant.BlindNil:
                    return "Blind Nil";
                case PlayingSubVariant.FastLane:
                    return "Fast Lane";
                case PlayingSubVariant.GoBig:
                    return "Go Big";
                case PlayingSubVariant.Jackpot:
                    return "Jackpot";
                case PlayingSubVariant.NoBags:
                    return "No Bags";
                case PlayingSubVariant.NoNil:
                    return "No Nil";
                case PlayingSubVariant.SingleRound:
                    return "Single Round";
                case PlayingSubVariant.Turbo:
                    return "Turbo";
                case PlayingSubVariant.Coins4Points:
                    return "Coins 4 Points";
                case PlayingSubVariant.HiLo:
                    return "Hi-Lo";
                default:
                    return "Regular";
            }
        }

        public static string HandleSubVariants(TableFilterData tableFilterData)
        {
            string result = "";
            int numOfSubVariants = 0;
            SpadesTableFilterData stfd = tableFilterData as SpadesTableFilterData;
            foreach (PlayingSubVariant item in stfd.PlayingSubVariants)
            {
                if (item != PlayingSubVariant.Regular)
                    numOfSubVariants++;
            }

            int counter = 0;
            foreach (PlayingSubVariant item in stfd.PlayingSubVariants)
            {
                string name = SpadesModelParser.SubVariantEnumToString(item);
                // "Regular" shouldn't be displayed to the user
                if (item == PlayingSubVariant.Regular)
                    continue;

                if (counter == 0)
                    result += name;
                else if (counter == numOfSubVariants - 1)
                    result += " and " + name + "";
                else
                    result += ", " + name;

                counter++;
            }

            return result;
        }

        public static string HandleVariants(TableFilterData tableFilterData, string subVariat)
        {
            string result = "";
            int numOfVariants = 0;
            bool containsClassic = false;

            SpadesTableFilterData stfd = tableFilterData as SpadesTableFilterData;
            foreach (PlayingVariant item in stfd.PlayingVariants)
            {
                if (item == PlayingVariant.Classic)
                    containsClassic = true;

                numOfVariants++;
            }

            int counter = 0;
            // if classic exists, show it first
            if (containsClassic)
            {
                if (numOfVariants == 1)
                    result += PlayingVariant.Classic;
                else
                    result += PlayingVariant.Classic + ", ";
            }

            foreach (PlayingVariant item in stfd.PlayingVariants)
            {
                if (item == PlayingVariant.Classic)
                {
                    counter++;
                    continue;
                }
                string name = SpadesModelParser.VariantEnumToString(item);

                if (numOfVariants > 1)
                {
                    if (counter == numOfVariants - 1 && subVariat == string.Empty)
                        result += " and " + name + "";
                    else if (counter == 0)
                        result += name;
                    else
                        result += ", " + name;
                }
                else
                {
                    result += name;
                }

                counter++;
            }

            return result;
        }
    }
}
