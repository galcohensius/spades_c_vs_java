﻿using common.comm;
using spades.commands;
using common.commands;

namespace spades.comm
{
	public class SendDataOfflineHandler
	{
		CommandsManager commandsManager;

		public SendDataOfflineHandler (CommandsManager commandsManager)
		{
			this.commandsManager = commandsManager;
		}

		public void HandleCommand (MMessage msg)
		{
			switch (msg.Code) {
			case SpadesCommandsBuilder.SEND_DATA_CODE:
				HandleSendDataCommand (msg);
				break;
			case SpadesCommandsBuilder.QUIT_CODE:
				HandleQuitCommand (msg);
				break;
			}

		}

		private void HandleSendDataCommand (MMessage msg)
		{
            //LoggerController.Instance.Log("Offline data received: " + msg);

			// Get the data code
			string gameId = msg.Params [0].Value;
			string dataCode = msg.Params [1].Value;
			string data = msg.Params [2].Value;

			MMessage offlineResp = new MMessage ();
			offlineResp.Code = SpadesCommandsBuilder.SEND_DATA_CODE;
			offlineResp.Params.Add (new SimpleParam (gameId));
			offlineResp.Params.Add (new SimpleParam (0));
			offlineResp.Params.Add (new SimpleParam (dataCode));
			offlineResp.Params.Add (new SimpleParam (data));

			commandsManager.ExecuteCommand (offlineResp);
			
		}

		private void HandleQuitCommand (MMessage msg)
		{

			string gameId = msg.Params [0].Value;

			MMessage offlineResp = new MMessage ();
			offlineResp.Code = SpadesCommandsBuilder.QUIT_CODE;
			offlineResp.Params.Add (new SimpleParam (gameId));
			offlineResp.Params.Add (new SimpleParam (0));

			commandsManager.ExecuteCommand (offlineResp);
		}
	}
}

