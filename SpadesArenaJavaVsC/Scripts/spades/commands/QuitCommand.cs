﻿using common.comm;
using common.commands;
using common.controllers;
using spades.controllers;

namespace spades.commands
{

    public class QuitCommand : Command
    {

        public override void Execute(MMessage message)
		{
			LoggerController.Instance.Log ("QUIT Commnad answer back");
			RoundController.Instance.LeaveTable (false);
		}

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;
    }
}