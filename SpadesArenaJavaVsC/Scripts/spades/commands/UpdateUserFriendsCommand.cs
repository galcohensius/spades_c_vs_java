﻿using System;
using common.commands;
using common.comm;
using spades.controllers;
using common.controllers;

namespace spades.commands
{
	public class UpdateUserFriendsCommand: Command
	{
		private const int STATUS_IDX = 0;
		private const string STATUS_OK = "0";

		private const int FACEBOOK_ID_IDX = 0;
		private const int FRIENDS_IDS_IDX = 1;

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;

        public override void Execute (MMessage message)
		{
			LoggerController.Instance.Log ("UpdateUserFriendsCommand Received");

			if (message.Params [STATUS_IDX].Value == STATUS_OK) { //status OK
				{
					SpadesStateController.Instance.SaveLeaderboardFriendsUpdatedTime (DateTime.UtcNow.Ticks);


				}
			}

			LeaderboardsController.Instance.OnUpdateUserFriendsCompleted (message.Params [STATUS_IDX].Value == STATUS_OK);
		}
	}
}

