using cardGames.controllers;
using cardGames.models;
using common.comm;
using common.commands;
using common.controllers;
using spades.comm;
using spades.controllers;
using spades.models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace spades.commands
{

    public class GameCreatedCommand : Command
	{
        

        public override void Execute (MMessage message)
		{
			
			SimpleParam game_id_param = message.Params [0] as SimpleParam;

			CompositeParam players_param = message.Params [1] as CompositeParam;

			GenerateTableAndPlayers (game_id_param.Value, players_param.Params);

		}

        public void ExecuteLocalFake()
        {
            string gameId = RandomString(8);

            CompositeParam me = new CompositeParam();
            me.AddValueParam(ModelManager.Instance.GetUser().GetID().ToString());
            me.AddValueParam("");
            me.AddValueParam("");
            me.AddValueParam("");
            me.AddValueParam("");
            me.AddValueParam("");
            me.AddValueParam("");
            me.AddValueParam("");
            me.AddValueParam("0");


            CompositeParam players_param = new CompositeParam();
            players_param.AddParam(me);

            GenerateTableAndPlayers(gameId, players_param.Params);
        }

        private string RandomString(int length)
        {
            System.Random random = new System.Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private void GenerateTableAndPlayers (string game_id, List<Param> players)
		{
			SpadesTable table = new SpadesTable ();
			ModelManager.Instance.SetTable (table);	

			table.Game_id = game_id;

			//find yourself in the players list and create the player.
			User user = ModelManager.Instance.GetUser ();
			Player currPlayer = null;

			int pos_to_server_pos_offset=0;

			foreach (CompositeParam player_param in players)
			{
				string user_id = player_param.Params [0].Value;
				
				if (user_id == user.GetID ().ToString ())
				{
					
					currPlayer = new Player (table,3, Player.PlayerType.Human, ModelManager.Instance.GetUser() as SpadesUser);

					int curr_player_server_position = Convert.ToInt32 (player_param.Params[8].Value);

					if (curr_player_server_position == 0)
						currPlayer.Master = true;
					
					pos_to_server_pos_offset = curr_player_server_position - 3;

					table.Server_to_local_pos = pos_to_server_pos_offset;

					table.SetPlayer (3, currPlayer);
				}
			}

			//create the other human players

			foreach (CompositeParam player_param in players)
			{
				string user_id = player_param.Params [0].Value;

				if (user_id != user.GetID ().ToString ())
				{
					int curr_player_server_position = Convert.ToInt32 (player_param.Params[8].Value);

					int player_pos = table.ServerToLocalPos (curr_player_server_position);

					SpadesUser other_user = GenerateUser (player_param);

					Player currOtherHumanPlayer = new Player (table,player_pos, Player.PlayerType.Human, other_user);

					if (curr_player_server_position == 0)
						currOtherHumanPlayer.Master = true;

					table.SetPlayer (player_pos, currOtherHumanPlayer);

				}
			}

			//create bots
			if (currPlayer.Master)
			{
				LoggerController.Instance.Log ("Player is master, number of other human players: " + players.Count);

				if (players.Count < 4)
				{
					GenerateBots (players.Count);
				}
				if (players.Count == 1) {
					LoggerController.Instance.Log ("No other human players on table. Going offline...");
					//SpadesSpadesGameServerManager.Instance.GoOffline ();
				}

				// Send bots data 
				string command_c3_bots = SendDataCommandsBuilder.Instance.BuildBotsCommand(((SpadesTable)ModelManager.Instance.GetTable()).Players);
				MMessage msg = SpadesCommandsBuilder.Instance.BuildSendDataCommand(command_c3_bots, game_id, SendDataCommandsBuilder.SEND_DATA_BOTS);
				SpadesGameServerManager.Instance.Send(msg);
			}
			else
			{
				LoggerController.Instance.Log ("Player is slave, number of other human players: " + players.Count);
				GenerateEmptyPlayers ();
			}

		}

		private void GenerateEmptyPlayers ()
		{
			//LoggerController.Instance.Log ("GenerateEmptyPlayers");

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;

			for (int pos = 0; pos < 4; pos++)
			{
				if (table.GetPlayer (pos) == null)
				{
					Player currPlayer = new Player (table,pos, Player.PlayerType.Bot, null);
					table.SetPlayer (pos, currPlayer);

				}
			}
		}

		private void GenerateBots (int real_players)
		{

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;

            int lbId = ModelManager.Instance.Leaderboards.CurrentLeaderboardId;
			int botId = SpadesStateController.Instance.GetAssignedBotId (lbId);

            User user = ModelManager.Instance.GetUser();

            bool fbBotsOnly = user.GetLevel() <= 3;
            List<BotListItem> botList = (CardGamesLocalDataController.Instance as CardGamesLocalDataController).FetchRandomBots (4 - real_players,fbBotsOnly);
			

            int counter = 0;

			//RoundController.Instance.SpadesBrain.ChattingBrain.Bot_positions.Clear ();

			for (int pos = 0; pos < 4; pos++)
			{
				if (table.GetPlayer (pos) == null)
				{
					SpadesUser bot_user = new SpadesUser();
					// TODO: Create Mavatar from BotListItem here
					//bot_user.SetMAvatar(avatars[counter].AvatarModel);

					//
					MAvatarModel model = MAvatarController.Instance.GenerateMavatarForBot(botList[pos].Nickname, botList[pos].gender, botList[pos].FbID, user.GetLevel());
					bot_user.SetMAvatar(model);
					//

					Player currPlayer = new Player (table,pos, Player.PlayerType.Bot, bot_user);
					table.SetPlayer (pos, currPlayer);
					counter++;

					//RoundController.Instance.SpadesBrain.ChattingBrain.Bot_positions.Add (RoundController.AIAdapter.IndexToPosition (pos));
				}
			}

			//special override when creating bots to allow rematch - for real multi player we need to get the rematch flag from the server
			if (SingleMatchController.Instance.Rematch)
				table.SetPlayer(1,SingleMatchController.Instance.Rematch_player);

		}

		private SpadesUser GenerateUser (CompositeParam user_data)
		{
			SpadesUser user = new SpadesUser();

			//OldAvatarModel av = new OldAvatarModel ();

			string[] avatar_from_data = user_data.Params [3].Value.Split ('~');

			/*av.SetGenderIndex (Convert.ToInt32 (avatar_from_data [0]));
			av.SetHeadIndex (Convert.ToInt32 (avatar_from_data [1]));
			av.SetFaceIndex (Convert.ToInt32 (avatar_from_data [2]));
			av.SetHairFrontIndex (Convert.ToInt32 (avatar_from_data [3]));
			av.SetHairColorIndex (Convert.ToInt32 (avatar_from_data [4]));
			av.SetSkinIndex (Convert.ToInt32 (avatar_from_data [5]));
			av.Base_Index = Convert.ToInt32 (avatar_from_data [6]);
            av.GemsCount = Convert.ToInt32(avatar_from_data[7]);
            av.SetNickName (user_data.Params [1].Value);*/

			//user.SetAvatar (av);
			//user.SetCountry (user_data.Params [6].Value);

			//if (user_data.Params [7].Value == "0")
			//user.GetAvatar ().Avatar_mode = (OldAvatarModel.AvatarMode)Convert.ToInt32(user_data.Params [7].Value);
			//else
			//	user.GetAvatar ().Avatar_mode = true;

			//user.GetAvatar ().Fb_id = user_data.Params [2].Value;

			user.SetID (Convert.ToInt32(user_data.Params [0].Value));

			return user;
		}

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;

    }
}
