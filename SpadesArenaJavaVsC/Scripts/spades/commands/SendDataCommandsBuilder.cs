using common.comm;
using spades.models;
using cardGames.models;

namespace spades.commands
{

	public class SendDataCommandsBuilder : MonoBehaviour
	{

		public const string SEND_DATA_DEAL = "K1";
		public const string SEND_DATA_BID = "K2";
		public const string SEND_DATA_THROW = "K3";
		public const string SEND_DATA_CHAT = "K4";
		public const string SEND_DATA_BOTS = "K5";
		public const string SEND_DATA_SYNC = "K6";

		int					m_sent_C3_commands_counter=0;

		Encoder m_encoder;
		System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding ();

		void Start ()
		{
			m_encoder = new Encoder ('[', ']', '|');
		}

		public static SendDataCommandsBuilder Instance;

		private void Awake()
        {
            Instance = this;
        }

		public string BuildThrowCommand(Card card, int player_pos,int human)
		{
			m_sent_C3_commands_counter++;

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
			MMessage m = new MMessage ();
			m.Code = SendDataCommandsBuilder.SEND_DATA_THROW;
			string server_position = table.LocalToServerPos (player_pos).ToString();

			SimpleParam server_position_param = new SimpleParam (server_position);
			SimpleParam card_param = new SimpleParam (card.ToString ());
			SimpleParam is_human_param = new SimpleParam (human.ToString ());

			m.Params.Add (server_position_param);
			m.Params.Add (card_param);
			m.Params.Add (is_human_param);

			byte[] K3_Command = m_encoder.Encode (m);

			string K3_string_commnad = utf8.GetString (K3_Command);

			return K3_string_commnad;
		}

		public string BuildBidCommand(int bid, bool blind, int player_pos,int human)
		{
			m_sent_C3_commands_counter++;

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
			MMessage m = new MMessage ();
			m.Code = SendDataCommandsBuilder.SEND_DATA_BID;
			string server_position = table.LocalToServerPos (player_pos).ToString();

			//LoggerController.Instance.Log ("BUILD command from server plstring bid_string="";

			if (blind)
				bid = -1;

			SimpleParam server_position_param = new SimpleParam (server_position);
			SimpleParam bid_param = new SimpleParam (bid);
			SimpleParam is_human_param = new SimpleParam (human);

			m.Params.Add (server_position_param);
			m.Params.Add (bid_param);
			m.Params.Add (is_human_param);

			byte[] K2_Command = m_encoder.Encode (m);

			string K2_string_commnad = utf8.GetString (K2_Command);

			return K2_string_commnad;
		}

		public string BuildBotsCommand (Player[] players)
		{
			m_sent_C3_commands_counter++;
			
			MMessage m = new MMessage ();
			m.Code = SendDataCommandsBuilder.SEND_DATA_BOTS;

			byte[] K5_Command = m_encoder.Encode (m);

			string K5_string_commnad = utf8.GetString (K5_Command);

			return K5_string_commnad;
			 
		}

		public string BuildChatCommand (int player_pos,int chat_index)
		{
			m_sent_C3_commands_counter++;

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
			MMessage m = new MMessage ();
			m.Code = SendDataCommandsBuilder.SEND_DATA_CHAT;
			int server_position = table.LocalToServerPos (player_pos);

			m.Params.Add (new SimpleParam (server_position));
			m.Params.Add (new SimpleParam (chat_index));

			byte[] K4_Command = m_encoder.Encode (m);

			string K4_string_commnad = utf8.GetString (K4_Command);

			return K4_string_commnad;

		}

        public string BuildDealCommand (int dealer, CardsList[] hands, CardsList high_cards)
		{
			m_sent_C3_commands_counter++;

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;
			MMessage m = new MMessage ();
			m.Code = SendDataCommandsBuilder.SEND_DATA_DEAL;

			int server_dealer = table.LocalToServerPos(dealer);

            CompositeParam hands_param = new CompositeParam ();

			//when sending the deal command - we need to use server position - however in this case the master is always pos 3 - so we can hard code the positions

            hands_param.AddParam(new SimpleParam(hands[3].ToString()));
			hands_param.AddParam(new SimpleParam(hands[0].ToString()));
			hands_param.AddParam(new SimpleParam(hands[1].ToString()));
			hands_param.AddParam(new SimpleParam(hands[2].ToString()));
            

            m.Params.Add (new SimpleParam(high_cards.ToString()));
			m.Params.Add (hands_param);
            m.Params.Add(new SimpleParam(server_dealer.ToString()));

			byte[] K1_Command = m_encoder.Encode (m);

			string K1_string_commnad = utf8.GetString (K1_Command);

			return K1_string_commnad;

		}

		public int Sent_C3_commands_counter {
			get {
				return m_sent_C3_commands_counter;
			}
		}
	}
}
