using cardGames.commands;
using cardGames.models;
using common.models;
using spades.models;

namespace spades.commands
{
    public class SpadesCommandsBuilder : CardGamesCommandsBuilder
    {
        protected override void Awake()
        {
            base.Awake();
        }

        protected override CommonUser GetUser()
        {
            return ModelManager.Instance.GetUser() as SpadesUser;
        }

        protected override int GetWinRatio(CommonUser user)
        {
            return ((user as SpadesUser).Stats as SpadesUserStats).MatchWinsPartner;
        }
	}
}
