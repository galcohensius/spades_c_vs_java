﻿using cardGames.models;
using common.comm;
using common.commands;
using common.controllers;
using common.models;
using spades.controllers;
using System;
using System.Collections.Generic;

namespace spades.commands
{

    public class GetLeaderBoardCommand : Command
    {
        private const int MAX_RANKS_PARSE = 80;
        private const int STATUS_IDX = 0;
        private const string STATUS_OK = "0";

        private const int BOARD_TYPE_IDX = 1;
        private const int BOARD_ID_IDX = 2;
        private const int ENTRIES_IDX = 3;

       

        public override void Execute(MMessage message)
        {
            LoggerController.Instance.Log("GetLeaderBoardCommand Received");
            LeaderboardModel leaderboard = null;

            //try
            //{


                if (message.Params[STATUS_IDX].Value == STATUS_OK)
                { //status OK
                    LeaderboardItemModel itemModel;
                    LeaderboardsModel.LeaderboardType leaderboardType = LeaderboardsModel.LeaderboardType.GlobalCurrent;


                    if (Convert.ToInt32(message.Params[BOARD_TYPE_IDX].Value) == 1)
                    {
                        //check id  
                        LeaderboardsModel.LeaderboardType tp = ModelManager.Instance.Leaderboards.FindLeaderboardTypeById(Convert.ToInt32(message.Params[BOARD_ID_IDX].Value));

                        if (tp == LeaderboardsModel.LeaderboardType.GlobalCurrent)
                            leaderboard = ModelManager.Instance.Leaderboards.GetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalCurrent);
                        else
                            leaderboard = ModelManager.Instance.Leaderboards.GetLeaderboard(LeaderboardsModel.LeaderboardType.GlobalPrevious);

                        leaderboardType = tp;
                    }

                    if (Convert.ToInt32(message.Params[BOARD_TYPE_IDX].Value) == 2)
                    {
                        leaderboardType = LeaderboardsModel.LeaderboardType.Friends;
                        leaderboard = ModelManager.Instance.Leaderboards.GetLeaderboard(leaderboardType);
                    }

                    if (Convert.ToInt32(message.Params[BOARD_TYPE_IDX].Value) == 10)
                    {
                        leaderboardType = LeaderboardsModel.LeaderboardType.HallOfFame;
                        leaderboard = ModelManager.Instance.Leaderboards.GetLeaderboard(leaderboardType);
                    }

                    string mRanks = SpadesStateController.Instance.GetLeaderboardRanks(leaderboardType);
                    Dictionary<long, int> mRanksDataDict = ParseRanksData(mRanks);

                    Array values = Enum.GetValues(typeof(LeaderboardModel.RankState));
                    System.Random random = new System.Random();

                    CompositeParam leaderboardItems = message.Params[ENTRIES_IDX] as CompositeParam;

                    if (leaderboardItems != null)
                    {

                        foreach (CompositeParam user_param in leaderboardItems.Params)
                        {
                            itemModel = new LeaderboardItemModel();

                            SimpleParam user_rank = user_param.Params[LeaderboardItemModel.RANK_IDX] as SimpleParam;
                            SimpleParam user_id = user_param.Params[LeaderboardItemModel.USER_ID_IDX] as SimpleParam;
                            SimpleParam user_facebook = user_param.Params[LeaderboardItemModel.FACEBOOK_ID_IDX] as SimpleParam;
                            SimpleParam user_score = user_param.Params[LeaderboardItemModel.SCORE_IDX] as SimpleParam;
                            SimpleParam user_details = user_param.Params[LeaderboardItemModel.USER_DETAILS_IDX] as SimpleParam;

                            itemModel.Rank = Convert.ToInt32(user_rank.Value);
                            itemModel.UserId = Convert.ToInt64(user_id.Value);
                            itemModel.FacebookId = user_facebook.Value;
                            itemModel.Score = Convert.ToInt64(user_score.Value);
                            itemModel.MAvatarModel = LeaderboardsController.Instance.ParseUserDetails(user_details.Value, itemModel);
                            //itemModel.AvatarModel.Fb_id = itemModel.FacebookId;

                            if (mRanksDataDict.ContainsKey(itemModel.UserId) && mRanksDataDict[itemModel.UserId] != 0)
                            {
                                if (mRanksDataDict[itemModel.UserId] > itemModel.Rank)
                                {
                                    itemModel.RankState = LeaderboardModel.RankState.UP;
                                }
                                else if (mRanksDataDict[itemModel.UserId] < itemModel.Rank)
                                {
                                    itemModel.RankState = LeaderboardModel.RankState.DOWN;
                                }
                                else
                                {
                                    itemModel.RankState = LeaderboardModel.RankState.NO_CHANGE;
                                }
                            }
                            else if (mRanksDataDict.Count > 0)
                            {
                                LeaderboardModel.RankState randomRank = (LeaderboardModel.RankState)values.GetValue(random.Next(values.Length));
                                itemModel.RankState = randomRank;
                            }
                            else
                            {
                                itemModel.RankState = LeaderboardModel.RankState.NO_CHANGE;
                            }

                            if (leaderboard != null && leaderboard.Items == null)
                            {
                                leaderboard.Items = new List<LeaderboardItemModel>();

                            }
                            leaderboard.Items.Add(itemModel);

                        }
                    }

                    ModelManager.Instance.Leaderboards.SetLeaderboard(leaderboardType, leaderboard);
                    LeaderboardsController.Instance.OnLeaderboardReceived(leaderboardType);

                }
                else
                {
                    //TODO handle error here
                }
            //}
            //catch (Exception e)
            //{
            //    LoggerController.Instance.Log("Error in getLeaderboard command: " + e.Message);
            //    throw e;
            //}

        }

        private Dictionary<long, int> ParseRanksData(string ranksData)
        {
            Dictionary<long, int> retVal = new Dictionary<long, int>();

            if (ranksData == "")
                return retVal;

            string[] ranks = ranksData.Split(',');

            for (int i = 0; i < ranks.Length; i++)
            {
                //fix for negative userid
                if (ranks[i].Split('-').Length == 3)
                    retVal.Add(Convert.ToInt64(ranks[i].Split('-')[1]) * (-1), Convert.ToInt32(ranks[i].Split('-')[2]));
                else
                    retVal.Add(Convert.ToInt64(ranks[i].Split('-')[0]), Convert.ToInt32(ranks[i].Split('-')[1]));

            }
            return retVal;
        }

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;

    }
}