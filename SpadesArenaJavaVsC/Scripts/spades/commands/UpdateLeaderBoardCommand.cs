﻿using common.comm;
using common.commands;
using common.controllers;
using spades.controllers;

namespace spades.commands
{
    public class UpdateLeaderBoardCommand : Command
	{

        public override void Execute (MMessage message)
		{
			int success;

			if (!int.TryParse(message.Params [0].Value,out success))
			{
				success = 1;
			}

			if (success == 0) {
                LoggerController.Instance.Log ("UpdateLeaderBoardCommand received - OK");

				//clear allcashe
				LeaderboardsController.Instance.StartLeaderboardCacheTimer();
                // TODO: Handle leaderboard updates when contests are not released yet
				//LeaderboardsController.Instance.UpdateLeaderboardBotWin ();
			} else {
                LoggerController.Instance.Log ("UpdateLeaderBoardCommand received - Error");
			}


			LeaderboardServerManager.Instance.Disconnect ();
		}

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;
	}
}