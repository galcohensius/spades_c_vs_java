using System.Collections.Generic;
using common.comm;
using common.controllers;
using spades.controllers;
using System;
using spades.models;
using common.commands;
using spades.views.popups;
using cardGames.models;

namespace spades.commands
{

	public class SendDataCommand : Command
	{
		Decoder m_decoder;
		System.Text.UTF8Encoding utf8 = new System.Text.UTF8Encoding ();

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;

        public SendDataCommand ()
		{
			m_decoder = new Decoder ('[', ']', '|');
		}

		public override void Execute (MMessage message)
		{
			//LoggerController.Instance.Log ("SendData Command Received in class");

			//if i am the Master (sending client) - i will ideally get an 0 answer indicating data was sent - if i am a Slave i will get back the data and handle that;
            
			string game_id = message.Params [0].Value; 

			if (ModelManager.Instance.GetTable ().Game_id != game_id)
			{
				LoggerController.Instance.LogFormat ("Wrong Game ID received. Curr game id is {0}, message game id is {1}", ModelManager.Instance.GetTable ().Game_id, game_id);
;
                //return;
            }
            

			string msg_value="";
			//TODO: change the answer back to an Enum
			int status = Convert.ToInt32 (message.Params [1].Value); 

			if (status == 0)
			{
				string dataCode = message.Params [2].Value;

				MMessage msg = new MMessage ();

				msg_value = message.Params [3].Value;

				msg = m_decoder.Decode (utf8.GetBytes (msg_value));


				if (!RoundController.Instance.Play_recorded_commands)// if not playing do the normal flow
				{
					switch (dataCode)
					{
					case(SendDataCommandsBuilder.SEND_DATA_DEAL):

						ProcessDealData (msg.Params);
						break;

					case(SendDataCommandsBuilder.SEND_DATA_BID):

						ProcessBid (msg.Params);
						break;

					case(SendDataCommandsBuilder.SEND_DATA_THROW):

						ProcessThrow (msg.Params);
						break;

					case(SendDataCommandsBuilder.SEND_DATA_CHAT):

						ProcessChat (msg.Params);
						break;

					case(SendDataCommandsBuilder.SEND_DATA_BOTS):

						ProcessBotsData (msg.Params);
						break;

					}
				}
				else//while playing the recorded commands - new commands arrive and need to be added as if they were server commands
				{
					SimpleParam command = new SimpleParam(message.Params [3].Value);

					LoggerController.Instance.Log (command.Value);

					LoggerController.Instance.Log ("Message arrived while playing recorded messages - adding to list");

					RoundController.Instance.Commands.Add (command);

				}

			}
			else if (status == 1)
			{
				//game not found
			}
			else if (status == 2)
			{
				//position not found
			}

		}

		private void ProcessThrow (List<Param> data)
		{
			int player_server_index = Convert.ToInt32(data [0].Value);
			Card card = new Card (data [1].Value);
			int pos = ModelManager.Instance.GetTable ().ServerToLocalPos (player_server_index);
			int player_type = Convert.ToInt32 (data [2].Value);

			int player_server_pos = RoundController.Instance.Player.ServerPos;
			bool is_player_master = RoundController.Instance.Player.Master;

			RoundController.Instance.ExecuteThrowCommand (card, pos, player_type);
			RoundController.Instance.Card_thrown_processed = true;

		}

		private void ProcessChat (List<Param> data)
		{
			int player_server_index = Convert.ToInt32(data [0].Value);
			int chat_index = Convert.ToInt32(data [1].Value);

			int player_pos = ModelManager.Instance.GetTable ().ServerToLocalPos (player_server_index);

			//if (RoundController.Instance.Player.ServerPos!=player_server_index)
			//{

				RoundController.Instance.ChatReceivedFromServer (chat_index, player_pos);

			//}

		}

		private void ProcessBid (List<Param> data)
		{

			int player_server_index = Convert.ToInt32(data [0].Value);
			int bid_value = Convert.ToInt32 (data [1].Value);
			int player_type = Convert.ToInt32(data [2].Value);
			int player_pos = ModelManager.Instance.GetTable ().ServerToLocalPos (player_server_index);
			//i am NOT the player
			bool blind = false;

			if (bid_value == -1)
				blind = true;


			if (RoundController.Instance.Player.ServerPos == player_server_index)
			{
				//i am the player
				RoundController.Instance.Bidding_popup.GetComponent<BiddingPopup>().CloseBiddingKeyboard();

			}

			RoundController.Instance.ExecuteBidCommand(bid_value, player_pos, blind, player_type);


		}

		private void ProcessDealData(List<Param> data)
		{
				SimpleParam high_cards_param = (SimpleParam)data [0];
				CompositeParam hands_param = (CompositeParam)data [1];
				SimpleParam dealer_param = (SimpleParam)data [2];

				CardsList high_cards_list = new CardsList (high_cards_param.Value);

				int dealer = Convert.ToInt32 (dealer_param.Value);
				List<CardsList>	hands = new List<CardsList> ();

				for (int i = 0; i < 4; i++)
				{
					hands.Add (new CardsList (hands_param.Params [i].Value));
				}

				RoundController.Instance.ExecuteDealCommand (dealer, high_cards_list,hands);

		}

		private void ProcessBotsData (List<Param> bot_data)
		{
			bool isMaster = ((SpadesTable)ModelManager.Instance.GetTable()).GetPlayer(3).Master;

			if (!isMaster)
			{
				// For slaves, set the bots data here. Master already have this model assigned.
				isMaster = false;
				SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;

				for (int i = 0; i < bot_data.Count; i++)
				{
					CompositeParam data_params = bot_data [i] as CompositeParam;
					//OldAvatarModel av = new OldAvatarModel ();
					SpadesUser bot_user = new SpadesUser ();

					string[] avatar_from_data = data_params.Params [3].Value.Split ('~');

					/*av.SetGenderIndex (Convert.ToInt32 (avatar_from_data [0]));
					av.SetHeadIndex (Convert.ToInt32 (avatar_from_data [1]));
					av.SetFaceIndex (Convert.ToInt32 (avatar_from_data [2]));
					av.SetHairFrontIndex (Convert.ToInt32 (avatar_from_data [3]));
					av.SetHairColorIndex (Convert.ToInt32 (avatar_from_data [4]));
					av.SetSkinIndex (Convert.ToInt32 (avatar_from_data [5]));
					av.Base_Index = Convert.ToInt32 (avatar_from_data [6]);
                    av.GemsCount = Convert.ToInt32(avatar_from_data[7]);*/

                    //av.SetNickName (data_params.Params [1].Value);

					if (data_params.Params [2].Value != "")
					{
						//av.Fb_id = data_params.Params [2].Value;
						//av.Avatar_mode = OldAvatarModel.AvatarMode.FBImage;
					}
					else
					{
						//av.Avatar_mode = OldAvatarModel.AvatarMode.Avatar;
					}
	
					//bot_user.SetAvatar (av);
					bot_user.SetCountry (data_params.Params [6].Value);

					int server_position = Convert.ToInt32 (data_params.Params [8].Value);
					int player_pos = table.ServerToLocalPos (server_position);


					table.GetPlayer (player_pos).User = bot_user;
				}
			}

			

			LobbyController.Instance.Searching_for_players_popup.SearchOtherPlayers (()=>{
				if (isMaster)  {
                    LoggerController.Instance.Log(LoggerController.Module.Game, "Searching players finished");
					// For master only, invoke the TableFound method here
					RoundController.Instance.Table_found_delegate(null);
				}
			});

		}

	}
}

