﻿using common.comm;
using common.controllers;
using spades.controllers;
using common.commands;
using System;
using spades.models;
using cardGames.models;

namespace spades.commands
{

	public class PlayerStatusCommand : Command
	{
      

        public enum PlayerStatusType
		{
			Disconnect,
			Reconnect,
			Quit
		}

		public override void Execute (MMessage message)
		{
			LoggerController.Instance.Log ("PlayerStatusCommand Received");

			//LoggerController.Instance.Log ("Connect to table Received");

			string game_id = message.Params [0].Value;
			int server_position = Convert.ToInt32 (message.Params [1].Value);
			PlayerStatusType change_type = (PlayerStatusType)Convert.ToInt32 (message.Params [2].Value);

			int local_position = ModelManager.Instance.GetTable ().ServerToLocalPos (server_position);

			SpadesTable table = ModelManager.Instance.GetTable() as SpadesTable;

			LoggerController.Instance.Log ("Player in Server Pos: " + server_position + " Local Pos" + local_position + " Left with quit type of: " + change_type);

			//change the player type according to the status
			if (change_type == PlayerStatusType.Quit)
			{
				table.Players [local_position].Player_Type = Player.PlayerType.Bot;

			}
			else if (change_type == PlayerStatusType.Disconnect)
			{
				table.Players [local_position].Turns_played_as_zombie = 0;
				table.Players [local_position].Player_Type = Player.PlayerType.Zombie;

			}
			else if (change_type == PlayerStatusType.Reconnect)
			{
				table.Players [local_position].Player_Type = Player.PlayerType.Human;
			}


			// Find out if this player is now the master
			table.GetPlayer (local_position).Master = false;

			// Find the min server position
			int min_server_pos = 10;
			for (int i = 0; i < 4; i++)
			{
				table.Players [i].Master = false;	
						
				if (table.Players [i].Player_Type == Player.PlayerType.Human)
					min_server_pos = Math.Min (min_server_pos, table.Players [i].ServerPos);
			}

			int min_local_pos = table.ServerToLocalPos (min_server_pos);

			table.Players [min_local_pos].Master = true;

			//if (table.GetPlayer (3).ServerPos == min_server_pos)
			//{
			//	table.GetPlayer (3).Master = true;

			//}

			LoggerController.Instance.Log ("Player in local position " + min_local_pos +  " And server pos: " +min_server_pos+ " is the new Master");



			// If I am the master, start playing for the zombie player
			if (table.GetPlayer (3).Master)
			{
				// Check if the disconnected player is the current and start playing
				if (table.GetPlayer (local_position).Player_Type != Player.PlayerType.Human && RoundController.Instance.Spades_engine.Curr_player_index == local_position)
				{
					LoggerController.Instance.Log ("Master started playing for player at " + local_position);

					//RoundController.Instance.NotifyPlayerToPlayOrBid (local_position);
					RoundController.Instance.StartTurn (local_position);
				}
			}

					
		}

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;


    }
}