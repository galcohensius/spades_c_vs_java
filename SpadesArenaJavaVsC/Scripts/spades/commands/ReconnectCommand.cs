﻿using common.comm;
using common.commands;
using common.controllers;
using spades.controllers;
using System;

namespace spades.commands
{

    public class ReconnectCommand : Command
    {

        public override void Execute(MMessage message)
		{
			LoggerController.Instance.Log ("Reconnect Command received --- ");

			int status = Convert.ToInt32 (message.Params [0].Value); 
			string game_id = message.Params [1].Value; // Not used on the client for now
			CompositeParam commands = message.Params [2] as CompositeParam;

			CompositeParam connected_positions = message.Params [3] as CompositeParam;

			RoundController.Instance.ReconnectReceived (commands.Params,connected_positions.Params);
		}

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;
    }
}