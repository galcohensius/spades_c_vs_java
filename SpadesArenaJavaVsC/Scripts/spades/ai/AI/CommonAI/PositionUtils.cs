﻿using System;
using System.Collections.Generic;
using System.Linq;
using SpadesAI.model;

namespace SpadesBots.Simulation {
    public class PositionUtils {
        /// <summary>
        /// Creates a list representing the order of the positions to play.
        /// </summary>
        /// <param name="firstPosition"> The first position.</param>
        /// <returns> A list of the positions.</returns>
        public static List<Position> ConstructPlayingPositionsOrder(Position firstPosition, int playersPlayedThisTrick = 0) {
            Position tempPosition = firstPosition;
            List<Position> playingPositionsOrderList = new List<Position>();
            for (int i = 0; i < Enum.GetValues(typeof(Position)).Length - playersPlayedThisTrick; i++) {
                playingPositionsOrderList.Add(tempPosition);
                tempPosition = GetNextPlayingPosition(tempPosition);
            }
            return playingPositionsOrderList;
        }

        /// <summary>
        /// Returns the next playing position.
        /// </summary>
        /// <param name="position"> The position.</param>
        /// <returns> The next playing position.</returns>
        public static Position GetNextPlayingPosition(Position position) {
            switch (position) {
                case Position.South:
                    return Position.West;
                case Position.West:
                    return Position.North;
                case Position.North:
                    return Position.East;
                case Position.East:
                    return Position.South;
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Randomly chooses a position.
        /// </summary>
        /// <returns> A random position.</returns>
        public static Position GetRandomPosition() {
            Position[] positions = Enum.GetValues(typeof(Position)).Cast<Position>().ToArray();
            Random random = new Random();
            int randomNumber = random.Next(4);
            return positions[randomNumber]; 
        }

        ///
        public static Position GetPartnershipPosition(Position position)
        {
            switch (position)
            {
                case Position.East:
                case Position.West:
                    return Position.East;
                case Position.North:
                case Position.South:
                    return Position.North;
                default:
                    return 0;
            }
        }
    }
}