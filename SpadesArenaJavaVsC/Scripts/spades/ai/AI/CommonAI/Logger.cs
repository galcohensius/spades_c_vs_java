﻿using System;


namespace CommonAI
{
	/// <summary>
	/// A logger utility that is used to print both using the System.Diagnostics mechanism and the UnityEditor
	/// </summary>
	public static class Logger
	{
        public static Action<string> OnDebugLog;
		
		public static bool Enabled = false;

		public static void Debug(string msg) {
			if (!Enabled)
				return;
			//DebugDiagnostics (msg);
			DebugUnityEngine (msg);

            if (OnDebugLog != null)
                OnDebugLog(msg);
		}

		public static void DebugFormat(string msg,params object[] tokens) {
			Debug (string.Format (msg, tokens));
		}

        
		private static void DebugUnityEngine(string msg) {
			try {
//				// LoggerController.Instance.Log(msg);
			} catch (Exception) {
			}
		}
       
		/*
		private static void DebugDiagnostics(string msg) {
		    System.Diagnostics.Debug.Print (msg);
			//Console.WriteLine(msg);
		}
 */

    }
}

