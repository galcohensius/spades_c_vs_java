﻿using System;
using System.Collections.Generic;
using cardGames.models;

namespace CommonAI {
    class SuitUtils {

        /// <summary>
        /// Returns all the suits except the joker suit.
        /// </summary>
        /// <returns> All the suits except the joker suit. </returns>
        public static List<Card.SuitType> getAllSuitsNoJoker() {
            List<Card.SuitType> result = new List<Card.SuitType>();
            foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType))) {
                if (suit == Card.SuitType.Joker) {
                    continue;
                }
                result.Add(suit);
            }
            return result;
        }

        /// <summary>
        /// Returns the other suits.
        /// </summary>
        /// <param name="suitsToExclude"> The suits to exculde. </param>
        /// <returns> The other suits. </returns>
        public static List<Card.SuitType> GetOtherSuits(ICollection<Card.SuitType> suitsToExclude) {
            List<Card.SuitType> result = new List<Card.SuitType>();
            foreach (Card.SuitType suit in getAllSuitsNoJoker()) {
                if (!suitsToExclude.Contains(suit)) {
                    result.Add(suit);
                }
            }
            return result;
        }
    }
}
