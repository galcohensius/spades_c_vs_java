﻿using System.Collections.Generic;
using System.Text;
using cardGames.models;

namespace CommonAI
{
	/// <summary>
	/// A cards list with strength factor.
	/// Each card is attached to a strength (0..1). The list is ordered by strength, the strongest card is the first.
	/// </summary>
	public class AIStrengthCardsList:CardsList
	{
		bool isOrderedByStrength = false;

		public AIStrengthCardsList ()
		{
		}

		public AIStrengthCardsList (CardsList cl):base(cl)
		{
		}


		public void AddCardWithStrength(Card card, float strength) {
			card.Strength = strength;
			Add (card);

			isOrderedByStrength = false;
		}


		private void OrderByStrength() {
			if (isOrderedByStrength)
				return;
			
			Sort ((c1, c2) => {
				if(c1.Strength == c2.Strength) {
					// Same strength, check suit and rank
					if(c1.Suit == Card.SuitType.Spades && c2.Suit != Card.SuitType.Spades) {
						return -1;
					} else if(c1.Suit != Card.SuitType.Spades && c2.Suit == Card.SuitType.Spades) { 
						return 1;
					} else {
						return (int)(c2.Rank - c1.Rank);
					}
				}
				if(c1.Strength<c2.Strength)
					return 1;
				else 
					return -1;
				
			});
			isOrderedByStrength = true;
		}


		public Card GetStrongCard() {
			if (Count == 0)
				return null;
			OrderByStrength ();
			return this [0];
		}

		public Card GetWeakCard() {
			if (Count == 0)
				return null;
			OrderByStrength ();
			return this [Count-1];
		}

        /// <summary>
        /// Gets the strongest card with the specified strength
        /// <returns>The card or null if not found</returns>
        /// </summary>
        public Card GetStrongestCardByStrength(float strength) {
			OrderByStrength ();
			Card card = Find (c => {
				return c.Strength==strength;
			});
			if (card == null) {
				return null;
			} else {
				return card;
			}

		}

		/// <summary>
		/// Gets the weakest card with the specified strength
		/// <returns>The card or null if not found</returns>
		/// </summary>
		public Card GetWeakestCardByStrength(float strength) {
			OrderByStrength ();
			Card card = FindLast (c => {
				return c.Strength==strength;
			});
			if (card == null) {
				return null;
			} else {
				return card;
			}

		}

		public float GetHighestStrength() {
			if (Count == 0)
				return 0;
			OrderByStrength ();
			return this [0].Strength;
		}

		public float GetLowestStrength() {
			if (Count == 0)
				return 0;
			OrderByStrength ();
			return this [Count-1].Strength;
		}

		public CardsList GetCardsWithStrength(float strength) {
			List<Card> cl = FindAll (c=>{
				return c.Strength==strength;
			});
			CardsList result = new CardsList ();
			foreach (Card card in cl) {
				result.Add (card);
			}

			return result;
		}

        // TODO: UT.
        public AIStrengthCardsList GetCardsWithinStrengthRange(float minStrength, float maxStrength) {
            List<Card> cl = FindAll(c => {
                return minStrength <= c.Strength && c.Strength <= maxStrength;
            });
            AIStrengthCardsList result = new AIStrengthCardsList();
            foreach (Card card in cl) {
                result.Add(card);
            }

            return result;
        }

        /// <summary>
        /// Gets the strength of card if found on the list. If not returns -1
        /// </summary>
        /// <returns>The strength of card or -1.</returns>
        /// <param name="card">The card to search for.</param>
        public float GetStrengthOfCard(Card card) {
			foreach (Card c in this) {
				if (c == card)
					return c.Strength;
			}
			return -1;
		}


		public override string ToString ()
		{
			OrderByStrength ();
			StringBuilder result = new StringBuilder ();

			result.Append ("StrengthCardList:\n");
			foreach (Card card in this) {
				result.Append (card + "\t" + card.Strength + "\n");
			}

			return result.ToString ();
		}

		public string ToStringCardsOnly() {
			return base.ToString ();
		}
		
	}


}

