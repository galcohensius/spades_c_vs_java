using SpadesAI.model;
using spades.models;
using cardGames.models;

#if !UNITY_WEBGL

#endif

namespace SpadesAI
{

    /// <summary>
    /// Async PlayMove.
    /// For mobile, PlayMove will be called using a Thread.
    /// For WebGL, PlayMove will be called after ThinkMove returns true
    /// </summary>
    public class BotASyncPlayMove : CustomYieldInstruction
    {
        ISpadesBrain spadesBrain;

        bool finished;

        Card playMoveResult;

        Position pos;
        SpadesPlayerRoundData prData;

        public BotASyncPlayMove(ISpadesBrain spadesBrain, Position pos, SpadesPlayerRoundData prData)
        {
            this.spadesBrain = spadesBrain;
#if !UNITY_WEBGL && false // CURRENTLY NOT USED (RAN 4/11/2019)
            Thread t = new Thread(()=>{
                playMoveResult = spadesBrain.PlayMove(pos,prData);
                finished = true;
            });
            t.Start();
#else
            this.pos = pos;
            this.prData = prData;
#endif
        }


        public override bool keepWaiting
        {
            get
            {
#if !UNITY_WEBGL && false // CURRENTLY NOT USED (RAN 4/11/2019)
                return !finished;
#else
                if (spadesBrain.ThinkMove(pos))
                {
                    playMoveResult = spadesBrain.PlayMove(pos,prData);
                    return false;
                }
                return true;
#endif
            }
        }


        public Card PlayMoveResult
        {
            get
            {
                return playMoveResult;
            }
        }
    }

}
