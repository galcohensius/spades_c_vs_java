using System.Collections.Generic;
using cardGames.models;
using spades.models;
using SpadesAI.model;


namespace SpadesAI {

    public enum BrainType
    {
        ClassicV1,          // Alon
        ClassicV2,          // Shachar
        ClassicV3,          // Peeking
		ClassicV4,          // Gal
        ClassicV4Peeking,   // Another peeking (for solo)

        Mirror,             // Gal Mirror
        Whiz,               // Gal Whiz
        Suicide,            // Gal Suicide
        Jokers,             // Gal Jokers
        GGP,                // Stephen Smith
        CallBreak,          // Gal - indian game
        UCT,                // search alg'

        // New 2020 implementations: 
        Jokers2020,          // loser and solo - Gal. loser@lvl 0
        ClassicV4JustToValidate,
        V3_LessObvious,     // Peeking but less obvious
        RoyaleBot,          // Plays the 8-cards variant
    }


    public interface ISpadesBrain
    {
        void StartMatch(AIMatchData matchData);

        void StartRound(SpadesTable table, Dictionary<Position, int> skillLevels, Dictionary<Position, BrainType> botTypes);

        void SetHand(Position pos, CardsList hand);

        CardsList GetHand(Position pos);

        int MakeBid(Position pos, SpadesPlayerRoundData prData);

        void SetBid(Position pos, int bid,bool isBlindNil=false);

        bool ThinkBid(Position pos);

        void StartTrick();

        Card PlayMove(Position pos, SpadesPlayerRoundData prData);

        void SetMove(Position pos, Card myCard);

        bool ThinkMove(Position pos);

        void TakeTrick(Position takerPos);

        AIMatchData MatchData
        {
            get;
            set;
        }

        IBiddingBrain BiddingBrain
        {
            get;
        }
    }

}
