﻿using System;
using SpadesAI.model;
using System.Collections.Generic;
using spades.models;

namespace SpadesAI {
    public class ChattingBrain {
        public delegate void TriggerChatDelegate(Position pos, AIChatType type, float delay);

        public enum AIChatType {
            GoodMove,
            OppoGoodMove,
            RoundStart,
            Congrats,
            Random
        }

        private const float PERCENT_START_GAME = .2f;
        private const float PERCENT_TRICK_TAKEN = .2f;
        private const float PERCENT_PLAYER_CHAT = .3f;

        private SpadesBrainImpl spadesBrain;
        private long lastChatTimestamp;
        private Random rand = new Random();

        private TriggerChatDelegate onChat;

        // List of positions of bots (excluding human players)
        private List<Position> botsPositions = new List<Position>();

        public ChattingBrain(SpadesBrainImpl spadesBrain) {
            this.spadesBrain = spadesBrain;
        }

        internal void RoundStarted() {
            if (rand.NextDouble() < PERCENT_START_GAME) {
                Position? pos = GetRandomBotPos();

                if (pos != null) {
                    AIChatType type = AIChatType.RoundStart;

                    float delay = rand.Next(2, 8);

                    if (onChat != null)
                        onChat((Position)pos, type, delay);
                }

            }
        }

        internal void TrickTaken(Position winnerPos) {

            if (rand.NextDouble() < PERCENT_TRICK_TAKEN) {

                Position? pos = GetRandomBotPos();

                if (pos != null) {
                    AIChatType type = AIChatType.OppoGoodMove;
                    if (pos == winnerPos ||
                        spadesBrain.MatchData.Mode == PlayingMode.Partners && pos == SpadesUtils.PartnerPosition(winnerPos) ||
                        spadesBrain.Players[(Position)pos].Takes >= spadesBrain.Players[(Position)pos].Bid) {
                        type = AIChatType.GoodMove;
                    }

                    float delay = rand.Next(10, 20) / 10;

                    if (onChat != null)
                        onChat((Position)pos, type, delay);
                }

            }
        }

        public void PlayerChatted() {
            if (rand.NextDouble() < PERCENT_PLAYER_CHAT) {
                Position? pos = GetRandomBotPos();

                if (pos != null) {
                    AIChatType type = AIChatType.Random;

                    float delay = rand.Next(10, 20) / 10;

                    if (onChat != null)
                        onChat((Position)pos, type, delay);
                }
            }
        }

        /// <summary>
        /// Gets the random bot position for chatting
        /// </summary>
        private Position? GetRandomBotPos() {
            if (botsPositions.Count == 0)
                return null;

            return botsPositions[rand.Next(0, botsPositions.Count)];
        }

        public void AddBotPosition(Position pos) {
            botsPositions.Add(pos);
        }

        public void ClearBotPositions() {
            botsPositions.Clear();
        }

        public TriggerChatDelegate OnChat {
            get {
                return this.onChat;
            }
            set {
                onChat = value;
            }
        }
    }
}

