using System;
using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using common.utils;
using CommonAI;
using spades.models;
using SpadesAI.model;

namespace SpadesAI.Internal.V4
{
    internal class BiddingBrainJokers : BiddingBrainV4
    {
        protected static List<string[]> m_jokers_nil_1_list;
        protected static List<string[]> m_jokers_nil_2_list;
        protected static List<string[]> m_jokers_nil_3_list;
        protected static List<string[]> m_high_cards_bid_value_list_Jokers;

        internal BiddingBrainJokers(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            Logger.Enabled = true;

            m_high_cards_bid_value_list_Jokers = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\high_cards_bid_value_12or13suits");

            m_jokers_nil_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\Jokers_partners_1");
            m_jokers_nil_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\Jokers_partners_2");
            m_jokers_nil_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\Jokers_partners_3");
            deck = "jokers";
        }

        private static float GetProbTableValueJokers(int numberOfPlayersThatMightCutYou, int cardsInSuit, int cardsInHand, Card.RankType rank)
        {
            Dictionary<string, float> highCardsBidValue = m_high_cards_bid_value_list_Jokers.ToDictionary(x => x[0] + x[1] + x[2] +x[3], x => float.Parse(x[4]));

            string lookForKey = numberOfPlayersThatMightCutYou.ToString() + cardsInSuit.ToString() + cardsInHand.ToString() + rank.ToString()[0];
            highCardsBidValue.TryGetValue(lookForKey, out float value);
            return value;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////// determined bid for (hand, position)
        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, " =================== position: {0} =================== ", pos);
            Position prt = SpadesUtils.PartnerPosition(pos);
            Position lho = SpadesUtils.LhoPosition(pos);
            Position rho = SpadesUtils.RhoPosition(pos);
            prData.BiddingPosition = GetBiddingPosition();

            int rhoPointsIfBetMet = GetPointsIfBetMet(rho);
            int prtPointsIfBetMet = GetPointsIfBetMet(prt);
            int lhoPointsIfBetMet = GetPointsIfBetMet(lho);

            int partnersPointsIfBidMetBeforeMyBid = prtPointsIfBetMet + spadesBrain.MatchData.MatchPoints[pos];
            int opponentsPointsIfBidMet = lhoPointsIfBetMet + rhoPointsIfBetMet;
            int opponentsBags = spadesBrain.MatchData.MatchBags[lho] + spadesBrain.MatchData.MatchBags[rho];
            int partnersBags = spadesBrain.MatchData.MatchBags[prt] + spadesBrain.MatchData.MatchBags[pos];
            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;

            // ReSharper disable once RedundantAssignment
            int finalBid = -1;
            bool nil = false;

            // before looking at the cards, check for Blind-Nil
            spadesBrain.Players[pos].BlindNil = false;
            if (ShouldBidBlindNil(pos, prData.BiddingPosition, opponentsPointsIfBidMet, partnersPointsIfBidMetBeforeMyBid))
            {
                // small peek to insure you dont hold BJ, since human would have quit the game.  
                if (!spadesBrain.Players[pos].Hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.JokerBig))
                {
                    spadesBrain.Players[pos].BlindNil = true;
                    finalBid = -2; // Bot treat this as bid = 0 & BlindNil = true
                    prData.Bid = finalBid;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, " Blind Nil ");
                    return finalBid;
                }
            }

            // You see your hand only after deciding if you try Blind Nil
            CardsList hand = spadesBrain.Players[pos].Hand;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "position {0}, hand: {1} ", pos, hand);

            // Initial bid as a function of the hand
            prData.InitialBid = InitialBidJokers(pos, hand, spadesBrain.MatchData.Mode);

            // adjustment and round according to bot aggressiveness level 
            float otherPlayersFactor = OtherPlayersFactor(prData.BiddingPosition, (int) Math.Round(prData.InitialBid, MidpointRounding.AwayFromZero));
            prData.InitialBid += otherPlayersFactor;
            if (prData.InitialBid <= 1) prData.InitialBid = 1; 

            // adjustment according bot aggressiveness level 
            int botLevel = spadesBrain.Players[pos].Level;
            prData.NumericBid = RoundingByBotLevel(prData.InitialBid, botLevel);

            // Check Nil
            float nilChances = 0;
            if (spadesBrain.MatchData.SubVariation != PlayingSubVariant.NoNil)
                nilChances = PartnersNilChancesJokers(hand);
            if (CheckNilJokers(spadesBrain.MatchData.Mode, hand, prData, pos, prData.BiddingPosition, nilChances, botLevel))
                nil = true;
            prData.NilValue = nilChances; // record in Database at table round_game_transaction

            // in C4P just maximize E[points], otherwise maximize Pr(Win)
            if (spadesBrain.MatchData.SubVariation != PlayingSubVariant.Coins4Points)
            {
                // bid {-1 / +1 / +2 / =0 / sum = 14} when you are about to lose the game if you don't (dont enter if you nil)
                if (nil == false)
                {
                    prData.NumericBid = LastRoundBidAdjust(bagsToPenalty, opponentsBags, prData.NumericBid, pos, hand,
                        prData.BiddingPosition, opponentsPointsIfBidMet, partnersPointsIfBidMetBeforeMyBid, nilChances);
                    if (prData.NumericBid == 0)
                    {
                        nil = true;
                    }
                }

                // Reduce risk: decrease bid by one if wining
                prData.NumericBid = ReduceRiskIfWinIsInMyPocket(prData.BiddingPosition, prData.NumericBid,
                    spadesBrain.MatchData.Mode, pos,
                    lhoPointsIfBetMet, rhoPointsIfBetMet, prtPointsIfBetMet, partnersPointsIfBidMetBeforeMyBid,
                    partnersBags);

                // Reduce risk: dont nil if wining by a lot
                if (nil && DontNilIfWinIsInMyPocket(prData.BiddingPosition, prData.NumericBid,
                        spadesBrain.MatchData.Mode, pos,
                        lhoPointsIfBetMet, rhoPointsIfBetMet, prtPointsIfBetMet, partnersPointsIfBidMetBeforeMyBid,
                        partnersBags))
                {
                    nil = false;
                }

            }
            // Validations
            prData.NumericBid = bidValidation(pos, prData.BiddingPosition, prData.NumericBid);

            // bid nil or numeric bid
            finalBid = nil ? 0 : prData.NumericBid;
            prData.Bid = finalBid;
            return finalBid;
        }


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public float InitialBidJokers(Position pos, CardsList hand, PlayingMode gameMode)
        {
            int numPlayers = gameMode == PlayingMode.Solo ? 3 : 2;
            List<Position> opponentsPositions = SpadesUtils.OpponentsPositions(pos, spadesBrain.MatchData.Mode);
            foreach (Position position in opponentsPositions)
            {
                if (spadesBrain.Players[position].Bid == 0)
                {
                    numPlayers = gameMode == PlayingMode.Solo ? 2 : 1;
                }
            }

            float initialBid = PotentialBid(hand, numPlayers);
            return initialBid;
        }

        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public override float PotentialBid(CardsList cardsList, int numberOfPlayersThatMightCutYou)
        {
            float result = 0;
            float aceFactor = 0, kingFactor = 0, queenFactor = 0;
            CardsList cards;

            int voids = 0, singletons = 0, doubletons = 0;
            // // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "numberOfPlayersThatMightCutYou ==" + numberOfPlayersThatMightCutYou);

            // Non trump counting
            Card.SuitType[] nonTrump = {Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts};
            foreach (Card.SuitType suit in nonTrump)
            {
                cards = cardsList.CardsBySuits(suit);
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "cards =" + cards);
                int suitLength;
                switch (suit)
                {
                    case Card.SuitType.Spades:
                        suitLength = 15;  // added SJ, BJ
                        break;
                    case Card.SuitType.Clubs:
                        suitLength = 13;
                        break;
                    case Card.SuitType.Diamonds:
                        suitLength = 12; // 2 removed
                        break;
                    case Card.SuitType.Hearts:
                        suitLength = 12; // 2 removed
                        break;
                    default:
                        suitLength = 13;
                        break;
                }

                if (cards.HasRank(Card.RankType.Ace))
                {
                    aceFactor = GetProbTableValueJokers(numberOfPlayersThatMightCutYou, suitLength, cards.Count, Card.RankType.Ace);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "for the ace  =" + aceFactor);
                }
                if (cards.HasRank(Card.RankType.King))
                {
                    kingFactor = GetProbTableValueJokers(numberOfPlayersThatMightCutYou, suitLength, cards.Count, Card.RankType.King);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "for the king =" + kingFactor);
                }
                if (cards.HasRank(Card.RankType.Queen))
                {
                    queenFactor = GetProbTableValueJokers(numberOfPlayersThatMightCutYou, suitLength, cards.Count, Card.RankType.Queen);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "for the queen =" + queenFactor);
                }

                result += aceFactor + kingFactor + queenFactor;
                aceFactor = 0; kingFactor = 0; queenFactor = 0;

                // Count short non-trump suits
                switch (cards.Count)
                {
                    case 0:
                        voids++;
                        break;
                    case 1:
                        singletons++;
                        break;
                    case 2:
                        doubletons++;
                        break;
                }
            }

            // Trump counting
            cards = cardsList.CardsBySuits(Card.SuitType.Spades);
            cards.SortHighToLowSuitFirst();
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "spades cards = " + cards);

            float bid = 0;
            int spareSpades = cards.Count;
            bool countK = false, countQ = false, countJ = false;
            if (cards.HasRank(Card.RankType.JokerBig))
            {
                //$ (big joker)
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "$s = 1");
                bid++;
                spareSpades--;
                if (cards.HasRank(Card.RankType.JokerSmall))
                {
                    // $*
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "*s = 1");
                    bid++;
                    spareSpades--;
                    countK = true;
                    if (cards.HasRank(Card.RankType.Ace))
                    {
                        // $*A
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "As = 1");
                        bid++;
                        spareSpades--;
                        countQ = true;
                        if (cards.HasRank(Card.RankType.King))
                        {
                            // $*AK
                            bid = cards.Count;
                            spareSpades = 0;
                            countJ = true;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "$*AKxx -any spade is a take =" + bid);
                        }
                        else if (spareSpades >= 2)
                        {
                            // $*A_xx
                            bid = cards.Count - 1;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "$*A_xx= " + bid);
                            spareSpades = 0;
                        }
                    }
                    else if (cards.HasRank(Card.RankType.King))
                    {
                        // $*_Kxx
                        bid = cards.Count - 1;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "$*_Kxx= " + bid);
                        spareSpades = 0;
                    }
                }
                else if (cards.HasRank(Card.RankType.Ace))
                {
                    // $_Ax
                    if (cards.Count > 2)
                    {
                        bid = 2;
                        spareSpades -= 2;
                    }
                    if (cards.HasRank(Card.RankType.King))
                    {
                        // $_AKxx
                        bid = cards.Count - 1;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "$_AKxx= " + bid);
                        spareSpades = 0;
                    }

                }
            }
            else if (cards.HasRank(Card.RankType.JokerSmall) && 
                     cards.HasRank(Card.RankType.Ace)&&
                     cards.HasRank(Card.RankType.King))
            {
                // _*AKxx
                bid = cards.Count - 1;
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "_*AKxx" + bid);
                spareSpades = 0;
            }

            else if (cards.HasRank(Card.RankType.JokerSmall) && 
                     cards.HasRank(Card.RankType.Ace))
            {
                // _*Axx
                if (cards.Count == 2)
                {
                    bid = 1;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "_*A" + bid);
                    spareSpades = 0;
                }
                else if (cards.Count == 3 || cards.Count == 4)
                {
                    bid = 2;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "_*Ax/x" + bid);
                    spareSpades = 0;
                }
                else if (cards.Count > 4)
                {
                    bid = cards.Count - 2;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "_*Axxx" + bid);
                    spareSpades = 0;
                }
            }
            else if (cards.Count >= 6 && spareSpades > 0)
            {
                switch (cards.Count)
                {
                    case 6:
                        bid += cards.Count - 4f;
                        break;
                    case 7:
                        bid += cards.Count - 3.5f;
                        break;
                    case 8:
                        bid += cards.Count - 3f;
                        break;
                    case 9:
                        bid += cards.Count - 2.5f;
                        break;
                    case 10:
                        bid += cards.Count - 2.0f;
                        break;
                    case 11:
                        bid += cards.Count - 1.5f;
                        break;
                    case 12:
                        bid += cards.Count - 1.0f;
                        break;
                    case 13:
                        bid += cards.Count - 0.0f;
                        break;
                    default:
                        bid += cards.Count - 4f;
                        break;
                }

                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "6+ spades - " + bid);
                spareSpades = 0;
            }

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "spades factor ==" + bid);
            bool countVoid = false, countSingleton = false, countDoubleton = false;

            while (spareSpades > 0)
            {
                if (voids > 0 && !countVoid)
                {
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "void");

                    if (spareSpades >= 3)
                    {
                        bid += 2.67f;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "more than 2 spare spades - 2.67" + '\n' + "spades factor ==" + bid);
                        spareSpades = spareSpades - 3;
                        countVoid = true;
                    }
                    else if (spareSpades == 2)
                    {
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "2 spare spades - 1.94" + '\n' + "spades factor ==" + bid);
                        bid += 1.94f;
                        spareSpades = spareSpades - 2;
                        countVoid = true;
                    }
                    else if (spareSpades == 1)
                    {
                        bid += 0.99f;
                        spareSpades = spareSpades - 1;
                        countVoid = true;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "1 spare spades - 0.99" + '\n' + "spades factor ==" + bid);
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "spades factor ==" + bid);
                    }
                }
                else if (singletons > 0 && !countSingleton)
                {
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "singletons>0");

                    if (spareSpades >= 2)
                    {
                        bid += 1.5f;
                        spareSpades = spareSpades - 2;
                        countSingleton = true;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "more than 1 spare spades - 1.5" + '\n' + "spades factor ==" + bid);
                    }
                    else if (spareSpades == 1)
                    {
                        bid += 0.915f;
                        spareSpades = spareSpades - 1;
                        countVoid = true;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "1 spare spades - 0.915" + '\n' + "spades factor ==" + bid);
                    }
                }
                else if (cards.HasRank(Card.RankType.JokerSmall) && !countK && spareSpades >= 2)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 2;
                    countK = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "JokerSmall + more than 2 spare spades - 1" + '\n' + "spades factor ==" + bid);
                }
                else if (cards.HasRank(Card.RankType.Ace) && !countQ && spareSpades >= 3)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 3;
                    countQ = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "Ace + more than 3 spare spades - 1" + '\n' + "spades factor ==" + bid);
                }
                else if (cards.HasRank(Card.RankType.King) && !countJ && spareSpades >= 4)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 4;
                    countJ = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "King + more than 4 spare spades - 1" + '\n' + "spades factor ==" + bid);
                }
                else if (doubletons > 0 && !countDoubleton && spareSpades >= 1)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 1;
                    countDoubleton = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "doubletons + more than 1 spare spades - 1" + '\n' + "spades factor ==" + bid);
                }
                else
                {
                    if (spareSpades > 3)
                    {
                        if (spareSpades == 5)
                        {
                            bid += 1.11f;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "5 spare spades - 1.11" + '\n' + "spades factor ==" + bid);
                        }

                        if (spareSpades == 4)
                        {
                            bid += 0.11f;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "4 spare spades - 0.11" + '\n' + "spades factor ==" + bid);
                        }
                    }

                    spareSpades = 0;
                }
            }

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "for all the spades ==" + bid);
            result += bid;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "first result==" + result);
            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        internal static float PartnersNilChancesJokers(CardsList hand)
        {
            Dictionary<Card.SuitType, float> nilChancePerSuit = new Dictionary<Card.SuitType, float>();

            //Dictionary<suit length,card, Value> singletonProb;
            //Dictionary<suit length,card1,card2, Value> doubletonProb;
            //Dictionary<suit length,card1,card2,card3,cards#, Value> threePlusProb;
            Dictionary<string, float> partnersNil1 = m_jokers_nil_1_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> partnersNil2 = m_jokers_nil_2_list.ToDictionary(x => x[0] + x[1] + x[2], x => float.Parse(x[3]));
            Dictionary<string, float> partnersNil3 = m_jokers_nil_3_list.ToDictionary(x => x[0] + x[1] + x[2] + x[3] + x[4], x => float.Parse(x[5]));

            Card.SuitType[] allSuits = {Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts, Card.SuitType.Spades};

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "============================================\n" +
//                               "check partner Nil probability " + "hand == " + hand);

            foreach (Card.SuitType suit in allSuits)
            {
                var cards = hand.CardsBySuits(suit);
                cards.SortHighToLowRankFirst();
                nilChancePerSuit.Add(suit, 0);

                List<int> cardsRanks = new List<int>();
                foreach (Card card in cards)
                    cardsRanks.Add((int) card.Rank);

                int suitLength;
                switch (suit)
                {
                    case Card.SuitType.Spades:
                        suitLength = 15;  // added SJ, BJ
                        break;
                    case Card.SuitType.Clubs:
                        suitLength = 13;
                        break;
                    case Card.SuitType.Diamonds:
                        suitLength = 12; // 2 removed
                        break;
                    case Card.SuitType.Hearts:
                        suitLength = 12; // 2 removed
                        break;
                    default:
                        suitLength = 13;
                        break;
                }
            
                float value;
                switch (cardsRanks.Count)
                {
                    case 0: // Void
                        value = 1.15f;
                        nilChancePerSuit[suit] = value;
                        break;

                    case 1:
                        partnersNil1.TryGetValue(suitLength.ToString() + cardsRanks[0].ToString(), out value);
                        nilChancePerSuit[suit] = value;
                        break;

                    case 2:
                        partnersNil2.TryGetValue(suitLength.ToString() + cardsRanks[1].ToString() + cardsRanks[0].ToString(), out value);
                        nilChancePerSuit[suit] = value;
                        break;

                    case 3:
                        String key3 = suitLength.ToString() + 
                                      cardsRanks[cardsRanks.Count - 1].ToString() + 
                                      cardsRanks[cardsRanks.Count - 2].ToString() + 
                                      cardsRanks[cardsRanks.Count - 3].ToString() +
                                      cards.Count.ToString();
                        partnersNil3.TryGetValue(key3, out value);
                        nilChancePerSuit[suit] = value;
                        break;

                    default:
                        String key4OrMore = suitLength.ToString() + 
                                            cardsRanks[cardsRanks.Count - 1].ToString() +
                                            cardsRanks[cardsRanks.Count - 2].ToString() +
                                            cardsRanks[cardsRanks.Count - 3].ToString() +
                                            cards.Count.ToString();
                        if (suit == Card.SuitType.Spades && cardsRanks.Count >= 4)
                        {
                            value = 0;
                            nilChancePerSuit[suit] = value;
                        }
                        else
                        {
                            partnersNil3.TryGetValue(key4OrMore, out value);
                            nilChancePerSuit[suit] = value;
                        }
                        break;
                }              

                if (suit == Card.SuitType.Spades &&
                    (cardsRanks.Count >= 4 ||
                     hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.JokerBig) ||
                     nilChancePerSuit[Card.SuitType.Spades] <= 0.25) )
                {
                    value = 0;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "spades problem (JokerBig or more than 4 spades) = " + value);
                    nilChancePerSuit[suit] = value;
                }
            }

            float nilChance = 1;
            foreach (KeyValuePair<Card.SuitType, float> kvp in nilChancePerSuit)
            {
                nilChance = nilChance * kvp.Value;
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "{0}: cards = {1}, Value = {2}", kvp.Key, hand.CardsBySuits(kvp.Key), kvp.Value);
            }

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "nil chance = " + nilChance);
            return nilChance;
        }
    }
}
