﻿using System;
using cardGames.models;
using SpadesAI.model;
using SpadesAI.V1.Chain;
using SpadesAI.V4.Chain;

namespace SpadesAI.V4
{
    public class PlayingBrainJokers : PlayingBrainV4
    {
        internal PlayingBrainJokers(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            // Build the chain of responsibilities handlers

            handler               = new PrepareAITurnDataHandlerV4(spadesBrain);    //turn prepare
            
            handler .SetNextHandler(new NilHandlerV4(spadesBrain))                  //Nil (s+p)
                    .SetNextHandler(new ProtectPartnerNilHandlerV4(spadesBrain))    //Cover Nil (p)
                    .SetNextHandler(new BreakOpponentNilHandlerV4(spadesBrain))     //Set Nil (s+p)

                    .SetNextHandler(new LeadHandlerV4(spadesBrain))                 //Lead a card (s+p)

                    .SetNextHandler(new SpadesCutSoloHandlerV4(spadesBrain))        //Cut (s)
                    .SetNextHandler(new SpadesCutPartnersHandlerV4(spadesBrain))    //Cut (p) 

                    .SetNextHandler(new SureWinSoloHandlerV4(spadesBrain))          //Sure win (s)
                    .SetNextHandler(new SureWinPartnersHandler(spadesBrain))        //Sure win (p)

                    .SetNextHandler(new SureLoseSoloHandlerV4(spadesBrain))         //lose (s)

                    .SetNextHandler(new ThirdPosHighPartnersHandlerV4(spadesBrain)) //3rd card high (p)

                    .SetNextHandler(new UnknownHandlerV4(spadesBrain));             //default (s+p)
        }


        public int getMaxRankJokers(Card.SuitType suit)
        {
            switch (suit)
            {
                case Card.SuitType.Spades: return 16;

                case Card.SuitType.Clubs: 
                case Card.SuitType.Diamonds: 
                case Card.SuitType.Hearts: return 14;

                case Card.SuitType.Joker: return 0;
            }
            return 0;
        }
        public int getMinRankJokers(Card.SuitType suit)
        {
            switch (suit)
            {
                case Card.SuitType.Spades: 
                case Card.SuitType.Clubs: return 2;

                case Card.SuitType.Diamonds:
                case Card.SuitType.Hearts: return 3;

                case Card.SuitType.Joker: return 0;
            }
            return 0;
        }

        public int getTotalCardsInSuit(Card.SuitType suit)
        {
            switch (suit)
            {
                case Card.SuitType.Spades: return 15;
                case Card.SuitType.Clubs: return 13;

                case Card.SuitType.Diamonds:
                case Card.SuitType.Hearts: return 12;

                case Card.SuitType.Joker: return 0;
            }
            return 0;
        }

        internal override float CalcCardStrength(Card card, Position playerPosition, SpadesBrainImpl myBrainImpl = null)
        {
            Card[] cardsToCheck = new Card[spadesBrain.CardsOnTable.Count + 1];
            cardsToCheck[0] = card;
            spadesBrain.CardsOnTable.Values.CopyTo(cardsToCheck, 1);

            // Create a card list of throw history + hand
            // Count the holes above and below a specific card
            CardsList historyAndHand = spadesBrain.ThrowHistory.CardsBySuits(card.Suit) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(card.Suit);
            int holesAbove = getMaxRankJokers(card.Suit) - (int)card.Rank - historyAndHand.CountGreaterThan(card.Rank);
            int holesBelow = (int)card.Rank - getMinRankJokers(card.Suit) - historyAndHand.CountLowerThan(card.Rank);

            // Check the cards currently on the table (we might already know this card will lose)
            // This is including the cases in which the card is a non spade card, not matching the leading suit.
            if (card != SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck))
            {
                return 0;
            }

            // When the player is the first to act, check if the card is the lowest remaining card on its suit.
            // Also make sure that not all the other players might are void on the suit.
            if (spadesBrain.CardsOnTable.Count == 0 && holesBelow == 0 && historyAndHand.Count != getTotalCardsInSuit(card.Suit))
            {
                return 0;
            }

            // When the player is the last to act, check if the card will make him a winner.
            // This last player acts with 100% certainty.
            if (spadesBrain.CardsOnTable.Count == 3 && card == SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck))
            {
                // The last player is a sure winner.
                return 1;
            }

            // Check if the card is the highest remaining spade.
            // (This is including the case when the player holds all the remaining spades)
            if (card.Suit == Card.SuitType.Spades && holesAbove == 0)
            {
                return 1;
            }

            // When there are no spades left for the other players, check if the non spade card is the highest on its suit.
            CardsList historyAndHandSpadesOnly = spadesBrain.ThrowHistory.CardsBySuits(Card.SuitType.Spades) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(Card.SuitType.Spades);
            if (historyAndHandSpadesOnly.Count == getTotalCardsInSuit(Card.SuitType.Spades) && holesAbove == 0)
            {
                return 1;
            }

            // Check if the card is a spade card.
            if (card.Suit == Card.SuitType.Spades)
            {
                if (spadesBrain.CardsOnTable.Count == 0 || spadesBrain.LeadingSuit == Card.SuitType.Spades)
                {
                    // The player is first or the leading suit is spades, other suits are not relevant.
                    float numeratorSpades = getMaxRankJokers(card.Suit) - holesAbove;
                    float denominatorSpades = getMaxRankJokers(card.Suit);
                    float strengthSpades = numeratorSpades / denominatorSpades;
                    return strengthSpades;
                }
                else if (spadesBrain.CardsOnTable.Count == 1 || spadesBrain.CardsOnTable.Count == 2)
                {
                    // The leading suit is not spades.
                    // Estimate the ability of the opponents to re-cut with spades.
                    // weak spade cards go down in value because of the risk of a second spade card above it.

                    // Check how many relevant voids are known
                    int leadingSuitVoidOpponentsWithSpadesNum = 0;
                    foreach (Position position in SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count))
                    {
                        // Consider every remaining opponent position.
                        // Make sure the opponent is not void on the spades suits as well.
                        // ReSharper disable once PossibleInvalidOperationException
                        bool isOpponentVoidOnCardSuit = spadesBrain.Players[position].VoidSuits.Contains(spadesBrain.LeadingSuit.Value);
                        bool isOpponentVoidOnSpades = spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades);
                        if (isOpponentVoidOnCardSuit && !isOpponentVoidOnSpades)
                        {
                            leadingSuitVoidOpponentsWithSpadesNum += 1;
                        }
                    }

                    float numeratorSpades = getMaxRankJokers(card.Suit) - holesAbove;
                    float denominatorSpades = getMaxRankJokers(card.Suit) + leadingSuitVoidOpponentsWithSpadesNum * holesAbove;
                    float strengthSpades = numeratorSpades / denominatorSpades;
                    return strengthSpades;
                }
                else
                {
                    throw new Exception("Last position should have been handled before...");
                }
            }
            else // card is not a Spade:
            {
                // Check how many relevant voids are known
                int voidOpponentsWithSpadesNum = 0;
                foreach (Position position in SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count))
                {
                    // Consider every remaining opponent position.
                    // Make sure the opponent is not void on the spades suits as well.
                    bool isOpponentVoidOnCardSuit = spadesBrain.Players[position].VoidSuits.Contains(card.Suit);
                    bool isOpponentVoidOnSpades = spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades);
                    if (isOpponentVoidOnCardSuit && !isOpponentVoidOnSpades)
                    {
                        voidOpponentsWithSpadesNum += 1;
                    }
                }

                // Treat this as a sure take.
                if (voidOpponentsWithSpadesNum == 0)
                {
                    switch (card.Rank)
                    {
                        case Card.RankType.Ace:
                            if (historyAndHand.Count < 9)
                            {
                                return 1;
                            }
                            break;
                        case Card.RankType.King:
                            if (historyAndHand.Count < 9 && holesAbove == 0)
                            {
                                return 1;
                            }
                            break;
                        default:
                            if (historyAndHand.Count < 7 && holesAbove == 0)
                            {
                                return 1;
                            }
                            break;
                    }
                }


                // The non spade card is considered stronger when:
                // 1. There are less unused cards above it (held by the opponents).
                // 2. There are less cards already seen from its suit. seen means it it was either thrown before or is being held by the player.
                // 3. There are less players to act who are void on the suit.
                float numerator = getMaxRankJokers(card.Suit) - holesAbove;
                float denominator = getMaxRankJokers(card.Suit) + historyAndHand.Count + 4 * voidOpponentsWithSpadesNum;
                float strength = numerator / denominator;
                return strength;
            }           
        }

        internal override float CalcCardStrengthAsLeader(Card card, Position playerPosition)
        {
            // Create a card list of throw history + hand
            // Count the holes above and below a specific card
            CardsList historyAndHand = spadesBrain.ThrowHistory.CardsBySuits(card.Suit) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(card.Suit);
            int holesAbove = getMaxRankJokers(card.Suit) - (int)card.Rank - historyAndHand.CountGreaterThan(card.Rank);
            int holesBelow = (int)card.Rank - getMinRankJokers(card.Suit) - historyAndHand.CountLowerThan(card.Rank);

            // When the player is the first to act, check if the card is the lowest remaining card on its suit.
            // Also make sure that not all the other players might are void on the suit.
            if (holesBelow == 0 && historyAndHand.Count != getTotalCardsInSuit(card.Suit))
            {
                return 0;
            }

            // Check if the card is the highest remaining spade.
            // (This is including the case when the player holds all the remaining spades)
            if (card.Suit == Card.SuitType.Spades && holesAbove == 0)
            {
                return 1;
            }

            // When there are no spades left for the other players, check if the non spade card is the highest on its suit.
            CardsList historyAndHandSpadesOnly = spadesBrain.ThrowHistory.CardsBySuits(Card.SuitType.Spades) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(Card.SuitType.Spades);
            if (historyAndHandSpadesOnly.Count == getTotalCardsInSuit(Card.SuitType.Spades) && holesAbove == 0)
            {
                return 1;
            }

            // Check if the card is a spade card.
            if (card.Suit == Card.SuitType.Spades)
            {
                float numeratorSpades = getMaxRankJokers(Card.SuitType.Spades) - holesAbove;
                float denominatorSpades = getMaxRankJokers(Card.SuitType.Spades);
                float strengthSpades = numeratorSpades / denominatorSpades;
                return strengthSpades;
            }


            // The card is not a spade card:


            // Check how many relevant voids are known
            int voidOpponentsWithSpadesNum = 0;
            foreach (Position position in SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode))
            {
                // Consider every remaining opponent position.
                // Make sure the opponent is not void on the spades suits as well.
                bool isOpponentVoidOnCardSuit = spadesBrain.Players[position].VoidSuits.Contains(card.Suit);
                bool isOpponentVoidOnSpades = spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades);
                if (isOpponentVoidOnCardSuit && !isOpponentVoidOnSpades)
                {
                    voidOpponentsWithSpadesNum += 1;
                }
            }

            // Treat this as a sure take.
            if (voidOpponentsWithSpadesNum == 0)
            {
                switch (card.Rank)
                {
                    case Card.RankType.Ace:
                        if (historyAndHand.Count < 9)
                        {
                            return 1;
                        }
                        break;
                    case Card.RankType.King:
                        if (historyAndHand.Count < 9 && holesAbove == 0)
                        {
                            return 1;
                        }
                        break;
                    default:
                        if (historyAndHand.Count < 7 && holesAbove == 0)
                        {
                            return 1;
                        }
                        break;
                }
            }


            // The non spade card is considered stronger when:
            // 1. There are less unused cards above it (held by the opponents).
            // 2. There are less cards already seen from its suit. seen means it it was either thrown before or is being held by the player.
            // 3. There are less players to act who are void on the suit.
            float numerator = getMaxRankJokers(card.Suit) - holesAbove;
            float denominator = getMaxRankJokers(card.Suit) + historyAndHand.Count + 4 * voidOpponentsWithSpadesNum;
            float strength = numerator / denominator;
            return strength;
        }
    }
}
