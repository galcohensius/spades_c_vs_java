﻿using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain {
    public class UnknownHandlerV4: AbstractHandler {

        private const string StrategyFieldName = "UHH";

        public UnknownHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }
        public override void Handle(AITurnData turnData) {

			int strategyRule = 0;

            if (turnData.Takes + turnData.TakesForSureFromSpade >= turnData.Bid)  // We made our bid
            {
				if (turnData.Strategy == AIStrategy.Over)
				{
					// Dump a weak card, as stronger cards might be used later.
					turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 1;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(UHH), {0} duck weak, since we got our bid and Over, contractFulfilled ", turnData.PlayerPos);
                    }
                }
                else // Under
                {
                    // Dump a strongest card, we do not need it anymore
                    turnData.CardToPlay = turnData.CardsToUse.GetStrongCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 2;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(UHH), {0} duck strongest, Under, contractFulfilled", turnData.PlayerPos);
                    }
                }
            }
            else  // still need more takes
            {
                if (turnData.Strategy == AIStrategy.Over)
                {
                    // Dump a weak card, as stronger cards might be used later.
                    turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
					if (turnData.CardToPlay != null) {
						strategyRule = 3;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(UHH), {0} duck weak, Over, !contractFulfilled", turnData.PlayerPos);
                    }
                }
                else // Under
                {
                    // Dump a medium card, as weaker cards might be used later.
                    // A stronger card might be required later, since the bot probably relied on it on the bidding stage.
                    turnData.CardToPlay = turnData.CardsToUse.GetMediumCard() ;

                    if (turnData.CardToPlay != null) {
						strategyRule = 4;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(UHH), {0} medium, Over, !contractFulfilled", turnData.PlayerPos);
                    }
                }
            }
           
            // Final test to make sure a card is selected
			if (turnData.CardToPlay==null) {
				strategyRule = 5;
				turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(UHH) {0}, should not get to here", turnData.PlayerPos);
                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(UHH) Pos: {0}, cards to use: {2}, cards to play {1}, cards on table {4}, leading suit {3}", turnData.PlayerPos, turnData.CardToPlay, turnData.CardsToUse, spadesBrain.LeadingSuit, spadesBrain.CardsOnTable);
            }
			turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
        }
    }
}