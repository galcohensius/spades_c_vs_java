﻿using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain
{

    public class SureWinSoloHandlerV4 : AbstractHandler
    {

        private const string StrategyFieldName = "SWS";

        public SureWinSoloHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override void Handle(AITurnData turnData)
        {

            if (spadesBrain.MatchData.Mode != PlayingMode.Solo)
            {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;

            if (turnData.Strategy == AIStrategy.Over)
            {
                // Try to take with the weakest sure winner card, as stronger cards might be used later.
                turnData.CardToPlay = turnData.CardsToUse.GetWeakestCardByStrength(1);

				if (turnData.CardToPlay != null) {
					strategyRule = 1;
					// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(SWS) {0} tries to take with the weakest winning card", turnData.PlayerPos);
                }

            }
            else if (turnData.Takes + turnData.TakesForSureFromSpade < turnData.Bid)
            {
                // Under strategy but needs more takes
                // Try to take with the strongest sure winner card, as weaker cards might be dumped later.
                turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(1);

				if (turnData.CardToPlay != null) {
					strategyRule = 2;
					// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(SWS) {0} tries to take with the strongest winning card", turnData.PlayerPos);
                }
            }

            if (turnData.CardToPlay == null)
            {
                nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}