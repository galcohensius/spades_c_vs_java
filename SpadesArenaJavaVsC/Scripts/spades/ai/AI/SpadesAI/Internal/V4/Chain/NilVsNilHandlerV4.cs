﻿using SpadesAI.model;
using SpadesAI.Internal;

namespace SpadesAI.V4.Chain
{
    public class NilVsNilHandlerV4 : AbstractHandler
	{
        private const string StrategyFieldName = "NVN";

        /// <summary>
        /// One player from each partnership bid Nil and both of them still didn't toke any trick.
        /// If a niler takes a trick then pass play to ProtectPartnerNilHandlerV4 or BreakOpponentNilHandlerV4.
        /// </summary>
        /// <param name="spadesBrain"></param>
		public NilVsNilHandlerV4(SpadesBrainImpl spadesBrain):base(spadesBrain)
		{
		}
		public override void Handle (AITurnData turnData)
		{
			//if (!turnData.NilVsNil) {
			//	nextHandler.Handle (turnData);
			//	return;
			//}
			int strategyRule = 0;
		    string deck = spadesBrain.PlayingBrain is PlayingBrainJokers ? "jokers" : "regular";

		    Position pos = turnData.PlayerPos;
		    Position partnerPos = SpadesUtils.PartnerPosition(pos);
		    Position rhoPos = SpadesUtils.RhoPosition(pos);
		    Position lhoPos = SpadesUtils.LhoPosition(pos);

                
		    //* if total bids >= 11 consider it an Over round and try to set opp cover

            // position after nilers:
            // leading trick:
            // if (total bids >= 11)
            //     play Over
            // else
            //      consider only non-void suits of oppNil
            //      if partner has a safe suit, play it
            //      else play medium (passing the control to oppCover)

            // playing 4th:
            // if winner is oppNil
            //      duck lowest
            // if winner is partner
            //      cover lowest
            // if winner is oppCover
            //      low/cover, based on Under/Over game

            // position before nilers:
            // leading trick:
            // if (total bids >= 11)
            //     play Over
            // else
            //      consider only non-void suits of oppNil
            //      if partner has a safe suit, play it
            //      else play medium-high
            // 
            // playing 2nd:
            // if partner is safe, play low, else play highest

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
	}
}
