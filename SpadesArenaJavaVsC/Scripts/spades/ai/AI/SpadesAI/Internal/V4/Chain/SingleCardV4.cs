﻿using cardGames.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain{
    public class SingleCard : AbstractHandler{
        private const string StrategyFieldName = "ONE";
        public SingleCard(SpadesBrainImpl spadesBrain) : base(spadesBrain) {}

        public override void Handle(AITurnData turnData)
        {                                        
            CardsList playableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[turnData.PlayerPos].Hand,
                                                                   spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));
            if (playableCards.Count == 1)
            {
                turnData.CardToPlay = playableCards[0];
                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(ONE) {0}, must play this card", turnData.PlayerPos);
            }

            if (turnData.CardToPlay == null)
            {
                nextHandler.Handle(turnData);
            }
            else
            {
                turnData.PlayerRoundData.Strategies += StrategyFieldName + turnData.CardToPlay + ",";
            }
        }
    }
}
