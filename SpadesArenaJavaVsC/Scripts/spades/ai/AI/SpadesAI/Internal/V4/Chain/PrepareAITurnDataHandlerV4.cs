﻿using System;
using SpadesAI.model;
using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using SpadesAI.V1.Chain;
using spades.models;
using Logger = CommonAI.Logger;


namespace SpadesAI.V4.Chain
{
    public class PrepareAITurnDataHandlerV4 : PrepareAITurnDataHandler
    {
        public PrepareAITurnDataHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {}

        public override void Handle(AITurnData turnData)
        {
            turnData.TakesForSureFromSpade = SpadesUtils.CalcTakesForSureFromSpade(turnData.PlayerPos, spadesBrain);

            // before first trick, find short suits and update MatchPointsIfBetMet
            if (spadesBrain.Players[turnData.PlayerPos].Hand.Count == 13)
            {
                FindSingleTonOrDoubleton(turnData, spadesBrain.Players[turnData.PlayerPos]);
                UpdateMatchPointsIfBetMet();
            }

            // opponents' Nil positions 
            turnData.OpponentNilPositions = GetOpponentNilPositions(turnData);

            // Get current player's or partner's bids and takes 
            GetBidAndTakes(turnData);

            // Check for Void suits according to counting all known cards. 
            // If the bot has all the cards left from a specific suit, all other Players are sure voids. 
            GetVoidSuits(turnData);

            // Playable cards. 
            turnData.CardsToUse = spadesBrain.IsVirtual ? spadesBrain.PlayingBrain.CalcPlayableCardsStrength(turnData.PlayerPos, spadesBrain)
                : spadesBrain.PlayingBrain.CalcPlayableCardsStrength(turnData.PlayerPos);

            // Determine the AIStrategy type according to the bid distance. 
            // For partners, requests >= 10  is over 
            // For solo, bid distance of -1 and above is over 
            turnData.Strategy = spadesBrain.MatchData.Mode == PlayingMode.Partners ? ChoosePartnersStrategy(turnData) : ChooseSoloStrategy(turnData);

            // Check if nil. 
            CheckNilAndPartnerNil(turnData);

            // Always move to next handler
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "round {4}, {5}, Prepare Turn successful, {0}, {1}, {2}, {3}, leading suit: {8}, cards on table: {9}, cards to use: {7}, cards to play: {6}", spadesBrain.PlayingBrain, spadesBrain.MatchData.Mode, spadesBrain.MatchData.Variation, spadesBrain.MatchData.SubVariation, spadesBrain.MatchData.RoundNum, turnData.PlayerPos, turnData.CardToPlay, turnData.CardsToUse.ToStringCardsOnly(), spadesBrain.LeadingSuit, string.Join(";", spadesBrain.CardsOnTable.Select(x => x.Value)));
            nextHandler.Handle(turnData);
        }
        // ----------------------------------------------------------------------------- //


        private void CheckNilAndPartnerNil(AITurnData turnData)
        {
            if (spadesBrain.Players[turnData.PlayerPos].Bid == 0 && spadesBrain.Players[turnData.PlayerPos].Takes == 0)
            {
                turnData.ProtectSelfNil = true;
            }

            // Check if cover nil.
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);
                if (spadesBrain.Players[partnerPos].Bid == 0 && spadesBrain.Players[partnerPos].Takes == 0)
                {
                    turnData.ProtectPartnerNil = true;
                }
            }
        }

        private void GetVoidSuits(AITurnData turnData)
        {
            foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType)))
            {
                int knownNumOfCards = spadesBrain.ThrowHistory.CardsBySuits(suit).Count;
                foreach (Card cardOnTable in spadesBrain.CardsOnTable.Values)
                {
                    if (cardOnTable.Suit == suit)
                        knownNumOfCards++;
                }

                if (knownNumOfCards + spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(suit).Count == 13)
                {
                    // Everyone else is void
                    // Use OpponentsPositions with Solo to get all other positions except the current bot
                    foreach (Position otherPos in SpadesUtils.OpponentsPositions(turnData.PlayerPos, PlayingMode.Solo))
                    {
                        spadesBrain.Players[otherPos].VoidSuits.Add(suit);
                    }

                    Logger.DebugFormat("All Players except {0} are void on {1}", turnData.PlayerPos, suit);
                }
            }
        }

        private void GetBidAndTakes(AITurnData turnData)
        {
            turnData.Bid = spadesBrain.Players[turnData.PlayerPos].Bid;
            turnData.Takes = spadesBrain.Players[turnData.PlayerPos].Takes;
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);
                turnData.Bid += spadesBrain.Players[partnerPos].Bid;
                turnData.Takes += spadesBrain.Players[partnerPos].Bid == 0 ? 0 : spadesBrain.Players[partnerPos].Takes;
            }
        }

        private List<Position> GetOpponentNilPositions(AITurnData turnData)
        {
            List<Position> opponentsPositions = SpadesUtils.OpponentsPositions(turnData.PlayerPos, spadesBrain.MatchData.Mode);
            List<Position> opponentNilPositions = new List<Position>();
            foreach (Position pos in opponentsPositions)
            {
                if (spadesBrain.Players[pos].Bid == 0 && spadesBrain.Players[pos].Takes == 0)
                {
                    opponentNilPositions.Add(pos);
                }
            }

            return opponentNilPositions;
        }

        private void UpdateMatchPointsIfBetMet()
        {
            if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                foreach (Position p in (Position[])Enum.GetValues(typeof(Position)))
                {
                    spadesBrain.MatchData.MatchPointsIfBetMet[p] = spadesBrain.MatchData.MatchPoints[p]
                                                                   + SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[p].Bid, spadesBrain.Players[p].BlindNil);
                }
            }
            else if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                spadesBrain.MatchData.MatchPointsIfBetMet[Position.East] =
                    spadesBrain.MatchData.MatchPoints[Position.East] + spadesBrain.MatchData.MatchPoints[Position.West] +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[Position.East].Bid, spadesBrain.Players[Position.East].BlindNil) +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[Position.West].Bid, spadesBrain.Players[Position.West].BlindNil);

                spadesBrain.MatchData.MatchPointsIfBetMet[Position.North] =
                    spadesBrain.MatchData.MatchPoints[Position.North] + spadesBrain.MatchData.MatchPoints[Position.South] +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[Position.North].Bid, spadesBrain.Players[Position.North].BlindNil) +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[Position.South].Bid, spadesBrain.Players[Position.South].BlindNil);
            }
        }

        private void FindSingleTonOrDoubleton(AITurnData turnData, AIPlayer player)
        {
            Dictionary<Card.SuitType, CardsList> suitCountDictionary = new Dictionary<Card.SuitType, CardsList>();
            foreach (Card card in spadesBrain.Players[turnData.PlayerPos].Hand)
            {
                if (card.Suit == Card.SuitType.Spades) continue; // Skip spades
                suitCountDictionary.TryGetValue(card.Suit, out CardsList cardsList);
                if (cardsList == null)
                {
                    suitCountDictionary[card.Suit] = new CardsList();
                }

                suitCountDictionary[card.Suit].Add(card);
            }

            int shortSuitCardCount = suitCountDictionary.Min(kvp => kvp.Value.Count);
            if (shortSuitCardCount < 3 && shortSuitCardCount > 0)
            {
                player.IsSingletonOrDoubleton = true;
                player.SingletonOrDoubleton = suitCountDictionary.Where(kvp => kvp.Value.Count == shortSuitCardCount).Select(kvp => kvp.Key).First();
            }
            else
            {
                player.IsSingletonOrDoubleton = false;
            }
        }

        // ----  Partners:  ----/////////////////////////////////////////////////////////////////////////////
        public AIStrategy ChoosePartnersStrategy(AITurnData turnData)
        {
            Position pos = turnData.PlayerPos;
            Position partnerPos = SpadesUtils.PartnerPosition(pos);
            int myPartnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;
            int myPartnerTakes = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Takes;
            int myBid = spadesBrain.Players[pos].Bid;
            int myTakes = spadesBrain.Players[pos].Takes;
            AIStrategy roundStrategy;

            int opponentsTotalBids = 0;
            int counter = 0;
            int opponent1Bid = 0;
            int opponent1Takes = 0;
            int opponent2Bid = 0;
            int opponent2Takes = 0;

            bool opponentsContractFulfilled = false;

            foreach (Position position in SpadesUtils.OpponentsPositions(pos, PlayingMode.Partners))
            {
                if (spadesBrain.Players[position].Bid > 0)
                    opponentsTotalBids += spadesBrain.Players[position].Bid;
                if (counter == 0)
                {
                    opponent1Bid = spadesBrain.Players[position].Bid;
                    opponent1Takes = spadesBrain.Players[position].Takes;
                }
                else
                {
                    opponent2Bid = spadesBrain.Players[position].Bid;
                    opponent2Takes = spadesBrain.Players[position].Takes;
                }
                counter++;
            }

            int partnersTotalTakes = 0;
            partnersTotalTakes += myBid == 0 ? 0 : myTakes;
            partnersTotalTakes += myPartnerBid == 0 ? 0 : myPartnerTakes;
            int partnersTotalTakesNiler = 0;
            partnersTotalTakesNiler += myBid == 0 ? myTakes : 0;
            partnersTotalTakesNiler += myPartnerBid == 0 ? myPartnerTakes : 0;

            int opponentsTotalTakes = 0;
            opponentsTotalTakes += opponent1Bid == 0 ? 0 : opponent1Takes;
            opponentsTotalTakes += opponent2Bid == 0 ? 0 : opponent2Takes;
            int opponentsTotalTakesNiler = 0;
            opponentsTotalTakesNiler += opponent1Bid == 0 ? opponent1Takes : 0;
            opponentsTotalTakesNiler += opponent2Bid == 0 ? opponent2Takes : 0;

            if (partnersTotalTakes + SpadesUtils.CalcTakesForSureFromSpade(pos, spadesBrain) >= myPartnerBid + myBid)
            {
                spadesBrain.Players[pos].PartnersContractFulfilled = true;
            }
            if (opponentsTotalTakes >= opponent1Bid + opponent2Bid)
            {
                opponentsContractFulfilled = true;
            }

            int partnersTotalBids = myBid + myPartnerBid;

            int totalBids = partnersTotalBids + opponentsTotalBids;
            turnData.TotalBids = totalBids;

            int partnersMissingTakes = partnersTotalBids - partnersTotalTakes;
            int opponentsMissingTakes = opponentsTotalBids - opponentsTotalTakes;

            int tricksLeft = spadesBrain.Players[turnData.PlayerPos].Hand.Count;
            int partnersOverTakes = (partnersTotalTakes > partnersTotalBids ? partnersTotalTakes - partnersTotalBids : 0);
            int opponentsOverTakes = (opponentsTotalTakes > opponentsTotalBids ? opponentsTotalTakes - opponentsTotalBids : 0);
            turnData.TotalOverTakes = partnersOverTakes + opponentsOverTakes;

            // I dont like under over, prefer: requests
            int requests = totalBids + partnersOverTakes + opponentsOverTakes + partnersTotalTakesNiler + opponentsTotalTakesNiler;
            int underOver = requests - 13;
            turnData.BidDistance = underOver;


            int rule;  // set strategy to Over/Under
            if (requests < 11)
            {
                roundStrategy = AIStrategy.Under;
                rule = 1;
            }
            else // if (requests >= 11)
            {
                if (spadesBrain.Players[pos].PartnersContractFulfilled)
                {
                    if (opponentsContractFulfilled)
                    {
                        roundStrategy = AIStrategy.Under;
                        rule = 2;
                    }
                    else // ! opponentsContractFulfilled
                    {
                        if (opponentsMissingTakes > tricksLeft)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "we made our contract, opponents set, avoid bags: Under");
                            roundStrategy = AIStrategy.Under;
                            rule = 6;
                        }
                        else
                        {
                            roundStrategy = AIStrategy.Over;
                            rule = 3;
                        }
                    }
                }
                else if (partnersMissingTakes > tricksLeft) // we lost, give bags
                {
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "we lost, give bags: Under");
                    roundStrategy = AIStrategy.Under;
                    rule = 4;
                }
                else // !partnersContractFulfilled
                {
                    roundStrategy = AIStrategy.Over;
                    rule = 5;
                }
            }
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "position:" + pos + "requests = " + requests + ", sure takes = " + SpadesUtils.CalcTakesForSureFromSpade(pos, spadesBrain) + ", strategy = " + roundStrategy + ", rule  = " + rule + '\n' +
//                               "partners Contract Fulfilled==" + spadesBrain.Players[pos].PartnersContractFulfilled + ", opponents Contract Fulfilled==" + opponentsContractFulfilled);

            if (roundStrategy == AIStrategy.Over) turnData.PlayerRoundData.Strategies += "O";
            else turnData.PlayerRoundData.Strategies += "U";
            turnData.PlayerRoundData.Strategies += rule;

            //Updates first non boss lead suit
            if (spadesBrain.CardsOnTable.Count == 2 && !spadesBrain.Players[turnData.PlayerPos].IsPartnerLeadNonBoss) //partner is leading non-boss for the first time
            {
                // if smaller than boss, it is a tell to partner to lead again this suit
                Card partnerCard = spadesBrain.CardsOnTable[partnerPos];
                Card bossCard = SpadesUtils.CalcBoss(partnerCard.Suit, spadesBrain);

                if (bossCard != null && partnerCard.Rank < bossCard.Rank)
                {
                    spadesBrain.Players[turnData.PlayerPos].IsPartnerLeadNonBoss = true;
                    spadesBrain.Players[turnData.PlayerPos].PartnersFirstLeadSuit = partnerCard.Suit;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} remember partner's first non-boss lead suit {1}", turnData.PlayerPos, partnerCard.Suit);
                }
            }

            // if you or partner leads a boss, it a signal to stop trying to set opponent Nil. (BON) 
            if (turnData.OpponentNilPositions != null && turnData.OpponentNilPositions.Count == 1 && spadesBrain.CardsOnTable.Count == 2)
            {
                // ReSharper disable once PossibleInvalidOperationException (since spadesBrain.CardsOnTable.Count == 2)
                Card bossCard = SpadesUtils.CalcBoss((Card.SuitType) spadesBrain.LeadingSuit, spadesBrain);
                if (bossCard != null)
                {
                    Card firstCardOfTrick = spadesBrain.CardsOnTable[AITurnData.LeadingPlayer];
                    if (firstCardOfTrick == bossCard && bossCard.Suit != Card.SuitType.Spades)
                    {
                        spadesBrain.Players[pos].StopToBreakOpponentNil = true;
                        spadesBrain.Players[partnerPos].StopToBreakOpponentNil = true;
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} partner lead side suit Boss, which signals to stop trying to set Nil", turnData.PlayerPos);
                    }
                }
            }
            return roundStrategy;
        }


        // ----  Solo:  ----- /////////////////////////////////////////////////////////////////////////////
        public AIStrategy ChooseSoloStrategy(AITurnData turnData)
        {
            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;
            Position pos = turnData.PlayerPos;
            AIStrategy roundStrategy;
            int myBid = spadesBrain.Players[pos].Bid;
            int myTakes = spadesBrain.Players[pos].Takes;
            int myBags = spadesBrain.MatchData.MatchBags[pos];

            int myPoints = spadesBrain.MatchData.MatchPoints[pos];
            int rhoPoints = spadesBrain.MatchData.MatchPoints[SpadesUtils.RhoPosition(pos)];
            int lhoPoints = spadesBrain.MatchData.MatchPoints[SpadesUtils.LhoPosition(pos)];
            int mhoPoints = spadesBrain.MatchData.MatchPoints[SpadesUtils.PartnerPosition(pos)];
            List<int> points = new List<int> { myPoints, rhoPoints, lhoPoints, mhoPoints };


            int player1Bid = -1;
            int player1Takes = 0;
            int player1MissingTakes = 0;

            int player2Bid = -1;
            int player2Takes = 0;
            int player2MissingTakes = 0;

            int player3Bid = -1;
            int player3Takes = 0;
            int player3MissingTakes = 0;

            int myDistanceFromBagsPenalty = bagsToPenalty - myBags;

            int rule;

            int counter = 0;
            foreach (Position opponentPos in SpadesUtils.OpponentsPositions(pos, PlayingMode.Solo))
            {
                if (spadesBrain.Players[opponentPos].Bid > -1)
                {
                    switch (counter)
                    {
                        case 0:
                            player1Bid = spadesBrain.Players[opponentPos].Bid;
                            player1Takes = spadesBrain.Players[opponentPos].Takes;
                            player1MissingTakes = player1Bid > 0 ? player1Bid - player1Takes : 0;
                            break;

                        case 1:
                            player2Bid = spadesBrain.Players[opponentPos].Bid;
                            player2Takes = spadesBrain.Players[opponentPos].Takes;
                            player2MissingTakes = player2Bid > 0 ? player2Bid - player2Takes : 0;
                            break;

                        case 2:
                            player3Bid = spadesBrain.Players[opponentPos].Bid;
                            player3Takes = spadesBrain.Players[opponentPos].Takes;
                            player3MissingTakes = player3Bid > 0 ? player3Bid - player3Takes : 0;
                            break;
                    }
                    counter++;
                }
            }

            int totalBids = myBid + player1Bid + player2Bid + player3Bid;
            turnData.TotalBids = totalBids;

            int tricksLeft = spadesBrain.Players[turnData.PlayerPos].Hand.Count;
            int myOverTakes = myTakes - myBid > 0 ? myTakes - myBid : 0;
            int player1OverTakes = player1Takes - player1Bid > 0 ? player1Takes - player1Bid : 0;
            int player2OverTakes = player2Takes - player2Bid > 0 ? player2Takes - player2Bid : 0;
            int player3OverTakes = player3Takes - player3Bid > 0 ? player3Takes - player3Bid : 0;

            turnData.TotalOverTakes = myOverTakes + player1OverTakes + player2OverTakes + player3OverTakes;

            if (myTakes + SpadesUtils.CalcTakesForSureFromSpade(pos, spadesBrain) >= myBid && myBid > 0)
                spadesBrain.Players[pos].MyContractFulfilled = true;

            // I dont like under over, prefer: requests (that sums up to ~13)
            int requests = totalBids + myOverTakes + player1OverTakes + player2OverTakes + player3OverTakes;
            int opponentsMissingTakesSolo = player1MissingTakes + player2MissingTakes + player3MissingTakes;
            int myMissingTakes = myBid > 0 ? myBid - myTakes : 0;
            int underOver = totalBids + myOverTakes + player1OverTakes + player2OverTakes + player3OverTakes - 13;
            turnData.BidDistance = underOver;

            {
                if (requests < 11) // will go to over once someone gets overtakes
                {
                    roundStrategy = AIStrategy.Under;
                    rule = 1;
                }
                else // request >= 11
                {
                    if (spadesBrain.Players[pos].MyContractFulfilled)
                    {
                        if (myDistanceFromBagsPenalty <= 2)
                        {
                            roundStrategy = AIStrategy.Under;
                            rule = 2;
                        }
                        else
                        {
                            roundStrategy = AIStrategy.Over;
                            rule = 3;
                        }
                    }
                    else // !myContractFulfilled
                    {
                        if (tricksLeft < myMissingTakes)  // I lost for sure
                        {
                            if (tricksLeft < opponentsMissingTakesSolo)
                            {
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "I lost for sure, try to set someone else as well");
                                roundStrategy = AIStrategy.Over;
                                rule = 4;
                            }
                            else  // tricksLeft > opponentsMissingTakesSolo
                            {
                                roundStrategy = AIStrategy.Under; // bag them
                                rule = 5;
                            }
                        }
                        else
                        {
                            roundStrategy = AIStrategy.Over;
                            rule = 6;
                        }
                    }
                }
            }
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "position:" + pos + "Bid = " + myBid + ", Takes = " + myTakes + ", Sure takes = " + turnData.TakesForSureFromSpade + ", Bags = " + myBags + ", Points = " + myPoints + '\n' +
//                               ", tricks left = " + tricksLeft + ", underOver = " + underOver + ", rule = " + rule);

            // Stop trying to set niler if: 
            // non-niler leads a boss (its a signal to stop)
            // niler is a Bot with negative score || behind the player with most points by more than 60
            if (turnData.OpponentNilPositions != null)
            {
                if (turnData.OpponentNilPositions.Count == 1 && spadesBrain.CardsOnTable.Count > 0)
                {
                    if (SpadesUtils.CalcBoss(spadesBrain.CardsOnTable[AITurnData.LeadingPlayer].Suit, spadesBrain) != null)
                    {
                        Card bossCard = SpadesUtils.CalcBoss(spadesBrain.CardsOnTable[AITurnData.LeadingPlayer].Suit, spadesBrain);
                        Card firstCardOfTrick = spadesBrain.CardsOnTable[AITurnData.LeadingPlayer];
                        int nilersPoints = spadesBrain.MatchData.MatchPoints[turnData.OpponentNilPositions[0]];
                        int maximalPoints = points.Max();

                        if (firstCardOfTrick == bossCard && bossCard.Suit != Card.SuitType.Spades)
                        {
                            spadesBrain.Players[Position.South].StopToBreakOpponentNil = true;
                            spadesBrain.Players[Position.East].StopToBreakOpponentNil = true;
                            spadesBrain.Players[Position.North].StopToBreakOpponentNil = true;
                            spadesBrain.Players[Position.West].StopToBreakOpponentNil = true;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} non niler lead side suit Boss, which signals to stop trying to break Nil", turnData.PlayerPos);
                        }
                        else if (turnData.OpponentNilPositions[0] != Position.South && nilersPoints < maximalPoints - 60)
                        {
                            spadesBrain.Players[Position.South].StopToBreakOpponentNil = true;
                            spadesBrain.Players[Position.East].StopToBreakOpponentNil = true;
                            spadesBrain.Players[Position.North].StopToBreakOpponentNil = true;
                            spadesBrain.Players[Position.West].StopToBreakOpponentNil = true;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "niler  is behind by more than 60, dont try to set Nil");
                        }
                    }
                }
            }

            if (roundStrategy == AIStrategy.Over)
            {
                turnData.PlayerRoundData.Strategies += "O";
            }
            else
            {
                turnData.PlayerRoundData.Strategies += "U";
            }
            turnData.PlayerRoundData.Strategies += rule;
            return roundStrategy;
        }
    }
}
