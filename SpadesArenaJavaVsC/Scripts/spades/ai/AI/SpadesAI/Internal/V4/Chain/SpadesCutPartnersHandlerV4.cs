﻿using cardGames.models;
using CommonAI;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain
{
    public class SpadesCutPartnersHandlerV4 : AbstractHandler {

        private const string StrategyFieldName = "SCP";

        public SpadesCutPartnersHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {
            if (spadesBrain.MatchData.Mode != PlayingMode.Partners) {
                nextHandler.Handle(turnData);
                return;
            }


            // The player can cut with spades when:
            // 1. He is not the first to act.
            // 2. The leading suit is not spades.
            // 3. The player has a spade in the playable cards.
            if (spadesBrain.CardsOnTable.Count == 0 || spadesBrain.LeadingSuit == Card.SuitType.Spades || turnData.CardsToUse.CountSuit(Card.SuitType.Spades) == 0)
            {
                nextHandler.Handle(turnData);
                return;
            }

            // If partner already played, and his card is going to win, do not cut partner
            bool partnerAboutToWin = SpadesUtils.IsPartnerAboutToWin(turnData.PlayerPos, spadesBrain);
            if (partnerAboutToWin)
            {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;

            // Cut to win.
            // Use the weakest card which can win the trick with a good probability.
            AIStrengthCardsList nonLosingSpadeCards = turnData.CardsToUse.GetCardsWithinStrengthRange(0.0001f, 1);
            if (nonLosingSpadeCards.Count > 0)
            {
                int leadingSuitCardsThrown = spadesBrain.ThrowHistory.CountSuit(spadesBrain.LeadingSuit);
                
                if (leadingSuitCardsThrown <= 8 && !SpadesUtils.IsAnyOpponentVoidOnSuitAndNotInSpadesAndNotNil(turnData.PlayerPos, spadesBrain, (Card.SuitType)spadesBrain.LeadingSuit))
                {
                    // Since there is a low probability for someone to cut above, use a small card.
                    turnData.CardToPlay = nonLosingSpadeCards.GetWeakCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 1;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(SCP) {0} cutting with weak spade, 8 or less {1} cards used", turnData.PlayerPos,spadesBrain.LeadingSuit);
                    }
                }
				else
				{
                    // if LHO and partner can cut after you, don't cut and let partner to cut last
				    if (SpadesUtils.IsAnyOpponentVoidOnSuitAndNotInSpadesAndNotNil(turnData.PlayerPos, spadesBrain, (Card.SuitType)spadesBrain.LeadingSuit) && 
				        SpadesUtils.IsPartnerVoidOnSuitAndNotInSpadesAndNotNil(turnData.PlayerPos, spadesBrain, (Card.SuitType)spadesBrain.LeadingSuit) )
				    {
				        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(SCP) {0} LHO and partner can cut after you, don't cut and let partner to cut last", turnData.PlayerPos);
                    }
				    else
				    {
				        // Since there is a higher probability for someone to cut above, use a medium card.
				        turnData.CardToPlay = nonLosingSpadeCards.GetMediumCard();

				        if (turnData.CardToPlay != null)
				        {
				            strategyRule = 2;
				            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(SCP) {0} cutting with medium spade, more than 8 {1} cards used", turnData.PlayerPos, spadesBrain.LeadingSuit);
				        }
                    }
                }
            }

			if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            }else
            {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}
