﻿using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain {
    public class LeadHandlerV4:AbstractHandler {

        private const string StrategyFieldName = "LED";

        public LeadHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }
        public override void Handle(AITurnData turnData) {
			int strategyRule = 0;

            if (spadesBrain.CardsOnTable.Count == 0)
            {
                // Lead: (from suits that no opponent is void)
                // 1. side suit singleton {A}
                // 2. side suit doubleton {A,K}
                // 3. (p) low from partner void sidesuit, but RHO isn't void
                // 4.1(p) lead second spade if partner lead spade (overall 2 tricks of spades) // TODO
                // 4.2(p) lead spades if have most (5+ / prt bid >=6 / bid+prt bid >=8)
                // 5. (p) lead what partner lead with non boss, but RHO isn't void
                // 6. your short side suit 
                // 7. low from side suit with no Ace
                // 8. Ace from side suit with Ace

                Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);
                Position rho = SpadesUtils.RhoPosition(turnData.PlayerPos);
                Position lho = SpadesUtils.LhoPosition(turnData.PlayerPos);
                CardsList myHand = spadesBrain.Players[turnData.PlayerPos].Hand;

                Dictionary<Card.SuitType, CardsList> sideSuitCardsListDictionary = GetSideSuitCardsListDictionary(turnData);
                // remove suits that:
                // in Partners: RHO void, LhO void & partner !void
                // in Solo: all voids
                RemoveOppVoids(sideSuitCardsListDictionary, rho, lho, partnerPos);

                // 1. side Singleton {A}
                List<Card> SingletonAce = GetSingletonAce(sideSuitCardsListDictionary);
                if (SingletonAce.Count > 0)
                {
                    turnData.CardToPlay = SingletonAce[0];
                    strategyRule = 1;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} singleton Ace", turnData.PlayerPos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

                // 2. side Doubleton {A,K}
                List<Card> aceKingDoubleton = GetAceKingDoubletonSuits(sideSuitCardsListDictionary);
                if (aceKingDoubleton.Count > 0)
                {
                    turnData.CardToPlay = aceKingDoubleton[0];
                    strategyRule = 2;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} doubleton of A,K", turnData.PlayerPos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

                // 3,4,5 only in Partners
                if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
                {

                    // 3. low from partner void + partner isn't void in spades + RHO isn't void in sidesuit
                    List<Card.SuitType> prtVoidRhoNot = GetPrtVoidRhoNot(partnerPos, rho);
                    if (prtVoidRhoNot.Count > 0 && myHand.CardsBySuits(prtVoidRhoNot).Count > 0)
                    { 
                        turnData.CardToPlay = myHand.CardsBySuits(prtVoidRhoNot)[myHand.CardsBySuits(prtVoidRhoNot).Count - 1];
                        strategyRule = 3;
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} low from partner void sidesuit, but RHO isn't void", turnData.PlayerPos);
                        turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                        return;
                    }

                    // 5. lead what partner lead with non boss, but RHO isn't void
                    List<Card.SuitType> rhoVoidsList = spadesBrain.Players[rho].VoidSuits.ToList();
                    if (spadesBrain.Players[turnData.PlayerPos].IsPartnerLeadNonBoss)
                    {
                        Card.SuitType prtFirstLeadSuit = spadesBrain.Players[turnData.PlayerPos].PartnersFirstLeadSuit;
                        if (!rhoVoidsList.Contains(prtFirstLeadSuit))
                        {
                            CardsList prtLeadSuitRhoNotVoid = turnData.CardsToUse.CardsBySuits(prtFirstLeadSuit);
                            if (prtLeadSuitRhoNotVoid.Count > 0)
                            {
                                prtLeadSuitRhoNotVoid.SortHighToLowRankFirst();
                                turnData.CardToPlay = prtLeadSuitRhoNotVoid[prtLeadSuitRhoNotVoid.Count - 1];
                                if (turnData.CardToPlay != null)
                                {
                                    strategyRule = 4;
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} top from partners' first non boss lead suit, where RHO isn't void", turnData.PlayerPos);
                                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                                    return;
                                }
                            }
                        }
                    }
                    
                }

                // 6. short suit
                if (spadesBrain.Players[turnData.PlayerPos].IsSingletonOrDoubleton)
                {
                    CardsList shortSuit = spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(spadesBrain.Players[turnData.PlayerPos].SingletonOrDoubleton);
                    if (shortSuit.Count > 0) // still have cards in short suit
                    {
                        shortSuit.SortHighToLowRankFirst();
                        turnData.CardToPlay = shortSuit.First(); // High to low doubleton
                        if (turnData.CardToPlay != null)
                        {
                            strategyRule = 5;
                            // if smaller than boss, it is a tell to partner to lead again this suit
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} top from short-suit (High to low doubleton) {1} ", turnData.PlayerPos, turnData.CardToPlay.Suit);
                            turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                            return;
                        }
                    }
                }
                // 7. Low with no Ace
                CardsList suitWithNoAce = GetSuitWithNoAce(sideSuitCardsListDictionary);
                if (suitWithNoAce.Count > 0)
                {
                    turnData.CardToPlay = suitWithNoAce.GetWeakest();
                    strategyRule = 6;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} low from suit with no Ace", turnData.PlayerPos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

                // 8. lead Ace
                Card.SuitType? suitWithAce = GetSuitWithAce(sideSuitCardsListDictionary);
                if (suitWithAce != null)
                {
                    turnData.CardToPlay = sideSuitCardsListDictionary[(Card.SuitType) suitWithAce].GetStrongestBelowOrEqualRank(Card.RankType.Ace);
                    strategyRule = 7;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Lead) {0} Ace since all side suits have an Ace and never underlead an Ace", turnData.PlayerPos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

            }
            if (turnData.CardToPlay == null) {
                nextHandler.Handle (turnData);
            } else {
			    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }

        private List<Card.SuitType> GetPrtVoidRhoNot(Position partnerPos, Position rho)
        {
            List<Card.SuitType> suitToPlay = new List<Card.SuitType>();
            if (!spadesBrain.Players[partnerPos].VoidSuits.Contains(Card.SuitType.Spades))
            {
                foreach (var prtVoidSuit in spadesBrain.Players[partnerPos].VoidSuits)
                {
                    if (!spadesBrain.Players[rho].VoidSuits.Contains(prtVoidSuit) ||
                        spadesBrain.Players[rho].VoidSuits.Contains(Card.SuitType.Spades))
                    {
                        suitToPlay.Add(prtVoidSuit);
                    }
                }
            }
            return suitToPlay;
        }

        private void RemoveOppVoids(Dictionary<Card.SuitType, CardsList> sideSuitCardsListDictionary, Position rho, Position lho, Position partnerPos)
        {
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                if (!spadesBrain.Players[rho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[rho].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }

                if (!spadesBrain.Players[lho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[lho].VoidSuits)
                    {
                        if (!spadesBrain.Players[partnerPos].VoidSuits.Contains(badSuit) || spadesBrain.Players[partnerPos].VoidSuits.Contains(Card.SuitType.Spades))
                        {
                            sideSuitCardsListDictionary.Remove(badSuit);
                        }
                    }
                }
            }
            else if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                if (!spadesBrain.Players[rho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[rho].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }

                if (!spadesBrain.Players[lho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[lho].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }

                if (!spadesBrain.Players[partnerPos].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[partnerPos].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }
            }
        }


        private Dictionary<Card.SuitType, CardsList> GetSideSuitCardsListDictionary(AITurnData turnData)
        {
            Dictionary<Card.SuitType, CardsList> suitCountDictionary = new Dictionary<Card.SuitType, CardsList>();
            foreach (Card card in spadesBrain.Players[turnData.PlayerPos].Hand)
            {
                if (card.Suit == Card.SuitType.Spades) continue; // Skip spades
                suitCountDictionary.TryGetValue(card.Suit, out CardsList cardsList);
                if (cardsList == null)
                {
                    suitCountDictionary[card.Suit] = new CardsList();
                }
                suitCountDictionary[card.Suit].Add(card);
            }
            return suitCountDictionary;
        }


        private static List<Card> GetSingletonAce(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            return suitCountDictionary
                .Where(pair => pair.Value.Count == 1 
                               && pair.Value[0].Rank == Card.RankType.Ace)
                .Select(k => k.Value[0]).ToList();
        }


        private static List<Card> GetAceKingDoubletonSuits(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            return suitCountDictionary
                .Where(pair => pair.Value.Count == 2 
                               && pair.Value[0].Rank == Card.RankType.Ace 
                               && pair.Value[1].Rank == Card.RankType.King)
                .Select(k => k.Value[1]).ToList();
        }


        private static CardsList GetSuitWithNoAce(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            List<Card> withNoAce = suitCountDictionary.Where(pair => pair.Value[0].Rank != Card.RankType.Ace).Select(k => k.Value.Last()).ToList();
            CardsList withNoAceCardsList = new CardsList();
            foreach (Card card in withNoAce)
            {
                withNoAceCardsList.Add(card);
            }

            return withNoAceCardsList;
        }


        private static Card.SuitType? GetSuitWithAce(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            if (suitCountDictionary.ContainsKey(Card.SuitType.Clubs)
                && suitCountDictionary[Card.SuitType.Clubs][0].Rank == Card.RankType.Ace) return Card.SuitType.Clubs;
            if (suitCountDictionary.ContainsKey(Card.SuitType.Diamonds)
                && suitCountDictionary[Card.SuitType.Clubs][0].Rank == Card.RankType.Ace) return Card.SuitType.Diamonds;
            if (suitCountDictionary.ContainsKey(Card.SuitType.Hearts)
                && suitCountDictionary[Card.SuitType.Clubs][0].Rank == Card.RankType.Ace) return Card.SuitType.Hearts;

            return null;
        }
    }
}
