﻿using System.Linq;
using cardGames.models;
using spades.models;
using spades.utils;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain
{
    public class ThirdPosHighPartnersHandlerV4 : AbstractHandler {

        private const string StrategyFieldName = "3rd";

        public ThirdPosHighPartnersHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData)
        {
            if (spadesBrain.MatchData.Mode != PlayingMode.Partners) {
                nextHandler.Handle(turnData);
                return;
            }
            // In Bridge, there is an axiom: “play low in the second seat – play high in the third seat.
            // 3rd must play high, otherwise 4th unnecessarily wins a cheap trick.
            int strategyRule = 0;
            string deck = spadesBrain.PlayingBrain is PlayingBrainJokers ? "jokers" : "regular";

            if (spadesBrain.CardsOnTable.Count == 2 && turnData.Strategy == AIStrategy.Over)
            {
                if (spadesBrain.CardsOnTable.First().Value.Suit == spadesBrain.CardsOnTable.Last().Value.Suit)  // second player didn't cut
                {
                    Card.SuitType leadingSuit = spadesBrain.CardsOnTable.First().Value.Suit;
                    Card bossCard = SpadesUtils.CalcBoss(leadingSuit, spadesBrain);
                    Card partnerCard = spadesBrain.CardsOnTable.First().Value;
                    Card oppCard = spadesBrain.CardsOnTable.Last().Value;
                    Card myHighestBelowBossMinusOne = turnData.CardsToUse.GetStrongestBelowRank(bossCard.Rank - 1) ;

                    // Partner is strongest on table
                    if (partnerCard.Rank > oppCard.Rank)
                    {
                        if (partnerCard.Rank < CardsUtils.GetXHighestCardInSuit(3, leadingSuit, deck).Rank && myHighestBelowBossMinusOne != null)  // Classic: J , jokers: K 
                        {
                            turnData.CardToPlay = myHighestBelowBossMinusOne;
                            strategyRule = 1;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} 3rd must play high (lower than boss-1), otherwise 4th unnecessarily wins a cheap trick", turnData.PlayerPos);
                        }
                    }
                    else // Opponents is strongest on table
                    {
                        if (oppCard.Rank == CardsUtils.GetXHighestCardInSuit(2, leadingSuit, deck).Rank )  // Classic: Q , jokers: A 
                        {
                            if (turnData.CardsToUse.GetWeakestAboveRank(CardsUtils.GetXHighestCardInSuit(3, leadingSuit, deck).Rank) != null) // get weakest above Q / A
                            {
                                turnData.CardsToUse.GetWeakestAboveRank(CardsUtils.GetXHighestCardInSuit(3, leadingSuit, deck).Rank); // BUG
                                strategyRule = 2;
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} 3rd must play King over opponent's Queen / (sJ over A)", turnData.PlayerPos);
                            }
                        }
                        if (myHighestBelowBossMinusOne != null && myHighestBelowBossMinusOne.Rank > oppCard.Rank)
                        {
                            turnData.CardToPlay = myHighestBelowBossMinusOne;
                            strategyRule = 3;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} 3rd must play higher than opponent (but lower than boss-1), otherwise 4th unnecessarily wins a cheap trick", turnData.PlayerPos);
                        }
                    }
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            }else
            {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}
