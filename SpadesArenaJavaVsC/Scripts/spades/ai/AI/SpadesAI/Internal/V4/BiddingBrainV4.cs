using System;
using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using common.utils;
using CommonAI;
using spades.models;
using spades.utils;
using SpadesAI.model;
using SpadesAI.V1;

namespace SpadesAI.Internal.V4
{
    internal class BiddingBrainV4 : BiddingBrainV1
    {
        protected static List<string[]> m_solo_nil_1_list;
        protected static List<string[]> m_solo_nil_2_list;
        protected static List<string[]> m_solo_nil_3_list;
        protected static List<string[]> m_high_cards_bid_value_list;
        protected static List<string[]> m_p_nil_side_1_list;
        protected static List<string[]> m_p_nil_side_2_list;
        protected static List<string[]> m_p_nil_side_3_list;
        protected static List<string[]> m_p_nil_spades_1_list;
        protected static List<string[]> m_p_nil_spades_2_list;
        protected static List<string[]> m_p_nil_spades_3_list;
        protected string deck;

        protected static List<string[]> Nil_Threshold_pos3;
        protected static List<string[]> Nil_Threshold_Pos4;

        public BiddingBrainV4(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            Logger.Enabled = true;
            deck = "regular";
            if (m_solo_nil_1_list == null) m_solo_nil_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\solo_nil_1");
            if (m_solo_nil_2_list == null) m_solo_nil_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\solo_nil_2");
            if (m_solo_nil_3_list == null) m_solo_nil_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\solo_nil_3");
            if (m_high_cards_bid_value_list == null) m_high_cards_bid_value_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\high_cards_bid_value");
            if (m_p_nil_side_1_list == null) m_p_nil_side_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_side_1");
            if (m_p_nil_side_2_list == null) m_p_nil_side_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_side_2");
            if (m_p_nil_side_3_list == null) m_p_nil_side_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_side_3_or_more");
            if (m_p_nil_spades_1_list == null) m_p_nil_spades_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_spades_1");
            if (m_p_nil_spades_2_list == null) m_p_nil_spades_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_spades_2");
            if (m_p_nil_spades_3_list == null) m_p_nil_spades_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_spades_3");

            if (Nil_Threshold_pos3 == null) Nil_Threshold_pos3 = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\Nil_threshold_pos3_cutof");
            if (Nil_Threshold_Pos4 == null) Nil_Threshold_Pos4 = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\Nil_threshold_pos4_cutof");
        }

        protected static float GetProbTableValue(Card.RankType rank, int cardsNum, int numberOfPlayersThatMightCutYou)
        {
            Dictionary<string, float> highCardsBidValue = m_high_cards_bid_value_list.ToDictionary(x => x[0] + x[1] + x[2], x => float.Parse(x[3]));

            string lookForKey = numberOfPlayersThatMightCutYou.ToString() + cardsNum.ToString() + rank.ToString()[0];
            highCardsBidValue.TryGetValue(lookForKey, out var value);
            return value;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// set bid based on hand and other bids
        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            Position prt = SpadesUtils.PartnerPosition(pos);
            Position lho = SpadesUtils.LhoPosition(pos);
            Position rho = SpadesUtils.RhoPosition(pos);
            prData.BiddingPosition = GetBiddingPosition();

            int rhoPointsIfBetMet = GetPointsIfBetMet(rho);
            int prtPointsIfBetMet = GetPointsIfBetMet(prt);
            int lhoPointsIfBetMet = GetPointsIfBetMet(lho);

            int partnersPointsIfBidMetBeforeMyBid = prtPointsIfBetMet + spadesBrain.MatchData.MatchPoints[pos];
            int opponentsPointsIfBidMet = lhoPointsIfBetMet + rhoPointsIfBetMet;
            int opponentsBags = spadesBrain.MatchData.MatchBags[lho] + spadesBrain.MatchData.MatchBags[rho];
            int partnersBags = spadesBrain.MatchData.MatchBags[prt] + spadesBrain.MatchData.MatchBags[pos];

            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;

            int finalBid;
            bool nil = false;

            // Blind-Nil (before looking at cards)
            spadesBrain.Players[pos].BlindNil = false;
            if (ShouldBidBlindNil(pos, prData.BiddingPosition, opponentsPointsIfBidMet, partnersPointsIfBidMetBeforeMyBid))
            {
                // small peek to insure you don't hold AS, since human would have quit the game.  
                if (!spadesBrain.Players[pos].Hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Ace))
                {
                    spadesBrain.Players[pos].BlindNil = true;
                    finalBid = -2;  // Bot treat this as bid = 0 & BlindNil = true
                    prData.Bid = finalBid;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, " Blind Nil ");
                    return finalBid;
                }
            }

            // You see your hand only after deciding if you try Blind Nil
            CardsList hand = spadesBrain.Players[pos].Hand;
//            hand = new CardsList("ASKS0S7S4S2SAHADKD9D6DKC8C"); //TODO: comment out
            // Initial bid as a function of the hand
            prData.InitialBid = InitialBid(pos, hand, spadesBrain.MatchData.Mode);
            
            // Adjustment according to the other bids
            float otherPlayersFactor = OtherPlayersFactor(prData.BiddingPosition, (int) Math.Round(prData.InitialBid, MidpointRounding.AwayFromZero));
            prData.InitialBid += otherPlayersFactor;
            if (prData.InitialBid <= 1) prData.InitialBid = 1;

            // adjustment and round according to bot aggressiveness level 
            int botLevel = spadesBrain.Players[pos].Level;
            prData.NumericBid = RoundingByBotLevel(prData.InitialBid, botLevel);

            // Check Nil
            float nilChances = 0;
            if (spadesBrain.MatchData.SubVariation != PlayingSubVariant.NoNil){
                nilChances = spadesBrain.MatchData.Mode == PlayingMode.Partners ? PartnersNilChances(hand): SoloNilChances(hand);
            }
            prData.NilValue = nilChances; // record in Database at table round_game_transaction

            if (CheckNilDynamicThreshold(pos, prData.BiddingPosition, nilChances, botLevel, partnersPointsIfBidMetBeforeMyBid, opponentsPointsIfBidMet)){
                nil = true;
            }

            // in C4P just maximize E[points], otherwise maximize Pr(Win)
            if (spadesBrain.MatchData.SubVariation != PlayingSubVariant.Coins4Points)
            {
                // bid {-1 / +1 / +2 / =0 / sum = 14} when you are about to lose the game if you don't (don't enter if you nil)
                if (nil == false)
                {
                    prData.NumericBid = LastRoundBidAdjust(bagsToPenalty, opponentsBags, prData.NumericBid, pos, hand, prData.BiddingPosition,
                        opponentsPointsIfBidMet, partnersPointsIfBidMetBeforeMyBid, nilChances);
                    if (prData.NumericBid == 0)
                    {
                        nil = true;
                    }
                }

                // Reduce risk: decrease bid by one if wining
                prData.NumericBid = ReduceRiskIfWinIsInMyPocket(prData.BiddingPosition, prData.NumericBid, spadesBrain.MatchData.Mode, pos,
                    lhoPointsIfBetMet, rhoPointsIfBetMet, prtPointsIfBetMet, partnersPointsIfBidMetBeforeMyBid,
                    partnersBags);
                // Reduce risk: don't nil if wining by a lot
                if (nil && DontNilIfWinIsInMyPocket(prData.BiddingPosition, prData.NumericBid, spadesBrain.MatchData.Mode, pos,
                        lhoPointsIfBetMet, rhoPointsIfBetMet, prtPointsIfBetMet, partnersPointsIfBidMetBeforeMyBid,
                        partnersBags))
                {
                    nil = false;
                }
            }
            // Validations
            prData.NumericBid = bidValidation(pos, prData.BiddingPosition, prData.NumericBid);

            finalBid = nil ? 0 : prData.NumericBid;
            prData.Bid = finalBid;
            return finalBid;
        }
        // --------------------------------------------------------------------------------------------------- //


        internal int GetPointsIfBetMet(Position pos)
        {
            int pointsIfBetMet = spadesBrain.MatchData.MatchPoints[pos];
            if (spadesBrain.Players[pos].Bid != -1) // bid -1 means this player didn't bid yet
            {
                pointsIfBetMet += SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[pos].Bid, spadesBrain.Players[pos].BlindNil);
            }
            return pointsIfBetMet;
        }

        internal new int GetBiddingPosition()
        {
            int biddingPosition = 1;
            foreach (Position p in Enum.GetValues(typeof(Position)))
            {
                if (spadesBrain.Players[p].Bid != -1)
                {
                    biddingPosition++;
                }
            }
            return biddingPosition;
        }


        /// <summary>
        /// check if we win even if we dont bid nil, then bid the numericBid
        /// </summary>
        protected bool DontNilIfWinIsInMyPocket(int biddingPosition, int prDataNumericBid, PlayingMode matchDataMode, Position pos, int lhoPointsIfBetMet, 
                                                int rhoPointsIfBetMet, int prtPointsIfBetMet, int partnersPointsIfBidMetBeforeMyBid, int partnersBags)
        {
            int myPointsIfSwitchToNumericBid = spadesBrain.MatchData.MatchPoints[pos] + SpadesUtils.PointsIfSuccessBid(matchDataMode, prDataNumericBid);
            int myBags = spadesBrain.MatchData.MatchBags[pos];
            int maxOppPointsIfBidMet = Math.Max(lhoPointsIfBetMet, Math.Max(rhoPointsIfBetMet, prtPointsIfBetMet));
           
            if (matchDataMode == PlayingMode.Solo)
            {
                if (biddingPosition == 4)
                {
                    // if we bid last and can win with numeric bid
                    if (myPointsIfSwitchToNumericBid >= spadesBrain.MatchData.WinConditionPoints &&
                        myPointsIfSwitchToNumericBid >= maxOppPointsIfBidMet &&
                        spadesBrain.MatchData.BagsToIncurPenalty - myBags >= 2)
                    {
                        Logger.DebugFormat("dont nil, safer to go for numeric bid");
                        return true;
                    }
                }
            }
            else if (matchDataMode == PlayingMode.Partners)
            {
                int partnersPointsIfSwitchToNumericBid = partnersPointsIfBidMetBeforeMyBid + SpadesUtils.PointsIfSuccessBid(matchDataMode, prDataNumericBid);

                if (biddingPosition == 4)
                {
                    // if we bid last and can win with numeric bid
                    if (partnersPointsIfSwitchToNumericBid >= spadesBrain.MatchData.WinConditionPoints &&
                        partnersPointsIfSwitchToNumericBid >= rhoPointsIfBetMet + lhoPointsIfBetMet &&
                        spadesBrain.MatchData.BagsToIncurPenalty - partnersBags >= 2)
                        {
                            Logger.DebugFormat("dont nil, safer to go for numeric bid");
                            return true;
                        }
                }
                else // bidding pos < 4
                {
                    // if can win with numeric bid
                    if (partnersPointsIfSwitchToNumericBid >= spadesBrain.MatchData.WinConditionPoints &&
                        partnersPointsIfSwitchToNumericBid >= rhoPointsIfBetMet + lhoPointsIfBetMet + 100 &&
                        spadesBrain.MatchData.BagsToIncurPenalty - partnersBags >= 2)
                    {
                        Logger.DebugFormat("dont nil, safer to go for numeric bid");
                        return true;
                    }
                }
            }
            return false; // go for the nil
        }


        /// <summary>
        /// change bid by {-1, +1, +2, =0, sum = 14} when about to lose if not
        /// </summary>
        protected int LastRoundBidAdjust(int bagsToPenalty, int opponentsBags, int myBid, Position pos, CardsList hand, int biddingPosition, int opponentsPointsIfBidMet, int partnersPointsIfBidMetBeforeMyBid, float nilChances)
        {
            string lastRoundBidAdjustString = "";
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners && myBid > 0 ) // Partners
            {
                int myPointsIfBidMet = SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, myBid, spadesBrain.Players[pos].BlindNil);
                int pointsGap = opponentsPointsIfBidMet - partnersPointsIfBidMetBeforeMyBid - myPointsIfBidMet;
                switch (biddingPosition)
                {
                    case 4:
                        if (pointsGap > 0 && opponentsPointsIfBidMet >= spadesBrain.MatchData.WinConditionPoints) // opponents win the game
                        {
                            lastRoundBidAdjustString += "-=!@#$  opponents can win the game  $#@!=-";
                            if ((spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid == 0 &&
                                 spadesBrain.Players[SpadesUtils.RhoPosition(pos)].BlindNil) ||
                                (spadesBrain.Players[SpadesUtils.LhoPosition(pos)].Bid == 0 &&
                                 spadesBrain.Players[SpadesUtils.LhoPosition(pos)].BlindNil))
                            {
                                lastRoundBidAdjustString += "opponents Blind nil, best chance is to set them ";
                                if (myBid > 1)
                                {
                                    lastRoundBidAdjustString += "opponents Blind nil, decrease bid by 1 to allows my partner and me to throw off in suits " ;
                                    // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                    return myBid - 1;
                                }
                                // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                return myBid;
                            }
                            else
                            {
                                if (myBid + CalcTotalBids() >= 13)
                                {
                                    lastRoundBidAdjustString += "Opponents win the game by making their bid, -> complete bids to 14 and try to set them.";
                                    // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                    return Math.Max(1, 14 - CalcTotalBids());
                                }
                                else
                                {
                                    if (pointsGap <= 10)
                                    {
                                        lastRoundBidAdjustString += "must increase bid by 1 to be able to win" + "final bid == " + (myBid + 1) ;
                                        // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                        return myBid + 1;
                                    }

                                    if (pointsGap > 10 &&
                                        nilChances >= 0.15 &&
                                        spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid != 0 &&
                                        !hand.CardsBySuits(Card.SuitType.Spades).HasRank(CardsUtils.GetXHighestCardInSuit(1, Card.SuitType.Spades, deck).Rank)) // King/SJ
                                    {
                                        lastRoundBidAdjustString += "must do risky Nil to be able to win (Threshold = 0.15) ";
                                        // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                        return 0;
                                    }

                                    if (bagsToPenalty - opponentsBags <= 2 &&
                                        (spadesBrain.MatchData.BagsPenalty >= pointsGap ||
                                         opponentsPointsIfBidMet - spadesBrain.MatchData.BagsPenalty <
                                         spadesBrain.MatchData.WinConditionPoints))
                                    {
                                        lastRoundBidAdjustString += "opponents close to bags limit, best chance is to Sandbag them ";
                                        // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                        if (myBid > 1)
                                            return myBid - 1;
                                        else
                                            return myBid;
                                    }

                                    if (pointsGap > 10 && pointsGap <= 20)
                                    {
                                        lastRoundBidAdjustString += "must increase bid by 2 to be able to win" + "final bid == " + (myBid + 2);
                                        // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                        return myBid + 2;
                                    }

                                    if (spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid == 0 ||
                                        spadesBrain.Players[SpadesUtils.LhoPosition(pos)].Bid == 0)
                                    {
                                        lastRoundBidAdjustString += "best chance is to set opponents' nil";
                                        if (myBid > 1)
                                        {
                                            lastRoundBidAdjustString += "decrease bid by 1 to allows my partner and me to throw off in suits ";
                                            // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                            return myBid - 1;
                                        }
                                        // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                        return myBid;
                                    }

                                    if (myBid + CalcTotalBids() >= 11)
                                    {
                                        lastRoundBidAdjustString += "must Set opponents, so bid to make total bid of 14. (if you set you get more points, if not you lose anyway)";
                                        // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                        return Math.Max(1, 14 - CalcTotalBids());
                                    }
                                }
                            }
                        }

                        break;
                    case 3: // Pos 3
                        const int forthOpponent = 20; // forth opponent will likely make at least 20
                        if (pointsGap + forthOpponent > 0 && opponentsPointsIfBidMet + forthOpponent >=
                            spadesBrain.MatchData.WinConditionPoints)
                        {
                            if (spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid == 0 &&
                                spadesBrain.Players[SpadesUtils.RhoPosition(pos)].BlindNil)
                            {
                                lastRoundBidAdjustString += "opponents Blind nil, best chance is to set them ";
                                if (myBid > 1)
                                {
                                    lastRoundBidAdjustString += "opponents Blind nil, decrease bid by 1 to allows my partner and me to throw off in suits ";
                                    // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                    return myBid - 1;
                                }
                                // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                return myBid;
                            }

                            // opponents do not bid BN:
                            if (pointsGap <= 10)
                            {
                                lastRoundBidAdjustString += "must increase bid by 1 to be able to win" + "final bid == " + (myBid + 1);
                                // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                return myBid + 1;
                            }

                            if (pointsGap > 0 && nilChances >= 0.15 && spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid != 0)
                            {
                                lastRoundBidAdjustString += "must do risky Nil to be able to win (nilChances >= 0.15) ";
                                // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                return 0;
                            }

                            if (bagsToPenalty - opponentsBags <= 2 &&
                                (spadesBrain.MatchData.BagsPenalty >= pointsGap ||
                                 opponentsPointsIfBidMet - spadesBrain.MatchData.BagsPenalty <
                                 spadesBrain.MatchData.WinConditionPoints))
                            {
                                lastRoundBidAdjustString += "opponents close to bags limit, best chance is to Sandbag them, don't under-bid since you pos=3 ";
                                // // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                                return myBid;
                            }
                            else
                            {
                                lastRoundBidAdjustString += "not much to do, prey for set/sandBags";
                            }
                        }

                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                        break;
                }
            }
            else if (spadesBrain.MatchData.Mode == PlayingMode.Solo && myBid > 0) // Solo
            {
                int humanBid = spadesBrain.Players[Position.South].Bid;

                // if human didn't bid yet assume he will make at least 20
                int thisRoundHumanPoints = humanBid == -1 ? 20 : SpadesUtils.PointsIfSuccessBid(PlayingMode.Solo, humanBid,spadesBrain.Players[Position.South].BlindNil); 
                int humanPointsIfBidMet = spadesBrain.MatchData.MatchPoints[Position.South] + thisRoundHumanPoints;
                int myPointsIfBidMet    = spadesBrain.MatchData.MatchPoints[pos] + SpadesUtils.PointsIfSuccessBid(PlayingMode.Solo, myBid, spadesBrain.Players[pos].BlindNil);

                if (humanPointsIfBidMet >= spadesBrain.MatchData.WinConditionPoints && humanPointsIfBidMet > myPointsIfBidMet)
                {
                    // Human is about to win
                    lastRoundBidAdjustString += "-=!@#$  opponent can win the game  $#@!=-";
                    if (humanPointsIfBidMet - myPointsIfBidMet <= 10)
                    {
                        lastRoundBidAdjustString += "increase bid by 1 to be able to win" + "final bid == " + (myBid + 1);
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                        return myBid + 1;
                    }
                    else if (nilChances >= 0.20)
                    {
                        lastRoundBidAdjustString += "risky Nil to be able to win (nilChances >= 0.2)";
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                        return 0;
                    }
                    else if (humanBid == 0)
                    {
                        lastRoundBidAdjustString += "reduce bid by one and to try to set Nil";
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                        return myBid - 1;
                    }
                    else if (humanPointsIfBidMet - myPointsIfBidMet <= 20)
                    {
                        Random rnd = new Random();
                        var number = rnd.NextDouble();
                        lastRoundBidAdjustString += $"50% of the times, add 2 to the bid.  if {number:0.##} > 0.5 add";
                        if (number > 0.5)
                        {
                            lastRoundBidAdjustString += "increase bid by 2 to be able to win" + "final bid == " + (myBid + 2);
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                            return myBid + 2;
                        }
                    }
                    else if (nilChances >= 0.10)
                    {
                        lastRoundBidAdjustString += "risky Nil to be able to win (nilChances >= 0.1)";
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
                        return 0;
                    }
                    else
                    {
                        lastRoundBidAdjustString += "nothing to do, pray that Human will be set/sandbag, final bid == " + myBid;
                    }
                }
            }

            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, lastRoundBidAdjustString);
            return myBid;
        }


        /// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// THIS IS AN OLD METHOD THAT SHOULD BE REMOVED AFTER WE ARE SURE THE NEW ONE IS WORKING (4/2/2020)
        /// /////////////////////////////////
        protected bool ShouldBidBlindNilOld(Position pos, int biddingPosition, int opponentsPointsIfBidMet, int partnersPointsIfBidMetBeforeMyBid)
        {
            // list of situations that never blind nil:
            if (spadesBrain.MatchData.SubVariation == PlayingSubVariant.NoNil || 
                spadesBrain.MatchData.Mode != PlayingMode.Partners || // there is no BN in Solo
                spadesBrain.MatchData.RoundNum <= 1 || // can't BN on the first round of a game
                biddingPosition < 3 || 
                spadesBrain.Players[pos].Level == 9 ||  // G9 never nil
                (spadesBrain.MatchData.SubVariation == PlayingSubVariant.Coins4Points && pos == Position.North) ) // partner will not BN in C4P
                return false;

            int assumeOppWillMakeAtLeast = biddingPosition == 3 ? 20 : 0;
            if (opponentsPointsIfBidMet + assumeOppWillMakeAtLeast >= spadesBrain.MatchData.WinConditionPoints &&
                opponentsPointsIfBidMet + assumeOppWillMakeAtLeast > partnersPointsIfBidMetBeforeMyBid + 100)
            {
                // opponents are winning the game and leading by more than a 100 -> BN might be the only option

                // if no nil bids, go BN
                if (spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid != 0 &&
                    spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid != 0 &&
                    spadesBrain.Players[SpadesUtils.LhoPosition(pos)].Bid != 0) 
                    return true;

                // if opponent already bid nil, bid BN only if the sum of bids is high.
                if (CalcTotalBids() >= 10 && spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid > 0)
                    return true;
                return false;
            }

            return false;
        }


        protected bool ShouldBidBlindNil(Position pos, int biddingPosition, int opponentsPointsIfBidMet, int partnersPointsIfBidMetBeforeMyBid)
        {
            // list of situations that never blind nil:
            if (spadesBrain.MatchData.SubVariation == PlayingSubVariant.NoNil ||
                spadesBrain.MatchData.Mode != PlayingMode.Partners || // there is no BN in Solo
                spadesBrain.MatchData.RoundNum <= 1 || // can't BN on the first round of a game
                biddingPosition < 3 ||
                spadesBrain.Players[pos].Level == 9 || // G9 never nil
                spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid == 0 || // never double nil
                (spadesBrain.MatchData.SubVariation == PlayingSubVariant.Coins4Points && pos == Position.North) || // partner will not BN in C4P
                (spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid == 0 && spadesBrain.Players[SpadesUtils.LhoPosition(pos)].Bid == 0) // if both opponents nil.
            )
                return false;

            int oppWillMakeAtLeast = biddingPosition == 3 ? 20 : 0;
            if (opponentsPointsIfBidMet + oppWillMakeAtLeast >= spadesBrain.MatchData.WinConditionPoints &&
                opponentsPointsIfBidMet + oppWillMakeAtLeast > partnersPointsIfBidMetBeforeMyBid + 100)
            {
                // opponents are winning the game and leading by more than a 100 -> BN might be the only winning chance

                int TotalBidsThreshold = biddingPosition == 3 ? 6 : 9;
                int partnerBidsThreshold = 4;
                // decrease 1 from the threshold if opponent bid nil
                if (spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid == 0 ||
                    spadesBrain.Players[SpadesUtils.LhoPosition(pos)].Bid == 0)
                    TotalBidsThreshold -= 1;

                // C4P -> be conservative: threshold + 2
                if (spadesBrain.MatchData.SubVariation == PlayingSubVariant.Coins4Points)
                {
                    TotalBidsThreshold += 2;
                    partnerBidsThreshold += 2;
                }

                if (CalcTotalBids() >= TotalBidsThreshold ||
                    spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid >= partnerBidsThreshold)
                {
                    // BN - other players bid high or partner bid 4+
                    return true;
                }

                // don't BN - other players bid too low
            }
            return false;
        }



        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public bool CheckNilJokers(PlayingMode mod, CardsList hand, PlayerRoundData prData, Position pos, int biddingPosition, float nilChances, int BotLevel)
        {
            float noise = GetNoise(BotLevel); // noise is added to levels 7,8,0.  Level 9 is never bidding Nil.
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "Nil noise is {0}", noise);

            // Solo
            if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                if (biddingPosition == 4 && CalcTotalBids() >= 13)
                {
                    if (nilChances + noise >= 0.23 && nilChances > 0.1)
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "bids are high (13+), Threshold decrease to = 0.23");
                        return true;
                    }
                }
                else
                {
                    if (nilChances + noise >= 0.30 && nilChances > 0.1)
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "going for nil - Threshold = 0.3 ");
                        return true;
                    }
                }
            }
            else // Partners //
            {
                if (biddingPosition >= 3)
                {
                    if (spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid == 0)
                    {
                        // if partner bid nil you must have a super-low hand to bid Nil
                        if (nilChances + noise >= 1.0 && nilChances > 0.1)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "!!! going for Double Nil !!! - Threshold = 1.0 ");
                            return true;
                        }
                    }
                    else
                    {
                        // partner bid > 0 
                        float threshold = (float) Math.Max(0.1, (0.25 - 0.02 * (spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid - 3)));
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "partner didn't bid 0 , modify Threshold according to partners bid - Threshold is: "+ threshold);
                        if (nilChances + noise >= threshold &&
                            nilChances > 0.1 &&
                            !hand.CardsBySuits(Card.SuitType.Spades).HasRank(CardsUtils.GetXHighestCardInSuit(1, Card.SuitType.Spades, deck).Rank)
                        ) // King/SJ  
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "nil - Threshold = 0.25 - 0.02 * (prtBid-3)");
                            return true;
                        }
                    }
                }
                else // biddingPosition < 3
                {
                    if (nilChances + noise >= 0.25 &&
                        nilChances > 0.1 &&
                        !hand.CardsBySuits(Card.SuitType.Spades)
                            .HasRank(CardsUtils.GetXHighestCardInSuit(1, Card.SuitType.Spades, deck).Rank)) // King/SJ
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "nil - Threshold = 0.25");
                        return true;
                    }
                }
            }
            return false;
        }


        /// <summary> ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Check if to bid nil. If probability of success is greater than (63%) then T
        /// Based on logistic regression(previous bids, nilChances).
        /// The threshold for nilChances is different for every biding sequence of opponents and partner.
        /// </summary>
        public bool CheckNilDynamicThreshold(Position pos, int biddingPosition, float nilChances, int BotLevel, int partnersPointsIfBidMetBeforeMyBid, int opponentsPointsIfBidMet)
        {
            if (spadesBrain.MatchData.SubVariation == PlayingSubVariant.NoNil)
                return false;
            
            float noise = GetNoise(BotLevel); // noise is added to levels 7,8,0.  Level 9 is never bidding Nil.
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "Nil noise is {0}", noise);

            // Solo // Threshold(opponents bids)
            if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                float NilThreshold;
                switch (biddingPosition)
                {
                    //case 1:
                    //case 2:
                    default:
                        NilThreshold = 0.4F;
                        break;
                    case 3:
                        NilThreshold = 0.65F - (CalcTotalBids() * 0.037F);
                        break;
                    case 4:
                        switch (CalcTotalBids())
                        {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                NilThreshold = 1.2F;
                                break;
                            case 6:
                                NilThreshold = 0.74F;
                                break;
                            case 7:
                                NilThreshold = 0.66F;
                                break;
                            case 8:
                                NilThreshold = 0.59F;
                                break;
                            case 9:
                                NilThreshold = 0.52F;
                                break;
                            case 10:
                                NilThreshold = 0.44F;
                                break;
                            case 11:
                                NilThreshold = 0.37F;
                                break;
                            case 12:
                                NilThreshold = 0.3F;
                                break;
                            case 13:
                                NilThreshold = 0.22F;
                                break;
                            default:
                                NilThreshold = 0.20F;
                                break;
                        }
                        break;
                }
                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "Nil Threshold = " + NilThreshold);
                if (nilChances + noise >= NilThreshold && nilChances > 0.1)
                {
                    return true;
                }
            }
            else // Partners //
            {
                int prt_bid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;
                int rho_bid = spadesBrain.Players[SpadesUtils.RhoPosition(pos)].Bid;
                int lho_bid = spadesBrain.Players[SpadesUtils.LhoPosition(pos)].Bid;
                float cutoff_nil_val;
                
                // if leading by more than 100, increase nil cutoff by 0.2*(gap/100)
                float increaseCutoffWhenLeading = 0;
                if (partnersPointsIfBidMetBeforeMyBid > opponentsPointsIfBidMet + 100)
                {
                    increaseCutoffWhenLeading = 0.2F * (partnersPointsIfBidMetBeforeMyBid - (float) opponentsPointsIfBidMet) / 100;
                }

                switch (biddingPosition)
                {
                    case 1:
                    case 2:
                        if (nilChances + noise >= 0.48 + increaseCutoffWhenLeading && nilChances > 0.1)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "Nil from first or second position");
                            return true;
                        }
                        break;

                    case 3:
                        if (prt_bid > 0)
                        {
                            Dictionary<string, float> DynamicThreshold_pos3 = Nil_Threshold_pos3.ToDictionary(x => x[0] + "," + x[1], x => float.Parse(x[2]));
                            string lookForKey_pos3 = rho_bid + "," + prt_bid;
                            if (DynamicThreshold_pos3.TryGetValue(lookForKey_pos3, out cutoff_nil_val))
                            {
                                if (nilChances + noise >= cutoff_nil_val + increaseCutoffWhenLeading && nilChances > 0.1)
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "nil value(+noise) = " + (nilChances + noise) + " Threshold based on previous bids = " + cutoff_nil_val);
                                    return true;
                                }
                            }
                            else
                            {
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "cutoff_nil_val wasn't found");
                                if (nilChances + noise >= 0.48 && nilChances > 0.1)
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "Nil");
                                    return true;
                                }
                            }
                        }
                        break;

                    case 4:
                        if (prt_bid > 0)
                        {
                            Dictionary<string, float> DynamicThreshold_pos4 =
                                Nil_Threshold_Pos4.ToDictionary(x => x[0] + "," + x[1], x => float.Parse(x[2]));
                            string lookForKey_pos4 = prt_bid.ToString() + "," + (lho_bid + rho_bid).ToString();
                            if (DynamicThreshold_pos4.TryGetValue(lookForKey_pos4, out cutoff_nil_val))
                            {
                                if (nilChances + noise >= cutoff_nil_val + increaseCutoffWhenLeading &&
                                    nilChances > 0.1)
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "nil value(+noise) = " + (nilChances + noise) + " Threshold based on previous bids = " + cutoff_nil_val);
                                    return true;
                                }
                            }
                            else
                            {
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "cutoff_nil_val wasn't found");
                                if (nilChances + noise >= 0.48 && nilChances > 0.1)
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "Nil");
                                    return true;
                                }
                            }
                        }
                        break;
                }
            }
            return false;
        }


        private static float GetNoise(int BotLevel)
        {
            float noise;
            Random random = new Random();
            var randomNumber = random.NextDouble();
            switch (BotLevel)
            {
                // && BotLevel <= 8)
                case 7: // noise = Uniform[-0.1,0.1]
                case 8: // noise = Uniform[-0.2,0.2]
                    noise = (float) (randomNumber / 5 - 0.1) * (BotLevel - 6);
                    break;
                case 0: // noise = [-0.2,0.2], skewed to extreme 
                    noise = (float) (randomNumber / 5 - 0.1) * 3;
                    break;

                case 9:
                    return -10; // level 9 never bids nil
                    
                default:
                    noise = 0;
                    break;
            }

            if (noise > 0.2) noise = 0.2F;
            if (noise < -0.2) noise = -0.2F;
            return noise;
        }


        public int RoundingByBotLevel(float bid, int botLevel)
        {
            int bidLevelAdjustment;
            switch (botLevel)
            {
                case 1:
                    bidLevelAdjustment = (int) Math.Round(bid - 0.5, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "very conservative bot bid - 0.5 = " + (bid - 0.5));
                    break;
                case 2:
                    bidLevelAdjustment = (int) Math.Round(bid - 0.3, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "conservative bot, bid - 0.3 = " + (bid - 0.3));
                    break;
                case 3:
                    bidLevelAdjustment = (int) Math.Round(bid - 0.1, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"neutral bot, bid - 0.1 = " + (bid - 0.1) );
                    break;
                case 4:
                    bidLevelAdjustment = (int) Math.Round(bid + 0.1, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "aggressive bot, bid + 0.1 = " + (bid + 0.1));
                    break;
                case 5:
                    bidLevelAdjustment = (int) Math.Round(bid + 0.3, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "very aggressive bot, bid + 0.3 = " + (bid + 0.3));
                    break;
                case 6:
                    bidLevelAdjustment = (int) Math.Round(bid + 0.5, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "very very aggressive bot, bid + 0.5 = " + (bid + 0.5));
                    break;
                case 0:
                    Random random = new Random();
                    float rnd_noise = (float) (random.NextDouble() * 2 - 1);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "add uniform[-1,1] noise");
                    bidLevelAdjustment = (int) Math.Round(bid + rnd_noise, MidpointRounding.AwayFromZero);
                    break;
                default:
                    bidLevelAdjustment = (int) Math.Round(bid, MidpointRounding.AwayFromZero);
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "neutral bot, bid = " + bid);
                    break;
            }
            return bidLevelAdjustment;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public float InitialBid(Position pos, CardsList hand, PlayingMode gameMode)
        {
            int numPlayers = gameMode == PlayingMode.Solo ? 3 : 2;
            List<Position> opponentsPositions = SpadesUtils.OpponentsPositions(pos, spadesBrain.MatchData.Mode);
            foreach (Position opponentPos in opponentsPositions)
            {
                if (spadesBrain.Players[opponentPos].Bid == 0)
                {
                    numPlayers = gameMode == PlayingMode.Solo ? 2 : 1;
                }
            }

            float initialBid = PotentialBid(hand, numPlayers);
            return initialBid;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public int ReduceRiskIfWinIsInMyPocket(int biddingPosition, int numericBid, PlayingMode gameMode, Position pos,
            int lhoPointsIfBetMet, int rhoPointsIfBetMet, int prtPointsIfBetMet, int partnersPointsIfBidMetBeforeMyBid,
            int partnersBags)
        {
            if (biddingPosition == 4)
            {
                int myPointsIfBidMet = spadesBrain.MatchData.MatchPoints[pos] +
                                       SpadesUtils.PointsIfSuccessBid(gameMode, numericBid);
                int myBags = spadesBrain.MatchData.MatchBags[pos];
                int maxOppPointsIfBidMet = Math.Max(lhoPointsIfBetMet, Math.Max(rhoPointsIfBetMet, prtPointsIfBetMet));

                if (gameMode == PlayingMode.Solo)
                {
                    if (myPointsIfBidMet >= spadesBrain.MatchData.WinConditionPoints + 10 &&
                        myPointsIfBidMet > maxOppPointsIfBidMet + 10 &&
                        spadesBrain.MatchData.BagsToIncurPenalty - myBags >= 3 &&
                        numericBid > 1)
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "decrease bid by one to reduce risk - we win this game");
                        return numericBid - 1;
                    }
                }
                else if (gameMode == PlayingMode.Partners)
                {
                    int partnersPointsIfBidMet = partnersPointsIfBidMetBeforeMyBid +
                                                 SpadesUtils.PointsIfSuccessBid(gameMode, numericBid);
                    if (partnersPointsIfBidMet >= spadesBrain.MatchData.WinConditionPoints + 10 &&
                        partnersPointsIfBidMet > rhoPointsIfBetMet + lhoPointsIfBetMet + 10 &&
                        spadesBrain.MatchData.BagsToIncurPenalty - partnersBags >= 3 &&
                        numericBid > 1)
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "decrease bid by one to reduce risk - we win this game");
                        return numericBid - 1;
                    }
                }
            }

            return numericBid;
        }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public override float PotentialBid(CardsList cardsList, int numberOfPlayersThatMightCutYou)
        {
            float result = 0;
            float aceFactor = 0, kingFactor = 0, queenFactor = 0;
            int voids = 0, singletons = 0, doubletons = 0;
            CardsList cards;
            string potentialBidString = "";

            // Non trump counting
            Card.SuitType[] nonTrump = {Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts};
            foreach (Card.SuitType suit in nonTrump)
            {
                cards = cardsList.CardsBySuits(suit);
                potentialBidString += "cards = " + cards;
                // Ace
                if (cards.HasRank(Card.RankType.Ace))
                {
                    aceFactor = GetProbTableValue(Card.RankType.Ace, cards.Count, numberOfPlayersThatMightCutYou);
                    potentialBidString += " for the ace = " + aceFactor;
                }

                // King
                if (cards.HasRank(Card.RankType.King))
                {
                    kingFactor = GetProbTableValue(Card.RankType.King, cards.Count, numberOfPlayersThatMightCutYou);
                    potentialBidString += " for the king = " + kingFactor;
                }

                // Queen
                if (cards.HasRank(Card.RankType.Queen))
                {
                    queenFactor = GetProbTableValue(Card.RankType.Queen, cards.Count, numberOfPlayersThatMightCutYou);
                    potentialBidString += " for the queen = " + queenFactor;
                }

                // Count short non-trump suits
                switch (cards.Count)
                {
                    case 0:
                        voids++;
                        break;
                    case 1:
                        singletons++;
                        break;
                    case 2:
                        doubletons++;
                        break;
                }

                result += aceFactor + kingFactor + queenFactor;
                aceFactor = 0; kingFactor = 0; queenFactor = 0;
            }

            // Trump counting
            cards = cardsList.CardsBySuits(Card.SuitType.Spades);
            cards.SortHighToLowSuitFirst();
            potentialBidString += " spades cards = " + cards;

            float bid = 0;
            int spareSpades = cards.Count;
            bool countK = false, countQ = false, countJ = false;
            if (cards.HasRank(Card.RankType.Ace))
            {
                //A
                potentialBidString += " As = 1";
                bid++;
                spareSpades--;
                if (cards.HasRank(Card.RankType.King))
                {
                    // AK
                    potentialBidString += " Ks = 1";
                    bid++;
                    spareSpades--;
                    countK = true;
                    if (cards.HasRank(Card.RankType.Queen))
                    {
                        // AKQ
                        potentialBidString += " Qs = 1";
                        bid++;
                        spareSpades--;
                        countQ = true;
                        if (cards.HasRank(Card.RankType.Jack))
                        {
                            // AKQJ
                            bid = cards.Count;
                            spareSpades = 0;
                            countJ = true;
                            potentialBidString += " AKQJxx - any spade is a take = " + bid;
                        }
                        else if (spareSpades >= 2)
                        {
                            // AKQxx
                            bid = cards.Count - 1;
                            potentialBidString += " AKQxx= " + bid;
                            spareSpades = 0;
                        }
                    }
                    else if (cards.HasRank(Card.RankType.Jack))
                    {
                        // AK_Jxx
                        bid = cards.Count - 1;
                        potentialBidString += " AK_Jxx= " + bid;
                        spareSpades = 0;
                    }
                }
                else if (cards.HasRank(Card.RankType.Queen))
                {
                    // A_Qx
                    if (cards.Count > 2)
                    {
                        bid = 2;
                        spareSpades -= 2;
                    }

                    if (cards.HasRank(Card.RankType.Jack))
                    {
                        // A_QJxx
                        bid = cards.Count - 1;
                        potentialBidString += " A_QJxx = " + bid;
                        spareSpades = 0;
                    }
                }
            }
            else if (cards.HasRank(Card.RankType.King) && cards.HasRank(Card.RankType.Queen) &&
                     cards.HasRank(Card.RankType.Jack))
            {
                // _KQJxx
                bid = cards.Count - 1;
                potentialBidString += " _KQJxx = " + bid;
                spareSpades = 0;
            }

            else if (cards.HasRank(Card.RankType.King) && cards.HasRank(Card.RankType.Queen))
            {
                // _KQxx
                if (cards.Count == 2)
                {
                    bid = 1;
                    potentialBidString += " _KQ = " + bid;
                    spareSpades = 0;
                }
                else if (cards.Count == 3 || cards.Count == 4)
                {
                    bid = 2;
                    potentialBidString += " _KQx/x = " + bid;
                    spareSpades = 0;
                }
                else if (cards.Count > 4)
                {
                    bid = cards.Count - 2;
                    potentialBidString += " _KQxxx" + bid;
                    spareSpades = 0;
                }
            }
            else if (cards.Count >= 6 && spareSpades > 0)
            {
                switch (cards.Count)
                {
                    case 6:
                        bid += cards.Count - 3.5f;
                        break;
                    case 7:
                        bid += cards.Count - 3.0f;
                        break;
                    case 8:
                        bid += cards.Count - 2.5f;
                        break;
                    case 9:
                        bid += cards.Count - 2.3f;
                        break;
                    case 10:
                        bid += cards.Count - 2.0f;
                        break;
                    case 11:
                        bid += cards.Count - 1.5f;
                        break;
                    case 12:
                        bid += cards.Count - 1.0f;
                        break;
                    case 13:
                        bid += cards.Count - 0.0f;
                        break;
                    default:
                        bid += cards.Count - 3.5f;
                        break;
                }

                potentialBidString += " 6+ spades = " + bid;
                spareSpades = 0;
            }

            if (cards.Count <= 1)
            {
                bid -= 1;
                potentialBidString += " void/singleton in spades (bid-1) " + bid;
            }

            potentialBidString += " spades factor = " + bid;
            bool countVoid = false, countSingleton = false, countDoubleton = false;

            while (spareSpades > 0)
            {
                if (voids > 0 && !countVoid)
                {
                    potentialBidString += " voids>0 = ";

                    if (spareSpades >= 3)
                    {
                        bid += 2f;
                        potentialBidString += " more than 2 spare spades = 2" + '\n' + "spades factor = " + bid; // 2.67
                        spareSpades -= 3;
                        countVoid = true;
                    }
                    else if (spareSpades == 2)
                    {
                        potentialBidString += " 2 spare spades = 1.5" + '\n' + "spades factor = " + bid; // 1.94
                        bid += 1.5f;
                        spareSpades -= 2;
                        countVoid = true;
                    }
                    else if (spareSpades == 1)
                    {
                        bid += 1f;
                        spareSpades -= 1;
                        countVoid = true;
                        potentialBidString += " 1 spare spades = 1" + '\n' + "spades factor = " + bid;
                    }
                }
                else if (singletons > 0 && !countSingleton)
                {
                    if (spareSpades >= 2)
                    {
                        bid += 1.5f;
                        spareSpades -= 2;
                        countSingleton = true;
                        potentialBidString += " more than 1 spare spades = 1.5" + '\n' + "spades factor = " + bid;
                    }
                    else if (spareSpades == 1)
                    {
                        bid += 0.915f;
                        spareSpades -= 1;
                        countVoid = true;
                        potentialBidString += " 1 spare spades = 0.915" + '\n' + "spades factor = " + bid;
                    }
                }
                else if (cards.HasRank(Card.RankType.King) && !countK && spareSpades >= 2)
                {
                    bid += 1f;
                    spareSpades -= 2;
                    countK = true;
                    potentialBidString += " king + more than 2 spare spades = 1" + '\n' + "spades factor = " + bid;
                }
                else if (cards.HasRank(Card.RankType.Queen) && !countQ && spareSpades >= 3)
                {
                    bid += 1f;
                    spareSpades -= 3;
                    countQ = true;
                    potentialBidString += " queen + more than 3 spare spades = 1" + '\n' + "spades factor = " + bid;
                }
                else if (cards.HasRank(Card.RankType.Jack) && !countJ && spareSpades >= 4)
                {
                    bid += 1f;
                    spareSpades -= 4;
                    countJ = true;
                    potentialBidString += " jack + more than 4 spare spades = 1" + '\n' + "spades factor = " + bid;
                }
                else if (doubletons > 0 && !countDoubleton && spareSpades >= 1)
                {
                    bid += 0.5f;
                    spareSpades -= 1;
                    countDoubleton = true;
                    potentialBidString += " doubletons + more than 1 spare spades = 1" + '\n' + "spades factor = " + bid;
                }
                else
                {
                    if (spareSpades > 3)
                    {
                        if (spareSpades == 5)
                        {
                            bid += 1.11f;
                            potentialBidString += " 5 spare spades = 1.11" + '\n' + "spades factor = " + bid;
                        }

                        if (spareSpades == 4)
                        {
                            bid += 0.11f;
                            potentialBidString += " 4 spare spades = 0.11" + '\n' + "spades factor = " + bid;
                        }
                    }

                    spareSpades = 0;
                }
            }

            potentialBidString += " for all the spades = " + bid;
            result += bid;
            potentialBidString += " first result = " + result;

            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, potentialBidString);
            return result;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        protected float OtherPlayersFactor(int biddingPosition, int myBid)
        {
            float result = 0;

            int totalBids = CalcTotalBids() + myBid;
            string OtherPlayersFactorString = "";
            OtherPlayersFactorString += "totalBids = " + totalBids;

            switch (biddingPosition)
            {
//                default:
//                case 1:
//                case 2:
//                     As suggested by http://www.sharkfeeder.com/spadsbook/basicbidding.html
//                     If you are 1st or 2nd to bid, keep in mind that your partner may go nil.
//                     You should slightly undervalue short suits and high non-trump if you also have low cards in that suit.
//                     The reason is because if you are protecting a nil, you'll often forgo the opportunity to take tricks for the opportunity to dump low cards.
//
//                    result = -0.5f;
//                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"1st partner - slightly underbid to allow cover if partner goes nil = " + result);
//                    break;

                case 4:
                    if (totalBids < 11)
                    {
                        result += Math.Min(0.3f * (11 - totalBids), 1.5f);
                        OtherPlayersFactorString += "position 4 total bids < 11 factor = " + result;
                    }
                    else if (totalBids > 12)
                    {
                        result += Math.Max(-0.4f * (totalBids - 12), -1.5f);
                        OtherPlayersFactorString += "position 4 total bids > 12 factor = " + result;
                    }

                    break;
                case 3:
                    if (totalBids < 7)
                    {
                        result += Math.Min(0.2f * (7 - totalBids), 1.0f);
                        OtherPlayersFactorString += "position 3 total bids < 7 factor = " + result;
                    }
                    else if (totalBids > 10)
                    {
                        result += Math.Max(-0.2f * (totalBids - 10), -1.0f);
                        OtherPlayersFactorString += "position 3 total bids > 10 factor = " + result;
                    }

                    break;
            }

            if (Math.Abs(result) > 0.1)
            {
                OtherPlayersFactorString += "other player factor = " + result;
            }

            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, OtherPlayersFactorString);
            return result;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        protected int bidValidation(Position pos, int biddingPosition, int bid)
        {
            if (bid == -1 | bid <= -3)
            {
                // // LoggerController.Instance.LogErrorFormat(// // LoggerController.Module.AI, "BUG: bid == -1 | bid <= -3");
                bid = 1;
            }

            if (bid > 13)
            {
                // // LoggerController.Instance.LogErrorFormat(// // LoggerController.Module.AI, "BUG: bid > 13");
                bid = 10;
            }

            if (spadesBrain.MatchData.SubVariation == PlayingSubVariant.NoNil && bid == 0)
            {
                // // LoggerController.Instance.LogErrorFormat(// // LoggerController.Module.AI, "BUG: NoNil variant but bid nil");
                bid = 1;
            }
            // If the partner did not bid so far, there is nothing to validate.
            if (biddingPosition < 3)
            {
                return bid;
            }
            else // Make sure the combined bid of the player and the partner is no more than 13.
            {
                int partnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;
                return Math.Min(bid, 13 - partnerBid);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static float SoloNilChances(CardsList hand)
        {
            Dictionary<Card.SuitType, float> nilChancePerSuit = new Dictionary<Card.SuitType, float>();
            Dictionary<int, float> soloNil1 = m_solo_nil_1_list.ToDictionary(x => int.Parse(x[0]), x => float.Parse(x[1]));
            Dictionary<string, float> soloNil2 = m_solo_nil_2_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> soloNil3 = m_solo_nil_3_list.ToDictionary(x => x[0] + x[1] + x[2] + x[3], x => float.Parse(x[4]));

            Card.SuitType[] allSuits = {Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts, Card.SuitType.Spades};
            string soloNilChancesString = "NilChances (Solo): ";

            foreach (Card.SuitType suit in allSuits)
            {
                var cards = hand.CardsBySuits(suit);
                cards.SortHighToLowRankFirst();
                nilChancePerSuit.Add(suit, 0);
                List<int> cardsRanks = new List<int>();

                foreach (Card card in cards)
                    cardsRanks.Add((int) card.Rank);

                float value;
                if (cardsRanks.Count == 1)
                {
                    soloNil1.TryGetValue(cardsRanks[0], out value);
                    nilChancePerSuit[suit] = value;
                }
                else if (cardsRanks.Count == 2)
                {
                    var doubletonCards = cardsRanks[1].ToString() + cardsRanks[0];
                    soloNil2.TryGetValue(doubletonCards, out value);
                    nilChancePerSuit[suit] = value;
                }
                else if (cardsRanks.Count >= 3)
                {
                    String threePlusCards = cardsRanks[cardsRanks.Count - 1].ToString() +
                                            cardsRanks[cardsRanks.Count - 2] +
                                            cardsRanks[cardsRanks.Count - 3] + cardsRanks.Count;
                    soloNil3.TryGetValue(threePlusCards, out value);
                    nilChancePerSuit[suit] = value;
                }
                else
                {
                    value = 1.15f;
                    nilChancePerSuit[suit] = value;
                }

                if (suit == Card.SuitType.Spades &&
                    (cardsRanks.Count >= 4 ||
                     hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Ace) ||
                     hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.King) ||
                     hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Queen)))
                {
                    value = 0;
                    soloNilChancesString += "spades problem (Ace/King/Queen or more than 4 cards) = " + value;
                    nilChancePerSuit[suit] = value;
                }
            }

            float nilChance = 1;
            foreach (KeyValuePair<Card.SuitType, float> kvp in nilChancePerSuit)
            {
                nilChance *= kvp.Value;
                soloNilChancesString += $"{kvp.Key}: {hand.CardsBySuits(kvp.Key)}, Value = {kvp.Value}";
            }

            soloNilChancesString += "nil chance = " + nilChance;
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, soloNilChancesString);
            return nilChance;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static float PartnersNilChances(CardsList hand)
        {
            Dictionary<Card.SuitType, float> nilChancePerSuit = new Dictionary<Card.SuitType, float>();
            Dictionary<int, float> partnersNilSideSuit1 = m_p_nil_side_1_list.ToDictionary(x => int.Parse(x[0]), x => float.Parse(x[1]));
            Dictionary<string, float> partnersNilSideSuit2 = m_p_nil_side_2_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> partnersNilSideSuit3 = m_p_nil_side_3_list.ToDictionary(x => x[0] + x[1] + x[2] + x[3], x => float.Parse(x[4]));
            Dictionary<int, float> partnersNilSpades1 = m_p_nil_spades_1_list.ToDictionary(x => int.Parse(x[0]), x => float.Parse(x[1]));
            Dictionary<string, float> partnersNilSpades2 = m_p_nil_spades_2_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> partnersNilSpades3 = m_p_nil_spades_3_list.ToDictionary(x => x[0] + x[1] + x[2], x => float.Parse(x[3]));
            Card.SuitType[] allSuits = {Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts, Card.SuitType.Spades};
            string nilChancesString = "NilChances (Partners): ";

            foreach (Card.SuitType suit in allSuits)
            {
                nilChancePerSuit.Add(suit, 0);

                CardsList cards = hand.CardsBySuits(suit);
                cards.SortHighToLowRankFirst();
                List<int> cardsRanks = new List<int>();
                foreach (Card card in cards)
                    cardsRanks.Add((int) card.Rank);
                
                float value;
                if (cardsRanks.Count == 1)
                {
                    if (suit == Card.SuitType.Spades)
                    {
                        partnersNilSpades1.TryGetValue(cardsRanks[0], out value);
                        nilChancePerSuit[suit] = value;
                    }
                    else
                    {
                        partnersNilSideSuit1.TryGetValue(cardsRanks[0], out value);
                        nilChancePerSuit[suit] = value;
                    }
                }
                else if (cardsRanks.Count == 2)
                {
                    var keyDoubleton = cardsRanks[1].ToString() + cardsRanks[0];
                    if (suit == Card.SuitType.Spades)
                    {
                        partnersNilSpades2.TryGetValue(keyDoubleton, out value);
                        nilChancePerSuit[suit] = value;
                    }
                    else
                    {
                        partnersNilSideSuit2.TryGetValue(keyDoubleton, out value);
                        nilChancePerSuit[suit] = value;
                    }
                }
                else if (cardsRanks.Count >= 3)
                {
                    String key3;
                    if (suit == Card.SuitType.Spades)
                    {
                        key3 = cardsRanks[cardsRanks.Count - 1].ToString() +
                               cardsRanks[cardsRanks.Count - 2] +
                               cardsRanks[cardsRanks.Count - 3];
                        if (cardsRanks.Count >= 4)
                        {
                            value = 0;
                            nilChancePerSuit[suit] = value;
                        }
                        else
                        {
                            partnersNilSpades3.TryGetValue(key3, out value);
                            nilChancePerSuit[suit] = value;
                        }
                    }
                    else // not spades
                    {
                        key3 = cards.Count.ToString() + cardsRanks[cardsRanks.Count - 1] +
                               cardsRanks[cardsRanks.Count - 2] +
                               cardsRanks[cardsRanks.Count - 3]; // + cardsRanks[cardsRanks.Count - 4] ;
                        partnersNilSideSuit3.TryGetValue(key3, out value);
                        nilChancePerSuit[suit] = value;
                    }
                }
                else
                {
                    value = 1.15f;
                    nilChancesString += "void nil chance == " + value;
                    nilChancePerSuit[suit] = value;
                }

                if (suit == Card.SuitType.Spades &&
                    (cardsRanks.Count >= 4 ||
                     hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Ace) ||
                     nilChancePerSuit[Card.SuitType.Spades] <= 0.25))
                {
                    value = 0;
                    nilChancesString += "spades problem (Ace or more than 4 spades) = " + value;
                    nilChancePerSuit[suit] = value;
                }
            }

            float nilChance = 1;
            foreach (KeyValuePair<Card.SuitType, float> kvp in nilChancePerSuit)
            {
                nilChance *= kvp.Value;
                nilChancesString += $"{kvp.Key}: {hand.CardsBySuits(kvp.Key)}, Value = {kvp.Value}";
            }

            nilChancesString += "nil chance = " + nilChance;
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, nilChancesString);
            return nilChance;
        }
    }
}
