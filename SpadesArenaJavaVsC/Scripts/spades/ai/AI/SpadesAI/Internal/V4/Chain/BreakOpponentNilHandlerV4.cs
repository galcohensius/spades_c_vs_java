﻿using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using SpadesAI.model;
using CommonAI;
using SpadesAI.Internal;
using spades.models;
using spades.utils;

namespace SpadesAI.V4.Chain
{
    public class BreakOpponentNilHandlerV4 : AbstractHandler
	{
        private const string StrategyFieldName = "BON";

		public BreakOpponentNilHandlerV4(SpadesBrainImpl spadesBrain):base(spadesBrain)
		{ 
		}
        
        public override void Handle (AITurnData turnData){
            if (turnData.OpponentNilPositions.Count == 0  )
            {
                nextHandler.Handle(turnData);
                return;
            }
            int strategyRule = 0;
		    string deck = spadesBrain.PlayingBrain is PlayingBrainJokers ? "jokers" : "regular";
		    Position myPos = turnData.PlayerPos;
		    Position partnerPos = SpadesUtils.PartnerPosition(myPos);
		    Position rhoPos = SpadesUtils.RhoPosition(myPos);
		    Position lhoPos = SpadesUtils.LhoPosition(myPos);

            // Solo:
            // Do not break nil when:
            // *.you are in position { 1,2,3} and Contract isn't Fulfilled yet and:
            // 1. someone signaled to stop BOP by playing sidesuit boss
            // 3. If the total bid is >=12 
            // 4. last 5 tricks
            if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                if (spadesBrain.CardsOnTable.Count < 3 && !spadesBrain.Players[myPos].MyContractFulfilled)
                {
                    int southPointsIfBetMet = spadesBrain.MatchData.MatchPointsIfBetMet[Position.South];
                    List<int> pointsIfBetMetListNoSouthList = new List<int> {spadesBrain.MatchData.MatchPointsIfBetMet[Position.East],
                                                                             spadesBrain.MatchData.MatchPointsIfBetMet[Position.North],
                                                                             spadesBrain.MatchData.MatchPointsIfBetMet[Position.West] };

                    // Set nil at all costs if (South) will win the game by making the nil
                    if (turnData.OpponentNilPositions.Contains(Position.South)  &&
                        southPointsIfBetMet >= spadesBrain.MatchData.WinConditionPoints && 
                        southPointsIfBetMet > pointsIfBetMetListNoSouthList.Max())
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) Set nil at all costs if (South) will win the game this round by making her nil");
                    }
                    else
                    {
                        // Non-niler signaled to stop BOP by playing sidesuit boss
                        if (spadesBrain.Players[myPos].StopToBreakOpponentNil)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) Non-niler signaled to stop BOP by playing sidesuit boss: not trying to break Nil");
                            nextHandler.Handle(turnData);
                            return;
                        }

                        // Over game & didn't make your bid yet
                        if (turnData.TotalBids + turnData.TotalOverTakes >= 12)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} don't break {1} Nil: since bids + over takes >= 12", myPos, turnData.OpponentNilPositions[0]);
                            nextHandler.Handle(turnData);
                            return;
                        }

                        // Don't execute in the last 5 tricks.
                        if (spadesBrain.Players[myPos].Hand.Count <= 5)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} don't break {1} Nil: since last 5 tricks", myPos, turnData.OpponentNilPositions[0]);
                            nextHandler.Handle(turnData);
                            return;
                        }
                    }
                }
            }

            // Partners:
            // 1. set nil at all cost if opponents will win the game if making the nil even if coverer is set.
            //   (if coverer bid is already met, set nil at all cost if opponents will win the game if making the nil + making the coverer.)
            
            // Do not break nil when:
            // *. you are in position {1,2,3} and Contract isn't Fulfilled yet but still possible and:
            // 1. Partner signaled to stop BOP by playing side-suit boss
            // 2. If we win this round we win the game & opponents will have less points than us
            // 3. If the total bid is >=13 
            // 4. last 5 tricks
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                if (spadesBrain.CardsOnTable.Count < 3 
                    && !spadesBrain.Players[myPos].PartnersContractFulfilled 
                    && StillCanMakeBid(myPos, partnerPos)) // set nil at all cost if you pos4 or if our contract is fulfilled or if we can't make our bid
                {
                    int ourPointsIfWinRound       = spadesBrain.MatchData.MatchPointsIfBetMet[myPos] + spadesBrain.MatchData.MatchPointsIfBetMet[partnerPos];
                    int opponentsPointsIfWinRound = spadesBrain.MatchData.MatchPointsIfBetMet[rhoPos] + spadesBrain.MatchData.MatchPointsIfBetMet[lhoPos];
                    int opponentsPointsIfNilMakesCoverSetIfStillCan = GetOpponentsPointsIfNilerWinCoverSet(rhoPos, lhoPos);

                    // 1. set nil at all cost if opponents will win the game if making the nil even if coverer is set.
                    // If coverer bid is already met, set nil at all cost if opponents will win the game if making the nil + making the coverer.
                    if (opponentsPointsIfNilMakesCoverSetIfStillCan >= spadesBrain.MatchData.WinConditionPoints &&
                        opponentsPointsIfNilMakesCoverSetIfStillCan > ourPointsIfWinRound &&
                        spadesBrain.MatchData.MatchBags[lhoPos] + spadesBrain.MatchData.MatchBags[rhoPos] <= spadesBrain.MatchData.BagsToIncurPenalty - 1)
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) Set nil at all costs if opponents will win the game if nil succeed, even if cover is set");
                    }
                    else
                    {
                        // 1.Partner signaled to stop BOP by playing sidesuit boss
                        if (spadesBrain.Players[myPos].StopToBreakOpponentNil)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) Partner signaled to stop BOP by playing sidesuit boss: not trying to break Nil");
                            nextHandler.Handle(turnData);
                            return;
                        }

                        // 2. If we win this round we win the game & opponents will have less points than us
                        if (ourPointsIfWinRound >= spadesBrain.MatchData.WinConditionPoints && 
                            ourPointsIfWinRound > opponentsPointsIfWinRound && 
                            spadesBrain.MatchData.MatchBags[myPos] <= spadesBrain.MatchData.BagsToIncurPenalty - 2)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} don't break {1} Nil: since we only need to make our contract to win the game",myPos, turnData.OpponentNilPositions[0]);
                            nextHandler.Handle(turnData);
                            return;
                        }

                        // 3. If the total bid is >=13
                        if (turnData.TotalBids + turnData.TotalOverTakes >= 13)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} don't break {1} Nil: since bids + overTakes >= 11, go for setting the coverer", myPos, turnData.OpponentNilPositions[0]);
                            nextHandler.Handle(turnData);
                            return;
                        }

                        // 4. Don't execute in the last 5 tricks.
                        if (spadesBrain.Players[myPos].Hand.Count <= 5)
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} don't break {1} Nil: since last 5 tricks", myPos, turnData.OpponentNilPositions[0]);
                            nextHandler.Handle(turnData);
                            return;
                        }
                    }
                }
            }



            // Iterate over every opponent who bid nil and look for one who can be broken.
            foreach (Position opponentNilPosition in turnData.OpponentNilPositions) {
				// Check if nil bidder already threw a card	
				if (spadesBrain.CardsOnTable.ContainsKey (opponentNilPosition))
                {
					Card nilBidderCard = spadesBrain.CardsOnTable [opponentNilPosition];

					// if Niler's card is highest on the table
					if (SpadesUtils.FindWinningPositionOnTable (spadesBrain) == opponentNilPosition) {

                        if (turnData.CardsToUse.CountSuit(spadesBrain.LeadingSuit) > 0)  // not Void in leading suit
                        {
                            Card lowest3Card = CardsUtils.GetXLowestCardInSuit(2, (Card.SuitType)spadesBrain.LeadingSuit, deck);
                            if (nilBidderCard.Rank <= lowest3Card.Rank && spadesBrain.CardsOnTable.Count < 3)
                            {
                                // Do not try to break, since most chances Niler will be covered by later players
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON), Do not try to break, since most chances Niler will be covered by later players");
                            }
                            else
                            {
                                Card strongestCardBelowRank = turnData.CardsToUse.CardsBySuits(nilBidderCard.Suit).GetStrongestBelowRank(nilBidderCard.Rank);
                                if (strongestCardBelowRank != null) // Try to break Niler
                                {
                                    // Use the strongest losing card, since weaker cards might be required to break later.
                                    turnData.CardToPlay = strongestCardBelowRank;
                                    if (turnData.CardToPlay != null)
                                    {
                                        strategyRule = 1;
                                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} Break {1} Nil: strongest losing card", myPos, opponentNilPosition);
                                    }
                                }
                                else // Cant break Niler - don't have lower card than Niler
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON), {0} Cant break Niler - don't have lower card than Niler", myPos);
                                }
                            }
                        }
                        else // Void in leading suit
                        {
                            if (spadesBrain.LeadingSuit != null)
                            {
                                List<Card.SuitType> nonSpadeSuitsList = SuitUtils.GetOtherSuits(new List<Card.SuitType>{Card.SuitType.Spades, spadesBrain.LeadingSuit.Value});
                                CardsList nonSpadeCardsList = turnData.CardsToUse.CardsBySuits(nonSpadeSuitsList);
                                if (nonSpadeCardsList.Count > 0) {
                                    // Try to break.
                                    // Use a medium non spade card, since weaker cards might be required to break later.
                                    turnData.CardToPlay = nonSpadeCardsList.GetMediumCard() ; 
                                    strategyRule = 2;
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0}, break nil, {1} nil: cannot follow suit, medium losing side suit", myPos, opponentNilPosition);
                                }
                                else // Niler's card is highest on the table, hero is Void in leading suit but have only spades
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON), Niler's card is highest on the table, Void in leading suit but have only spades - go to next handler" );
                                }
                            }
                        }

					}
                    else  // Niler is already covered.
                    {
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} (BON), Niler {1} already covered", myPos, opponentNilPosition);
                    }
				}
                else // Niler hasn't played yet
                {
					HashSet<Card.SuitType> nilerVoids = spadesBrain.Players [opponentNilPosition].VoidSuits;
                    HashSet<Card.SuitType> covererVoids = spadesBrain.Players[SpadesUtils.PartnerPosition(opponentNilPosition)].VoidSuits;
                    nilerVoids.UnionWith(covererVoids);

                    if (spadesBrain.CardsOnTable.Count == 0) // Leader
                    {    
                        List<Card.SuitType> nilBidderNonVoidsList = SuitUtils.GetOtherSuits(nilerVoids);  // Remove Niler's void suits
                        CardsList nonVoidCardsList = turnData.CardsToUse.CardsBySuits(nilBidderNonVoidsList);
                        if (nonVoidCardsList.Count > 0)
                        {
                            turnData.CardToPlay = nonVoidCardsList.GetWeakest();
							strategyRule = 3;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} Break {1} nil: Lead weakest card in Niler's non void suit", myPos, opponentNilPosition);
                        }
                        else  // nonVoidCardsList.Count == 0
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON), nonVoidCardsList.Count == 0, niler is void in all the suits I hold");
                        }
                    }
                    else // hero is follower before Niler (Pos 2 or 3)
                    {
                        Card[] cardsOnTable = new Card[spadesBrain.CardsOnTable.Count];
                        spadesBrain.CardsOnTable.Values.CopyTo(cardsOnTable, 0);
                        Card tableWinnerCard = SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsOnTable);  // current Table Winner Card
                        Card tableWinnerOneRankHigherCard = SpadesUtils.CalcOneHigherRankCard(tableWinnerCard, spadesBrain);

                        if (tableWinnerCard.Suit == Card.SuitType.Spades && tableWinnerCard.Suit != spadesBrain.LeadingSuit)
                        {  // Someone cut before Niler - Niler is safe
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, " {0} (BON), Someone cut before Niler - Niler is safe", myPos);
                        }
                        else
                        {
                            if (spadesBrain.LeadingSuit != null && nilerVoids.Contains((Card.SuitType)spadesBrain.LeadingSuit))  // Niler void in leading suit 
                            {
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, " {0} (BON), Niler void in leading suit, continue ", myPos);
                                // changing the strategy is not enough, since for example the bot might cut with spades right after a low card is thrown by RHO.
                                // Change the strategy to under to affect the next handlers.
                                // This will cause high cards to be used as strongest losers, and low cards to be kept for later use.
                            }
                            else // Niler isn't void in leading suit
                            {
                                if (spadesBrain.LeadingSuit != null)
                                {
                                    CardsList myCardsFromLeadingSuit = turnData.CardsToUse.CardsBySuits((Card.SuitType)spadesBrain.LeadingSuit);
                                    if (myCardsFromLeadingSuit.Count > 0) // hero isn't void in leading suit
                                    {
                                        // strongest Below "highest+1" card on the table (and below king)
                                        Card strongestCardBelowCurrentPlusOne = myCardsFromLeadingSuit.GetStrongestBelowOrEqualRank(tableWinnerOneRankHigherCard.Rank);

                                        if (strongestCardBelowCurrentPlusOne != null) // Try to break Niler
                                        {
                                            // don't waste a King under Ace.  In jokers: don't waste a SJ under BJ
                                            Card.RankType KorSJ = CardsUtils.GetXHighestCardInSuit(1,(Card.SuitType) spadesBrain.LeadingSuit, deck).Rank;
                                            Card.RankType AorBJ = CardsUtils.GetXHighestCardInSuit(0,(Card.SuitType) spadesBrain.LeadingSuit, deck).Rank;
                                            if (strongestCardBelowCurrentPlusOne.Rank == KorSJ && tableWinnerCard.Rank == AorBJ)
                                            {
                                                strongestCardBelowCurrentPlusOne = myCardsFromLeadingSuit.GetStrongestBelowRankBelowSecondHighest(tableWinnerOneRankHigherCard.Rank, tableWinnerOneRankHigherCard.Suit,deck);
                                            }
                                            
                                            // Use winner by one or the strongest losing card, since weaker cards might be required to break later.
                                            turnData.CardToPlay = strongestCardBelowCurrentPlusOne;
                                            {
                                                strategyRule = 6;
                                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} Break {1} Nil: winner by one or strongest losing card that is not a king/SJ", myPos, opponentNilPosition);
                                            }
                                        }
                                        else // don't have lower card than table Winner
                                        {
                                            turnData.CardToPlay = myCardsFromLeadingSuit[myCardsFromLeadingSuit.Count - 1];
                                            {
                                                strategyRule = 7;
                                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(BON) {0} don't have lower card than table Winner - play weakest card", myPos);
                                            }
                                        }
                                    }
                                    else // hero is void in leading suit (and Niler isn't void in leading suit )
                                    {
                                        if (cardsOnTable.Length == 2 && cardsOnTable.First().Rank > cardsOnTable.Last().Rank) // partner is table strongest
                                        { 
                                            // play medium non-spade
                                            List<Card.SuitType> nonSpadeSuitsList = SuitUtils.GetOtherSuits(new List<Card.SuitType> { Card.SuitType.Spades });
                                            CardsList nonSpadeCardsList = turnData.CardsToUse.CardsBySuits(nonSpadeSuitsList);
                                            turnData.CardToPlay = nonSpadeCardsList.GetMediumCard();
                                            if (turnData.CardToPlay != null)
                                            {
                                                strategyRule = 8;
                                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, " {0} (BON), hero is void in leading suit - play medium non-spade ", myPos);
                                            }
                                        }
                                        else // opponent is currently table strongest
                                        {
                                            //if opponent played boss, then cut low, else play medium non-spade

                                            if (tableWinnerCard.Rank == SpadesUtils.CalcBoss(spadesBrain.LeadingSuit.Value, spadesBrain).Rank )
                                            {
                                                // cut low
                                                turnData.CardToPlay = turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades).GetWeakest();
                                                if (turnData.CardToPlay != null)
                                                {
                                                    strategyRule = 9;
                                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, " {0} (BON), hero is void in leading suit and coverer  ", myPos);
                                                }
                                            }
                                            else
                                            {
                                                // play medium non-spade
                                                List<Card.SuitType> nonSpadeSuitsList = SuitUtils.GetOtherSuits(new List<Card.SuitType> { Card.SuitType.Spades });
                                                CardsList nonSpadeCardsList = turnData.CardsToUse.CardsBySuits(nonSpadeSuitsList);
                                                turnData.CardToPlay = nonSpadeCardsList.GetMediumCard();
                                                if (turnData.CardToPlay != null)
                                                {
                                                    strategyRule = 10;
                                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, " {0} (BON), hero is void in leading suit - play medium non-spade ", myPos);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
				}
			}
			
			if (turnData.CardToPlay == null) {
				nextHandler.Handle(turnData);
            } else {
                turnData.PlayerRoundData.Strategies += StrategyFieldName+strategyRule+turnData.CardToPlay+",";
            }
		}


        private bool StillCanMakeBid(Position myPos, Position partnerPos)
        {
            return spadesBrain.Players[myPos].Takes + spadesBrain.Players[partnerPos].Takes + spadesBrain.Players[myPos].Hand.Count >=
                   spadesBrain.Players[myPos].Bid + spadesBrain.Players[partnerPos].Bid;
        }


        private int GetOpponentsPointsIfNilerWinCoverSet(Position rhoPos, Position lhoPos)
        {
            int opponentsPointsIfNilerWinCoverSet;
            AIPlayer rho = spadesBrain.Players[rhoPos];
            AIPlayer lho = spadesBrain.Players[lhoPos];

            if (rho.Bid == 0 && lho.Bid == 0) // both opponents bid nil
            {
                opponentsPointsIfNilerWinCoverSet =
                    spadesBrain.MatchData.MatchPoints[rhoPos] + spadesBrain.MatchData.MatchPoints[lhoPos] +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, rho.Bid, rho.BlindNil) +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, lho.Bid, lho.BlindNil);
            }
            else
            {
                if (rho.Bid == 0) // rho is niler
                {
                    opponentsPointsIfNilerWinCoverSet =
                        spadesBrain.MatchData.MatchPoints[rhoPos] + spadesBrain.MatchData.MatchPoints[lhoPos] +
                        SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, rho.Bid, rho.BlindNil);

                    // if coverer did not fulfilled yet her bid
                    if (lho.Takes < lho.Bid)
                    {
                        opponentsPointsIfNilerWinCoverSet -= SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, lho.Bid, lho.BlindNil);
                    }
                    else
                    {
                        opponentsPointsIfNilerWinCoverSet += SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, lho.Bid, lho.BlindNil);
                    }

                }
                else // lho is niler
                {
                    opponentsPointsIfNilerWinCoverSet =
                        spadesBrain.MatchData.MatchPoints[rhoPos] + spadesBrain.MatchData.MatchPoints[lhoPos] +
                        SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, lho.Bid, lho.BlindNil);

                    // if coverer did not fulfilled yet her bid
                    if (rho.Takes < rho.Bid)
                    {
                        opponentsPointsIfNilerWinCoverSet -= SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, rho.Bid, rho.BlindNil);
                    }
                    else
                    {
                        opponentsPointsIfNilerWinCoverSet += SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, rho.Bid, rho.BlindNil);
                    }
                }
            }
            return opponentsPointsIfNilerWinCoverSet;
        }
    }
}
