﻿using SpadesAI.model;
using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using spades.utils;
using SpadesAI.Internal;

namespace SpadesAI.V4.Chain
{
    public class ProtectPartnerNilHandlerV4:AbstractHandler
	{
        private const string StrategyFieldName = "PPN";

		public ProtectPartnerNilHandlerV4 (SpadesBrainImpl spadesBrain):base(spadesBrain)
		{
		}
		public override void Handle (AITurnData turnData)
		{
			if (!turnData.ProtectPartnerNil) {
				nextHandler.Handle (turnData);
				return;
			}
			int strategyRule = 0;
            string deck = spadesBrain.PlayingBrain is PlayingBrainJokers ? "jokers" : "regular";
            Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);

            // Check if the partner has already threw a card
            if (spadesBrain.CardsOnTable.ContainsKey(partnerPos)) {
                Card partnerCard = spadesBrain.CardsOnTable[partnerPos];

                // Check if the partner's card is the highest on the table 
                if (SpadesUtils.FindWinningPositionOnTable(spadesBrain) == partnerPos) {
                    if (turnData.CardsToUse.CountSuit(partnerCard.Suit) > 0)
                    { // follow suit
                        Card weakestCardAboveRank = turnData.CardsToUse.CardsBySuits(partnerCard.Suit).GetWeakestAboveRank(partnerCard.Rank);
                        if (weakestCardAboveRank != null)
                        {
                            // Use a weak card, since a strong card might be required to protect later.
                            turnData.CardToPlay = weakestCardAboveRank;
                            strategyRule = 1;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: weakest higher than partner", turnData.PlayerPos);
                        }
                        else // have to follow but can't cover
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: can't cover, follow lower than niler", turnData.PlayerPos);
                        }
                    }
                    else { // can't follow suit
                        CardsList spadeCardsList = turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades);
                        if (spadeCardsList.Count > 0) {
                            // Use a weak spade card, since higher spade cards should be used to protect later.
                            turnData.CardToPlay = spadeCardsList[spadeCardsList.Count - 1];

							if (turnData.CardToPlay != null) {
								strategyRule = 2;
								// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: weakest spade", turnData.PlayerPos);
                            }
                        }
                        else // can't follow suit, dont have spades
                        {
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: can't cover since don't have spades", turnData.PlayerPos);
                        }
                    }
                } else {
                    // Already covered
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: Niler is safe", turnData.PlayerPos);
                }
            } else {
                // partner has not played yet
                HashSet<Card.SuitType> partnerVoids = spadesBrain.Players[partnerPos].VoidSuits;

                // Leader: 
                if (spadesBrain.CardsOnTable.Count == 0)
                {   
                    // 1. lead partner's void suit
                    // 2. if partner lead the first trick, lead that suit
                    // 3. lead safe according to: SafeCoverNilDictionary
                    // 4. lead high

                    if (partnerVoids.Count > 0) {
                        // 1. lead partner's void suit
                        CardsList voidCardsList = turnData.CardsToUse.CardsBySuits(partnerVoids);
                        if(voidCardsList.Count > 0) {
                            turnData.CardToPlay = voidCardsList[voidCardsList.Count - 1];
                            if (turnData.CardToPlay != null)
                            {
                                strategyRule = 3;
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: Lead, weakest card of partner's void suit", turnData.PlayerPos);
                            }
                        }
                    }
                    // 2. if partner lead the first trick, lead that suit
                    if (turnData.CardToPlay == null)
                    { 
                        if (spadesBrain.Players[partnerPos].IsNilerWasFirstLeader)
                        {
                            CardsList nilerLeadSuitCardsList = turnData.CardsToUse.CardsBySuits(spadesBrain.Players[partnerPos].PartnersFirstLeadSuit);
                            if (nilerLeadSuitCardsList.Count > 0)
                            {
                                nilerLeadSuitCardsList.SortHighToLowRankFirst();
                                // re-lead that suit if you have a card larger than 8
                                if (nilerLeadSuitCardsList[0].Rank > CardsUtils.GetXLowestCardInSuit(6, spadesBrain.Players[partnerPos].PartnersFirstLeadSuit, deck).Rank )
                                {
                                    turnData.CardToPlay = nilerLeadSuitCardsList[0];
                                    if (turnData.CardToPlay != null)
                                    {
                                        strategyRule = 4;
                                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: Lead: re-lead partners first lead suit, highest in suit", turnData.PlayerPos);
                                    }
                                }
                            }
                        }
                    }
                    // 3.lead safe according to: SafeCoverNilDictionary
                    if (turnData.CardToPlay == null)
                    {
                        CardsList safeCoverCardsList = new CardsList();
                        CardsList unplayedCardsList = SpadesUtils.GetUnplayedDeck(spadesBrain);
                        foreach (Card myCard in turnData.CardsToUse)
                        {
                            Card.RankType safeDicRank = spadesBrain.Players[partnerPos].SafeCoverNilDictionary[myCard.Suit] ;
                            // if safe dictionary has Ace, than only Ace is safe, if niler played already a safe card then its that card-1
                            Card strongestBelowSafeCard = unplayedCardsList.GetStrongestBelowRank(safeDicRank);
                            Card.RankType safeTouchFromBelow = 0;
                            if (safeDicRank == CardsUtils.GetXHighestCardInSuit(0, myCard.Suit, deck).Rank)
                                safeTouchFromBelow = CardsUtils.GetXHighestCardInSuit(0, myCard.Suit, deck).Rank;
                            else if (strongestBelowSafeCard != null)
                            {
                                safeTouchFromBelow = strongestBelowSafeCard.Rank;
                            }
                                
                            if (myCard.Rank >= safeTouchFromBelow)
                            {
                                safeCoverCardsList.Add(myCard);
                            }
                        }
                        if (safeCoverCardsList.Count > 0)
                        {
                            strategyRule = 8;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: safe according to: SafeCoverNilDictionary, holding {1} safe cards", turnData.PlayerPos, safeCoverCardsList.Count);
                            turnData.CardToPlay = safeCoverCardsList.First();
                        }
                    }
                    // 4. lead high
                    if (turnData.CardToPlay == null) {
                        turnData.CardToPlay = turnData.CardsToUse.GetStrongCard();
                        if (turnData.CardToPlay != null)
                        {
							strategyRule = 5;
							// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: Lead, strongest card", turnData.PlayerPos);
                        }
                    }
                } else {
                    // Bot is second
                    // If the RHO threw a card weaker than 10, which the partner is not void in, and its not in the safe SafeCoverNilDictionary
                    Card rhoCard = spadesBrain.CardsOnTable[SpadesUtils.RhoPosition(turnData.PlayerPos)];
                    if (rhoCard.Rank < Card.RankType.Jack && !partnerVoids.Contains(rhoCard.Suit))
                    {
                        CardsList unplayedCardsFromRhoSuit = SpadesUtils.GetUnplayedDeck(spadesBrain).CardsBySuits(rhoCard.Suit);
                        Card strongestSafeCard = unplayedCardsFromRhoSuit.GetStrongestBelowRank(spadesBrain.Players[partnerPos].SafeCoverNilDictionary[rhoCard.Suit]);
                        Card.RankType safeTouchFromBelow = 0;

                        if (spadesBrain.Players[partnerPos].SafeCoverNilDictionary[rhoCard.Suit] == Card.RankType.Ace)
                            safeTouchFromBelow = Card.RankType.Ace;
                        else if (strongestSafeCard != null)
                        {
                            safeTouchFromBelow = strongestSafeCard.Rank;
                        }
                            
                        if (rhoCard.Rank < safeTouchFromBelow)
                        {
                            // DANGER DANGER DANGER!!!
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: ----- DANGER -----" + '\n' + "try to cover", turnData.PlayerPos);

                            if (turnData.CardsToUse.CountSuit(spadesBrain.LeadingSuit) > 0)
                            {
                                // Use a strong card, since the RHO presented a weak card.
                                turnData.CardToPlay = turnData.CardsToUse.GetStrongCard();

                                if (turnData.CardToPlay != null)
                                {
                                    strategyRule = 6;
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: Second, follow strongest card", turnData.PlayerPos);
                                }
                            }
                            else
                            {
                                CardsList spadeCardsList = turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades);
                                if (spadeCardsList.Count > 0)
                                {
                                    // Use a weak spade card, since higher spade cards should be used to protect later.
                                    turnData.CardToPlay = spadeCardsList[spadeCardsList.Count - 1];

                                    if (turnData.CardToPlay != null)
                                    {
                                        strategyRule = 7;
                                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: second, weakest spade", turnData.PlayerPos);
                                    }
                                }
                                else
                                {
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Cover: can't cover, dont have leading suits or spades", turnData.PlayerPos);
                                }
                            }
                        }
                    }
                }
            }
            
            if (turnData.CardToPlay == null) {
                turnData.Strategy = AIStrategy.Over;  // partner is Nil but is covered or we can't protect her on this trick.  Should save high cards to protect her on later tricks.
                nextHandler.Handle(turnData);
            } else {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
	}
}
