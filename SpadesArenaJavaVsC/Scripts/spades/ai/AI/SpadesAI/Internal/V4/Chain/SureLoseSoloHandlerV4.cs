﻿using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain {
    public class SureLoseSoloHandlerV4:AbstractHandler {

        private const string StrategyFieldName = "SLS";
        
        public SureLoseSoloHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {
            if (spadesBrain.MatchData.Mode != PlayingMode.Solo)
            {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            if (turnData.Takes + turnData.TakesForSureFromSpade >= turnData.Bid && turnData.Strategy == AIStrategy.Under) {
                // Under strategy, dump the strongest card that can lose.
                turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(0);

				if (turnData.CardToPlay != null) {
					strategyRule = 1;
					// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(SLS) {0} duck strongest since we made our contract and Under", turnData.PlayerPos);
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}