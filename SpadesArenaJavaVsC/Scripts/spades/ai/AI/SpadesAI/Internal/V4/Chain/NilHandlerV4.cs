﻿using cardGames.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain {
    public class NilHandlerV4 : AbstractHandler {

        private const string StrategyFieldName = "Nil";

        public NilHandlerV4(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {
            if (!turnData.ProtectSelfNil) {
                nextHandler.Handle (turnData);
                return;
            }
            AIPlayer player = spadesBrain.Players[turnData.PlayerPos];
            int strategyRule = 0;
            // Never need to go to the next handler.

            // Lead (only possible at the first trick of the round (or if you set) )
            if (spadesBrain.CardsOnTable.Count == 0 && spadesBrain.Players[turnData.PlayerPos].Hand.Count == 13)
            {
                // Partners
                {
                    player.IsNilerWasFirstLeader = true;  
                    if (player.IsSingletonOrDoubleton)
                    {
                        // from a short suit, play the high card if is smaller than 9.  Your partner will know to re-lead this suit
                        CardsList shortSuit = spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(player.SingletonOrDoubleton);
                        shortSuit.SortHighToLowRankFirst();
                        if (shortSuit.Count == 1)  // Singleton
                        {
                            if (shortSuit[0].Rank <= Card.RankType.Eight)
                            {
                                turnData.CardToPlay = shortSuit[0];
                                strategyRule = 1;
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Nil) {0} Lead, singleton (smaller than 9)", turnData.PlayerPos);
                            }
                        }
                        else  // Doubleton
                        {
                            if (shortSuit[0].Rank <= Card.RankType.Eight)
                            {
                                turnData.CardToPlay = shortSuit[0];
                                strategyRule = 2;
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Nil) {0} Lead, second lowest from doubleton (smaller than 9)", turnData.PlayerPos);
                            }
                            else
                            {
                                // Doubleton with card higher than 8, dont play Doubleton
                                // goto: second lowest from safe suit
                            }
                        }
                    }
                    if (turnData.CardToPlay == null) // second lowest from safe suit
                    {
                        Card clubsSecondLowestCard    = spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(Card.SuitType.Clubs)   .GetSecondWeakestCard() ?? new Card("AC");
                        Card heartsSecondLowestCard   = spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(Card.SuitType.Hearts)  .GetSecondWeakestCard() ?? new Card("AH");
                        Card diamondsSecondLowestCard = spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(Card.SuitType.Diamonds).GetSecondWeakestCard() ?? new Card("AD");

                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Nil) {0} Lead, second lowest from safe suit", turnData.PlayerPos);
                        strategyRule = 3;

                        if (clubsSecondLowestCard.Rank <= heartsSecondLowestCard.Rank && 
                            clubsSecondLowestCard.Rank <= diamondsSecondLowestCard.Rank) 
                            turnData.CardToPlay = clubsSecondLowestCard;
                        else if (heartsSecondLowestCard.Rank <= clubsSecondLowestCard.Rank && 
                                 heartsSecondLowestCard.Rank <= diamondsSecondLowestCard.Rank)
                            turnData.CardToPlay = heartsSecondLowestCard;
                        else turnData.CardToPlay = diamondsSecondLowestCard;
                    }
                }
            }
            // Follow
            else
            {
                // Use the strongest sure loser if exist, 
                // else, use weakest card.

                // Try Dumping the strongest card that can lose.
                turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(0);
                if (turnData.CardToPlay == null)
                {
                    // No sure loser, use weakest card
                    turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
                    strategyRule = 6;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Nil) {0} follow, No sure loser -> weakest card", turnData.PlayerPos);
                }
                else
                {
                    strategyRule = 7;
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "(Nil) {0} follow strongest sure loser", turnData.PlayerPos);
                }
            }
			turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
        }
    }
}
