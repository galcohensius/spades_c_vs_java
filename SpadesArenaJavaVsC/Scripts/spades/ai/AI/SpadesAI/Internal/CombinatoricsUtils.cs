﻿using System.Linq;

namespace SpadesAI
{
	public static class CombinatoricsUtils
	{
		public static int Factorial(int n) {
			if (n<=0) return 1;
			return Enumerable.Range(1, n).Aggregate((acc, x) => acc * x);
		}

		/// <summary>
		/// N choose K
		/// </summary>
		public static int Choose(int n, int k) {
			return Factorial (n) / (Factorial (k) * Factorial (n - k));
		}
	}
}

