﻿using SpadesAI.model;
using spades.models;
using cardGames.models;

namespace SpadesAI
{
	public interface IBiddingBrain
	{
		/// <summary>
		/// Makes the bid for the specified position
		/// </summary>
        int MakeBid(Position pos, SpadesPlayerRoundData prData);

        /// <summary>
        /// Calculate the potential bid for a specific list of cards.
        /// </summary>
        /// <returns>The bid.</returns>
        /// <param name="cardsList">Cards list.</param>
        /// <param name="param"> number of players that might cut you (3 in solo, 2 in partners) </param>
        float PotentialBid(CardsList cardsList, int param);
	}
}
