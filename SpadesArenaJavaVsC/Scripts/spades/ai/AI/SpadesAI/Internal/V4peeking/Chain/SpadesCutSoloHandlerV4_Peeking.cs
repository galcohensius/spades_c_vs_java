﻿using System.Collections.Generic;
using cardGames.models;
using CommonAI;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;
using SpadesBots.Simulation;

namespace SpadesAI.V4.Chain
{
    public class SpadesCutSoloHandlerV4_Peeking : AbstractHandler
    {

        private const string StrategyFieldName = "SCS";

        public SpadesCutSoloHandlerV4_Peeking(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override void Handle(AITurnData turnData)
        {
            if (spadesBrain.MatchData.Mode != PlayingMode.Solo)
            {
                nextHandler.Handle(turnData);
                return;
            }

            // The player can cut with spades when:
            // 1. He is not the first to act.
            // 2. The leading suit is not spades.
            // 3. The player has a spade in the playable cards.
            if (spadesBrain.CardsOnTable.Count == 0 || spadesBrain.LeadingSuit == Card.SuitType.Spades || turnData.CardsToUse.CountSuit(Card.SuitType.Spades) == 0)
            {
                nextHandler.Handle(turnData);
                return;
            }

            // Go on if the player is last, since we know for sure which cards will win or lose, and other handlers will select the card
            // to play
            if (spadesBrain.CardsOnTable.Count == 3)
            {
                nextHandler.Handle(turnData);
                return;
            }

            int strategyRule = 0;


            // cut with the lowest spade that will not be over cut (little peek)
            if (turnData.Takes + turnData.TakesForSureFromSpade < turnData.Bid || turnData.Strategy == AIStrategy.Over)
            {
                Card allVillainsTopSpade = GetHighestSpadeThatCanOverCutYou(turnData);
                CardsList mySpades = spadesBrain.Players[turnData.PlayerPos].Hand.CardsBySuits(Card.SuitType.Spades);
                
                turnData.CardToPlay = mySpades.GetWeakestAboveRank(allVillainsTopSpade.Rank);
                if (turnData.CardToPlay != null)
                {
                    strategyRule = 1;
                    if (Logger.Enabled) Logger.DebugFormat("(SCS) {0} cutting with weakest spade that will win", turnData.PlayerPos);
                }
            }
            else
            {
                // Cut to lose.
                // play your highest spade which is a sure loser.
                AIStrengthCardsList losingSpadeCards = new AIStrengthCardsList(turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades)).GetCardsWithinStrengthRange(0, 0);

                if (losingSpadeCards.Count > 0)
                {
                    turnData.CardToPlay = losingSpadeCards.GetStrongCard();
                    strategyRule = 3;
                    Logger.DebugFormat("(SCS) {0} highest spade which is a sure loser", turnData.PlayerPos);
                }
            }

            if (turnData.CardToPlay == null){
                nextHandler.Handle(turnData);
            }
            else{
                turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }



        private Card GetHighestSpadeThatCanOverCutYou(AITurnData turnData)
        {
            Card allVillainsTopSpade = new Card("2S");
            List<Position> playersYetToAct = PositionUtils.ConstructPlayingPositionsOrder(PositionUtils.GetNextPlayingPosition(turnData.PlayerPos) , spadesBrain.CardsOnTable.Count+1);
            foreach (Position opponentThatMightCutHigher in playersYetToAct)
            {
                // if that player can also cut: check what his highest spade
                if (spadesBrain.Players[opponentThatMightCutHigher].Hand.CardsBySuits((Card.SuitType) spadesBrain.LeadingSuit).Count == 0)
                {
                    CardsList villainSpades = spadesBrain.Players[opponentThatMightCutHigher].Hand.CardsBySuits(Card.SuitType.Spades);
                    Card villainHighestSpade = villainSpades.GetStrongestBelowOrEqualRank(Card.RankType.Ace);
                    if (villainHighestSpade.Rank >= allVillainsTopSpade.Rank)
                    {
                        allVillainsTopSpade = villainHighestSpade;
                    }
                }
            }

            return allVillainsTopSpade;
        }
    }
}
