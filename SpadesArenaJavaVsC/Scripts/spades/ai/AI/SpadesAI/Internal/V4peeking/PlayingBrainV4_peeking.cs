﻿using System.Collections.Generic;
using cardGames.models;
using SpadesAI.model;
using SpadesAI.V1.Chain;
using SpadesAI.V2;
using SpadesAI.V4.Chain;

namespace SpadesAI.V4
{
    public class PlayingBrainV4_peeking : PlayingBrainV2
    {
        internal PlayingBrainV4_peeking(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            // Build the chain of responsibilities handlers
            handler = new PrepareAITurnDataHandlerV4(spadesBrain);    // turn prepare /peeking/

            handler.SetNextHandler(new SingleCard(spadesBrain))                 //1 card possible
                .SetNextHandler(new NilHandlerV4(spadesBrain))                  //Nil_NS (s+p)
                .SetNextHandler(new ProtectPartnerNilHandlerV4(spadesBrain))    //Cover Nil_NS (p)
                .SetNextHandler(new BreakOpponentNilHandlerV4_Peeking(spadesBrain)) // (s) - /peeking/

                .SetNextHandler(new LeadHandlerV4(spadesBrain))                 //Lead a card (s+p)

                .SetNextHandler(new SpadesCutSoloHandlerV4_Peeking(spadesBrain)) //Cut (s) - /peeking/
                .SetNextHandler(new SpadesCutPartnersHandlerV4(spadesBrain))    //Cut (p) 

                .SetNextHandler(new SureWinSoloHandlerV4(spadesBrain))          //Sure win (s)
                .SetNextHandler(new SureWinPartnersHandler(spadesBrain))        //Sure win (p)

                .SetNextHandler(new SureLoseSoloHandlerV4(spadesBrain))         //lose (s)

                .SetNextHandler(new ThirdPosHighPartnersHandlerV4(spadesBrain)) //3rd card high (p)

                .SetNextHandler(new UnknownHandlerV4(spadesBrain));             //default (s+p)
        }


        /// The peeking version of V3, stronger than the non-peeking (obviously)
        internal override float CalcCardStrength(Card card, Position playerPosition, SpadesBrainImpl myBrainImpl = null)
        {
            Card[] cardsToCheck = new Card[spadesBrain.CardsOnTable.Count + 1];
            cardsToCheck[0] = card;
            spadesBrain.CardsOnTable.Values.CopyTo(cardsToCheck, 1);

            // Check the cards currently on the table (we might already know this card will lose)
            // This is including the cases in which the card is a non spade card, not matching the leading suit.
            if (card != SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck))
            {
                return 0;
            }

            // Determine the current leading suit. If the user is first, the current card will be the leading suit
            Card.SuitType? curLeadingSuit = spadesBrain.LeadingSuit ?? card.Suit;

            // Find the playable cards for each remaining opponent.
            List<Position> oppoPositions = SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count);
            Dictionary<Position, CardsList> opponentsPlayableCards = new Dictionary<Position, CardsList>();
            foreach (Position oppoPos in oppoPositions)
            {
                opponentsPlayableCards[oppoPos] = SpadesUtils.GetPlayableCards(curLeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[oppoPos].Hand);
            }

            // Count the cards above and below a specific card
            Dictionary<Position, int> opponentsCardsAbove = new Dictionary<Position, int>();
            Dictionary<Position, int> opponentsCardsBelow = new Dictionary<Position, int>();
            int numberOfOpponentsWithCardsAbove = 0;
            foreach (Position oppoPos in oppoPositions)
            {
                opponentsCardsAbove[oppoPos] = opponentsPlayableCards[oppoPos].CardsBySuits(card.Suit).CountGreaterThan(card.Rank);
                if (opponentsCardsAbove[oppoPos] > 0)
                {
                    numberOfOpponentsWithCardsAbove++;
                }
                opponentsCardsBelow[oppoPos] = opponentsPlayableCards[oppoPos].CardsBySuits(card.Suit).CountLowerThan(card.Rank);
            }

            // Check if the bot is a sure loser.
            // If an opponent hand which contains only cards above and no cards below exists, then the bot card is a 100% loser.
            // There is one exception in which the bot is not a sure loser even though the above rule is satisfied. It is checked below.
            foreach (Position oppoPos in oppoPositions)
            {
                if (opponentsCardsAbove[oppoPos] > 0 && opponentsCardsBelow[oppoPos] == 0)
                {
                    // An opponent has cards with the same suit as the bot card.
                    // All the cards of the opponent are above.

                    // If:
                    // 1. The bot card is a spade card.
                    // 2. The leading suit is a non spade card.
                    // Then:
                    // Make sure that the opponent has spades only.
                    //
                    // If the opponent has a card which is a non spade card, the opponent can use it to become a 100% sure loser,
                    // and therefore the bot will not be able to become a sure loser himself.
                    if (card.Suit == Card.SuitType.Spades
                        && curLeadingSuit != Card.SuitType.Spades
                        && !opponentsPlayableCards[oppoPos].HasSingleSuit(Card.SuitType.Spades))
                    {
                        continue;
                    }

                    return 0;
                }
            }

            // When the player is the last to act, check if the card will make him a winner.
            // This last player acts with 100% certainty.
            if (spadesBrain.CardsOnTable.Count == 3 && card == SpadesUtils.FindWinningCard(curLeadingSuit, cardsToCheck))
            {
                // The last player is a sure winner.
                return 1;
            }


            // Check if the card is a spade card.
            if (card.Suit == Card.SuitType.Spades)
            {

                // Check if the card is the highest remaining spade.
                // (This is including the case when the player holds all the remaining spades)
                if (numberOfOpponentsWithCardsAbove == 0)
                {
                    return 1;
                }


                if (curLeadingSuit == Card.SuitType.Spades)
                {
                    // The player is first or the leading suit is spades, other suits are not relevant.
                    float numeratorSpades = MAX_RANK - numberOfOpponentsWithCardsAbove;
                    float denominatorSpades = MAX_RANK;
                    float strengthSpades = numeratorSpades / denominatorSpades;
                    return strengthSpades;
                }
                else
                {
                    // The leading suit is not spades.
                    // Estimate the ability of the opponents to re-cut with spades.

                    // Consider every remaining opponent position.
                    // Find out how many opponents are void on the leading suit, as well as holding a higher spade card.
                    int leadingSuitVoidOpponentsWithSpadesAboveNum = 0;
                    foreach (Position oppoPos in oppoPositions)
                    {
                        if (opponentsPlayableCards[oppoPos].CountSuit(curLeadingSuit) == 0)
                        {
                            Card weakestAboveRank = opponentsPlayableCards[oppoPos].CardsBySuits(Card.SuitType.Spades).GetWeakestAboveRank(card.Rank);
                            if (weakestAboveRank != null)
                            {
                                leadingSuitVoidOpponentsWithSpadesAboveNum += 1;
                            }
                        }
                    }

                    float numeratorSpades = MAX_RANK - leadingSuitVoidOpponentsWithSpadesAboveNum;
                    float denominatorSpades = MAX_RANK;
                    float strengthSpades = numeratorSpades / denominatorSpades;
                    return strengthSpades;
                }
            }



            // The card is not a spade card:


            // Check if the bot is a sure loser.
            // If an opponent hand which is void on the leading suit and contains only spade cards exists, then the bot card is a 100% loser.
            foreach (Position oppoPos in oppoPositions)
            {
                if (opponentsPlayableCards[oppoPos].HasSingleSuit(Card.SuitType.Spades))
                {
                    // The bot card is not spades and an opponent has spades only.
                    return 0;
                }
            }

            // Find the number of opponents who hold playable spade cards.
            int numberOfOpponentsWithPlayableSpades = 0;
            foreach (Position position in oppoPositions)
            {
                if (opponentsPlayableCards[position].CountSuit(Card.SuitType.Spades) > 0)
                {
                    numberOfOpponentsWithPlayableSpades += 1;
                }
            }

            // Treat this as a sure take.
            if (numberOfOpponentsWithPlayableSpades == 0 && numberOfOpponentsWithCardsAbove == 0)
            {
                return 1;
            }


            // The non spade card is considered stronger when:
            // 1. There are less players to act who hold cards above it.
            // 2. There are less players to act who are void on the suit.
            float numerator = MAX_RANK - numberOfOpponentsWithCardsAbove;
            float denominator = MAX_RANK + 4 * numberOfOpponentsWithPlayableSpades;
            float strength = numerator / denominator;
            return strength;
        }
    }
}
