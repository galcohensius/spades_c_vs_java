﻿using System;
using System.Collections.Generic;
using cardGames.models;
using SpadesAI.model;
using CommonAI;
using spades.models;
using spades.utils;
using SpadesAI.Internal;

namespace SpadesAI.V4.Chain
{
    public class BreakOpponentNilHandlerV4_Peeking : AbstractHandler
    {
        private const string StrategyFieldName = "BON";

        public BreakOpponentNilHandlerV4_Peeking(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override void Handle(AITurnData turnData)
        {
            if (turnData.OpponentNilPositions.Count == 0)
            {
                nextHandler.Handle(turnData);
                return;
            }
            int strategyRule = 0;
            string deck = "regular";
            Position pos = turnData.PlayerPos;
            Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);
            Position rhoPos = SpadesUtils.RhoPosition(turnData.PlayerPos);
            Position lhoPos = SpadesUtils.LhoPosition(turnData.PlayerPos);

            // Partners:
            // Do not break nil when:
            // *. you are in position {1,2,3} and Contract isn't Fulfilled yet and:
            // 1. Partner signaled to stop BOP by playing sidesuit boss
            // 2. If we win this round we win the game & opponents will have less points than us
            // 3. If the total bid is >=11 & opponents will earn less points than us
            // 4. last 5 tricks
            if (ShouldNotBreak(turnData, pos, partnerPos, rhoPos, lhoPos))
            {
                nextHandler.Handle(turnData);
                return;
            }

            // Iterate over every opponent who bid nil and look for one who can be broken.
            foreach (Position opponentNilPosition in turnData.OpponentNilPositions)
            {
                // Check if nil bidder already threw a card	
                if (spadesBrain.CardsOnTable.ContainsKey(opponentNilPosition))
                {
                    Card nilBidderCard = spadesBrain.CardsOnTable[opponentNilPosition];

                    // if Niler's card is highest on the table
                    if (SpadesUtils.FindWinningPositionOnTable(spadesBrain) == opponentNilPosition)
                    {

                        if (turnData.CardsToUse.CountSuit(spadesBrain.LeadingSuit) > 0)  // not Void in leading suit
                        {
                            Card lowest3Card = CardsUtils.GetXLowestCardInSuit(2, (Card.SuitType)spadesBrain.LeadingSuit, deck);
                            if (nilBidderCard.Rank <= lowest3Card.Rank && spadesBrain.CardsOnTable.Count < 3)
                            {
                                // Do not try to break, since most chances Niler will be covered by later Players
                                if (Logger.Enabled) Logger.DebugFormat("(BON), Do not try to break, since most chances Niler will be covered by later Players");
                            }
                            else
                            {
                                Card strongestCardBelowRank = turnData.CardsToUse.CardsBySuits(nilBidderCard.Suit).GetStrongestBelowRank(nilBidderCard.Rank);
                                if (strongestCardBelowRank != null) // Try to break Niler
                                {
                                    // Use the strongest losing card, since weaker cards might be required to break later.
                                    turnData.CardToPlay = strongestCardBelowRank;
                                    if (turnData.CardToPlay != null)
                                    {
                                        strategyRule = 1;
                                        if (Logger.Enabled) Logger.DebugFormat("(BON) {0} Break {1} Nil: strongest losing card", turnData.PlayerPos, opponentNilPosition);
                                    }
                                }
                                else // Cant break Niler - don't have lower card than Niler
                                {
                                    if (Logger.Enabled) Logger.DebugFormat("(BON), {0} Cant break Niler - don't have lower card than Niler", turnData.PlayerPos);
                                }
                            }
                        }
                        else // Void in leading suit
                        {
                            if (spadesBrain.LeadingSuit != null)
                            {
                                List<Card.SuitType> nonSpadeSuitsList = SuitUtils.GetOtherSuits(new List<Card.SuitType> { Card.SuitType.Spades, spadesBrain.LeadingSuit.Value });
                                CardsList nonSpadeCardsList = turnData.CardsToUse.CardsBySuits(nonSpadeSuitsList);
                                if (nonSpadeCardsList.Count > 0)
                                {
                                    // Try to break.
                                    // Use a medium non spade card, since weaker cards might be required to break later.
                                    turnData.CardToPlay = nonSpadeCardsList.GetMediumCard();
                                    strategyRule = 2;
                                    if (Logger.Enabled) Logger.DebugFormat("(BON) {0}, break nil, {1} nil: cannot follow suit, medium losing side suit", turnData.PlayerPos, opponentNilPosition);
                                }
                                else // Niler's card is highest on the table, hero is Void in leading suit but have only spades
                                {
                                    if (Logger.Enabled) Logger.DebugFormat("(BON), Niler's card is highest on the table, Void in leading suit but have only spades - go to next handler");
                                }
                            }
                        }

                    }
                    else  // Niler is already covered.
                    {
                        if (Logger.Enabled) Logger.DebugFormat("{0} (BON), Niler {1} already covered", turnData.PlayerPos, opponentNilPosition);
                    }
                }
                else // Niler hasn't played yet
                {
                    // Check if the nil bidder's cards can protect him versus the cards currently on the table, if so, there is no reason to try to break.
                    if (!IsSureLossCardsOnTable(opponentNilPosition))
                        continue;

                    // Try to find a bot card which will lose to every niler card.
                    CardsList sureLoseCards = GetSureLoseCards(turnData, opponentNilPosition);

                    if (sureLoseCards.Count > 0)
                    {
                        turnData.CardToPlay = sureLoseCards[sureLoseCards.Count - 1];
                        Logger.DebugFormat("{0} trying to break {1} nil with weakest sure loser card", turnData.PlayerPos, opponentNilPosition);
                    }
                    else
                    {
                        // Change the strategy to under to affect the next handlers.
                        // This will cause high cards to be used as strongest losers, and low cards to be kept for later use.
                        turnData.Strategy = AIStrategy.Under;
                    }
                }
            }

            if (turnData.CardToPlay == null)
            {
                nextHandler.Handle(turnData);
            }
            else
            {
                turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }


        private int GetOpponentsPointsIfNilerWinCoverSet(Position rhoPos, Position lhoPos)
        {
            int opponentsPointsIfNilerWinCoverSet;
            if (spadesBrain.Players[rhoPos].Bid == 0) // rho is niler
            {
                opponentsPointsIfNilerWinCoverSet =
                    spadesBrain.MatchData.MatchPoints[rhoPos] + spadesBrain.MatchData.MatchPoints[lhoPos] +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[rhoPos].Bid, spadesBrain.Players[rhoPos].BlindNil) -
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[lhoPos].Bid, spadesBrain.Players[lhoPos].BlindNil);
            }
            else // lho is niler
            {
                opponentsPointsIfNilerWinCoverSet =
                    spadesBrain.MatchData.MatchPoints[rhoPos] + spadesBrain.MatchData.MatchPoints[lhoPos] +
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[lhoPos].Bid, spadesBrain.Players[lhoPos].BlindNil) -
                    SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[rhoPos].Bid, spadesBrain.Players[rhoPos].BlindNil);
            }

            return opponentsPointsIfNilerWinCoverSet;
        }


        private CardsList GetSureLoseCards(AITurnData turnData, Position opponentNilPosition)
        {
            CardsList sureLoseCards = new CardsList();
            foreach (Card card in turnData.CardsToUse)
            {
                // Determine the current leading suit. If the user is first, the current card will be the leading suit.
                Card.SuitType? LeadingSuit = spadesBrain.LeadingSuit ?? card.Suit;

                // Peek at the nil bidder's playable cards.
                CardsList nilerPlayableCards = SpadesUtils.GetPlayableCards(LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[opponentNilPosition].Hand);

                // Look at the bot card and the niler's hand. Look for cards which lose to the nil bidder for 100%.
                // This action does not consider the future action by max additional 2 Players.
                if (SpadesUtils.FindWinStatus(card, LeadingSuit, nilerPlayableCards) == SpadesUtils.WinStatus.SureLose)
                {
                    sureLoseCards.Add(card);
                }
            }

            return sureLoseCards;
        }

        private bool IsSureLossCardsOnTable(Position opponentNilPosition)
        {
            if (spadesBrain.CardsOnTable.Count > 0)
            {
                // Find the winning card on the table.
                Card[] cardsToCheck = new Card[spadesBrain.CardsOnTable.Count];
                spadesBrain.CardsOnTable.Values.CopyTo(cardsToCheck, 0);
                Card winningCardOnTable = SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck);

                // Peek at the nil bidder's playable cards.
                CardsList nilBidderPlayableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[opponentNilPosition].Hand);

                // Compare the winning card to the nil bidder's cards.
                if (SpadesUtils.FindWinStatus(winningCardOnTable, spadesBrain.LeadingSuit, nilBidderPlayableCards) != SpadesUtils.WinStatus.SureLose)
                    return false;
            }
            return true;
        }

        public bool ShouldNotBreak(AITurnData turnData, Position pos, Position partnerPos, Position rhoPos, Position lhoPos)
        {
            switch (spadesBrain.MatchData.Mode)
            {
                case PlayingMode.Partners:
                    {
                        if (spadesBrain.CardsOnTable.Count < 3 && !spadesBrain.Players[pos].PartnersContractFulfilled) // don't check cases if you pos 4 or if our contract is fulfilled
                        {
                            int ourPointsIfWinRound = spadesBrain.MatchData.MatchPointsIfBetMet[pos] + spadesBrain.MatchData.MatchPointsIfBetMet[partnerPos];
                            int opponentsPointsIfWinRound = spadesBrain.MatchData.MatchPointsIfBetMet[rhoPos] + spadesBrain.MatchData.MatchPointsIfBetMet[lhoPos];
                            int opponentsPointsIfNilerWinCoverSet = GetOpponentsPointsIfNilerWinCoverSet(rhoPos, lhoPos);

                            // 1.Partner signaled to stop BOP by playing sidesuit boss
                            if (spadesBrain.Players[pos].StopToBreakOpponentNil)
                            {
                                if (Logger.Enabled) Logger.DebugFormat("(BON) Partner signaled to stop BOP by playing sidesuit boss: not trying to break Nil");
                                nextHandler.Handle(turnData);
                                return true;
                            }

                            // Set nil at all costs if opponents will win the game this round if they make the nil, even if cover is set
                            if (opponentsPointsIfNilerWinCoverSet >= spadesBrain.MatchData.WinConditionPoints &&
                                opponentsPointsIfNilerWinCoverSet > ourPointsIfWinRound &&
                                spadesBrain.MatchData.MatchBags[lhoPos] + spadesBrain.MatchData.MatchBags[rhoPos] <=
                                spadesBrain.MatchData.BagsToIncurPenalty - 2)
                            {
                                if (Logger.Enabled) Logger.DebugFormat("(BON) Set nil at all costs if opponents will win the game this round if they make the nil, even if cover is set");
                            }
                            else
                            {
                                // 2. If we win this round we win the game & opponents will have less points than us
                                if (ourPointsIfWinRound >= spadesBrain.MatchData.WinConditionPoints &&
                                    ourPointsIfWinRound >= opponentsPointsIfWinRound &&
                                    spadesBrain.MatchData.MatchBags[pos] <= spadesBrain.MatchData.BagsToIncurPenalty - 2)
                                {
                                    if (Logger.Enabled) Logger.DebugFormat("(BON) {0} don't break {1} Nil: since we only need to make our contract to win the game", turnData.PlayerPos, turnData.OpponentNilPositions[0]);
                                    nextHandler.Handle(turnData);
                                    return true;
                                }

                                // 3. If the total bid is >=11
                                if (turnData.TotalBids + turnData.TotalOverTakes >= 11)
                                {
                                    if (Logger.Enabled)
                                        Logger.DebugFormat("(BON) {0} don't break {1} Nil: since bids + overTakes >= 11, go for setting the coverer", turnData.PlayerPos, turnData.OpponentNilPositions[0]);
                                    nextHandler.Handle(turnData);
                                    return true;
                                }

                                // 4. Don't execute in the last 5 tricks.
                                if (spadesBrain.Players[turnData.PlayerPos].Hand.Count <= 5)
                                {
                                    if (Logger.Enabled)
                                        Logger.DebugFormat("(BON) {0} don't break {1} Nil: since last 5 tricks", turnData.PlayerPos, turnData.OpponentNilPositions[0]);
                                    nextHandler.Handle(turnData);
                                    return true;
                                }
                            }
                        }

                        break;
                    }

                case PlayingMode.Solo:
                    Random rand = new Random();

                    if ((spadesBrain.MatchData.MatchPointsIfBetMet[pos] >= spadesBrain.MatchData.WinConditionPoints &&
                         spadesBrain.MatchData.MatchPointsIfBetMet[pos] >= getMaxOpponentNilerPoints(turnData)) 
                        || GetHighestOpponentBid(turnData) >= 6
                        || (!turnData.OpponentNilPositions.Contains(Position.South) && rand.NextDouble() > 0.7) ) // Avoid setting nils of players not in south
                    {
                        // niler is not a threat, dont try to set him.
                        return true;
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            return false; // do try to break
        }

        private int GetHighestOpponentBid(AITurnData turnData)
        {
            int highestOpponentBid = -10;
            foreach (Position opponentPosition in turnData.OpponentNilPositions)
            {
                if (spadesBrain.Players[opponentPosition].Bid > highestOpponentBid)
                {
                    highestOpponentBid = spadesBrain.Players[opponentPosition].Bid;
                }
            }

            return highestOpponentBid;
        }


        private int getMaxOpponentNilerPoints(AITurnData turnData)
        {
            int maxOpponentNilerPoints = -1000;
            foreach (Position opponentNilPosition in turnData.OpponentNilPositions)
            {
                if (spadesBrain.MatchData.MatchPointsIfBetMet[opponentNilPosition] > maxOpponentNilerPoints)
                {
                    maxOpponentNilerPoints = spadesBrain.MatchData.MatchPointsIfBetMet[opponentNilPosition];
                }
            }

            return maxOpponentNilerPoints;
        }
    }
}
