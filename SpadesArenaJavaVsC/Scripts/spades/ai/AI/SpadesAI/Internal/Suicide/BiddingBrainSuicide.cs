using System;
using cardGames.models;
using spades.models;
using SpadesAI.model;

namespace SpadesAI.Internal.V4
{
    internal class BiddingBrainSuicide : BiddingBrainV4
    {

        internal BiddingBrainSuicide(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            // Suicide Spades:
            // each partnership must bid one Nil and one bid >= 4
            // if first partner bids Nil, then second partner must bid >= 4
            // if first partner bids >=4, then second partner must bid Nil

            CardsList hand = spadesBrain.Players[pos].Hand;
            Position prt = SpadesUtils.PartnerPosition(pos);
            CardsList partnerHand = spadesBrain.Players[prt].Hand; // peeking at partner's hand \(*)(*)/

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "position {0}, hand: {1} ", pos, hand);

            float initialBid = InitialBid(pos, hand, PlayingMode.Partners) + 1;  // increase bid by 1 for Suicide variant
            int numericalBid = (int)Math.Round(initialBid, MidpointRounding.AwayFromZero);  
            numericalBid = numericalBid > 4 ? numericalBid : 4;

            float nilChances = PartnersNilChances(hand);
            
            int biddingPosition = 1;
            foreach (Position p in Enum.GetValues(typeof(Position)))
            { if (spadesBrain.Players[p].Bid != -1) biddingPosition++; }
            prData.BiddingPosition = biddingPosition;

            if (biddingPosition < 3) // first partner
            {
                if (pos == Position.West || pos == Position.East)  // TODO: check for Human by using brain type == Human instead of Position.South
                {
                    float partnerNilChance = PartnersNilChances(partnerHand);
                    if (nilChances > partnerNilChance && nilChances > 0.05)
                    {
                        return 0;
                    }
                    else
                    {
                        return numericalBid > 4 ? numericalBid : 4;
                    }
                }
                else
                {
                    if (nilChances > 0.14)
                    {
                        return 0;
                    }
                    else
                    {
                        return numericalBid > 4 ? numericalBid : 4;
                    }
                }
            }
            else // (biddingPosition >= 3) // second partner - must bid opposite from partner: 0/4+ 
            {
                if (spadesBrain.Players[prt].Bid == 0)
                {
                    return numericalBid > 4 ? numericalBid : 4;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
