﻿using SpadesAI.model;
//using UnityEditor.VersionControl;

namespace SpadesAI.Internal
{

	public abstract class AbstractHandler
	{
		protected AbstractHandler nextHandler;
		protected SpadesBrainImpl spadesBrain;

		public AbstractHandler (SpadesBrainImpl spadesBrain)
		{
			this.spadesBrain = spadesBrain;
		}

		public abstract void Handle(AITurnData turnData);

		public AbstractHandler SetNextHandler(AbstractHandler handler) {
			this.nextHandler = handler;
			return handler;
		}

	}


}

