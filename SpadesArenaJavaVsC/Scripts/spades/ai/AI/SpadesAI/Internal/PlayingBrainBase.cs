﻿using SpadesAI.model;
using CommonAI;
using System.Linq;
using spades.models;
using cardGames.models;

namespace SpadesAI
{
	public abstract class PlayingBrainBase
	{
		protected SpadesBrainImpl spadesBrain;

		internal PlayingBrainBase (SpadesBrainImpl spadesBrain)
		{
			this.spadesBrain = spadesBrain;
		}


        internal abstract Card PlayMove(Position pos, SpadesPlayerRoundData prData);

		/// <summary>
		/// Creates an AIStrengthCardsList from the playable cards in the bot hand.
		/// </summary>
		internal AIStrengthCardsList CalcPlayableCardsStrength(Position pos) {
			AIStrengthCardsList result = new AIStrengthCardsList ();

			CardsList hand = spadesBrain.Players [pos].Hand;

			CardsList playableCards = SpadesUtils.GetPlayableCards (spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, hand, 
			                            spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));
            foreach (Card card in playableCards) {
				result.AddCardWithStrength (card, CalcCardStrength (card,pos));
			}

			return result;
		}

        /// <summary>
        /// Creates an AIStrengthCardsList from the playable cards in the bot hand.
        /// </summary>
        internal AIStrengthCardsList CalcPlayableCardsStrength(Position pos, SpadesBrainImpl myBrainImpl)
        {
            AIStrengthCardsList result = new AIStrengthCardsList();

            CardsList playableCards = SpadesUtils.GetPlayableCards(myBrainImpl.LeadingSuit, myBrainImpl.SpadesBroken, myBrainImpl.Players[pos].Hand);
            foreach (Card card in playableCards)
            {
                result.AddCardWithStrength(card, CalcCardStrength(card, pos, myBrainImpl));
            }

            return result;
        }

        /// <summary>
		/// Creates an AIStrengthCardsList from the playable cards in the bot hand.
		/// </summary>
		internal AIStrengthCardsList CalcCardsStrengthAsLeaders(Position pos) {
            AIStrengthCardsList result = new AIStrengthCardsList();

            CardsList hand = spadesBrain.Players[pos].Hand;

            CardsList playableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, hand,
                                        spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));

            CardsList unplayableCards = new CardsList(hand);
            unplayableCards = new CardsList(unplayableCards.Except(playableCards));

            foreach (Card card in unplayableCards) {
                result.AddCardWithStrength(card, CalcCardStrengthAsLeader(card, pos));
            }

            return result;
        }

        //internal abstract float CalcCardStrength(Card card, Position playerPosition);
        internal abstract float CalcCardStrength(Card card, Position playerPosition, SpadesBrainImpl myBrainImpl = null);
        internal virtual float CalcCardStrengthAsLeader(Card card, Position playerPosition) {
            return 0;
        }

    }
}

