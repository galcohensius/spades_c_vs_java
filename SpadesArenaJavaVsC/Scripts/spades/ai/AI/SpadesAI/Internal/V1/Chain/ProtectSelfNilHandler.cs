﻿using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V1.Chain {
    public class ProtectSelfNilHandler:AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "PSN";

        public ProtectSelfNilHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {
            if (!turnData.ProtectSelfNil) {
                nextHandler.Handle (turnData);
                return;
            }

			int strategyRule;

            // A greedy approach shall be used here.
            // In each trick, the bot will use the strongest sure loser if any.
            // If there is not any sure loser, it shall use the weakest card. 
            // Never need to go to the next handler.
            
            // Try Dumping the strongest card that can lose.
            turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(0);
            if (turnData.CardToPlay == null) {
                // No sure loser was found.
                // Throw the weakest card and hope for good.
                turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
				strategyRule = 1;
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} protects self nil with weakest card", turnData.PlayerPos);

            } else {
				strategyRule = 2;
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} protects self nil with strongest sure loser", turnData.PlayerPos);
            }

			turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
        }
    }
}