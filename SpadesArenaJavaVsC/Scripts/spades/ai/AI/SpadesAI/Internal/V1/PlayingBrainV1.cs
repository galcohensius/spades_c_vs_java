﻿using System;
using cardGames.models;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;
using SpadesAI.V1.Chain;

namespace SpadesAI.V1 {
    public class PlayingBrainV1:PlayingBrainBase{
	    
	    public const int TOTAL_CARDS_IN_SUIT = 13;
	    public const int MAX_RANK = TOTAL_CARDS_IN_SUIT + 1;
        
        protected AbstractHandler handler;

        internal PlayingBrainV1 (SpadesBrainImpl spadesBrain):base(spadesBrain)
        {
            // Build the chain of responsibilities handlers
            handler = new PrepareAITurnDataHandler (spadesBrain);
            handler
	            .SetNextHandler (new ProtectSelfNilHandler (spadesBrain))
                .SetNextHandler (new ProtectPartnerNilHandler(spadesBrain))
                .SetNextHandler (new BreakOpponentNilHandler(spadesBrain))
                .SetNextHandler (new SpadesCutSoloHandler(spadesBrain))
                .SetNextHandler (new SpadesCutPartnersHandler(spadesBrain))
                .SetNextHandler (new SureWinSoloHandler (spadesBrain))
                .SetNextHandler (new SureWinPartnersHandler (spadesBrain))
	            .SetNextHandler (new CreateVoidHandler(spadesBrain))
                .SetNextHandler (new SureLoseSoloHandler (spadesBrain)) 
            	.SetNextHandler (new UnknownSoloHandler (spadesBrain));
        }

        /// <summary>
        /// Plays the move.
        /// </summary>
        /// <returns>The card to throw</returns>
        internal override Card PlayMove(Position pos, SpadesPlayerRoundData prData) {

            AITurnData turnData = new AITurnData
            {
                PlayerPos = pos,
                PlayerRoundData = prData
            };
            handler.Handle (turnData);

            return turnData.CardToPlay;
        }

		internal override float CalcCardStrength(Card card, Position playerPosition, SpadesBrainImpl myBrainImpl = null)
        {
			Card[] cardsToCheck = new Card[spadesBrain.CardsOnTable.Count + 1];
			cardsToCheck [0] = card;
			spadesBrain.CardsOnTable.Values.CopyTo (cardsToCheck, 1);

            // Create a card list of throw history + hand
            // Count the holes above and below a specific card
            CardsList historyAndHand = spadesBrain.ThrowHistory.CardsBySuits(card.Suit) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(card.Suit);
			int holesAbove = MAX_RANK - (int)card.Rank - historyAndHand.CountGreaterThan (card.Rank);
			int holesBelow = (int)card.Rank - 2 - historyAndHand.CountLowerThan (card.Rank);

            // Check the cards currently on the table (we might already know this card will lose)
            // This is including the cases in which the card is a non spade card, not matching the leading suit.
            if (card != SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck))
            {
                return 0;
            }

            // When the player is the first to act, check if the card is the lowest remaining card on its suit.
            // Also make sure that not all the other players might are void on the suit.
            if (spadesBrain.CardsOnTable.Count == 0 && holesBelow == 0 && historyAndHand.Count != TOTAL_CARDS_IN_SUIT)
            {
                return 0;
            }

            // When the player is the last to act, check if the card will make him a winner.
            // This last player acts with 100% certainty.
            if (spadesBrain.CardsOnTable.Count == 3 && card == SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck))
            {
                // The last player is a sure winner.
                return 1;
            }

            // Check if the card is the highest remaining spade.
            // (This is including the case when the player holds all the remaining spades)
            if (card.Suit == Card.SuitType.Spades && holesAbove == 0) {
				return 1;
			}

            // When there are no spades left for the other players, check if the non spade card is the highest on its suit.
            CardsList historyAndHandSpadesOnly = spadesBrain.ThrowHistory.CardsBySuits(Card.SuitType.Spades) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(Card.SuitType.Spades);
            if (historyAndHandSpadesOnly.Count == TOTAL_CARDS_IN_SUIT && holesAbove == 0)
            {
                return 1;
            }

            // Check if the card is a spade card.
            if(card.Suit == Card.SuitType.Spades) 
            {
                if(spadesBrain.CardsOnTable.Count == 0 || spadesBrain.LeadingSuit == Card.SuitType.Spades) {
                    // The player is first or the leading suit is spades, other suits are not relevant.
                    float numeratorSpades   = MAX_RANK - holesAbove;
                    float denominatorSpades = MAX_RANK;
                    float strengthSpades = numeratorSpades / denominatorSpades;
                    return strengthSpades;
                } else if(spadesBrain.CardsOnTable.Count == 1 || spadesBrain.CardsOnTable.Count == 2) {
                    // The leading suit is not spades.
                    // Estimate the ability of the opponents to re-cut with spades.
                    // weak spade cards go down in value because of the risk of a second spade card above it.

                    // Check how many relevant voids are known
                    int leadingSuitVoidOpponentsWithSpadesNum = 0;
                    foreach (Position position in SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count)) {
                        // Consider every remaining opponent position.
                        // Make sure the opponent is not void on the spades suits as well.
                        bool isOpponentVoidOnCardSuit = spadesBrain.Players[position].VoidSuits.Contains(spadesBrain.LeadingSuit.Value);
                        bool isOpponentVoidOnSpades = spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades);
                        if (isOpponentVoidOnCardSuit && !isOpponentVoidOnSpades) {
                            leadingSuitVoidOpponentsWithSpadesNum += 1;
                        }
                    }

                    float numeratorSpades   = MAX_RANK - holesAbove;
                    float denominatorSpades = MAX_RANK + leadingSuitVoidOpponentsWithSpadesNum * holesAbove;
                    float strengthSpades = numeratorSpades / denominatorSpades;
                    return strengthSpades;
                } else {
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(CalcCardStrengthV1) - card = {0}, cardsToCheck = {1} ,playerPosition = {2}", card, cardsToCheck, playerPosition);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(CalcCardStrengthV1) - the winner card is = {0}", SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck));
                    throw new Exception("Last position should have been handled before...");
                }
            } // The card is not a spade card:

            // Check how many relevant voids are known
            int voidOpponentsWithSpadesNum = 0;
            foreach (Position position in SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count)) {
                // Consider every remaining opponent position.
                // Make sure the opponent is not void on the spades suits as well.
                bool isOpponentVoidOnCardSuit = spadesBrain.Players[position].VoidSuits.Contains(card.Suit);
                bool isOpponentVoidOnSpades = spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades);
                if (isOpponentVoidOnCardSuit && !isOpponentVoidOnSpades) {
                    voidOpponentsWithSpadesNum += 1;
                }
            }

            // Treat this as a sure take.
            if (voidOpponentsWithSpadesNum == 0) {
                switch (card.Rank) {
                    case Card.RankType.Ace:
                        if (historyAndHand.Count < 9) {
                            return 1;
                        }
                        break;
                    case Card.RankType.King:
                        if (historyAndHand.Count < 9 && holesAbove == 0) {
                            return 1;
                        }
                        break;
                    default:
                        if (historyAndHand.Count < 7 && holesAbove == 0) {
                            return 1;
                        }
                        break;
                }
            }


            // The non spade card is considered stronger when:
            // 1. There are less unused cards above it (held by the opponents).
            // 2. There are less cards already seen from its suit. seen means it it was either thrown before or is being held by the player.
            // 3. There are less players to act who are void on the suit.
            float numerator   = MAX_RANK - holesAbove;
            float denominator = MAX_RANK + historyAndHand.Count + 4 * voidOpponentsWithSpadesNum;
            float strength = numerator / denominator;
            return strength;
		}

        internal override float CalcCardStrengthAsLeader(Card card, Position playerPosition) {
            // Create a card list of throw history + hand
            // Count the holes above and below a specific card
            CardsList historyAndHand = spadesBrain.ThrowHistory.CardsBySuits(card.Suit) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(card.Suit);
            int holesAbove = MAX_RANK - (int)card.Rank - historyAndHand.CountGreaterThan(card.Rank);
            int holesBelow = (int)card.Rank - 2 - historyAndHand.CountLowerThan(card.Rank);

            // When the player is the first to act, check if the card is the lowest remaining card on its suit.
            // Also make sure that not all the other players might are void on the suit.
            if (holesBelow == 0 && historyAndHand.Count != TOTAL_CARDS_IN_SUIT) {
                return 0;
            }

            // Check if the card is the highest remaining spade.
            // (This is including the case when the player holds all the remaining spades)
            if (card.Suit == Card.SuitType.Spades && holesAbove == 0) {
                return 1;
            }

            // When there are no spades left for the other players, check if the non spade card is the highest on its suit.
            CardsList historyAndHandSpadesOnly = spadesBrain.ThrowHistory.CardsBySuits(Card.SuitType.Spades) + spadesBrain.Players[playerPosition].Hand.CardsBySuits(Card.SuitType.Spades);
            if (historyAndHandSpadesOnly.Count == TOTAL_CARDS_IN_SUIT && holesAbove == 0) {
                return 1;
            }

            // Check if the card is a spade card.
            if (card.Suit == Card.SuitType.Spades) {
                float numeratorSpades   = MAX_RANK - holesAbove;
                float denominatorSpades = MAX_RANK;
                float strengthSpades = numeratorSpades / denominatorSpades;
                return strengthSpades;
            }


            // The card is not a spade card:


            // Check how many relevant voids are known
            int voidOpponentsWithSpadesNum = 0;
            foreach (Position position in SpadesUtils.OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode)) {
                // Consider every remaining opponent position.
                // Make sure the opponent is not void on the spades suits as well.
                bool isOpponentVoidOnCardSuit = spadesBrain.Players[position].VoidSuits.Contains(card.Suit);
                bool isOpponentVoidOnSpades = spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades);
                if (isOpponentVoidOnCardSuit && !isOpponentVoidOnSpades) {
                    voidOpponentsWithSpadesNum += 1;
                }
            }

            // Treat this as a sure take.
            if (voidOpponentsWithSpadesNum == 0) {
                switch (card.Rank) {
                    case Card.RankType.Ace:
                        if (historyAndHand.Count < 9) {
                            return 1;
                        }
                        break;
                    case Card.RankType.King:
                        if (historyAndHand.Count < 9 && holesAbove == 0) {
                            return 1;
                        }
                        break;
                    default:
                        if (historyAndHand.Count < 7 && holesAbove == 0) {
                            return 1;
                        }
                        break;
                }
            }


            // The non spade card is considered stronger when:
            // 1. There are less unused cards above it (held by the opponents).
            // 2. There are less cards already seen from its suit. seen means it it was either thrown before or is being held by the player.
            // 3. There are less players to act who are void on the suit.
            float numerator = MAX_RANK - holesAbove;
            float denominator = MAX_RANK + historyAndHand.Count + 4 * voidOpponentsWithSpadesNum;
            float strength = numerator / denominator;
            return strength;
        }
    }
}