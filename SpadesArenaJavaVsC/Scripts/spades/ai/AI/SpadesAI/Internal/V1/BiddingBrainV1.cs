﻿using System;
using SpadesAI.model;
using spades.models;
using cardGames.models;

namespace SpadesAI.V1
{
    internal class BiddingBrainV1 : IBiddingBrain {
        protected SpadesBrainImpl spadesBrain;

        protected float curBid;

        System.Random rand = new System.Random();

        internal BiddingBrainV1(SpadesBrainImpl spadesBrain) {
            this.spadesBrain = spadesBrain;
        }

        /// <summary>
        /// Makes the bid for the specified position
        /// </summary>
        public virtual int MakeBid(Position pos, SpadesPlayerRoundData prData) {
            prData.BiddingPosition = GetBiddingPosition();
            curBid = 0;
            PointCounting(pos);
            //OtherPlayersFactor(pos, prData.BiddingPosition);
            prData.InitialBid = curBid;
			NilValidation(pos);

            // Round the bid
            curBid = (int)Math.Round(curBid);
			if (curBid > 7)
				curBid = 7;

            // Bots level effects the +1 on the bid
            double bidIncPercent = 0;
            switch(spadesBrain.Players[pos].Level) {
                default:
                case 1:
                    bidIncPercent = 1;
                    break;
                case 2:
                    bidIncPercent = 0.75;
                    break;
                case 3:
                    bidIncPercent = 0.5;
                    break;
                case 4:
                    bidIncPercent = 0.25;
                    break;
            }

            if(rand.NextDouble()<bidIncPercent) {
                curBid++;
            }
            // level 9 never bids nil
            if (spadesBrain.Players[pos].Level == 9 && curBid < 1)
                curBid = 1;

            // Validate that the partners do not have a bid larger than 13
            PartnersBidsValidation(pos, prData.BiddingPosition);

			return (int)curBid;
        }


        internal int GetBiddingPosition()
        {
            int biddingPosition = 1;
            foreach (Position p in Enum.GetValues(typeof(Position)))
            {
                if (spadesBrain.Players[p].Bid != -1)
                {
                    biddingPosition++;
                }
            }
            return biddingPosition;
        }

        protected int CalcTotalBids() {
            int result = 0;

            foreach (Position pos in Enum.GetValues(typeof(Position))) {
                result += Math.Max(spadesBrain.Players[pos].Bid, 0);
            }

            return result;
        }


        /// <summary>
        /// Count honor cards and calc the bidding
        /// </summary>
        /// <see cref="http://www.sharkfeeder.com/spadsbook/basicbidding.html"/>
        private void PointCounting(Position pos) {
            CardsList hand = spadesBrain.Players[pos].Hand;

            float result = PotentialBid(hand,0);
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "{0} points Counting: {1}", pos, result);

            curBid += result;
        }

        public virtual float PotentialBid(CardsList cardsList,int param)
        {
            float result = 0;

            CardsList cards = null;

            int voids = 0, singletons = 0, doubletons = 0;

            // Non trump counting
            Card.SuitType[] nonTrump = { Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts };
            foreach (Card.SuitType suit in nonTrump)
            {
                cards = cardsList.CardsBySuits(suit);

                // Ace
                if (cards.HasRank(Card.RankType.Ace))
                {
                    if (cards.Count < 7)
                        result += 1;
                    else
                        result += 0.5F;
                }

                // King
                if (cards.HasRank(Card.RankType.King))
                {
                    if (cards.Count == 1 || cards.Count == 5)
                        result += 0.25F;
                    else if (cards.Count < 5)
                        result += 0.75F;

                    if (cards.Count < 6 &&
                        cards.HasRank(Card.RankType.Ace) ||
                        cards.HasRank(Card.RankType.Queen) ||
                        cards.HasRank(Card.RankType.Jack))
                        result += 0.25F;
                }

                // Queen
                if (cards.HasRank(Card.RankType.Queen))
                {
                    if (cards.HasRank(Card.RankType.Ace) && cards.HasRank(Card.RankType.King) && cards.Count <= 4)
                        result += 0.5F; 
                    else if (cards.Count < 6)
                        result += 0.25F;
                }

                // Count short non-trump suits
                switch (cards.Count)
                {
                    case 0:
                        voids++;
                        break;
                    case 1:
                        singletons++;
                        break;
                    case 2:
                        doubletons++;
                        break;
                }
            }

            // Trump counting
            cards = cardsList.CardsBySuits(Card.SuitType.Spades);
            cards.SortHighToLowSuitFirst();


            // Handle voids and singletons
            if (voids > 0)
            {
                if (cards.Count >= 1 && cards.Count <= 3)
                {
                    result += 1;
                    cards.RemoveAt(cards.Count - 1);
                }
                else if (cards.Count > 3)
                {
                    result += 2;
                    cards.RemoveAt(cards.Count - 1);
                    cards.RemoveAt(cards.Count - 1);
                }
            }
            if (singletons > 0)
            {
                if (cards.Count >= 2 && cards.Count <= 4)
                {
                    result += 1;
                    cards.RemoveAt(cards.Count - 1);
                }
                else if (cards.Count > 4)
                {
                    result += 2;
                    cards.RemoveAt(cards.Count - 1);
                    cards.RemoveAt(cards.Count - 1);
                }
            }

            // Num of trumps factor
            if (cards.Count == 6)
                result += 0.5F;

            else if (cards.Count > 6)
                result += (cards.Count - 6); //  7=>1, 8=>2, ...

            // Count holes and protection
            // Iterate the trump cards from high to low.
            // For each hole, remove a low card, for each non-hole, count as 1 take
            for (int i = 14; i >= 2; i--)
            {
                Card.RankType rank = (Card.RankType)i;
                //Logger.Debug("rank==" + rank);
                if (cards.HasRank(rank))
                {
                    result += 1;
                }
                else
                {
                    //Logger.Debug("count lower ==" + cards.CountLowerThan(rank));
                    // Remove the last card if exists
                    if (cards.CountLowerThan(rank) > 0)
                    {
                        cards.RemoveAt(cards.Count - 1);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// Check what other players have bid and adjust accordingly.
        /// In Partners, if partner bid nil, try to take more cards.
        /// </summary>
        private void OtherPlayersFactor(Position pos, int biddingPosition) {
            float result = 0;

            int totalBids = CalcTotalBids();

            // FIX BOTS 27-3:
            // Removed factor for second bidder				
            // Change factors: 0.75=>0.25, 1=>0.5

            // Adjust according to prev bidders and bidding position
            if (biddingPosition == 2) {
                //				if (totalBids >= 4)
                //					result += -0.5F;
                //				else if (totalBids<=2) 
                //					result += 0.5F;
            } else if (biddingPosition == 3) {
                //if (totalBids >= 8)
                  //  result += -0.25F;
                //else if (totalBids <= 4)
                  //  result += 0.25F;
            //} else if (biddingPosition == 4) {
              //  if (totalBids >= 12)
                //    result += -0.5F;
                //else if (totalBids <= 6)
                 //   result += 0.5F;
            }

            // Adjust according to partner
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners && biddingPosition >= 3) {
                int partnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;

                // Nil - Sometimes the player have to take more to protect partner's nil
                //if (partnerBid == 0 && totalBids < 10) {
                //    result += 1;
                //    Logger.Debug("Partner bids nil, add 1");
                //}

            }

            // If someone have already bid nil, and the bot wanted to bid nil, add 1 - TBD
            if (curBid == 0 && result == 0) {

            }

            // Make sure we are not going negative
            result = Math.Max(result, 0);

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "Other players factor is: " + result + ". Bidding pos: " + biddingPosition + ", Total bids: " + totalBids);

            curBid += result;

        }

        /// <summary>
        /// Adjust the bags according to points and bags. 
        /// If the bot (or team) are close to the bags penalty, increase the bid and try to avoid bags.
        /// If the bot (or team) are close to winning, increase the bid and try to win in this round.
        /// </summary>
        private void PointsBagsFactor(Position pos, int biddingPosition) {
            float result = 0;

            int points = 0;
            int bags = 0;
            int pointsIfBidMet = 0;

            if (spadesBrain.MatchData.Mode == PlayingMode.Partners) {
                Position partnerPos = SpadesUtils.PartnerPosition(pos);
                points = spadesBrain.MatchData.MatchPoints[pos] + spadesBrain.MatchData.MatchPoints[partnerPos];
                bags = spadesBrain.MatchData.MatchBags[pos] + spadesBrain.MatchData.MatchBags[partnerPos];
                pointsIfBidMet = points + (int)Math.Round(curBid) * 10;  // BUG: Nil isn't treated

                if (biddingPosition >= 3) {
                    // Partner has already bid
                    int partnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;
                    if (partnerBid > 0) {
                        // Use factor only if partner did not bid nil
                        pointsIfBidMet += partnerBid * 10;
                        if (spadesBrain.MatchData.WinConditionPoints - pointsIfBidMet > 0 && spadesBrain.MatchData.WinConditionPoints - pointsIfBidMet <= 10) {
                            // Add 1 to the bid since the team can try and win the game
                            result = 1;

                        } else if (bags == spadesBrain.MatchData.BagsToIncurPenalty - 1) {
                            result = 1;
                        }
                    }

                }


            } else {
                points = spadesBrain.MatchData.MatchPoints[pos];
                bags = spadesBrain.MatchData.MatchBags[pos];

                pointsIfBidMet = points + (int)Math.Round(curBid) * 10;
                if (spadesBrain.MatchData.WinConditionPoints - pointsIfBidMet > 0 && spadesBrain.MatchData.WinConditionPoints - pointsIfBidMet <= 10) {
                    // Add 1 to the bid since the bot can try and win the game
                    result = 1;

                } else if (bags == spadesBrain.MatchData.BagsToIncurPenalty - 1) {
                    result = 1;
                }
            }
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "Points and bags factor is: {0}. Points: {1}, Bags: {2}, PointsIfBidMet: {3}", result, points, bags, pointsIfBidMet);

            curBid += result;
        }

        /// <summary>
        /// If the current bid is nil, bid 1 if the following is true:
        /// * The player has J or above trump
        /// * The player has A of any suit
        /// * The player has a lowest card of 9 for any suit 
        /// </summary>
        private void NilValidation(Position pos) {
            if (curBid < 1) {
                CardsList hand = spadesBrain.Players[pos].Hand;

                CardsList cards = hand.CardsBySuits(Card.SuitType.Spades);
                if (cards.CountGreaterThan(Card.RankType.Ten) > 0) {
                    curBid = 1;
                    return;
                }

                Card.SuitType[] nonTrump = { Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts };
                foreach (Card.SuitType suit in nonTrump) {
                    cards = hand.CardsBySuits(suit);
                    if (cards.Count > 0 && (cards.HasRank(Card.RankType.Ace) || cards.CountLowerThan(Card.RankType.Nine) == 0)) {
                        curBid = 1;
                        break;
                    }
                }

                foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType))) {
                    cards = hand.CardsBySuits(suit);
                    if (cards.Count > 0 && cards.CountLowerThan(Card.RankType.Six) == 0) {
                        curBid = 1;
                        break;
                    }
                }

//                if ((int)curBid == 1)
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "Nil validation failed, bidding 1 instead");
            }
        }

        /// <summary>
		/// Checks that both partners do not bid more than 13 together.
		/// </summary>
        protected void PartnersBidsValidation(Position pos, int biddingPosition) {
            if(spadesBrain.MatchData.Mode != PlayingMode.Partners) {
                return;
            }

            // If the partner did not bid so far, there is nothing to validate.
            if(biddingPosition < 3){
                return;
            }

            // Make sure the combined bid of the player and the partner is no more than 13.
            int partnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;
            curBid = Math.Min(curBid, 13 - partnerBid);
        }
    }
}
