﻿using System;
using SpadesAI.model;
using System.Collections.Generic;
using System.Linq;
using spades.models;
using SpadesAI.Internal;

namespace SpadesAI.V1.Chain
{
	public class PrepareAITurnDataHandler:AbstractHandler
	{
		public PrepareAITurnDataHandler (SpadesBrainImpl spadesBrain):base(spadesBrain)
		{
		}
     
        public override void Handle (AITurnData turnData)
		{
			// Calc the bid distance.
			int totalBids = 0, totalOvertakes = 0;
			foreach (Position pos in Enum.GetValues(typeof(Position))) 
            {
				AIPlayer player = spadesBrain.Players [pos];
				totalBids += player.Bid;
				totalOvertakes += Math.Max (player.Takes - player.Bid, 0);
			}

			turnData.BidDistance = totalBids - 13 + totalOvertakes;

            // Determine the AIStrategy type according to the bid distance.
            // For partners, bid distance of -2 and above is over
            // For solo, bid distance of -1 and above is over
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                if (turnData.BidDistance >= -2)
                  turnData.Strategy = AIStrategy.Over;
                else
                  turnData.Strategy = AIStrategy.Under;
            } else {
                if (turnData.BidDistance >= -1)
                    turnData.Strategy = AIStrategy.Over;
                else
                    turnData.Strategy = AIStrategy.Under;
            }
            // LosingBot: level 9 always Under 
            if (spadesBrain.Players[turnData.PlayerPos].Level == 9)
                turnData.Strategy = AIStrategy.Under;
            
            // save strategy to data base
            turnData.PlayerRoundData.Strategies += turnData.Strategy == AIStrategy.Over ? "O0" : "U0";
            
            // Check if the player bid nil.
			if (spadesBrain.Players[turnData.PlayerPos].Bid == 0) {
				turnData.ProtectSelfNil = true;
			}

			// Protect partner nil flag.
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners) {
				Position partnerPos = SpadesUtils.PartnerPosition (turnData.PlayerPos);
				if (spadesBrain.Players [partnerPos].Bid == 0 && spadesBrain.Players [partnerPos].Takes == 0) {
					turnData.ProtectPartnerNil = true;
				}
			}

			// Nil position for opponents.
			List<Position> oppoPositions = SpadesUtils.OpponentsPositions (turnData.PlayerPos, spadesBrain.MatchData.Mode);
			List<Position> opponentNilPositions = new List<Position>();
			foreach (var pos in oppoPositions) {
				if (spadesBrain.Players [pos].Bid == 0 && spadesBrain.Players [pos].Takes == 0) {
					opponentNilPositions.Add (pos);
				}
			}
			turnData.OpponentNilPositions = opponentNilPositions;
			
			// Get current player's or partner's bids and takes
			turnData.Bid = spadesBrain.Players [turnData.PlayerPos].Bid;
			turnData.Takes = spadesBrain.Players [turnData.PlayerPos].Takes;
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners) {
				Position partnerPos = SpadesUtils.PartnerPosition (turnData.PlayerPos);
				turnData.Bid += spadesBrain.Players [partnerPos].Bid;
				turnData.Takes += spadesBrain.Players [partnerPos].Takes;
			}

			// Playable cards.
			turnData.CardsToUse = spadesBrain.PlayingBrain.CalcPlayableCardsStrength (turnData.PlayerPos);

            // Player all cards as leaders.
            // This is including both the playable and unplayable cards, Ignoring the current trick state.
            // Therefore, this might miss on 1 card which will be used on the current trick when the bot
            // is not the leader.
            // turnData.CardsAsLeaders = spadesBrain.PlayingBrain.CalcCardsStrengthAsLeaders(turnData.PlayerPos);


            // For skill level 10, strategy is always Over
            AIPlayer curPlayer = spadesBrain.Players[turnData.PlayerPos];
            if (curPlayer.Level == 10)
                turnData.Strategy = AIStrategy.Over;

            // Always move to next handler
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "round {4}, {5}, Prepare Turn successful, {0}, {1}, {2}, {3}, leading suit: {8}, cards on table: {9}, cards to use: {7}, cards to play: {6}", spadesBrain.PlayingBrain, spadesBrain.MatchData.Mode, spadesBrain.MatchData.Variation, spadesBrain.MatchData.SubVariation, spadesBrain.MatchData.RoundNum, turnData.PlayerPos, turnData.CardToPlay, turnData.CardsToUse.ToStringCardsOnly(), spadesBrain.LeadingSuit, string.Join(";", spadesBrain.CardsOnTable.Select(x => x.Value)));
            nextHandler.Handle(turnData);
		}
		
	}
}

