﻿using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V1.Chain {
	
    public class SureWinSoloHandler:AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "SWS";

        public SureWinSoloHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {

            if (spadesBrain.MatchData.Mode != PlayingMode.Solo) {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            if (turnData.Takes < turnData.Bid) { 
		        // Needs more takes
		        if (turnData.Strategy == AIStrategy.Over) {
			        // Try to take with the weakest card, as stronger cards might be used later.
			        turnData.CardToPlay = turnData.CardsToUse.GetWeakestCardByStrength(1);

					if (turnData.CardToPlay != null) {
						strategyRule = 1;
						// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} tries to take with the weakest winning card", turnData.PlayerPos);
                    }

		        } else {
			        // Try to take with the strongest card, as weaker cards might be dumped later.
			        turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(1);

					if (turnData.CardToPlay != null) {
						strategyRule = 2;
						// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} tries to take with the strongest winning card", turnData.PlayerPos);
                    }
		        }
	        }

	        if (turnData.CardToPlay == null) {
		        nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}