﻿using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V1.Chain {
    public class CreateVoidHandler:AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "CVH";

        public CreateVoidHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {

            // No need to execute this in the last 5 tricks.
            if(spadesBrain.Players[turnData.PlayerPos].Hand.Count <= 5) {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            Dictionary<Card.SuitType, CardsList> suitCountDictionary =
                new Dictionary<Card.SuitType, CardsList>();
            
            // Group the cards by suit.
            foreach (Card Card in turnData.CardsToUse) {
                
                // Skip any spade card.
                if (Card.Suit == Card.SuitType.Spades) {
                    continue;
                }
                
                CardsList CardsList;
                suitCountDictionary.TryGetValue(Card.Suit, out CardsList);
                if (CardsList == null) {
                    suitCountDictionary[Card.Suit] = new CardsList();
                }
                suitCountDictionary[Card.Suit].Add(Card);
            }

            // Make sure there is more than 1 suit.
            if (suitCountDictionary.Count > 1) {
                // Find the suit with the minimal number of cards.
                int minSuitCount = 14;
                CardsList minSuitCountCardsList = null;
                foreach (KeyValuePair<Card.SuitType,CardsList> keyValuePair in suitCountDictionary) {
                    if (keyValuePair.Value.Count < minSuitCount) {
                        minSuitCountCardsList = keyValuePair.Value;
                    }
                }
            
                // Look for something small enough.
                if (minSuitCountCardsList.Count <= 2) {
                    // Find the smallest ranked card.
                    minSuitCountCardsList.SortHighToLowSuitFirst();
                    turnData.CardToPlay = minSuitCountCardsList.Last();

					if (turnData.CardToPlay != null) {
						strategyRule = 1;
						// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} trying to create void suit {1}", turnData.PlayerPos, turnData.CardToPlay.Suit);
                    }
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle (turnData);
            } else {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}

