﻿using SpadesAI.model;
using spades.models;
using SpadesAI.Internal;

namespace SpadesAI.V1.Chain {

    public class SureWinPartnersHandler : AbstractHandler {

        private const string StrategyFieldName = "SWP";


        public SureWinPartnersHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {

            if(spadesBrain.MatchData.Mode != PlayingMode.Partners) {
                nextHandler.Handle(turnData);
                return;
            }

            // If partner already played, and his card is going to win, do not cut partner // RAN CHANGE 1.3
            bool partnerAboutToWin = SpadesUtils.IsPartnerAboutToWin(turnData.PlayerPos, spadesBrain);
            if (partnerAboutToWin)
            {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;

            if (turnData.Strategy == AIStrategy.Under) {
                // Try to take with the strongest card, as weaker cards might be dumped later.
                turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(1);

				if (turnData.CardToPlay!=null) {
					strategyRule = 1;
					// LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(SWP) {0} tries to take with the strongest winning card", turnData.PlayerPos);
                }
            } else {
                // Try to take with the weakest card, as stronger cards might be used later.
                turnData.CardToPlay = turnData.CardsToUse.GetWeakestCardByStrength(1);

				if (turnData.CardToPlay != null) {
					strategyRule = 2;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(SWP) {0} tries to take with the weakest winning card", turnData.PlayerPos);
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}
