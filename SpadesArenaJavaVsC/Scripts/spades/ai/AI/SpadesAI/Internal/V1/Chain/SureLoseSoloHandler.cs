﻿using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V1.Chain {
    public class SureLoseSoloHandler:AbstractHandler {


        private const string STRATEGY_FIELD_NAME = "SLS";


        public SureLoseSoloHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {
            if (spadesBrain.MatchData.Mode != PlayingMode.Solo)
            {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            if (turnData.Takes >= turnData.Bid) {
                // Already have all takes, dump the strongest card that can lose.
                turnData.CardToPlay = turnData.CardsToUse.GetStrongestCardByStrength(0);

				if (turnData.CardToPlay != null) {
					strategyRule = 1;
					// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} throwing strongest card since already have all takes", turnData.PlayerPos);
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}