﻿using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V1.Chain {
    public class UnknownSoloHandler:AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "UHH";

        public UnknownSoloHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {

			int strategyRule = 0;


            if (turnData.Takes >= turnData.Bid && spadesBrain.MatchData.Mode == PlayingMode.Solo) {   // RAN CHANGE 28.2
                // Try not to take.
                turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
				strategyRule = 1;    
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";

                return;
            }


            // Needs more takes
            switch (turnData.Strategy) {
                case AIStrategy.Over:
                    // Dump a weak card, as stronger cards might be used later.
                    turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 2;
						// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} dumps a weak card", turnData.PlayerPos);
                    }
                    break;
                case AIStrategy.Under:
                    // Dump a medium card, as weaker cards might be used later.
                    // A stronger card might be required later, since the bot probably relied on it on the bidding stage.
                    turnData.CardToPlay = turnData.CardsToUse[turnData.CardsToUse.Count / 2];

					if (turnData.CardToPlay != null) {
						strategyRule = 3;
						// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} dumps a medium card", turnData.PlayerPos);
                    }
                    break;
            }

            // Final test to make sure a card is selected
			if (turnData.CardToPlay==null) {
				strategyRule = 4;
				turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
            }

			turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";

        }
    }
}