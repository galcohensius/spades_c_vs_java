﻿using SpadesAI.model;
using System.Collections.Generic;
using SpadesAI.Internal;
using cardGames.models;

namespace SpadesAI.V1.Chain
{
	public class ProtectPartnerNilHandler:AbstractHandler
	{
        private const string STRATEGY_FIELD_NAME = "PPN";

		public ProtectPartnerNilHandler (SpadesBrainImpl spadesBrain):base(spadesBrain)
		{
		}


		public override void Handle (AITurnData turnData)
		{
			if (!turnData.ProtectPartnerNil) {
				nextHandler.Handle (turnData);
				return;
			}

			int strategyRule = 0;

            Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);

            // Check if the partner has already threw a card
            if (spadesBrain.CardsOnTable.ContainsKey(partnerPos)) {
                Card partnerCard = spadesBrain.CardsOnTable[partnerPos];

                // Check if the partner's card is the highest on the table
                if (SpadesUtils.FindWinningPositionOnTable(spadesBrain) == partnerPos) {
                    if (turnData.CardsToUse.CountSuit(partnerCard.Suit) > 0) {
                        Card weakestCardAboveRank = turnData.CardsToUse.CardsBySuits(partnerCard.Suit).GetWeakestAboveRank(partnerCard.Rank);
                        if (weakestCardAboveRank != null) {
                            // Use a weak card, since a strong card might be required to protect later.
                            turnData.CardToPlay = weakestCardAboveRank;
							strategyRule = 1;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} protects partner's nil with weakest card", turnData.PlayerPos);
                        }
                    } else {
                        CardsList spadeCardsList = turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades);
                        if (spadeCardsList.Count > 0) {
                            // Use a weak spade card, since higher spade cards should be used to protect later.
                            turnData.CardToPlay = spadeCardsList[spadeCardsList.Count - 1];

							if (turnData.CardToPlay != null) {
								strategyRule = 2;
								// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} uses weakest spade", turnData.PlayerPos);
                            }
                        }
                    }
                } else {
                    // Already covered
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} don't need to protect partner's nil", turnData.PlayerPos);
                }
            } else {
                // partner has not played yet

                HashSet<Card.SuitType> partnerVoids = spadesBrain.Players[partnerPos].VoidSuits;

                if (spadesBrain.CardsOnTable.Count == 0) {
                    // Bot is the leader:
                    // Try to find first a card which the partner is void of.
                    // If none is found then use a high card.
                    if (partnerVoids.Count > 0) {
                        // Use a weak card that the partner is void off.
                        CardsList voidCardsList = turnData.CardsToUse.CardsBySuits(partnerVoids);
                        if(voidCardsList.Count > 0) {
                            turnData.CardToPlay = voidCardsList[voidCardsList.Count - 1];
							strategyRule = 3;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} uses weakest card of partner's void suit", turnData.PlayerPos);
                        }
                    }
                    if(turnData.CardToPlay == null) {
                        // Use a strong card that the partner is not void off.
                        turnData.CardToPlay = turnData.CardsToUse.GetStrongCard();

						if (turnData.CardToPlay != null) {
							strategyRule = 4;
							// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} uses strongest card", turnData.PlayerPos);
                        }
                    }
                } else {
                    // Bot is second
                    // If the RHO threw a card weaker than 10, which the partner is not void in.
                    Card RHOCard = spadesBrain.CardsOnTable[SpadesUtils.RhoPosition(turnData.PlayerPos)];
                    if (RHOCard.Rank < Card.RankType.Jack && !partnerVoids.Contains(RHOCard.Suit)) {

                        // DANGER DANGER DANGER!!!

                        if (turnData.CardsToUse.CountSuit(spadesBrain.LeadingSuit) > 0) {
                            // Use a strong card, since the RHO presented a weak card.
                            turnData.CardToPlay = turnData.CardsToUse.GetStrongCard();

							if (turnData.CardToPlay != null) {
								strategyRule = 5;
								// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} uses strongest card", turnData.PlayerPos);
                            }
                        } else {
                            CardsList spadeCardsList = turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades);
                            if (spadeCardsList.Count > 0) {
                                // Use a weak spade card, since higher spade cards should be used to protect later.
                                turnData.CardToPlay = spadeCardsList[spadeCardsList.Count - 1];

								if (turnData.CardToPlay != null) {
									strategyRule = 6;
									// LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} uses weakest spade", turnData.PlayerPos);
                                }
                            }
                        }
                    }
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
	}
}

