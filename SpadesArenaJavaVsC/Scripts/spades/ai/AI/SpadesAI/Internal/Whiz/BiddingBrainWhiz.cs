using System;
using cardGames.models;
using spades.models;
using SpadesAI.model;

namespace SpadesAI.Internal.V4
{
    internal class BiddingBrainWhiz : BiddingBrainV4
    {
        internal BiddingBrainWhiz(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            // Whiz: bid either spades# or Nil          
            CardsList hand = spadesBrain.Players[pos].Hand;
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "position {0}, hand: {1} ", pos, hand);

            int spadesCount = hand.CardsBySuits(Card.SuitType.Spades).Count;
            float initialBid = InitialBid(pos, hand, PlayingMode.Partners);

            double nilChances = Math.Round(PartnersNilChances(hand),4);
            double nilChanceThreshold = getNilChanceThreshold(spadesCount, initialBid, spadesBrain.Players[pos].Level);

            int biddingPosition = 1;
            foreach (Position p in Enum.GetValues(typeof(Position)))
                {if (spadesBrain.Players[p].Bid != -1) biddingPosition++;}
            prData.BiddingPosition = biddingPosition;

            if (biddingPosition < 3) // First partner
            {
            }
            else // biddingPosition >= 3 // Second partner
            {
                Position prt = SpadesUtils.PartnerPosition(pos);
                int prtBid = spadesBrain.Players[prt].Bid;
                if (prtBid == 0)
                {
                    nilChanceThreshold = 0.9;
                }
                else
                {
                    nilChanceThreshold = nilChanceThreshold + 0.6 - 0.2 * prtBid;
                }
            }
            nilChanceThreshold = Math.Round(Math.Min(Math.Max(nilChanceThreshold, 0.1), 0.9),4);  // [0.1 - 0.8]
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "=== Whiz === \nNil Chance = {0}, Nil Threshold = {1}", nilChances, nilChanceThreshold);

            if (nilChances >= nilChanceThreshold &&
                !hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.King))
                return 0;
            else
                return spadesCount;
        }


        private double getNilChanceThreshold(int spadesCount, float initialBid, int botLevel)
        {
            switch (botLevel)
            {
                case 1:
                case 2:              
                case 3:                   
                case 4:                    
                case 5:                   
                case 6:
                default:
                    return 0.48 - 0.2 * (spadesCount - initialBid);
                case 7:
                    return 0.4 - 0.1 * (spadesCount - initialBid);
                case 8:
                    return 0.4 - 0.2 * (spadesCount - initialBid);
                case 9: 
                    return 0.4 - 0.3 * (spadesCount - initialBid);
                case 0:
                    return 0.4 - 0.4 * (spadesCount - initialBid);
            } 
        }
    }
}
