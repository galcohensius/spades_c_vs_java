﻿using SpadesAI.V1.Chain;
using SpadesAI.V4.Chain;

namespace SpadesAI.V4
{
    public class PlayingBrainWhiz : PlayingBrainV4
    {
        internal PlayingBrainWhiz(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            // Build the chain of responsibilities handlers

            handler = new PrepareAITurnDataHandlerV4(spadesBrain);              //turn prepare
            handler.SetNextHandler(new WhizPrepareTurnData(spadesBrain))        //Whiz additions
                .SetNextHandler(new NilHandlerV4(spadesBrain))                  //Nil (s+p)
                .SetNextHandler(new ProtectPartnerNilHandlerV4(spadesBrain))    //Cover Nil (p)
                .SetNextHandler(new BreakOpponentNilHandlerV4(spadesBrain))     //Set Nil (s+p)

                .SetNextHandler(new LeadHandlerV4(spadesBrain))                 //Lead a card (s+p)

                .SetNextHandler(new SpadesCutSoloHandlerV4(spadesBrain))        //Cut (s)
                .SetNextHandler(new SpadesCutPartnersHandlerV4(spadesBrain))    //Cut (p) 

                .SetNextHandler(new SureWinSoloHandlerV4(spadesBrain))          //Sure win (s)
                .SetNextHandler(new SureWinPartnersHandler(spadesBrain))        //Sure win (p)

                .SetNextHandler(new SureLoseSoloHandlerV4(spadesBrain))         //lose (s)

                .SetNextHandler(new ThirdPosHighPartnersHandlerV4(spadesBrain)) //3rd card high (p)

                .SetNextHandler(new UnknownHandlerV4(spadesBrain));             //default (s+p)
        }
    }
}
