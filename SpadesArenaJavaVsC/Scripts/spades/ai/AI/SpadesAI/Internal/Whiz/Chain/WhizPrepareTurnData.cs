﻿using System;
using SpadesAI.model;
using System.Collections.Generic;
using System.Linq;
using SpadesAI.V1.Chain;
using cardGames.models;

namespace SpadesAI.V4.Chain
{
    public class WhizPrepareTurnData : PrepareAITurnDataHandler
    {

        public WhizPrepareTurnData(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }
        public override void Handle(AITurnData turnData)
        {
            Position myPos = turnData.PlayerPos;
            Dictionary<Position, int> spadesCountDic = new Dictionary<Position, int>();

            int opponentsNils = 0;
            bool isMeNil = false;
            foreach (Position p in (Position[])Enum.GetValues(typeof(Position)))
            {
                if (spadesBrain.Players[p].Bid == 0)
                {
                    if (p == myPos)
                    {
                        isMeNil = true;
                    }
                    else
                    {
                        opponentsNils++;
                    }
                }
            }
            
            // deduce #Spades according to bids when there is less than 2 opponents that bid 0.
            foreach (Position p in (Position[])Enum.GetValues(typeof(Position)))
            {
                if (p == myPos)
                {
                    spadesCountDic[p] = spadesBrain.Players[myPos].Hand.CardsBySuits(Card.SuitType.Spades).Count;
                }
                else
                {
                    if (spadesBrain.Players[p].Bid > 0)
                    {
                        spadesCountDic[p] = spadesBrain.Players[p].Bid -
                                            spadesBrain.Players[p].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count;
                    }
                    else // Players[p].Bid == 0
                    {
                        if (opponentsNils == 1)
                        {
                            if (!isMeNil)
                            {
                                // if only one nil, then the number of spades he start with is (13-total bids)
                                spadesCountDic[p] = 13 - CalcTotalBids()
                                                       - spadesBrain.Players[p].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count;
                            }
                            else //isMeNil
                            {
                                // 1 opp nil & myself nil.  opp Niler has spades: 13 -total bids - my spades in hand.
                                spadesCountDic[p] = 13 - CalcTotalBids()
                                                       - spadesBrain.Players[myPos].Hand.CardsBySuits(Card.SuitType.Spades).Count
                                                       - spadesBrain.Players[myPos].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count
                                                       - spadesBrain.Players[p].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count;
                            }
                        }
                        // when (opponents Nils > 1) can know #spades only for non-Nilers
                        //else{}
                    }
                }
                
                if (spadesCountDic.ContainsKey(p))
                {
                    if (spadesCountDic[p] == 0) // void in spades
                    {
                        spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Spades);
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Void in Spades ", p);
                    }
                    else if (spadesCountDic[p] == spadesBrain.Players[turnData.PlayerPos].Hand.Count) // void in all but spades
                    {
                        spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Clubs);
                        spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Diamonds);
                        spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Hearts);
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} Void in all but Spades ", p);
                    }
                }
            }

            // Always move to next handler
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "round {4}, {5}, Prepare Turn successful, {0}, {1}, {2}, {3}, leading suit: {8}, cards on table: {9}, cards to use: {7}, cards to play: {6}", spadesBrain.PlayingBrain, spadesBrain.MatchData.Mode, spadesBrain.MatchData.Variation, spadesBrain.MatchData.SubVariation, spadesBrain.MatchData.RoundNum, turnData.PlayerPos, turnData.CardToPlay, turnData.CardsToUse.ToStringCardsOnly(), spadesBrain.LeadingSuit, string.Join(";", spadesBrain.CardsOnTable.Select(x => x.Value)));
            nextHandler.Handle(turnData);
        }


        protected int CalcTotalBids()
        {
            int result = 0;
            foreach (Position p in Enum.GetValues(typeof(Position))){
                result += Math.Max(spadesBrain.Players[p].Bid, 0);}
            return result;
        }
    }
}
