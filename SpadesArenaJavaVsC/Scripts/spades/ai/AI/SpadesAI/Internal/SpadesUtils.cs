﻿using System.Collections.Generic;
using SpadesAI.model;
using CommonAI;
using spades.models;
using System;
using System.Linq;
using spades.utils;
using SpadesAI.V4;
using cardGames.models;

namespace SpadesAI
{
    public static class SpadesUtils
    {
        public enum WinStatus { SureWin, SureLose, Unsure }

        static readonly List<Position> allPositions;

        static SpadesUtils()
        {
            Position[] positions = {
                Position.South,
                Position.West,
                Position.North,
                Position.East,
                Position.South,
                Position.West,
                Position.North
            };
            allPositions = new List<Position>(positions);
        }


        public static Card FindWinningCard(Card.SuitType? leadingSuit, params Card[] cards)
        {
            if (cards.Length == 0)
                return null;

            Card result = cards[0];

            for (int i = 1; i < cards.Length; i++)
            {
                if (result.Suit == cards[i].Suit)
                {
                    // Same suit check rank
                    if (cards[i].Rank > result.Rank)
                        result = cards[i];
                }
                else if (cards[i].Suit == Card.SuitType.Spades)
                {
                    // Card is spades, must be better
                    result = cards[i];
                }
                else if (result.Suit == Card.SuitType.Spades)
                {
                    // current result is spades, must be better
                }
                else if (cards[i].Suit == leadingSuit)
                {
                    result = cards[i];
                }
            }
            return result;
        }


        public static WinStatus FindWinStatus(Card card, Card.SuitType? leadingSuit, IEnumerable<Card> cards)
        {
            int total = 0;
            int winners = 0;

            // Search for the first card that makes this card lose
            foreach (Card curCard in cards)
            {
                if (card.Suit == curCard.Suit)
                {
                    // Same suit check rank
                    if (curCard.Rank > card.Rank)
                        winners++;
                }
                else if (curCard.Suit == Card.SuitType.Spades)
                {
                    // card is not spades, always lose
                    winners++;
                }
                else if (card.Suit != Card.SuitType.Spades && card.Suit != leadingSuit && curCard.Suit == leadingSuit)
                {
                    // Card is not leading suit - always lose
                    winners++;
                }
                total++;
            }

            if (winners == 0)
                return WinStatus.SureWin;
            if (winners == total)
                return WinStatus.SureLose;
            return WinStatus.Unsure;
        }


        public static Position FindWinningPositionOnTable(SpadesBrainImpl spadesBrain)
        {
            CardsList cardsToCheck = new CardsList();
            foreach (Card card in spadesBrain.CardsOnTable.Values)
            {
                cardsToCheck.Add(card);
            }

            Card winningCard = FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck.ToArray());
            foreach (Position pos in spadesBrain.CardsOnTable.Keys)
            {
                if (winningCard == spadesBrain.CardsOnTable[pos])
                    return pos;
            }

            return 0;
        }

        public static List<Position> PlayersPositionsLeftToPlay(Position playerPos, int alreadyPlayed)
        {

            return allPositions.GetRange((int)playerPos + 1, 3 - alreadyPlayed);
        }

        public static List<Position> OpponentsPositions(Position playerPos, PlayingMode mode, int alreadyPlayed = 0)
        {

            if (mode == PlayingMode.Solo)
            {
                return PlayersPositionsLeftToPlay(playerPos, alreadyPlayed);
            }
            else
            {
                List<Position> result = new List<Position>();
                result.AddRange(PlayersPositionsLeftToPlay(playerPos, alreadyPlayed));
                if (result.Count >= 2)
                    result.RemoveAt(1);

                return result;
            }
        }

        public static Position LhoPosition(Position playerPos)
        {
            return allPositions[(int)playerPos + 1];
        }
        public static Position PartnerPosition(Position playerPos)
        {
            return allPositions[(int)playerPos + 2];
        }

        public static Position RhoPosition(Position playerPos)
        {
            return allPositions[(int)playerPos + 3];
        }

        /// <summary>
        /// Gets the playable cards from the player's hand according to leading suit and spades broken state. *without adjacent cards*
        /// </summary>
        /// <returns>The playable cards, without adjacent cards.</returns>
        internal static CardsList GetPlayableCardsRemoveAdjacent(Card.SuitType? leadingSuit, bool spadesBroken, CardsList hand, SpadesBrainImpl spadesBrain)
        {
            CardsList playableCards = GetPlayableCards(leadingSuit, spadesBroken, hand);
            playableCards.SortHighToLowSuitFirst();

            foreach (Card playableCard in playableCards.ToList())
            {
                Card oneCardHigher = CalcOneHigherRankCard(playableCard, spadesBrain, false);

                if (playableCards.Contains(oneCardHigher))
                {
                    playableCards.Remove(playableCard);
                }
            }
            return playableCards;
        }

        // Gets the playable cards from the player's hand according to leading suit and spades broken state. (for any variation but callBreak).
        internal static CardsList GetPlayableCards(Card.SuitType? leadingSuit, bool spadesBroken, CardsList hand)
        {
            return GetPlayableCards(leadingSuit, spadesBroken, hand, PlayingVariant.Classic, null);
        }

        /// <summary>
        /// Gets the playable cards from the player's hand according to leading suit and spades broken state.
        /// </summary>
        /// <returns>The playable cards.</returns>
        internal static CardsList GetPlayableCards(Card.SuitType? leadingSuit, bool spadesBroken, CardsList hand, PlayingVariant gameVariation, CardsList cardsOnTable)
        {
            if (gameVariation != PlayingVariant.CallBreak)
            {
                if (leadingSuit == null)
                {
                    // Player is first, can play any card and spades if spades-broken or spades-tight
                    if (spadesBroken || hand.HasSingleSuit(Card.SuitType.Spades))
                    {
                        // All hand is playable
                        return new CardsList(hand);
                    }
                    else
                    {
                        // Only non spades are playable
                        return hand.CardsBySuits(Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts);
                    }
                }
                else
                {
                    // Must use leading suit or all hand
                    if (hand.CountSuit((Card.SuitType)leadingSuit) > 0)
                    {
                        // Only leading suit is playable
                        return hand.CardsBySuits((Card.SuitType)leadingSuit);
                    }
                    else
                    {
                        // All hand is playable
                        return new CardsList(hand);
                    }
                }
            }
            else
            {
                // CallBreak:
                return GetPlayableCardsCallBreak(leadingSuit, hand, cardsOnTable);
            }
        }

 


        private static CardsList GetPlayableCardsCallBreak(Card.SuitType? leadingSuit, CardsList hand, CardsList cardsOnTable)
        {
            if (leadingSuit == null) // leader:
            {
                // Player is first, can play any card
                return new CardsList(hand);
            }
            else // follower:
            {
                Card currentWinningCard = CardsUtils.FindWinningCard(leadingSuit, cardsOnTable);

                if (hand.CountSuit((Card.SuitType) leadingSuit) > 0) // has leading suit
                {
                    if (currentWinningCard.Suit == leadingSuit) // no one cuts before you
                    {
                        if (hand.CardsBySuits((Card.SuitType) leadingSuit).CountGreaterThan(currentWinningCard.Rank) > 0
                        ) // you have a higher card from this suit
                        {
                            return hand.CardsBySuits((Card.SuitType) leadingSuit).GetAllCardsAboveRank(currentWinningCard.Rank);
                        }
                        else // dont have a winner -> can play any of the leading suit
                        {
                            return hand.CardsBySuits((Card.SuitType) leadingSuit);
                        }
                    }
                    else // someone cuts before you + you have leading suit  -> can play any of the leading suit
                    {
                        return hand.CardsBySuits((Card.SuitType) leadingSuit);
                    }
                }
                else // don't have leading suit
                {
                    if (currentWinningCard.Suit == leadingSuit) // no one cuts before you
                    {
                        if (hand.CardsBySuits(Card.SuitType.Spades).Count > 0) // you have Spades -> must play spade
                        {
                            return hand.CardsBySuits(Card.SuitType.Spades);
                        }
                        else // dont have spades -> can play anything
                        {
                            return hand;
                        }
                    }
                    else // someone cuts before you
                    {
                        if (hand.CardsBySuits(Card.SuitType.Spades).CountGreaterThan(currentWinningCard.Rank) > 0
                        ) // has higher spade -> must play higher spade
                        {
                            return hand.CardsBySuits(Card.SuitType.Spades).GetAllCardsAboveRank(currentWinningCard.Rank);
                        }
                        else // dont have higher spade -> can play anything
                        {
                            return hand;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the partner has already put the best card on the table.
        /// </summary>
        /// <param name="playerPos"> The player position.</param>
        /// <param name="spadesBrain"> The spades brain.</param>
        /// <returns> True if the partner has already put the best card on the table.</returns>
        public static bool IsPartnerAboutToWin(Position playerPos, SpadesBrainImpl spadesBrain)
        {
            Position partnerPos = PartnerPosition(playerPos);
            spadesBrain.CardsOnTable.TryGetValue(partnerPos, out Card partnerCard);
            if (partnerCard != null)
            {
                // Partner played.
                Card[] cardsToCheck = new Card[spadesBrain.CardsOnTable.Count];
                spadesBrain.CardsOnTable.Values.CopyTo(cardsToCheck, 0);

                Card winningCard = FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck);

                if (cardsToCheck.Length == 2)
                {
                    // The partner was 1st to play

                    if (partnerCard != winningCard)
                        return false; // The partner can never win, his card already loses

                    if (partnerCard.Rank == Card.RankType.Ace || partnerCard.Rank == Card.RankType.King)
                    {
                        // The partner threw an Ace or King, he has a good chance to win (or at least thinks he has...)
                        return true;
                    }


                    if (partnerCard.Suit == Card.SuitType.Spades && spadesBrain.LeadingSuit != Card.SuitType.Spades)
                    {
                        // The partner cut with spades. He has a good chance to win.
                        return true;
                    }
                }
                else
                {
                    // If partner was 2nd player, check that is card is a winner
                    if (partnerCard == winningCard)
                    {
                        return true;
                    }

                }

            }
            return false;
        }

        /// <summary>
        /// Retrieves the weakest bot card which is not a sure loser, if any.
        /// </summary>
        /// <param name="cardsToUse"> The bot cards. </param>
        /// <param name="partnerPos"> The partner position. </param>
        /// <param name="spadesBrain"> The spades brain. </param>
        /// <returns> The weakest bot card which is not a sure loser, if any. </returns>
        public static Card GetWeakestAbovePartner(AIStrengthCardsList cardsToUse, Position partnerPos, SpadesBrainImpl spadesBrain)
        {
            CardsList abovePartnerCards = new CardsList();
            foreach (Card card in cardsToUse)
            {

                // Determine the current leading suit. If the user is first, the current card will be the leading suit.
                Card.SuitType? leadingSuit = spadesBrain.LeadingSuit ?? card.Suit;

                // Pick at the partner's playable cards.
                CardsList partnerPlayableCards = GetPlayableCards(leadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[partnerPos].Hand,
                                                    spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));

                // If this card is not a sure lose, then it can protect the nil.
                if (FindWinStatus(card, spadesBrain.LeadingSuit, partnerPlayableCards) != WinStatus.SureLose)
                {
                    abovePartnerCards.Add(card);
                }
            }

            if (abovePartnerCards.Count > 0)
            {
                return abovePartnerCards[abovePartnerCards.Count - 1];
            }

            return null;
        }


        /// <summary>
        /// Check if any opponent left to play is void on a specific suit
        /// </summary>
		public static bool IsAnyOpponentVoidOnSuit(Position playerPosition, SpadesBrainImpl spadesBrain, Card.SuitType suit)
        {
            foreach (Position position in OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count))
            {
                if (spadesBrain.Players[position].VoidSuits.Contains(suit))
                    return true;
            }
            return false;

        }

        /// <summary>
        /// Check if any opponent left to play is void on a specific suit and not void in Spades
        /// </summary>
        public static bool IsAnyOpponentVoidOnSuitAndNotInSpadesAndNotNil(Position playerPosition, SpadesBrainImpl spadesBrain, Card.SuitType suit)
        {
            foreach (Position position in OpponentsPositions(playerPosition, spadesBrain.MatchData.Mode, spadesBrain.CardsOnTable.Count))
            {
                if (spadesBrain.Players[position].VoidSuits.Contains(suit) &&
                    !spadesBrain.Players[position].VoidSuits.Contains(Card.SuitType.Spades) &&
                     spadesBrain.Players[position].Bid > 0)
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Check if partner left to play and is void on a specific suit and not void in Spades
        /// </summary>
        public static bool IsPartnerVoidOnSuitAndNotInSpadesAndNotNil(Position playerPosition, SpadesBrainImpl spadesBrain, Card.SuitType suit)
        {
            if (spadesBrain.CardsOnTable.Count < 2)
            {
                if (spadesBrain.Players[PartnerPosition(playerPosition)].VoidSuits.Contains(suit) &&
                    !spadesBrain.Players[PartnerPosition(playerPosition)].VoidSuits.Contains(Card.SuitType.Spades) &&
                     spadesBrain.Players[PartnerPosition(playerPosition)].Bid > 0)
                    return true;
            }
            return false;
        }



        /// <summary/>
        /// returns number of takes you get for sure, given your spades and unplayed opponent spades
        /// Two situation generate sure takes: 
        /// 1. spades bosses
        /// 2. more spades than opponents have
        /// in worse case, 1 opponent has all unseen spades, you always lead and play your spades high to low, if opponent have a higher spade he play it, else he play lowest.
        /// <returns/>int
        public static int CalcTakesForSureFromSpade(Position myPos, SpadesBrainImpl spadesBrain)
        {
            CardsList mySpades = spadesBrain.Players[myPos].Hand.CardsBySuits(Card.SuitType.Spades);
            CardsList spadesPlayed = GetSpadesPlayed(spadesBrain);
            // spadesLeft = all 13/15 spades - mySpades - spadesPlayed
            CardsList spadesLeft = GetSpadesLeft(spadesBrain, mySpades, spadesPlayed);

            if (mySpades.Count == 0)
                return 0; // no spades
            if (spadesLeft.Count == 0)
                return mySpades.Count; // opponents dont have any spades -> all my spades are takes
            mySpades.SortHighToLowSuitFirst();
            spadesLeft.SortHighToLowSuitFirst();

            int sureTakes = 0;
            CardsList mySpadesCopy = new CardsList(mySpades);
            foreach (Card s in mySpadesCopy)
            {
                if (spadesLeft.CountGreaterThan(s.Rank) > 0)  // opponent have a higher spade
                {
                    spadesLeft.RemoveCardsFromBeginning(1);  // strongest spade wins s
                }
                else
                {
                    spadesLeft.RemoveCardsFromEnd(1);  // weakest spade lose to s
                    sureTakes++;
                }
                mySpades.RemoveCardsFromBeginning(1);
                if (mySpades.Count == 0) return sureTakes;
                if (spadesLeft.Count == 0) return sureTakes + mySpades.Count;
            }
            return sureTakes;
        }

        private static CardsList GetSpadesLeft(SpadesBrainImpl spadesBrain, CardsList mySpades, CardsList spadesPlayed)
        {
            CardsList spadesLeft = spadesBrain.PlayingBrain is PlayingBrainJokers
                ? CardsListBuilder.FullDeckWithJokers().CardsBySuits(Card.SuitType.Spades)
                : CardsListBuilder.FullDeckNoJokers().CardsBySuits(Card.SuitType.Spades);

            if (mySpades.Count > 0) spadesLeft.RemoveAll(mySpades.Contains);
            if (spadesPlayed.Count > 0) spadesLeft.RemoveAll(spadesPlayed.Contains);
            return spadesLeft;
        }

        private static CardsList GetSpadesPlayed(SpadesBrainImpl spadesBrain)
        {
            CardsList spades = new CardsList(); // out of the round, already played
            foreach (Position p in Enum.GetValues(typeof(Position)))
            {
                spades.AddRange(spadesBrain.Players[p].ThrowHistory.CardsBySuits(Card.SuitType.Spades));
            }

            return spades;
        }


        /// strongest unplayed card in the suit
        public static Card CalcBoss(Card.SuitType s, SpadesBrainImpl spadesBrain)
        {
            CardsList unplayedCards = spadesBrain.PlayingBrain is PlayingBrainJokers ? CardsListBuilder.FullDeckWithJokers().CardsBySuits(s) : CardsListBuilder.FullDeckNoJokers().CardsBySuits(s);
            foreach (Card c in spadesBrain.ThrowHistory.CardsBySuits(s)) unplayedCards.Remove(c);
            if (unplayedCards.Count == 0)
                return null;
            unplayedCards.SortHighToLowRankFirst();
            return unplayedCards[0];
        }

        /// weakest unplayed card in the suit
        public static Card CalcWeakestUnplayed(Card.SuitType s, SpadesBrainImpl spadesBrain)
        {
            CardsList unplayedCards = spadesBrain.PlayingBrain is PlayingBrainJokers ? CardsListBuilder.FullDeckWithJokers().CardsBySuits(s) : CardsListBuilder.FullDeckNoJokers().CardsBySuits(s);
            foreach (Card c in spadesBrain.ThrowHistory.CardsBySuits(s)) unplayedCards.Remove(c);
            if (unplayedCards.Count == 0)
                return null;
            unplayedCards.SortHighToLowRankFirst();
            return unplayedCards[unplayedCards.Count - 1];
        }


        /// unplayed deck
        public static CardsList GetUnplayedDeck(SpadesBrainImpl spadesBrain)
        {
            CardsList unplayedCards = spadesBrain.PlayingBrain is PlayingBrainJokers ? CardsListBuilder.FullDeckWithJokers() : CardsListBuilder.FullDeckNoJokers();
            foreach (Card c in spadesBrain.ThrowHistory) unplayedCards.Remove(c);
            if (unplayedCards.Count == 0)
                return null;
            unplayedCards.SortHighToLowSuitFirst();
            return unplayedCards;
        }


        /// find rank + 1 while disregarding played cards
        public static Card CalcOneHigherRankCard(Card card, SpadesBrainImpl spadesBrain, bool IfNoGreaterReturnSameCard = true)
        {
            CardsList unplayedCards = CardsListBuilder.FullDeckNoJokers().CardsBySuits(card.Suit);
            foreach (Card playedCard in spadesBrain.ThrowHistory.CardsBySuits(card.Suit)) unplayedCards.Remove(playedCard);
            if (unplayedCards.Count == 0)
                return null;
            unplayedCards.SortHighToLowRankFirst();
            int index = unplayedCards.FindIndex(a => a == card);
            if (index > 0)
            {
                return unplayedCards[index - 1];
            }
            else
            {
                if (IfNoGreaterReturnSameCard)
                {
                    return unplayedCards[index];
                }
                else return null;
            }
        }


        public static int PointsIfSuccessBid(PlayingMode gameMode, int bid, bool blindNil = false)
        {
            int pointFromThisRound = 0;
            if (gameMode == PlayingMode.Solo)
            {
                if (bid > 0) { pointFromThisRound = bid * 10; }
                else if (bid == 0) { pointFromThisRound = 50; }
//                else if (bid == -1) // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "player didn't bid yet (-1)");
//                else // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "! bug in pointsIfSuccessBid - unknown bid ! = " + bid);
            }
            else if (gameMode == PlayingMode.Partners)
            {
                if (bid > 0) { pointFromThisRound = bid * 10; }
                else if (bid == 0 && !blindNil) { pointFromThisRound = 100; }
                else if (bid == 0 && blindNil) { pointFromThisRound = 200; }
//                else if (bid == -1) // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "player didn't bid yet (-1)");
//                else // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "! bug in pointsIfSuccessBid - unknown bid ! = " + bid);
            }
            return pointFromThisRound;
        }

    }

}
