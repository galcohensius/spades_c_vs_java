﻿using System;
using System.Collections.Generic;
using SpadesAI.model;
using SpadesAI.V1;
using SpadesAI.Internal.V2;
using spades.models;
using SpadesAI.V3;
using SpadesAI.V2;
using System.Linq;
using SpadesAI.Internal.V4;
using SpadesAI.V4;
using cardGames.models;
using SpadesAI.Internal.UCT;

namespace SpadesAI
{
    public class SpadesBrainImpl : ISpadesBrain
    {
        private AIMatchData matchData;
        private Dictionary<Position, AIPlayer> players;
        private Dictionary<Position, Card> cardsOnTable;
        private Card.SuitType? leadingSuit;
        private bool spadesBroken;
        private CardsList throwHistory;

        private IBiddingBrain biddingBrain;
        private PlayingBrainBase playingBrain;
        private ChattingBrain chattingBrain;


        // default Constructor
        public SpadesBrainImpl()
        {
            IsVirtual = true;
        }

        // Constructor with BrainType
        public SpadesBrainImpl(BrainType brainV)
        {
            switch (brainV)
            {
                case BrainType.ClassicV1:
                    biddingBrain = new BiddingBrainV1(this);
                    playingBrain = new PlayingBrainV2(this);
                    break;
                case BrainType.ClassicV2:
                    biddingBrain = new BiddingBrainV2(this);
                    playingBrain = new PlayingBrainV2(this);
                    break;
                case BrainType.ClassicV3:
                    biddingBrain = new BiddingBrainV2(this);
                    playingBrain = new PlayingBrainV3(this);
                    break;
                case BrainType.ClassicV4:
                    biddingBrain = new BiddingBrainV4(this);
                    playingBrain = new PlayingBrainV4(this);
                    break;
                case BrainType.ClassicV4Peeking:
                    biddingBrain = new BiddingBrainV4(this);
                    playingBrain = new PlayingBrainV4_peeking(this); // SOLO: peeking
                    break;

                case BrainType.Mirror:
                    biddingBrain = new BiddingBrainMirror(this);
                    playingBrain = new PlayingBrainMirror(this);
                    break;
                case BrainType.Whiz:
                    biddingBrain = new BiddingBrainWhiz(this);
                    playingBrain = new PlayingBrainWhiz(this);
                    break;
                case BrainType.Suicide:
                    biddingBrain = new BiddingBrainSuicide(this);
                    playingBrain = new PlayingBrainV4(this);
                    break;
                case BrainType.Jokers:
                    biddingBrain = new BiddingBrainJokers(this);
                    playingBrain = new PlayingBrainJokers(this);
                    break;
                case BrainType.CallBreak:
                    biddingBrain = new BiddingBrainCallBreak(this);
                    playingBrain = new PlayingBrainCallBreak(this);
                    break;
                case BrainType.UCT:
                    biddingBrain = new BiddingBrainV4(this);
                    playingBrain = new PlayingBrainUct(this);
                    break;
            }

            chattingBrain = new ChattingBrain(this);
        }


        // Copy brain
        public void CopyBrainImpl(SpadesBrainImpl spadesBrainToCopy)
        {
            this.matchData = spadesBrainToCopy.matchData;

            if (this.throwHistory == null) this.throwHistory = new CardsList();
            this.throwHistory.CopyCardsList(spadesBrainToCopy.ThrowHistory);

            // if player==null player = new AI player
            if (this.players == null)
                this.players = new Dictionary<Position, AIPlayer>()
                {
                    {Position.South, new AIPlayer()},
                    {Position.East, new AIPlayer()},
                    {Position.North, new AIPlayer()},
                    {Position.West, new AIPlayer()},
                };
            foreach (var positionAIPlayerPair in spadesBrainToCopy.players)
            {
                this.players[positionAIPlayerPair.Key].CopyAiPlayer(positionAIPlayerPair.Value);
            }

            this.spadesBroken = spadesBrainToCopy.spadesBroken;

            //this.cardsOnTable = spadesBrainToCopy.cardsOnTable;
            if (this.cardsOnTable == null) this.cardsOnTable = new Dictionary<Position, Card>();
            foreach (var positionCardPair in spadesBrainToCopy.cardsOnTable)
            {
                this.cardsOnTable[positionCardPair.Key] = spadesBrainToCopy.cardsOnTable[positionCardPair.Key];
            }


            this.leadingSuit = spadesBrainToCopy.leadingSuit;

            this.biddingBrain = spadesBrainToCopy.biddingBrain;
            this.playingBrain = spadesBrainToCopy.playingBrain;
        }


        /// <summary>
        /// Starts a new match
        /// </summary>
        public void StartMatch(AIMatchData matchData)
        {
            this.matchData = matchData;
            spadesBroken = false;
        }

        /// <summary>
        /// Starts a new round
        /// </summary>
        public void StartRound(SpadesTable table, Dictionary<Position, int> skillLevels, Dictionary<Position, BrainType> brainTypes)
        {
            // Create the AI players
            // All players get a level according to the bots data, even if they are human
            players = new Dictionary<Position, AIPlayer>
            {
                {Position.South, new AIPlayer(brainTypes[Position.South])},
                {Position.East,  new AIPlayer(brainTypes[Position.East])},
                {Position.North, new AIPlayer(brainTypes[Position.North])},
                {Position.West,  new AIPlayer(brainTypes[Position.West])}
            };

            foreach (Position pos in skillLevels.Keys)
            {
                players[pos].Level = skillLevels[pos];
            }

            cardsOnTable = new Dictionary<Position, Card>();
            spadesBroken = false;
            leadingSuit = null;
            chattingBrain.RoundStarted();
            matchData.RoundNum++;
        }

        public void StartRound(int roundNum, Dictionary<Position, BrainType> brainTypes)
        {
            matchData.RoundNum = roundNum;
            cardsOnTable = new Dictionary<Position, Card>();
            leadingSuit = null;
            spadesBroken = false;

            players = new Dictionary<Position, AIPlayer>
            {
                {Position.South, new AIPlayer(brainTypes[Position.South])},
                {Position.East,  new AIPlayer(brainTypes[Position.East])},
                {Position.North, new AIPlayer(brainTypes[Position.North])},
                {Position.West,  new AIPlayer(brainTypes[Position.West])}
            };
        }


        public void SetHand(Position pos, CardsList hand)
        {
            players[pos].Hand = hand;
        }

        public CardsList GetHand(Position pos)
        {
            return players[pos].Hand;
        }

        /// <summary>
        /// Asks the brain to make a bid for a specified position
        /// </summary>
        public int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            try
            {
                int bid = biddingBrain.MakeBid(pos, prData);
                SetBid(pos, bid, Players[pos].BlindNil);
                return bid;
            }
            catch (Exception e)
            {
                // Catch any exception and play a default card if thrown
                // LoggerController.Instance.LogError("Illegal make bid from bot at pos " + pos + ".Exception: " + e);

                return 1;
            }
        }

        /// <summary>
        /// Sets the bid for a real player sitting in a specified position
        /// </summary>
        public void SetBid(Position pos, int bid, bool isBlindNil = false)
        {
            players[pos].Bid = bid;
            players[pos].BlindNil = isBlindNil;
        }

        public void StartTrick()
        {
        }

        /// <summary>
        /// Asks the brain to play a move for a specified position
        /// </summary>
        public Card PlayMove(Position pos, SpadesPlayerRoundData prData)
        {
            Card card;
            try
            {
                card = playingBrain.PlayMove(pos, prData);
            }
            catch (Exception e)
            {
                // Catch any exception and play a default card if thrown
                // LoggerController.Instance.LogError("Illegal make move from bot at pos " + pos + ".Exception: " + e);
                // LoggerController.Instance.LogErrorFormat(// LoggerController.Module.AI, "round {0}, {1}, exception at PlayMove, {2}, {3}, {4}, {5}, leading suit: {6}, cards on table: {7}",
//                        MatchData.RoundNum, pos, PlayingBrain, MatchData.Mode, MatchData.Variation, MatchData.SubVariation,  LeadingSuit, string.Join(";", CardsOnTable.Select(x => x.Value)));

                card = SpadesUtils.GetPlayableCards(LeadingSuit, SpadesBroken, Players[pos].Hand,
                        this.MatchData.Variation, new CardsList(this.CardsOnTable.Values)).First();
            }

            //SetMove (pos, myCard);
            return card;
        }

        /// <summary>
        /// Sets the move for a real player or Bot sitting in a specified position
        /// </summary>
        public void SetMove(Position pos, Card myCard)
        {
            if (!cardsOnTable.ContainsKey(pos))
                cardsOnTable.Add(pos, myCard);

            if (players[pos].Hand != null) players[pos].Hand.Remove(myCard);

            if (cardsOnTable.Count == 1)
            {
                leadingSuit = myCard.Suit;
                AITurnData.LeadingPlayer = pos;  // remember leading player's position
            }

            if (myCard.Suit == Card.SuitType.Spades)
            {
                spadesBroken = true;
            }

            // updating the Dictionary of Safe cards to Cover the Nil:
            try
            {
                if (leadingSuit != null)
                {
                    // player did not follow suit: add void suit and update SafeCoverNilDictionary
                    if (myCard.Suit != leadingSuit)
                    {
                        players[pos].VoidSuits.Add((Card.SuitType)leadingSuit);
                        players[pos].SafeCoverNilDictionary[(Card.SuitType)leadingSuit] = Card.RankType.Two;
                    }

                    // update SafeCoverNilDictionary
                    // in niler's turn, if cardsOnTable contain boss or trump != leading suit,
                    // then SafeCoverNilDictionary.add(niler suit, niler.rank-1)
                    if (MatchData.Mode == PlayingMode.Partners && Players[pos].Bid == 0 && Players[pos].Takes == 0) // Niler
                    {
                        // no one cut
                        Card boss = SpadesUtils.CalcBoss((Card.SuitType)leadingSuit, this);
                        if (CardsOnTable.ContainsValue(boss) 
                            && myCard.Suit == leadingSuit){
                            UpdateSafeNilCoverDic(pos, myCard, "Boss was played before Niler: {0} Niler is safe in {1} from rank {2}");
                        }

                        if (myCard.Suit != leadingSuit 
                            && myCard.Suit != Card.SuitType.Spades 
                            && leadingSuit != Card.SuitType.Spades) { // Niler duck different side suit
                            UpdateSafeNilCoverDic(pos, myCard, "Niler play different side suit: {0} Niler is safe in {1} from rank {2}");
                        }

                        // someone cut before Niler
                        List<Card> CardsOnTableList = CardsOnTable.Values.ToList();
                        List<Card.SuitType> SuitOnTableList = new List<Card.SuitType>();
                        foreach (Card tableCard in CardsOnTableList)
                        { SuitOnTableList.Add(tableCard.Suit); }

                        if (leadingSuit != Card.SuitType.Spades && SuitOnTableList.Contains(Card.SuitType.Spades))
                        {
                            if (myCard.Suit == leadingSuit) // someone cut before Niler and Niler follow leading Suit
                            {
                                UpdateSafeNilCoverDic(pos, myCard, "Someone cut before Niler: {0} Niler is safe in {1} from rank {2}");
                            }
                            else if (myCard.Suit == Card.SuitType.Spades)  // someone cut before Niler and Niler played a Spade
                            {
                                Card spadesBoss = SpadesUtils.CalcBoss(Card.SuitType.Spades, this);
                                if (CardsOnTable.ContainsValue(spadesBoss))
                                {
                                    UpdateSafeNilCoverDic(pos, myCard, "cut with Spades boss before Niler: {0} Niler is safe in {1} from rank {2}");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                // Catch any exception and play a default card if thrown
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "Exception thrown from update SafeCoverNilDictionary: " + e);
            }

        }

        private void UpdateSafeNilCoverDic(Position pos, Card myCard, String debugText)
        {
            players[pos].SafeCoverNilDictionary[myCard.Suit] = myCard.Rank;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, debugText, pos, myCard.Suit, myCard.Rank);
            if (myCard.Rank == SpadesUtils.CalcWeakestUnplayed(myCard.Suit, this).Rank) // lowest unplayed 
            {
                players[pos].VoidSuits.Add(myCard.Suit);
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "{0} is void on {1} since Niler played lowest unplayed card in the suit", pos, leadingSuit);
            }
        }

        public void TakeTrick(Position takerPos)
        {
            foreach (Position pos in Enum.GetValues(typeof(Position)))
            {
                if (cardsOnTable.ContainsKey(pos))
                    players[pos].ThrowHistory.Add(cardsOnTable[pos]);
            }
            cardsOnTable.Clear();
            players[takerPos].Takes++;
            leadingSuit = null;

            if (!IsVirtual){
                try{
                    chattingBrain.TrickTaken(takerPos);
                }
                catch (Exception e){
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.chattingBrain, "Exception: " + e + " at the Chatting brain at SpadesBrainImpl.TakeTrick");
                    Console.WriteLine(e);
                }
            }
        }

        public bool ThinkBid(Position pos)
        {
            return true;
        }

        public bool ThinkMove(Position pos)
        {
            return true;
        }


        internal Dictionary<Position, AIPlayer> Players
        {
            get
            {
                return players;
            }
        }

        internal Dictionary<Position, Card> CardsOnTable
        {
            get
            {
                return cardsOnTable;
            }
        }

        internal Card.SuitType? LeadingSuit
        {
            get
            {
                return leadingSuit;
            }
        }

        internal bool SpadesBroken
        {
            get
            {
                return spadesBroken;
            }
        }

        internal CardsList ThrowHistory
        {
            get
            {
                CardsList result = new CardsList();
                foreach (AIPlayer player in players.Values)
                {
                    result.AddRange(player.ThrowHistory);
                }
                return result;
            }
        }

        internal PlayingBrainBase PlayingBrain
        {
            get
            {
                return playingBrain;
            }
        }

        public IBiddingBrain BiddingBrain
        {
            get
            {
                return biddingBrain;
            }
        }

        public ChattingBrain ChattingBrain
        {
            get
            {
                return chattingBrain;
            }
        }

        public AIMatchData MatchData
        {
            get
            {
                return matchData;
            }

            set
            {
                matchData = value;
            }
        }
        public bool IsVirtual { get; private set; }
    }
}
