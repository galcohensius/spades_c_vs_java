﻿using System.Collections.Generic;
using cardGames.models;
using CommonAI;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V3.Chain
{
    public class BreakOpponentNilSoloPeekingHandler : AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "BON";

        public BreakOpponentNilSoloPeekingHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {

            if (spadesBrain.MatchData.Mode != PlayingMode.Solo) {
                nextHandler.Handle(turnData);
                return;
            }
			int strategyRule = 0;

            // TODO: Do not break nil when:
            // 1. If we win this round we win the game + the opponents will not get points above us
            // 2. If the total bid is >=13, check the points if bids are met. If the opponents are not above us, do not break


            // Iterate every opponent who bid nil, look for one who can be broken.
            foreach (Position opponentNilPosition in turnData.OpponentNilPositions) {
                // Check if nil bidder already threw a card	
                if (spadesBrain.CardsOnTable.ContainsKey(opponentNilPosition))
                {
                    Card nilBidderCard = spadesBrain.CardsOnTable[opponentNilPosition];

                    // Check if the nil bidder's card is the highest on the table
                    if (SpadesUtils.FindWinningPositionOnTable(spadesBrain) == opponentNilPosition) {
                        if (turnData.CardsToUse.CountSuit(spadesBrain.LeadingSuit) > 0)
                        {
                            if (nilBidderCard.Rank > Card.RankType.Four || spadesBrain.CardsOnTable.Count == 3)
                            {
                                Card strongestCardBelowRank = turnData.CardsToUse.CardsBySuits(nilBidderCard.Suit).GetStrongestBelowRank(nilBidderCard.Rank);
                                if (strongestCardBelowRank != null)
                                {
                                    // Try to break.
                                    // Use the strongest losing card, since weaker cards might be required to break later.
                                    turnData.CardToPlay = strongestCardBelowRank;
                                    strategyRule = 1;
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} trying to break {1} nil with strongest losing card", turnData.PlayerPos,opponentNilPosition);
                                }
                            }
                            else
                            {
                                // low chances thus do not try to break 
                                continue;
                            }
                        } else {
                            List<Card.SuitType> nonSpadeSuitsList = SuitUtils.GetOtherSuits(new List<Card.SuitType> { Card.SuitType.Spades, spadesBrain.LeadingSuit.Value });
                            CardsList nonSpadeCardsList = turnData.CardsToUse.CardsBySuits(nonSpadeSuitsList);
                            if (nonSpadeCardsList.Count > 0) {
                                // Try to break.
                                // Use a medium non spade card, since weaker cards might be required to break later.
                                turnData.CardToPlay = nonSpadeCardsList[nonSpadeCardsList.Count / 2];
								strategyRule = 2;
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} trying to break {1} nil with medium void losing card", turnData.PlayerPos, opponentNilPosition);
                            }
                        }

                    } else {
                        // Nil bidder is already covered.
                        continue;
                    }
                }
                else
                {
                    // Nil bidder has not played yet:

                    // Check if the nil bidder's cards can protect him versus the cards currently on the table. If so, there is no reason to try to break.
                    bool isSureLossCardsOnTable = true;
                    if (spadesBrain.CardsOnTable.Count > 0)
                    {
                        // Find the winning card on the table.
                        Card[] cardsToCheck = new Card[spadesBrain.CardsOnTable.Count];
                        spadesBrain.CardsOnTable.Values.CopyTo(cardsToCheck, 0);
                        Card winningCardOnTable = SpadesUtils.FindWinningCard(spadesBrain.LeadingSuit, cardsToCheck);

                        // Peek at the nil bidder's playable cards.
                        CardsList nilBidderPlayableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[opponentNilPosition].Hand,
                                                            spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));

                        // Compare the winning card to the nil bidder's cards.
                        if (SpadesUtils.FindWinStatus(winningCardOnTable, spadesBrain.LeadingSuit, nilBidderPlayableCards) != SpadesUtils.WinStatus.SureLose)
                        {
                            isSureLossCardsOnTable = false;
                        }
                    }

                    // No need to try to break. 
                    // The nil bidder's cards will protect him versus the cards currently on the table.
                    if (!isSureLossCardsOnTable) {
                        continue;
                    }

                    // Try to find a card which will lose to every niler card.
                    CardsList sureLoseCards = new CardsList();
                    foreach (Card card in turnData.CardsToUse) {
                        
                        // Determine the current leading suit. If the user is first, the current card will be the leading suit.
                        Card.SuitType? leadingSuit = spadesBrain.LeadingSuit ?? card.Suit;

                        // Peek at the nil bidder's playable cards.
                        CardsList nilBidderPlayableCards = SpadesUtils.GetPlayableCards(leadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[opponentNilPosition].Hand,
                                                            spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));

                        // Look at the bot card and the niler's hand. Look for cards which lose to the nil bidder for 100%.
                        // This action does not consider the future action by max additional 2 players.
                        if (SpadesUtils.FindWinStatus(card, leadingSuit, nilBidderPlayableCards) == SpadesUtils.WinStatus.SureLose) {
                            sureLoseCards.Add(card);
                        }
                    }

                    if (sureLoseCards.Count > 0 && spadesBrain.Players[turnData.PlayerPos].Level != 0) { // Level 0 does not obvious peek.
                        turnData.CardToPlay = sureLoseCards[sureLoseCards.Count - 1];
						strategyRule = 3;
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} trying to break {1} nil with weakest sure loser card", turnData.PlayerPos, opponentNilPosition);
                    } else {
                        // Change the strategy to under to affect the next handlers.
                        // This will cause high cards to be used as strongest losers, and low cards to be kept for later use.
                        turnData.Strategy = AIStrategy.Under;
                        continue;
                    }
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else{
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}
