﻿using spades.models;
using SpadesAI.model;
using SpadesAI.V1.Chain;

namespace SpadesAI.V3.Chain {
    public class BreakOpponentNilPartnersHandler : BreakOpponentNilHandlerV2 {
		public BreakOpponentNilPartnersHandler(SpadesBrainImpl spadesBrain):base(spadesBrain)
		{
		}

		public override void Handle (AITurnData turnData){

            if (spadesBrain.MatchData.Mode != PlayingMode.Partners) {
                nextHandler.Handle(turnData);
                return;
            }

            base.Handle(turnData);
        }
	}
}

