﻿using SpadesAI.model;
using CommonAI;
using System.Collections.Generic;
using SpadesAI.Internal;
using cardGames.models;

namespace SpadesAI.V3.Chain {
    public class ProtectPartnerNilPeekingHandler : AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "PPN";

        public ProtectPartnerNilPeekingHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        /// <summary>
		/// Removes the (potentially) unprotected cards from the cards list.
		/// </summary>
		public override void Handle(AITurnData turnData) {
            if (!turnData.ProtectPartnerNil) {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);

            // Check if the partner has already threw a card
            if (spadesBrain.CardsOnTable.ContainsKey(partnerPos)) {
                Card partnerCard = spadesBrain.CardsOnTable[partnerPos];

                // Check if the partner's card is the highest on the table
                if (SpadesUtils.FindWinningPositionOnTable(spadesBrain) == partnerPos) {
                    if (turnData.CardsToUse.CountSuit(partnerCard.Suit) > 0) {
                        Card weakestCardAboveRank = turnData.CardsToUse.CardsBySuits(partnerCard.Suit).GetWeakestAboveRank(partnerCard.Rank);
                        if (weakestCardAboveRank != null) {
                            // Use a weak card, since a strong card might be required to protect later.
                            turnData.CardToPlay = weakestCardAboveRank;
							strategyRule = 1;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} protects partner's nil with weakest card", turnData.PlayerPos);
                        }
                    } else {
                        CardsList spadeCardsList = turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades);
                        if (spadeCardsList.Count > 0) {
                            // Use a weak spade card, since higher spade cards should be used to protect later.
                            turnData.CardToPlay = spadeCardsList[spadeCardsList.Count - 1];

							if (turnData.CardToPlay != null) {
								strategyRule = 2;
								// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} uses weakest spade", turnData.PlayerPos);
                            }
                        }
                    }
                } else {
                    // Already covered
                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} don't need to protect partner's nil", turnData.PlayerPos);
                }
            } else {
                // partner has not played yet

                if (spadesBrain.CardsOnTable.Count == 0) {
                    // Bot is the leader:
                    // Try to find first a card which the partner is void of.
                    // If none is found then use a card which is stronger than the partner's weakest card.

                    // Find the void suits of the partner.
                    HashSet<Card.SuitType> partnerVoids = new HashSet<Card.SuitType>();
                    foreach (Card.SuitType suit in SuitUtils.getAllSuitsNoJoker()) {
                        if (spadesBrain.Players[partnerPos].Hand.CountSuit(suit) == 0) {
                            partnerVoids.Add(suit);
                        }
                    }

                    if (partnerVoids.Count > 0) {
                        // Use a weak card that the partner is void off.
                        CardsList voidCardsList = turnData.CardsToUse.CardsBySuits(partnerVoids);
                        if (voidCardsList.Count > 0) {
                            turnData.CardToPlay = voidCardsList[voidCardsList.Count - 1];
							strategyRule = 3;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} protects partner's nil with weakest card of partner's void suit", turnData.PlayerPos);
                        }
                    }
                    if (turnData.CardToPlay == null) {
                        // Use a card which is stronger than the partner's weakest card because:
                        // 1. It will allow to save strong cards for later use.
                        // 2. An opponent may use a strong card, allowing to partner to get rid of a high card as well.
                        //    If this does not happen, the partner will always be able to use a weak card, as planned.
                        Card card = SpadesUtils.GetWeakestAbovePartner(turnData.CardsToUse, partnerPos, spadesBrain);
                        if (card != null) {
                            turnData.CardToPlay = card;
							strategyRule = 4;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} protects partner's nil with weakest card above partner card", turnData.PlayerPos);
                        }
                    }
                } else {
                    // Bot is second

                    // Peek at the partner's playable cards.
                    CardsList partnerPlayableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[partnerPos].Hand,
                                                        spadesBrain.MatchData.Variation, new CardsList(spadesBrain.CardsOnTable.Values));

                    // Check if the partner is protected by the card on the table.
                    Card RHOCard = spadesBrain.CardsOnTable[SpadesUtils.RhoPosition(turnData.PlayerPos)];
                    SpadesUtils.WinStatus winStatus = SpadesUtils.FindWinStatus(RHOCard, spadesBrain.LeadingSuit, partnerPlayableCards);
                    if (winStatus == SpadesUtils.WinStatus.SureLose) {
                        // The RHO card will surely lose to every partner card.
                        // The bot should help the partner.


                        // DANGER DANGER DANGER!!!


                        // Use a card which is stronger than the partner's weakest card because:
                        // 1. It will allow to save strong cards for later use.
                        // 2. An opponent may use a strong card, allowing to partner to get rid of a high card as well.
                        //    If this does not happen, the partner will always be able to use a weak card, as planned.
                        Card card = SpadesUtils.GetWeakestAbovePartner(turnData.CardsToUse, partnerPos, spadesBrain);
                        if (card != null) {
                            turnData.CardToPlay = card;
							strategyRule = 5;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} protects partner's nil with weakest card above partner card", turnData.PlayerPos);
                        }
                    } else {
                        // Already covered
                        // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "{0} don't need to protect partner's nil", turnData.PlayerPos);
                    }
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}


