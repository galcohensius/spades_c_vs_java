using SpadesAI.model;
using System.Collections.Generic;
using System.Linq;
using spades.models;
using SpadesAI.V1.Chain;
using SpadesAI.V1;

namespace SpadesAI.V2.Chain
{
    public class PrepareAITurnDataHandlerV2:PrepareAITurnDataHandler
	{

        public PrepareAITurnDataHandlerV2(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override void Handle(AITurnData turnData)
        {
            turnData.TakesForSureFromSpade = SpadesUtils.CalcTakesForSureFromSpade(turnData.PlayerPos, spadesBrain); // V4

            // Determine the AIStrategy type according to the bid distance.
            // For partners, bid distance of -2 and above is over
            // For solo, bid distance of -1 and above is over
            turnData.Strategy = spadesBrain.MatchData.Mode == PlayingMode.Partners ? ChoosePartnersStrategy(turnData) : ChooseSoloStrategy(turnData);

            // Check if the player bid nil.
            if (spadesBrain.Players[turnData.PlayerPos].Bid == 0 && spadesBrain.Players[turnData.PlayerPos].Takes == 0)
            {
                turnData.ProtectSelfNil = true;
            }

            // Protect partner nil flag.
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);
                if (spadesBrain.Players[partnerPos].Bid == 0 && spadesBrain.Players[partnerPos].Takes == 0)
                {
                    turnData.ProtectPartnerNil = true;
                }
            }

            // Nil position for opponents.
            List<Position> opponentsPositions = SpadesUtils.OpponentsPositions(turnData.PlayerPos, spadesBrain.MatchData.Mode);
            List<Position> opponentNilPositions = new List<Position>();
            foreach (var pos in opponentsPositions)
            {
                if (spadesBrain.Players[pos].Bid == 0 && spadesBrain.Players[pos].Takes == 0)
                {
                    opponentNilPositions.Add(pos);
                }
            }
            turnData.OpponentNilPositions = opponentNilPositions;

            // Get current player's or partner's bids and takes
            turnData.Bid = spadesBrain.Players[turnData.PlayerPos].Bid;
            turnData.Takes = spadesBrain.Players[turnData.PlayerPos].Takes;
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                Position partnerPos = SpadesUtils.PartnerPosition(turnData.PlayerPos);
                turnData.Bid += spadesBrain.Players[partnerPos].Bid;
                turnData.Takes += spadesBrain.Players[partnerPos].Takes;
            }

            // Playable cards.
            turnData.CardsToUse = spadesBrain.PlayingBrain.CalcPlayableCardsStrength(turnData.PlayerPos);

            // Always move to next handler
            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI, "round {4}, {5}, Prepare Turn successful, {0}, {1}, {2}, {3}, leading suit: {8}, cards on table: {9}, cards to use: {7}, cards to play: {6}", spadesBrain.PlayingBrain, spadesBrain.MatchData.Mode, spadesBrain.MatchData.Variation, spadesBrain.MatchData.SubVariation, spadesBrain.MatchData.RoundNum, turnData.PlayerPos, turnData.CardToPlay, turnData.CardsToUse.ToStringCardsOnly(), spadesBrain.LeadingSuit, string.Join(";", spadesBrain.CardsOnTable.Select(x => x.Value)));
            nextHandler.Handle(turnData);
        }
        // ------------------------------------------------------------------------------------------------ //


        public AIStrategy ChoosePartnersStrategy(AITurnData turnData)
        {
            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;
            Position pos = turnData.PlayerPos;
            Position partnerPos = SpadesUtils.PartnerPosition(pos);
            int partnersBags = spadesBrain.MatchData.MatchBags[pos] + spadesBrain.MatchData.MatchBags[partnerPos];
            int myPartnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;
            int myPartnerTakes = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Takes;
            int myBid = spadesBrain.Players[pos].Bid;
            int myTakes = spadesBrain.Players[pos].Takes;
            AIStrategy roundStrategy = AIStrategy.Over;
            int partnersDistanceFromBagsPenalty = bagsToPenalty - partnersBags;
            
            int opponentsPointsBeforeRound = 0;
            int opponentsBags = 0;
            int opponentsTotalBids = 0;
            int counter = 0;
            int opponent1Bid = 0;
            int opponent1Takes = 0;
            int opponent2Bid = 0;
            int opponent2Takes = 0;
            int partnersOverTakes = 0;
            int opponentsOverTakes = 0;
            bool partnersContractFulfilled = false;
            bool opponentsContractFulfilled = false;

            foreach (Position opponentPos in SpadesUtils.OpponentsPositions(pos, PlayingMode.Partners))
            {
                opponentsPointsBeforeRound += spadesBrain.MatchData.MatchPoints[opponentPos];
                opponentsBags += spadesBrain.MatchData.MatchBags[opponentPos];
                if (spadesBrain.Players[opponentPos].Bid > 0)
                    opponentsTotalBids += spadesBrain.Players[opponentPos].Bid;
                if (counter == 0)
                {
                    opponent1Bid = spadesBrain.Players[opponentPos].Bid;
                    opponent1Takes = spadesBrain.Players[opponentPos].Takes;

                }
                else
                {
                    opponent2Bid = spadesBrain.Players[opponentPos].Bid;
                    opponent2Takes = spadesBrain.Players[opponentPos].Takes;
                }
                counter++;
            }

            if (myTakes + myPartnerTakes >= myPartnerBid + myBid) partnersContractFulfilled = true; 
            if (opponent1Takes + opponent2Takes >= opponent1Bid + opponent2Bid) opponentsContractFulfilled = true;
            int partnersTotalTakes = myTakes + myPartnerTakes;
            int opponentsTotalTakes = opponent1Takes + opponent2Takes;
            int opponentsDistanceFromBagsPenalty = bagsToPenalty - opponentsBags;
            int partnersTotalBids = myBid + myPartnerBid;
            int totalBids = partnersTotalBids + opponentsTotalBids;
            int partnersMissingTakes = partnersTotalBids - partnersTotalTakes;
            int opponentsMissingTakes = opponentsTotalBids - opponentsTotalTakes;

            int takesLeft = 13 - partnersTotalTakes - opponentsTotalTakes;
            partnersOverTakes = (partnersTotalTakes - partnersTotalBids   > 0 ? partnersTotalTakes  - partnersTotalBids  : 0);
            opponentsOverTakes= (opponentsTotalTakes - opponentsTotalBids > 0 ? opponentsTotalTakes - opponentsTotalBids : 0);
            
            int underOver = totalBids - 13 + partnersOverTakes + opponentsOverTakes;
            turnData.BidDistance = underOver;

            int rule = 0;
            int numberOfBagsWeCouldFeedTheOpponents = takesLeft - opponentsMissingTakes;

            if (underOver < -2)
            {
                roundStrategy = AIStrategy.Under;
                rule = 1;
            }
            else if (underOver >= -2)
            {
                roundStrategy = AIStrategy.Over;
                rule = 1;
            }


           
            if (partnersContractFulfilled && opponentsDistanceFromBagsPenalty <= numberOfBagsWeCouldFeedTheOpponents)
            {
                roundStrategy = AIStrategy.Under;
                rule = 2;
            }
            else if (partnersContractFulfilled && !opponentsContractFulfilled && partnersDistanceFromBagsPenalty > takesLeft && takesLeft <=3 && underOver<1)
            {
                roundStrategy = AIStrategy.Over;
                rule = 3;
            }

            if (partnersContractFulfilled && partnersDistanceFromBagsPenalty <= 2)
            {
                roundStrategy = AIStrategy.Under;
                rule = 4;
            }

            if (underOver > 0 && opponentsContractFulfilled)
            {
                rule = 5;
                roundStrategy = AIStrategy.Under;
            }

            if (partnersContractFulfilled && opponentsContractFulfilled)
            {
                rule = 6;
                roundStrategy = AIStrategy.Under;
            }

            switch (spadesBrain.Players[turnData.PlayerPos].Level)
            {
                // level 10: strategy is always Over
                case 10:
                    roundStrategy = AIStrategy.Over;
                    break;
                // level 9 + biddingBrainV1: strategy is always Under 
                case 9 when spadesBrain.BiddingBrain.GetType() == typeof(BiddingBrainV1): 
                    roundStrategy = AIStrategy.Under;
                    break;
            }

            // // LoggerController.Instance.Log(// // LoggerController.Module.AI,"takes lef==" + takesLeft);
            // // LoggerController.Instance.Log(// // LoggerController.Module.AI,"rule==" + rule);
            // // LoggerController.Instance.Log(// // LoggerController.Module.AI,"strategy ==" + roundStrategy);

            turnData.PlayerRoundData.Strategies += roundStrategy == AIStrategy.Over ? "O" : "U";
            turnData.PlayerRoundData.Strategies += rule;

            return roundStrategy;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        public AIStrategy ChooseSoloStrategy(AITurnData turnData)
        {
            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;
            Position pos = turnData.PlayerPos;
            AIStrategy roundStrategy = AIStrategy.Over;
            int myBid = spadesBrain.Players[pos].Bid;
            int myTakes = spadesBrain.Players[pos].Takes;
            int myPoints = spadesBrain.MatchData.MatchPoints[pos];
            int myBags = spadesBrain.MatchData.MatchBags[pos];
            int counter = 0;

            int player1Points = 0;
            int player1Bags = 0;
            int player1Bid = 0;
            int player1Takes = 0;

            int player2Points = 0;
            int player2Bags = 0;
            int player2Bid = 0;
            int player2Takes = 0;

            int player3Points = 0;
            int player3Bags = 0;
            int player3Bid = 0;
            int player3Takes = 0;

            bool myContractFulfilled = false;
            bool player1ContractFulfilled = false;
            bool player2ContractFulfilled = false;
            bool player3ContractFulfilled = false;
            int myDistanceFromBagsPenalty = bagsToPenalty - myBags;  // bug fix: updated myBags before this line.

            int rule = 0;

            foreach (Position opponentPos in SpadesUtils.OpponentsPositions(pos, PlayingMode.Solo))
            {
                
                switch (counter)
                {
                    case 0:
                        player1Points = spadesBrain.MatchData.MatchPoints[opponentPos];
                        player1Bid = spadesBrain.Players[opponentPos].Bid;
                        player1Bags = spadesBrain.MatchData.MatchBags[opponentPos];
                        player1Takes = spadesBrain.Players[opponentPos].Takes;
                        break;

                    case 1:
                        player2Points = spadesBrain.MatchData.MatchPoints[opponentPos];
                        player2Bid = spadesBrain.Players[opponentPos].Bid;
                        player2Bags = spadesBrain.MatchData.MatchBags[opponentPos];
                        player2Takes = spadesBrain.Players[opponentPos].Takes;
                        break;

                    case 2:
                        player3Points = spadesBrain.MatchData.MatchPoints[opponentPos];
                        player3Bid = spadesBrain.Players[opponentPos].Bid;
                        player3Bags = spadesBrain.MatchData.MatchBags[opponentPos];
                        player3Takes = spadesBrain.Players[opponentPos].Takes;
                        break;
                }
                counter++;
            }

            int totalBids = myBid + player1Bid + player2Bid + player3Bid;
            int totalTakes = myTakes + player1Takes + player2Takes + player3Takes;
            int takesLeft = 13 - totalTakes;
            int myOverTakes = (myTakes - myBid > 0 ? myTakes - myBid : 0);
            int player1overTakes = (player1Takes - player1Bid > 0 ? player1Takes - player1Bid : 0);
            int player2overTakes = (player2Takes - player2Bid > 0 ? player2Takes - player2Bid : 0);
            int player3overTakes = (player3Takes - player3Bid > 0 ? player3Takes - player3Bid : 0);

            if (myTakes >= myBid && myBid!=0) myContractFulfilled = true;
            if (player1Takes >= player1Bid && player1Bid!=0) player1ContractFulfilled = true;
            if (player2Takes >= player2Bid && player2Bid!=0) player2ContractFulfilled = true;
            if (player3Takes >= player3Bid && player3Bid!=0) player3ContractFulfilled = true;

            int underOver = totalBids - 13 + myOverTakes + player1overTakes + player2overTakes + player3overTakes;
            turnData.BidDistance = underOver;

            if (underOver < -1 && !myContractFulfilled)
            {
                roundStrategy = AIStrategy.Under;
                rule = 1;
            }
            else if (underOver >= -1 && !myContractFulfilled)
            {
                roundStrategy = AIStrategy.Over;
                rule = 8;
            }

            if (myContractFulfilled && underOver<-1)
            {
                roundStrategy = AIStrategy.Under;
                rule = 7;
            }

            if (myContractFulfilled && myDistanceFromBagsPenalty <= 2)
            {
                rule = 2;
                roundStrategy = AIStrategy.Under;
            }

            if (underOver > 0 && !myContractFulfilled && player1ContractFulfilled && player2ContractFulfilled && player3ContractFulfilled)
            {
                rule = 3;
                roundStrategy = AIStrategy.Under;
            }
            if (myContractFulfilled && underOver < -2)
            {
                rule = 4;
                roundStrategy = AIStrategy.Under;
            }
            if (myContractFulfilled && underOver>=-1 && myDistanceFromBagsPenalty >= 2 && takesLeft >= 2)
            {
                rule = 5;
                roundStrategy = AIStrategy.Over;
            }
            if (myContractFulfilled && takesLeft == 1 && (!player1ContractFulfilled | !player2ContractFulfilled | !player3ContractFulfilled) && myDistanceFromBagsPenalty >= 2)
            {
                rule = 6;
                roundStrategy = AIStrategy.Over;
            }

            switch (spadesBrain.Players[turnData.PlayerPos].Level)
            {
                // level 10: strategy is always Over
                case 10:
                    roundStrategy = AIStrategy.Over;
                    break;
                // level 9 + biddingBrainV1: strategy is always Under 
                case 9 when spadesBrain.BiddingBrain.GetType() == typeof(BiddingBrainV1):
                    roundStrategy = AIStrategy.Under;
                    break;
            }

            // // LoggerController.Instance.Log(// // LoggerController.Module.AI,"takes left = " + takesLeft);
            // // LoggerController.Instance.Log(// // LoggerController.Module.AI,"rule = " + rule);
            // // LoggerController.Instance.Log(// // LoggerController.Module.AI,"strategy = " + roundStrategy);

            turnData.PlayerRoundData.Strategies += roundStrategy == AIStrategy.Over ? "O" : "U";
            turnData.PlayerRoundData.Strategies += rule;

            return roundStrategy;
        }
	}
}
