﻿using System.Collections.Generic;
using SpadesAI.model;
using CommonAI;
using SpadesAI.Internal;
using cardGames.models;

namespace SpadesAI.V1.Chain
{
	public class BreakOpponentNilHandlerV2:AbstractHandler
	{
        private const string STRATEGY_FIELD_NAME = "BON";

		public BreakOpponentNilHandlerV2 (SpadesBrainImpl spadesBrain):base(spadesBrain)
		{
		}

		public override void Handle (AITurnData turnData){

            // TODO: Do not break nil when:
            // 1. If we win this round we win the game + the opponents will not get points above us
            // 2. If the total bid is >=13, check the points if bids are met. If the opponents are not above us, do not break


            // Do not break nil if over
            if (turnData.Strategy==AIStrategy.Over) {
                nextHandler.Handle(turnData);
                return;
            }

            // Do not break nil if:
            // Tricks_left - (my_bid - my_takes) <= threshold
            int breakNilThreshold = 4;
            if (turnData.BidDistance <= -3)
                breakNilThreshold = 3; // For strong under, stop dealing with nil only at the last 3 tricks

            int tricksLeft = spadesBrain.Players[turnData.PlayerPos].Hand.Count;
            if (tricksLeft - (turnData.Bid - turnData.Takes)<=breakNilThreshold) {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            // Iterate every opponent who bid nil.
            // Look for one who can be broken.
            foreach (Position opponentNilPosition in turnData.OpponentNilPositions) {
				// Check if nil bidder already threw a card	
				if (spadesBrain.CardsOnTable.ContainsKey (opponentNilPosition)) {
					Card nilBidderCard = spadesBrain.CardsOnTable [opponentNilPosition];

					// Check if the nil bidder's card is the highest on the table
					if (SpadesUtils.FindWinningPositionOnTable (spadesBrain) == opponentNilPosition) {

                        if (turnData.CardsToUse.CountSuit(spadesBrain.LeadingSuit) > 0) {
                            if(nilBidderCard.Rank <= Card.RankType.Four && spadesBrain.CardsOnTable.Count < 3) {
                                // Do not try to break.

                                // There are 2 cases here:

                                // 1. The nill bidder is currently winning with 3 or 4.
                                //    Since these are very low cards which are not likely to remain as winners after other players throw cards,
                                //    do not waste a very low card here such as 2 or 3 and keep it for later use.

                                // 2. If the nil bidder is the leader and throwing 2, the nil bidder can never be broken, since the bot is not void.
                                continue;
                            } else {
                                Card strongestCardBelowRank = turnData.CardsToUse.CardsBySuits(nilBidderCard.Suit).GetStrongestBelowRank(nilBidderCard.Rank);
                                if (strongestCardBelowRank != null) {
                                    // Try to break.
                                    // Use the strongest losing card, since weaker cards might be required to break later.
                                    turnData.CardToPlay = strongestCardBelowRank;
									strategyRule = 1;
                                    // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} trying to break {1} nil with strongest losing card", turnData.PlayerPos, opponentNilPosition);
                                }
                            }
                        } else {
                            List<Card.SuitType> nonSpadeSuitsList = SuitUtils.GetOtherSuits(new List<Card.SuitType>{Card.SuitType.Spades, spadesBrain.LeadingSuit.Value});
                            CardsList nonSpadeCardsList = turnData.CardsToUse.CardsBySuits(nonSpadeSuitsList);
                            if (nonSpadeCardsList.Count > 0) {
                                // Try to break.
                                // Use a medium non spade card, since weaker cards might be required to break later.
                                turnData.CardToPlay = nonSpadeCardsList[nonSpadeCardsList.Count / 2];
								strategyRule = 2;
                                // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} trying to break {1} nil with medium void losing card", turnData.PlayerPos, opponentNilPosition);
                            }
                        }

					} else {
						// Nil bidder is already covered.
						continue;
					}
				} else {
					// Nil bidder has not played yet

					HashSet<Card.SuitType> nilBidderVoids = spadesBrain.Players [opponentNilPosition].VoidSuits;

					if (spadesBrain.CardsOnTable.Count == 0) {
                        // Remove cards that nil bidder is void of.
                        List<Card.SuitType> nilBidderNonVoidsList = SuitUtils.GetOtherSuits(nilBidderVoids);
                        CardsList nonVoidCardsList = turnData.CardsToUse.CardsBySuits(nilBidderNonVoidsList);
                        if (nonVoidCardsList.Count > 0) {
                            turnData.CardToPlay = nonVoidCardsList[nonVoidCardsList.Count - 1];
							strategyRule = 3;
                            // // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} trying to break {1} nil with weakest card", turnData.PlayerPos, opponentNilPosition);
                        }
                    } else {
						// Check if the nil bidder is void on leading suit.
						if (nilBidderVoids.Contains ((Card.SuitType) spadesBrain.LeadingSuit)) {
							continue;
						}

                        // TODO: 13.12.2017 - Many times changing the strategy is not enough, since for example the bot might cut with spades right after a low card is thrown by RHO.
                        // Change the strategy to under to affect the next handlers.
                        // This will cause high cards to be used as strongest losers, and low cards to be kept for later use.
                        turnData.Strategy = AIStrategy.Under;
                        continue;
                    }
				}
			}
			
			if (turnData.CardToPlay == null) {
				nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
			
		}
	}
}

