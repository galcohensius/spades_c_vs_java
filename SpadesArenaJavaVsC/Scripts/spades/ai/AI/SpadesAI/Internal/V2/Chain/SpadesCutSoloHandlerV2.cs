﻿using cardGames.models;
using CommonAI;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V2.Chain {
    public class SpadesCutSoloHandlerV2 : AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "SCS";

        public SpadesCutSoloHandlerV2(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {
            if (spadesBrain.MatchData.Mode != PlayingMode.Solo) {
                nextHandler.Handle(turnData);
                return;
            }

            // The player can cut with spades when:
            // 1. He is not the first to act.
            // 2. The leading suit is not spades.
            // 3. The player has a spade in the playable cards.
            if(spadesBrain.CardsOnTable.Count == 0 || spadesBrain.LeadingSuit == Card.SuitType.Spades || turnData.CardsToUse.CountSuit(Card.SuitType.Spades) == 0) {
                nextHandler.Handle(turnData);
                return;
            }

            // Go on if the player is last, since we know for sure which cards will win or lose, and other handlers will select the card
            // to play
            if(spadesBrain.CardsOnTable.Count == 3) {
                nextHandler.Handle(turnData);
                return;
            }

			int strategyRule = 0;


            if (turnData.Takes < turnData.Bid || turnData.Strategy==AIStrategy.Over) { 
                // Cut to win.
                // Use the weakest card which can win the trick with a good probability.
                AIStrengthCardsList nonLosingSpadeCards = turnData.CardsToUse.GetCardsWithinStrengthRange(0.0001f, 1);
                if(nonLosingSpadeCards.Count > 0) {
                    int leadingSuitCardsThrown = spadesBrain.ThrowHistory.CountSuit(spadesBrain.LeadingSuit);
					if (leadingSuitCardsThrown <= 8 && !SpadesUtils.IsAnyOpponentVoidOnSuit(turnData.PlayerPos,spadesBrain, (Card.SuitType) spadesBrain.LeadingSuit)) {
                        // Since there is a low probability for someone to cut above, use a small card.
                        turnData.CardToPlay = nonLosingSpadeCards.GetWeakCard();

						if (turnData.CardToPlay != null) {
							strategyRule = 1;
							// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} cutting with weak spade, 8 or less {1} cards used", turnData.PlayerPos, spadesBrain.LeadingSuit);
                        }
                    } else {
                        // Since there is a higher probability for someone to cut above, use a medium card.
                        turnData.CardToPlay = nonLosingSpadeCards[nonLosingSpadeCards.Count / 2];

						if (turnData.CardToPlay != null) {
							strategyRule = 2;
							// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} cutting with medium spade, more than 8 {1} cards used", turnData.PlayerPos, spadesBrain.LeadingSuit);
                        }
                    }
                }
            } else {
                // Cut to lose.
                // Get rid of spade cards which are sure losers.
                AIStrengthCardsList losingSpadeCards = new AIStrengthCardsList(turnData.CardsToUse.CardsBySuits(Card.SuitType.Spades)).GetCardsWithinStrengthRange(0, 0);

                if (losingSpadeCards.Count > 0) {
                    // TODO: Currently not implemented
                }
            }

            if (turnData.CardToPlay == null) {
                nextHandler.Handle(turnData);
            } else
            {
				turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}
