﻿using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V2.Chain {
    public class UnknownHandlerV2: AbstractHandler {

        private const string STRATEGY_FIELD_NAME = "UHH";

        public UnknownHandlerV2(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }

        public override void Handle(AITurnData turnData) {

			int strategyRule = 0;

            if (turnData.Takes >= turnData.Bid) {
				// We have all the cards
				if (turnData.Strategy == AIStrategy.Over)
				{
					// Dump a weak card, as stronger cards might be used later.
					turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 1;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} dumps a weak card", turnData.PlayerPos);
                    }
                } else {
                    // Dump a strongest card, we do not need it anymore
                    turnData.CardToPlay = turnData.CardsToUse.GetStrongCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 2;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} dumps a strongest card", turnData.PlayerPos);
                    }
                }
            } else {
                // We still need more cards
                if (turnData.Strategy == AIStrategy.Over)
                {
                    // Dump a weak card, as stronger cards might be used later.
                    turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();

					if (turnData.CardToPlay != null) {
						strategyRule = 3;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} dumps a weak card", turnData.PlayerPos);
                    }
                }
                else
                {
                    // Dump a medium card, as weaker cards might be used later.
                    // A stronger card might be required later, since the bot probably relied on it on the bidding stage.
                    turnData.CardToPlay = turnData.CardsToUse[turnData.CardsToUse.Count / 2];

					if (turnData.CardToPlay != null) {
						strategyRule = 4;
						// // LoggerController.Instance.LogFormat(// // LoggerController.Module.AI,"{0} dumps a medium card", turnData.PlayerPos);
                    }
                }
            }

           
            // Final test to make sure a card is selected
			if (turnData.CardToPlay==null) {
				strategyRule = 5;
				turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
            }

			turnData.PlayerRoundData.Strategies += STRATEGY_FIELD_NAME + strategyRule + turnData.CardToPlay + ",";
        }
    }
}