using System.Collections.Generic;
using SpadesAI.model;
using SpadesAI.V1;
using System;
using CommonAI;
using spades.models;
using common.utils;
using System.Linq;
using cardGames.models;

namespace SpadesAI.Internal.V2
{
    internal class BiddingBrainV2 : BiddingBrainV1
    {
        List<string[]> m_solo_nil_1_list;
        List<string[]> m_solo_nil_2_list;
        List<string[]> m_solo_nil_3_list;
        List<string[]> m_p_nil_side_1_list;
        List<string[]> m_p_nil_side_2_list;
        List<string[]> m_p_nil_side_3_list;
        List<string[]> m_p_nil_spades_1_list;
        List<string[]> m_p_nil_spades_2_list;
        List<string[]> m_p_nil_spades_3_list;

        internal BiddingBrainV2(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            Logger.Enabled = true;
            if (m_solo_nil_1_list == null) m_solo_nil_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\solo_nil_1");
            if (m_solo_nil_2_list == null) m_solo_nil_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\solo_nil_2");
            if (m_solo_nil_3_list == null) m_solo_nil_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\solo_nil_3");
            if (m_p_nil_side_1_list == null) m_p_nil_side_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_side_1");
            if (m_p_nil_side_2_list == null) m_p_nil_side_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_side_2");
            if (m_p_nil_side_3_list == null) m_p_nil_side_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_side_3_or_more");
            if (m_p_nil_spades_1_list == null) m_p_nil_spades_1_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_spades_1");
            if (m_p_nil_spades_2_list == null) m_p_nil_spades_2_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_spades_2");
            if (m_p_nil_spades_3_list == null) m_p_nil_spades_3_list = CSVReader.ReadFile<string>("C:\\Dev\\C_Vs_Java\\SpadesArenaJavaVsC\\Scripts\\spades\\ai\\AI\\SpadesAI\\botsData\\p_nil_spades_3");
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// Makes the bid for the specified position
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            curBid = 0;
            int biddingPosition = GetBiddingPosition(prData);
            string botType = GetBotType(pos, prData);
            CardsList hand = spadesBrain.Players[pos].Hand;
            float myFloatBid = BidPointsCount(hand);
            prData.InitialBid = myFloatBid;

            // get the probability of making Nil
            float nilChances = spadesBrain.MatchData.Mode == PlayingMode.Partners ? PartnersNilChances(hand) : SoloNilChances(hand);
            
            // Adjust bid according to botType and bids of the other players.
            curBid = spadesBrain.MatchData.Mode == PlayingMode.Partners ? partnersAdjustments(pos, biddingPosition, myFloatBid, botType, prData, hand, nilChances) : 
                                                                          soloAdjustments(pos, biddingPosition, myFloatBid, botType, prData, hand, nilChances);
            // Check Nil
            if (CheckNil(spadesBrain.MatchData.Mode, hand, prData, pos, biddingPosition, nilChances)){
                curBid = 0;
            }

            // Validate that the partners do not have a bid larger than 13
            PartnersBidsValidation(pos, biddingPosition);

            return (int)curBid;
        }

        /// /////////////////////////////////////////////////////////////////////////////////////////////////
        
        private int GetBiddingPosition(SpadesPlayerRoundData prData)
        {
            int biddingPosition = 1;
            // Get the prev bidding, a player who did not bid yet will have -1 as bid value
            foreach (Position p in Enum.GetValues(typeof(Position)))
            {
                if (spadesBrain.Players[p].Bid >= 0)
                {
                    biddingPosition++;
                }
            }

            prData.BiddingPosition = biddingPosition;
            return biddingPosition;
        }

        private int PlayersPointsIfBidMet(int player1Bid, int player2Bid, int playersPointsBeforeRound)
        {
            int playersPointsIfBidMet = 0;
            List<int> partnersBidsList = new List<int> { player1Bid, player2Bid };

            foreach (int b in partnersBidsList)
            {
                if (b > 0) playersPointsIfBidMet += 10 * b;
                else if (b == 0) playersPointsIfBidMet += 100;
                else if (b == -2) playersPointsIfBidMet += 200;
            }
            return playersPointsBeforeRound + playersPointsIfBidMet;
        }

        private String GetBotType(Position pos, SpadesPlayerRoundData prData)
        {
            String botType;
            // P is only use P6 anyway, we will use the level for other stuff
            if (prData.BotType == BrainType.ClassicV3)
                botType = "dynamic";

            else
            {
                switch (spadesBrain.Players[pos].Level)
                {
                    case 1:
                        botType = "veryConservative";
                        break;
                    case 2:
                        botType = "conservative";
                        break;
                    case 3:
                        botType = "middle";
                        break;
                    case 4:
                        botType = "aggressive";
                        break;
                    case 5:
                        botType = "veryAggressive";
                        break;
                    case 6:
                        botType = "dynamic";
                        break;
                    case 7:
                        botType = "dynamic2";
                        break;
                    case 10:
                        botType = "over";
                        break;
                    default:
                        botType = "dynamic";
                        break;
                }
            }

            return botType;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        private int soloAdjustments(Position pos, int biddingPosition, float myFloatBid, string botType, SpadesPlayerRoundData prData, CardsList hand, float chancesForNil)
        {
            string soloAdjustmentsString = "";
            int rule = 0;
            int underOver = 0;

            bool conservativeNilChance = false;
            bool middleNilChance = false;
            bool aggressiveNilChance = false;
            if (chancesForNil >= 0.35) { conservativeNilChance = true; }
            else if (chancesForNil >= 0.31) { middleNilChance = true; }
            else if (chancesForNil >= 0.27) { aggressiveNilChance = true; }
            
            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;
            int myDiffPointsFromMaxPoints = 0;
            int totalBidsBeforMyBid = 0;
            int totalBidsAfterMyInitialBid = 0;

            int counter = 0;

            int myInitialIntBid = 1;

            int player1Points = 0;
            int player1Bags = 0;
            int player1Bid = 0;
            int player1PointsIfBidMet = 0;

            int player2Points = 0;
            int player2Bags = 0;
            int player2Bid = 0;
            int player2PointsIfBidMet = 0;

            int player3Points = 0;
            int player3Bags = 0;
            int player3Bid = 0;
            int player3PointsIfBidMet = 0;

            int myPoints = 0;
            int myBags = 0;
            int myPointsIfBidMet = 0;
            bool youAreCloseToBagsPenalty = false;
            bool oneOfTheOpponentsIsCloseToBagsPenalty = false;
            int bagsToSoloPenalty = spadesBrain.MatchData.BagsToIncurPenalty;
            int maxPoints = spadesBrain.MatchData.WinConditionPoints;
            bool youAreAboutToWin = false;
            bool oneOfTheOpponentsIsAboutToWin = false;
            bool oneOfTheOpponentsBetNil = false;

            foreach (Position oppoPos in SpadesUtils.OpponentsPositions(pos, PlayingMode.Solo))
            {
                if (spadesBrain.Players[oppoPos].Bid > 0)
                    totalBidsBeforMyBid += spadesBrain.Players[oppoPos].Bid;

                switch (counter)
                {
                    case 0:
                        player1Points = spadesBrain.MatchData.MatchPoints[oppoPos];
                        player1Bid = spadesBrain.Players[oppoPos].Bid;
                        player1Bags = spadesBrain.MatchData.MatchBags[oppoPos];
                        break;

                    case 1:
                        player2Points = spadesBrain.MatchData.MatchPoints[oppoPos];
                        player2Bid = spadesBrain.Players[oppoPos].Bid;
                        player2Bags = spadesBrain.MatchData.MatchBags[oppoPos];
                        break;

                    case 2:
                        player3Points = spadesBrain.MatchData.MatchPoints[oppoPos];
                        player3Bid = spadesBrain.Players[oppoPos].Bid;
                        player3Bags = spadesBrain.MatchData.MatchBags[oppoPos];
                        break;
                }

                counter++;
            }

            myPoints = spadesBrain.MatchData.MatchPoints[pos];
            myBags = spadesBrain.MatchData.MatchBags[pos];

            if (bagsToSoloPenalty - myBags == 1)
                youAreCloseToBagsPenalty = true;

            if (bagsToSoloPenalty - player1Bags == 1 || bagsToSoloPenalty - player2Bags == 1 || bagsToSoloPenalty - player3Bags == 1)
            {
                oneOfTheOpponentsIsCloseToBagsPenalty = true;
            }


            myDiffPointsFromMaxPoints = spadesBrain.MatchData.WinConditionPoints - myPoints;

            if (player1Bid >= 0)
                player1PointsIfBidMet = player1Points + ((player1Bid > 0) ? player1Bid * 10 : 50);

            if (player2Bid >= 0)
                player2PointsIfBidMet = player2Points + ((player2Bid > 0) ? player2Bid * 10 : 50);

            if (player3Bid >= 0)
                player3PointsIfBidMet = player3Points + ((player3Bid > 0) ? player3Bid * 10 : 50);


            float totalBidWithMyFloatBid = (float)totalBidsBeforMyBid + myFloatBid;
            float roundDownFloatBid = (float)Math.Truncate(myFloatBid);
            float remainder = myFloatBid - roundDownFloatBid;
            myInitialIntBid = (int)roundDownFloatBid;
            myFloatBid = myFloatBid - 0.01F;

            switch (botType)
            {
                case "veryConservative":
                    myFloatBid -= 0.50F;
                    remainder = 0;
                    myInitialIntBid = (int)Math.Round(myFloatBid, MidpointRounding.AwayFromZero);
                    soloAdjustmentsString += "very conservative bot -0.50 ==" + myFloatBid;
                    break;

                case "veryAggressive":
                    myFloatBid += 0.50F;
                    remainder = 0;
                    myInitialIntBid = (int)Math.Round(myFloatBid, MidpointRounding.AwayFromZero);
                    soloAdjustmentsString += "veryAggressive bot +0.50 ==" + myFloatBid;
                    break;

                case "dynamic":

                    if (biddingPosition == 4 && totalBidWithMyFloatBid <= 12) { myFloatBid += 0.01F; }
                    else if (oneOfTheOpponentsIsAboutToWin) { myFloatBid += 0.01F; }
                    else if (myPoints < 0) { myFloatBid += 0.01F; }
                    else if (youAreCloseToBagsPenalty) { myFloatBid += 0.01F; }
                    remainder = 0;
                    myInitialIntBid = (int)Math.Round(myFloatBid, MidpointRounding.AwayFromZero);
                    soloAdjustmentsString += "dynamic bot ==" + myFloatBid;
                    break;
            }

            switch (remainder)
            {
                case 0.25f:
                    if (botType == "aggressive")
                    {
                        if (oneOfTheOpponentsIsAboutToWin || (biddingPosition == 4 && totalBidWithMyFloatBid <= 10.25))
                        {
                            myInitialIntBid++;
                            rule = 31;
                        }
                    }
                    break;

                case 0.50f:
                {
                    if ((botType == "aggressive" || botType == "middle") && totalBidWithMyFloatBid <= 13)
                    {
                        if (player1Points >= myPoints || player2Points >= myPoints || player3Points >= myPoints)
                        {
                            rule = 32;
                            myInitialIntBid++;
                        }
                    }
                    else if (youAreCloseToBagsPenalty)
                    {
                        rule = 33;
                        myInitialIntBid++;
                    }
                    break;
                }

                case 0.75f:
                {
                    myInitialIntBid++;
                    if (botType == "Conservative" && totalBidWithMyFloatBid >= 12)
                    {
                        rule = 34;
                        myInitialIntBid--;
                    }
                    else if (myPoints >= player1Points + 30 && myPoints >= player2Points + 30 && myPoints >= player3Points + 30 && !youAreCloseToBagsPenalty && totalBidWithMyFloatBid >= 12)
                    {
                        rule = 35;
                        myInitialIntBid--;
                    }
                }
                break;
            }



            prData.InitialBid = (float)myInitialIntBid;
            totalBidsAfterMyInitialBid = totalBidsBeforMyBid + myInitialIntBid;
            underOver = totalBidsBeforMyBid + myInitialIntBid - 13;
            myPointsIfBidMet = myPoints + ((myInitialIntBid > 0) ? myInitialIntBid * 10 : 50);

            if (myPointsIfBidMet >= maxPoints && myPointsIfBidMet > player1PointsIfBidMet && myPointsIfBidMet > player2PointsIfBidMet && myPointsIfBidMet > player3PointsIfBidMet)
                youAreAboutToWin = true;

            if (!youAreAboutToWin && (player1PointsIfBidMet >= maxPoints | player2PointsIfBidMet >= maxPoints | player3PointsIfBidMet >= maxPoints))
                oneOfTheOpponentsIsAboutToWin = true;

            if (player1Bid == 0 || player2Bid == 0 || player3Bid == 0) oneOfTheOpponentsBetNil = true;
       
            int myFinalBid = myInitialIntBid;

            if (myFinalBid == 0 && !conservativeNilChance)
            {
                rule = 36;
                myFinalBid = 1;
            }

            //rule 40 if you are about to win you can be conservative do -1 
            else if (myPointsIfBidMet - 10 >= maxPoints && myPointsIfBidMet - 10 > player1PointsIfBidMet && myPointsIfBidMet - 10 > player2PointsIfBidMet && myPointsIfBidMet - 10 > player3PointsIfBidMet && !youAreCloseToBagsPenalty && biddingPosition >= 3 && remainder < 0.75)
            {
                myFinalBid--;
                rule = 40;
            }

            //break the under-over == 0 to under or over
            else if (oneOfTheOpponentsIsAboutToWin && underOver == 0 && biddingPosition >= 3)
            {
                if (myFinalBid >= 4 && rule != 34 && rule != 35)
                {
                    myFinalBid--;
                    rule = 41;
                }
                else if (biddingPosition == 4 && myFinalBid == 1  && rule != 31 && remainder >= 0.50)
                {
                    myFinalBid++;
                    rule = 42;
                }
            }

            //big over -1
            if (biddingPosition >= 3 && totalBidsBeforMyBid + myFinalBid >= 15 && !oneOfTheOpponentsIsAboutToWin && myFinalBid > 3 && rule != 34 && rule != 35)
            {
                myFinalBid--;
                rule = 43;
                soloAdjustmentsString += "big over decrease bid -1 --" + myFinalBid;
            }
            // big under +1
            if (biddingPosition == 4 && totalBidsBeforMyBid + myFinalBid <= 9 && myFinalBid > 0 && myFinalBid <= 4 && rule != 31)
            {
                myFinalBid++;
                rule = 44;
                soloAdjustmentsString += "big under increase bid + 1 --" + myFinalBid;
            }

            if (myFinalBid <= -1) { myFinalBid = 1; }
            if (rule > 0) soloAdjustmentsString += "rule ==" + rule;

            soloAdjustmentsString += "my final bid ==" + myFinalBid;
            return myFinalBid;
        }



        /// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private int partnersAdjustments(Position pos, int biddingPosition, float myFloatBid, string botType, SpadesPlayerRoundData prData, CardsList hand, float chancesForNil)
        {
            string partnersAdjustmentsString = "";
            int rule = 0;
            int opponentsPointsBeforeRound = 0;
            int partnersPointsBeforeRound = 0;
            int opponentsPointsIfBidMet = 0;
            int partnersPointsIfBidMet = 0;
            int opponentsBags = 0;
            int partnersBags = 0;
            int diffBetweenOpponentsAndPartnersIfBidMet = 0;
            int myPartnerBid = 0;
            int opponentsTotalBid = 0;
            int opponent1Bid = 0;
            int opponent2Bid = 0;
            int underOver = 0;

            bool conservativeNilChance = false;
            bool middleNilChance = false;
            bool aggressiveNilChance = false;
            if (chancesForNil >= 0.30) { conservativeNilChance = true; }
            else if (chancesForNil >= 0.25) { middleNilChance = true; }
            else if (chancesForNil >= 0.20) { aggressiveNilChance = true; }
            
            bool myPartnerBetNil = false;
            bool myPartnerBetBlindNil = false;
            bool oneOfTheOpponentsBetNil = false;
            bool oneOfTheOpponentsBetBlindNil = false;
            bool opponentsAreCloseToBagPenalty = false;
            bool partnersAreCloseToBagPenalty = false;
            bool opponentsAreAboutToWin = false;
            bool partnersAreAboutToWin = false;
            int bagsToPenalty = spadesBrain.MatchData.BagsToIncurPenalty;
            int opponentsDiffPointsFromMaxPoints = 0;
            int partnersDiffPointsFromMaxPoints = 0;
            int totalBidsBeforeMyBid = 0;
            int totalBidsAfterMyInitialBid = 0;
            int counter = 0;
            int myFinalBid = 1;
            int myInitialIntBid = 1;

            foreach (Position opponentPos in SpadesUtils.OpponentsPositions(pos, PlayingMode.Partners))
            {
                opponentsPointsBeforeRound += spadesBrain.MatchData.MatchPoints[opponentPos];
                opponentsBags += spadesBrain.MatchData.MatchBags[opponentPos];
                if (spadesBrain.Players[opponentPos].Bid > 0)
                    opponentsTotalBid += spadesBrain.Players[opponentPos].Bid;
                if (counter == 0)
                {
                    opponent1Bid = spadesBrain.Players[opponentPos].Bid;
                    if (opponent1Bid == 0)
                    {
                        if (spadesBrain.Players[opponentPos].BlindNil)
                        {
                            oneOfTheOpponentsBetBlindNil = true;
                            opponent1Bid = -2;
                        }
                        else { oneOfTheOpponentsBetNil = true; }
                    }

                }
                else
                {
                    opponent2Bid = spadesBrain.Players[opponentPos].Bid;
                    if (opponent2Bid == 0)
                    {
                        if (spadesBrain.Players[opponentPos].BlindNil) {
                            oneOfTheOpponentsBetBlindNil = true;
                            opponent2Bid = -2;
                        }
                        else
                        {
                            oneOfTheOpponentsBetNil = true;
                        }
                    }
                }
                counter++;
            }


            Position partnerPos = SpadesUtils.PartnerPosition(pos);
            partnersPointsBeforeRound = spadesBrain.MatchData.MatchPoints[pos] + spadesBrain.MatchData.MatchPoints[partnerPos];
            partnersBags = spadesBrain.MatchData.MatchBags[pos] + spadesBrain.MatchData.MatchBags[partnerPos];
            myPartnerBid = spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid;

            if (myPartnerBid == 0)
            {
                if (spadesBrain.Players[partnerPos].BlindNil) { myPartnerBetBlindNil = true; }
                else { myPartnerBetNil = true; }
            }

            if (bagsToPenalty - opponentsBags == 1) { opponentsAreCloseToBagPenalty = true; }
            if (bagsToPenalty - partnersBags == 1) { partnersAreCloseToBagPenalty = true; }
            totalBidsBeforeMyBid = ((opponent1Bid < 0) ? 0 : opponent1Bid) + ((opponent2Bid < 0) ? 0 : opponent2Bid) + ((myPartnerBid < 0) ? 0 : myPartnerBid);
            opponentsPointsIfBidMet = PlayersPointsIfBidMet(opponent1Bid, opponent2Bid, opponentsPointsBeforeRound);
            opponentsDiffPointsFromMaxPoints = opponentsPointsIfBidMet - spadesBrain.MatchData.WinConditionPoints;

            float totalBidWithMyFloatBid = (float)totalBidsBeforeMyBid + myFloatBid;

            partnersAdjustmentsString += "my initial float bid ==" + myFloatBid;
            float roundDownFloatBid = (float)Math.Truncate(myFloatBid);
            float remainder = myFloatBid - roundDownFloatBid;
            myInitialIntBid = (int)roundDownFloatBid;

            myFloatBid = myFloatBid - 0.01F;

            switch (botType)
            {
                case "veryConservative":
                    myFloatBid -= 0.50F;
                    remainder = 0;
                    myInitialIntBid = (int)Math.Round(myFloatBid, MidpointRounding.AwayFromZero);
                    partnersAdjustmentsString += "very conservative bot -0.50 ==" + myFloatBid;
                    break;

                case "veryAggressive":
                    myFloatBid += 0.50F;
                    remainder = 0;
                    myInitialIntBid = (int)Math.Round(myFloatBid, MidpointRounding.AwayFromZero);
                    partnersAdjustmentsString += "veryAggressive bot +0.50 ==" + myFloatBid;
                    break;

                case "dynamic":
                    if (biddingPosition == 4 && totalBidWithMyFloatBid <= 12) { myFloatBid += 0.01F; }
                    else if (opponentsDiffPointsFromMaxPoints > 0) { myFloatBid += 0.01F; }
                    else if (opponentsPointsBeforeRound - partnersPointsBeforeRound > 30) { myFloatBid += 0.01F; }
                    else if (partnersAreCloseToBagPenalty) { myFloatBid += 0.01F; }

                    remainder = 0;
                    myInitialIntBid = (int)Math.Round(myFloatBid, MidpointRounding.AwayFromZero);
                    partnersAdjustmentsString += "dynamic bot ==" + myFloatBid;
                    break;
            }

            if (remainder == 0.25)
            {
                if (botType == "aggressive")
                {
                    if (opponentsAreAboutToWin || (biddingPosition == 4 && totalBidWithMyFloatBid <= 11.25))
                    {
                        rule = 1;
                        myInitialIntBid++;
                    }
                }
            }

            if (remainder == 0.50)
            {
                if (botType == "aggressive" && totalBidWithMyFloatBid <= 13)
                {
                    myInitialIntBid++;
                }
                else if (partnersAreCloseToBagPenalty || partnersPointsBeforeRound < 0)
                {
                    myInitialIntBid++;
                }
            }

            if (remainder == 0.75)
            {
                myInitialIntBid++;
                if (botType == "Conservative" && totalBidWithMyFloatBid >= 12)
                {
                    rule = 2;
                    myInitialIntBid--;
                }
                else if (partnersPointsBeforeRound >= opponentsPointsBeforeRound + 50 && !partnersAreCloseToBagPenalty && totalBidWithMyFloatBid >= 10)
                {
                    rule = 3;
                    myInitialIntBid--;
                }
            }

            if (myInitialIntBid <= 0) { myInitialIntBid = 1; }
            myFinalBid = myInitialIntBid;
            prData.NilValue = (float)myInitialIntBid;

            partnersPointsIfBidMet = PlayersPointsIfBidMet(myPartnerBid, myInitialIntBid, partnersPointsBeforeRound);
            diffBetweenOpponentsAndPartnersIfBidMet = opponentsPointsIfBidMet - partnersPointsIfBidMet;
            partnersDiffPointsFromMaxPoints = partnersPointsIfBidMet - spadesBrain.MatchData.WinConditionPoints;
            if (opponentsDiffPointsFromMaxPoints > 0 && diffBetweenOpponentsAndPartnersIfBidMet > 0) { opponentsAreAboutToWin = true; }
            if (partnersDiffPointsFromMaxPoints > 0 && diffBetweenOpponentsAndPartnersIfBidMet < 0) { partnersAreAboutToWin = true; }
            totalBidsAfterMyInitialBid = ((opponent1Bid < 0) ? 0 : opponent1Bid) + ((opponent2Bid < 0) ? 0 : opponent2Bid) + ((myPartnerBid < 0) ? 0 : myPartnerBid) + ((myInitialIntBid < 0) ? 0 : myInitialIntBid);
            underOver = totalBidsAfterMyInitialBid - 13;

            // if opponents are about to win and partners need one more take to win and the game is in under--- be aggressive 
            if (opponentsAreAboutToWin && diffBetweenOpponentsAndPartnersIfBidMet <= 10 && underOver <= 0 && biddingPosition == 4)
            {
                myFinalBid++;
                rule = 4;
            }

            //if opponentsAreAboutToWin diff is going to be more than 10 points and total bid is 13 try go under(if opponents are close to bags penalty) or over
            else if (opponentsAreAboutToWin && underOver == 0 && biddingPosition == 4)
            {
                if (opponentsAreCloseToBagPenalty)
                {
                    myFinalBid--;
                    rule = 5;
                }
            }
            // suicideMode && nilChance	Defeat the opponent by blind nil
            else if (opponentsAreAboutToWin &&  partnersPointsBeforeRound < 0 && opponentsPointsBeforeRound - partnersPointsBeforeRound >= 80 && biddingPosition >= 3 && myInitialIntBid <= 2 && !myPartnerBetNil && !myPartnerBetBlindNil && !oneOfTheOpponentsBetNil)
            {
                if (middleNilChance)
                {
                    myFinalBid = -2;
                    rule = 7;
                }
                else
                {
                    System.Random rand = new System.Random();
                    double blindNilPercent = 0.05;
                    if (rand.NextDouble() < blindNilPercent)
                    {
                        myFinalBid = -2;
                        rule = 7;
                    }
                }
            }

            else if (opponentsAreCloseToBagPenalty && underOver <= 1 && !partnersAreAboutToWin && biddingPosition == 4 && rule != 1)
            {
                myFinalBid++;
                rule = 8;
            }

            //partnersAreCloseToBagPenalty in under make +1
            else if (partnersAreCloseToBagPenalty && underOver <= -1 && biddingPosition == 4 && rule != 1)
            {
                myFinalBid++;
                rule = 9;
            }
            // bigDiffBetweenPartnersAndOpponents && underOver>0 && ! partnersAreCloseToBagPenalty - conservative
            else if (partnersPointsBeforeRound - opponentsPointsBeforeRound >= 100 && underOver >= 0 && !partnersAreCloseToBagPenalty && biddingPosition >= 3 && rule != 4 && rule != 5)
            {
                myFinalBid--;
                rule = 10;
            }

            //OpponentsNil prepare to take more if one of the opponents bet nil
            else if (oneOfTheOpponentsBetNil && underOver <= 14 && !partnersAreCloseToBagPenalty && biddingPosition == 4 && rule != 1)
            {
                myFinalBid++;
                rule = 11;
            }
            //help the niler
            else if (myPartnerBetNil | myPartnerBetBlindNil)
            {
                //avoid situations where the niler stand and you didn't take enough
                if (myInitialIntBid >= 5 && underOver >= 0 && !partnersAreCloseToBagPenalty && rule != 4 && rule != 5)
                {
                    myFinalBid--;
                    rule = 12;
                }

                //increase the bid to help the niler
                if (myInitialIntBid <= 4 && underOver <= -1 && biddingPosition >= 3 && rule != 1 && totalBidsAfterMyInitialBid < 13)
                {
                    myFinalBid++;
                    rule = 13;
                }
            }

            //if your situation is bad and total bid before you is above 11 go to blind nil
            else if ((myPartnerBetNil | myPartnerBetBlindNil) && biddingPosition==4 && totalBidsBeforeMyBid>=10 && opponentsPointsBeforeRound - partnersPointsBeforeRound >= 80 && middleNilChance)
            {
                myFinalBid = -2;
                rule = 14;
            }
            //be conservative if the bid is very high
            else if (myInitialIntBid >= 8 && !opponentsAreAboutToWin && !partnersAreAboutToWin && underOver >= -1 && !partnersAreCloseToBagPenalty && biddingPosition >= 3 && rule != 4 && rule != 5)
            {
                myFinalBid--;
                rule = 15;
            }

            if (rule > 40)
            {
                partnersAdjustmentsString += "opponentsPointsBeforeRound==" + opponentsPointsBeforeRound;
                partnersAdjustmentsString += "partnersPointsBeforeRound==" + partnersPointsBeforeRound;
                partnersAdjustmentsString += "opponentsPointsIfBidMet==" + opponentsPointsIfBidMet;
                partnersAdjustmentsString += "partnersPointsIfBidMet==" + partnersPointsIfBidMet;
                partnersAdjustmentsString += "opponentsBags==" + opponentsBags;
                partnersAdjustmentsString += "partnersBags==" + partnersBags;
                partnersAdjustmentsString += "diffBetweenOpponentsAndPartnersIfBidMet==" + diffBetweenOpponentsAndPartnersIfBidMet;
                partnersAdjustmentsString += "myPartnerBid==" + myPartnerBid;
                partnersAdjustmentsString += "myInitialIntBid==" + myInitialIntBid;
                partnersAdjustmentsString += "opponentsTotalBid==" + opponentsTotalBid;
                partnersAdjustmentsString += "opponent1Bid==" + opponent1Bid;
                partnersAdjustmentsString += "opponent2Bid==" + opponent2Bid;
                partnersAdjustmentsString += "myPartnerBetNil==" + myPartnerBetNil;
                partnersAdjustmentsString += "myPartnerBetBlindNil==" + myPartnerBetBlindNil;
                partnersAdjustmentsString += "oneOfTheOpponentsBetNil==" + oneOfTheOpponentsBetNil;
                partnersAdjustmentsString += "oneOfTheOpponentsBetBlindNil==" + oneOfTheOpponentsBetBlindNil;
                partnersAdjustmentsString += "opponentsAreCloseToBagPenalty==" + opponentsAreCloseToBagPenalty;
                partnersAdjustmentsString += "partnersAreCloseToBagPenalty==" + partnersAreCloseToBagPenalty;
                partnersAdjustmentsString += "opponentsAreAboutToWin==" + opponentsAreAboutToWin;
                partnersAdjustmentsString += "partnersAreAboutToWin==" + partnersAreAboutToWin;
                partnersAdjustmentsString += "gameBagsToPenalty==" + bagsToPenalty;
                partnersAdjustmentsString += "opponentsDiffPointsFromMaxPoints==" + opponentsDiffPointsFromMaxPoints;
                partnersAdjustmentsString += "partnersDiffPointsFromMaxPoints==" + partnersDiffPointsFromMaxPoints;
                partnersAdjustmentsString += "totalBidsBeforeMyBid==" + totalBidsBeforeMyBid;
                partnersAdjustmentsString += "totalBidsAfterMyInitialBid==" + totalBidsAfterMyInitialBid;
                partnersAdjustmentsString += "rule==" + rule;
            }


            if (myFinalBid == 0 && !conservativeNilChance)
            {
                rule = 16;
                myFinalBid = 1;
            }

            // big over -1
            if (biddingPosition >= 3 && totalBidsBeforeMyBid + myFinalBid >= 15 && myFinalBid >= 3 && rule != 4 && rule != 5)
            {
                myFinalBid--;
                rule = 20;
                partnersAdjustmentsString += "big over decrease bid -1 --" + myFinalBid;
            }

            //big under +1
            if (biddingPosition == 4 && totalBidsBeforeMyBid + myFinalBid <= 10 && myFinalBid > 0 && myFinalBid <= 4 && rule != 1)
            {
                myFinalBid++;
                rule = 21;
                partnersAdjustmentsString += "big under increase bid + 1 --" + myFinalBid;
            }


            if (myFinalBid == -1 | myFinalBid <= -3) myFinalBid = 1;

            if (rule > 0) partnersAdjustmentsString += "rule==" + rule;
            prData.NumericBid = rule;
            partnersAdjustmentsString += "myFinalBid==" + myFinalBid;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, partnersAdjustmentsString);
            return myFinalBid;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
        public static float BidPointsCount(CardsList cardsList)
        {
            float bid = 0;

            CardsList cards = null;

            int voids = 0, singletons = 0, doubletons = 0;
            float nonTrumpAceFactor = 0, nonTrumpKingFactor = 0, nonTrumpQueenFactor = 0, voidsFactor = 0, singletonFactor = 0, doubletonsFactor = 0, spadesCountFactor = 0, spadesCardsFactor = 0;
            float aceOfSpadesFactor = 0, kingOfSpadesFactor = 0, queenOfSpadesFactor = 0, jackOfSpadesFactor = 0;
            // Non trump counting
            Card.SuitType[] nonTrump = { Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts };

            foreach (Card.SuitType suit in nonTrump)
            {
                cards = cardsList.CardsBySuits(suit);

                // Ace
                if (cards.HasRank(Card.RankType.Ace))
                {
                    if (cards.Count < 7)
                        nonTrumpAceFactor = 1;
                    else
                        nonTrumpAceFactor = 0.5F;

                    bid += nonTrumpAceFactor;
                }

                // King
                if (cards.HasRank(Card.RankType.King))
                {
                    if (cards.Count == 1 || cards.Count == 5)
                        nonTrumpKingFactor = 0.25F;
                    else if (cards.Count < 5)
                        nonTrumpKingFactor = 0.75F;

                    bid += nonTrumpKingFactor;
                    if (cards.Count < 6 && cards.HasRank(Card.RankType.Ace) || cards.HasRank(Card.RankType.Queen) || cards.HasRank(Card.RankType.Jack))
                    {
                        nonTrumpKingFactor = 0.25F;
                        bid += nonTrumpKingFactor;
                    }
                }

                // Queen
                if (cards.HasRank(Card.RankType.Queen))
                {
                    if (cards.HasRank(Card.RankType.Ace) && cards.HasRank(Card.RankType.King) && cards.Count <= 4)
                        nonTrumpQueenFactor = 0.5F;
                    else if (cards.Count >= 3 && cards.Count <= 5)
                        nonTrumpQueenFactor = 0.25F;

                    bid += nonTrumpQueenFactor;
                }

                // Count short non-trump suits
                switch (cards.Count)
                {
                    case 0:
                        voids++;
                        break;
                    case 1:
                        singletons++;
                        break;
                    case 2:
                        doubletons++;
                        break;
                }
                nonTrumpKingFactor = 0; nonTrumpAceFactor = 0; nonTrumpQueenFactor = 0;
            }

            // Trump counting
            cards = cardsList.CardsBySuits(Card.SuitType.Spades);
            cards.SortHighToLowSuitFirst();
            int spadesCount = cards.Count;
            int spadesTakes = 0;

            if (cards.HasRank(Card.RankType.Ace))
            {
                aceOfSpadesFactor = 1;
                spadesCount--; spadesTakes++;
            }

            if (cards.HasRank(Card.RankType.King))
            {
                if (spadesTakes == 1)
                {
                    kingOfSpadesFactor = 1;
                    spadesCount--; spadesTakes++;
                }
                else if (cards.Count == 1)
                {
                    kingOfSpadesFactor = 0.25F;
                }
                else
                {
                    kingOfSpadesFactor = 1;
                    spadesCount -= 2; spadesTakes += 2;
                }
            }

            if (cards.HasRank(Card.RankType.Queen))
            {
                if (spadesTakes == 2)
                {
                    queenOfSpadesFactor = 1;
                    spadesCount--; spadesTakes++;
                }
                else if (spadesTakes == 1 && spadesCount >= 2)
                {
                    queenOfSpadesFactor = 1;
                    spadesCount -= 2; spadesTakes += 2;
                }

                else if (spadesCount == 2)
                {
                    queenOfSpadesFactor = 0.25F;
                    spadesCount -= 1; spadesTakes += 1;
                }
                else if (spadesCount == 3)
                {
                    queenOfSpadesFactor = 0.75F;
                    spadesCount -= 3; spadesTakes += 3;
                }
                else if (spadesCount > 3)
                {
                    queenOfSpadesFactor = 1;
                    spadesCount -= 3; spadesTakes += 3;
                }
            }

            if (cards.HasRank(Card.RankType.Jack))
            {
                if (spadesTakes == 3)
                {
                    jackOfSpadesFactor = 1;
                    spadesCount--; spadesTakes++;
                }
                else if (spadesTakes == 2 && spadesCount >= 2)
                {
                    jackOfSpadesFactor = 1;
                    spadesCount -= 2; spadesTakes += 2;
                }
                else if (spadesTakes == 1 && spadesCount >= 3)
                {
                    jackOfSpadesFactor = 1;
                    spadesCount -= 3; spadesTakes += 3;
                }
                else if (spadesCount == 3)
                {
                    jackOfSpadesFactor = 0.25F;
                    spadesCount -= 2; spadesTakes += 2;
                }
                else if (spadesCount == 4)
                {
                    jackOfSpadesFactor = 0.75F;
                    spadesCount -= 3; spadesTakes += 3;
                }

                else if (spadesCount > 4)
                {
                    jackOfSpadesFactor = 1;
                    spadesCount -= 3; spadesTakes += 3;
                }
            }

            // Handle voids and singletons
            if (voids == 1 && spadesCount > 1)
            {
                if (singletons > 0 | doubletons > 0)
                    voidsFactor = 2;
                else voidsFactor = 1.5F;
                spadesCount -= 2; spadesTakes += 2;
            }
            if (singletons > 0 && spadesCount > 0)
            {
                singletonFactor = 1;
                spadesCount--; spadesTakes++;
            }

            if (doubletons > 0 && spadesCount > 0)
            {
                doubletonsFactor = 0.5F;
                spadesCount--; spadesTakes++;
            }

            // left spades
            if (spadesTakes >= 4 && spadesCount > 0)
                spadesCountFactor = spadesCount;
            else if (spadesTakes == 3)
            {
                if (spadesCount == 1)
                    spadesCountFactor = 0.75F;
                if (spadesCount == 2)
                    spadesCountFactor = 1;
                if (spadesCount > 2)
                    spadesCountFactor = spadesCount - 1;
            }
            else if (spadesTakes == 2)
            {
                if (spadesCount == 1)
                    spadesCountFactor = 0.25F;
                if (spadesCount == 2)
                    spadesCountFactor = 0.75F;
                if (spadesCount == 3)
                    spadesCountFactor = 1;
                if (spadesCount > 3)
                    spadesCountFactor = spadesCount - 2;
            }

            else if (spadesTakes == 1)
            {
                if (spadesCount == 2)
                    spadesCountFactor = 0.25F;
                if (spadesCount == 3)
                    spadesCountFactor = 1;
                if (spadesCount > 3)
                    spadesCountFactor = spadesCount - 3;
            }

            else if (spadesTakes == 0)
            {
                if (spadesCount == 3)
                    spadesCountFactor = 0.25F;
                if (spadesCount == 4)
                    spadesCountFactor = 1;
                if (spadesCount > 4)
                    spadesCountFactor = spadesCount - 4;
            }

            bid = bid + aceOfSpadesFactor + kingOfSpadesFactor + queenOfSpadesFactor + jackOfSpadesFactor + voidsFactor + singletonFactor + doubletonsFactor + spadesCountFactor;
            if (bid >= 7 && spadesCountFactor >= 3)
            {
                bid = bid - 1;
            }

            if (bid < 1)
            {
                bid = 1;
            }
            else if (bid >= 10)
            {
                bid = 9;
            }
            return bid;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public bool CheckNil(PlayingMode mod, CardsList hand, SpadesPlayerRoundData prData, Position pos, int biddingPosition, float nilChances)
        {
            Random random = new Random();
            double noise = Math.Round(random.NextDouble()/2.5 - 0.2, 2); // [-0.2, 0.2]
            float nilChancesWithNoise = nilChances + (float)noise;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "Nil chance = " + nilChances + ", noise added to score = " + noise + "\nNil chances with noise = " + nilChancesWithNoise);

            // Solo
            if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                if (biddingPosition == 4 && CalcTotalBids() >= 13)
                {
                    if (nilChancesWithNoise >= 0.23 )
                    {
                        prData.NilValue = nilChances;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "bids are high (13+), Threshold decrease to = 0.23 " + nilChances);
                        return true;
                    }
                }
                else
                {
                    if (nilChancesWithNoise >= 0.3 )
                    {
                        prData.NilValue = nilChances;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "going for nil - nil chance ==" + nilChances);
                        return true;
                    }
                }
            }
            else
            { // Partners //
                {
                    if (biddingPosition >= 3)
                    {
                        if (spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid == 0 || spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid == -2)
                        { // if partner bid nil you must have a super-low hand to bid Nil
                            if (nilChancesWithNoise >= 0.9)
                            {
                                prData.NilValue = nilChances;
                                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "!!! going for Double Nil !!! - nil chance ==" + nilChances);
                                return true;
                            }
                        }
                        else
                        {  // partner did not bid nil
                            if (nilChancesWithNoise >= 0.25 - 0.02 * (spadesBrain.Players[SpadesUtils.PartnerPosition(pos)].Bid - 3))  // find value of parameter
                            {
                                if (!hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.King))
                                {
                                    prData.NilValue = nilChances;
                                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "partner did not bid nil, modify Nil Threshold according to partners bid - nil chance ==" + nilChances);
                                    return true;
                                }
                            }
                        }
                    }
                    else // partner did not bid yet
                    {
                        if (nilChancesWithNoise >= 0.25  && !hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.King))
                        {
                            prData.NilValue = nilChances;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "going for nil - nil chance ==" + nilChances);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        //////////////////////////////////////////////////////////////////////////////
        private float SoloNilChances(CardsList hand)
        {
            string SoloNilChancesString = "";
            Dictionary<Card.SuitType, float> nilChancePerSuit = new Dictionary<Card.SuitType, float>();

            Dictionary<int, float> s_nil_1 = m_solo_nil_1_list.ToDictionary(x => int.Parse(x[0]), x => float.Parse(x[1]));
            Dictionary<string, float> s_nil_2 = m_solo_nil_2_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> s_nil_3 = m_solo_nil_3_list.ToDictionary(x => x[0] + x[1] + x[2] + x[3], x => float.Parse(x[4]));

            Card.SuitType[] allSuits = { Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts, Card.SuitType.Spades };

            SoloNilChancesString +=  "========================== CHECK THE CHANCES FOR NIL" + "hand == " + hand;

            foreach (Card.SuitType suit in allSuits)
            {
                CardsList cards = hand.CardsBySuits(suit);
                cards.SortHighToLowRankFirst();
                SoloNilChancesString += string.Format("cards == " + cards);
                nilChancePerSuit.Add(suit, 0);
                List<int> cardsRanks = new List<int>();

                foreach (Card card in cards)
                    cardsRanks.Add((int)card.Rank);

                SoloNilChancesString +=  "cards ranks == " + FormatUtils.FormatList(cardsRanks);

                float value = 0;
                if (cardsRanks.Count == 1)
                {
                    SoloNilChancesString += string.Format("key  == " + cardsRanks[0]);
                    s_nil_1.TryGetValue(cardsRanks[0], out value);
                    SoloNilChancesString += string.Format("singleton card nil chance == " + value);
                    nilChancePerSuit[suit] = value;
                }
                else if (cardsRanks.Count == 2)
                {
                    String douletonCards = cardsRanks[1].ToString() + cardsRanks[0].ToString();
                    SoloNilChancesString += $"key  == {douletonCards}";
                    s_nil_2.TryGetValue(douletonCards, out value);
                    SoloNilChancesString += string.Format("doubleton cards nil chance == " + value);
                    nilChancePerSuit[suit] = value;

                }
                else if (cardsRanks.Count >= 3)
                {
                    String threePlusCards = cardsRanks[cardsRanks.Count - 1].ToString() + cardsRanks[cardsRanks.Count - 2].ToString() + cardsRanks[cardsRanks.Count - 3].ToString() + cardsRanks.Count.ToString();
                    SoloNilChancesString += $"key  == {threePlusCards}";
                    s_nil_3.TryGetValue(threePlusCards, out value);
                    SoloNilChancesString += string.Format("three plus cards nil chance == " + value);
                    nilChancePerSuit[suit] = value;
                }
                else
                {
                    value = 1.15f;
                    SoloNilChancesString += string.Format("void nil chance == " + value);
                    nilChancePerSuit[suit] = value;
                }

                if (suit == Card.SuitType.Spades &&
                    (cardsRanks.Count >= 4 || hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Ace) ||
                                                hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.King) ||
                                                hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Queen)))
                {
                    value = 0;
                    SoloNilChancesString += string.Format("spades problem (Ace/King/Queen or more than 4 cards)  == " + value);
                    nilChancePerSuit[suit] = value;
                }
                value = 0;
            }

            float nilChance = 1;

            foreach (KeyValuePair<Card.SuitType, float> kvp in nilChancePerSuit)
            {
                nilChance = nilChance * kvp.Value;
                SoloNilChancesString += $"Key = {kvp.Key}, Value = {kvp.Value}";
            }

            SoloNilChancesString +=  "nil chance == " + nilChance;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, SoloNilChancesString);
            return nilChance;
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private float PartnersNilChances(CardsList hand)
        {
            Dictionary<Card.SuitType, float> nilChancePerSuit = new Dictionary<Card.SuitType, float>();

            Dictionary<int, float> p_nil_side_1 = m_p_nil_side_1_list.ToDictionary(x => int.Parse(x[0]), x => float.Parse(x[1]));
            Dictionary<string, float> p_nil_side_2 = m_p_nil_side_2_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> p_nil_side_3 = m_p_nil_side_3_list.ToDictionary(x => x[0] + x[1] + x[2] + x[3], x => float.Parse(x[4]));
            Dictionary<int, float> p_nil_spades_1 = m_p_nil_spades_1_list.ToDictionary(x => int.Parse(x[0]), x => float.Parse(x[1]));
            Dictionary<string, float> p_nil_spades_2 = m_p_nil_spades_2_list.ToDictionary(x => x[0] + x[1], x => float.Parse(x[2]));
            Dictionary<string, float> p_nil_spades_3 = m_p_nil_spades_3_list.ToDictionary(x => x[0] + x[1] + x[2], x => float.Parse(x[3]));

            Card.SuitType[] allSuits = { Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts, Card.SuitType.Spades };
            string partnerNilChancesString = "";

            partnerNilChancesString +=  "CHECK THE CHANCES FOR PARTNERS NIL";

            foreach (Card.SuitType suit in allSuits)
            {
                CardsList cards = hand.CardsBySuits(suit);
                cards.SortHighToLowRankFirst();
                partnerNilChancesString +=  "cards == " + cards;
                nilChancePerSuit.Add(suit, 0);
                List<int> cardsRanks = new List<int>();

                foreach (Card card in cards)
                    cardsRanks.Add((int)card.Rank);

                partnerNilChancesString +=  "cards ranks == " + FormatUtils.FormatList(cardsRanks);

                float value = 0;
                if (cardsRanks.Count == 1)
                {
                    partnerNilChancesString += string.Format("key  == " + cardsRanks[0]);
                    if (suit == Card.SuitType.Spades)
                    {
                        p_nil_spades_1.TryGetValue(cardsRanks[0], out value);
                        partnerNilChancesString +=  "singleton card nil chance == " + value;
                        nilChancePerSuit[suit] = value;
                    }
                    else
                    {
                        p_nil_side_1.TryGetValue(cardsRanks[0], out value);
                        partnerNilChancesString +=  "singleton card nil chance == " + value;
                        nilChancePerSuit[suit] = value;
                    }

                }
                else if (cardsRanks.Count == 2)
                {
                    String keyDoubleton = cardsRanks[1].ToString() + cardsRanks[0].ToString();
                    partnerNilChancesString += $"key  == {keyDoubleton}";
                    if (suit == Card.SuitType.Spades)
                    {
                        p_nil_spades_2.TryGetValue(keyDoubleton, out value);
                        partnerNilChancesString +=  "doubleton cards nil chance == " + value;
                        nilChancePerSuit[suit] = value;
                    }
                    else
                    {
                        p_nil_side_2.TryGetValue(keyDoubleton, out value);
                        partnerNilChancesString +=  "doubleton cards nil chance == " + value;
                        nilChancePerSuit[suit] = value;
                    }
                }
                else if (cardsRanks.Count >= 3)
                {
                    String key3;
                    if (suit == Card.SuitType.Spades)
                    {
                        key3 = cardsRanks[cardsRanks.Count - 1].ToString() + cardsRanks[cardsRanks.Count - 2].ToString() + cardsRanks[cardsRanks.Count - 3].ToString();
                        partnerNilChancesString += $"key  == {key3}";
                        if (cardsRanks.Count >= 4)
                        {
                            value = 0;
                            partnerNilChancesString +=  "spades problem (more than 4 spades)  == " + value;
                            nilChancePerSuit[suit] = value;
                        }
                        else
                        {
                            p_nil_spades_3.TryGetValue(key3, out value);
                            partnerNilChancesString +=  "three plus cards nil chance == " + value;
                            nilChancePerSuit[suit] = value;
                        }
                    }
                    else // not spades
                    {
                        key3 = cards.Count.ToString() + cardsRanks[cardsRanks.Count - 1].ToString() + cardsRanks[cardsRanks.Count - 2].ToString() + cardsRanks[cardsRanks.Count - 3].ToString();// + cardsRanks[cardsRanks.Count - 4] ;
                        key3.ToList();
                        partnerNilChancesString += $"key  == {key3}";
                        p_nil_side_3.TryGetValue(key3, out value);
                        partnerNilChancesString +=  "three plus cards nil chance == " + value;
                        nilChancePerSuit[suit] = value;
                    }
                }
                else
                {
                    value = 1.25f;
                    partnerNilChancesString +=  "void nil chance == " + value;
                    nilChancePerSuit[suit] = value;
                }

                if (suit == Card.SuitType.Spades &&
                    (cardsRanks.Count >= 4 || hand.CardsBySuits(Card.SuitType.Spades).HasRank(Card.RankType.Ace) || nilChancePerSuit[Card.SuitType.Spades] <= 0.25))
                {
                    value = 0;
                    partnerNilChancesString += "spades problem (As or more than 4 spades)  == " + value;
                    nilChancePerSuit[suit] = value;
                }
                value = 0;
            }

            float nilChance = 1;

            foreach (KeyValuePair<Card.SuitType, float> kvp in nilChancePerSuit)
            {
                nilChance = nilChance * kvp.Value;
                partnerNilChancesString += $"Key = {kvp.Key}, Value = {kvp.Value}";
            }

            partnerNilChancesString += "nil chance == " + nilChance;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, partnerNilChancesString);
            return nilChance;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
