﻿using SpadesAI.V1;
using SpadesAI.V1.Chain;
using SpadesAI.V2.Chain;

namespace SpadesAI.V2 {
    public class PlayingBrainV2 : PlayingBrainV1
    {

        internal PlayingBrainV2(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            // Build the chain of responsibilities handlers
            handler = new PrepareAITurnDataHandlerV2(spadesBrain);
            handler
                .SetNextHandler(new ProtectSelfNilHandler(spadesBrain))
                .SetNextHandler(new ProtectPartnerNilHandler(spadesBrain))
                .SetNextHandler(new BreakOpponentNilHandler(spadesBrain))
                .SetNextHandler(new SpadesCutSoloHandlerV2(spadesBrain))
                .SetNextHandler(new SpadesCutPartnersHandler(spadesBrain))
                .SetNextHandler(new SureWinSoloHandlerV2(spadesBrain))
                .SetNextHandler(new SureWinPartnersHandler(spadesBrain))
                .SetNextHandler(new CreateVoidHandler(spadesBrain))
                .SetNextHandler(new SureLoseSoloHandlerV2(spadesBrain))
                .SetNextHandler(new UnknownHandlerV2(spadesBrain));
        }
    }

}