using System;
using cardGames.models;
using spades.models;
using SpadesAI.model;
using SpadesAI.V1;

namespace SpadesAI.Internal.V4
{
    internal class BiddingBrainMirror : BiddingBrainV1
    {

        internal BiddingBrainMirror(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            int biddingPosition = 1;
            foreach (Position p in Enum.GetValues(typeof(Position)))
            { if (spadesBrain.Players[p].Bid != -1) biddingPosition++; }
            prData.BiddingPosition = biddingPosition;

            // Bid = spades#
            return spadesBrain.Players[pos].Hand.CardsBySuits(Card.SuitType.Spades).Count;
        }
    }
}
