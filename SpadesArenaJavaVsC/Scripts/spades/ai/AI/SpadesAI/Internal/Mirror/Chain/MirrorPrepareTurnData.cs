﻿using System;
using SpadesAI.model;
using System.Collections.Generic;
using System.Linq;
using SpadesAI.V1.Chain;
using cardGames.models;

namespace SpadesAI.V4.Chain
{
    public class MirrorPrepareTurnData : PrepareAITurnDataHandler
    {

        public MirrorPrepareTurnData(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }
        public override void Handle(AITurnData turnData)
        {
            Dictionary<Position, int> spadesCountDic = new Dictionary<Position, int>();

            foreach (Position p in Enum.GetValues(typeof(Position)))
            {
                spadesCountDic[p] = spadesBrain.Players[p].Bid - spadesBrain.Players[p].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count;

                if (spadesCountDic[p] == 0) // void in spades
                {
                    spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Spades);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} Void in Spades ", p);
                }
                else if (spadesCountDic[p] == spadesBrain.Players[turnData.PlayerPos].Hand.Count) // void in all but spades
                {
                    spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Clubs);
                    spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Diamonds);
                    spadesBrain.Players[p].VoidSuits.Add(Card.SuitType.Hearts);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"{0} Void in all but Spades ", p);
                }
            }

            // Always move to next handler
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "round {4}, {5}, Prepare Turn successful, {0}, {1}, {2}, {3}, leading suit: {8}, cards on table: {9}, cards to use: {7}, cards to play: {6}", spadesBrain.PlayingBrain, spadesBrain.MatchData.Mode, spadesBrain.MatchData.Variation, spadesBrain.MatchData.SubVariation, spadesBrain.MatchData.RoundNum, turnData.PlayerPos, turnData.CardToPlay, turnData.CardsToUse.ToStringCardsOnly(), spadesBrain.LeadingSuit, string.Join(";", spadesBrain.CardsOnTable.Select(x => x.Value)));
            nextHandler.Handle(turnData);


        }
    }
}