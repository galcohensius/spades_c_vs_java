﻿using cardGames.models;
using SpadesAI.model;

namespace SpadesAI.Internal.UCT.Chain
{
    public class HighestCard : AbstractHandler
    {
        private const string StrategyFieldName = "Hgh";

        public HighestCard(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override void Handle(AITurnData turnData)
        {

            CardsList playableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[turnData.PlayerPos].Hand);
            playableCards.SortHighToLowRankFirstSpadesFirst();
            turnData.CardToPlay = playableCards[0];

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(Hgh) {0}, strongest playable card: {1}", turnData.PlayerPos, turnData.CardToPlay);

            turnData.PlayerRoundData.Strategies += StrategyFieldName + turnData.CardToPlay + ",";
        }
    }
}
