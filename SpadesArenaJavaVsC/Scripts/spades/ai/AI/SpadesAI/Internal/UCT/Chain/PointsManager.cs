﻿namespace Assets.Scripts.spades.ai.AI.SpadesAI.Internal.UCT.Chain {
    public class PointsManager {
        public const int NIL_POINTS_SOLO = 50;
        public const int NIL_POINTS_PARTNERS = 100;
        
        /// <summary>
        /// Calculates the points and bags for a solo game.
        /// </summary>
        /// <param name="bid"> The bid.</param>
        /// <param name="takes"> The takes.</param>
        /// <returns> A PointsAndBagsPair object.</returns>
        public static PointsAndBagsPair CalculatePointsAndBagsSolo(int bid, int takes) {
            PointsAndBagsPair pointsAndBagsPair = new PointsAndBagsPair();
            if (bid == 0 && takes == 0) {
                // Nil_NS met.
                pointsAndBagsPair.Points = NIL_POINTS_SOLO;
            } else if (bid == 0 && takes > 0) {
                // Nil_NS set.
                pointsAndBagsPair.Points = -NIL_POINTS_SOLO;
                pointsAndBagsPair.Bags = takes;
            } else if (takes < bid) {
                // bid set.
                pointsAndBagsPair.Points = -10*bid;
            } else {
                // takes >= bid
                pointsAndBagsPair.Points = 10*bid + (takes - bid);
                pointsAndBagsPair.Bags = takes - bid;
            }

            return pointsAndBagsPair;
        }
        
        /// <summary>
        /// Calculates the points and bags for a partners game.
        /// </summary>
        /// <param name="bid"> The bid.</param>
        /// <param name="takes"> The takes.</param>
        /// <param name="partnerBid"> The partner bid.</param>
        /// <param name="partnerTakes"> The partner takes.</param>
        /// <returns>  A PointsAndBagsPair object.</returns>
        public static PointsAndBagsPair CalculatePointsAndBagsPartners(int bid, int takes, int partnerBid, int partnerTakes) {
            PointsAndBagsPair pointsAndBagsPair = new PointsAndBagsPair();
            
            int groupBid = bid + partnerBid;
            int groupTakes = takes + partnerTakes;
            
            // If at least one partner bid nil, the bags are treated separately.
            if (bid == 0 && takes == 0) {
                // Nil_NS met.
                pointsAndBagsPair.Points += NIL_POINTS_PARTNERS;
            } else if (bid == 0 && takes > 0) {
                // Nil_NS set.
                pointsAndBagsPair.Points += -NIL_POINTS_PARTNERS;
                pointsAndBagsPair.Bags += takes;
                groupTakes -= takes;
            }
            
            // If at least one partner bid nil, the bags are treated separately.
            if (partnerBid == 0 && partnerTakes == 0) {
                // Nil_NS met.
                pointsAndBagsPair.Points += NIL_POINTS_PARTNERS;
            } else if (partnerBid == 0 && partnerTakes > 0) {
                // Nil_NS set.
                pointsAndBagsPair.Points += -NIL_POINTS_PARTNERS;
                pointsAndBagsPair.Bags += takes;
                groupTakes -= takes;
            }
            
            // no nil - Examine bids as a group.
            if (groupTakes < groupBid) {
                // bid set
                pointsAndBagsPair.Points -= 10*groupBid; 
            } else {
                // takes >= bid
                pointsAndBagsPair.Points += 10*groupBid + 1*(groupTakes - groupBid);
                pointsAndBagsPair.Bags += groupTakes - groupBid;
            }
            
            return pointsAndBagsPair;
        }


        public class PointsAndBagsPair {
            public int Points { get; set; }
            public int Bags { get; set; }
        }
    }
}