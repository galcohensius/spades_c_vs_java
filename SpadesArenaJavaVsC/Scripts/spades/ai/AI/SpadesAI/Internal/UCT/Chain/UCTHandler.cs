﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using cardGames.models;
using SpadesAI;
using SpadesAI.Internal;
using SpadesAI.model;
using SpadesAI.V1.Chain;
using SpadesAI.V4.Chain;
using SpadesBots.Simulation;
using PointsManager = Assets.Scripts.spades.ai.AI.SpadesAI.Internal.UCT.Chain.PointsManager;

namespace SpadesBots2.SpadesAI.Internal.UCT.Chain
{
    public class UctHandler : AbstractHandler
    {
        private const string StrategyFieldName = "UCT";
        private const int MaxIterations = 100;
        private const int UctStartAtHandSize = 3;

        public UctHandler(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        private static readonly Random Random = new Random();

        /// Loop until time limit
        ///  Deal
        ///     Choose card that maximize f(x) = x + C*√(ln⁡(n)/n_i) 
        ///     Loop over tricks:
        ///         For each player:
        ///             Create virtual brain & data
        ///             choose card to play
        ///             Update cards on table, cards in hands, leading suit, spades broken
        ///         update end of trick
        ///     update x_i (our points - opponents points)
        /// return the card that maximize average f(x)
        public override void Handle(AITurnData turnData)
        {
            Stopwatch uctSingleMoveStopwatch = Stopwatch.StartNew();
            
            if (spadesBrain.Players[turnData.PlayerPos].Hand.Count > UctStartAtHandSize)
            {
                nextHandler.Handle(turnData);
                return;
            }

            Position myPos = turnData.PlayerPos;
            Position leadingPositionWhenUctStart = AITurnData.LeadingPlayer;
            int misdeal = 0;

            // cards in the hands of the other three players
            CardsList cardsToDeal = GetCardsToDeal(turnData, spadesBrain.CardsOnTable);
            CardsList cardsToDealVirtual = new CardsList();

            // Average points per playable card (adjacent cards considered once)
            Dictionary<Card, List<int>> actionScoresPair = CreateCardListIntDict(turnData);
            Dictionary<Card, List<int>> actionWinRatioPair = CreateCardListIntDict(turnData);
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(UCT), " + myPos + " holds " + string.Join(",", spadesBrain.Players[myPos].Hand) +
//                         ".  Possible cards: " + string.Join(",", actionScoresPair.Keys));

            // Virtual brain and data
            SpadesBrainImpl virtualBrain = new SpadesBrainImpl();
            AITurnData virtualTurnData = new AITurnData();

            // Chain of handlers
            AbstractHandler virtualHandler = CreateVirtualHandler(virtualBrain);


            /////////////////////////////  Main loop  /////////////////////////////
            for (int n = 0; n < MaxIterations; n++)
            {
                // Copy the state of the game
                virtualBrain.CopyBrainImpl(spadesBrain);
                virtualTurnData.CopyAITurnData(turnData, leadingPositionWhenUctStart);

                // Deal
                cardsToDealVirtual.CopyCardsList(cardsToDeal);
                if (!DealUnplayedCardsSimple(cardsToDealVirtual, virtualBrain, myPos))
                { // misdeal
                    n--;
                    misdeal++;
                    if (misdeal == 50) 
                        break;
                    continue;
                }

                // choose the card that maximize f(x) 
                Card cardToTryNext = GetCardToTryNext(actionScoresPair, n);

                virtualBrain.SetMove(myPos, cardToTryNext); // Update cards on table and cards in hand
                virtualTurnData.PlayerPos = PositionUtils.GetNextPlayingPosition(myPos);


                // ------------ Loop over tricks: --------------------------------------------------------- //
                for (int tricksLeft = virtualBrain.Players[myPos].Hand.Count + 1; tricksLeft > 0; tricksLeft--)
                {
                    // Loop over players left to play:
                    string trickString = "(Virtual: " + virtualBrain.IsVirtual + ") trick " + (14 - tricksLeft) + ": ";
                    Position firstPositionToActOnNextTrick = virtualTurnData.PlayerPos;
                    if (virtualBrain.CardsOnTable.Count < 4)
                    {
                        foreach (Position position in PositionUtils.ConstructPlayingPositionsOrder(
                            firstPositionToActOnNextTrick, virtualBrain.CardsOnTable.Values.Count))
                        {
                            AITurnData.ResetAITurnData(virtualTurnData, position);
                            // Invoke handler & Update cards on table and cards in hand
                            virtualHandler.Handle(virtualTurnData);
                            Card card = virtualTurnData.CardToPlay;
                            if (card == null) throw new ArgumentException("BUG: virtualTurnData.CardToPlay = null ");
                            virtualBrain.SetMove(virtualTurnData.PlayerPos, card);

//                            Logger.Enabled = true;
                            trickString += position + " " + card + ", ";
                        }
                    }

                    // update end of trick
                    firstPositionToActOnNextTrick = SpadesUtils.FindWinningPositionOnTable(virtualBrain);
                    virtualBrain.TakeTrick(firstPositionToActOnNextTrick);
                    trickString += " --> " + firstPositionToActOnNextTrick + " takes trick ";
                    virtualTurnData.PlayerPos = firstPositionToActOnNextTrick;
                }

                // END OF ROUND:
                // update winingPosition and points gaps to x 
                Dictionary<Position, int> results = CalculatePointsUCT(virtualBrain);

                int pointsGap = myPos == Position.North || myPos == Position.South
                    ? results[Position.North] - results[Position.East]
                    : results[Position.East] - results[Position.North];
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(Virtual) Deal #" + n + " points gap: " + pointsGap);

                actionScoresPair[cardToTryNext].Add(pointsGap);

                UpdateWinRatioPair(results, virtualBrain, myPos, actionWinRatioPair, cardToTryNext);

                // anti bags bias: penalty per bag of -1
                foreach (Position position in new List<Position> {Position.East, Position.North})
                {
                    results[position] -= 2 * GetBags(virtualBrain, position);
                }

                // check if decision can be made before max iterations
                if ((n == actionScoresPair.Count * 5 || n == actionScoresPair.Count * 10) &&
                    IsPointsGapIsLargeOrZero(actionScoresPair))
                    break;
            }

            // argMax[Pr(win)], tie-breaker: E(points gap)
            Card bestCard = GetWinProbabilityMaximizerCard(actionScoresPair, actionWinRatioPair) ??
                            GetPointsMaximizerCard(actionScoresPair);  // tie-breaker


            // retrieve the LeadingPlayer that may been changed during simulations.
            AITurnData.LeadingPlayer = leadingPositionWhenUctStart;

            int timeToMove = unchecked((int) GetTime(turnData, uctSingleMoveStopwatch));


            if (bestCard != null)
            {
                turnData.CardToPlay = bestCard;
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(UCT) {0} plays {1}", turnData.PlayerPos, turnData.CardToPlay);
                turnData.PlayerRoundData.Strategies += StrategyFieldName + turnData.CardToPlay + ",";
            }
            else
            {
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(UCT) {0} could find a valid deal - next handler", turnData.PlayerPos);
                nextHandler.Handle(turnData);
            }
        }


        // ------------------------------------------------------------------------------------- //
        // ------------------------------------------------------------------------------------- //

        private void PrintRealHands()
        {
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "Real hands West holds: " + spadesBrain.Players[Position.West].Hand));
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "Real hands North holds: " + spadesBrain.Players[Position.North].Hand));
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "Real hands East holds: " + spadesBrain.Players[Position.East].Hand));
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "Real hands South holds: " + spadesBrain.Players[Position.South].Hand));
        }

        private static AbstractHandler CreateVirtualHandler(SpadesBrainImpl virtualBrain)
        {
            AbstractHandler virtualHandler = new PrepareAITurnDataHandlerV4(virtualBrain); // turn prepare
            virtualHandler.SetNextHandler(new SingleCard(virtualBrain)) // single playable card
                .SetNextHandler(new NilHandlerV4(virtualBrain)) // Nil (s+p)
                .SetNextHandler(new ProtectPartnerNilHandlerV4(virtualBrain)) // Cover Nil (p)
                .SetNextHandler(new BreakOpponentNilHandlerV4(virtualBrain)) // Set Nil (s+p)
                .SetNextHandler(new LeadHandlerV4(virtualBrain)) // Lead a card (s+p)
                .SetNextHandler(new SpadesCutPartnersHandlerV4(virtualBrain)) // Cut (p) 
                .SetNextHandler(new SureWinPartnersHandler(virtualBrain)) // Sure win (p)
                .SetNextHandler(new ThirdPosHighPartnersHandlerV4(virtualBrain)) // 3rd card high (p)
                .SetNextHandler(new UnknownHandlerV4(virtualBrain)); // default (s+p)
            return virtualHandler;
        }


        private int GetBags(SpadesBrainImpl virtualBrain, Position position)
        {
            // Calculate the change of points and bags for each partnership
            Position partnerPos = SpadesUtils.PartnerPosition(position);
            PointsManager.PointsAndBagsPair pointsAndBagsPair = PointsManager.CalculatePointsAndBagsPartners(
                virtualBrain.Players[position].Bid, virtualBrain.Players[position].Takes,
                virtualBrain.Players[partnerPos].Bid, virtualBrain.Players[partnerPos].Takes);

            return pointsAndBagsPair.Bags;
        }

        private long GetTime(AITurnData turnData, Stopwatch uctSingleMoveStopwatch)
        {
            uctSingleMoveStopwatch.Stop();
            long elapsedMilliseconds = uctSingleMoveStopwatch.ElapsedMilliseconds;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "elapsed[ms]: " + elapsedMilliseconds + ", hand count: " + spadesBrain.Players[turnData.PlayerPos].Hand.Count);
            return elapsedMilliseconds;
        }


        /// <summary>
        /// stop before max-iteration if points gap is very large or 0
        /// </summary>
        /// <param name="actionScoresPair"></param>
        /// <returns></returns>
        private bool IsPointsGapIsLargeOrZero(Dictionary<Card, List<int>> actionScoresPair)
        {
            double bestAverage = -1000;
            double secondAverage = -1000;
            Card bestCard = null;

            foreach (KeyValuePair<Card, List<int>> cardPointsPair in actionScoresPair)
            {
                if (cardPointsPair.Value.Count > 0 && cardPointsPair.Value.Average() > bestAverage)
                {
                    bestAverage = cardPointsPair.Value.Average();
                    bestCard = cardPointsPair.Key;
                }
            }

            foreach (KeyValuePair<Card, List<int>> cardPointsPair in actionScoresPair)
            {
                if (cardPointsPair.Key == bestCard) continue;
                if (cardPointsPair.Value.Count > 0 && cardPointsPair.Value.Average() > secondAverage)
                {
                    secondAverage = cardPointsPair.Value.Average();
                }
            }

            // is points gap is close to 0 || super big?
            return bestAverage - secondAverage < 0.5 || bestAverage - secondAverage > 50;
        }


        // replaced by GetWinProbabilityMaximizerCard,  leave it as an alternative (maybe for the coins for points sub-variant)
        private static Card GetPointsMaximizerCard(Dictionary<Card, List<int>> actionScoresPair)
        {
            double bestAverage = -1000;
            Card bestCard = null;

            foreach (KeyValuePair<Card, List<int>> cardPointsPair in actionScoresPair)
            {
                if (cardPointsPair.Value.Count > 0)
                {
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"playing " + cardPointsPair.Key + " will make " + Math.Round(cardPointsPair.Value.Average(), 1) + " points");
                    if (cardPointsPair.Value.Average() > bestAverage)
                    {
                        bestAverage = cardPointsPair.Value.Average();
                        bestCard = cardPointsPair.Key;
                    }
                }
            }

            return bestCard;
        }


        private static Card GetWinProbabilityMaximizerCard(Dictionary<Card, List<int>> actionScoresPair,
            Dictionary<Card, List<int>> actionWinRatePair)
        {
            double bestWinRate = -1000;
            Card bestCard = null;

            foreach (KeyValuePair<Card, List<int>> cardWinRatePair in actionWinRatePair)
            {
                if (cardWinRatePair.Value.Count > 0)
                {
                    float ratio = cardWinRatePair.Value.Sum() / (float)cardWinRatePair.Value.Count;
                    if (ratio > bestWinRate)
                    {
                        bestWinRate = cardWinRatePair.Value.Average();
                        bestCard = cardWinRatePair.Key;
                    }
                    else if (Math.Abs(ratio - bestWinRate) < 0.01 && 
                             (bestCard == null || actionScoresPair[cardWinRatePair.Key].Average() > actionScoresPair[bestCard].Average()))
                    {
                        // same win rate with better E[points]
                        bestWinRate = cardWinRatePair.Value.Average();
                        bestCard = cardWinRatePair.Key;
                    }
                }
            }
            return bestCard;
        }


        private static void UpdateWinRatioPair(Dictionary<Position, int> results, SpadesBrainImpl virtualBrain,
            Position myPos,
            Dictionary<Card, List<int>> actionWinRatioPair, Card cardToTryNext)
        {
            Position? winingPosition = CheckIfSomeOneWon(results, virtualBrain);
            if (winingPosition != null)
            {
                if (winingPosition == myPos || winingPosition == SpadesUtils.PartnerPosition(myPos)){
                    actionWinRatioPair[cardToTryNext].Add(1);
                }
                else{
                    actionWinRatioPair[cardToTryNext].Add(-1);
                }
            }
        }


        private Card GetCardToTryNext(Dictionary<Card, List<int>> actionScoresPair, int n)
        {
            // return a card we didn't play even once
            if (actionScoresPair.Count(kvp => kvp.Value.Count < 1) > 0)
            {
                return actionScoresPair.Where(pair => pair.Value.Count < 1).Select(pair => pair.Key).First();
            }

            // all cards been played at least once (3), return the card that maximize f() = x + C* √( ln⁡(n)/n_i ) 
            double maxAveragePoints = -1000.00;
            Card resultCard = actionScoresPair.Keys.First();
            foreach (KeyValuePair<Card, List<int>> keyValuePair in actionScoresPair)
            {
                // Choose card that maximize f(x) = x + C*√(ln⁡(n)/n_i) 
                double x_i = keyValuePair.Value.Average() +
                             100 * Math.Sqrt(2.0f * Math.Log(n) / keyValuePair.Value.Count);
                if (x_i > maxAveragePoints)
                {
                    maxAveragePoints = x_i;
                    resultCard = keyValuePair.Key;
                }
            }

            return resultCard;
        }


        private CardsList GetCardsToDeal(AITurnData virtualTurnData, Dictionary<Position, Card> cardsOnTheTable)
        {
            CardsList cardsToDeal = SpadesUtils.GetUnplayedDeck(spadesBrain);
            cardsToDeal.RemoveSomeCards(spadesBrain.Players[virtualTurnData.PlayerPos].Hand);
            RemoveSomeCards(cardsToDeal, cardsOnTheTable);
            cardsToDeal.Shuffle();
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"cards to deal: " + cardsToDeal);
            return cardsToDeal;
        }

        /// <summary>
        /// Removes cards form this list that exist in the specified list
        /// </summary>
        public void RemoveSomeCards(CardsList cardsList, Dictionary<Position, Card> cardsToRemove)
        {
            foreach (Card card in cardsToRemove.Values)
            {
                cardsList.Remove(card);
            }
        }


        private Dictionary<Card, List<int>> CreateCardListIntDict(AITurnData turnData)
        {
            CardsList playableCards = SpadesUtils.GetPlayableCardsRemoveAdjacent(spadesBrain.LeadingSuit,
                spadesBrain.SpadesBroken, spadesBrain.Players[turnData.PlayerPos].Hand, spadesBrain);
            Dictionary<Card, List<int>> pointsGapPerCard = new Dictionary<Card, List<int>>();
            foreach (Card playableCard in playableCards)
            {
                pointsGapPerCard.Add(playableCard, new List<int>());
            }

            return pointsGapPerCard;
        }


        // return false if misdeal
        /// 1. *Uniformly*, sample a deal from unplayed cards (consider Voids, needTODO: instead of uniform deals, consider bids)
        /// deal unplayed cards:
        // for each card see what positions he can be found,
        // then order card from low number of possible position to high (1-3) place each card uniformly into possible position.
        private bool DealUnplayedCardsSimple(CardsList unplayedCards, SpadesBrainImpl virtualBrain, Position myPos)
        {
            EmptyVirtualHandsOfOtherPlayers(myPos, virtualBrain);

            Dictionary<Position, int> positionsNeedCardsDic = GetNumberOfCardsNeededPerPositions(myPos, virtualBrain);
            Dictionary<Card, List<Position>> positionsPerCard =
                GetPositionsPerCard(unplayedCards, virtualBrain, myPos, positionsNeedCardsDic);
            if (positionsPerCard == null)
                return false;
            List<Card> unplayedCardsOrderedByPossibilities = SortByPossibilities(unplayedCards, positionsPerCard);

            try
            {
                // print deal:
                for (int i = 0; i < unplayedCards.Count; i++)
                {
                    if (unplayedCardsOrderedByPossibilities.Count == 0)
                    {
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"Misdeal (DealUnplayedCardsSimple)");
                        return false;
                    }

                    Card nextCard = unplayedCardsOrderedByPossibilities.First();
                    int nextPos = Random.Next(positionsPerCard[nextCard].Count);
                    Position posGotCard = positionsPerCard[nextCard][nextPos];

                    virtualBrain.Players[posGotCard].Hand.Add(nextCard);

                    positionsNeedCardsDic[posGotCard] -= 1;
                    positionsPerCard.Remove(nextCard);

                    if (positionsNeedCardsDic[posGotCard] == 0)
                    {
                        foreach (KeyValuePair<Card, List<Position>> cardListPositionPair in positionsPerCard)
                        {
                            cardListPositionPair.Value.Remove(posGotCard);
                        }
                    }

                    unplayedCardsOrderedByPossibilities.Remove(nextCard);
                    unplayedCardsOrderedByPossibilities =
                        SortByPossibilities(new CardsList(unplayedCardsOrderedByPossibilities), positionsPerCard);
                }
            }
            catch (Exception)
            {
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"Misdeal - re-deal hand");
                return false;
            }

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "West holds: " + virtualBrain.Players[Position.West].Hand));
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "North holds: " + virtualBrain.Players[Position.North].Hand));
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "East holds: " + virtualBrain.Players[Position.East].Hand));
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,string.Join(",", "South holds: " + virtualBrain.Players[Position.South].Hand));
            return true;
        }


        private List<Card> SortByPossibilities(CardsList unplayedCards, Dictionary<Card, List<Position>> positionsPerCard)
        {
            // TODO: should be an Array, remove the 1,2,3  || List with ensure capacity(300) , 1-100 1 possibilities, 101-200: 2 possibilities, 201:300 3 possibilities.
            CardsList result = new CardsList();
            CardsList onePossibilities = new CardsList();
            CardsList twoPossibilities = new CardsList();
            CardsList threePossibilities = new CardsList();

            foreach (Card card in unplayedCards)
            {
                if (positionsPerCard.ContainsKey(card))
                {
                    switch (positionsPerCard[card].Count)
                    {
                        case 1:
                            onePossibilities.Add(card);
                            break;
                        case 2:
                            twoPossibilities.Add(card);
                            break;
                        case 3:
                            threePossibilities.Add(card);
                            break;
                    }
                }
                else
                {
                    return null;
                }
            }

            result.AddRange(onePossibilities);
            result.AddRange(twoPossibilities);
            result.AddRange(threePossibilities);
            return result;
        }


        private Dictionary<Card, List<Position>> GetPositionsPerCard(CardsList unplayedCards,
            SpadesBrainImpl virtualBrain, Position myPos, Dictionary<Position, int> positionsNeedCardsDic)
        {
            Dictionary<Card, List<Position>> positionsPerCard = new Dictionary<Card, List<Position>>();
            foreach (Card card in unplayedCards)
            {
                positionsPerCard[card] = new List<Position>();
            }

            foreach (Position pos in Enum.GetValues((typeof(Position))))
            {
                if (pos == myPos || positionsNeedCardsDic[pos] == 0) continue;
                foreach (Card card in unplayedCards)
                {
                    if (!virtualBrain.Players[pos].VoidSuits.Contains(card.Suit))
                    {
                        positionsPerCard[card].Add(pos);
                    }
                }
            }

            foreach (Card card in unplayedCards)
            {
                if (positionsPerCard[card].Count == 0)
                    return null;
            }

            return positionsPerCard;
        }


        private Dictionary<Position, int> GetNumberOfCardsNeededPerPositions(Position myPos, SpadesBrainImpl brain)
        {
            Dictionary<Position, int> numberOfCardsNeededPerPositions = new Dictionary<Position, int>();
            foreach (Position pos in Enum.GetValues((typeof(Position))))
            {
                if (pos == myPos) continue;
                {
                    int cardOnTable = brain.CardsOnTable.ContainsKey(pos) ? 1 : 0;
                    numberOfCardsNeededPerPositions[pos] = 13 - brain.Players[pos].ThrowHistory.Count - cardOnTable -
                                                           brain.Players[pos].Hand.Count;
                }
            }

            return numberOfCardsNeededPerPositions;
        }


        /// <summary>
        /// Calculate the change of points and bags.
        /// </summary>
        private static Dictionary<Position, int> CalculatePointsUCT(SpadesBrainImpl Brain)
        {
            Dictionary<Position, int> results = new Dictionary<Position, int>();
            foreach (Position position in new List<Position> {Position.East, Position.North})
            {
                // Calculate the change of points and bags for each partnership
                Position partnerPos = SpadesUtils.PartnerPosition(position);
                PointsManager.PointsAndBagsPair pointsAndBagsPair = PointsManager.CalculatePointsAndBagsPartners(
                    Brain.Players[position].Bid, Brain.Players[position].Takes,
                    Brain.Players[partnerPos].Bid, Brain.Players[partnerPos].Takes);

                // Update total points and bags.
                results[position] = Brain.MatchData.MatchPoints[position] + pointsAndBagsPair.Points;
                int myBags = Brain.MatchData.MatchBags[position] + pointsAndBagsPair.Bags;

                // bags penalty.
                if (myBags >= Brain.MatchData.BagsToIncurPenalty)
                {
                    results[position] -= Brain.MatchData.BagsPenalty;
                }
            }
            return results;
        }


        private static Position? CheckIfSomeOneWon(Dictionary<Position, int> results, SpadesBrainImpl brain)
        {
            if ((results[Position.North] >= brain.MatchData.WinConditionPoints &&
                 results[Position.North] > results[Position.East]) ||
                (results[Position.East] <= brain.MatchData.LoseConditionPoints &&
                 results[Position.North] > results[Position.East]))
            {
                return Position.North;
            }

            if ((results[Position.East] >= brain.MatchData.WinConditionPoints &&
                 results[Position.East] > results[Position.North]) ||
                (results[Position.North] <= brain.MatchData.LoseConditionPoints &&
                 results[Position.East] > results[Position.North]))
            {
                return Position.East;
            }

            return null;
        }


        private static void EmptyVirtualHandsOfOtherPlayers(Position myPos, SpadesBrainImpl virtualBrain)
        {
            foreach (Position p in Enum.GetValues((typeof(Position))))
            {
                if (p == myPos) continue;

                CardsList emptyHand = new CardsList();
                virtualBrain.SetHand(p, emptyHand);
            }
        }
    }
}