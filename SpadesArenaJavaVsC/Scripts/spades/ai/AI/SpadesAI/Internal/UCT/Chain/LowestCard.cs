﻿using cardGames.models;
using SpadesAI.model;

namespace SpadesAI.Internal.UCT.Chain
{
    public class LowestCard : AbstractHandler
    {
        private const string StrategyFieldName = "Low";

        public LowestCard(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }

        public override void Handle(AITurnData turnData)
        {
            CardsList playableCards = SpadesUtils.GetPlayableCards(spadesBrain.LeadingSuit, spadesBrain.SpadesBroken, spadesBrain.Players[turnData.PlayerPos].Hand);
            playableCards.SortHighToLowRankFirstSpadesFirst();
            turnData.CardToPlay = playableCards[playableCards.Count-1];

             // LoggerController.Instance.LogFormat(// LoggerController.Module.AI, "(Low) {0}, weakest playable card", turnData.PlayerPos);

            turnData.PlayerRoundData.Strategies += StrategyFieldName + turnData.CardToPlay + ",";
        }
    }
}
