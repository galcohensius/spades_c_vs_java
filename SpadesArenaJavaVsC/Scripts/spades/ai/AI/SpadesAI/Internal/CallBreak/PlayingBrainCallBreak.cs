﻿using SpadesAI.V4.Chain;

namespace SpadesAI.V4
{
    public class PlayingBrainCallBreak : PlayingBrainV4
    {
        internal PlayingBrainCallBreak(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            // Build the chain of responsibilities handlers
            handler               = new PrepareAITurnDataHandlerV4(spadesBrain);    //turn prepare
            handler .SetNextHandler(new LeadHandlerCallBreak(spadesBrain))          //Lead a card (CallBreak) **
                    .SetNextHandler(new SpadesCutSoloHandlerV4(spadesBrain))        //Cut (s)
                    .SetNextHandler(new SureWinSoloHandlerV4(spadesBrain))          //Sure win (s)
                    .SetNextHandler(new CantWinDuckLowCallBreak(spadesBrain))       //Sure lose (CallBreak) **
                    .SetNextHandler(new UnknownHandlerV4(spadesBrain));             //default (s+p)
        }
    }
}
