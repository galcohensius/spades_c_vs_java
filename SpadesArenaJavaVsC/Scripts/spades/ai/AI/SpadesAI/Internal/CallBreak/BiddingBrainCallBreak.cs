using System;
using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using common.utils;
using CommonAI;
using spades.models;
using SpadesAI.model;

namespace SpadesAI.Internal.V4
{
    internal class BiddingBrainCallBreak : BiddingBrainV4
    {
        protected static List<string[]> m_high_cards_bid_value_list;
        protected string deck;

        public BiddingBrainCallBreak(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
            Logger.Enabled = true;
            deck = "regular";
            if (m_high_cards_bid_value_list == null) m_high_cards_bid_value_list = CSVReader.ReadFile<string>("botsData/high_cards_bid_value");    
        }

        protected float GetProbTableValue(Card.RankType rank, int cardsNum, int numberOfPlayersThatMightCutYou)
        {
            Dictionary<string, float> highCardsBidValue = m_high_cards_bid_value_list.ToDictionary(x => x[0] + x[1] + x[2], x => float.Parse(x[3]));

            float value;
            string lookForKey = numberOfPlayersThatMightCutYou.ToString() + cardsNum.ToString() + rank.ToString()[0];
            highCardsBidValue.TryGetValue(lookForKey, out value);
            return value;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// set bid based on hand and other bids
        public override int MakeBid(Position pos, SpadesPlayerRoundData prData)
        {
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI," =================== position: {0} =================== ", pos);
            Position prt = SpadesUtils.PartnerPosition(pos);
            Position lho = SpadesUtils.LhoPosition(pos);
            Position rho = SpadesUtils.RhoPosition(pos);
            int biddingPosition = 1;
            foreach (Position p in Enum.GetValues(typeof(Position)))
            {
                if (spadesBrain.Players[p].Bid != -1)
                {
                    biddingPosition++;
                }
            }
            prData.BiddingPosition = biddingPosition;  

            // Solo
            int lhoPointsIfBetMet = spadesBrain.MatchData.MatchPoints[lho];
            int rhoPointsIfBetMet = spadesBrain.MatchData.MatchPoints[rho];
            int prtPointsIfBetMet = spadesBrain.MatchData.MatchPoints[prt];
            if (biddingPosition > 1)
            {
                rhoPointsIfBetMet += SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[rho].Bid, spadesBrain.Players[rho].BlindNil);
            }
            if (biddingPosition > 2)
            {
                prtPointsIfBetMet += SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[prt].Bid, spadesBrain.Players[prt].BlindNil);
            }
            if (biddingPosition > 3)
            {
                lhoPointsIfBetMet += SpadesUtils.PointsIfSuccessBid(spadesBrain.MatchData.Mode, spadesBrain.Players[lho].Bid, spadesBrain.Players[lho].BlindNil);
            }

            int lhoBags = spadesBrain.MatchData.MatchBags[lho];
            int rhoBags = spadesBrain.MatchData.MatchBags[rho];
            int prtBags = spadesBrain.MatchData.MatchBags[prt];
            int myBags = spadesBrain.MatchData.MatchBags[pos];

            // ReSharper disable once RedundantAssignment
            int finalBid = -1;

            // You see your hand only after deciding if you try Blind Nil
            CardsList hand = spadesBrain.Players[pos].Hand;
            // if (biddingPosition ==1 ) hand.SetCards("6H5H4H3H2H5D2D4C3C2C4S3S2S");  // set hand manually 
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"position {0}, hand: {1} ", pos, hand);

            // Initial bid as a function of the hand
            prData.InitialBid = InitialBid(pos, hand, spadesBrain.MatchData.Mode);
            
            // Adjustment according to the other bids
            float otherPlayersFactor = OtherPlayersFactor(biddingPosition, (int) Math.Round(prData.InitialBid, MidpointRounding.AwayFromZero));
            prData.InitialBid += otherPlayersFactor;
            if (prData.InitialBid <= 1) prData.InitialBid = 1;

            // adjustment and round according to bot aggressiveness level 
            int botLevel = spadesBrain.Players[pos].Level;
            prData.NumericBid = RoundingByBotLevel(prData.InitialBid, botLevel);

            // bid {-1 / +1 / +2 / =0 / sum = 14} when you are about to lose the game if you don't
            //prData.NumericBid = LastRoundBidAdjust(bagsToPenalty, opponentsBags, prData.NumericBid, pos, hand, biddingPosition, opponentsPointsIfBidMet, partnersPointsIfBidMetBeforeMyBid, 0);

            // Adjust bid if we dont need so much points to reduce risk of being set
            //prData.NumericBid += ReduceRiskIfWinIsInMyPocket(biddingPosition, prData.NumericBid, spadesBrain.MatchData.Mode, pos,
            //    lhoPointsIfBetMet, rhoPointsIfBetMet, prtPointsIfBetMet, partnersPointsIfBidMetBeforeMyBid,
            //    partnersBags);

            // Validations
            prData.NumericBid = BidValidationCallBreak(pos, biddingPosition, prData.NumericBid);

            finalBid = prData.NumericBid;
            prData.Bid = prData.NumericBid;
            return finalBid;
        }


        /// <summary>
        /// change bid by {-1, +1, +2} when sure lose if not
        /// </summary>
        protected int LastRoundBidAdjust(int bagsToPenalty, int opponentsBags, int myBid, Position pos, CardsList hand,
            int biddingPosition, int opponentsPointsIfBidMet, int partnersPointsIfBidMetBeforeMyBid, float nilChances)
        {
            return myBid;
        }
       
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public int RoundingByBotLevel(float bid, int botLevel)
        {
            int bidleveladjustment;
            switch (botLevel)
            {
                case 1:
                    bidleveladjustment = (int) Math.Round(bid - 0.5, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"very conservative bot bid - 0.5 = " + (bid - 0.5) );
                    break;

                case 2:
                    bidleveladjustment = (int) Math.Round(bid - 0.3, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"conservative bot, bid - 0.3 = " + (bid - 0.3) );
                    break;

                case 3:
                    bidleveladjustment = (int) Math.Round(bid - 0.1, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"neutral bot, bid - 0.1 = " + (bid - 0.1));
                    break;

                case 4:
                    bidleveladjustment = (int) Math.Round(bid + 0.1, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"aggressive bot, bid + 0.1 = " + (bid + 0.1) );
                    break;

                case 5:
                    bidleveladjustment = (int) Math.Round(bid + 0.3, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"very aggressive bot, bid + 0.3 = " + (bid+0.3) );
                    break;
                case 6:
                    bidleveladjustment = (int) Math.Round(bid + 0.5, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"very very aggressive bot, bid + 0.5 = " + (bid+0.5) );
                    break;
                case 0:
                    Random randm = new Random();
                    float rnd_noise = (float)(randm.NextDouble() * 2 - 1);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"add uniform[-1,1] noise");
                    bidleveladjustment = (int)Math.Round(bid + rnd_noise, MidpointRounding.AwayFromZero);
                    break;
                default:
                    bidleveladjustment = (int) Math.Round(bid, MidpointRounding.AwayFromZero);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"neutral bot, bid = " + bid);
                    break;
            }

            return bidleveladjustment;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public float InitialBid(Position pos, CardsList hand, PlayingMode gameMode)
        {
            int numPlayers = gameMode == PlayingMode.Solo ? 3 : 2;
            List<Position> oppoPositions = SpadesUtils.OpponentsPositions(pos, spadesBrain.MatchData.Mode);
            foreach (var oppoPos in oppoPositions)
            {
                if (spadesBrain.Players[oppoPos].Bid == 0)
                {
                    numPlayers = gameMode == PlayingMode.Solo ? 2 : 1;
                }
            }

            float initialBid = PotentialBid(hand, numPlayers);
            return initialBid;
        }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public override float PotentialBid(CardsList cardsList, int numberOfPlayersThatMightCutYou)
        {
            float result = 0;
            float aceFactor = 0, kingFactor = 0, queenFactor = 0;
            int voids = 0, singletons = 0, doubletons = 0;
            CardsList cards;

            // Non trump counting
            Card.SuitType[] nonTrump = {Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts};
            foreach (Card.SuitType suit in nonTrump)
            {
                cards = cardsList.CardsBySuits(suit);
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"cards = " + cards);
                // Ace
                if (cards.HasRank(Card.RankType.Ace))
                {
                    aceFactor = GetProbTableValue(Card.RankType.Ace, cards.Count, numberOfPlayersThatMightCutYou);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"for the ace = " + aceFactor);
                }

                // King
                if (cards.HasRank(Card.RankType.King))
                {
                    kingFactor = GetProbTableValue(Card.RankType.King, cards.Count, numberOfPlayersThatMightCutYou);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"for the king = " + kingFactor);
                }

                // Queen
                if (cards.HasRank(Card.RankType.Queen))
                {
                    queenFactor = GetProbTableValue(Card.RankType.Queen, cards.Count, numberOfPlayersThatMightCutYou);
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"for the queen = " + queenFactor);
                }

                // Count short non-trump suits
                switch (cards.Count)
                {
                    case 0:
                        voids++;
                        break;
                    case 1:
                        singletons++;
                        break;
                    case 2:
                        doubletons++;
                        break;
                }

                result += aceFactor + kingFactor + queenFactor;
                aceFactor = 0; kingFactor = 0; queenFactor = 0;
            }

            // Trump counting
            cards = cardsList.CardsBySuits(Card.SuitType.Spades);
            cards.SortHighToLowSuitFirst();
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"spades cards = " + cards);

            float bid = 0;
            int spareSpades = cards.Count;
            bool countK = false, countQ = false, countJ = false;
            if (cards.HasRank(Card.RankType.Ace))
            {
                //A
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"As = 1");
                bid++;
                spareSpades--;
                if (cards.HasRank(Card.RankType.King))
                {
                    // AK
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"Ks = 1");
                    bid++;
                    spareSpades--;
                    countK = true;
                    if (cards.HasRank(Card.RankType.Queen))
                    {
                        // AKQ
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"Qs = 1");
                        bid++;
                        spareSpades--;
                        countQ = true;
                        if (cards.HasRank(Card.RankType.Jack))
                        {
                            // AKQJ
                            bid = cards.Count;
                            spareSpades = 0;
                            countJ = true;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"AKQJxx - any spade is a take = " + bid);
                        }
                        else if (spareSpades >= 2)
                        {
                            // AKQxx
                            bid = cards.Count - 1;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"AKQxx= " + bid);
                            spareSpades = 0;
                        }
                    }
                    else if (cards.HasRank(Card.RankType.Jack))
                    {
                        // AK_Jxx
                        bid = cards.Count - 1;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"AK_Jxx= " + bid);
                        spareSpades = 0;
                    }
                }
                else if (cards.HasRank(Card.RankType.Queen))
                {
                    // A_Qx
                    if (cards.Count > 2)
                    {
                        bid = 2;
                    }
                    if (cards.HasRank(Card.RankType.Jack))
                    {
                        // A_QJxx
                        bid = cards.Count - 1;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"A_QJxx = " + bid);
                        spareSpades = 0;
                    }

                }
            }
            else if (cards.HasRank(Card.RankType.King) && cards.HasRank(Card.RankType.Queen) &&
                     cards.HasRank(Card.RankType.Jack))
            {
                // _KQJxx
                bid = cards.Count - 1;
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"_KQJxx = " + bid);
                spareSpades = 0;
            }

            else if (cards.HasRank(Card.RankType.King) && cards.HasRank(Card.RankType.Queen))
            {
                // _KQxx
                if (cards.Count == 2)
                {
                    bid = 1;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"_KQ = " + bid);
                    spareSpades = 0;
                }
                else if (cards.Count == 3 || cards.Count == 4)
                {
                    bid = 2;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"_KQx/x = " + bid);
                    spareSpades = 0;
                }
                else if (cards.Count > 4)
                {
                    bid = cards.Count - 2;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"_KQxxx" + bid);
                    spareSpades = 0;
                }
            }
            else if (cards.Count >= 6 && spareSpades > 0)
            {
                switch (cards.Count)
                {
                    case 6:
                        bid += cards.Count - 3.5f;
                        break;
                    case 7:
                        bid += cards.Count - 3.0f;
                        break;
                    case 8:
                        bid += cards.Count - 2.5f;
                        break;
                    case 9:
                        bid += cards.Count - 2.3f;
                        break;
                    case 10:
                        bid += cards.Count - 2.0f;
                        break;
                    case 11:
                        bid += cards.Count - 1.5f;
                        break;
                    case 12:
                        bid += cards.Count - 1.0f;
                        break;
                    case 13:
                        bid += cards.Count - 0.0f;
                        break;
                    default:
                        bid += cards.Count - 3.5f;
                        break;
                }

                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"6+ spades = " + bid);
                spareSpades = 0;
            }

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"spades factor = " + bid);
            bool countVoid = false, countSingelton = false, countDoubleton = false;

            while (spareSpades > 0)
            {
                if (voids > 0 && !countVoid)
                {
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"voids>0 = ");

                    if (spareSpades >= 3)
                    {
                        bid += 2.67f;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"more than 2 spare spades = 2.67" + '\n' + "spades factor = " + bid);
                        spareSpades = spareSpades - 3;
                        countVoid = true;
                    }
                    else if (spareSpades == 2)
                    {
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"2 spare spades = 1.94" + '\n' + "spades factor = " + bid);
                        bid += 1.94f;
                        spareSpades = spareSpades - 2;
                        countVoid = true;
                    }
                    else if (spareSpades == 1)
                    {
                        bid += 0.99f;
                        spareSpades = spareSpades - 1;
                        countVoid = true;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"1 spare spades = 0.99" + '\n' + "spades factor = " + bid);
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"spades factor ==" + bid);
                    }
                }
                else if (singletons > 0 && !countSingelton)
                {
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"singletons>0");

                    if (spareSpades >= 2)
                    {
                        bid += 1.5f;
                        spareSpades = spareSpades - 2;
                        countSingelton = true;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"more than 1 spare spades = 1.5" + '\n' + "spades factor = " + bid);
                    }
                    else if (spareSpades == 1)
                    {
                        bid += 0.915f;
                        spareSpades = spareSpades - 1;
                        countVoid = true;
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"1 spare spades = 0.915" + '\n' + "spades factor = " + bid);
                    }
                }
                else if (cards.HasRank(Card.RankType.King) && !countK && spareSpades >= 2)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 2;
                    countK = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"king + more than 2 spare spades = 1" + '\n' + "spades factor = " + bid);
                }
                else if (cards.HasRank(Card.RankType.Queen) && !countQ && spareSpades >= 3)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 3;
                    countQ = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"queen + more than 3 spare spades = 1" + '\n' + "spades factor = " + bid);
                }
                else if (cards.HasRank(Card.RankType.Jack) && !countJ && spareSpades >= 4)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 4;
                    countJ = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"jack + more than 4 spare spades = 1" + '\n' + "spades factor = " + bid);
                }
                else if (doubletons > 0 && !countDoubleton && spareSpades >= 1)
                {
                    bid += 1f;
                    spareSpades = spareSpades - 1;
                    countDoubleton = true;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"doubletons + more than 1 spare spades = 1" + '\n' + "spades factor = " + bid);
                }
                else
                {
                    if (spareSpades > 3)
                    {
                        if (spareSpades == 5)
                        {
                            bid += 1.11f;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"5 spare spades = 1.11" + '\n' + "spades factor = " + bid);
                        }

                        if (spareSpades == 4)
                        {
                            bid += 0.11f;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"4 spare spades = 0.11" + '\n' + "spades factor = " + bid);
                        }
                    }

                    spareSpades = 0;
                }
            }

            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"for all the spades = " + bid);
            result += bid;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"first result = " + result);
            return result;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        protected float OtherPlayersFactor(int biddingPosition, int myBid)
        {
            float result = 0;

            int totalBids = CalcTotalBids() + myBid;
            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"totalBids = " + totalBids);

            switch (biddingPosition)
            {
                case 4:
                    if (totalBids < 11)
                    {
                        result += Math.Min(0.3f * (11 - totalBids), 1.5f);
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"position 4 total bids < 11 factor = " + result);
                    }
                    else if (totalBids > 12)
                    {
                        result += Math.Max(-0.4f * (totalBids - 12), -1.5f);
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"position 4 total bids > 12 factor = " + result);
                    }

                    break;
                case 3:
                    if (totalBids < 7)
                    {
                        result += Math.Min(0.2f * (7 - totalBids), 1.0f);
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"position 3 total bids < 7 factor = " + result);
                    }
                    else if (totalBids > 10)
                    {
                        result += Math.Max(-0.2f * (totalBids - 10), -1.0f);
                        // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"position 3 total bids > 10 factor = " + result);
                    }

                    break;
            }

            if (Math.Abs(result) > 0.1)
            {
                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"other player factor = " + result);
            }

            return result;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        protected int BidValidationCallBreak(Position pos, int biddingPosition, int bid)
        {
            if (bid < 1) bid = 1;
            if (bid > 8) bid = 8;
            return bid;
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
