﻿using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain {
    public class CantWinDuckLowCallBreak : AbstractHandler {

        private const string StrategyFieldName = "LOS";

        public CantWinDuckLowCallBreak(SpadesBrainImpl spadesBrain) : base(spadesBrain)
        {
        }
        public override void Handle(AITurnData turnData)
        {
            // cant win this trick -> duck lowest in the suit.
            int strategyRule = 0;
            turnData.CardToPlay = turnData.CardsToUse.GetWeakCard();
            if (turnData.CardToPlay != null)
            {
                strategyRule = 1;
//                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(DUK), {0} duck weakest", turnData.PlayerPos);
            }

            if (turnData.CardToPlay == null)
            {
                nextHandler.Handle(turnData);
            }
            else
            {
                turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }
    }
}
