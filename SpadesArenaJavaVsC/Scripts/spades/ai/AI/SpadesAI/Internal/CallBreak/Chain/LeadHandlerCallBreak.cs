﻿using System;
using System.Collections.Generic;
using System.Linq;
using cardGames.models;
using spades.models;
using SpadesAI.Internal;
using SpadesAI.model;

namespace SpadesAI.V4.Chain {
    public class LeadHandlerCallBreak : AbstractHandler {

        private const string StrategyFieldName = "LED";

        public LeadHandlerCallBreak(SpadesBrainImpl spadesBrain) : base(spadesBrain) {
        }
        public override void Handle(AITurnData turnData) {
			int strategyRule = 0;
            Position pos = turnData.PlayerPos;

            if (spadesBrain.CardsOnTable.Count == 0)
            {
                // Lead: (from suits that no opponent is void)
                // 1. side suit singleton {A}
                // 2. side suit doubleton {A,K}
                // 3. if have more than 5/13 spades, lead spades, if have spades boss lead boss otherwise lead low spade. **
                // 6. your short side suit 
                // 7. low from side suit with second boss **
                // 8. Ace from side suit with Ace

                Position oho = SpadesUtils.PartnerPosition(pos);  // opposite hand opponent
                Position rho = SpadesUtils.RhoPosition(pos);
                Position lho = SpadesUtils.LhoPosition(pos);
                CardsList myHand = spadesBrain.Players[pos].Hand;

                Dictionary<Card.SuitType, CardsList> sideSuitCardsListDictionary = GetsideSuitCardsListDictionary(turnData);
                // remove suits that:
                // in Partners: RHO void, LhO void & partner !void
                // in Solo: all voids
                RemoveOppVoids(sideSuitCardsListDictionary, rho, lho, oho);

                // 1. side Singelton {A}
                List<Card> singeltonAce = GetSingeltonAce(sideSuitCardsListDictionary);
                if (singeltonAce.Count > 0)
                {
                    turnData.CardToPlay = singeltonAce[0];
                    strategyRule = 1;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0} singleton Ace", pos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

                // 2. side Doubleton {A,K}
                List<Card> aceKingDoubleton = GetAceKingDoubletonSuits(sideSuitCardsListDictionary);
                if (aceKingDoubleton.Count > 0)
                {
                    turnData.CardToPlay = aceKingDoubleton[0];
                    strategyRule = 2;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0} doubleton of A,K", pos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

                // 3. if start with more than 5 spades, lead spades, if have spades boss lead boss otherwise lead low spade. **
                CardsList mySpades = myHand.CardsBySuits(Card.SuitType.Spades);
                int thrownSpades = spadesBrain.Players[pos].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count + 
                                   spadesBrain.Players[oho].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count +
                                   spadesBrain.Players[rho].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count +
                                   spadesBrain.Players[lho].ThrowHistory.CardsBySuits(Card.SuitType.Spades).Count;
                int UnplayedSpades = 13 - mySpades.Count - thrownSpades;
                if (mySpades.Count > 0 && 
                   (UnplayedSpades == 0 || (mySpades.Count / (float)UnplayedSpades >= 5f / 8f)) )
                {
                    // play spade
                    Card spadeBoss = SpadesUtils.CalcBoss(Card.SuitType.Spades, spadesBrain);
                    if (spadeBoss != null)
                    {
                        if (mySpades.Contains(spadeBoss))
                        {
                            // play spades Boss
                            turnData.CardToPlay = spadeBoss;
                            if (turnData.CardToPlay != null)
                            {
                                strategyRule = 3;
                                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0}, {1} Boss", pos, turnData.CardToPlay.Suit);
                                turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                                return;
                            }
                        }
                        else
                        {
                            // play lowest spade
                            turnData.CardToPlay = mySpades.GetWeakest();
                            if (turnData.CardToPlay != null)
                            {
                                strategyRule = 4;
                                // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0}, {1} small", pos, turnData.CardToPlay.Suit);
                                turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                                return;
                            }
                        }
                    }
                }
                // 6. short suit
                if (spadesBrain.Players[pos].IsSingletonOrDoubleton)
                {
                    CardsList shortSuit = spadesBrain.Players[pos].Hand.CardsBySuits(spadesBrain.Players[pos].SingletonOrDoubleton);
                    if (shortSuit.Count > 0) // still have cards in short suit
                    {
                        shortSuit.SortHighToLowRankFirst();
                        turnData.CardToPlay = shortSuit.Last(); // low  to high
                        if (turnData.CardToPlay != null)
                        {
                            strategyRule = 5;
                            // if smaller than boss, it is a tell to partner to lead agian this suit
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0} low from shortsuit {1} ", pos, turnData.CardToPlay.Suit);
                            turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                            return;
                        }
                    }
                }
                // 7. low from side suit with second boss (to force play of the boss)**
                // try promote your second boss to a boss by playing medium card.
                foreach (Card.SuitType suit in Enum.GetValues(typeof(Card.SuitType)))
                {
                    CardsList unplayedBySuits = SpadesUtils.GetUnplayedDeck(spadesBrain).CardsBySuits(suit);
                    CardsList myHandBySuit = myHand.CardsBySuits(suit);
                    unplayedBySuits.SortHighToLowRankFirst();

                    Card bossBySuit = unplayedBySuits.GetStrongestBelowOrEqualRank(Card.RankType.Ace);
                    Card secondBossBySuit = null;
                    if (bossBySuit != null)
                    {
                        secondBossBySuit = unplayedBySuits.GetStrongestBelowRank(bossBySuit.Rank);
                    }

                    if (secondBossBySuit != null && !myHandBySuit.Contains(bossBySuit) && myHandBySuit.Contains(secondBossBySuit))  // dont have the boss but have second boss
                    {
                        turnData.CardToPlay = myHandBySuit.GetStrongestBelowRank(secondBossBySuit.Rank);
                        if (turnData.CardToPlay != null)
                        {
                            strategyRule = 6;
                            // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0}, {1}, low from side suit with second boss", pos, turnData.CardToPlay.Suit);
                            turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                            return;
                        }
                    }
                }

                // 8. lead Ace
                List<Card> suitWithAce = GetSuitWithAce(sideSuitCardsListDictionary);
                if (suitWithAce.Count > 0)
                {
                    turnData.CardToPlay = suitWithAce[0];
                    strategyRule = 7;
                    // LoggerController.Instance.LogFormat(// LoggerController.Module.AI,"(Lead) {0} Ace since all side suits have an Ace and never underlead an Ace", pos);
                    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
                    return;
                }

            }
            if (turnData.CardToPlay == null) {
                nextHandler.Handle (turnData);
            } else {
			    turnData.PlayerRoundData.Strategies += StrategyFieldName + strategyRule + turnData.CardToPlay + ",";
            }
        }

        private void RemoveOppVoids(Dictionary<Card.SuitType, CardsList> sideSuitCardsListDictionary, Position rho, Position lho, Position partnerPos)
        {
            if (spadesBrain.MatchData.Mode == PlayingMode.Partners)
            {
                if (!spadesBrain.Players[rho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[rho].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }

                if (!spadesBrain.Players[lho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[lho].VoidSuits)
                    {
                        if (!spadesBrain.Players[partnerPos].VoidSuits.Contains(badSuit) || spadesBrain.Players[partnerPos].VoidSuits.Contains(Card.SuitType.Spades))
                        {
                            sideSuitCardsListDictionary.Remove(badSuit);
                        }
                    }
                }
            }
            else if (spadesBrain.MatchData.Mode == PlayingMode.Solo)
            {
                if (!spadesBrain.Players[rho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[rho].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }

                if (!spadesBrain.Players[lho].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[lho].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }

                if (!spadesBrain.Players[partnerPos].VoidSuits.Contains(Card.SuitType.Spades))
                {
                    foreach (Card.SuitType badSuit in spadesBrain.Players[partnerPos].VoidSuits)
                    {
                        sideSuitCardsListDictionary.Remove(badSuit);
                    }
                }
            }
        }


        private Dictionary<Card.SuitType, CardsList> GetsideSuitCardsListDictionary(AITurnData turnData)
        {
            Dictionary<Card.SuitType, CardsList> suitCountDictionary = new Dictionary<Card.SuitType, CardsList>();
            foreach (Card card in spadesBrain.Players[turnData.PlayerPos].Hand)
            {
                if (card.Suit == Card.SuitType.Spades) continue; // Skip spades
                CardsList cardsList;
                suitCountDictionary.TryGetValue(card.Suit, out cardsList);
                if (cardsList == null)
                {
                    suitCountDictionary[card.Suit] = new CardsList();
                }
                suitCountDictionary[card.Suit].Add(card);
            }
            return suitCountDictionary;
        }


        private static List<Card> GetSingeltonAce(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            return suitCountDictionary
                .Where(pair => pair.Value.Count == 1 
                               && pair.Value[0].Rank == Card.RankType.Ace)
                .Select(k => k.Value[0]).ToList();
        }


        private static List<Card> GetAceKingDoubletonSuits(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            return suitCountDictionary
                .Where(pair => pair.Value.Count == 2 
                               && pair.Value[0].Rank == Card.RankType.Ace 
                               && pair.Value[1].Rank == Card.RankType.King)
                .Select(k => k.Value[1]).ToList();
        }


        private static List<Card> GetSuitWithAce(Dictionary<Card.SuitType, CardsList> suitCountDictionary)
        {
            return suitCountDictionary.Where(pair => pair.Value[0].Rank == Card.RankType.Ace).Select(k => k.Value[0]).ToList();
        }
    }
}
