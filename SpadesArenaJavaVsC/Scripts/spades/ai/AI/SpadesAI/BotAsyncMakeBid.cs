using SpadesAI.model;
using spades.models;

#if !UNITY_WEBGL

#endif

namespace SpadesAI
{
    /// <summary>
    /// Async MakeBid.
    /// For mobile, MakeBid will be called using a Thread.
    /// For WebGL, MakeBid will be called after ThinkBid returns true
    /// </summary>
    public class BotASyncMakeBid : CustomYieldInstruction
    {
        ISpadesBrain spadesBrain;

        bool finished;

        int makeBidResult;

        Position pos;
        SpadesPlayerRoundData prData;

        public BotASyncMakeBid(ISpadesBrain spadesBrain, Position pos, SpadesPlayerRoundData prData)
        {
            this.spadesBrain = spadesBrain;
#if !UNITY_WEBGL && false // CURRENTLY NOT USED (RAN 4/11/2019)
            Thread t = new Thread(()=>{
				makeBidResult = spadesBrain.MakeBid(pos,prData);
                finished = true;
            });
            t.Start();
#else
            this.pos = pos;
            this.prData = prData;
#endif
        }


        public override bool keepWaiting
        {
            get
            {
#if !UNITY_WEBGL && false // CURRENTLY NOT USED (RAN 4/11/2019)
                return !finished;
#else
                if (spadesBrain.ThinkBid(pos)) {
                    makeBidResult = spadesBrain.MakeBid(pos, prData);
                    return false;
                }
                return true;
#endif
            }
        }


        public int MakeBidResult
        {
            get
            {
                return makeBidResult;
            }
        }
    }

}
