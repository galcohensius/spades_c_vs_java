﻿using System.Collections.Generic;
using cardGames.models;
using CommonAI;
using spades.models;

namespace SpadesAI.model
{
	public class AITurnData
    {
        // default Constructor
        public AITurnData() { }

        // Copy Constructor
        public AITurnData(AITurnData turnDataToCopy)
        {
            PlayerPos = turnDataToCopy.PlayerPos;
            Bid = turnDataToCopy.Bid;
            Takes = turnDataToCopy.Takes;
            TotalBids = turnDataToCopy.TotalBids;
            TotalOverTakes = turnDataToCopy.TotalOverTakes;
            BidDistance = turnDataToCopy.BidDistance;
            Strategy = turnDataToCopy.Strategy;

            ProtectSelfNil = turnDataToCopy.ProtectSelfNil;
            ProtectPartnerNil = turnDataToCopy.ProtectPartnerNil;
            OpponentNilPositions = turnDataToCopy.OpponentNilPositions;

            TakesForSureFromSpade = turnDataToCopy.TakesForSureFromSpade;
            NextTricksSureTakes = turnDataToCopy.NextTricksSureTakes;
            PredictedTakes = turnDataToCopy.PredictedTakes;

            CardsToUse = turnDataToCopy.CardsToUse;
            CardToPlay = turnDataToCopy.CardToPlay;

            PlayerRoundData = turnDataToCopy.PlayerRoundData;
        }

        public void CopyAITurnData(AITurnData aiTurnDataToCopy, Position leadPositionWhenUCTStart)
        {
            PlayerPos = aiTurnDataToCopy.PlayerPos;
            Bid = aiTurnDataToCopy.Bid;
            Takes = aiTurnDataToCopy.Takes;
            TotalBids = aiTurnDataToCopy.TotalBids;
            TotalOverTakes = aiTurnDataToCopy.TotalOverTakes;
            BidDistance = aiTurnDataToCopy.BidDistance;
            Strategy = aiTurnDataToCopy.Strategy;

            ProtectSelfNil = aiTurnDataToCopy.ProtectSelfNil;
            ProtectPartnerNil = aiTurnDataToCopy.ProtectPartnerNil;
            OpponentNilPositions = aiTurnDataToCopy.OpponentNilPositions;

            TakesForSureFromSpade = aiTurnDataToCopy.TakesForSureFromSpade;
            NextTricksSureTakes = aiTurnDataToCopy.NextTricksSureTakes;
            PredictedTakes = aiTurnDataToCopy.PredictedTakes;

            if (CardsToUse is null) CardsToUse = new AIStrengthCardsList();
            CardsToUse.CopyCardsList(aiTurnDataToCopy.CardsToUse);
            CardToPlay = aiTurnDataToCopy.CardToPlay;

            PlayerRoundData = aiTurnDataToCopy.PlayerRoundData;
            // AITurnData.LeadingPlayer is static
            LeadingPlayer = leadPositionWhenUCTStart;
        }


        public static void ResetAITurnData(AITurnData aiTurnDataToReset, Position position)
        {
            aiTurnDataToReset.PlayerPos = position;
            aiTurnDataToReset.Bid = -1;
            aiTurnDataToReset.Takes = -1;
            aiTurnDataToReset.TotalBids = -1;
            aiTurnDataToReset.TotalOverTakes = -1;
            aiTurnDataToReset.BidDistance = -1;
            aiTurnDataToReset.Strategy = AIStrategy.StrongOver;

            aiTurnDataToReset.ProtectSelfNil = false;
            aiTurnDataToReset.ProtectPartnerNil = false;
            aiTurnDataToReset.OpponentNilPositions = null;

            aiTurnDataToReset.TakesForSureFromSpade = -1;
            aiTurnDataToReset.NextTricksSureTakes = -1;
            aiTurnDataToReset.PredictedTakes = -1;

            aiTurnDataToReset.CardsToUse = new AIStrengthCardsList();
            aiTurnDataToReset.CardToPlay = null;

            // aiTurnDataToReset.PlayerRoundData = null; 
            // aiTurnDataToReset.LeadingPlayer = leadPositionWhenUCTStart; 
        }


        public Position PlayerPos { get; set; }
		public int Bid { get; set; }
		public int Takes { get; set; }
		public bool ProtectSelfNil { get; set; }
		public int NextTricksSureTakes { get; set; }
		public int PredictedTakes { get; set; }

		public int BidDistance { get; set; } // Distance = totalBids - 13 + totalOverTakes
        public int TotalBids { get; set; }  // V4
        public int TotalOverTakes { get; set; } // V4
        public AIStrategy Strategy { get; set; }
		
		public AIStrengthCardsList CardsToUse { get; set; }
		public Card CardToPlay { get; set; }
		
		public bool ProtectPartnerNil { get; set; }
		public List<Position> OpponentNilPositions { get; set; }

        public SpadesPlayerRoundData PlayerRoundData { get; set; }

        public int TakesForSureFromSpade { get; set; } // V4

        public static Position LeadingPlayer { get; set; } // V4

    }
}

