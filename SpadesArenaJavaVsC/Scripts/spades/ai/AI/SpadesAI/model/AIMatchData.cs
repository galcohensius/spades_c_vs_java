﻿using System.Collections.Generic;
using spades.models;

namespace SpadesAI.model
{
	public class AIMatchData
	{
	    public AIMatchData ()
		{
			MatchPoints = new Dictionary<Position,int> () {
				{ Position.East,0 },
				{ Position.North,0 },
				{ Position.South,0 },
				{ Position.West,0 }
			};

		    MatchPointsIfBetMet = new Dictionary<Position, int>() {
		        { Position.East,0 },
		        { Position.North,0 },
		        { Position.South,0 },
		        { Position.West,0 }
		    };

            MatchBags = new Dictionary<Position,int> () {
				{ Position.East,0 },
				{ Position.North,0 },
				{ Position.South,0 },
				{ Position.West,0 }
			};
			
			MatchBagsPenalty = new Dictionary<Position,int> () {
				{ Position.East,0 },
				{ Position.North,0 },
				{ Position.South,0 },
				{ Position.West,0 }
			};

		}
        public PlayingMode Mode { get; set; }
	    internal int RoundNum { get; set; }
        public PlayingVariant Variation { get; set;}
        public PlayingSubVariant SubVariation { get; set;}

        public int LoseConditionPoints { get; set; }
	    public int WinConditionPoints { get; set; }
	    public int BagsToIncurPenalty { get; set; }
	    public int BagsPenalty { get; set; }

        public Dictionary<Position, int> MatchPoints { get; set; }
	    public Dictionary<Position, int> MatchPointsIfBetMet { get; set; }  // TODO: update it in the right place (round controller? PrepareAITurnDataHandlerV4?)
        public Dictionary<Position, int> MatchBags { get; set; }
	    public Dictionary<Position, int> MatchBagsPenalty { get; private set; }
	}
}

