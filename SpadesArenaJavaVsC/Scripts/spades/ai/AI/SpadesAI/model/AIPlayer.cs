﻿using System.Collections.Generic;
using cardGames.models;

namespace SpadesAI.model
{
	public enum Position {
		South,West,North,East
	}

	public class AIPlayer
	{
	    // Data collected during game play
	    private Dictionary<Card.SuitType, Card.RankType> safeCoverNilDictionary = new Dictionary<Card.SuitType, Card.RankType>();

	    internal AIPlayer (BrainType? brainType=null, int level = 1)
        {
		    Bid = -1;
		    VoidSuits = new HashSet<Card.SuitType>();
            Level = level;
            ThrowHistory = new CardsList ();
            BrainType = brainType;

		    safeCoverNilDictionary.Add(Card.SuitType.Clubs, Card.RankType.Ace);
		    safeCoverNilDictionary.Add(Card.SuitType.Diamonds, Card.RankType.Ace);
		    safeCoverNilDictionary.Add(Card.SuitType.Hearts, Card.RankType.Ace);
		    safeCoverNilDictionary.Add(Card.SuitType.Spades,
		        brainType == SpadesAI.BrainType.Jokers ? Card.RankType.JokerBig : Card.RankType.Ace);
		}


        // Copy AIPlayer
        public void CopyAiPlayer(AIPlayer aiPlayerToCopy)
        {
            this.BrainType = aiPlayerToCopy.BrainType;

            if (this.Hand == null) this.Hand = new CardsList();
            this.Hand.CopyCardsList(aiPlayerToCopy.Hand);
            this.Bid = aiPlayerToCopy.Bid;
            this.BlindNil = aiPlayerToCopy.BlindNil;
            this.Takes = aiPlayerToCopy.Takes;
            this.ThrowHistory.CopyCardsList(aiPlayerToCopy.ThrowHistory);
            this.VoidSuits = new HashSet<Card.SuitType>(aiPlayerToCopy.VoidSuits);

            this.PartnersContractFulfilled = aiPlayerToCopy.PartnersContractFulfilled;
            this.MyContractFulfilled = aiPlayerToCopy.MyContractFulfilled;
            this.SingletonOrDoubleton = aiPlayerToCopy.SingletonOrDoubleton;

            this.Level = aiPlayerToCopy.Level;

            // Singleton or doubleton - collected before first trick 
            this.IsSingletonOrDoubleton = aiPlayerToCopy.IsSingletonOrDoubleton;

            // re-lead your partner's first lead suit that wasn't a boss
            this.PartnersFirstLeadSuit = aiPlayerToCopy.PartnersFirstLeadSuit;
            this.IsPartnerLeadNonBoss = aiPlayerToCopy.IsPartnerLeadNonBoss;
            this.IsNilerWasFirstLeader = aiPlayerToCopy.IsNilerWasFirstLeader;

            // if true, stop trying to set the niler and go for setting the coverer
            this.StopToBreakOpponentNil = aiPlayerToCopy.StopToBreakOpponentNil;
        }

        public Dictionary<Card.SuitType, Card.RankType> SafeCoverNilDictionary
	    {
	        get { return safeCoverNilDictionary; }
	        set { safeCoverNilDictionary = value; }
	    }

	    public BrainType? BrainType { get; private set; }
        public CardsList Hand { get; set; }
	    internal int Bid { get; set; }
	    public bool BlindNil { get; set; }
        public int Takes { get; set; }
	    internal CardsList ThrowHistory { get; private set; }
	    internal HashSet<Card.SuitType> VoidSuits { get; private set; }

        public bool PartnersContractFulfilled = false;
	    public bool MyContractFulfilled = false;
        internal Card.SuitType SingletonOrDoubleton { get; set; }

	    public int Level { get; set; }

	    // Singleton or doubleton - collected before first trick 
        public bool IsSingletonOrDoubleton { get; set; }  

	    // re-lead your partner's first lead suit that wasn't a boss
        public Card.SuitType PartnersFirstLeadSuit { get; set; }  
        public bool IsPartnerLeadNonBoss { get; set; }
	    public bool IsNilerWasFirstLeader { get; set; }

        // if true, stop trying to set the niler and go for setting the coverer
	    public bool StopToBreakOpponentNil { get; set; }  
    }
}
