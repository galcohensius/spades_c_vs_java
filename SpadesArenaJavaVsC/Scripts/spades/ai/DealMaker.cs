using spades.models;
using System;
using SpadesAI;
using spades.utils;
using SpadesAI.Internal.V4;
using System.Collections.Generic;
using cardGames.models;
using Logger = CommonAI.Logger;

namespace spades.ai
{

    public class DealMaker
    {

        enum SwitchHandMode
        {
            WeakestToPlayer, StrongestToPlayer
        }


        IBiddingBrain biddingBrain;
        System.Random random = new System.Random();

        int numGamesForRiggedHands=6,numOfStrongCards=3;

        private HandPattern[] strongHandPatterns = new HandPattern[5];


        public DealMaker(IBiddingBrain biddingBrain)
        {
            this.biddingBrain = biddingBrain;

            // Create strong hand patterns
            strongHandPatterns[0] = new HandPattern(this,3, 1, 2, 2, 5, 2, 3, 3);
            strongHandPatterns[1] = new HandPattern(this,3, 1, 3, 2, 3, 1, 4, 3);
            strongHandPatterns[2] = new HandPattern(this,4, 1, 4, 3, 0, 0, 5, 2);
            strongHandPatterns[3] = new HandPattern(this,3, 2, 4, 2, 1, 1, 5, 2);
            strongHandPatterns[4] = new HandPattern(this,4, 2, 3, 2, 2, 2, 4, 3);

            // Set the special room title
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                numGamesForRiggedHands = ldc.GetSettingAsInt("NumGamesForRiggedHands", 6);
                numOfStrongCards = ldc.GetSettingAsInt("NumOfStrongCards", 3);
            };

        }



        /// <summary>
        /// Switch the cards according to number of total matches and bots data.
        /// For match 1, use special cards that make sure he will win (Always partners, 3 bots)
        /// For match 2-6, switch in favor of the player
        /// For match 7- , switch according to bots data
        /// </summary>
        public void SwitchCards(SpadesTable table, SpadesActiveMatch aMatch)
        {
            ISpadesMatch spadesMatch = aMatch.GetMatch();
            SpadesUser user = table.Players[3].User;
            int totalMatches = (user.Stats as SpadesUserStats).TotalMatchesPlayed;
            int roundNum = aMatch.GetRoundsCount();

            if (totalMatches == 0 && roundNum == 0)
            {
                SwitchCardsFirstMatchFirstRound(table, aMatch);
            }
            else if (totalMatches < numGamesForRiggedHands)
            {
                OverrideDealWithPattern(table, spadesMatch);
            }
            else
            {
                if (biddingBrain != null)
                {

                    BotsData botsData = spadesMatch.BotsData;
                    // Check the round num for the card switch ratio
                    // RoundSwitchCardsRatio: if it's 1, always switch, else use modulo on roundNum (which is 0 based)
                    if (botsData.RoundSwitchCardsRatio == 0 ||
                       (botsData.RoundSwitchCardsRatio > 1 && roundNum % botsData.RoundSwitchCardsRatio > 0) ||
                        botsData.SwitchCardsNum == 0)
                    {
                        // LoggerController.Instance.LogFormat("No cards switching due to card switch ratio. Round is {0}, ratio is {1}", roundNum, botsData.RoundSwitchCardsRatio);
                    }
                    else if (IsShouldSwitch(spadesMatch, table, aMatch))
                    {
                        switch (botsData.SwitchType)
                        {
                            case "H":
                                SwitchHands(table, aMatch, SwitchHandMode.WeakestToPlayer);
                                // LoggerController.Instance.LogFormat("SwitchHandMode.WeakestToPlayer");
                                break;
                            case "P":
                                SwitchHands(table, aMatch, SwitchHandMode.StrongestToPlayer);
                                // LoggerController.Instance.LogFormat("SwitchHandMode.StrongestToPlayer");
                                break;
                            case "C":
                                SwitchSomeCards(table, aMatch, botsData.SwitchCardsNum);
                                // LoggerController.Instance.LogFormat("Switch Some Cards");
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        private bool IsShouldSwitch(ISpadesMatch spadesMatch, SpadesTable table, SpadesActiveMatch aMatch)
        {
            // Check the points spread. If the human is more then 50% or 70% the max points behind, do not switch
            int highPoints = spadesMatch.HighPoints;
            if (spadesMatch.Playing_mode == PlayingMode.Solo)
            {
                if (FindPointsSpreadSolo(table, aMatch) >= highPoints * 0.5)
                    return false;
            }
            else // Partners:
            {
                if (FindPointsSpreadPartners(aMatch) >= highPoints * 0.7)
                    return false;
            }
            return true;
        }


        private void SwitchHands(SpadesTable table, SpadesActiveMatch aMatch, SwitchHandMode mode) {
            ISpadesMatch spadesMatch = aMatch.GetMatch();

            // Find the weakest and strongest hands by comparing the bids
            int weakestHandPos = 0, strongestHandPos = 0, nilHandPos = -1; 
            int weakestBid = 14, strongestBid = -1; 

			for (int pos = 0; pos < 4; pos++)
            {
                // Find the bid value and the nil chance for the hand
                int numberOfPlayersThatMightCutYou = spadesMatch.Playing_mode == PlayingMode.Partners ? 2 : 3;
                float nilChances;
                int bid;
                if (spadesMatch.Variant == PlayingVariant.Jokers) // Jokers Deck
                {
                    nilChances = BiddingBrainJokers.PartnersNilChancesJokers(table.GetPlayer(pos).GetCards());
                    bid = (int)Math.Round(biddingBrain.PotentialBid(table.GetPlayer(pos).GetCards(), numberOfPlayersThatMightCutYou));
                }
                else  // Classic deck
                {
                    nilChances = spadesMatch.Playing_mode == PlayingMode.Partners 
                        ? BiddingBrainV4.PartnersNilChances(table.GetPlayer(pos).GetCards()) 
                        : BiddingBrainV4.SoloNilChances(table.GetPlayer(pos).GetCards());
                    bid = (int)Math.Round(biddingBrain.PotentialBid(table.GetPlayer(pos).GetCards(), numberOfPlayersThatMightCutYou));
                }

                // Check if weakest hand so far
                if (bid < weakestBid && nilChances < 0.4)
                {
                    weakestBid = bid;
                    weakestHandPos = pos;
                }
                // Check if strongest hand so far
                if (bid > strongestBid)
                {
                    strongestBid = bid;
                    strongestHandPos = pos;
                }
                // Store the nil hand if exists
                if (nilChances >= 0.5) {
                    nilHandPos = pos;
                }
			}

            if (spadesMatch.Playing_mode == PlayingMode.Solo)
            {
                if(mode == SwitchHandMode.WeakestToPlayer) {
                    SwitchHandBetweenPlayers(table, weakestHandPos, 3); // Give weakest hand to player
                } else {
                    SwitchHandBetweenPlayers(table, strongestHandPos, 3); // Give strongest hand to player
                }
            } 
            else 
            {
                if (mode == SwitchHandMode.WeakestToPlayer)
                {
                    // For partners, nil is the strongest hand
                    if (nilHandPos == 3) 
                        SwitchHandBetweenPlayers(table, 3, 0); // Give nil hand to LHO
                    else if (nilHandPos == 1)
                        SwitchHandBetweenPlayers(table, 1, 0); // Give nil hand to LHO
                    else if (weakestHandPos != 3 && weakestHandPos != 1)
                        SwitchHandBetweenPlayers(table, 1, weakestHandPos); // Give weakest hand to partner
                }
                else // SwitchHandMode.StrongestToPlayer
                {
                    if (nilHandPos == 0 || nilHandPos == 2)
                        SwitchHandBetweenPlayers(table, nilHandPos, 3); // Give nil hand to player
                    else if (strongestHandPos == 0 || strongestHandPos == 2)
                        SwitchHandBetweenPlayers(table, strongestHandPos, 3); // Give strongest hand to player
                }
            }
        }

        private void SwitchHandBetweenPlayers(SpadesTable table,int fromPos, int toPos) {
            CardsList fromHand = table.GetPlayer(fromPos).GetCards();
            CardsList toHand = table.GetPlayer(toPos).GetCards();

            table.GetPlayer(fromPos).SetCards(toHand);
            table.GetPlayer(toPos).SetCards(fromHand);

            // LoggerController.Instance.LogFormat("Switching hands between {0} and {1}", (SpadesRoundResults.Positions)fromPos,(SpadesRoundResults.Positions)toPos);

            // Update round data
            ((SpadesRoundData)ModelManager.Instance.RoundData)[table.LocalToServerPos(fromPos)].CardsSwitch = 13; // Entire hand
        }


        //        Position.West - 0
        //        Position.North - 1
        //        Position.East - 2
        //        Position.South - 3
        /// Three different switches for the variants: Suicide, Partners, Solo
        private void SwitchSomeCards(SpadesTable table, SpadesActiveMatch aMatch, int switchCardsNum)
        {
            ISpadesMatch spadesMatch = aMatch.GetMatch();
            if (spadesMatch.Variant == PlayingVariant.Suicide || spadesMatch.Variant == PlayingVariant.Whiz || spadesMatch.Variant == PlayingVariant.Mirror){
                CardsSwitchBetweenThePartners(table, switchCardsNum);
            }
            else if (spadesMatch.Playing_mode == PlayingMode.Solo){
                CardsSwitchSolo(table, aMatch, switchCardsNum);
            }
            else {// Partners
                CardsSwitchPartners(table, aMatch, switchCardsNum);
            }
        }


        private void CardsSwitchPartners(SpadesTable table, SpadesActiveMatch aMatch, int switchCardsNum)
        {
            SpadesRoundResults.Positions strongestHumanPartners = FindStrongestHumanPartners(table, aMatch);
            Player from;
            Player to;
            int x;

            if ((switchCardsNum > 0 && strongestHumanPartners == SpadesRoundResults.Positions.North_South) ||
                (switchCardsNum < 0 && strongestHumanPartners == SpadesRoundResults.Positions.West_East))
            {
                from = table.Players[1];  // take from North (partner of human)
                to = table.Players[0];    // give to West
                x = table.LocalToServerPos(0);
            }
            else
            {
                from = table.Players[0]; // take from West
                to = table.Players[3];   // give to South (human)
                x = table.LocalToServerPos(3);
            }
            
            ReplaceStrongestCards(from.GetCards(), to.GetCards(), switchCardsNum);
            
            // Update round data
            ((SpadesRoundData)ModelManager.Instance.RoundData)[x].CardsSwitch = switchCardsNum;
        }


        private void CardsSwitchSolo(SpadesTable table, SpadesActiveMatch aMatch, int switchCardsNum)
        {
            
            SpadesRoundResults.Positions strongestHuman = FindStrongestPosSolo(table, aMatch, Player.PlayerType.Human);
            SpadesRoundResults.Positions strongestBot = FindStrongestPosSolo(table, aMatch, Player.PlayerType.Bot);

            if (switchCardsNum > 0)
            {
                // Move cards from the strongest player to the strongest bot
                ReplaceStrongestCards(table.Players[(int) strongestHuman].GetCards(),table.Players[(int) strongestBot].GetCards(), switchCardsNum);
                // LoggerController.Instance.LogFormat("Switching {0} cards from {1} to {2}", switchCardsNum, strongestHuman, strongestBot);
            }
            else if (switchCardsNum < 0)
            {
                // Move cards from to player
                ReplaceStrongestCards(table.Players[(int) strongestBot].GetCards(),table.Players[(int) strongestHuman].GetCards(), switchCardsNum);
                // LoggerController.Instance.LogFormat("Switching {0} cards from {1} to {2}", switchCardsNum, strongestHuman, strongestBot);
            }

            // Update round data
            ((SpadesRoundData) ModelManager.Instance.RoundData)[table.LocalToServerPos((int) strongestHuman)].CardsSwitch = switchCardsNum;
        }

        private void CardsSwitchBetweenThePartners(SpadesTable table, int switchCardsNum)
        {
            CardsList handPartner1 = new CardsList();
            CardsList handPartner2 = new CardsList();
            // switch highest card from niler, for lowest from her partner.  
            if (switchCardsNum > 0)
            {
                // West <-> East
                handPartner1 = table.Players[0].GetCards();
                handPartner2 = table.Players[2].GetCards();
            }
            else if (switchCardsNum < 0)
            {
                // North <-> South
                handPartner1 = table.Players[1].GetCards();
                handPartner2 = table.Players[3].GetCards();
            }

            // Find better niler between the partners
            Logger.Enabled = false;
            float NilChancePartner1 = BiddingBrainV4.PartnersNilChances(handPartner1);
            float NilChancePartner2 = BiddingBrainV4.PartnersNilChances(handPartner2);
            Logger.Enabled = true;

            if (NilChancePartner1 > NilChancePartner2)
            {
                // west -> east / north -> south
                ReplaceStrongestCards(handPartner1, handPartner2, switchCardsNum);
                // LoggerController.Instance.LogFormat("Switching {0} cards between partners", switchCardsNum);
            }
            else
            {
                // east -> west / south -> north
                ReplaceStrongestCards(handPartner2, handPartner1, switchCardsNum);
                // LoggerController.Instance.LogFormat("Switching {0} cards between partners", switchCardsNum);
            }

            // Update round data
            if (switchCardsNum > 0)
            {
                ((SpadesRoundData) ModelManager.Instance.RoundData)[table.LocalToServerPos(0)].CardsSwitch = switchCardsNum;
                ((SpadesRoundData) ModelManager.Instance.RoundData)[table.LocalToServerPos(2)].CardsSwitch = switchCardsNum;
            }
            else if (switchCardsNum < 0)
            {
                ((SpadesRoundData) ModelManager.Instance.RoundData)[table.LocalToServerPos(1)].CardsSwitch = switchCardsNum;
                ((SpadesRoundData) ModelManager.Instance.RoundData)[table.LocalToServerPos(3)].CardsSwitch = switchCardsNum;
            }
        }


        private void SwitchCardsFirstMatchFirstRound(SpadesTable table, SpadesActiveMatch aMatch)
        {
            ISpadesMatch spadesMatch = aMatch.GetMatch();

            if (spadesMatch.Playing_mode == PlayingMode.Solo)
            {
                // Solo
            }
            else
            { // Partners

                CreateFirstRoundDealPattern().SwitchCards(
                    table.Players[0].GetCards(),
                    table.Players[1].GetCards(),
                    table.Players[2].GetCards(),
                    table.Players[3].GetCards()
                );

                for (int i = 0; i < 4; i++)
                {
                    // LoggerController.Instance.LogFormat("{0} new hand: {1}", (SpadesRoundResults.Positions)i, table.Players[i].GetCards());
                }

            }
        }


        /// <summary>
        /// Switch high cards so West is always the dealer
        /// </summary>
        public void UserBidsFirst(SpadesTable table, SpadesActiveMatch aMatch, CardsList highCards)
        {
            // Check for 3 bots and 1 real player
            if (table.Players[0].Player_Type == Player.PlayerType.Bot &&
                table.Players[1].Player_Type == Player.PlayerType.Bot &&
                table.Players[2].Player_Type == Player.PlayerType.Bot)
            {
                SpadesUser user = table.Players[3].User;

                int totalMatches = (user.Stats as SpadesUserStats).TotalMatchesPlayed;
                int roundNum = aMatch.GetRoundsCount();

                if (totalMatches == 0 && roundNum == 0)
                {
                    // Make the last card highest
                    highCards.Sort(new SpadesCardComparer());
                    CardsList topCard = highCards.RemoveCardsFromEnd(1);
                    highCards.Insert(0,topCard[0]);
                }
            }
        }

        private void ReplaceStrongestCards(CardsList from, CardsList to, int num)
        {
            CardsList fromCopy = new CardsList(from);
            fromCopy.SortHighToLowRankFirst();

            // Target list will not contain spades
            CardsList toCopy = to.CardsBySuits(Card.SuitType.Clubs, Card.SuitType.Hearts, Card.SuitType.Diamonds);
            toCopy.SortHighToLowRankFirst();

            for (int i = 0; i < Math.Abs(num); i++)
            {
                Card strongCard = fromCopy[0];
                fromCopy.RemoveAt(0);

                Card weakCard = toCopy[toCopy.Count - 1];
                toCopy.RemoveAt(toCopy.Count - 1);


                from.Remove(strongCard);
                from.Add(weakCard);
                
                to.Remove(weakCard);
                to.Add(strongCard);
            }
        }


        private SpadesRoundResults.Positions FindStrongestPosSolo(SpadesTable table, SpadesActiveMatch aMatch, Player.PlayerType playerType)
        {
            SpadesRoundResults.Positions result = SpadesRoundResults.Positions.South;

            if (aMatch.GetRoundsCount() == 0)
            {
                // For first round, search for the first player from position 3 to 0
                for (int pos = 3; pos >= 0; pos--)
                {
                    if (table.Players[pos].Player_Type == playerType) {
                        return (SpadesRoundResults.Positions) pos;
                    }
                }
                return result;
            }
            else
            {
                // Use points to find the strongest
                int highestPoints = -1000;

				for (int pos = 0; pos < 4; pos++) // Easier to run on the index and not the enum
				{
					if (table.Players[pos].Player_Type == playerType)
					{
						int points = aMatch.GetCalculatedTotalResults((SpadesRoundResults.Positions)pos);
						if (points > highestPoints)
						{
							highestPoints = points;
							result = (SpadesRoundResults.Positions) pos;
						}
					}
				}
				return result;

            }

        }


        private SpadesRoundResults.Positions FindStrongestHumanPartners(SpadesTable table, SpadesActiveMatch aMatch)
        {
            ISpadesMatch spadesMatch = aMatch.GetMatch();

            bool nsHasHumans = table.GetPlayer(3).Player_Type == Player.PlayerType.Human ||
                                table.GetPlayer(1).Player_Type == Player.PlayerType.Human;
            bool ewHasHumans = table.GetPlayer(0).Player_Type == Player.PlayerType.Human ||
                                table.GetPlayer(2).Player_Type == Player.PlayerType.Human;

            if (nsHasHumans && !ewHasHumans)
                // Humans only on NS
                return SpadesRoundResults.Positions.North_South;

            if (!nsHasHumans && ewHasHumans)
                // Humans only on EW
                return SpadesRoundResults.Positions.West_East;

            // Humans on both sides - check strength
            if (aMatch.GetRoundsCount() == 0)
            {
                int numberOfPlayersThatMightCutYou = spadesMatch.Playing_mode == PlayingMode.Partners ? 2 : 3;
                
                float potentialBidNS = biddingBrain.PotentialBid(table.Players[3].GetCards(),numberOfPlayersThatMightCutYou) +
                                       biddingBrain.PotentialBid(table.Players[1].GetCards(),numberOfPlayersThatMightCutYou);
                float potentialBidEW = biddingBrain.PotentialBid(table.Players[0].GetCards(),numberOfPlayersThatMightCutYou) +
                                       biddingBrain.PotentialBid(table.Players[2].GetCards(),numberOfPlayersThatMightCutYou);

                if (potentialBidNS > potentialBidEW)
                    return SpadesRoundResults.Positions.North_South;
                return SpadesRoundResults.Positions.West_East;
            }

            int roundResultsNS = aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South);
            int roundResultsEW = aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East);

            if (roundResultsNS > roundResultsEW)
                return SpadesRoundResults.Positions.North_South;
            return SpadesRoundResults.Positions.West_East;

        }

        /// <summary>
		/// Finds the points spread between the south and the highest position for Solo match
        /// </summary>
		private int FindPointsSpreadSolo(SpadesTable table, SpadesActiveMatch aMatch) {

			int humanPoints = aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.South);

			SpadesRoundResults.Positions strongestBot = FindStrongestPosSolo(table, aMatch, Player.PlayerType.Bot);
			int strongestBotPoints = aMatch.GetCalculatedTotalResults(strongestBot);

			return strongestBotPoints - humanPoints;
		}

		/// <summary>
        /// Finds the points spread between the south-north and west-east for partners match
        /// </summary>
		private int FindPointsSpreadPartners(SpadesActiveMatch aMatch)
        {
			int humanPoints = aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.North_South);
            int oppoPoints  = aMatch.GetCalculatedTotalResults(SpadesRoundResults.Positions.West_East);
            return oppoPoints - humanPoints;
        }


        private DealPattern CreateFirstRoundDealPattern()
        {
            DealPattern result = new DealPattern();

            //result.items[(int)SpadesRoundResults.Positions.West] = new DealPatternItem("KHQH3H", "2H4H5H6H7H8H9H0HJHAHADACASKS");
            //result.items[(int)SpadesRoundResults.Positions.North] = new DealPatternItem("AH7H", null);
            //result.items[(int)SpadesRoundResults.Positions.East] = new DealPatternItem("JH9H5H", null);
            //result.items[(int)SpadesRoundResults.Positions.South] = new DealPatternItem("ACAS4H6H", null);

            result.items[(int)SpadesRoundResults.Positions.West] = new DealPatternItem("2HQH2D4D5D8DJC6C5C3C2S3S5S", null);
            result.items[(int)SpadesRoundResults.Positions.North] = new DealPatternItem("AHKH0HJH3D9DAC0C9C4CKS8S7S", null);
            result.items[(int)SpadesRoundResults.Positions.East] = new DealPatternItem("9H7H5H4H6D7DQDKDQC7C4S6SQS", null);
            result.items[(int)SpadesRoundResults.Positions.South] = new DealPatternItem("8H6H3H0DJDADKC8C2CAS9S0SJS", null);

            return result;
        }


        public int OverrideBid(SpadesTable table, SpadesActiveMatch aMatch, SpadesRoundResults.Positions pos, int bid)
        {
            SpadesUser user = table.Players[3].User;
            ISpadesMatch spadesMatch = aMatch.GetMatch();

            int totalMatches = (user.Stats as SpadesUserStats).TotalMatchesPlayed;
            int roundNum = aMatch.GetRoundsCount();

            if (totalMatches == 0)
            {
                if (roundNum == 0)
                {
                    if (pos == SpadesRoundResults.Positions.West)
                        return 0;
                    else if (pos == SpadesRoundResults.Positions.East)
                    {
                        return 6;
                    }
                }
            } else if (totalMatches < numGamesForRiggedHands) {
                // No nils for opponents
                if (spadesMatch.Playing_mode == PlayingMode.Partners) {
                    if (pos == SpadesRoundResults.Positions.West || pos == SpadesRoundResults.Positions.East) {
                        if (bid == 0)
                            return 1;
                    }
                } else {
                    if (bid == 0)
                        return 1;
                }
            }
            return bid;
        }

        public Card OverridePlay(SpadesTable table, SpadesActiveMatch aMatch, SpadesRoundResults.Positions pos,CardsList hand,Card card)
        {
            if (table.Players[0].Player_Type == Player.PlayerType.Bot &&
                table.Players[1].Player_Type == Player.PlayerType.Bot &&
                table.Players[2].Player_Type == Player.PlayerType.Bot)
            {
                SpadesUser user = table.Players[3].User;

                int totalMatches = (user.Stats as SpadesUserStats).TotalMatchesPlayed;
                int roundNum = aMatch.GetRoundsCount();

                if (totalMatches == 0)
                {
                    if (roundNum == 0)
                    {
                        if (pos == SpadesRoundResults.Positions.North)
                        {
                            if (hand.Count == 13)
                                return new Card("AC");
                            else if (hand.Count == 12)
                                return new Card("0H");
                            else if (hand.Count == 11)
                                return new Card("JH");

                        }

                    }
                }

            }
            return card;
        }


        public void OverrideDealWithPattern(SpadesTable table, ISpadesMatch match) {
            CardsList deck = match.Variant == PlayingVariant.Jokers ? CardsListBuilder.FullDeckWithJokers() : CardsListBuilder.FullDeckNoJokers();

            // Randomize a pattern
            int index = random.Next(5);
            CardsList playerHand = strongHandPatterns[index].BuildHand(deck);
            table.GetPlayer(3).SetCards(playerHand);

            // Remove the cards that were already dealt
            deck.RemoveSomeCards(playerHand);

            // Deal to other players
            random.Shuffle(deck);

            for (int i = 0; i < 3; i++)
            {
                table.GetPlayer(i).SetCards(deck.RemoveCardsFromEnd(13));
            }

        }


        class DealPattern
        {
            internal DealPatternItem[] items = new DealPatternItem[4];

            internal void SwitchCards(params CardsList[] hands)
            {
                for (int pos = 0; pos < 4; pos++)
                {
                    if (items[pos] != null)
                    {
                        CardsList curHand = hands[pos];
                        // Move must not haves to the beginning of the hand, so when removing cards from the hand, we first remove the
                        // must not haves
                        foreach (Card card in items[pos].mustNotHaves)
                        {
                            curHand.MoveCardToBeginning(card);
                        }


                        // Get must haves
                        foreach (Card card in items[pos].mustHaves)
                        {
                            foreach (CardsList hand in hands)
                            {
                                if (hand.Remove(card))
                                {
                                    curHand.Add(card);

                                    if (curHand.Count > 13)
                                    {
                                        Card cardToRemove = curHand[0];
                                        curHand.RemoveAt(0);
                                        hand.Add(cardToRemove);
                                    }
                                }
                            }
                        }

                        // Make sure we do not have must not haves left
                        foreach (Card card in items[pos].mustNotHaves)
                        {
                            if (curHand.Remove(card))
                            {
                                // Switch cards with the next hand index
                                CardsList hand = hands[(pos + 1) % 4];
                                curHand.Add(hand[0]);
                                hand.RemoveAt(0);
                                hand.Add(card);
                            }
                        }
                    }
                }
            }



        }

        class DealPatternItem
        {
            internal CardsList mustHaves;
            internal CardsList mustNotHaves;

            internal DealPatternItem(string mustHaveStr, string mustNotHaveStr)
            {
                mustHaves = new CardsList(mustHaveStr);
                mustNotHaves = new CardsList(mustNotHaveStr);
            }
        }

        /// <summary>
        /// A pattern of cards that will represent a Hand
        /// For each suit, the pattern holds the length and the number of strong (Q,K,A) cards
        /// </summary>
        public class HandPattern {
            DealMaker dealMaker;
            HandPatternItem[] nonTrumpPatterns;
            HandPatternItem trumpPattern;

            public HandPattern(DealMaker dealMaker, 
                               int suit1Num, int suit1Strong,
                               int suit2Num, int suit2Strong,
                               int suit3Num, int suit3Strong,
                               int trumpNum, int trumpStrong)
            {
                this.dealMaker = dealMaker;
                nonTrumpPatterns = new HandPatternItem[3];
                nonTrumpPatterns[0] = new HandPatternItem(suit1Num, suit1Strong);
                nonTrumpPatterns[1] = new HandPatternItem(suit2Num, suit2Strong);
                nonTrumpPatterns[2] = new HandPatternItem(suit3Num, suit3Strong);
                trumpPattern = new HandPatternItem(trumpNum, trumpStrong);
            }

            public CardsList BuildHand(CardsList deck) {
                CardsList result = new CardsList();

                System.Random random = new System.Random();

                // Trump suit
                CardsList cardsBySuit = deck.CardsBySuits(Card.SuitType.Spades);
                cardsBySuit.SortHighToLowRankFirst();

                // Strong cards
                CardsList strongCards = cardsBySuit.RemoveCardsFromBeginning(dealMaker.numOfStrongCards);
                random.Shuffle(strongCards);
                result.AddRange(strongCards.RemoveCardsFromBeginning(trumpPattern.NumStrongCards));

                // Weak cards
                random.Shuffle(cardsBySuit);
                result.AddRange(cardsBySuit.RemoveCardsFromBeginning(trumpPattern.NumWeakCards));


                // Randomize the suit order for the non trump suit
                List<Card.SuitType> suits = new List<Card.SuitType> { Card.SuitType.Clubs, Card.SuitType.Diamonds, Card.SuitType.Hearts };
                random.Shuffle(suits);

                // Non trump suits
                for (int i = 0; i < 3; i++)
                {
                    cardsBySuit = deck.CardsBySuits(suits[i]);
                    cardsBySuit.SortHighToLowRankFirst();

                    // Strong cards
                    strongCards = cardsBySuit.RemoveCardsFromBeginning(dealMaker.numOfStrongCards);
                    random.Shuffle(strongCards);
                    result.AddRange(strongCards.RemoveCardsFromBeginning(nonTrumpPatterns[i].NumStrongCards));

                    // Weak cards
                    random.Shuffle(cardsBySuit);
                    result.AddRange(cardsBySuit.RemoveCardsFromBeginning(nonTrumpPatterns[i].NumWeakCards));
                }

               
                return result;
            }

        }

        class HandPatternItem
        {
            public int NumCards { get; private set; }
            public int NumStrongCards { get; private set; }
            public int NumWeakCards {
                get {
                    return NumCards - NumStrongCards;
                }
            }
            public HandPatternItem(int numCards, int numStrongCards)
            {
                this.NumCards = numCards;
                this.NumStrongCards = numStrongCards;

                if (numStrongCards>3)
                    throw new ArgumentOutOfRangeException(string.Format("HandPatternItem: strong cards ({0}) is too large", numStrongCards));
                if (numCards < numStrongCards)
                    throw new ArgumentOutOfRangeException(string.Format("HandPatternItem: num cards ({0}) is smaller than strong cards ({1})", numCards, numStrongCards));
            }
        }     
    }
}
