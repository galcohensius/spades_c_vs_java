﻿using spades.controllers;

namespace spades.ims
{
    public class SpadesIMSPromoRoomInjector : CardGamesIMSPromoRoomInjector
    {
        protected override void EndSwipe()
        {
#if !UNITY_WEBGL
            LobbyController.Instance.Lobby_view.GetSwipe().EndSwipe();
#endif
        }

        protected override void ExecuteAction()
        {
#if !UNITY_WEBGL
            LobbyController.Instance.Lobby_view.GetSwipe().StartSwipe(() => { Clicked(); });
#else
                Clicked();
#endif
        }

        protected override void StartSwipe()
        {
#if !UNITY_WEBGL
            LobbyController.Instance.Lobby_view.GetSwipe().StartSwipe();
#else
//                    Clicked();
#endif
        }
    }
}