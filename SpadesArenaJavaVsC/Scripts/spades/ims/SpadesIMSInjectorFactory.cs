﻿namespace spades.ims
{
    public class SpadesIMSInjectorFactory : IMSInjectorFactory
    {
        public static new SpadesIMSInjectorFactory Instance;

        protected override void Awake()
        {
            base.Awake();
            if (Instance == null)
            {
                Instance = this;
            }
        }
        // from IMSPopup
        [SerializeField] protected GameObject m_voucher_prefab;

        // from IMSOpenTablePopup 
        [SerializeField] protected GameObject m_hat1_prefab;
        [SerializeField] protected GameObject m_hat2_prefab;
        [SerializeField] protected GameObject m_hat3_prefab;


        // from IMSPopup
        public GameObject voucher_prefab { get { return m_voucher_prefab; } set { m_voucher_prefab = value; } }

        // from IMSOpenTablePopup 
        public GameObject hat1_prefab { get { return m_hat1_prefab; } set { m_hat1_prefab = value; } }
        public GameObject hat2_prefab { get { return m_hat2_prefab; } set { m_hat2_prefab = value; } }
        public GameObject hat3_prefab { get { return m_hat3_prefab; } set { m_hat3_prefab = value; } }


        public override IMSBannerInjector CreateByTemplate (string templateName)
        {
            IMSBannerInjector bannereInjector = base.CreateByTemplate(templateName);

            if (bannereInjector == null)
            {
                switch (templateName)
                {
                    case ("POT"): bannereInjector = new IMSOTPromoRoomInjector(); break;
                    case ("PBN"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                }
            }
            return bannereInjector;
        }

        public override IMSBannerInjector CreateByLocation(string locationName)
        {
            IMSBannerInjector bannereInjector = base.CreateByLocation(locationName);

            if (bannereInjector == null)
            {
                switch (locationName)
                {
                    case ("R11"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R12"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R13"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R14"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R21"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R22"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R23"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                    case ("R24"): bannereInjector = new SpadesIMSPromoRoomInjector(); break;
                }
            }
            return bannereInjector;
        }

    }
}