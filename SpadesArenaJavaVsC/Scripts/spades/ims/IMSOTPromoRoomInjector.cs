﻿using System.Collections;
using System.Collections.Generic;
using spades.controllers;
using System;
using spades.models;

namespace spades.ims
{
    public class IMSOTPromoRoomInjector : SpadesIMSPromoRoomInjector
    {


        TMP_Text m_rewardAmountText;
        Image m_icon;

        // from IMSOpenTablePopup
        protected IMSPODynamicContent m_dynamicContentPO;
        protected Transform m_selectedPanel = null;
        protected Button m_close_button;
        protected GameObject m_loadedObj = null; // the loaded prefab - comes from the bundle
        protected GameObject m_prefabInstance = null; // The prefab instance
        protected bool m_isShown = false;
        protected bool m_loadingFailed = false;

        protected SpadesIMSInjectorFactory spadesInjectorFactory;

        List<SpadesSingleMatch> m_spadesSingleMatches;
        SpadesContestsController m_controller;
        List<VouchersGroup> m_vouchers;
        List<GameObject> m_voucherGOs;

        public IMSOTPromoRoomInjector()
        {
            m_controller = SpadesContestsController.Instance;
            m_spadesSingleMatches = new List<SpadesSingleMatch>();
            m_vouchers = new List<VouchersGroup>();
            m_voucherGOs = new List<GameObject>();
        }


        private PromoRoomItemView FindPromoRoomItemView(Transform curObject)
        {
            PromoRoomItemView promoRoomItemView = curObject.GetComponent<PromoRoomItemView>();

            if (promoRoomItemView != null)
            {
                return promoRoomItemView;
            }
            else
            {
                if (curObject.parent != null)
                    return FindPromoRoomItemView(curObject.parent);
                else
                    return null;
            }
        }


        protected override void BannerInject()
        {
            spadesInjectorFactory = (SpadesIMSInjectorFactory)IMSInjectorFactory.Instance;

            if (spadesInjectorFactory != null)
                InstantiatePrefab();
        }

        private void InstantiatePrefab()
        {
            IMSDynamicContent dynamicContent = m_iZone.DynamicContent as IMSDynamicContent;

            Transform contests = m_loaded_asset.transform.FindDeepChild("Contests");

            // clear hats if enabled
            // should be deleted on the updated bundle
            if (contests != null)
            {
                for (int i = 0; i < contests.childCount; i++)
                {
                    contests.GetChild(i).gameObject.SetActive(false);
                }
            }

            List<Transform> voucherPlaceholder = m_loaded_asset.transform.FindDeepChildren("VoucherPlaceholder");
            List<Transform> dummyVoucher = m_loaded_asset.transform.FindDeepChildren("DummyVoucher");

            for (int i = 0; i < m_iZone.DynamicContent.Content.Count; i++)
            {
                int index = i + 1;
                m_spadesSingleMatches.Add(SpadesCommManager.Instance.GetSpadesModelParser().ParseSingleMatch(dynamicContent.Content[i], null));
                // Handeling voucheres data
                VouchersGroup voucher = SpadesVoucherController.Instance.GetVouchersBySpadesMatch(m_spadesSingleMatches[i]);
                dummyVoucher[i].GetComponent<Image>().enabled = false;
                if (voucher != null)
                {
                    voucherPlaceholder[i].gameObject.SetActive(true);
                    m_voucherGOs.Add(GameObject.Instantiate(spadesInjectorFactory.voucher_prefab, voucherPlaceholder[i]));
                    m_voucherGOs[m_voucherGOs.Count - 1].GetComponent<VoucherView>().SetData(voucher, false);

                    m_vouchers.Add(voucher);

                    voucher.OnVoucherUpdate -= UpdateVouchers;
                    voucher.OnVoucherUpdate += UpdateVouchers;
                }

                Transform item = m_loaded_asset.transform.FindDeepChild("Item_" + index);

                if (item != null)
                {
                    if (m_spadesSingleMatches[i].Coins4PointsFactor == 0)
                    {
                        Transform c4p = item.FindDeepChild("C4P");
                        if (c4p != null)
                            c4p.gameObject.SetActive(false);
                    }
                    else
                    {
                        Transform c4p = item.FindDeepChild("C4P");
                        if (c4p != null)
                            c4p.gameObject.SetActive(true);

                        Transform c4pFactor = item.FindDeepChild("C4PFactor");
                        if (c4pFactor != null)
                            c4pFactor.GetComponent<TMP_Text>().text = "<sprite=1>" + FormatUtils.FormatBuyIn(m_spadesSingleMatches[i].Coins4PointsFactor);
                    }
                }
            }

            SetContestsHats();

            List<Transform> missions = m_loaded_asset.transform.FindDeepChildren("Mission");
            List<string> contestsOn = new List<string>();

            for (int i = 0; i < m_iZone.DynamicContent.Content.Count; i++)
            {
                contestsOn.Add(string.Empty);
                // button init
                Transform btn = m_loaded_asset.transform.FindDeepChild("Button_" + (i + 1));
                if (btn != null)
                {
                    int index = i;
                    // Use pointer down to support external URL actions
                    btn.gameObject.AddComponent<PointerDownHandler>();
                    btn.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                    btn.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                    {
#if !UNITY_WEBGL
                        LobbyController.Instance.Lobby_view.GetSwipe().StartSwipe(() => { Clicked(index); });
#else
                        Clicked(index);
#endif
                    });

                    btn.gameObject.AddComponent<PointerUpHandler>();
                    btn.gameObject.GetComponent<PointerUpHandler>().OnUp.RemoveAllListeners();
                    btn.gameObject.GetComponent<PointerUpHandler>().OnUp.AddListener(() =>
                    {
#if !UNITY_WEBGL
                        LobbyController.Instance.Lobby_view.GetSwipe().EndSwipe();
#endif

                    });

                    btn.gameObject.AddComponent<ButtonsOverView>();
                }

                // Bet Amount
                List<Transform> betAmount = m_loaded_asset.transform.FindDeepChildren("BetAmount");
                if (betAmount[i] != null)
                {
                    betAmount[i].GetComponent<TMP_Text>().text = "<sprite=1>" + FormatUtils.FormatBuyIn(m_spadesSingleMatches[i].Buy_in);
                }

                List<Transform> highLow = m_loaded_asset.transform.FindDeepChildren("HighLowText");
                if (highLow[i] != null)
                {
                    highLow[i].GetComponent<TMP_Text>().text = m_spadesSingleMatches[i].LowPoints + "/" + m_spadesSingleMatches[i].HighPoints;
                }

                List<Transform> penaltyAmount = m_loaded_asset.transform.FindDeepChildren("PenaltyAmount");
                if (penaltyAmount[i] != null)
                {
                    penaltyAmount[i].GetComponent<TMP_Text>().text = m_spadesSingleMatches[i].BagsPanelty + " pt.";
                }

                List<Transform> numBags = m_loaded_asset.transform.FindDeepChildren("NumBags");
                if (numBags[i] != null)
                {
                    numBags[i].GetComponent<TMP_Text>().text = m_spadesSingleMatches[i].BagsCount.ToString();
                }

                // if challenge is the same as this table
                Challenge spadesChallenge = SpadesMissionController.Instance.GetChallengeBySpadesMatch(m_spadesSingleMatches[i]);

                if (spadesChallenge == null)
                {
                    missions[i].gameObject.SetActive(false);
                }
                else
                {
                    missions[i].gameObject.SetActive(true);
                }
            }
        }

        public void UpdateVouchers()
        {
            if (this == null)
                return;
     
            for (int i = 0; i < m_voucherGOs.Count; i++)
            {
                if (m_voucherGOs[i] == null) continue; // check if not already destroyed

                m_voucherGOs[i].GetComponent<VoucherView>().SetData(m_vouchers[i], false);
                if (m_vouchers[i].Vouchers.Count == 0)
                    m_voucherGOs[i].gameObject.SetActive(false);
                else
                    m_voucherGOs[i].gameObject.SetActive(true);
            }
     
        }


        private void SetContestsHats()
        {
            Contest contest;
            List<bool> occupiedPlaces = new List<bool>();
            occupiedPlaces.Add(false);
            occupiedPlaces.Add(false);
            occupiedPlaces.Add(false);

            if (m_spadesSingleMatches.Count == 3)
            {
                contest = m_controller.GetContestForMatchList(m_spadesSingleMatches);
                if (contest != null)
                {
                    // prefab of the 3 hats on all 3 matches
                    // Placeholder 1-3
                    InitContestHatPrefab(spadesInjectorFactory.hat3_prefab, "1-3", contest);
                    occupiedPlaces[0] = true;
                    occupiedPlaces[1] = true;
                    occupiedPlaces[2] = true;
                    return;
                }
                contest = m_controller.GetContestForMatchList(m_spadesSingleMatches.GetRange(1, 2));
                if (contest != null)
                {
                    // prefab of the 2 hats on the right
                    // Placeholder 2-3
                    InitContestHatPrefab(spadesInjectorFactory.hat2_prefab, "2-3", contest);
                    occupiedPlaces[1] = true;
                    occupiedPlaces[2] = true;
                }
            }

            if (m_spadesSingleMatches.Count >= 2)
            {
                contest = m_controller.GetContestForMatchList(m_spadesSingleMatches.GetRange(0, 2));
                if (contest != null && !occupiedPlaces[0] && !occupiedPlaces[1])
                {
                    // prefab of the 2 hats on left
                    // Placeholder 1-2
                    InitContestHatPrefab(spadesInjectorFactory.hat2_prefab, "1-2", contest);
                    occupiedPlaces[0] = true;
                    occupiedPlaces[1] = true;
                }
            }

            if (m_spadesSingleMatches.Count > 0)
            {
                contest = m_controller.GetContestForMatch(m_spadesSingleMatches[0]);
                if (contest != null && !occupiedPlaces[0])
                {
                    // prefab of the 1 hat on the left
                    // Placeholder 1
                    InitContestHatPrefab(spadesInjectorFactory.hat1_prefab, "1", contest);
                    occupiedPlaces[0] = true;
                }
            }

            if (m_spadesSingleMatches.Count >= 2)
            {
                contest = m_controller.GetContestForMatch(m_spadesSingleMatches[1]);
                if (contest != null && !occupiedPlaces[1])
                {
                    // prefab of the 1 hat on the center
                    // Placeholder 2
                    InitContestHatPrefab(spadesInjectorFactory.hat1_prefab, "2", contest);
                    occupiedPlaces[2] = true;
                }
            }

            if (m_spadesSingleMatches.Count >= 3)
            {
                contest = m_controller.GetContestForMatch(m_spadesSingleMatches[2]);
                if (contest != null && !occupiedPlaces[2])
                {
                    // prefab of the 1 hat on the right
                    // Placeholder 3
                    InitContestHatPrefab(spadesInjectorFactory.hat1_prefab, "3", contest);
                    occupiedPlaces[2] = true;

                }
            }
        }

        private void InitContestHatPrefab(GameObject prefab, string range, Contest contest)
        {
            Transform container = m_loaded_asset.transform.FindDeepChild(range);
            if (container == null)
                return;

            container.gameObject.SetActive(true);

            foreach (Transform trns in container)
                trns.gameObject.SetActive(false);

            GameObject go = GameObject.Instantiate(prefab, container);
            Transform child = go.transform.GetChild(0);
            child.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

            go.GetComponent<ContestHatView>().InitAndLink(contest);
        }

        private IEnumerator RunTimer(TimeSpan ts, TMP_Text timer)
        {
            float startTime = Time.realtimeSinceStartup;

            while (ts.TotalSeconds >= 0)
            {
                timer.text = DateUtils.TimespanToDateMission(ts);
                yield return new WaitForSecondsRealtime(1f);
                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }
        }

        IEnumerator CountToMissionExpire(Mission mission)
        {
            TimeSpan time_to_wait = mission.End_date - DateTime.Now;

            yield return new WaitForSecondsRealtime((float)time_to_wait.TotalSeconds);

            // once the mission expired, close the popup
            //CloseButtonClicked();
        }
    }
}
