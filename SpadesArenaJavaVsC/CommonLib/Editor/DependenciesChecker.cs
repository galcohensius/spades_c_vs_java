﻿using System.IO;
using System.Reflection;

[InitializeOnLoad]
public static class DependenciesChecker 
{

    private const string CARDGAMES_NS = "cardGames";
    private const string SPADES_NS = "spades";
    private const string RUMMY_NS = "rummy";

    private static bool foundProblems = false;

    [MenuItem("Tools/Check Modules Dependencies", false, 100)]
    public static void CheckDependencies()
    {
        foundProblems = false;
        ClearLog();

        CheckCommonDependencies();
        CheckCardGamesDependencies();

        if (foundProblems)
        {
            EditorUtility.DisplayDialog("Dependencies Checker", "Found dependencies problems!\nCheck the editor log.", "Ok");
        } else
        {
            EditorUtility.DisplayDialog("Dependencies Checker", "No dependencies problems found.", "Ok");
        }
    }

    private static void CheckCommonDependencies()
    {

        string[] files = Directory.GetFiles(Directory.GetCurrentDirectory()
            + Path.DirectorySeparatorChar + "Assets"
            + Path.DirectorySeparatorChar + "CommonLib", "*.cs", SearchOption.AllDirectories);

        float totalFiles = files.Length;

        EditorUtility.DisplayProgressBar("Dependencies Checker", "Checking dependecies in CommonLib...",0);

        for (int i = 0; i < totalFiles; i++)
        {
            EditorUtility.DisplayProgressBar("Dependencies Checker", "Checking dependecies in CommonLib...", i/totalFiles);
            CheckSourceFileForDependencies(files[i],CARDGAMES_NS,SPADES_NS,RUMMY_NS);

        }
        EditorUtility.ClearProgressBar();

    }

    private static void CheckCardGamesDependencies()
    {

        string[] files = Directory.GetFiles(Directory.GetCurrentDirectory()
            + Path.DirectorySeparatorChar + "Assets"
            + Path.DirectorySeparatorChar + "CardGamesLib", "*.cs", SearchOption.AllDirectories);

        float totalFiles = files.Length;

        EditorUtility.DisplayProgressBar("Dependencies Checker", "Checking dependecies in CardGamesLib...", 0);

        for (int i = 0; i < totalFiles; i++)
        {
            EditorUtility.DisplayProgressBar("Dependencies Checker", "Checking dependecies in CardGamesLib...", i / totalFiles);
            CheckSourceFileForDependencies(files[i], SPADES_NS, RUMMY_NS);

        }
        EditorUtility.ClearProgressBar();

    }

    private static void CheckSourceFileForDependencies(string path,params string[] namespaces)
    {
        string source = File.ReadAllText(path);

        foreach (var ns in namespaces)
        {
            if (source.IndexOf("using "+ ns) >0)
            {
                Debug.Log("Found 'using " + ns + "' in file: " + path);
                foundProblems = true;
            }

            if (source.IndexOf("namespace " + ns) > 0)
            {
                Debug.Log("Found 'namespace " + ns + "' in file: " + path);
                foundProblems = true;
            }

        }

    }

    public static void ClearLog()
    {
        var assembly = Assembly.GetAssembly(typeof(UnityEditor.Editor));
        var type = assembly.GetType("UnityEditor.LogEntries");
        var method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }

}
