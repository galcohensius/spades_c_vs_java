﻿namespace UnityEngine.UI
{
    [CustomEditor(typeof(SliderExtremeSelectButton))]
    public class SelectableButtonEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SliderExtremeSelectButton targetMenuButton = (SliderExtremeSelectButton)target;
            DrawDefaultInspector();

        }
    }
}