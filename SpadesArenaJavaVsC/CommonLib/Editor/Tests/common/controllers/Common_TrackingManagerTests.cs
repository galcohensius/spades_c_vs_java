﻿using System.Collections.Generic;
using common.controllers;
using System.Linq;

namespace Tests.common.controllers
{
    [TestFixture]
    public class Common_AppsFlyerTrackingTests 
    {

        [OneTimeSetUp]
        public void Init()
        {
            StateController.Instance = new StateController();
        }

        [TestCase("FriendsJoined", 0, 1, new int[] { 3, 5, 10 }, new string[] { }, TestName = "1 joined friends, no previous event, no previews friends")]
        [TestCase("FriendsJoined", 0, 5, new int[] { 3, 5, 10 }, new string[] { "FriendsJoined3", "FriendsJoined5" }, TestName = "5 joined friends, no previews friends")]
        [TestCase("FriendsJoined", 0, 10, new int[] { 3, 5, 10 }, new string[] { "FriendsJoined3", "FriendsJoined5", "FriendsJoined10" }, TestName = "11 joined friends,  no previews friends")]
        [TestCase("FriendsJoined", 5, 5, new int[] { 3, 5, 10 }, new string[] { "FriendsJoined10" }, TestName = "5 joined friends, 5 previous friends")]
        [TestCase("FriendsJoined", 14, 2, new int[] { 3, 5, 10 }, new string[] { }, TestName = "5 joined friends, 5 previous friends")]
        public void IncrementalAppsFlyerEvents(string eventName, int currentValue, int newValue, int[] eventMilestones, string[] expectedResults)
        {
            TrackingManager.AppsFlyerEvent appsFlyerEvent = new TrackingManager.AppsFlyerEvent(eventName);
            StateController.Instance.StoreUserIntValue(eventName, currentValue, false);
            List<string> expectedResultsList = new List<string>(expectedResults);

            List<string> eventList = appsFlyerEvent.CreateIncrementalEvents(eventMilestones, newValue);
            int eventSent = eventList.Count();
           
            Assert.AreEqual(eventList, expectedResultsList);
        }

        [TestCase("PlayRuby", 0, 1, TestName = "Played Rubby for the first time")]
        [TestCase("PlayRuby", 1, 0, TestName = "Played Rubby again")]
        public void OneTimeAppsFlyerEvent(string eventName, int currentValue, int expectedResult)
        {
            TrackingManager.AppsFlyerEvent appsFlyerEvent = new TrackingManager.AppsFlyerEvent(eventName);
            StateController.Instance.StoreUserIntValue(eventName, currentValue, false);

            List<string> eventList = appsFlyerEvent.CreateOneTimeEvent();
            int eventSent = eventList.Count;

            Assert.AreEqual(eventSent, expectedResult);
        }


        [OneTimeTearDown]
        public void CleanUp()
        {

        }
    }
}
