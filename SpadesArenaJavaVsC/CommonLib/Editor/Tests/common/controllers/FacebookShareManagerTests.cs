﻿using System.IO;

namespace Tests.common.controllers
{
    public class FacebookShareManagerTests
    {

        private string ReadGoodJSON()
        {
            string path = Application.dataPath + "/CommonLib/Editor/Tests/common/controllers/test files/FacebookShare.json";
            string result = File.ReadAllText(path);
            if (string.IsNullOrEmpty(result))
                Debug.LogError("could not read FacebookShare.json!!!");
            return result;
        }

        private string ReadBadJSON()
        {
            string path = Application.dataPath + "/CommonLib/Editor/Tests/common/controllers/test files/FacedookShare.json";
            string result = File.ReadAllText(path);
            if (string.IsNullOrEmpty(result))
                Debug.LogError("could not read FacebookShare.json!!!");
            return result;
        }

        [Test]
        public void ReadStaticAchievementsData_Pass()
        {
            string jsonData = ReadGoodJSON();
            JSONArray root = JSON.Parse(jsonData).AsArray;

            Assert.DoesNotThrow(() => ProcessJson(root));
        }

        private void ProcessJson(JSONArray root)
        {
            foreach (JSONNode node in root)
            {
                string id = node["id"].Value;
                string title = node["title"].Value;
                string desc = node["description"].Value;
                string og = node["og_action"].Value;

                if (id == "" || title == "" || desc == "" || og == "")
                    Debug.LogWarning("one of the keys has no value. the key is wrong or the data is incomoplete");
            }
        }

        [Test]
        public void ReadStaticAchievementsData_Fail()
        {
            string jsonData = ReadBadJSON();
            JSONArray root = JSON.Parse(jsonData).AsArray;

            LogAssert.Expect(LogType.Warning, "one of the keys has no value. the key is wrong or the data is incomoplete");
            ProcessJson(root);
        }

        //no idea what to do with Share method
        //-shay
    }
}
