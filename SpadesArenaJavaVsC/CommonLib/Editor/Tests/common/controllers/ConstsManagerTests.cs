﻿using common.controllers;

namespace Tests.common.controllers
{
    public class ConstsManagerTests
    {
        // A Test behaves as an ordinary method
        [TestCase("this")]
        [TestCase("777")]
        [TestCase("is")]
        [TestCase("-98.76")]
        [TestCase("a")]
        [TestCase("string")]
        [TestCase("")]
        public void ConstsManager_GetStringPass(string s)
        {
            ConstsManager.Const c = new ConstsManager.Const(s);
            Assert.AreEqual(s, (string)c);
        }

        [TestCase(-696)]
        [TestCase(int.MinValue)]
        [TestCase(0)]
        [TestCase(8888)]
        [TestCase(int.MaxValue)]
        public void ConstsManager_GetIntPass(int i)
        {
            ConstsManager.Const c = new ConstsManager.Const(i.ToString());
            Assert.AreEqual(i, (int)c);
        }


        [TestCase("this")]
        [TestCase("7-7-7")]
        [TestCase("is")]
        [TestCase("-9$8.76")]
        [TestCase("a")]
        [TestCase("string")]
        [TestCase("")]
        public void ConstsManager_GetIntFail(string i)
        {
            ConstsManager.Const c = new ConstsManager.Const(i);
            Assert.AreEqual(0, (int)c);
        }


        [TestCase(-36f)]
        [TestCase(-0.0f)]
        [TestCase(float.NaN)]
        [TestCase(float.NegativeInfinity)]
        [TestCase(float.PositiveInfinity)]
        [TestCase(1234.567f)]
        public void ConstsManager_GetFloatPass(float f)
        {
            ConstsManager.Const c = new ConstsManager.Const(f.ToString());
            Assert.AreEqual(f, (float)c);
        }
        [Test]
        //this actually fails for some reason, probably due to parsing underflow going from fringe
        //float value to string and then convert it back and also for out of range in the case of 1234.56789
        /* 
        [TestCase(float.MaxValue)] 
        [TestCase(float.MinValue)] 
        [TestCase(1234.56789f)]
        */
        [TestCase("this")]
        [TestCase("7-7-7")]
        [TestCase("is")]
        [TestCase("-9$8.76f")]
        [TestCase("a")]
        [TestCase("string")]
        [TestCase("")]
        public void ConstsManager_GetFloatFail(string f)
        {
            ConstsManager.Const c = new ConstsManager.Const(f);
            Assert.AreEqual(0, (float)c);
        }

    }
}
