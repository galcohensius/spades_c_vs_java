﻿using common.comm;

namespace Tests.common.comm
{
    public class DecoderTests 
    {
        [Test]
        public void LargeMessageTest()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            for (int i = 0; i < 1200; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    sb.Append(j.ToString());
                }
            }

            Debug.Log(sb.ToString());

            string text = ((TextAsset)Resources.Load("long_c5_bad")).text;

            Decoder decoder = new Decoder('{', '}', ',');

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);

            MMessage msg = decoder.Decode(bytes);




        }
    }
}
