﻿using System.Collections.Generic;
using common.comm;
using System.Text;

namespace Tests.common.comm
{
    public class CompositeParamTests
    {
        [TestFixture]
        public class SimpleParamsTest
        {

            // A Test behaves as an ordinary method
            [Test]
            public void CompositeParam_AddValueParams_SimplePass()
            {
                CompositeParam compositeParam = new CompositeParam();

                compositeParam.AddValueParam("first param");
                compositeParam.AddValueParam("second param");
                compositeParam.AddValueParam("third param");

                LogAssert.NoUnexpectedReceived();

                List<Param> storedParams = compositeParam.Params;

                Assert.AreEqual(3, storedParams.Count);

                Assert.AreEqual("", compositeParam.Value);

                compositeParam.AddValueParam("forth param");
                compositeParam.AddValueParam("fifth param");

                Assert.AreEqual("{first param,second param,third param,forth param,fifth param}", compositeParam.ToString());
            }

            [Test]
            public void CompositeParam_AddSimpleParam_SimplePass()
            {
                CompositeParam compositeParam = new CompositeParam();

                SimpleParam stringParam = new SimpleParam("string param");
                SimpleParam intParam = new SimpleParam((int)4756);
                SimpleParam longParam = new SimpleParam(9999L);

                compositeParam.AddParam(stringParam);
                compositeParam.AddParam(intParam);
                compositeParam.AddParam(longParam);

                LogAssert.NoUnexpectedReceived();

                List<Param> storedParams = compositeParam.Params;

                Assert.AreEqual(3, storedParams.Count);

                Assert.AreEqual("", compositeParam.Value);

                compositeParam.AddParam(new SimpleParam(5));
                compositeParam.AddParam(new SimpleParam("gib money"));

                Assert.AreEqual("{string param,4756,9999,5,gib money}", compositeParam.ToString());
            }

            [Test]
            public void CompositeParam_SetParams_SimplePass()
            {
                List<Param> _params = new List<Param>();
                CompositeParam compositeParam = new CompositeParam();

                _params.Add(new SimpleParam("string param"));
                _params.Add(new SimpleParam((int)4756));
                _params.Add(new SimpleParam(9999L));

                LogAssert.NoUnexpectedReceived();

                compositeParam.Params = _params;
                List<Param> storedParams = compositeParam.Params;

                Assert.AreEqual(3, storedParams.Count);

                Assert.AreEqual("", compositeParam.Value);

                compositeParam.AddParam(new SimpleParam(5));
                compositeParam.AddParam(new SimpleParam("gib money"));

                Assert.AreEqual("{string param,4756,9999,5,gib money}", compositeParam.ToString());
            }

            [Test]
            public void CompositeParam_SetParamsInternal_SimplePass()
            {
                CompositeParam compositeParamOriginal = new CompositeParam();
                CompositeParam compositeParamInternal = new CompositeParam();

                compositeParamOriginal.AddValueParam("string param");
                compositeParamOriginal.AddParam(new SimpleParam((int)4756));
                compositeParamOriginal.AddParam(new SimpleParam(9999L));

                LogAssert.NoUnexpectedReceived();

                compositeParamInternal.SetParamsInternal(compositeParamOriginal);
                List<Param> storedParams = compositeParamInternal.Params;

                Assert.AreEqual(3, storedParams.Count);

                Assert.AreEqual("", compositeParamInternal.Value);

                compositeParamInternal.AddParam(new SimpleParam(5));
                compositeParamInternal.AddValueParam("gib money");

                Assert.AreEqual("{string param,4756,9999,5,gib money}", compositeParamInternal.ToString());
            }
        }

        [TestFixture]
        public class CompositeParamsTest
        {
            // A Test behaves as an ordinary method
            [Test]
            public void CompositeParam_AddValueParams_BuildFromThemNewComposite_Pass()
            {
                CompositeParam compositeParam1 = new CompositeParam();

                compositeParam1.AddValueParam("first param");
                compositeParam1.AddValueParam("second param");
                compositeParam1.AddValueParam("third param");
                compositeParam1.AddValueParam("forth param");
                compositeParam1.AddValueParam("fifth param");

                CompositeParam compositeParam2 = new CompositeParam();
                compositeParam2.SetParamsInternal(compositeParam1);

                CompositeParam superCompositeParam = new CompositeParam();
                superCompositeParam.AddParam(compositeParam1);
                superCompositeParam.AddValueParam("shay");
                superCompositeParam.AddValueParam("is");
                superCompositeParam.AddValueParam("horny");
                superCompositeParam.AddParam(compositeParam2);

                StringBuilder sb = new StringBuilder("{");
                sb.Append("{first param,second param,third param,forth param,fifth param},");
                sb.Append("shay,is,horny,");
                sb.Append("{first param,second param,third param,forth param,fifth param}}");

                LogAssert.NoUnexpectedReceived();
                List<Param> storedParams = superCompositeParam.Params;
                Assert.AreEqual(5, storedParams.Count);
                Assert.AreEqual("", superCompositeParam.Value);
                Assert.AreEqual(sb.ToString(), superCompositeParam.ToString());
            }

            [Test]
            public void CompositeParam_AddSimpleParam_BuildFromThemNewComposite_Pass()
            {
                CompositeParam compositeParam1 = new CompositeParam();

                SimpleParam stringParam = new SimpleParam("string param");
                SimpleParam intParam = new SimpleParam((int)4756);
                SimpleParam longParam = new SimpleParam(9999L);

                compositeParam1.AddParam(stringParam);
                compositeParam1.AddParam(intParam);
                compositeParam1.AddParam(longParam);


                CompositeParam compositeParam2 = new CompositeParam();

                compositeParam2.AddParam(new SimpleParam(5));
                compositeParam2.AddParam(new SimpleParam("gib money"));

                CompositeParam superComposite1 = new CompositeParam();
                superComposite1.AddParam(compositeParam1);
                superComposite1.AddParam(new SimpleParam("simple string"));
                superComposite1.AddParam(compositeParam2);


                CompositeParam superComposite2 = new CompositeParam();
                superComposite2.AddParam(compositeParam2);
                superComposite2.AddParam(new SimpleParam(0));
                superComposite2.AddParam(compositeParam1);

                CompositeParam ultraComposite = new CompositeParam();
                ultraComposite.AddParam(superComposite1);
                ultraComposite.AddParam(new SimpleParam(-700L));
                ultraComposite.AddParam(superComposite2);

                StringBuilder sb = new StringBuilder("{");
                sb.Append(superComposite1.ToString());
                sb.Append(",-700,");
                sb.Append(superComposite2.ToString());
                sb.Append("}");

                LogAssert.NoUnexpectedReceived();
                List<Param> storedParams = ultraComposite.Params;
                Assert.AreEqual(3, storedParams.Count);
                Assert.AreEqual("", ultraComposite.Value);
                Assert.AreEqual(sb.ToString(), ultraComposite.ToString());
            }

            [Test]
            public void CompositeParam_SetParams_ComplexPass()
            {
                List<Param> _params = new List<Param>();
                CompositeParam compositeParam1 = new CompositeParam();

                _params.Add(new SimpleParam("string param"));
                _params.Add(new SimpleParam((int)4756));
                _params.Add(new SimpleParam(9999L));
                compositeParam1.Params = _params;

                CompositeParam compositeParam2 = new CompositeParam();
                compositeParam2.AddParam(new SimpleParam(5));
                compositeParam2.AddParam(new SimpleParam("gib money"));
                compositeParam2.AddParam(compositeParam1);

                CompositeParam settedCP = new CompositeParam();
                settedCP.Params = compositeParam2.Params;

                LogAssert.NoUnexpectedReceived();
                List<Param> storedParams = settedCP.Params;
                Assert.AreEqual(3, storedParams.Count);
                Assert.AreEqual("", settedCP.Value);
                Assert.AreEqual("{5,gib money,{string param,4756,9999}}", settedCP.ToString());
            }

            [Test]
            public void CompositeParam_SetParamsInternal_ComplexPass()
            {
                List<Param> _params = new List<Param>();
                CompositeParam compositeParam1 = new CompositeParam();

                _params.Add(new SimpleParam("string param"));
                _params.Add(new SimpleParam((int)4756));
                _params.Add(new SimpleParam(9999L));

                LogAssert.NoUnexpectedReceived();

                compositeParam1.Params = _params;

                CompositeParam compositeParam2 = new CompositeParam();
                compositeParam2.AddParam(new SimpleParam(5));
                compositeParam2.AddParam(new SimpleParam("gib money"));
                compositeParam2.AddParam(compositeParam1);

                CompositeParam settedCP = new CompositeParam();
                settedCP.SetParamsInternal(compositeParam2);

                LogAssert.NoUnexpectedReceived();
                List<Param> storedParams = settedCP.Params;
                Assert.AreEqual(3, storedParams.Count);
                Assert.AreEqual("", settedCP.Value);
                Assert.AreEqual("{5,gib money,{string param,4756,9999}}", settedCP.ToString());
            }
        }
    }
}
