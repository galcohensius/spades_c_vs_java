﻿using common.comm;

namespace Tests.common.comm
{
    public class SimpleParamTests
    {
        // A Test behaves as an ordinary method
        [TestCase("string test")]
        [TestCase("")]
        [TestCase("5tr1ng 7es7")]
        [TestCase("-7")]
        public void SimpleParam_AssignString_AndGet_Test(string testString)
        {
            SimpleParam simpleParam = new SimpleParam(testString);
            Assert.AreEqual(testString, simpleParam.ToString());
            Assert.AreEqual(testString, simpleParam.Value);
        }

        [TestCase((int)9001.789)]
        [TestCase((int)0)]
        [TestCase(int.MaxValue)]
        [TestCase(int.MinValue)]
        [TestCase((int)-7000.75840)]
        public void SimpleParam_AssignInt_AndGet_Test(int testInt)
        {
            SimpleParam simpleParam = new SimpleParam(testInt);
            Assert.AreEqual(testInt.ToString(), simpleParam.ToString());
            Assert.AreEqual(testInt.ToString(), simpleParam.Value);
        }

        [TestCase((long)98001.789)]
        [TestCase((long)-0)]
        [TestCase(long.MaxValue)]
        [TestCase(long.MinValue)]
        [TestCase((long)-78000.75840)]
        public void SimpleParam_AssignLong_AndGet_Test(long testLong)
        {
            SimpleParam simpleParam = new SimpleParam(testLong);
            Assert.AreEqual(testLong.ToString(), simpleParam.ToString());
            Assert.AreEqual(testLong.ToString(), simpleParam.Value);
        }
    }
}
