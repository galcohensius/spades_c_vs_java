﻿using System.Collections.Generic;
using common.comm;

namespace Tests.common.comm
{
    public class MMessageTests
    {
        // A Test behaves as an ordinary method
        //due to "lack of content and all the methods are stright-forward these two tests cover everything without
        //breaking into smaller test
        [Test]
        public void MMessageTests_SimpleParams_Passes()
        {
            MMessage mMessage = new MMessage();
            mMessage.Code = "S1";
            List<Param> _params = new List<Param>();
            _params.Add(new SimpleParam(5));
            _params.Add(new SimpleParam("five"));
            _params.Add(new SimpleParam(555L));
            mMessage.Params = _params;
            LogAssert.NoUnexpectedReceived();
            Assert.AreEqual("[Message: code=S1, params=5,five,555]", mMessage.ToString());
        }

        [Test]
        public void MMessageTests_CompositeParams_Passes()
        {
            MMessage mMessage = new MMessage();
            mMessage.Code = "S1";

            List<Param> _params = new List<Param>();
            CompositeParam compositeParam1 = new CompositeParam();

            _params.Add(new SimpleParam("string param"));
            _params.Add(new SimpleParam((int)4756));
            _params.Add(new SimpleParam(9999L));
            compositeParam1.Params = _params;

            CompositeParam compositeParam2 = new CompositeParam();
            compositeParam2.AddParam(new SimpleParam(5));
            compositeParam2.AddParam(new SimpleParam("gib money"));
            compositeParam2.AddParam(compositeParam1);

            CompositeParam settedCP = new CompositeParam();
            settedCP.SetParamsInternal(compositeParam2);

            mMessage.Params = settedCP.Params;

            LogAssert.NoUnexpectedReceived();
            Assert.AreEqual("[Message: code=S1, params=5,gib money,{string param,4756,9999}]", mMessage.ToString());
        }
    }
}
