﻿using common.experiments;
using System;

namespace Tests.common.experiments
{
    [TestFixture]
    public class ExperimentsManagerTests
    {
        private Action PrintAction1;
        private Action PrintAction2;
        private Action PrintAction3;
        GameObject go;
        ExperimentsManager experimentsManager;

        [OneTimeSetUp]
        public void Init()
        {
            GameObject go = new GameObject();
            experimentsManager = go.AddComponent<ExperimentsManager>();
        }

        #region the actual tests

        // A Test behaves as an ordinary method
        [Test]
        public void ExperimentsManager_GetExperimentValues_Pass()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            Assert.AreEqual(1, experimentsManager.GetValue("exp1"));
            Assert.AreEqual(22, experimentsManager.GetValue("exp2"));
            Assert.AreEqual(44, experimentsManager.GetValue("exp4"));
            Assert.AreEqual(3, experimentsManager.GetValue("prms"));
        }

        [Test]
        public void ExperimentsManager_GetExperimentValues_Fail()
        {
            Assert.Throws<NullReferenceException>(()=> experimentsManager.GetValue("null"));
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));    
            Assert.AreEqual(1, experimentsManager.GetValue("exp3"));
        }

        [Test]
        public void ExperimentsManager_RunSingleAction_FromExperiments()
        { 
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();

            LogAssert.Expect(LogType.Log, "OK");
            experimentsManager.With("exp1", PrintAction1);
            UnregisterActions();
        }

        [Test]
        public void ExperimentsManager_RunSingleAction_Default()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();

            LogAssert.Expect(LogType.Log, "Error: j/k");
            experimentsManager.With("exp3", PrintAction3);
            UnregisterActions();
        }

        [Test]
        public void ExperimentsManager_RunSingleAction_ValueExceedsParams()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();

            LogAssert.Expect(LogType.Log, "EX2");
            Action[] _params = new Action[] { PrintAction2, PrintAction3, PrintAction1 };
            experimentsManager.With("exp2", PrintAction2, _params);
            UnregisterActions();
        }

        [Test]
        public void ExperimentsManager_RunSingleAction_ActionFromParams()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();

            LogAssert.Expect(LogType.Log, "OK");
            Action[] _params = new Action[] { PrintAction2, PrintAction1, PrintAction3, PrintAction1, PrintAction2, PrintAction3, PrintAction1 };
            experimentsManager.With("prms", PrintAction2, _params);
            LogAssert.Expect(LogType.Log, "EX2");
            experimentsManager.With("prmz", PrintAction2, _params);
            UnregisterActions();
        }

        [Test]
        public void ExperimentsManager_RunIfAction_Pass()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();

            LogAssert.Expect(LogType.Log, "OK");
            experimentsManager.RunIf("exp1", 1, PrintAction1);
            UnregisterActions();
        }

        [Test]
        public void ExperimentsManager_RunIfAction_Mismatch_expectedVal_Fail()
        {
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=exp1, value=1]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=exp2, value=22]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=exp4, value=44]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=prms, value=3]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=prmz, value=6]");

            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();
            
            experimentsManager.RunIf("exp1", -1, PrintAction1);
            UnregisterActions();
            LogAssert.NoUnexpectedReceived();
        }

        [Test]
        public void ExperimentsManager_RunIfAction_Mismatch_expId_Fail()
        {
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=exp1, value=1]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=exp2, value=22]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=exp4, value=44]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=prms, value=3]");
            LogAssert.Expect(LogType.Log, "Experiment loaded: [Experiment: id=prmz, value=6]");

            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();
            experimentsManager.RunIf("exp1", -1, PrintAction3);
            UnregisterActions();
            LogAssert.NoUnexpectedReceived();
        }

        [Test]
        public void ExperimentsManager_RunIfAction_Mismatch_expId_Default()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));
            RegisterActions();
            LogAssert.Expect(LogType.Log, "Error: j/k");
            experimentsManager.RunIf("1", 1, PrintAction3);
            UnregisterActions();
        }

        [Test]
        public void ExperimentsManager_OverrideValue_KeyExists()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));

            experimentsManager.OverrideValue("exp1", 10);
            Assert.AreEqual(10, experimentsManager.GetValue("exp1"));
        }

        [Test]
        public void ExperimentsManager_OverrideValue_NewKey()
        {
            Assert.DoesNotThrow(() => experimentsManager.LoadExperiments(CreateExperimentsJSON()));

            Assert.AreEqual(1, experimentsManager.GetValue("new"));
            experimentsManager.OverrideValue("new", 10);
            Assert.AreEqual(10, experimentsManager.GetValue("new"));
        }

        #endregion

        #region helper methods
        private JSONNode CreateExperimentsJSON()
        {
            JSONArray experiments = new JSONArray();

            JSONObject exp1 = new JSONObject();
            exp1["id"] = "exp1";
            exp1["vVe"] = "1";
            experiments.Add(exp1);

            JSONObject exp2 = new JSONObject();
            exp2["id"] = "exp2";
            exp2["vVe"] = "22";
            experiments.Add(exp2);

            JSONObject exp3 = new JSONObject();
            exp3["id"] = "exp4";
            exp3["vVe"] = "44";
            experiments.Add(exp3);

            JSONObject exp5 = new JSONObject();
            exp5["id"] = "prms";
            exp5["vVe"] = "3";
            experiments.Add(exp5);

            JSONObject exp6 = new JSONObject();
            exp6["id"] = "prmz";
            exp6["vVe"] = "6";
            experiments.Add(exp6);

            return experiments;
        }

        private void RegisterActions()
        {
            PrintAction1 += PrintOK;
            PrintAction2 += PrintEX2;
            PrintAction3 += PrintErrorJK;
        }

        private void PrintOK()
        {
            Debug.Log("OK");
        }

        private void PrintEX2()
        {
            Debug.Log("EX2");
        }

        private void PrintErrorJK()
        {
            Debug.Log("Error: j/k");
        }

        private void UnregisterActions()
        {
            PrintAction1 -= PrintOK;
            PrintAction2 -= PrintEX2;
            PrintAction3 -= PrintErrorJK;
        }

        #endregion

        [OneTimeTearDown]
        public void CleanUp()
        {
            UnityEngine.GameObject.DestroyImmediate(go);
        }
    }
}
