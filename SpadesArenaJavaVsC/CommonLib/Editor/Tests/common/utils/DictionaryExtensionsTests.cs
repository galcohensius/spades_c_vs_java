﻿using System.Collections.Generic;
using common.utils;

namespace Tests.common.utils
{
    public class DictionaryExtensionsTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void DictionaryExtensionsTestsSimplePasses()
        {
            // Use the Assert class to test conditions
            Dictionary<int, string> myDictionary = new Dictionary<int, string>();
            myDictionary.Add(0, "zero");
            myDictionary.Add(2, "two");
            myDictionary.Add(3, "three");
            myDictionary.Add(5, "five_original");

            Dictionary<int, string> mergeDictionary = new Dictionary<int, string>();
            mergeDictionary.Add(1, "one");
            mergeDictionary.Add(4, "four");
            mergeDictionary.Add(5, "five_new");

            Dictionary<int, string> assertDictionary = new Dictionary<int, string>();
            assertDictionary.Add(0, "zero");
            assertDictionary.Add(1, "one");
            assertDictionary.Add(2, "two");
            assertDictionary.Add(3, "three");
            assertDictionary.Add(4, "four");
            assertDictionary.Add(5, "five_new");

            myDictionary.Merge<int, string>(mergeDictionary);
            CollectionAssert.AreEquivalent(assertDictionary, myDictionary);

            assertDictionary[5] = "not five";
            CollectionAssert.AreNotEquivalent(assertDictionary, myDictionary);

            assertDictionary[5] = "five_new";
            assertDictionary.Add(6, "six");
            CollectionAssert.AreNotEquivalent(assertDictionary, myDictionary);
        }

    }
}
