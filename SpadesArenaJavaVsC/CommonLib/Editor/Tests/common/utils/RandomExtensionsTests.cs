﻿using System.Collections.Generic;
using common.utils;

namespace Tests.common.utils
{
    public class RandomExtensionsTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void RandomExtensions_RandomGaussian_Test()
        {
            //needs more tests for more inputs?
            System.Random r = new System.Random();
            Assert.DoesNotThrow(()=> r.NextGaussian());
        }

        // A Test behaves as an ordinary method
        [Test]
        public void RandomExtensions_RandomTriangular_Test()
        {
            //needs more tests for more inputs? o
            System.Random r = new System.Random();
            Assert.DoesNotThrow(() => r.NextTriangular(0,5,2)); //a < c < b
            Assert.Throws(typeof(System.Exception), () => r.NextTriangular(2, 0, 1)); //a > b
            Assert.Throws(typeof(System.Exception), ()=> r.NextTriangular(0, 5, -2)); //c < a
            Assert.Throws(typeof(System.Exception), () => r.NextTriangular(0, 5, 7)); // c > b
        }
        
        // A Test behaves as an ordinary method
        [Test]
        public void RandomExtensions_RandomBoolean_Test()
        {
            //needs more tests for more inputs?
            System.Random r = new System.Random();
            Assert.DoesNotThrow(() => r.NextBoolean());
        }

        // A Test behaves as an ordinary method
        [Test]
        public void RandomExtensions_RandomBoolean_WithProbability_Test()
        {
            //needs more tests for more inputs?
            System.Random r = new System.Random();
            Assert.Throws(typeof(System.Exception), () => r.NextBoolean(-1));
            Assert.DoesNotThrow(() => r.NextBoolean(0));
            Assert.DoesNotThrow(() => r.NextBoolean(0.5f));
            Assert.DoesNotThrow(() => r.NextBoolean(1));
            Assert.Throws(typeof(System.Exception), () => r.NextBoolean(2));
        }

        // A Test behaves as an ordinary method
        [Test]
        public void RandomExtensions_Shuffle_Test()
        {
            //needs more tests for more inputs?
            System.Random r = new System.Random();
            int[] array = new int[] { 3, 7, 5, 8, 0 };
            Assert.DoesNotThrow(() => r.Shuffle(array));

            List<int> emptylist = new List<int>();
            Assert.DoesNotThrow(() => r.Shuffle(emptylist));

            Assert.Throws(typeof(System.Exception), () => r.Shuffle(null));
        }

        // A Test behaves as an ordinary method
        [Test]
        public void RandomExtensions_RandomFromList_Test()
        {
            //needs more tests for more inputs?
            System.Random r = new System.Random();
            int[] array = new int[] { 3, 7, 5, 8, 0 };
            Assert.DoesNotThrow(() => r.RandomFromList(array));

            List<int> emptylist = new List<int>();
            Assert.IsEmpty(r.RandomFromList(emptylist));

            emptylist.Clear();
            emptylist = null;
            Assert.IsNull(r.RandomFromList(emptylist));
        }

    }
}
