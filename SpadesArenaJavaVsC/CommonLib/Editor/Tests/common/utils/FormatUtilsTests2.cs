﻿using System.Collections;
using System.Collections.Generic;
using common.utils;

namespace Tests.common.utils
{
    /// <summary>
    /// this is a working template of how one can asset againt data of inconsistant number of arguments and a single return value.
    /// for a version with fixed number of arguments: https://github.com/nunit/docs/wiki/TestCase-Attribute
    /// </summary>
    public class FormatUtilsTests2
    {
        [TestFixture]
        public class MyTests
        {
            [TestCaseSource(typeof(MyDataClass), "TestCases")]
            public string FormatListTest(List<object> list)
            {
                return FormatUtils.FormatList(list);
            }
        }

        public class MyDataClass
        {
            public static IEnumerable TestCases
            {
                get
                {
                    yield return new TestCaseData(new List<object> { 0, 2, 3 }).Returns("0,2,3");
                    yield return new TestCaseData(new List<object> { "1", "thanks", "<3" }).Returns("1,thanks,<3");
                    yield return new TestCaseData(new List<object> { 0 }).Returns("0");
                    yield return new TestCaseData(new List<object> { }).Returns("");
                }
            }
        }
    }
}
