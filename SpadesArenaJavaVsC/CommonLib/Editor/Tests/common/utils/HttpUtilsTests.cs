﻿using System.Collections.Generic;
using common.utils;
using System;

namespace Tests.common.utils
{
    public class HttpUtilsTests
    {
        [Test]
        public void ParseQueryParams_EmptyURI_Fail()
        {
            Assert.Throws(typeof(System.UriFormatException), () => HttpUtils.ParseQueryParams(new System.Uri("")));
        }

        [Test]
        public void ParseQueryParams_BadURI_Fail()
        {
            Assert.Throws<System.UriFormatException>(() => HttpUtils.ParseQueryParams(new System.Uri("ww.fail.co")));
        }

        [Test]
        public void ParseQueryParams_Pass()
        {
            string test_uri = "http://example.com/path/to/page?name=ferret&color=purple&shay=awesome";
            Uri uri = new Uri(test_uri);
            Dictionary<string, string> result = HttpUtils.ParseQueryParams(uri);

            Assert.IsNotNull(result);
            CollectionAssert.AllItemsAreNotNull(result);
            CollectionAssert.IsNotEmpty(result);
            Assert.True(result.Count == 3);
        }
    }
}
