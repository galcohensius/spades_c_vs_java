﻿using common.utils;

namespace Tests.common.utils
{
    public class TMPBidiHelperTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TMPBidiHelper_FlipString()
        {
            //long string
            string original = "תרוכשמב האלעה רניירק ישל ונת";
            string flipped = "תנו לשי קריינר העלאה במשכורת";

            string result = TMPBidiHelper.MakeRTL(original);
            Assert.AreEqual(flipped, result);

            //empty string
            result = TMPBidiHelper.MakeRTL("");
            Assert.AreEqual("", result);

            //single char string
            result = TMPBidiHelper.MakeRTL("ח");
            Assert.AreEqual("ח", result);
        }


        [Test]
        public void TMPBidiHelper_FlipTMP_Text()
        {
            GameObject textObject = new GameObject();
            TextMeshProUGUI textMeshProUGUI = textObject.AddComponent<TextMeshProUGUI>();

            //long string
            string rtlEnabledString = "תרוכשמב האלעה רניירק ישל ונת"; // ==  "תנו לשי קריינר העלאה במשכורת";
            textMeshProUGUI.text = rtlEnabledString;

            TMPBidiHelper.MakeRTL(textMeshProUGUI);
            Assert.AreEqual(rtlEnabledString, textMeshProUGUI.text);
            Assert.True(textMeshProUGUI.isRightToLeftText);

            //empty string
            textMeshProUGUI.text = "";
            TMPBidiHelper.MakeRTL(textMeshProUGUI);
            Assert.AreEqual("", textMeshProUGUI.text);

            //single char string
            textMeshProUGUI.text = "ח";
            TMPBidiHelper.MakeRTL(textMeshProUGUI);
            Assert.AreEqual("ח", textMeshProUGUI.text);
            Assert.False(textMeshProUGUI.isRightToLeftText);

            MonoBehaviour.DestroyImmediate(textObject);
        }


    }
}
