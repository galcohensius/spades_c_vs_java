﻿using System;
using System.Text;
using common.utils;

namespace Tests.common.utils
{
    public class DateUtilsTests
    {
        private const long SECONDS_IN_A_YEAR = 31557600;
        private const int SECONDS_IN_A_WEEK = 604800;
        private const int SECONDS_IN_A_DAY = 86400;
        private const int SECONDS_IN_A_HOUR = 3600;
        private const int SECONDS_IN_A_MINUTE = 60;
        //all the calculations at DateUtils are made against this DateTime.
        private readonly DateTime baseDate = new DateTime(1970, 1, 1);

        #region TotalSeconds

        // A Test behaves as an ordinary method
        [Test]
        public void TotalSeconds_ZeroTime_Test()
        {
            //zero time from the base
            long result = DateUtils.TotalSeconds(baseDate);
            Assert.Zero(result);
        }

        [Test]
        public void TotalSeconds_CurrentDate_Test()
        {
            //test against current date
            DateTime today = DateTime.Today;
            long diffInSeconds = (long)(today - baseDate).TotalSeconds;
            long result = DateUtils.TotalSeconds(today);
            Assert.True(result == diffInSeconds);
        }

        [Test]
        public void TotalSeconds_CurrentTime_Test()
        {
            //test against current time
            long diffInSeconds = (long)(DateTime.Now - baseDate).TotalSeconds;
            long result = DateUtils.TotalSeconds(DateTime.Now);
            Assert.True(result == diffInSeconds);
        }

        [Test]
        public void TotalSeconds_RandomTime_Test()
        {
            //test against random time            
            int range = (DateTime.Today - baseDate).Days;
            DateTime randomDate = baseDate;
            randomDate.AddDays(UnityEngine.Random.Range(0, range));
            randomDate.AddSeconds(UnityEngine.Random.Range(0, SECONDS_IN_A_DAY));
            long diffInSeconds = (long)(randomDate - baseDate).TotalSeconds;
            long result = DateUtils.TotalSeconds(randomDate);
            Assert.True(result == diffInSeconds);
        }

        /// <summary>
        /// probably TotalSeconds should be expended to throw exceptions for this
        /// </summary>
        [Test]
        public void TotalSeconds_DateLowerThanBase_Test()
        {
            //test for wrong input, expecting negative number
            long result = DateUtils.TotalSeconds(new DateTime(1965, 1, 1));
            Assert.Negative(result);
        }

        #endregion

        #region TotalSeconds & UnixTimestamp
        // I have no idea how to test this
        //[Test]
        //public void TotalSeconds_UnixTimeStamp_CurrentTime_Test()
        //{
        //    //test for wrong input, expecting negative number
        //    var nowTime = DateTime.UtcNow.Subtract(baseDate);
        //    long result = DateUtils.TotalSeconds((int)nowTime.TotalSeconds);
        //    Assert.AreEqual(DateUtils.UnixTimestamp(), result);
        //}

        #endregion

        #region TimespanToDate

        [Test]
        public void TimespanToDate_Test()
        {
            TimeSpan ts = new TimeSpan(0, 5, 2, 3);
            Assert.AreEqual("05:02:03" ,DateUtils.TimespanToDate(ts));
            ts = new TimeSpan(0, 15, 22, 33);
            Assert.AreEqual("15:22:33", DateUtils.TimespanToDate(ts));

            ts = new TimeSpan(1, 5, 2, 3);
            Assert.AreEqual("1Day 05:02:03", DateUtils.TimespanToDate(ts));
            ts = new TimeSpan(1, 15, 22, 33);
            Assert.AreEqual("1Day 15:22:33", DateUtils.TimespanToDate(ts));

            ts = new TimeSpan(2, 5, 2, 3);
            Assert.AreEqual("2Days 05:02:03", DateUtils.TimespanToDate(ts));
            ts = new TimeSpan(2, 15, 22, 33);
            Assert.AreEqual("2Days 15:22:33", DateUtils.TimespanToDate(ts));
        }

        [Test]
        public void TimespanToDate_ZeroTime_Test()
        {
            TimeSpan t = new TimeSpan(0, 0, 0);
            Assert.AreEqual("00:00:00", DateUtils.TimespanToDate(t));
        }

        [Test]
        public void TimespanToDate_LessThanADay_Test()
        {
            int hours = UnityEngine.Random.Range(0, 24);
            int minutes = UnityEngine.Random.Range(0, 60);
            int seconds = UnityEngine.Random.Range(0, 60);
            TimeSpan t = new TimeSpan(0, hours, minutes, seconds);

            string result = DateUtils.TimespanToDate(t);

            Assert.False(result.Contains("Day"));

            StringBuilder sb = new StringBuilder();
            if (hours < 10)
                sb.Append("0");
            sb.Append(hours);
            sb.Append(":");
            if (minutes < 10)
                sb.Append("0");
            sb.Append(minutes);
            sb.Append(":");
            if (seconds < 10)
                sb.Append("0");
            sb.Append(seconds);

            Assert.AreEqual(sb.ToString(), result);

        }

        [Test]
        public void TimespanToDate_OneDay_Test()
        {
            int hours = UnityEngine.Random.Range(0, 24);
            int minutes = UnityEngine.Random.Range(0, 60);
            int seconds = UnityEngine.Random.Range(0, 60);
            TimeSpan t = new TimeSpan(1, hours, minutes, seconds);

            string result = DateUtils.TimespanToDate(t);

            StringBuilder sb = new StringBuilder("1Day ");
            if (hours < 10)
                sb.Append("0");
            sb.Append(hours);
            sb.Append(":");
            if (minutes < 10)
                sb.Append("0");
            sb.Append(minutes);
            sb.Append(":");
            if (seconds < 10)
                sb.Append("0");
            sb.Append(seconds);

            Assert.AreEqual(sb.ToString(), result);
        }

        [Test]
        public void TimespanToDate_AtLeastTwoDays_Test()
        {
            int numOfDays = UnityEngine.Random.Range(2, 28);
            int hours = UnityEngine.Random.Range(0, 24);
            int minutes = UnityEngine.Random.Range(0, 60);
            int seconds = UnityEngine.Random.Range(0, 60);
            TimeSpan t = new TimeSpan(numOfDays, hours, minutes, seconds);

            string result = DateUtils.TimespanToDate(t);

            StringBuilder sb = new StringBuilder(numOfDays + "Days ");
            if (hours < 10)
                sb.Append("0");
            sb.Append(hours);
            sb.Append(":");
            if (minutes < 10)
                sb.Append("0");
            sb.Append(minutes);
            sb.Append(":");
            if (seconds < 10)
                sb.Append("0");
            sb.Append(seconds);

            Assert.AreEqual(sb.ToString(), result);
        }

        #endregion

        //no idea how to test this
        [Test]
        public void GetUserTimeStampViaDaysFromReg_Test()
        {
            DateUtils.GetUserTimeStampViaDaysFromReg(0);
        }

        [Test]
        public void TimespanTo48HoursDate_MoreThan48Hours()
        {
            int numOfDays = UnityEngine.Random.Range(2, 28);
            int hours = UnityEngine.Random.Range(0, 24);
            int minutes = UnityEngine.Random.Range(0, 60);
            int seconds = UnityEngine.Random.Range(1, 60);
            TimeSpan t = new TimeSpan(numOfDays, hours, minutes, seconds);

            string result = DateUtils.TimespanTo48HoursDate(t);

            StringBuilder sb = new StringBuilder(numOfDays + " Days");
            Assert.AreEqual(sb.ToString(), result);
        }

        [Test]
        public void TimespanTo48HoursDate_48Hours()
        {
            TimeSpan t = new TimeSpan(48, 0, 0);

            string result = DateUtils.TimespanTo48HoursDate(t);

            string expected = "48:00:00";
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void TimespanTo48HoursDate_24To48Hours()
        {
            int hours = 24 + UnityEngine.Random.Range(0, 24);
            int minutes = UnityEngine.Random.Range(0, 60);
            int seconds = UnityEngine.Random.Range(0, 60);
            TimeSpan t = new TimeSpan(hours, minutes, seconds);

            string result = DateUtils.TimespanTo48HoursDate(t);

            StringBuilder sb = new StringBuilder();
            if (hours < 10)
                sb.Append("0");
            sb.Append(hours);
            sb.Append(":");
            if (minutes < 10)
                sb.Append("0");
            sb.Append(minutes);
            sb.Append(":");
            if (seconds < 10)
                sb.Append("0");
            sb.Append(seconds);

            Assert.AreEqual(sb.ToString(), result);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        //[UnityTest]
        //public IEnumerator DateUtilsTestsWithEnumeratorPasses()
        //{
        //     Use the Assert class to test conditions.
        //     Use yield to skip a frame.
        //    yield return null;
        //}
    }
}
