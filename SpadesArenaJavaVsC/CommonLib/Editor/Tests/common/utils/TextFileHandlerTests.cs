﻿using System.IO;

namespace Tests.common.utils
{
    public class TextFileHandlerTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void ReadStringFromTextFile_Pass()
        {
            string absolutePath = Application.dataPath + "/CommonLib/Editor/Tests/common/utils/Resources/faux files/text file for test.txt";
            string expectedString = "if any pretty girl reads it, give me a call ;P www.shaykrainer.com";
            string loadPath = "faux files/text file for test";

            //Debug.Log(File.Exists(absolutePath));

            if (!File.Exists(absolutePath))
                LogAssert.Equals(LogType.Error, string.Format("Path Does Not Exist : {0}", absolutePath));

            string result = TextFileHandler.ReadStringFromTextFile(loadPath);

            Assert.IsNotNull(result);
            Assert.AreEqual(expectedString, result);
        }

        [Test]
        public void ReadStringFromTextFile_Fail()
        {
            string absolutePath = Application.dataPath + "/CommonLib/Editor/Tests/common/utils/Resources/faux files/no text file here LOL.txt";
            string loadPath = "faux files/no text file here LOL";

            if (File.Exists(absolutePath))
                Debug.LogError("something went wrong the file to test actually exists wence it should not!!!");

            LogAssert.Equals(LogType.Error, string.Format("Path Does Not Exist : {0}", loadPath));
        }

        /*
         fails in the editor unless created files and THEIR METAS are not deleted manually before-hand
         conflicting with Unity's AssetDatabase checks
        [Test]
        public void WriteToPathTextFile_Create_Test()
        {
            //System.IO.File.WriteAllText(path, text);

            string absolutePath = Application.dataPath + "/CommonLib/Editor/Tests/common/utils/Resources/faux files/created file.txt";
            string inputText = "after this test is done I'll be snapped away";
            string loadPath = "faux files/created file";

            if (File.Exists(absolutePath))
            {
                //Debug.LogWarning("something went wrong the path and/or file exists!!!");
                File.Delete(absolutePath);
                File.Delete(absolutePath + ".meta");
            }

            Assert.DoesNotThrow(()=> TextFileHandler.WriteToPathTextFile(absolutePath, inputText));

            string result = TextFileHandler.ReadStringFromTextFile(loadPath);
            Assert.AreEqual(result, inputText);

            File.Delete(absolutePath);
        }
        */

        [Test]
        public void WriteToPathTextFile_Overwrite_Test()
        {
            //System.IO.File.WriteAllText(path, text);

            string absolutePath = Application.dataPath + "/CommonLib/Editor/Tests/common/utils/Resources/faux files/text file for test.txt";
            string inputText = "if any pretty girl reads it, give me a call ;P www.shaykrainer.com";
            string originalText = "";
            string loadPath = "faux files/text file for test";

            if (!File.Exists(absolutePath))
                Assert.Fail("something went wrong the path and/or file to test is missing!!!");
            else
                originalText = TextFileHandler.ReadStringFromTextFile(loadPath);

            Assert.DoesNotThrow(() => TextFileHandler.WriteToPathTextFile(absolutePath, inputText));
            string result = TextFileHandler.ReadStringFromTextFile(loadPath);

            Assert.IsNotNull(result);
            Assert.AreEqual(inputText, result);

            TextFileHandler.WriteToPathTextFile(absolutePath, originalText);
        }
    }
}
