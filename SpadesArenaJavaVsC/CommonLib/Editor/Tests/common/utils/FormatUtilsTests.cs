﻿using System.Collections;
using System.Collections.Generic;
using common.utils;
using System;

namespace Tests.common.utils
{
    public class FormatUtilsTests
    {
        #region FormatPrice
        //version 1 - let's try to test FormatUtils methods
        //thousands have commas

        // A Test behaves as an ordinary method
        [Test]
        public void FormatPriceZeroIntToZeroStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatPrice(0);
            Assert.AreEqual("0", result);
        }

        [Test]
        public void FormatPriceThousandsIntToStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatPrice(1234);
            Assert.AreEqual("1,234", result);
        }

        [Test]
        public void FormatPriceHundredThousandsIntToStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatPrice(100000);
            Assert.AreEqual("100K", result);
        }

        [Test]
        public void FormatPriceMillionsIntToStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatPrice(1200000);
            Assert.AreEqual("1.2M", result);
        }
        #endregion

        #region FormatBuyIn
        //thousands have DO NOT commas
        [Test]
        public void FormatBuyInZeroIntToZeroStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatBuyIn(0);
            Assert.AreEqual("0", result);
        }

        [Test]
        public void FormatBuyInThousandsIntToZeroStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatBuyIn(2345);
            Assert.AreEqual("2345", result);
        }

        [Test]
        public void FormatBuyInThousandsIntToZeroStringNoCommasTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatBuyIn(2345);
            Assert.AreNotEqual("2,345", result);
        }

        [Test]
        public void FormatBuyInHundredThousandsIntToZeroStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatBuyIn(10000);
            Assert.AreEqual("10K", result);
        }

        [Test]
        public void FormatBuyInMillionsIntToZeroStringTest()
        {
            // Use the Assert class to test conditions
            string result = FormatUtils.FormatBuyIn(1200000);
            Assert.AreEqual("1.2M", result);
        }

        #endregion

        #region FormatBalance

        [Test, Sequential]
        public void FormatBalanceCommasTest(
            [Values(0, 2, 34, 555, 2100, 13456, 500000, 1456987, 10456987, 104569874, 2147483646)] int intAmount, 
            [Values("0", "2", "34", "555", "2,100", "13,456", "500,000", "1,456,987", "10,456,987", "104,569,874", "2,147,483,646")] string formattedStringResult)
        //2147483647 is Int max value. More tests are needed?
        {
            string result = FormatUtils.FormatBalance(intAmount);
            Assert.AreEqual(formattedStringResult, result);
        }

        #endregion

        #region versioning
        //missing - tests for correctness of input. could they even happen?
        //1) what are the valid semvers ranges for minors and builds? for example:
        //in the last input we expect 1012561024 but it is 101257024 due to how the function builds version code Google style.
        //2) could negative numbers accidently happen?
        [Test, Sequential]
        public void VersionStringToIntVersionCodeTest(
    [Values("0.0.0", "1.0.0", "1.2.3", "4.0.5", "12.23.45", "101.256.1024")] string versionString,
    [Values(0, 1000000, 1002003, 4000005, 12023045, 101257024)] int  intVersionCode)
        {
            int result = FormatUtils.VersionToVersionCode(versionString);
            Assert.AreEqual(intVersionCode, result);
        }

        [Test, Sequential]
        public void IntVersionCodeToVersionStringTest(
[Values(0, 1000000, 1002003, 4000005, 12023045)] int intVersionCode,
[Values("0.0.0", "1.0.0", "1.2.3", "4.0.5", "12.23.45")] string versionString)
        {
            string result = FormatUtils.VersionCodeToVersion(intVersionCode);
            Assert.AreEqual(versionString, result);
        }

        #endregion

        #region format data structures
        [Test]
        [Sequential]
        public void FormatListTest(
[Values(new object[] { 0, 2, 3 },
                    new object[] { "1", "thanks", "<3" },
                    new object[] {0},
                    new object[] {})] IEnumerable ListCases,
[Values("0,2,3", "1,thanks,<3", "0", "")] string listResult)
        {
            string result = FormatUtils.FormatList(ListCases);
            Assert.AreEqual(listResult, result);
        }


        [Test]
        public void FormatGenericDictionaryTest()
        {
            //rigid "not smart" test. might be worth investigating in the future for better utilizations and tests.

            //hashtable so we could in one swoop match any key type to any value type
            Hashtable hashtable = new Hashtable();
            hashtable.Add("hello", "world");
            hashtable.Add(4, "4");
            hashtable.Add("5", 5);
            hashtable.Add('c', -3);
            hashtable.Add(-2, 'b');

            string result = FormatUtils.FormatDictionary(hashtable);
            Assert.True(result.Contains("hello=world"));
            Assert.True(result.Contains("4=4"));
            Assert.True(result.Contains("5=5"));
            Assert.True(result.Contains("c=-3"));
            Assert.True(result.Contains("-2=b"));
            Assert.AreEqual(TextTool.CountStringOccurrences(result, ","), 4);
        }

        [Test]
        public void FormatDictionarySinglePairTest()
        {
            Dictionary<string, string> singlePair1 = new Dictionary<string, string>();
            singlePair1.Add("pass", "ok");
            string result = FormatUtils.FormatDictionary(singlePair1);
            Assert.True(result == "pass=ok");

            Dictionary<int, char> singlePair2 = new Dictionary<int, char>();
            singlePair2.Add(1, 'a');
            result = FormatUtils.FormatDictionary(singlePair2);
            Assert.True(result == "1=a");
        }

        [Test]
        public void FormatEmptyDictionaryTest()
        {
            Dictionary<string, string> noPair1 = new Dictionary<string, string>();
            string result = FormatUtils.FormatDictionary(noPair1);
            Assert.IsEmpty(result);

            Dictionary<int, char> noPair2 = new Dictionary<int, char>();
            result = FormatUtils.FormatDictionary(noPair2);
            Assert.IsEmpty(result);
        }

        [Test]
        public void GetLogTimeTest()
        {
            string result = FormatUtils.GetLogTime();
            Assert.AreEqual(DateTime.Now.ToString("H:mm:ss.ff"), result, "result is formatted as 'H: mm:ss.ff':" + result + "nothing else to test aginst");
        }

        #endregion

        #region aux methods
        /// <summary>
        /// Contains static text methods.
        /// Put this in a separate class in your project.
        /// </summary>
        public static class TextTool
        {
            /// <summary>
            /// Count occurrences of strings.
            /// </summary>
            public static int CountStringOccurrences(string text, string pattern)
            {
                // Loop through all instances of the string 'text'.
                int count = 0;
                int i = 0;
                while ((i = text.IndexOf(pattern, i)) != -1)
                {
                    i += pattern.Length;
                    count++;
                }
                return count;
            }
        }
        #endregion
    }
}
