﻿using common.utils;

namespace Tests.common.utils
{
    public class HebrewCharsExtensionsTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void IsHebrew_Pass_Test()
        {
            char heb = 'כ';
            Assert.True(heb.IsHebrew());
        }

        [Test]
        public void IsHebrew_Fail_Test()
        {
            char non_heb = 'n';
            Assert.False(non_heb.IsHebrew());
        }

        //how do I check against specific char types?
        [Test]
        public void GetHebrewCharType_Pass_Test()
        {
            char heb = 'כ';
            Assert.True(heb.GetHebrewCharType() != HebrewCharTypes.None);
        }

        [Test]
        public void GetHebrewCharType_Pass_Fail()
        {
            char non_heb = 'n';
            Assert.True(non_heb.GetHebrewCharType() == HebrewCharTypes.None);
        }

    }
}
