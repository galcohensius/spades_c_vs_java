﻿using System.Collections.Generic;
using common.commands;
using common.comm;

namespace Tests.common.commands
{
    public class CommandTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void CommandTestSimplePasses()
        {
            var testCommand = Substitute.For<Command>();
            MMessage testMessage = new MMessage();
            testMessage.Code = "test code";
            testMessage.Params = new List<Param>();
            testMessage.Params.Add(new TestParam1());
            testMessage.Params.Add(new TestParam2());
            Assert.DoesNotThrow(()=> testCommand.Execute(new MMessage()));
        }

        private class TestParam1 : Param
        {
            public override string Value => "test param 1 get";
        }

        private class TestParam2 : Param
        {
            public override string Value => "test param 2 get";
        }

    }
}
