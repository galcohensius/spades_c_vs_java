﻿using System.Collections.Generic;
using common.commands;
using common.comm;
//using common.utils;
using System;

namespace Tests.common.commands
{
    public class CommandsManagerTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void CommandsManager_RegisterCommand_Passes()
        {
            //setup commands and messages
            var errorCommand = Substitute.For<Command>();
            MMessage errorMEssage = new MMessage();
            errorMEssage.Code = "E1";
            errorMEssage.Params = new List<Param>();
            errorMEssage.Params.Add(new TestParam1());
            errorMEssage.Params.Add(new TestParam2());

            //var unknownCommand = Substitute.For<Command>();
            MMessage unknownMessage = new MMessage();
            unknownMessage.Code = "Z0";
            unknownMessage.Params = new List<Param>();
            unknownMessage.Params.Add(new TestParam3());
            unknownMessage.Params.Add(new TestParam4());

            var validCommand = Substitute.For<Command>();
            MMessage validMessage = new MMessage();
            validMessage.Code = "A1";
            validMessage.Params = new List<Param>();
            validMessage.Params.Add(new TestParam2());
            validMessage.Params.Add(new TestParam4());

            //setup command manager
            CommandsManager commandsManager = new CommandsManager();

            //tests begin here:
            //register commands
            Assert.DoesNotThrow(()=> commandsManager.RegisterCommand("E1", errorCommand));
            Assert.DoesNotThrow(() => commandsManager.RegisterCommand("A1", validCommand));

            //assert commands are registered
            Assert.True(commandsManager.Commands.Count == 2);

            //assert 3 possible executions
            //LogAssert.Expect(LogType.Log, "Error in message: " + errorMEssage.Code);
            Assert.Throws(typeof(Exception), () => commandsManager.ExecuteCommand(errorMEssage));


            //LogAssert.Expect(LogType.Log, "Unknown command for Code: " + unknownMessage.Code);
            //commandsManager.ExecuteCommand(unknownMessage);
            Assert.Throws(typeof(Exception), () => commandsManager.ExecuteCommand(unknownMessage));

            LogAssert.NoUnexpectedReceived();
            commandsManager.ExecuteCommand(validMessage);

            //Debug.Log(validMessage.ToString());
            //Debug.Log(unknownMessage.ToString());
            //Debug.Log(errorMEssage.ToString());
        }

        private class TestParam1 : Param
        {
            public override string Value => "test param 1 get";
        }

        private class TestParam2 : Param
        {
            public override string Value => "test param 2 get";
        }

        private class TestParam3 : Param
        {
            public override string Value => "test param 3 get";
        }

        private class TestParam4 : Param
        {
            public override string Value => "test param 4 get";
        }
    }
}
