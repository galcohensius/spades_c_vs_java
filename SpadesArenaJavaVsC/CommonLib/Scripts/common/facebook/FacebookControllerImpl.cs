using System;
using System.Collections.Generic;
using common.facebook.models;
using common.controllers.purchase;
using common.controllers;

namespace common.facebook
{
    public class FacebookControllerImpl : FacebookController
    {
        private InitCallback m_init_callback;
        private LoginCallback m_login_callback;
        private UserDetailsCallback m_user_details_callback;
        private PurchaseItemCallback m_purchase_item_callback;
        private UserFriendsCallback m_user_app_friends_callback;
        private UserFriendsCallback m_user_invite_friends_callback;
        private UserAppRequestCallback m_user_app_request_callback;

        private bool m_askForUserFriendsPermission;

        public override void InitFB(InitCallback init_callback, bool askForUserFriendsPermission = true)
        {
            m_askForUserFriendsPermission = askForUserFriendsPermission;

            m_init_callback = init_callback;
            if (!FB.IsInitialized)
            {
                LoggerController.Instance.Log("Trying to initialize Facebook SDK, using app id: " + FB.AppId);
                FB.Init(OnInitComplete);
            }
            else
            {
                LoggerController.Instance.Log("Facebook SDK already initialized");
                OnInitComplete();
            }
        }



        private void OnInitComplete()
        {
            LoggerController.Instance.Log("Facebook SDK Initialized");
            FB.ActivateApp();
            m_init_callback();

        }

        public override void Login(LoginCallback login_callback)
        {
            List<string> permissions = new List<string>() { "public_profile", "email" };

            if (m_askForUserFriendsPermission)
                permissions.Add(Permissions.USER_FRIENDS);

            m_login_callback = login_callback;
            FB.LogInWithReadPermissions(permissions, HandleLoginResult);
        }

        private void HandleLoginResult(ILoginResult result)
        {
            if (FB.IsLoggedIn)
            {
                m_login_callback(true);
            }
            else
            {
                m_login_callback(false);
            }
        }

        // Unity will call OnApplicationPause(false) when an app is resumed
        // from the background
        void OnApplicationPause(bool pauseStatus)
        {
            // Check the pauseStatus to see if we are in the foreground
            // or background
            if (!pauseStatus)
            {
                //app resume
                if (FB.IsInitialized)
                {
                    FB.ActivateApp();
                }
                else
                {
                    //Handle FB.Init
                    FB.Init(() =>
                    {
                        FB.ActivateApp();
                    });
                }
            }
        }

        public override bool IsLoggedIn()
        {
            return FB.IsLoggedIn;
        }

        public override void Logout()
        {
            FB.LogOut();
            UserFacebookData = null;
        }



        public override void GetUserDetails(UserDetailsCallback user_details_callback)
        {

            m_user_details_callback = user_details_callback;
            FB.API("/me?fields=first_name,last_name,email,middle_name,gender", HttpMethod.GET, HandleUserDetailsResults);

        }



        private void HandleUserDetailsResults(IResult result)
        {
            bool success = false;
            FacebookData facebook_data = null;

            if (result == null)
            {
                LoggerController.Instance.LogError("HandleUserDetailsResults - Null Response");
            }
            // Some platforms return the empty string instead of null.
            if (!string.IsNullOrEmpty(result.Error))
            {
                LoggerController.Instance.LogError("HandleUserDetailsResults - Error Response:\n" + result.Error);
            }
            else if (result.Cancelled)
            {
                LoggerController.Instance.LogError("HandleUserDetailsResults - Cancelled Response:\n" + result.RawResult);
            }
            else if (string.IsNullOrEmpty(result.RawResult))
            {
                LoggerController.Instance.LogError("HandleUserDetailsResults - Empty Response");
            } 
            else
            {
                LoggerController.Instance.Log("Facebook user details received: " + result.RawResult);


                object firstname, lastname, email, middlename, user_id, gender;

                result.ResultDictionary.TryGetValue("id", out user_id);
                result.ResultDictionary.TryGetValue("first_name", out firstname);
                result.ResultDictionary.TryGetValue("last_name", out lastname);
                result.ResultDictionary.TryGetValue("middle_name", out middlename);
                result.ResultDictionary.TryGetValue("email", out email);
                result.ResultDictionary.TryGetValue("gender", out gender);

                base.m_user_id = user_id.ToString();
                m_user_avatar_image_cache = null;

                success = true;
                facebook_data = new FacebookData();

                facebook_data.First_name = firstname != null ? firstname.ToString() : null;
                facebook_data.Last_name = lastname != null ? lastname.ToString() : null;
                facebook_data.Middle_name = middlename != null ? middlename.ToString() : null;
                facebook_data.User_id = m_user_id;
                facebook_data.Email = email != null ? email.ToString() : null;

                if (gender != null)
                    facebook_data.Gender = gender.ToString().ToUpper().Substring(0, 1);

                UserFacebookData = facebook_data;

            }

            if (!success)
                Logout(); // Logout from facebook if we cannot get the user's details

            m_user_details_callback(success);

        }

        public override void PurchaseItem(string itemId, string loginToken, string cashierVersion, PurchaseTrackingData purchaseTrackingData,
            PurchaseItemCallback callback)
        {
            m_purchase_item_callback = callback;

            string productUrl = string.Format("{0}/gameProduct.php?oId={1}&v={2}", FBBaseUrl, itemId, cashierVersion);

            JSONObject requestId = new JSONObject();
            requestId["oId"] = itemId;
            requestId["ltn"] = loginToken;
            requestId["rId"] = DateTime.Now.Ticks.ToString();

            // IMS related
            // requestId cannot be more the 256 chars long, so this creates a problem for now
            /*
            requestId["mDa"] = purchaseTrackingData.imsMetaData;
            requestId["aVe"] = new IMSModelParser().SerializeActionValue(purchaseTrackingData.imsActionValue);
            requestId["eDa"] = new JSONObject
            {
                ["pIx"] = purchaseTrackingData.popupIndex
            };
            */

            FB.Canvas.Pay(
                product: productUrl,
                requestId: requestId.ToString(),
                callback: HandlePurchaseItemResult
            );

            LoggerController.Instance.LogFormat("Canvas.Pay invoked with product: {0}, requestId: {1}", productUrl, requestId.ToString());
        }

        private void HandlePurchaseItemResult(IPayResult result)
        {

            if (result != null && !string.IsNullOrEmpty(result.RawResult))
            {
                if (result.Cancelled)
                {
                    LoggerController.Instance.Log("Canvas.Pay Canceled: " + result.RawResult);
                    m_purchase_item_callback(false, true, false, null, null);
                }
                else if (!string.IsNullOrEmpty(result.Error))
                {
                    LoggerController.Instance.LogError("Canvas.Pay Error: " + result.Error);
                    m_purchase_item_callback(false, false, false, null, null);
                }
                else
                {
                    object status, paymentId, signedRequest;

                    result.ResultDictionary.TryGetValue("status", out status);
                    result.ResultDictionary.TryGetValue("payment_id", out paymentId);
                    result.ResultDictionary.TryGetValue("signed_request", out signedRequest);

                    if (status == null || string.IsNullOrEmpty(status.ToString()) || status.ToString() == "failed")
                    {
                        m_purchase_item_callback(false, false, false, null, null);
                    }
                    else if (status.ToString() == "initiated")
                    {
                        m_purchase_item_callback(false, false, true, null, null);
                    }
                    else
                    {
                        // Success
                        LoggerController.Instance.Log("Canvas.Pay Successful: " + result.RawResult);
                        m_purchase_item_callback(true, false, false, paymentId.ToString(), signedRequest.ToString());
                    }
                }
            }
            else
            {
                throw new Exception("Canvas.Pay Empty Response");
            }
        }

        public override void LogAppEvent(string logEvent, Dictionary<string, object> parameters, float? valueToSum)
        {
            base.LogAppEvent(logEvent, parameters, valueToSum);
            FB.LogAppEvent("FB_" + logEvent, valueToSum, parameters);

        }

        public override void UpdateAchievement(string id)
        {
            if (FB.IsInitialized && FB.IsLoggedIn)
            {
                LoggerController.Instance.Log("Publishing FB achievemenet with id " + id);

                Dictionary<string, string> data = new Dictionary<string, string>() { { "achievement", id } };
                FB.API("/me/achievements", Facebook.Unity.HttpMethod.POST, AchievementCallback, data);

            }
            else
            {
                LoggerController.Instance.Log("Trying to post achievement to FB - but FB is not initialized or not logged in");
            }
        }

        void AchievementCallback(IGraphResult result)
        {

            if (result.Cancelled || !string.IsNullOrEmpty(result.Error))
            {
                LoggerController.Instance.Log("FB achievemenet error: " + result.Error);
            }
            else
            {
                LoggerController.Instance.Log("FB achievemenet published: " + result.ToString());
            }

        }

        public override bool CheckPermission(string permissionName)
        {
            if (!IsLoggedIn())
                return false;

            return AccessToken.CurrentAccessToken.Permissions.Where(s => s == permissionName).Count() > 0;
        }

        public override void GrantPermission(string permissionName, Action<bool> GrantedCallback)
        {
            if (!IsLoggedIn())
                return;

            if (!CheckPermission(permissionName))
            {
                FB.LogInWithReadPermissions(new List<string> { permissionName }, (ILoginResult) =>
                {
                    GrantedCallback(CheckPermission(permissionName));
                });
            }
            else
            {
                GrantedCallback(true);
            }
        }

        public override void DeleteAppRequest(string id, Action<bool> AppRequestResultCallback = null)
        {
            FB.API(id, HttpMethod.DELETE, (IGraphResult result) =>
            {
                if (result == null)
                {
                    LoggerController.Instance.Log("DeleteAppRequest result is null");
                    if (AppRequestResultCallback != null)
                        AppRequestResultCallback(false);
                }
                else if (!string.IsNullOrEmpty(result.Error))
                {
                    LoggerController.Instance.Log("DeleteAppRequest result error: " + result.Error);
                    if (AppRequestResultCallback != null)
                        AppRequestResultCallback(false);
                }
                else if (result.Cancelled)
                {
                    LoggerController.Instance.Log("DeleteAppRequest result cancelled");
                    if (AppRequestResultCallback != null)
                        AppRequestResultCallback(false);
                }
                else
                {
                    if (AppRequestResultCallback != null)
                        AppRequestResultCallback(true);
                }

            });
        }

        public override void GetUserAppFriends(UserFriendsCallback userFriendsCallback)
        {
            m_user_app_friends_callback = userFriendsCallback;

            if (IsLoggedIn())
                FB.API("/me/friends?fields=first_name,last_name,email,middle_name,gender&limit=50", HttpMethod.GET, GetUserAppFriendsCallback);
            else
                m_user_app_friends_callback(new List<FacebookData>());
        }

        public override void GetUserInviteFriends(UserFriendsCallback userFriendsCallback)
        {
            m_user_invite_friends_callback = userFriendsCallback;

            FB.API("/me/invitable_friends?fields=first_name,last_name,email,middle_name,gender,picture&limit=1000", HttpMethod.GET, GetUserInviteFriendsCallback);
        }

        public override void GetUserAppRequests(UserAppRequestCallback userAppRequestCallback)
        {
            m_user_app_request_callback = userAppRequestCallback;

            FB.API("/me/apprequests", HttpMethod.GET, GetAppRequestsCallback);
        }

        private void GetAppRequestsCallback(IGraphResult result)
        {
            if (result == null)
            {
                LoggerController.Instance.LogError("GetAppRequestsCallback: Null Response");
                return;
            }
            if (!string.IsNullOrEmpty(result.Error))
            {
                LoggerController.Instance.LogError("GetAppRequestsCallback: Error Response: " + result.Error);
                return;
            }
            if (result.Cancelled)
            {
                LoggerController.Instance.LogError("GetAppRequestsCallback: Cancelled Response: " + result.RawResult);
                return;
            }

            List<FBRequestData> app_request_List = new List<FBRequestData>();

            IDictionary<string, object> data = result.ResultDictionary;
            List<object> apprequests = (List<object>)data["data"];

            LoggerController.Instance.LogFormat("Found {0} app requests", apprequests.Count);

            foreach (object obj in apprequests)
            {
                Dictionary<string, object> apprequest = (Dictionary<string, object>)obj;

                //LoggerController.Instance.Log("App request: " +  FormatUtils.FormatDictionary (apprequest));

                object created_time, message, name, id, requestId, req_data_obj, req_from;
                DateTime create_time_converted = DateTime.Now;

                id = null;
                name = null;

                apprequest.TryGetValue("created_time", out created_time);
                apprequest.TryGetValue("data", out req_data_obj);
                apprequest.TryGetValue("message", out message);
                apprequest.TryGetValue("id", out requestId);
                apprequest.TryGetValue("from", out req_from);


                if (req_from != null)
                {
                    ((Dictionary<string, object>)req_from).TryGetValue("name", out name);
                    ((Dictionary<string, object>)req_from).TryGetValue("id", out id);
                }

                //LoggerController.Instance.Log ("Req Datqa object: " + req_data_obj);
                JSONObject req_data_json = null;
                if (req_data_obj != null)
                {
                    req_data_json = JSONObject.Parse(req_data_obj.ToString()).AsObject;
                }

                create_time_converted = Convert.ToDateTime(created_time);

                FBRequestData req_data = new FBRequestData();
                req_data.RequestId = requestId != null ? requestId.ToString() : null;
                req_data.Create_time = create_time_converted;
                req_data.Data = req_data_json;
                req_data.Message = message != null ? message.ToString() : null;
                req_data.Sender_name = name != null ? name.ToString() : null;
                req_data.Sender_id = id != null ? id.ToString() : null;

                app_request_List.Add(req_data);
            }

            m_user_app_request_callback(app_request_List);

        }

        private void GetUserAppFriendsCallback(IGraphResult result)
        {
            if (result == null)
            {
                LoggerController.Instance.LogError("Null Response");
                return;
            }
            if (!string.IsNullOrEmpty(result.Error))
            {
                LoggerController.Instance.LogError("Error Response:\n" + result.Error);
                return;
            }
            if (result.Cancelled)
            {
                LoggerController.Instance.LogError("Cancelled Response:\n" + result.RawResult);
                return;
            }

            List<FacebookData> friendsList = new List<FacebookData>();

            IDictionary<string, object> data = result.ResultDictionary;
            List<object> friends = (List<object>)data["data"];

            LoggerController.Instance.LogFormat("Found {0} facebook friends", friends.Count);

            foreach (object obj in friends)
            {
                Dictionary<string, object> friend = (Dictionary<string, object>)obj;

                object firstname, lastname, email, middlename, id, gender;

                friend.TryGetValue("id", out id);
                friend.TryGetValue("first_name", out firstname);
                friend.TryGetValue("last_name", out lastname);
                friend.TryGetValue("middle_name", out middlename);
                friend.TryGetValue("email", out email);
                friend.TryGetValue("gender", out gender);

                FacebookData facebook_data = new FacebookData();

                facebook_data.First_name = firstname != null ? firstname.ToString() : null;
                facebook_data.Last_name = lastname != null ? lastname.ToString() : null;
                facebook_data.Middle_name = middlename != null ? middlename.ToString() : null;
                facebook_data.User_id = id != null ? id.ToString() : null;
                facebook_data.Email = email != null ? email.ToString() : null;
                facebook_data.Image_url = null;

                if (gender != null)
                    facebook_data.Gender = gender.ToString().ToUpper().Substring(0, 1);

                friendsList.Add(facebook_data);

                LoggerController.Instance.Log("Found FB Friend: " + facebook_data.ToString());

            }

            m_user_app_friends_callback(friendsList);

        }

        private void GetUserInviteFriendsCallback(IGraphResult result)
        {
            if (result == null)
            {
                LoggerController.Instance.LogError("Null Response");
                return;
            }
            if (!string.IsNullOrEmpty(result.Error))
            {
                LoggerController.Instance.LogError("Error Response:\n" + result.Error);
                return;
            }
            if (result.Cancelled)
            {
                LoggerController.Instance.LogError("Cancelled Response:\n" + result.RawResult);
                return;
            }

            List<FacebookData> friendsList = new List<FacebookData>();

            IDictionary<string, object> data = result.ResultDictionary;
            List<object> friends = (List<object>)data["data"];

            foreach (object obj in friends)
            {
                Dictionary<string, object> friend = (Dictionary<string, object>)obj;

                object firstname, lastname, email, middlename, id, gender, url;

                friend.TryGetValue("id", out id);
                friend.TryGetValue("first_name", out firstname);
                friend.TryGetValue("last_name", out lastname);
                friend.TryGetValue("middle_name", out middlename);
                friend.TryGetValue("email", out email);
                friend.TryGetValue("gender", out gender);

                Dictionary<string, object> picture = (Dictionary<string, object>)friend["picture"];
                Dictionary<string, object> picture_data = (Dictionary<string, object>)picture["data"];

                url = picture_data["url"];

                FacebookData facebook_data = new FacebookData();

                facebook_data.First_name = firstname != null ? firstname.ToString() : null;
                facebook_data.Last_name = lastname != null ? lastname.ToString() : null;
                facebook_data.Middle_name = middlename != null ? middlename.ToString() : null;
                facebook_data.User_id = id != null ? id.ToString() : null;
                facebook_data.Email = email != null ? email.ToString() : null;
                facebook_data.Image_url = url != null ? url.ToString() : null;

                if (gender != null)
                    facebook_data.Gender = gender.ToString().ToUpper().Substring(0, 1);
                friendsList.Add(facebook_data);

                //LoggerController.Instance.Log ("Added a user to invite with ID: " + facebook_data.ToString ());

            }

            m_user_invite_friends_callback(friendsList);

        }


        public override void Share(string og_action, string title = "", string description = "", string imageUrl = "", Action<bool> ShareCompleted = null)
        {

            string ogURL = string.Format("{0}/og/og_share.php?title={1}&desc={2}&type={3}&image={4}",
                               FBBaseUrl, title, description, og_action, imageUrl);

            LoggerController.Instance.Log("Facebook OG share url: " + ogURL);

            FB.FeedShare(link: new Uri(ogURL), callback: (IShareResult result) =>
            {
                if (ShareCompleted != null)
                {
                    if (!string.IsNullOrEmpty(result.Error))
                        ShareCompleted(true);
                    else
                        ShareCompleted(false);
                }
            });
        }

        //public override void SendInviteFriends(string sender_name, Action<List<string>> AppRequestResultCallback)
        public override void SendInviteFriends(int userId, string inviteText, Action<List<string>> AppRequestResultCallback)
        {

            JSONObject json_data = new JSONObject();
            json_data.Add("type", "I");
            //json_data.Add("userId", ModelManager.Instance.GetUser().GetID());
            json_data.Add("userId", userId);

            FB.AppRequest(
                title: "Invite Your Friends to play",
                message: inviteText,
                data: json_data.ToString(),
                filters: new string[] { "app_non_users" },
                callback: (IAppRequestResult result) =>
                {
                    List<string> invite_result = new List<string>();

                    if (result.To != null)
                        invite_result = new List<string>(result.To);

                    AppRequestResultCallback(invite_result);

                });

        }

        public override void SendGiftToFriends(string sender_name, List<string> users, Action<int> AppRequestResultCallback)
        {
            LoggerController.Instance.Log("facebook controller received send gift for: " + users.Count);

            JSONObject json_data = new JSONObject();
            json_data.Add("type", "G");

            FB.AppRequest(
                message: sender_name + " sent you free coins! Click to collect!",
                to: users,
                data: json_data.ToString(),
                callback: (IAppRequestResult result) =>
                {
                    LoggerController.Instance.Log("App request: " + result);

                    if (AppRequestResultCallback != null)
                    {
                        if (result.To != null)
                            AppRequestResultCallback(result.To.Count());// will delete from social popup
                        else
                            AppRequestResultCallback(0);
                    }

                });


        }

    }
}