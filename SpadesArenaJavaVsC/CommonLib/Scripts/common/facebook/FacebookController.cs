﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using common.facebook.models;
using common.controllers.purchase;
using common.controllers;

namespace common.facebook
{


    public delegate void InitCallback();
    public delegate void LoginCallback(bool logged_success);
    public delegate void UserDetailsCallback(bool success);
    public delegate void UserPicCallback(Sprite user_pic);
    public delegate void PurchaseItemCallback(bool success, bool canceled, bool latePayment, string paymentId, string signedRequest);
    public delegate void GameRequestsCallback(bool success, string requestId, IEnumerable<string> recipientIds);
    public delegate void UserFriendsCallback(List<FacebookData> friends);
    public delegate void UserAppRequestCallback(List<FBRequestData> friends);

    public delegate void AppRequestResultCallback(IAppRequestResult result);



    public abstract class FacebookController : MonoBehaviour
    {

        private static FacebookController m_instance;

        protected Sprite m_user_avatar_image_cache;
        private Dictionary<string, Sprite> m_oppo_avatar_image_cache = new Dictionary<string, Sprite>();

        private const int OPPO_CACHE_SIZE = 50;
        protected string m_user_id;

        private Sprite m_defaultFacebookImage;


        public static FacebookController Instance
        {
            get
            {

                if (m_instance == null)
                {

                    GameObject facebookRoot = new GameObject("FacebookControllerRoot");
                    DontDestroyOnLoad(facebookRoot);

#if UNITY_EDITOR
                    facebookRoot.AddComponent<FacebookControllerEditor>();
                    m_instance = facebookRoot.GetComponent<FacebookControllerEditor>();

#else
					facebookRoot.AddComponent<FacebookControllerImpl> ();
					m_instance= facebookRoot.GetComponent<FacebookControllerImpl>();
#endif
                }
                return m_instance;
            }
        }

        #region Abstract methods

        public abstract void InitFB(InitCallback init_callback,bool askForUserFriendsPermission=true);

        public abstract bool IsLoggedIn();

        public abstract void Login(LoginCallback login_callback);

        public abstract void Logout();

        public abstract void GetUserDetails(UserDetailsCallback user_details_callback);

        public abstract void PurchaseItem(string itemId, string loginToken, string cashierVersion, PurchaseTrackingData purchaseTrackingData, PurchaseItemCallback callback);

        //public abstract void SendGameRequests (string message, GameRequestsCallback callback=null, Action RequestComplete=null);

        public abstract void UpdateAchievement(string id);

        public abstract bool CheckPermission(string permissionName);

        public abstract void GrantPermission(string permissionName, Action<bool> grantedCallback);

        public abstract void GetUserAppFriends(UserFriendsCallback userFriendsCallback);

        public abstract void GetUserInviteFriends(UserFriendsCallback userFriendsCallback);

        public abstract void SendInviteFriends(int userId, string inviteText, Action<List<string>> AppRequestResultCallback);
        //public abstract void SendInviteFriends(string sender_name, Action<List<string>> AppRequestResultCallback);

        public abstract void SendGiftToFriends(string sender_name, List<string> users, Action<int> AppRequestResultCallback);

        public abstract void GetUserAppRequests(UserAppRequestCallback userAppRequestCallback);

        public abstract void Share(string og_action, string title = "", string description = "", string imageUrl = "", Action<bool> ShareCompleted = null);

        public abstract void DeleteAppRequest(string id, Action<bool> AppRequestResultCallback);

        public virtual void LogAppEvent(string logEvent, Dictionary<string, object> parameters = null, float? valueToSum = null)
        {
            string paramsStr = "";
            if (parameters != null)
                paramsStr = string.Join(";", parameters.Select(x => x.Key + "=" + x.Value).ToArray());

            LoggerController.Instance.LogFormat("FBAppEvent Name={0}, params={1}", logEvent, paramsStr);
        }


        #endregion


        public void GetUserPicture(string user_id, int width, int height, bool useCache, UserPicCallback user_picture_callback)
        {
            if (user_id == m_user_id)
            {
                if (m_user_avatar_image_cache != null)
                {
                    user_picture_callback(m_user_avatar_image_cache);
                    return;
                }

            }
            else
            { //not user
                Sprite user_sprite;

                if (m_oppo_avatar_image_cache.TryGetValue(user_id, out user_sprite))
                {
                    user_picture_callback(user_sprite);
                    return;
                }
            }

            string imageUrl = string.Format("https://graph.facebook.com/{0}/picture?width={1}&height={2}", user_id, width, height);

            StartCoroutine(UserImage(imageUrl, user_id, width, height, useCache, user_picture_callback));

        }

        public void GetInviteFriendPicture(string friend_id, string imageUrl, UserPicCallback user_picture_callback)
        {
            Sprite user_sprite;
            m_oppo_avatar_image_cache.TryGetValue(friend_id, out user_sprite);


            if (user_sprite != null)
            {
                LoggerController.Instance.Log("Got image from cache " + friend_id);
                user_picture_callback(user_sprite);
                return;
            }

            StartCoroutine(UserImage(imageUrl, friend_id, 50, 50, false, user_picture_callback));

        }


        private IEnumerator UserImage(string imageUrl, string user_id, int width, int height, bool useCache, UserPicCallback call_back)
        {

            WWW url = new WWW(imageUrl);
            Texture2D textFb2 = new Texture2D(width, height, TextureFormat.DXT1, false); //TextureFormat must be DXT5
            yield return url;

            try
            {
                url.LoadImageIntoTexture(textFb2);
            }
            catch (Exception)
            {
                // DO nothing
                LoggerController.Instance.LogError("Cannot load image from url: " + imageUrl);
            }


            if (textFb2.width == 8)
            {
                LoggerController.Instance.Log("Cannot load facebook image: " + imageUrl + ". Using default image.");
                call_back(m_defaultFacebookImage);
            }
            else
            {
                Sprite user_sprite = Sprite.Create(textFb2, new Rect(0, 0, textFb2.width, textFb2.height), new Vector2(0.5f, 0.5f));

                if (useCache)
                {
                    if (user_id == m_user_id)
                        m_user_avatar_image_cache = user_sprite;
                    else
                    {
                        if (m_oppo_avatar_image_cache.Count >= OPPO_CACHE_SIZE)
                        {
                            string first_key = null;
                            foreach (var key in m_oppo_avatar_image_cache.Keys)
                            {
                                first_key = key;
                                break;
                            }
                            m_oppo_avatar_image_cache.Remove(first_key);

                        }
                        m_oppo_avatar_image_cache[user_id] = user_sprite;
                    }
                }

                call_back(user_sprite);
            }

        }

        public string FBBaseUrl
        {
            get;
            set;
        }

        

        public Sprite DefaultFacebookImage
        {
            get
            {
                return m_defaultFacebookImage;
            }

            set
            {
                m_defaultFacebookImage = value;
            }
        }

        public FacebookData UserFacebookData { get; protected set; }


        public static class Permissions
        {
            public const string USER_FRIENDS = "user_friends";



        }


    }

}