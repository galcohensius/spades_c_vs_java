﻿using common.utils;
using common.controllers;

namespace common.facebook.views
{
    public class FacebookButton : MonoBehaviour
    {
        public enum ButtonMode
        {
            Connect, SignOut, Waiting
        }

        [SerializeField] Button button = null;
        [SerializeField] TMP_Text text = null;
        [SerializeField] Image giftImage = null;

        GameObject waitingIndicator = null;

        private void Start()
        {
            waitingIndicator = Instantiate(PopupManager.Instance.Loader, transform);
            waitingIndicator.SetActive(false);
        }

        public void SetMode(ButtonMode buttonText, bool showGift = false)
        {
            text.gameObject.SetActive(true);
            if (waitingIndicator != null)
                waitingIndicator.SetActive(false);
            giftImage.gameObject.SetActive(false);
            button.interactable = true;

            switch (buttonText)
            {
                case ButtonMode.Connect:
                    text.text = "connect";
                    giftImage.gameObject.SetActive(showGift);
                    break;
                case ButtonMode.SignOut:
                    text.text = "sign out";
                    break;
                case ButtonMode.Waiting:
                    text.gameObject.SetActive(false);
                    if (waitingIndicator != null)
                        waitingIndicator.SetActive(true);
                    button.interactable = false;
                    break;
            }
        }

        public bool Interactable
        {
            set
            {
                button.interactable = value;
                ViewUtils.EnableDisableButtonText(button);
            }
        }

        public GameObject WaitingIndicator { get => waitingIndicator; set => waitingIndicator = value; }
    }

}