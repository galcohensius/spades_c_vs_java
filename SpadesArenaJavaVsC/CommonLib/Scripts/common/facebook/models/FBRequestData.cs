﻿using System;


namespace common.facebook.models
{

	public class FBRequestData {

		string	m_sender_id;
		string 	m_sender_name;
		JSONObject	m_data;
		string	m_message;
		DateTime	m_create_time;
		string	m_object_id;

		public FBRequestData()
		{
		}

		public FBRequestData (string user_id, string first_name, JSONObject data, string message, DateTime create_time)
		{
			m_sender_id = user_id;
			m_sender_name = first_name;
			m_data = data;
			m_message = message;
			m_create_time = create_time;


		}

		public string Sender_id {
			get {
				return m_sender_id;
			}
			set {
				m_sender_id = value;
			}
		}

		public string Sender_name {
			get {
				return m_sender_name;
			}
			set {
				m_sender_name = value;
			}
		}

		public DateTime Create_time {
			get {
				return m_create_time;
			}
			set {
				m_create_time = value;
			}
		}

		public string Message {
			get {
				return m_message;
			}
			set {
				m_message = value;
			}
		}

		public JSONObject Data {
			get {
				return m_data;
			}
			set {
				m_data = value;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[FBGiftData: m_sender_id={0}, m_sender_name={1}, ObjectId={2}]", m_sender_id, m_sender_name,m_object_id);
		}
		
		public string RequestId {
			get {
				return m_object_id;
			}
			set {
				m_object_id = value;
			}
		}
	}

}