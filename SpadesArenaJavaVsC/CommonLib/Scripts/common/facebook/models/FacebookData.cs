﻿namespace common.facebook.models
{

	public class FacebookData {

		string	user_id;
		string 	first_name;
		string 	last_name;
		string 	middle_name;
		string 	email;
		string  gender;
		string  image_url;

		bool	selected=false;

		public FacebookData ()
		{
		}

		public FacebookData (string user_id, string first_name, string last_name, string middle_name, string email, string gender, string image_url=null)
		{
			this.user_id = user_id;
			this.first_name = first_name;
			this.last_name = last_name;
			this.middle_name = middle_name;
			this.email = email;
			this.gender = gender;
			this.image_url = image_url;
		}
		

		public string Gender {
			get {
				return gender;
			}
			set {
				gender = value;
			}
		}

		public string User_id {
			get {
				return this.user_id;
			}
			set {
				user_id = value;
			}
		}

		public string First_name {
			get {
				return this.first_name;
			}
			set {
				first_name = value;
			}
		}

		public string Last_name {
			get {
				return this.last_name;
			}
			set {
				last_name = value;
			}
		}

		public string Middle_name {
			get {
				return this.middle_name;
			}
			set {
				middle_name = value;
			}
		}

		public string FullName
		{
			get{
				if (string.IsNullOrEmpty (middle_name))
					return string.Format ("{0} {1}", first_name, last_name);

				return string.Format ("{0} {1} {2}", first_name, middle_name, last_name);
			}
		}

		public string Email {
			get {
				return this.email;
			}
			set {
				email = value;
			}
		}


		public string Image_url {
			get {
				return image_url;
			}
			set {
				image_url = value;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[FacebookData: user_id={0}, first_name={1}, last_name={2}]", user_id, first_name, last_name);
		}
		
		public bool Selected {
			get {
				return selected;
			}
			set {
				selected = value;
			}
		}
	}


}