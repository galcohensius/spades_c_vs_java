﻿using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using common.facebook.models;
using common.controllers.purchase;
using common.controllers;

namespace common.facebook
{

    public class FacebookControllerEditor : FacebookController
    {

        HashSet<string> permissions = new HashSet<string>();

        public override void InitFB(InitCallback init_callback, bool askForUserFriendsPermission = true)
        {
            LoggerController.Instance.Log("Facebook initialized (Dummy) EDITOR");
            init_callback();
        }

        public override bool IsLoggedIn()
        {
            string fb = PlayerPrefs.GetString("Facebook_logged_in");

            if (fb == "true")
                return true;
            else
                return false;
        }

        public override void Login(LoginCallback login_callback)
        {
            PlayerPrefs.SetString("Facebook_logged_in", "true");
            login_callback(true);
            /*

            PopupManager.Instance.ShowQuestionPopup(PopupManager.AddMode.ShowAndRemove, "Login to Fakebook?", "Login", "Cancel", () =>
			{
                PlayerPrefs.SetString("Facebook_logged_in", "true");
                login_callback(true);
			}, () =>
			{
                PlayerPrefs.SetString("Facebook_logged_in", "false");
                login_callback(false);
			});*/

        }

        public override void Logout()
        {
            UserFacebookData = null;
            PlayerPrefs.SetString("Facebook_logged_in", "false");
        }


        public override void GetUserDetails(UserDetailsCallback user_details_callback)
        {
            string path = "FB_user_id.txt";

            string fb_id = null;
            using (StreamReader sr = File.OpenText(path))
            {
                if ((fb_id = sr.ReadLine()) != null)
                {
                    LoggerController.Instance.Log("FB id read from file: " + fb_id);
                    base.m_user_id = fb_id;
                    m_user_avatar_image_cache = null;
                }
            }

            FacebookData facebook_data = new FacebookData();
            facebook_data.User_id = fb_id;
            facebook_data.First_name = "Bibi";
            facebook_data.Last_name = "Netanyaho";

            UserFacebookData = facebook_data;

            user_details_callback(true);
        }


        public override void PurchaseItem(string itemId, string loginToken, string cashierVersion, PurchaseTrackingData purchaseTrackingData, PurchaseItemCallback callback)
        {
            // Dummy purchase data
            string paymentId = DateTime.Now.Ticks.ToString();
            string signedRequest = "SR_TEST"; // Predefined to skip Server checks

            StartCoroutine(PurchaseItemCallbackCoroutine(callback, true, false, paymentId, signedRequest));
            /*
            PopupManager.Instance.ShowQuestionPopup(PopupManager.AddMode.ShowAndKeep, "*** Editor Popup ***\nAre you sure you want to buy item " + itemId + "?", "Yes", "Cancel",
             () =>
             {
                 PopupManager.Instance.HidePopup();
                StartCoroutine(PurchaseItemCallbackCoroutine(callback,true, false, paymentId, signedRequest));
             }, () =>
             {
                PopupManager.Instance.HidePopup();
                StartCoroutine(PurchaseItemCallbackCoroutine(callback, false, true, paymentId, signedRequest));
             });
             */
        }

        private IEnumerator PurchaseItemCallbackCoroutine(PurchaseItemCallback purchaseItemCallback, bool success, bool canceled, string paymentId, string signedRequest)
        {
            // Synthetic wait so the popup will have time to be removed 
            yield return new WaitForSeconds(1f);
            purchaseItemCallback(success, canceled, false, paymentId, signedRequest);
        }



        public override void UpdateAchievement(string id)
        {
            if (id != "")
                LoggerController.Instance.Log("Achievement completed in FB EDITOR :" + id);

        }

        public override bool CheckPermission(string permissionName)
        {
            return permissions.Contains(permissionName);
            //return true;
        }

        public override void GrantPermission(string permissionName, Action<bool> GrantedCallback)
        {
            permissions.Add(permissionName);
            GrantedCallback(true);
        }

        public override void DeleteAppRequest(string id, Action<bool> AppRequestResultCallback)
        {
            if (AppRequestResultCallback != null)
                AppRequestResultCallback(true);
        }

        public override void GetUserAppRequests(UserAppRequestCallback userAppRequestCallback)
        {

            List<FBRequestData> friendsList = new List<FBRequestData>();
            FBRequestData[] dummyFriend = new FBRequestData[2];

            JSONObject json_data = new JSONObject();
            json_data.Add("type", "G");

            dummyFriend[0] = new FBRequestData("1", "Alon1", json_data, "Message", DateTime.Now);
            dummyFriend[1] = new FBRequestData("2", "Alon2", json_data, "Message", DateTime.Now);


            //dummyFriend[2]  = new FBRequestData ("4","Alon22", json_data,"Message",DateTime.Now);
            //dummyFriend[3]  = new FBRequestData ("5","Alon33", json_data,"Message",DateTime.Now);
            //dummyFriend[4]  = new FBRequestData ("3","Alon3", json_data,"Message",DateTime.Now);

            for (int i = 5; i < dummyFriend.Length; i++)
            {
                dummyFriend[i] = new FBRequestData(i.ToString(), "Alon" + i, json_data, "Message", DateTime.Now);
            }


            //friendsList.Add (dummyFriend[0]);
            //friendsList.Add (dummyFriend[1]);
            //friendsList.Add (dummyFriend[2]);
            //friendsList.Add (dummyFriend[3]);
            //friendsList.Add (dummyFriend[4]);

            for (int i = 0; i < dummyFriend.Length; i++)
                friendsList.Add(dummyFriend[i]);

            userAppRequestCallback(friendsList);


        }

        public override void GetUserAppFriends(UserFriendsCallback userFriendsCallback)
        {

            List<FacebookData> friendsList = new List<FacebookData>();

            if (IsLoggedIn())
            {
                FacebookData[] dummyFriend = new FacebookData[25];

                //("1842778399337205", "Dummy", "Cohen", null, null, null,null);

                // Add X friends for testing
                for (int i = 0; i < dummyFriend.Length; i++)
                {
                    dummyFriend[i] = new FacebookData((i + 1).ToString(), i + "moshe", i + "cohen", null, null, null,
                        //"https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/15350613_10157789088500570_8680460189375269680_n.jpg?oh=ea0b803fb3deada6fa1930a88747d86d&oe=5A78AD96"
                        "https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/16831157_10212888436275839_7957894583639097046_n.jpg?oh=469eaf7035538ccd167e22aca5d401a9&oe=5A6A53A5" + i
                    );
                    friendsList.Add(dummyFriend[i]);
                }
            }

            userFriendsCallback(friendsList);
        }

        public override void GetUserInviteFriends(UserFriendsCallback userFriendsCallback)
        {
            List<FacebookData> friendsList = new List<FacebookData>();
            FacebookData[] dummyFriend = new FacebookData[150];
            for (int i = 0; i < 150; i++)
            {
                dummyFriend[i] = new FacebookData("123" + i, "Invite" + i, i + "LAST", null, null, null,
                                                  "https://lookaside.facebook.com/gg/profilepic/?token=AVl446ImdC93vfLYegcEx3Zst8E1RWmfReQ87J4vsxBdWph3-rh0iWvUJ5kfr5YgUw9z1Xm9fOXQr4TlkrEd6TOZpepPrkSF3UX3G3S9P8J1Bg&tag=0"
                );
            }


            // Add X friends for testing
            for (int i = 0; i < 150; i++)
            {
                friendsList.Add(dummyFriend[i]);
            }

            userFriendsCallback(friendsList);
        }


        public override void Share(string og_action, string title = "", string description = "", string imageUrl = "", Action<bool> ShareCompleted = null)
        {
            //        PopupManager.Instance.ShowMessagePopup (string.Format ("Content shared:\n{0}\n{1}",title,description),PopupManager.AddMode.ShowAndRemove, true,()=>
            //{
            //            if (ShareCompleted!=null)
            //	ShareCompleted(true);
            //},null,700);

            if (ShareCompleted != null)
                ShareCompleted(true);

        }

        public override void SendInviteFriends(int userId, string inviteText, Action<List<string>> invite_completed_callback)
        {

            JSONObject json_data = new JSONObject();
            json_data.Add("type", "I");
            json_data.Add("userId", userId);

            LoggerController.Instance.Log(json_data.ToString());

            invite_completed_callback(null);
        }

        public override void SendGiftToFriends(string sender_name, List<string> users, Action<int> invite_completed_callback)
        {
            LoggerController.Instance.Log("facebook controller received send gift users list: " + users.Count);

            if (invite_completed_callback != null)
                invite_completed_callback(users.Count);
        }
    }
}