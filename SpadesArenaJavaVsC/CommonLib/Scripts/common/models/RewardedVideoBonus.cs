﻿namespace common.models
{
    public class RewardedVideoBonus : GeneralBonus
    {
        BonusTypes m_bonusType;

        public RewardedVideoBonus(BonusTypes bonusType)
        {
            m_bonusType = bonusType;
        }

        public override BonusTypes Type
        {
            get
            {
                return m_bonusType;
            }
        }

        public override int SubType
        {
            get
            {
                return 0;
            }
        }

        public BonusTypes BonusType { get => m_bonusType; set => m_bonusType = value; }
    }
}
