﻿namespace common.models
{

	public class GiftBonus:Bonus {


		public override BonusTypes Type {
			get {
				return BonusTypes.Gift;
			}
		}

		public override int SubType {
			get {
				return 0;
			}
		}
	}
}