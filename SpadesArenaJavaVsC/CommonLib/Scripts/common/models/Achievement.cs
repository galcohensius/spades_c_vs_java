﻿using common.controllers;

namespace common.models
{
	public enum AchievementType
	{
		OneTime,
		Accumulative,
		InternalAccum
	}

	public class Achievement 
	{
		int 			m_group_id;
		int 			m_id;
		string			m_name;
		string			m_description;
		int				m_coins;
		AchievementType	m_type;
		int				m_total;
		int				m_counter;
		string 			m_fb_id;
		string			m_google_id;
		string			m_apple_id;

		public Achievement (int m_group_id, int m_id, string m_name, string m_description, int coins, AchievementType m_type, int m_total,string fb_id,string apple_id,string google_id)
		{
			this.m_group_id = m_group_id;
			this.m_id = m_id;
			this.m_name = m_name;
			this.m_description = m_description;
			this.m_coins = coins;
			this.m_type = m_type;
			this.m_total = m_total;
			this.m_fb_id = fb_id;
			this.m_apple_id = apple_id;
			this.m_google_id = google_id;
		}


		public void IncCounter(int delta)
		{
			/*
			if (m_counter == 0 && m_type == achievementType.Timed)
				m_start_time = DateTime.UtcNow;
*/
				m_counter += delta;
		}

		public void PrintMe()
		{
			LoggerController.Instance.Log ("print me in achievemebnt manager name:" + m_name);
			LoggerController.Instance.Log ("print me in achievemebnt manager counter:" + m_counter);
		}

		public bool IsCompleted()
		{
			if (m_counter >= m_total)
				return true;
			else
				return false;
		}

		public int Id {
			get {
				return m_id;
			}
		}

		public int Group_id {
			get {
				return this.m_group_id;
			}
		}

		public int Counter {
			get {
				return m_counter;
			}
			set{
				m_counter = value;
			}
		}



		public string Name {
			get {
				return m_name;
			}
		}

		public string Fb_id {
			get {
				return m_fb_id;
			}
		}

		public string Google_id {
			get {
				return m_google_id;
			}
		}

		public string Apple_id {
			get {
				return m_apple_id;
			}
		}

		public string Description {
			get {
				return m_description;
			}
		}

		public int Coins {
			get {
				return m_coins;
			}
		}

		public AchievementType AC_Type {
			get {
				return m_type;
			}
		}

		public int Total {
			get {
				return m_total;
			}
		}
	}
}