﻿namespace common.models
{
    public abstract class TableFilterData
    {
        int m_minBuyIn;
        int m_maxBuyIn;
        bool m_specialOnly;

        public int MinBuyIn { get => m_minBuyIn; set => m_minBuyIn = value; }
        public int MaxBuyIn { get => m_maxBuyIn; set => m_maxBuyIn = value; }
        public bool SpecialOnly { get => m_specialOnly; set => m_specialOnly = value; }

        public override bool Equals(object obj)
        {
            TableFilterData other = (TableFilterData)obj;

            return other.m_minBuyIn == m_minBuyIn && other.m_maxBuyIn == m_maxBuyIn && other.m_specialOnly == m_specialOnly;
        }

        public override int GetHashCode()
        {
            var hashCode = 629651924;
            hashCode = hashCode * -1521134295 + m_minBuyIn.GetHashCode();
            hashCode = hashCode * -1521134295 + m_maxBuyIn.GetHashCode();
            hashCode = hashCode * -1521134295 + m_specialOnly.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(TableFilterData c1, TableFilterData c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(TableFilterData c1, TableFilterData c2)
        {
            return !c1.Equals(c2);
        }
    }
}


