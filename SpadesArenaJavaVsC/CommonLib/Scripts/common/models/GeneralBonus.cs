﻿namespace common.models
{
	public class GeneralBonus:Bonus
	{
		private string m_token = null;

        public GeneralBonus ()
		{
		}

		public string Token {
			get {
				return m_token;
			}
			set {
				m_token = value;
			}
		}


		public override BonusTypes Type {
			get {
				return BonusTypes.GeneralBonus;
			}
		}

		public override int SubType {
			get {
				return 0;
			}
		}

       
    }
}

