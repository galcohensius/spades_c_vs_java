﻿namespace common.models
{
    public class FacebookBonus : GeneralBonus
    {
        public override BonusTypes Type
        {
            get
            {
                return BonusTypes.FacebookConnect;
            }
        }

        public override int SubType
        {
            get
            {
                return 0;
            }
        }

    }
}
