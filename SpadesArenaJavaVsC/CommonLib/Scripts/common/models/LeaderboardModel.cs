﻿using common.comm;
using System.Collections.Generic;

namespace common.models
{
	public class LeaderboardModel
	{
		private LeaderboardMeta m_leaderboardMetaData;
		private List<LeaderboardItemModel> m_items;

		public enum RankState
		{
			DOWN = 1,
			NO_CHANGE,
			UP
		}

		public LeaderboardModel (CompositeParam users)
		{
		}

		public LeaderboardModel ()
		{
		}

	
		public void ClearCache (bool all)
		{
			if (all)
				m_leaderboardMetaData = null;
			
			m_items = null;
		}

		public List<LeaderboardItemModel> Items {
			get {
				return m_items;
			}
			set {
				m_items = value;
			}
		}

		public LeaderboardMeta LeaderboardMetaData {
			get {
				return m_leaderboardMetaData;
			}
			set {
				m_leaderboardMetaData = value;
			}
		}
	}
		

}

