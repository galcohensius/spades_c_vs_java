﻿using System.Collections.Generic;


namespace common.models
{

	public class InviteBonus:Bonus {

		private List<CommonUser> m_inviters = new List<CommonUser>();

		public override BonusTypes Type {
			get {
				return BonusTypes.Invite;
			}
		}

		public override int SubType {
			get {
				return 0;
			}
		}


		public List<CommonUser> Inviters {
			get {
				return m_inviters;
			}
			set {
				m_inviters = value;
			}
		}
	}
}

