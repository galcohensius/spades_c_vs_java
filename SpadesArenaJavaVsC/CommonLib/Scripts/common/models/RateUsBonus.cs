﻿namespace common.models
{

    public class RateUsBonus : GeneralBonus {



		public override BonusTypes Type {
			get {
				return BonusTypes.RateUs;
			}
		}

		public override int SubType {
			get {
				return 0;
			}
		}
	}
}