﻿using System;
using System.Collections.Generic;

namespace common.models
{
	public class DailyBonus:Bonus
	{
		int steps;
		int nextStep;
		int	interval;
		DateTime nextBonusTime;
		List<int> consecutiveDaysFactor = new List<int>();
		List<int> friendsFactor = new List<int>();
		List<int> pay_table = new List<int>();


		public int Steps {
			get {
				return this.steps;
			}
			set {
				steps = value;
			}
		}

		public int NextStep {
			get {
				return this.nextStep;
			}
			set {
				nextStep = value;
			}
		}

		public List<int> ConsecutiveDaysFactor {
			get {
				return this.consecutiveDaysFactor;
			}
		}

		public List<int> Pay_table {
			get {
				return pay_table;
			}
		}

		public int Interval {
			get {
				return this.interval;
			}
			set {
				interval = value;
				nextBonusTime = DateTime.Now.AddSeconds (interval);
			}
		}

		public DateTime NextBonusTime {
			get {
				return this.nextBonusTime;
			}
		}

		public override BonusTypes Type {
			get {
				return BonusTypes.Daily;
			}
		}

		public override int SubType {
			get {
				return 0;
			}
		}

		public List<int> FriendsFactor {
			get {
				return friendsFactor;
			}
			set {
				friendsFactor = value;
			}
		}
	}
}

