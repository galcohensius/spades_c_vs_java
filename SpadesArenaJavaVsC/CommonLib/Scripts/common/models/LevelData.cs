﻿using System.Collections.Generic;

namespace common.models
{
	public class LevelData
	{
		int level;
		int xpThreshold;
		BonusAwarded levelBonus;
		List<Bonus> newBonuses;

		public int Level {
			get {
				return this.level;
			}
			set {
				level = value;
			}
		}

		public int XpThreshold {
			get {
				return this.xpThreshold;
			}
			set {
				xpThreshold = value;
			}
		}

		public BonusAwarded LevelBonus {
			get {
				return this.levelBonus;
			}
			set {
				levelBonus = value;
			}
		}

		public List<Bonus> NewBonuses {
			get {
				return this.newBonuses;
			}
			set {
				newBonuses = value;
			}
		}
	}
}

