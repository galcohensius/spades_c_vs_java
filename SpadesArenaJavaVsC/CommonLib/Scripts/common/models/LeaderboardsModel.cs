using System.Collections.Generic;
using System;

namespace common.models
{
	public class LeaderboardsModel
	{
		public static int LEADERBOARD_MIN_LEVEL = 15;

        public static Action<LeaderboardType, string> OnJoinedRanksData;

        public enum LeaderboardType
		{
			GlobalCurrent=0,
			GlobalPrevious=1,
			Friends=2,
			HallOfFame=10
		}

		public Dictionary<string,string> InfoTable = new Dictionary<string, string>();
		private Dictionary<string,string> m_leaderboardTitleMap = new Dictionary<string, string> () {
			{ "A1","BRONZE" },
			{ "A2","BRONZE" },
			{ "A3","BRONZE" },
			{ "B1","SILVER" },
			{ "B2","SILVER" },
			{ "B3","SILVER" },
			{ "C1","GOLD" },
			{ "C2","GOLD" },
			{ "C3","GOLD" },
			{ "D1","PLATINUM" },
			{ "D2","PLATINUM" },
			{ "D3","PLATINUM" }
		};

		private Dictionary<long, CommonUser> userProfileCache = new Dictionary<long, CommonUser> ();
		private Dictionary<LeaderboardType,LeaderboardModel> leaderboards = new Dictionary<LeaderboardType,LeaderboardModel> ();
		private int m_currentLeaderboardId = -1;

		private float m_minBotFactor = 0;
		private float m_maxBotFactor = 0;

        private CommonUser m_currUser;


        public LeaderboardsModel()
        {
            leaderboards.Add(LeaderboardType.GlobalCurrent, new LeaderboardModel());
            leaderboards.Add(LeaderboardType.GlobalPrevious, new LeaderboardModel());
            leaderboards.Add(LeaderboardType.Friends, new LeaderboardModel());
            leaderboards.Add(LeaderboardType.HallOfFame, new LeaderboardModel());
        }

        public LeaderboardsModel (CommonUser currUser)
		{
			leaderboards.Add (LeaderboardType.GlobalCurrent, new LeaderboardModel ());
			leaderboards.Add (LeaderboardType.GlobalPrevious, new LeaderboardModel ());
			leaderboards.Add (LeaderboardType.Friends, new LeaderboardModel ());
			leaderboards.Add (LeaderboardType.HallOfFame, new LeaderboardModel ());

            m_currUser = currUser;
		}

		public string GetTitleFromGroup (string group)
		{
			if (group == null)
				group = "A1";
			
			return m_leaderboardTitleMap [group];
		}


		public string GetColorFromGroup (string group)
		{
			string color = "";

			if (group == null)
				group = "A1";
			
			switch (group.Substring (0, 1)) {
			case "A":
				color = "BRONZE";
				break;
			case "B":
				color = "SILVER";
				break;
			case "C":
				color = "GOLD";
				break;
			case "D":
				color = "PLATINUM";
				break;
			}

			return color;
		}


		public Dictionary<long, CommonUser> UserProfileCache {
			get {
				return userProfileCache;
			}
			set {
				userProfileCache = value;
			}
		}

		public LeaderboardModel GetLeaderboard (LeaderboardType type)
		{
			if (leaderboards.ContainsKey (type))
				return leaderboards [type];
			
			return null;
		}

		public LeaderboardType FindLeaderboardTypeById (int id)
		{
			if (leaderboards.ContainsKey (LeaderboardType.GlobalCurrent)) {
				if (leaderboards [LeaderboardType.GlobalCurrent].LeaderboardMetaData != null && leaderboards [LeaderboardType.GlobalCurrent].LeaderboardMetaData.Id == id) {
					return LeaderboardType.GlobalCurrent;
				}

			}

			if (leaderboards.ContainsKey (LeaderboardType.GlobalPrevious)) {
				if (leaderboards [LeaderboardType.GlobalPrevious].LeaderboardMetaData != null && leaderboards [LeaderboardType.GlobalPrevious].LeaderboardMetaData.Id == id) {
					return LeaderboardType.GlobalPrevious;
				}
			}

			return LeaderboardType.GlobalCurrent;
		}

		public void SetLeaderboard (LeaderboardType type, LeaderboardModel lm)
		{
			Dictionary<long,int> ranksData = new Dictionary<long, int> ();

			leaderboards [type] = lm;

			if (lm == null)
				return;

			if (lm.Items != null)
				for (int itmIdx = 0; itmIdx < lm.Items.Count; itmIdx++) {
					if (itmIdx < 80) {
						ranksData.Add (lm.Items [itmIdx].UserId, lm.Items [itmIdx].Rank);
					} else if (lm.Items [itmIdx].UserId == m_currUser.GetID ()) {
						ranksData.Add (lm.Items [itmIdx].UserId, lm.Items [itmIdx].Rank);
					}
				}

			string mJoindeRanksData = JoinRanksData (ranksData);

			if (mJoindeRanksData != "")
            {
                OnJoinedRanksData?.Invoke(type, mJoindeRanksData);
            }
		}

		public void ClearCache (bool includeMetaData)
		{
			userProfileCache.Clear ();

			foreach (KeyValuePair<LeaderboardType,LeaderboardModel> ldr in leaderboards) {
				ldr.Value.ClearCache (includeMetaData);
			}

			
		}

		public int CurrentLeaderboardId {
			get {
				return m_currentLeaderboardId;
			}
			set {
				m_currentLeaderboardId = value;
			}
		}

		private string JoinRanksData (Dictionary<long,int> ranksData)
		{
			string retVal = "";

			foreach (KeyValuePair<long, int> pair in ranksData) {
				retVal += pair.Key + "-" + pair.Value + ",";
			}

			if (retVal != "")
				retVal = retVal.Remove (retVal.Length - 1);

			return retVal;
		}

		public float MinBotFactor {
			get {
				return m_minBotFactor;
			}
			set {
				m_minBotFactor = value;
			}
		}

		public float MaxBotFactor {
			get {
				return m_maxBotFactor;
			}
			set {
				m_maxBotFactor = value;
			}
		}
			
	}
}


