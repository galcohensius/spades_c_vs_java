﻿using System;

namespace common.models
{
	public class CollectibleBonus : Bonus
	{
		string worldId;
		BonusTypes type;

		public CollectibleBonus (BonusTypes type,int sub_type)
		{
			this.type = type;
			worldId = sub_type.ToString ();
		}

		public string WorldId {
			get {
				return this.worldId;
			}
			set {
				worldId = value;
			}
		}
			
		public override BonusTypes Type {
			get {
				return type;
			}
		}

		public override int SubType {
			get {
				return Convert.ToInt32(worldId);
			}
		}

	}
}

