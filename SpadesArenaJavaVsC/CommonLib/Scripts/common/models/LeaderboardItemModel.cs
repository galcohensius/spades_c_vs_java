﻿using cardGames.models;
using System;

namespace common.models
{
	public class LeaderboardItemModel
	{
        public static Func<int> OnSetUserID;

		public const int RANK_IDX = 0;
		public const int USER_ID_IDX = 1;
		public const int FACEBOOK_ID_IDX = 2;
		public const int SCORE_IDX = 3;
		public const int USER_DETAILS_IDX = 4;

		private int mRank = 0;
		private long mUserId = 0;
		private string mFbId = "";
		private long mScore = 0;
		private common.models.LeaderboardModel.RankState mRankState = LeaderboardModel.RankState.NO_CHANGE;

        private MAvatarModel m_MAvatarModel;

        private bool m_isMe = false;
        private bool m_shouldDisplaySilhouette = false;

        public LeaderboardItemModel()
        {
            
        }

		public int Rank {
			get {
				return mRank;
			}
			set {
				mRank = value;
			}
		}

		public long UserId
        {
			get
            {
				return mUserId;
			}
			set
            {
				mUserId = value;

                if (OnSetUserID != null)
                {
                    if (mUserId == OnSetUserID())
                        IsMe = true;
                }
                else
                {
                    throw new Exception("could not get UserID from ModelManager and compare to LeaderboardItemModel mUserId");
                }
			}
		}

		public string FacebookId {
			get {
				return mFbId;
			}
			set {
				mFbId = value;
			}
		}

		public long Score {
			get {
				return mScore;
			}
			set {
				mScore = value;
			}
		}




		public common.models.LeaderboardModel.RankState RankState {
			get {
				return mRankState;
			}
			set {
				mRankState = value;
			}
		}

        public MAvatarModel MAvatarModel
        {
            get
            {
                return m_MAvatarModel;
            }

            set
            {
                m_MAvatarModel = value;
            }
        }

        public bool IsMe
        {
            get
            {
                return m_isMe;
            }

            private set
            {
                m_isMe = value;
            }
        }

        public bool ShouldDisplaySilhouette
        {
            get
            {
                return m_shouldDisplaySilhouette;
            }

            set
            {
                m_shouldDisplaySilhouette = value;
            }
        }
    }
}

