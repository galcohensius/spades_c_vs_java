﻿using common.ims.model;

namespace common.models
{
	public class BonusAwarded
	{
		Bonus.BonusTypes type;
        IMSGoodsList goodsList = new IMSGoodsList();
		int interval;
		int nextStep;
		int goodsIndex;
		int friendsFactor;
		int subType;

		public Bonus.BonusTypes Type {
			get {
				return this.type;
			}
			set {
				type = value;
			}
		}
		

		public int Interval {
			get {
				return this.interval;
			}
			set {
				interval = value;
			}
		}

		public int NextStep {
			get {
				return this.nextStep;
			}
			set {
				nextStep = value;
			}
		}

		public int GoodsIndex {
			get {
				return this.goodsIndex;
			}
			set {
				goodsIndex = value;
			}
		}

		public int FriendsFactor {
			get {
				return this.friendsFactor;
			}
			set {
				friendsFactor = value;
			}
		}

		public int SubType {
			get {
				return subType;
			}
			set {
				subType = value;
			}
		}

        public IMSGoodsList GoodsList { get => goodsList; set => goodsList = value; }

		
	}
}

