﻿using System.Collections.Generic;


namespace common.models
{
	
	public class FBShare 
	{
		
		string 			m_id;
		string			m_title;
		string			m_description;
		List<string>	m_images = new List<string> ();
		string 			m_og_action;

		public FBShare()
		{
		}

		public FBShare (string id, string title, string description,string og_action)
		{
			this.m_id = id;
			this.m_title = title;
			this.m_description = description;
            this.m_og_action = og_action;

		}

		public List<string> Images {
			get {
				return m_images;
			}
		}

		public string Id {
			get {
				return m_id;
			}
		}

		public string Title {
			get {
				return m_title;
			}
		}

		public string Description {
			get {
				return m_description;
			}
		}

		public string Og_action {
			get {
				return m_og_action;
			}
		}
	}
}