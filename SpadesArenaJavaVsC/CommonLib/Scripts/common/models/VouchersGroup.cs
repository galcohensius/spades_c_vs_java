﻿using System;
using System.Collections.Generic;

namespace common.models
{
    /// <summary>
    /// Represents a group of vouchers which have a single value 
    /// </summary>
    public class VouchersGroup
    {
        private List<Voucher> m_vouchers;
        private int m_vouchersValue = -1;
        private TableFilterData m_tableFilterData;

        public Action OnVoucherUpdate;

        public VouchersGroup()
        {
            m_vouchers = new List<Voucher>();
        }

        public void AddVoucher(Voucher voucher)
        {
            if (VouchersValue > 0 && voucher.Value != VouchersValue)
                throw new Exception("Cannot add voucher with value " + voucher.Value + " to list with value " + VouchersValue);

            m_vouchers.Add(voucher);
            m_vouchersValue = voucher.Value;

            m_vouchers.Sort(CompareVouchersByDate);

            OnVoucherUpdate?.Invoke();
        }

        public void AddVouchersGroup(VouchersGroup vouchers)
        {
            foreach (Voucher voucher in vouchers.m_vouchers)
                AddVoucher(voucher);
        }

        public void RemoveExpiredVouchers()
        {
            for (int i = m_vouchers.Count - 1; i >= 0; i--)
            {
                if (m_vouchers[i].ExpireTime <= DateTime.Now)
                {
                    m_vouchers.RemoveAt(i);
                }
            }

            OnVoucherUpdate?.Invoke();
        }



        public Voucher RemoveVoucherById(string voucherId)
        {
            foreach (Voucher voucher in m_vouchers)
            {
                if (voucher.Id == voucherId)
                {
                    m_vouchers.Remove(voucher);
                    OnVoucherUpdate?.Invoke();
                    return voucher;
                }
            }
            return null;
        }

        private int CompareVouchersByDate(Voucher v1, Voucher v2)
        {
            return DateTime.Compare(v1.ExpireTime, v2.ExpireTime);
        }


        public int VouchersValue { get => m_vouchersValue; set => m_vouchersValue = value; }

        public int Count { get => m_vouchers.Count; }

        public DateTime EarliestExpireTime
        {
            get
            {
                if (m_vouchers.Count > 0)
                    return m_vouchers[0].ExpireTime;
                else
                    return DateTime.Now;
            }
        }

        public TableFilterData TableFilterData { get => m_tableFilterData; set => m_tableFilterData = value; }
        public List<Voucher> Vouchers { get => m_vouchers; set => m_vouchers = value; }

        public override string ToString()
        {
            string vouchers = string.Join(",", m_vouchers);
            return string.Format("VoucherGroup: " + vouchers);
        }
    }
}
