﻿using System;
using System.Collections.Generic;

namespace common.models
{
    /// <summary>
    /// Base class for all bonus models
    /// </summary>
    public abstract class Bonus
    {
        public enum BonusTypes
        {
            Hourly = 1,
            Daily = 12, // Old daily=2
            LevelUp = 3,
            FacebookConnect = 4,
            GeneralBonus = 5,
            Collectible = 6,
            Achievement = 7,
            Gift = 8,
            Leaderboard = 9,
            Invite = 10,
            RateUs = 11,
            RewardedVideoBonus = 16,
            RewardedSlotVideoBonus = 19, //daily 
            RewardedHourlyVideoBonus = 20, //hourly
            RewardedVideoLevelUp = 23, //levelup
            RewardedVideoLoseInARow = 24, //lose in a row

        }


        protected List<int> coins = new List<int>();
        public event Action<bool> OnIsEntitledChange;
        protected bool isEntitled;


        public List<int> Coins
        {
            get
            {
                return this.coins;
            }

        }

        public int CoinsSingle
        {
            get
            {
                return coins[0];
            }
        }

        public bool IsEntitled
        {
            get
            {
                return this.isEntitled;
            }
            set
            {
                isEntitled = value;

                if (OnIsEntitledChange != null)
                    OnIsEntitledChange(value);
            }
        }

        public abstract BonusTypes Type
        {
            get;
        }

        public abstract int SubType
        {

            get;
        }

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Bonus p = (Bonus)obj;
            return (Type == p.Type) && (SubType == p.SubType);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }




    }
}

