﻿using System;

namespace common.models
{

	public class HourlyBonus:Bonus {

		int	interval;
		DateTime nextBonusTime;


		public int Interval {
			get {
				return this.interval;
			}
			set {
				interval = value;
				nextBonusTime = DateTime.Now.AddSeconds (interval);
			}
		}

		public DateTime NextBonusTime {
			get {
				return this.nextBonusTime;
			}
		}


		public override BonusTypes Type {
			get {
				return BonusTypes.Hourly;
			}
		}

		public override int SubType {
			get {
				return 0;
			}
		}
	}
}