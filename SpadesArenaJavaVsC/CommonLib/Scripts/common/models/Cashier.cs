﻿using System.Collections.Generic;

namespace common.models
{
	public class Cashier  {

        int m_optimum_group = 0;

		List<CashierItem> m_cashier_packages = new List<CashierItem>();

		string custom_bundle_path;

        string imsMetadata;

        // For tracking
		JSONObject trackingData; 
		bool trackImpressions, trackClicks;

		public void AddPackage(CashierItem cp)
		{
            m_cashier_packages.Add(cp);
		}

		/// <summary>
		/// Used for facebook purchases, to eliminate caching problems
		/// </summary>
		public string CashierVersion {
			get;
			set;
		}

		public string Custom_bundle_path {
			get {
				return custom_bundle_path;
			}
			set {
				custom_bundle_path = value;
			}
		}

        public int Optimum_group
        {
            get
            {
                return m_optimum_group;
            }

            set
            {
                m_optimum_group = value;
            }
        }

        public string ImsMetadata { get => imsMetadata; set => imsMetadata = value; }
        public List<CashierItem> Cashier_packages { get => m_cashier_packages; set => m_cashier_packages = value; }
        public JSONObject TrackingData { get => trackingData; set => trackingData = value; }
        public bool TrackImpressions { get => trackImpressions; set => trackImpressions = value; }
        public bool TrackClicks { get => trackClicks; set => trackClicks = value; }
    }
}