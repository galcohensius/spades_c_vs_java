﻿using common.ims.model;

namespace common.models
{
    public class CashierItem
    {
        string m_id;
        string m_external_id;
        string m_price;
        string m_goods;
        int m_bonus;
        bool m_best_value;
        bool m_most_popular;
        IMSActionValue m_imsActionValue;
        IMSGoodsList m_goodsList;

        public string Id
        {
            get
            {
                return m_id;
            }
            set
            {
                m_id = value;
            }
        }


        public string External_id
        {
            get
            {
                return m_external_id;
            }
            set
            {
                m_external_id = value;
            }
        }


        public string Price
        {
            get
            {
                return m_price;
            }
            set
            {
                m_price = value;
            }
        }

        public string Goods
        {
            get
            {
                return m_goods;
            }
            set
            {
                m_goods = value;
            }
        }

        public int Bonus
        {
            get
            {
                return m_bonus;
            }
            set
            {
                m_bonus = value;
            }
        }

        public bool Best_value
        {
            get
            {
                return m_best_value;
            }
            set
            {
                m_best_value = value;
            }
        }

        public bool Most_popular
        {
            get
            {
                return m_most_popular;
            }
            set
            {
                m_most_popular = value;
            }
        }

        public IMSActionValue ImsActionValue { get => m_imsActionValue; set => m_imsActionValue = value; }
        public IMSGoodsList GoodsList { get => m_goodsList; set => m_goodsList = value; }

        public override string ToString()
        {
            return string.Format("[CashierItem: Id={0}]", Id);
        }
    }
}