﻿using System;

namespace common.models
{
    public class Voucher
    {
        #region Private Members
        private string m_id;
        private DateTime m_expireTime;
        private int m_value;
        #endregion

        public Voucher(string id, DateTime expireTime, int value)
        {
            m_id = id;
            m_expireTime = expireTime;
            m_value = value;
        }

        public Voucher(int value)
        {
            m_value = value;
        }

        public int Value
        {
            get
            {
                return m_value;
            }
        }

        public DateTime ExpireTime
        {
            get
            {
                return m_expireTime;
            }
        }

        public string Id
        {
            get
            {
                return m_id;
            }
        }

        public override string ToString()
        {
            return string.Format("[Voucher: id={0}, value={1}, expiration={2}]", m_id, m_value, m_expireTime);
        }
    }
}
