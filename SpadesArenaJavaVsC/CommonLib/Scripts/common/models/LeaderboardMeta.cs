﻿using System.Collections.Generic;

namespace common.models
{
	public class LeaderboardMeta
	{
		
		private int m_id;
		private int m_minLvl;
		private int m_maxLvl;
        private List<string> m_userRanks = new List<string>();
        private List<int> m_userPrizes = new List<int>();
		private int m_timeToEnd;
		private string m_group;

		public LeaderboardMeta (int leaderboardId,int minLevel,int maxLevel,JSONArray userRanks,JSONArray userPrizes,int timeToEnd,string groupId)
		{
			m_id = leaderboardId;
			m_minLvl = minLevel;
			m_maxLvl = maxLevel;
			m_group = groupId;

            m_userRanks.Clear();
			for (int i=0;i<userRanks.Count;i++)
			{
                m_userRanks.Add(userRanks [i]);
			}

            m_userPrizes.Clear();
			for (int i=0;i<userPrizes.Count;i++)
			{
                m_userPrizes.Add(userPrizes [i]);
			}
				
			m_timeToEnd = timeToEnd;

		}

		public int Id {
			get {
				return m_id;
			}
		}

		public List<string> UserRanks {
			get {
				return m_userRanks;
			}
		}

		public List<int> UserPrizes {
			get {
				return m_userPrizes;
			}
		}

		public int TimeToEnd {
			get {
				return m_timeToEnd;
			}
		}

		public string GroupId {
			get {
				return m_group;
			}
		}

		public int MaxLvl {
			get {
				return m_maxLvl;
			}
		}

		public int MinLvl {
			get {
				return m_minLvl;
			}
		}
	}
}
