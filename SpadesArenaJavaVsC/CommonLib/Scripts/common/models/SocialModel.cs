﻿using System.Collections.Generic;
using System;
using common.facebook.models;

namespace common.models
{
	public class SocialModel 
	{
		public delegate void AppRequestChangedDelegate(int count, bool added);

		List<FacebookData>	m_invites = new List<FacebookData>();
		List<FacebookData>	m_send_gifts = new List<FacebookData>();
		List<FBRequestData>	m_receive_gifts = new List<FBRequestData>();

		public AppRequestChangedDelegate AppRequestsChanged;
		public  Action<string> FBFriendsChanged;
		public  Action<string> FBInvitableFriendsChanged;


		public List<FacebookData> InvitableFriends {
			get {
				return m_invites;
			}
			set {
				m_invites = value;

				if (FBInvitableFriendsChanged != null)
					FBInvitableFriendsChanged ("");
			}
		}

		public List<FacebookData> AppFriends {
			get {
				return m_send_gifts;
			}
			set {
				m_send_gifts = value;

				if (FBFriendsChanged != null)
					FBFriendsChanged ("");

			}
		}

        public void AddAppRequest(IEnumerable<FBRequestData> fb_req_datas)
		{
            foreach (var fb_req_data in fb_req_datas)
            {
                m_receive_gifts.Add (fb_req_data);
            }

			if (AppRequestsChanged != null)
				AppRequestsChanged (m_receive_gifts.Count,true);
		}

		public void RemoveAppRequest(FBRequestData fb_req_data)
		{
			m_receive_gifts.Remove (fb_req_data);

			if (AppRequestsChanged != null)
				AppRequestsChanged (m_receive_gifts.Count,false);
		}

		public void RemoveAllAppRequests()
		{
			m_receive_gifts.Clear ();

			if (AppRequestsChanged != null)
				AppRequestsChanged (m_receive_gifts.Count,false);

		}


		public List<FBRequestData> App_Requests {
			get {
				return m_receive_gifts;
			}

		}
	}
}