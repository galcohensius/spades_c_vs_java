﻿using System;
using UnityEngine.SignInWithApple;

namespace common.models
{
    /// <summary>
    /// basic user data in CommonLib realm. CardGames have its derived user with Avatars and UserStats that are not common.
    /// </summary>
    public abstract class CommonUser 
    {
        public enum UserType
        {
            NonPaying = 1,
            Paying = 2
        }

        public enum AccountStatus
        {
            BanFromLogin,
            Active,
            BanFromPurchase,
            WarnBeforeBan
        }

        protected int m_user_id;
        protected int m_coins_balance;
        protected int m_xp_in_level;
        protected int m_level;
        protected string m_country;
        protected int m_total_xp_in_level;
        protected bool m_send_match_history;
        protected UserType m_user_type;
        protected int m_uvs_level;
        protected int m_activity_level;
        protected int m_days_from_reg;
        private AccountStatus m_account_status;

        protected UserInfo m_apple_data;

        public event Action OnXPChange;

        public event Action OnBalanceChange;

        public CommonUser()
        {
            m_user_id = 0;
            m_coins_balance = 0;
            m_level = 0;
            m_xp_in_level = 0;
            m_country = "";
        }


        public void SetMatchHistoryFlag(bool match_history)
        {
            m_send_match_history = match_history;
        }

        public bool GetMatchHistoryFlag()
        {
            return m_send_match_history;
        }

        public string GetCountry()
        {
            return m_country;
        }

        public void SetCountry(string country)
        {
            m_country = country;
        }

        public void Add_Coins(int value)
        {
            m_coins_balance += value;
            OnBalanceChange?.Invoke();

            CrashReportHandler.SetUserMetadata("balance", m_coins_balance.ToString());
        }

        public void SetCoins(int coins)
        {
            m_coins_balance = coins;
            OnBalanceChange?.Invoke();

            CrashReportHandler.SetUserMetadata("balance", m_coins_balance.ToString());
        }



        public void SetLevel(int level, int total_xp_in_level, int xp_in_level)
        {
            m_level = level;
            m_xp_in_level = xp_in_level;
            m_total_xp_in_level = total_xp_in_level;

            OnXPChange?.Invoke();

            // For unity crash reporting
            CrashReportHandler.SetUserMetadata("level", level.ToString());
        }

        public void SetXPInLevel(int xp)
        {
            m_xp_in_level = xp;
            if (OnXPChange != null)
                OnXPChange();

        }

        public void SetID(int id)
        {
            m_user_id = id;
        }

        public int GetLevelTotalXP()
        {
            return m_total_xp_in_level;
        }

        public int GetLevel()
        {
            return m_level;
        }

        public int GetCoins()
        {
            return m_coins_balance;
        }

        public int GetXPInLevel()
        {
            return m_xp_in_level;
        }

        public int GetID()
        {
            return m_user_id;
        }

        public UserInfo apple_data
        {
            get
            {
                return m_apple_data;
            }
            set
            {
                m_apple_data = value;
            }
        }


        public UserType User_type
        {
            get
            {
                return m_user_type;
            }

            set
            {
                m_user_type = value;
            }
        }

        public bool NeverPlayed
        {
            get
            {

                return m_xp_in_level == 0 && m_level == 1;
            }

        }

        public int Uvs_level
        {
            get
            {
                return m_uvs_level;
            }

            set
            {
                m_uvs_level = value;
            }
        }

        public int Activity_level
        {
            get
            {
                return m_activity_level;
            }

            set
            {
                m_activity_level = value;
            }
        }

        public int Days_from_reg
        {
            get
            {
                return m_days_from_reg;
            }

            set
            {
                m_days_from_reg = value;
            }
        }

        public AccountStatus Account_Status { get => m_account_status; set => m_account_status = value; }
    }
}