﻿using System;
using System.Collections.Generic;

namespace common.models
{
	public class LeaderboardBonus: Bonus
	{
		

		public enum LeaderboardSubType
		{
			Global=1,Country,Friends
		}

	
		private LeaderboardSubType m_bonus_sub_type;
		private List<string> m_places;
		private int lowestRewardPlace;

		public LeaderboardBonus (int sub_type)
		{
			m_bonus_sub_type = (LeaderboardBonus.LeaderboardSubType) sub_type;

			m_places = new List<string> ();
		}

		public override BonusTypes Type {
			get {
				return BonusTypes.Leaderboard;
			}
		}

		public override int SubType {
			get {
				return Convert.ToInt32(m_bonus_sub_type);
			}
		}

		public List<string> Places
		{
			get {
				return m_places;
			}
		}

		public int LowestRewardPlace {
			get {
				return lowestRewardPlace;
			}
			set {
				lowestRewardPlace = value;
			}
		}

			
	}
}

