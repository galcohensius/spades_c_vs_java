﻿using System;

namespace UnityEngine.UI
{
    /// <summary>
    /// fires event when drag is beginning with distiction between value increases and decreases. Doesn't care about the value itself
    /// so it doesn't use OnValueChange event, only the dragging
    /// </summary>
    public class SliderValueChangingEventPropagator : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerUpHandler
    {
        private Slider slider;
        private Slider.Direction direction;

        private bool lastDirection;

        public Action OnSliderValueIncreased;
        public Action OnSliderValueDecreased;
        public Action OnSliderPointerUp;
        public Action OnSliderDargEnd;

        public bool pointerUpAndDragEnd = false;

        // Start is called before the first frame update
        void Start()
        {
            slider = GetComponent<Slider>();
            direction = slider.direction;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            switch (direction)
            {
                case Slider.Direction.LeftToRight:
                    if (eventData.delta.x > 0)
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                    }
                    else
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                    }
                    break;
                case Slider.Direction.RightToLeft:
                    if (eventData.delta.x > 0)
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                    }
                    else
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                    }
                    break;
                case Slider.Direction.BottomToTop:
                    if (eventData.delta.y > 0)
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                    }
                    else
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                    }
                    break;
                case Slider.Direction.TopToBottom:
                    if (eventData.delta.y > 0)
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                    }
                    else
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                    }
                    break;
            }
        }

        public void OnDrag(PointerEventData eventData)
        {
            switch (direction)
            {
                case Slider.Direction.LeftToRight:
                    if (eventData.delta.x > 0 && !lastDirection)
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                        return;
                    }
                    if (eventData.delta.x < 0 && lastDirection)
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                        return;
                    }
                    break;
                case Slider.Direction.RightToLeft:
                    if (eventData.delta.x > 0 && lastDirection)
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                        return;
                    }
                    if (eventData.delta.x < 0 && !lastDirection)
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                        return;
                    }
                    break;
                case Slider.Direction.BottomToTop:
                    if (eventData.delta.y > 0 && !lastDirection)
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                        return;
                    }
                    if (eventData.delta.y < 0 && lastDirection)
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                        return;
                    }
                    break;
                case Slider.Direction.TopToBottom:
                    if (eventData.delta.y > 0 && lastDirection)
                    {
                        OnSliderValueDecreased?.Invoke();
                        lastDirection = false;
                        return;
                    }
                    if (eventData.delta.y < 0 && !lastDirection)
                    {
                        OnSliderValueIncreased?.Invoke();
                        lastDirection = true;
                        return;
                    }
                    break;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnSliderPointerUp?.Invoke();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (pointerUpAndDragEnd)
                OnSliderDargEnd?.Invoke();
        }
    }
}