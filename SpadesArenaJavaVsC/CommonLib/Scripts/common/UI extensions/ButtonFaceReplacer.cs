﻿using common.controllers;

namespace UnityEngine.UI
{
    /// <summary>
    /// upon clicking a button replaces its sprites with other allowing to "toggle" normal button.
    /// </summary>
    [RequireComponent(typeof(Button))]
    public class ButtonFaceReplacer : MonoBehaviour
    {
        public Sprite altNormalState;
        public Sprite altHoverState;
        public Sprite altPressedState;
        public Sprite altDisabledState;

        private Sprite originalNormalState;
        private SpriteState originalSpriteState = new SpriteState();
        private SpriteState altSpriteState = new SpriteState();

        private Button button;

        public bool originalState = true;
        public bool debugSetup = false;

        private void Awake()
        {
            button = GetComponent<Button>();
            if (!button)
            {
                LoggerController.Instance.LogError("no button found to use alt art on it");
                return;
            }

            if (button.transition != Selectable.Transition.SpriteSwap)
            {
                if (debugSetup)
                    LoggerController.Instance.LogWarning("button was not set to sprite swap. weird behaviour may happen");
                button.transition = Selectable.Transition.SpriteSwap;
            }

            if (altNormalState)
            {//button should start at its original idle state
                originalNormalState = button.image.sprite;
            }
            else
            {
                if (debugSetup)
                    LoggerController.Instance.LogError("no alternative normal state provided");
            }

            if (altHoverState)
            {
                altSpriteState.highlightedSprite = altHoverState;
            }
            else
            {
                if (debugSetup)
                    LoggerController.Instance.LogWarning("no alternative hover state provided");
            }

            if(altPressedState)
                altSpriteState.pressedSprite = altPressedState;
            else
            {
                if (debugSetup)
                    LoggerController.Instance.LogWarning("no alternative pressed state provided");
            }

            if (altDisabledState)
                altSpriteState.disabledSprite = altDisabledState;
            else
            {
                if (debugSetup)
                    LoggerController.Instance.LogWarning("no alternative disabled state provided");
            }

            originalSpriteState.disabledSprite = button.spriteState.disabledSprite;
            originalSpriteState.highlightedSprite = button.spriteState.highlightedSprite;
            originalSpriteState.pressedSprite = button.spriteState.pressedSprite;
        }

        private void OnEnable()
        {
            button.onClick.AddListener(SwitchArt);
        }

        /// <summary>
        /// will fail if OnClick throws exception and doesn't run all the code
        /// </summary>
        public void SwitchArt()
        {
            if (originalState)
            {
                button.image.sprite = altNormalState;
                button.spriteState = altSpriteState;
            }
            else
            {
                button.image.sprite = originalNormalState;
                button.spriteState = originalSpriteState;
            }

            originalState = !originalState;
        }

        private void OnDisable()
        {
            button.onClick.RemoveListener(SwitchArt);
        }
    }
}