﻿namespace UnityEngine.UI
{
    public class SliderExtremeSelectButton : Button
    {
        public enum Extreme { Min, Max };

        public Sprite selectedSprite;
        public Sprite selectedSpriteInnerOn;
        public Sprite selectedSpriteInnerOff;
        public Image targetInnerImage;
        private Sprite normalSprite;

        [SerializeField]
        private Slider targetSlider;

        public Extreme extreme = Extreme.Min;
        public bool InExtreme { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            normalSprite = image.sprite;
            if (targetSlider == null)
            {
                Debug.LogError("No accompanied slider assigned for " + gameObject.name + ". Turning off.");
                this.enabled = false;
            }
            else
                this.enabled = true;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            targetSlider.onValueChanged.AddListener(OnValueChanged);
        }

        protected override void Start()
        {
            base.Start();
            //get value from playerprefs
            OnValueChanged(targetSlider.value);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            IndicateExtreme();
        }

        private void IndicateExtreme()
        {
            image.sprite = selectedSprite;
            InExtreme = true;
            targetSlider.value = extreme == Extreme.Min ? targetSlider.minValue : targetSlider.maxValue;
        }

        public void OnValueChanged(float value)
        {
            if (targetSlider.value == 0)
                targetInnerImage.sprite = selectedSpriteInnerOff;
            else
                targetInnerImage.sprite = selectedSpriteInnerOn;

            if ((extreme == Extreme.Min && value == targetSlider.minValue) || (extreme == Extreme.Max && value == targetSlider.maxValue))
            {
                IndicateExtreme();
            }
            else
            {
                if (InExtreme == true)
                {
                    InExtreme = false;
                    image.sprite = normalSprite;
                }
            }

        }

        protected override void OnDisable()
        {
            base.OnDisable();
            targetSlider.onValueChanged.RemoveListener(OnValueChanged);
        }

    }
}