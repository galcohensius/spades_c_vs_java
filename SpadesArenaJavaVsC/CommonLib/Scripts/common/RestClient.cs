using System;
using System.Collections;
using System.Text;
using System.Security.Cryptography;
using common.utils;
using common.controllers;
using System.Collections.Generic;

namespace common
{
    public delegate void ResponseDelegate(bool success, JSONNode responseData);

    public class RestClient : MonoBehaviour
    {
        const int SLOW_CONN_THRESHOLD_SEC = 5;
        public const int CONN_TIMEOUT_SEC = 60;
        public const string INVALID_LOGIN_TOKEN = "INVALID_LOGIN_TOKEN";


        const int ERROR_DUPLICATE_LOGIN = 801;
        const int ERROR_DUPLICATE_MESSAGE = 802; //A message with the same signature was already processed
        const int ERROR_USER_BANNED = 803;
        const int ERROR_MAINTENANCE = 5002;
        const int ERROR_DUPLICATE_COMMAND = 10101; // A command with the same action was already processed (e.g 2 end game commands)


        [Header("Default URLs")]
        [SerializeField] string baseUrl = null;
        [SerializeField] string baseUrlQA1Mode = null;
        [SerializeField] string baseUrlQA2Mode = null;
        [SerializeField] string baseUrlDevMode = null;

        [Header("Config settings")]
        [SerializeField] string prodUrlConfigKey = null;



        private Action<string,long,string> m_network_error_callback;
        private Action m_duplicate_login_error_callback;
        private Action m_banned_user_callback;
        private Action m_maintenance_proc_callback;
        private Action<bool> m_slow_connection_callback;
        private Action<string> m_connection_timeout_callback;

        //public delegate void ResponseReceived (string command_name);

        float m_last_communication = 0;

        private string m_url;

        private string m_login_token;

        //for enabling send request again
        string m_last_sent_command;
        string m_last_sent_commandPrefix;
        string m_last_sent_requestData;
        ResponseDelegate m_last_respDelegate;
        MessageSignatureData m_last_sign_data;
        bool m_isAutoRetry;



        SHA1 m_sha1 = new SHA1CryptoServiceProvider();
        string m_dp; // Password for SHA-1

        Coroutine m_send_request_coroutine = null;
        Coroutine m_slow_conn_check_coroutine = null;

        HashSet<String> m_errorIgnoreCommands = new HashSet<string>();
        HashSet<String> m_logIgnoreCommands = new HashSet<string>();

        public void Start()
        {
            // Load the base url from config file (for prod only!)
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
#if DEV_MODE
			    m_url = baseUrlDevMode;
#elif QA_MODE
                UpdateQAUrlByEnvIndex(StateController.Instance.GetQAEnvironment());
                StateController.Instance.OnQAEnvChanged += UpdateQAUrlByEnvIndex;
#else
                m_url = ldc.GetSetting(prodUrlConfigKey, baseUrl);
#endif
                LoggerController.Instance.Log(LoggerController.Module.Comm, "Using Server URL: " + m_url);

            };


        }

        public void SendRequest(string command, JSONNode requestData, ResponseDelegate respDelegate, bool signRequest = false, string commandPrefix = null)
        {
            if (!m_errorIgnoreCommands.Contains(command))
            {
                // Do not store the last command for error ignore commands
                m_last_sent_command = command;
                m_last_sent_commandPrefix = commandPrefix;
                m_last_sent_requestData = requestData.ToString();
                m_last_respDelegate = respDelegate;
            } else
            {
                m_last_sent_command = null;
            }

            // Create message signature if needed
            MessageSignatureData signatureData = default(MessageSignatureData);
            if (signRequest)
            {
                // Calc digest
                signatureData.utp = DateUtils.JavaTimestamp();
                signatureData.digest = CreateSignature(requestData.ToString(), signatureData.utp);
            }

            m_last_sign_data = signatureData;


            m_send_request_coroutine = StartCoroutine(SendRequestCoroutine(command, requestData.ToString(), respDelegate, signatureData, commandPrefix));

        }

        private IEnumerator SendRequestCoroutine(string command, string requestJson, ResponseDelegate respDelegate, MessageSignatureData signData, string commandPrefix,
                                                 bool isRetry = false)
        {

            //this is a way for us to avoid getting commands sent with wrong login token which results in error 801
            if (m_login_token == INVALID_LOGIN_TOKEN && !isRetry)
                yield break;

            string commandUrl = m_url + "/" + command;

            if (m_logIgnoreCommands.Contains(command) ==false)
                LoggerController.Instance.Log(LoggerController.Module.Comm, "Request '" + commandUrl + "' sent: " + requestJson + ", signature: " + signData.digest+ ", isRetry: "+isRetry);


            if(commandPrefix!=null)
                commandUrl = m_url + "/" + commandPrefix +"/" + command;

            using (UnityWebRequest request = new UnityWebRequest(commandUrl, "POST"))
            {
                request.SetRequestHeader("Content-Type", "application/json");
                request.SetRequestHeader("Cache-Control", "no-cache");
                if (m_login_token != null)
                {
                    request.SetRequestHeader("Auth-Token", m_login_token);
                }

                if (signData.utp > 0)
                {
                    request.SetRequestHeader("utp", signData.utp + "");
                    request.SetRequestHeader("Digest", signData.digest);
                }

                if (isRetry)
                    request.SetRequestHeader("Retry", "1");

#if !UNITY_WEBGL
                request.SetRequestHeader("Accept-Encoding", "gzip");
#endif

                UploadHandler uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(requestJson));
                uploadHandler.contentType = "application/json";

                DownloadHandler downloadHandler = new DownloadHandlerBuffer();

                request.uploadHandler = uploadHandler;
                request.downloadHandler = downloadHandler;

                CommRequestSent();

                yield return request.SendWebRequest();

                CommResponseReceived();

                m_last_communication = Time.realtimeSinceStartup;
                  
                if (request.isNetworkError || request.isHttpError)
                {
                    LoggerController.Instance.Log(LoggerController.Module.Comm, "Error response for '" + command + "': " + request.error);
                    if (command != m_last_sent_command && m_last_sent_command!=null)
                    {
                        // Another command was already sent - Do nothing with the error
                        LoggerController.Instance.Log(LoggerController.Module.Comm, "Another command already sent: " + m_last_sent_command + ", ignoring error");

                    }
                    else if(m_isAutoRetry || m_errorIgnoreCommands.Contains(command))
                    {
                        if(!m_errorIgnoreCommands.Contains(command))
                        {
                            LoggerController.Instance.LogError(LoggerController.Module.Comm, "Error response for '" + command + "': " + request.error);
                            m_network_error_callback?.Invoke(command,request.responseCode,request.error);
                        }

                        m_isAutoRetry = false;

                        //respDelegate(false, null);  DO NOT CALL THIS ONE ON NETWORK ERROR, SINCE ANOTHER EVENT IS INVOKED (RAN 3/12/2019)
                    }
                    else
                    {
                        LoggerController.Instance.Log(LoggerController.Module.Comm, "Auto retrying command '" + command + "'");
                        m_isAutoRetry = true;
                        SendRequestAgain();
                    }
                    
                }
                else
                {
                    string responseData = downloadHandler.text;

                    // Check gzip response
                    if (request.GetResponseHeader("Content-Encoding") == "gzip" &&
                        GZipCompressor.IsGZipCompressedData(downloadHandler.data))
                    {
                        responseData = GZipCompressor.DecompressBytes(downloadHandler.data);
                    }

                    JSONNode respData = JSON.Parse(responseData)[command];

                    if(respData.Count == 0)
                    {
                        LoggerController.Instance.LogError(LoggerController.Module.Comm, "Response '" + command + "' is empty");
                        respDelegate(false, respData);
                        yield break;
                    }

                    if (m_logIgnoreCommands.Contains(command) == false)
                        LoggerController.Instance.Log(LoggerController.Module.Comm, "Response '" + command + "' received: " + respData.ToString());

                    m_isAutoRetry = false;

                    string status = respData["sts"];
                    if (respDelegate != null)
                    {
                        if (status?.ToUpper() == "S")
                        {
                            respDelegate(true, respData);
                        }
                        else
                        {
                            int errorNum = respData["eNr"].AsInt;
                            if (errorNum == ERROR_DUPLICATE_LOGIN)
                            {
                                if (!m_errorIgnoreCommands.Contains(command))
                                    m_duplicate_login_error_callback?.Invoke();
                            }
                            else if (errorNum == ERROR_USER_BANNED)
                            {
                                m_banned_user_callback?.Invoke();
                            }
                            else if (errorNum == ERROR_MAINTENANCE) 
                            {
                                m_maintenance_proc_callback?.Invoke();
                            }
                            else if (errorNum == ERROR_DUPLICATE_COMMAND || errorNum == ERROR_DUPLICATE_MESSAGE)
                            {
                                // Ignore the response
                            }
                            else
                            {
                                // Check that the failed command is the last command sent.
                                // This check is to prevent the following scenario:
                                // Command A is sent and no response (client error). Command B is send with a successful response. Command A is returned with an error. 
                                // The user clicks retry - Commnand B is sent again..
                                if (command == m_last_sent_command || m_last_sent_command==null)
                                    respDelegate(false, respData);
                                else
                                    LoggerController.Instance.Log("Ignoring command error for command: " + command + ". Last sent command: " + m_last_sent_command);
                            }
                        }
                    }
                }

            }

        }

        /// <summary>
        /// Sends the same last request again, including the signature
        /// </summary>
        public void SendRequestAgain()
        {
            LoggerController.Instance.Log(LoggerController.Module.Comm, "Sending request again with command: " + m_last_sent_command);

            CancelSendRequest();

            if (m_last_sent_command!=null)
            {
                m_send_request_coroutine = StartCoroutine(SendRequestCoroutine(m_last_sent_command, m_last_sent_requestData,
                                                                           m_last_respDelegate, m_last_sign_data,m_last_sent_commandPrefix, true));

            }

        }

        public void CancelSendRequest()
        {
            if (m_send_request_coroutine != null)
                StopCoroutine(m_send_request_coroutine);

            m_send_request_coroutine = null;
        }


        private void CommRequestSent()
        {
            CommResponseReceived();

            m_slow_conn_check_coroutine = StartCoroutine(SlowConnectionCheck());

        }

        IEnumerator SlowConnectionCheck()
        {
            // Wait for slow connection indication
            yield return new WaitForSecondsRealtime(SLOW_CONN_THRESHOLD_SEC);

            if (m_slow_connection_callback != null)
                m_slow_connection_callback(true);

            // Wait some more for full timeout
            yield return new WaitForSecondsRealtime(CONN_TIMEOUT_SEC - SLOW_CONN_THRESHOLD_SEC);

            // Cancel the current request
            CancelSendRequest();

            // Invoke the timeout callback
            if (m_connection_timeout_callback != null)
                m_connection_timeout_callback(m_last_sent_command);

        }

        private void CommResponseReceived()
        {
            if (m_slow_conn_check_coroutine != null)
            {
                StopCoroutine(m_slow_conn_check_coroutine);
                m_slow_conn_check_coroutine = null;
            }

            if (m_slow_connection_callback != null)
                m_slow_connection_callback(false);
        }


        private String CreateSignature(string requestJson, long timeStamp)
        {

            string fullText = requestJson + m_dp + timeStamp;

            byte[] hash = m_sha1.ComputeHash(Encoding.UTF8.GetBytes(fullText));

            return Convert.ToBase64String(hash);

        }

        private void UpdateQAUrlByEnvIndex(int index)
        {
            switch (index) {
                case 2:
                    m_url = baseUrlQA2Mode;
                    break;
                default:
                    m_url = baseUrlQA1Mode;
                    break;
            }
        }

        public string Login_token
        {
            get
            {
                return m_login_token;
            }
            set
            {
                m_login_token = value;
            }
        }

        public float Last_communication
        {
            get
            {
                return m_last_communication;
            }

        }


        public Action<bool> SlowConnectionCallback
        {
            get
            {
                return m_slow_connection_callback;
            }
            set
            {
                m_slow_connection_callback = value;
            }
        }

        public Action<string> ConnectionTimeoutCallback
        {
            get
            {
                return m_connection_timeout_callback;
            }

            set
            {
                m_connection_timeout_callback = value;
            }
        }

        public Action<string,long,string> NetworkErrorCallback
        {
            get
            {
                return m_network_error_callback;
            }
            set
            {
                m_network_error_callback = value;
            }
        }

        public Action DuplicateLoginCallback
        {
            get
            {
                return m_duplicate_login_error_callback;
            }
            set
            {
                m_duplicate_login_error_callback = value;
            }
        }

        public Action BannedUserCallback
        {
            get
            {
                return m_banned_user_callback;
            }
            set
            {
                m_banned_user_callback = value;
            }
        }

        public Action MaintainanceProcedureCallback
        {
            get
            {
                return m_maintenance_proc_callback;
            }

            set
            {
                m_maintenance_proc_callback = value;
            }
        }



        // Password for message signature
        public byte[] Dp
        {
            set
            {
                m_dp = Encoding.ASCII.GetString(value);
            }
        }

        public string APIServerUrl { get => m_url; }

        public HashSet<string> ErrorIgnoreCommands { get => m_errorIgnoreCommands; }
        public HashSet<string> LogIgnoreCommands { get => m_logIgnoreCommands; set => m_logIgnoreCommands = value; }
    }

    public struct MessageSignatureData
    {
        public long utp;
        public string digest;
    }
}

