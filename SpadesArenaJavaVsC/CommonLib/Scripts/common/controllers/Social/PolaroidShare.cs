﻿using cardGames.controllers;
using common.controllers.social.sharer;
using System;
using System.Collections;
using System.IO;
using System.Threading;

namespace common.controllers.social
{
    public class PolaroidShare : MediaShareBase
    {
        private bool success = false;
        public bool CroppingSuccess { get { return success; } }
        private GameObject Selection;
        private Texture2D screenshot;
        private Texture2D croppedScreenshot;
        private int realworldCroppedwidth = 0;
        private int realworldCroppedHeight = 0;
        private bool doUseSwipeBottomAssets = false;
        private bool doRotateImage = false;
        private GameObject sharerParent;
        private RawImage croppedImageHolderForTests;

        public void Init(SharingConfig shareParams, GameObject sharer, GameObject selection, bool useSwipeAssets, bool rotateImage)
        {
            base.Init(shareParams);
            Selection = selection;
            doUseSwipeBottomAssets = useSwipeAssets;
            doRotateImage = rotateImage;
            sharerParent = sharer;
            BetterStreamingAssets.Initialize();
        }

        public override IEnumerator CreateImageFileMedia(Action<bool> onFinish)
        {
            yield return SocialController.Instance.StartCoroutine(CreatePolaroid());
            onFinish(success);
            Debug.Log("finished");
        }

        private Texture2D GetRandomImageFromFolder(string subFolder, string folderHighPriority, string folderLowPriority)
        {
            string[] imageFiles;
            string imageFile = "";
            string baseFolder = parameters.baseImagesFolder;


            string imagesPath = baseFolder + "/" + folderHighPriority + "/" + subFolder;
            if (BetterStreamingAssets.DirectoryExists(imagesPath))
            {

                imageFiles = BetterStreamingAssets.GetFiles(imagesPath).Where(name => !name.EndsWith(".meta")).ToArray<string>();
                if (imageFiles.Length > 0)
                {
                    imageFile = imageFiles[UnityEngine.Random.Range(0, imageFiles.Length)];
                }
            }
            if (imageFile == "")
            {
                imagesPath = baseFolder + "/" + folderLowPriority + "/" + subFolder;
                if (BetterStreamingAssets.DirectoryExists(imagesPath))
                {

                    imageFiles = BetterStreamingAssets.GetFiles(imagesPath).Where(name => !name.EndsWith(".meta")).ToArray<string>();
                    if (imageFiles.Length > 0)
                    {
                        imageFile = imageFiles[UnityEngine.Random.Range(0, imageFiles.Length)];
                    }
                }
            }

            if (imageFile != "")
            {
                //Converts desired path into byte array
                byte[] imageBytes = BetterStreamingAssets.ReadAllBytes(imageFile);

                //Creates texture and loads byte array data to create image
                Texture2D imageTexture = new Texture2D(2, 2);
                imageTexture.LoadImage(imageBytes);
                imageTexture.Apply();
                return imageTexture;
            }

            return null;
        }

        private IEnumerator CreatePolaroid()
        {
            DeleteOldMediaFile(parameters.shareFileName);
            Texture2D sharedImage = null;
            if (parameters.useScreenshot)
            {
                yield return SocialController.Instance.StartCoroutine(CropScreenshot());

                if (success == true)
                {
                    Debug.Log("Share Instagram -> CropScreenshot result = " + success.ToString());
                    if (success == true)
                    {

                        sharedImage = croppedScreenshot;

                        if (parameters.useFrame)
                        {
                            Texture2D frameTexture = GetRandomImageFromFolder(parameters.frameImagesFolder, parameters.eventName, parameters.generalFolder);

                            // Scale the cropped screenshot
                            int newSharedImageWidth = (int)(frameTexture.width - ((float)frameTexture.width / 16));
                            int newSharedImageHeight = (int)((float)((float)newSharedImageWidth / (float)sharedImage.width) * (float)sharedImage.height);
                            TextureScale.Point(sharedImage, newSharedImageWidth, newSharedImageHeight);

                            int topMargin = (frameTexture.width - sharedImage.width) / 2;

                            sharedImage = Overlay(frameTexture, false,
                                sharedImage, frameTexture.width, frameTexture.height,
                                OverlayHorizonalPosition.Middle, OverlayVerticalPosition.Top,
                                topMargin, 0, 0, 0, false);

                            // Get frame text
                            Texture2D frameTextTexture = GetRandomImageFromFolder(parameters.frameTextAssetsFolder, parameters.eventName, parameters.generalFolder);
                            // Adding the frame text
                            if (frameTextTexture != null)
                            {
                                sharedImage = Overlay(sharedImage, false,
                                    frameTextTexture, 0,0,
                                    OverlayHorizonalPosition.Middle, OverlayVerticalPosition.Bottom,
                                    0, 0, 0, 0, false);
                            }

                        }

                        // Rotating
                        if (doRotateImage && parameters.rotateImage)
                        {
                            ImageRotator rotator = new ImageRotator();
                            sharedImage = rotator.SetRotate(sharedImage, parameters.rotateAngle);
                        }

                        // Background

                        if (parameters.useBackground)
                        {
                            //Texture2D BGTexture = parameters.bgImages[UnityEngine.Random.Range(0, parameters.bgImages.Length)];
                            Texture2D BGTexture = GetRandomImageFromFolder(parameters.bgImagesFolder, parameters.eventName, parameters.generalFolder);

                            if (BGTexture != null)
                            {
                                //BGTexture = Resize(BGTexture, 460, 819);
                                TextureScale.Point(BGTexture, 460, 819);

                                sharedImage = Overlay(BGTexture, false,
                                    sharedImage, sharedImage.width, sharedImage.height,
                                    OverlayHorizonalPosition.Middle, OverlayVerticalPosition.Top,
                                    (int)(BGTexture.height / 4.8), 0, 0, 0, false);

                                // Top asset text
                                //Texture2D BGTopAssetTexture = parameters.topAssets[UnityEngine.Random.Range(0, parameters.topAssets.Length)];
                                Texture2D BGTopAssetTexture = GetRandomImageFromFolder(parameters.topAssetsFolder, parameters.eventName, parameters.generalFolder);

                                if (BGTopAssetTexture != null)
                                {
                                    sharedImage = Overlay(sharedImage, false,
                                        BGTopAssetTexture, BGTopAssetTexture.width, BGTopAssetTexture.height,
                                        OverlayHorizonalPosition.Middle, OverlayVerticalPosition.Top,
                                        (int)(BGTexture.height / 11), 0, 0, 0, false);
                                }

                                Texture2D BGBottomAssetTexture = null;
                                int bottomMargin = 0;
                                if (doUseSwipeBottomAssets)
                                {
                                    BGBottomAssetTexture = GetRandomImageFromFolder(parameters.bottomSwipeAssetsFolder, parameters.eventName, parameters.generalFolder);
                                    bottomMargin = (int)(BGTexture.height / 12);
                                }
                                else
                                {
                                    BGBottomAssetTexture = GetRandomImageFromFolder(parameters.bottomAssetsFolder, parameters.eventName, parameters.generalFolder);
                                    bottomMargin = (int)(BGTexture.height / 14.2);
                                }
                                // Bottom asset text - Swipe
                                if (BGBottomAssetTexture != null)
                                {
                                    sharedImage = Overlay(sharedImage, false,
                                        BGBottomAssetTexture, BGBottomAssetTexture.width, BGBottomAssetTexture.height,
                                        OverlayHorizonalPosition.Middle, OverlayVerticalPosition.Bottom,
                                         0, bottomMargin, 0, 0, false);
                                }


                                // Logo
                                //Texture2D LogoAssetTexture = parameters.gameLogo[UnityEngine.Random.Range(0, parameters.gameLogo.Length)];
                                Texture2D LogoAssetTexture = GetRandomImageFromFolder(parameters.gameLogoFolder, parameters.eventName, parameters.generalFolder);
                                if (LogoAssetTexture != null)
                                {
                                    sharedImage = Overlay(sharedImage, false,
                                        LogoAssetTexture, LogoAssetTexture.width, LogoAssetTexture.height,
                                        OverlayHorizonalPosition.Middle, OverlayVerticalPosition.Bottom,
                                        0, (int)(BGTexture.height / 6.6), 0, 0, false);
                                }

                            }

                        }
                    }
                    if (SaveTextureAsPNG(parameters.shareFileName, sharedImage))
                    {
                        success = true;
                    }
                }
            }
            else
            {
                // Use a ready made image
            }

        }

        private IEnumerator CropScreenshot()
        {
            yield return (ScreenshotCrop());
            Debug.Log("Crop Screenshot, result = " + success.ToString());
        }

        private void TakeScreenshot()
        {
            screenshot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGBA32, false);
            screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
            screenshot.Apply();
        }

        private Texture2D CropImage(Texture2D source, int width, int height, int xOffset, int yOffset)
        {
            if (width <= 0 || width > source.width)
                width = source.width;
            if (height <= 0 || height > source.height)
                height = source.height;

            if (xOffset <= 0)
                xOffset = 0;
            else if (xOffset + width > source.width)
                xOffset = source.width - width;

            if (yOffset <= 0)
                yOffset = 0;
            else if (yOffset + height > source.height)
                yOffset = source.height - height;

            Texture2D result = new Texture2D(width, height, source.format, source.mipmapCount > 0);
            result.SetPixels(source.GetPixels(xOffset, yOffset, width, height));
            result.Apply();

            return result;
        }

        private IEnumerator ScreenshotCrop()
        {
            yield return new WaitForEndOfFrame();

            //Get screenshot
            TakeScreenshot();

            if (screenshot != null)
            {
                RectTransform selectionTransform = (RectTransform)Selection.transform;

                Canvas parentCanvas = sharerParent.GetComponentInParent<Canvas>();
                float canvasScaleFactor = parentCanvas.scaleFactor;
                Vector2 selectionSizeDelta = selectionTransform.sizeDelta;

                Vector2 selectionScale = new Vector2(selectionSizeDelta.x * canvasScaleFactor, selectionSizeDelta.y * canvasScaleFactor);
                Camera cam = Camera.main;
                Vector3 screenPos = cam.WorldToScreenPoint(Selection.transform.position);


                realworldCroppedwidth = (int)selectionScale.x;
                realworldCroppedHeight = (int)selectionScale.y;

                int xOffset = (int)screenPos.x - realworldCroppedwidth / 2;
                int yOffset = (int)screenPos.y - realworldCroppedHeight / 2;

                croppedScreenshot = CropImage(screenshot, realworldCroppedwidth, realworldCroppedHeight, xOffset, yOffset);

                success = true;
            }
            else
            {
                success = false;
            }
        }

        private static Texture2D Resize(Texture2D source, int newWidth, int newHeight)
        {
            source.filterMode = FilterMode.Point;
            RenderTexture rt = RenderTexture.GetTemporary(newWidth, newHeight);
            rt.filterMode = FilterMode.Point;
            RenderTexture.active = rt;
            Graphics.Blit(source, rt);
            Texture2D nTex = new Texture2D(newWidth, newHeight);
            nTex.ReadPixels(new Rect(0, 0, newWidth, newHeight), 0, 0);
            nTex.Apply();
            RenderTexture.active = null;
            RenderTexture.ReleaseTemporary(rt);
            return nTex;
        }

        private enum OverlayVerticalPosition { Top, Middle, Bottom };
        private enum OverlayHorizonalPosition { Left, Middle, Right };

        private Texture2D Overlay(Texture2D baseLayer, bool setOverlayWidthByBase,
                                Texture2D overlayLayer, int overlayWidth, int overlayHeight,
                                OverlayHorizonalPosition horizontalPosition, OverlayVerticalPosition verticalPosition,
                                int topMarging, int bottomMargin, int leftMargin, int rightMargin,
                                bool setBaseWidthByOverlay)
        {
            Debug.Log("SharedMediaHandler - Overlay function");

            if (setOverlayWidthByBase)
            {
                overlayLayer = Resize(overlayLayer, overlayWidth, overlayHeight);
            }

            if (setBaseWidthByOverlay)
            {
                int newBaseWidth = overlayWidth + leftMargin + rightMargin;
                float baseRatio = (float)newBaseWidth / (float)(baseLayer.width);
                int newBaseHeight = (int)(baseRatio * baseLayer.height);
                if (newBaseHeight < overlayHeight + topMarging + bottomMargin)
                    newBaseHeight = overlayHeight + topMarging + bottomMargin;
                baseLayer = Resize(baseLayer, newBaseWidth, newBaseHeight);
            }

            int minX = 0;
            int minY = 0;
            switch (horizontalPosition)
            {
                case (OverlayHorizonalPosition.Left):
                    minX = leftMargin;
                    break;
                case (OverlayHorizonalPosition.Middle):
                    minX = (baseLayer.width / 2) - (overlayLayer.width / 2);
                    break;
                case (OverlayHorizonalPosition.Right):
                    minX = baseLayer.width - overlayLayer.width - rightMargin;
                    break;
            }

            switch (verticalPosition)
            {
                case (OverlayVerticalPosition.Bottom):
                    minY = bottomMargin;
                    break;
                case (OverlayVerticalPosition.Middle):
                    minY = (baseLayer.height / 2) - (overlayLayer.height / 2);
                    break;
                case (OverlayVerticalPosition.Top):
                    minY = baseLayer.height - overlayLayer.height - topMarging;
                    break;
            }

            Color curColor;
            float alpha;

            Texture2D overlayedImage = new Texture2D(baseLayer.width, baseLayer.height, TextureFormat.RGBA32, baseLayer.mipmapCount > 0);

            overlayedImage.SetPixels32(0, 0, baseLayer.width, baseLayer.height, baseLayer.GetPixels32());
            overlayedImage.Apply();

            int curY = minY;
            while (curY < minY + overlayLayer.height)
            {
                int curX = minX;
                while (curX < minX + overlayLayer.width)
                {
                    curColor = overlayLayer.GetPixel(curX - minX, curY - minY);
                    if (curColor.a > 0.5)
                        //    overlayedImage.SetPixel(curX, curY, curColor);

                        alpha = 1;
                    else
                        alpha = 0;
                    curColor *= alpha;

                    alpha = 1f - alpha;
                    curColor += baseLayer.GetPixel(curX, curY) * alpha;
                    overlayedImage.SetPixel(curX, curY, curColor);

                    curX++;
                }
                curY++;
            }
            overlayedImage.Apply();
            return overlayedImage;
        }

        private Texture2D RotateTexture2D(Texture2D source, float angle)
        {
            ImageRotator rotator = new ImageRotator();
            return rotator.SetRotate(source, angle);
        }

        private void DeleteOldMediaFile(string fileName)
        {
            string imgPath = Application.persistentDataPath + "/" + fileName;
            File.Delete(imgPath);
        }

        public static bool SaveTextureAsPNG(string fileName, Texture2D texture)
        {
            string imgPath = Application.persistentDataPath + "/" + fileName;
            Debug.Log("SharedMediaHandler imgUrl = " + imgPath);

            //Create Directory if it does not exist
            if (!Directory.Exists(Path.GetDirectoryName(imgPath)))
                Directory.CreateDirectory(Path.GetDirectoryName(imgPath));

            // Save file
            try
            {
                File.WriteAllBytes(imgPath, texture.EncodeToPNG());
                Debug.Log("SharedMediaHandler successfuly saved after crop");
                return true;
            }
            catch (Exception E)
            {
                Debug.Log("ERROR - SharedMediaHandler = " + E.Message);
                return false;
            }
        }

        private void HandleSticker(Texture2D[] stickers, string stickerFileName)
        {
            Texture2D sticker = stickers[UnityEngine.Random.Range(0, stickers.Length)];
            SaveTextureAsPNG(stickerFileName, sticker);
        }

        private Vector2 RotateXY(Vector2 source, double degrees,
                   int offsetX, int offsetY)
        {
            Vector2 result = new Vector2();

            result.x = (int)(Math.Round((source.x - offsetX) *
                       Math.Cos(degrees) - (source.y - offsetY) *
                       Math.Sin(degrees))) + offsetX;


            result.y = (int)(Math.Round((source.x - offsetX) *
                       Math.Sin(degrees) + (source.y - offsetY) *
                       Math.Cos(degrees))) + offsetY;


            return result;
        }
    }

    public class TextureScale
    {
        public class ThreadData
        {
            public int start;
            public int end;
            public ThreadData(int s, int e)
            {
                start = s;
                end = e;
            }
        }

        private static Color[] texColors;
        private static Color[] newColors;
        private static int w;
        private static float ratioX;
        private static float ratioY;
        private static int w2;
        private static int finishCount;
        private static Mutex mutex;

        public static void Point(Texture2D tex, int newWidth, int newHeight)
        {
            ThreadedScale(tex, newWidth, newHeight, false);
        }

        public static void Bilinear(Texture2D tex, int newWidth, int newHeight)
        {
            ThreadedScale(tex, newWidth, newHeight, true);
        }

        private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear)
        {
            texColors = tex.GetPixels();
            newColors = new Color[newWidth * newHeight];
            if (useBilinear)
            {
                ratioX = 1.0f / ((float)newWidth / (tex.width - 1));
                ratioY = 1.0f / ((float)newHeight / (tex.height - 1));
            }
            else
            {
                ratioX = ((float)tex.width) / newWidth;
                ratioY = ((float)tex.height) / newHeight;
            }
            w = tex.width;
            w2 = newWidth;
            var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
            var slice = newHeight / cores;

            finishCount = 0;
            if (mutex == null)
            {
                mutex = new Mutex(false);
            }
            if (cores > 1)
            {
                int i = 0;
                ThreadData threadData;
                for (i = 0; i < cores - 1; i++)
                {
                    threadData = new ThreadData(slice * i, slice * (i + 1));
                    ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
                    Thread thread = new Thread(ts);
                    thread.Start(threadData);
                }
                threadData = new ThreadData(slice * i, newHeight);
                if (useBilinear)
                {
                    BilinearScale(threadData);
                }
                else
                {
                    PointScale(threadData);
                }
                while (finishCount < cores)
                {
                    Thread.Sleep(1);
                }
            }
            else
            {
                ThreadData threadData = new ThreadData(0, newHeight);
                if (useBilinear)
                {
                    BilinearScale(threadData);
                }
                else
                {
                    PointScale(threadData);
                }
            }

            tex.Resize(newWidth, newHeight);
            tex.SetPixels(newColors);
            tex.Apply();
        }

        public static void BilinearScale(System.Object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                int yFloor = (int)Mathf.Floor(y * ratioY);
                var y1 = yFloor * w;
                var y2 = (yFloor + 1) * w;
                var yw = y * w2;

                for (var x = 0; x < w2; x++)
                {
                    int xFloor = (int)Mathf.Floor(x * ratioX);
                    var xLerp = x * ratioX - xFloor;
                    newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor + 1], xLerp),
                                                           ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor + 1], xLerp),
                                                           y * ratioY - yFloor);
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        public static void PointScale(System.Object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                var thisY = (int)(ratioY * y) * w;
                var yw = y * w2;
                for (var x = 0; x < w2; x++)
                {
                    newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        private static Color ColorLerpUnclamped(Color c1, Color c2, float value)
        {
            return new Color(c1.r + (c2.r - c1.r) * value,
                              c1.g + (c2.g - c1.g) * value,
                              c1.b + (c2.b - c1.b) * value,
                              c1.a + (c2.a - c1.a) * value);
        }
    }




    public class ImageRotator
    {

        //private SpriteRenderer spriteRenderer;
        private Texture2D referenceTexture; // The 8x scaled image used for sampling during the rotation
        private Texture2D rotatedTexture; // The final image that gets set on the sprite
        private float Angle { get; set; } // I don't understand C# lol

        // Width and Height in pixels of original sprite
        // We can use this for rotating around a pivot
        private int originalWidth;
        private int originalHeight;
        private int xPad;
        private int yPad;

        // Used when scaling
        private Rect scaledTexBounds;

        private Texture2D CreateRotatedTextureDimantions(Texture2D source, float angle)
        {
            double pi2 = Math.PI / 2.0;
            int oldWidth = source.width;
            int oldHeight = source.height;
            double theta = angle * Math.PI / 180.0;
            double locked_theta = theta;
            if (locked_theta < 0)
                locked_theta += 2 * Math.PI;


            double newWidth;
            double newHeight;
            int nWidth;
            int nHeight;
            double adjacentTop;
            double oppositeTop;
            double adjacentBottom;
            double oppositeBottom;
            if (locked_theta >= 0.0 && locked_theta < pi2)
            {
                adjacentTop = Math.Abs(Math.Cos(locked_theta)) * oldWidth;
                oppositeTop = Math.Abs(Math.Sin(locked_theta)) * oldWidth;

                adjacentBottom = Math.Abs(Math.Cos(locked_theta)) * oldHeight;
                oppositeBottom = Math.Abs(Math.Sin(locked_theta)) * oldHeight;
            }
            else
            {
                adjacentTop = Math.Abs(Math.Sin(locked_theta)) * oldHeight;
                oppositeTop = Math.Abs(Math.Cos(locked_theta)) * oldHeight;

                adjacentBottom = Math.Abs(Math.Sin(locked_theta)) * oldWidth;
                oppositeBottom = Math.Abs(Math.Cos(locked_theta)) * oldWidth;
            }

            newWidth = adjacentTop + oppositeBottom;
            newHeight = adjacentBottom + oppositeTop;

            nWidth = (int)(Math.Ceiling(newWidth));
            nHeight = (int)(Math.Ceiling(newHeight));

            if (nWidth % 2 == 1) nWidth++;
            if (nHeight % 2 == 1) nHeight++;


            return new Texture2D(nWidth, nHeight);

        }

        // Use this for initialization
        void Init(Texture2D original, float angle)
        {
            Debug.Log("ImageHandler - start init");

            originalWidth = original.width;
            originalHeight = original.height;

            // Get temporary render texture to get access to sprite pixels
            RenderTexture tmp = RenderTexture.GetTemporary(originalWidth, originalHeight, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
            Graphics.Blit(original, tmp);
            RenderTexture old = RenderTexture.active;
            RenderTexture.active = tmp;

            // Calculate diagonal to ensure corners aren't cuttoff when rotating
            float diag = Mathf.Sqrt(originalWidth * originalWidth + originalHeight * originalHeight);
            int iDiag = Mathf.RoundToInt(diag + 0.5f); // Round up

            // Make iDiag even to ensure centering works
            if (iDiag % 2 == 1) iDiag++;

            // Represent the lower left corner of where the original sprite sits on the padded texture
            xPad = (iDiag - originalWidth) / 2;
            yPad = (iDiag - originalHeight) / 2;

            referenceTexture = new Texture2D(iDiag, iDiag);
            //referenceTexture = CreateRotatedTextureDimantions(original, angle);

            referenceTexture.filterMode = FilterMode.Point;

            Debug.Log("ImageHandler - in the middle of init");

            // Draw the sprite to the reference texture
            referenceTexture.ReadPixels(new Rect(0, 0, originalWidth, originalHeight), iDiag / 2 - originalWidth / 2, iDiag / 2 - originalHeight / 2);
            referenceTexture.Apply();

            RenderTexture.active = old;
            RenderTexture.ReleaseTemporary(tmp);

            // The texture that's attached to the sprite. Copy the reference texture (unscaled) to the sprite texturer
            rotatedTexture = new Texture2D(referenceTexture.width, referenceTexture.height);
            rotatedTexture.filterMode = FilterMode.Point;
            rotatedTexture.SetPixels32(referenceTexture.GetPixels32());
            rotatedTexture.Apply();
            // Scale for a total of 8x (note: this only needs to be done once, so no performance is lost. The only
            // issue is memory, but for small sprites this isn't an issue)
            scaledTexBounds = new Rect(xPad, yPad, originalWidth + 1, originalHeight + 1);
            Debug.Log("ImageHandler - scale 2X");
            Scale2x();
            Debug.Log("ImageHandler - scale 2X");
            Scale2x();
            Debug.Log("ImageHandler - scale 2X");
            Scale2x();
            Debug.Log("ImageHandler - finish init");
        }

        // Rotates the sprite about its center by the specified number of degrees
        public Texture2D SetRotate(Texture2D source, float degrees)
        {
            Debug.Log("ImageHandler - Start rotate");
            Init(source, degrees);
            Debug.Log("ImageHandler - init");

            Vector3 trash = new Vector3();
            SetRotate(source, degrees, new Vector2(originalWidth / 2, originalHeight / 2), out trash);
            Debug.Log("ImageHandler - rotated");
            return rotatedTexture;
        }

        // Pivot represents a PIXEL position on the ORIGINAL sprite
        // Out Vector local represents the translation in transform local space
        // Add local to your sprite transform after the rotation is finished
        private void SetRotate(Texture2D source, float degrees, Vector2 pivot, out Vector3 local)
        {
            Angle = degrees;

            // Negate the degrees to get a counter-clockwise rotation
            // This algorithm rotates clockwise by default, so we
            // need a negative angle to rotate CCW properly.
            float radians = -Mathf.Deg2Rad * degrees;

            int width = rotatedTexture.width;
            int height = rotatedTexture.height;

            Color32[] pixels = referenceTexture.GetPixels32();
            Color32[] newPixels = rotatedTexture.GetPixels32();

            Debug.Log("ImageHandler - pixels = " + pixels.Length.ToString());
            Debug.Log("ImageHandler - newPixels = " + newPixels.Length.ToString());

            int xcenter = width / 2;
            int ycenter = height / 2;

            float sin = Mathf.Sin(radians);
            float cos = Mathf.Cos(radians);

            int xOff = 4;
            int yOff = 4;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    // Make sure all pixels have a color
                    newPixels[x + y * width] = new UnityEngine.Color(0, 0, 0, 0);

                    // Rotate around center point
                    int xx = (int)((x - xcenter) * cos + (y - ycenter) * -sin) + xcenter;
                    int yy = (int)((x - xcenter) * sin + (y - ycenter) * cos) + ycenter;

                    // Make sure the rotated point falls within the bounds of the texture
                    // Note: Because we padded the original texture with transparent borders,
                    // no pixel on the original sprite will fall outside the bounds of the texture.
                    if (xx >= 0 && xx < width && yy >= 0 && yy < height)
                    {
                        newPixels[x + y * width] = pixels[xx * 8 + xOff + ((yy * 8 + yOff) * width * 8)];
                    }
                }
            }

            // Apply the pixels and reset the sprite
            rotatedTexture.SetPixels32(newPixels);
            rotatedTexture.Apply();

            local = new Vector3(0, 0, 0);
        }

        private float GetAngle()
        {
            return Angle;
        }

        // Scaling algorithm used in RotSprite
        // See link below for documentation on how this algorithm works
        // https://github.com/alteredgenome/grafx2/issues/385
        private void Scale2x()
        {
            int width = referenceTexture.width;
            int height = referenceTexture.height;

            Texture2D scaledTex = new Texture2D(width * 2, height * 2);
            scaledTex.filterMode = FilterMode.Point;
            Color32[] colors = referenceTexture.GetPixels32();
            Color32[] scaledCols = scaledTex.GetPixels32();

            int scaledWidth = width * 2;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int x1 = x * 2;
                    int x2 = x1 + 1;
                    int y1 = y * 2;
                    int y2 = y1 + 1;

                    // If we're on a pixel that's apart of the padding, then we don't have to calculate the scale
                    if (x < scaledTexBounds.x - 1 || x > scaledTexBounds.x + scaledTexBounds.width ||
                        y < scaledTexBounds.y - 1 || y > scaledTexBounds.y + scaledTexBounds.height)
                    {
                        // Set 4 corresponding pixels to blank
                        scaledCols[x1 + y1 * scaledWidth] = new Color32(0, 0, 0, 0);
                        scaledCols[x2 + y1 * scaledWidth] = new Color32(0, 0, 0, 0);
                        scaledCols[x1 + y2 * scaledWidth] = new Color32(0, 0, 0, 0);
                        scaledCols[x2 + y2 * scaledWidth] = new Color32(0, 0, 0, 0);
                        continue;
                    }

                    // Obtain 9 neighbor pixels
                    // cA cB cC
                    // cD cE cF
                    // cG cH cI

                    Color32 cA = colors[x - 1 + (y + 1) * width];
                    Color32 cB = colors[x + (y + 1) * width];
                    Color32 cC = colors[x + 1 + (y + 1) * width];
                    Color32 cD = colors[x - 1 + y * width];
                    Color32 cE = colors[x + y * width];
                    Color32 cF = colors[x + 1 + y * width];
                    Color32 cG = colors[x - 1 + (y - 1) * width];
                    Color32 cH = colors[x + (y - 1) * width];
                    Color32 cI = colors[x + 1 + (y - 1) * width];

                    // Outputs
                    // oA oB
                    // oC oD
                    Color32 oA;
                    Color32 oB;
                    Color32 oC;
                    Color32 oD;

                    if (different(cD, cF, cE) && different(cB, cH, cE) &&
                        ((similar(cE, cD, cE) || similar(cE, cH, cE) || similar(cE, cF, cE) || similar(cE, cB, cE) ||
                        ((different(cA, cI, cE) || similar(cE, cG, cE) || similar(cE, cC, cE)) &&
                        (different(cG, cC, cE) || similar(cE, cI, cE) || similar(cE, cA, cE))))))
                    {
                        oA = ((similar(cB, cD, cE) && (different(cE, cA, cE) && (different(cE, cA, cE) || different(cE, cI, cE) || different(cB, cC, cE) || different(cD, cG, cE)))) ? cB : cE);
                        oB = ((similar(cB, cF, cE) && (different(cE, cC, cE) && (different(cE, cC, cE) || different(cE, cG, cE) || different(cF, cI, cE) || different(cB, cA, cE)))) ? cF : cE);
                        oC = ((similar(cD, cH, cE) && (different(cE, cG, cE) && (different(cE, cG, cE) || different(cE, cC, cE) || different(cD, cA, cE) || different(cH, cI, cE)))) ? cD : cE);
                        oD = ((similar(cH, cF, cE) && (different(cE, cI, cE) && (different(cE, cI, cE) || different(cE, cA, cE) || different(cH, cG, cE) || different(cF, cC, cE)))) ? cH : cE);

                    }
                    else
                    {
                        oA = cE;
                        oB = cE;
                        oC = cE;
                        oD = cE;
                    }


                    scaledCols[x1 + y2 * scaledWidth] = oA; // oA
                    scaledCols[x2 + y2 * scaledWidth] = oB; // oB
                    scaledCols[x1 + y1 * scaledWidth] = oC; // oC
                    scaledCols[x2 + y1 * scaledWidth] = oD; // oD
                }
            }

            scaledTex.SetPixels32(scaledCols);
            scaledTex.Apply();

            referenceTexture = scaledTex;
            scaledTexBounds.x *= 2;
            scaledTexBounds.y *= 2;
            scaledTexBounds.width *= 2;
            scaledTexBounds.height *= 2;
        }

        private float distance(Color32 c1, Color32 c2)
        {
            // IF the colors are the same, their distance is 0
            if (eq(c1, c2)) return 0;
            return Mathf.Abs(c1.r - c2.r) + Mathf.Abs(c1.g - c2.g) + Mathf.Abs(c1.b - c2.b);
        }

        private bool similar(Color32 c1, Color32 c2, Color32 reference)
        {
            if (eq(c1, c2)) return true;
            float d12 = distance(c1, c2);
            float d1r = distance(c1, reference);
            float d2r = distance(c2, reference);
            return d12 <= d1r && d12 <= d2r;
        }

        private bool different(Color32 c1, Color32 c2, Color32 reference)
        {
            return !similar(c1, c2, reference);
        }

        private bool eq(Color32 c1, Color32 c2)
        {
            return c1.r == c2.r && c1.g == c2.g && c1.b == c2.b;
        }

        // Tex is the 8x scaled texture
        // TOO SLOW TOO SLOW TOO SLOW TOO SLOW
        //
        // This algorithm calculates the best rotation offsets to use on the scaled image
        // by reducing the squared error of rotated pixels and their neighbor's colors
        private Vector2 bestOffset(Texture2D tex, float radians)
        {
            float cos = Mathf.Cos(radians);
            float sin = Mathf.Sin(radians);
            int xcenter = tex.width / 2;
            int ycenter = tex.height / 2;

            Color32[] colors = tex.GetPixels32();

            float xBest = 0;
            float yBest = 0;
            float lowestError = float.MaxValue;
            for (int xOff = 0; xOff < 8; xOff++)
            {
                for (int yOff = 0; yOff < 8; yOff++)
                {
                    float error = 0;

                    // Loop through pixels and get squared error
                    for (int x = xOff; x < tex.width; x += 8)
                    {
                        for (int y = yOff; y < tex.height; y += 8)
                        {
                            int xx = (int)((x - xcenter) * cos + (y - ycenter) * -sin) + xcenter;
                            int yy = (int)((x - xcenter) * sin + (y - ycenter) * cos) + ycenter;

                            if (xx < 0 || xx >= tex.width || yy < 0 || yy >= tex.height) continue;

                            error += neighborSquaredError(xx, yy, tex);
                        }
                    }

                    // If the accumulated error is less than the best lowest error, then we found better rotation offsets
                    if (error < lowestError)
                    {
                        lowestError = error;
                        xBest = xOff;
                        yBest = yOff;
                    }
                }
            }

            return new Vector2(xBest, yBest);
        }

        private float neighborSquaredError(int x, int y, Texture2D tex)
        {
            Color32[] colors = tex.GetPixels32();
            Color32 col = colors[x + y * tex.width];

            float error = 0;

            // Left Pixel
            int xx = x - 1;
            int yy = y;

            if (xx >= 0)
            {
                error += sqrError(col, colors[xx + yy * tex.width]);
            }

            // Right Pixel
            xx = x + 1;

            if (xx < tex.width)
            {
                error += sqrError(col, colors[xx + yy * tex.width]);
            }

            // Top Pixel
            xx = x;
            yy = y + 1;

            if (yy < tex.height)
            {
                error += sqrError(col, colors[xx + yy * tex.width]);
            }

            // Bottom Pixel
            yy = y - 1;

            if (yy >= 0)
            {
                error += sqrError(col, colors[xx + yy * tex.width]);
            }
            return error;
        }

        private float sqrError(Color32 c1, Color32 c2)
        {
            float r = c1.r - c2.r;
            float g = c1.g - c2.g;
            float b = c1.b - c2.b;
            float a = c1.a - c2.a;
            return r * r + g * g + b * b + a * a;
        }





        /* --------------------------------------------------------- */


        private Vector2 RotateXY(Vector2 source, double degrees,
                           int offsetX, int offsetY)
        {
            Vector2 result = new Vector2();

            result.x = (int)(Math.Round((source.x - offsetX) *
                       Math.Cos(degrees) - (source.y - offsetY) *
                       Math.Sin(degrees))) + offsetX;


            result.y = (int)(Math.Round((source.x - offsetX) *
                       Math.Sin(degrees) + (source.y - offsetY) *
                       Math.Cos(degrees))) + offsetY;


            return result;
        }

        public Texture2D RotateImage(Texture2D sourceTexture2D, double degrees)
        {
            Color32[] pixelBuffer = sourceTexture2D.GetPixels32();

            Color32[] resultBuffer = sourceTexture2D.GetPixels32();

            //Convert to Radians 
            degrees = degrees * Math.PI / 180.0;

            //Calculate Offset in order to rotate on image middle 
            int xOffset = (int)(sourceTexture2D.width / 2.0);
            int yOffset = (int)(sourceTexture2D.height / 2.0);


            int sourceXY = 0;
            int resultXY = 0;


            Vector2 sourcePoint = new Vector2();
            Vector2 resultPoint = new Vector2();


            Rect imageBounds = new Rect(0, 0,
                                    sourceTexture2D.width,
                                   sourceTexture2D.height);


            for (int row = 0; row < sourceTexture2D.height; row++)
            {
                for (int col = 0; col < sourceTexture2D.width; col++)
                {
                    sourceXY = row * sourceTexture2D.width + col * 4;


                    sourcePoint.x = col;
                    sourcePoint.y = row;


                    if (sourceXY >= 0 && sourceXY + 3 < pixelBuffer.Length)
                    {
                        //Calculate Blue Rotation 


                        resultPoint = RotateXY(sourcePoint, degrees, xOffset, yOffset);


                        resultXY = (int)(Math.Round(
                              (resultPoint.y * sourceTexture2D.width) +
                              (resultPoint.x * 4.0)));


                        if (imageBounds.Contains(resultPoint) && resultXY >= 0)
                        {
                            if (resultXY + 6 < resultBuffer.Length)
                            {
                                resultBuffer[resultXY + 4] = pixelBuffer[sourceXY];
                                resultBuffer[resultXY + 7] = new Color32(0, 0, 0, 0);
                            }


                            if (resultXY + 3 < resultBuffer.Length)
                            {
                                resultBuffer[resultXY] = pixelBuffer[sourceXY];
                                resultBuffer[resultXY + 3] = new Color32(0, 0, 0, 0);
                            }
                        }

                    }
                }
            }

            Texture2D resultTexture = new Texture2D((int)(sourceTexture2D.width * 1.5), (int)(sourceTexture2D.height * 1.5));
            resultTexture.SetPixels32(resultBuffer);
            return resultTexture;
        }
    }







}