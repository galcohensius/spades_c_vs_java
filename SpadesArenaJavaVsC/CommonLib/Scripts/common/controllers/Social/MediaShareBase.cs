﻿using common.controllers.social.sharer;
using System;
using System.Collections;
namespace common.controllers.social
{
    public abstract class MediaShareBase 
    {

        protected SharingConfig parameters = null;

        protected virtual void Init (SharingConfig shareParams)
        {
            parameters = shareParams;
        }

        public virtual IEnumerator CreateImageFileMedia(Action<bool> onFinish)
        {
            // TBD
            return null;
        }

        public virtual void CreateVideoFileMedia()
        {
            // TBD
        }



    }
}