﻿using System.Collections;
using System;


namespace common.controllers.social
{
	public abstract class SocialBase : MonoBehaviour
	{
        public abstract ChannelOptions channel
        {
            get;
        }

        public abstract MediaOptions mediaOptions
        {
            get;
        }

        public delegate void SocialClickDelegate();
        public SocialClickDelegate OnClickHandler;
        public SocialClickDelegate OnEndHandler;

        [Flags]
		public enum ChannelOptions
		{
            TestCropping = -1,
			None = 0,
			Facebook = 1,
			Whatsapp = 2,
			Messenger = 4,
			Mail = 8,
			Twitter = 16,
			Sms = 32,
			Native = 64,
            GMail = 128,
            Instagram = 256,
            Snapchat = 512
		}


        [Flags]
        public enum MediaOptions
        {
            None = 0,
            Text = 1,
            Image = 2,
            Video = 4,
            TextLink = 8,
            ImageLink = 16,
            Voice = 32
        }

        public abstract bool IsAppInstalled();

        protected bool CheckAndroidBundleExist(string bundleId)
		{
#if UNITY_ANDROID
			AndroidJavaObject launchIntent = null;
			try
			{
				AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
				AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

				launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
			}
			catch (Exception ex)
			{
				LoggerController.Instance.Log("exception" + ex.Message);
			}

			return launchIntent != null;
#else
			return false;
#endif
		}

		public string ChannelName
		{
            get { return channel.ToString(); }
		}

        public virtual void OnClick()
        {
            StartCoroutine(DelayAndClick(0.01f));
        }

        IEnumerator DelayAndClick (float delay_ms)
        {           
            yield return new WaitForSeconds(delay_ms);
            OnClickHandler?.Invoke();
        }

        protected bool CheckIOSCanOpenUrl(string urlScheme)
		{
#if UNITY_EDITOR
            return true;
#elif UNITY_IOS
			return canOpenURL(urlScheme);
#else
			return false;
#endif
		}

#if UNITY_IOS
		[DllImport("__Internal")] private static extern bool canOpenURL(string urlScheme);
#endif

	}

}