﻿namespace common.controllers.social.inviter
{
	public class SmsInviter : InviterBase
	{
		public override string ChannelName
		{
			get
			{
				return "sms";
			}
		}

		public override void OnClick()
		{
			base.OnClick();

#if UNITY_ANDROID

            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            // Set action for intent
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_VIEW"));

			AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
			AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "sms:");
			intentObject.Call<AndroidJavaObject>("setData", uriObject);

            //Set Subject of action
			intentObject.Call<AndroidJavaObject>("putExtra", "sms_body", BuildInviterBody());
                     
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            // Invoke android activity for passing intent to share data
            currentActivity.Call("startActivity", intentObject);



#elif UNITY_IOS
			NativeShareManager.SocialSharingStruct conf = new NativeShareManager.SocialSharingStruct();
			conf.text = BuildInviterBody();

			conf.subject = m_title;
            
			showMessagesSharing(ref conf);
#endif
		}

		protected override bool IsAppInstalled()
		{
			return true;
		}


#if UNITY_IOS
		[DllImport("__Internal")] private static extern void showMessagesSharing(ref NativeShareManager.SocialSharingStruct conf);
#endif
	}
}

