﻿using System;


namespace common.controllers.social.inviter
{
	public abstract class InviterBase : MonoBehaviour
	{
		[SerializeField] protected MediaOptions m_option;
		[SerializeField] protected Button m_button;

		protected int m_user_id;
		protected string m_title;
		protected string m_body;
		public delegate void InviterClickDelegate();
		private InviterClickDelegate m_InviterrClick;

        [Flags]
		public enum MediaOptions
		{
			None = 0,
			Facebook = 1,
			Whatsapp = 2,
			Messenger = 4,
			Mail = 8,
			Twitter = 16,
			Sms = 32,
			Native = 64,
            GMail = 128,
            Instagram = 256
		}

        //public const SharerOptions SHARER_OPTIONS_SET = SharerOptions.Facebook;
            
#if UNITY_ANDROID
		public const MediaOptions INVITER_OPTIONS_SET = MediaOptions.Facebook | MediaOptions.Whatsapp  | MediaOptions.Mail | MediaOptions.Twitter | MediaOptions.Sms | MediaOptions.Native | MediaOptions.Instagram;
#elif UNITY_IOS
		public const MediaOptions INVITER_OPTIONS_SET = MediaOptions.Facebook | MediaOptions.Whatsapp  | MediaOptions.Mail | MediaOptions.Twitter | MediaOptions.Sms | MediaOptions.Native | MediaOptions.Instagram;
#elif UNITY_WEBGL
		public const MediaOptions INVITER_OPTIONS_SET = MediaOptions.Facebook | MediaOptions.Mail | MediaOptions.GMail | MediaOptions.Twitter | MediaOptions.Instagram;
#endif

        public virtual void Init(int userId, string title, string body)
		{
			m_user_id = userId;
			m_title = title;
			m_body = body;
           
			gameObject.SetActive((INVITER_OPTIONS_SET & m_option) != MediaOptions.None);
			m_button.interactable = IsAppInstalled();


		}

		protected string BuildInviterBody()
		{
			return String.Format(m_body, ChannelName, m_user_id);
		}

		public virtual void OnClick()
		{
            m_InviterrClick?.Invoke();
		}

		protected abstract bool IsAppInstalled();

		protected bool CheckAndroidBundleExist(string bundleId)
		{
#if UNITY_ANDROID
			AndroidJavaObject launchIntent = null;
			try
			{
				AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
				AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

				launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", bundleId);
			}
			catch (Exception ex)
			{
				LoggerController.Instance.Log("exception" + ex.Message);
			}

			return launchIntent != null;
#else
			return false;
#endif


		}

		public abstract string ChannelName
		{
			get;
		}

		public InviterClickDelegate InviterClick
		{
			get
			{
				return m_InviterrClick;
			}
			set
			{
                m_InviterrClick = value;
			}
		}

		protected bool CheckIOSCanOpenUrl(string urlScheme)
		{
#if UNITY_EDITOR
            return true;
#elif UNITY_IOS
			return canOpenURL(urlScheme);
#else
			return false;
#endif
		}

#if UNITY_IOS
		[DllImport("__Internal")] private static extern bool canOpenURL(string urlScheme);
#endif

	}

}