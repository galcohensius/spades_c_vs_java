﻿using System;

namespace common.controllers.social.inviter
{
	public class TwitterInviter:InviterBase
	{
		private const string BUNDLE_ID = "com.twitter.android";
		private const string Address = "http://twitter.com/intent/tweet";


		public override string ChannelName {
			get {
				return "twitter";
			}
		}

		public override void OnClick ()
		{
			base.OnClick ();
#if UNITY_WEBGL && !UNITY_EDITOR
			openBrowserWindow (Address +"?text=" +  Uri.EscapeDataString(BuildInviterBody()));
#else
			Application.OpenURL(Address + "?text=" + Uri.EscapeDataString(BuildInviterBody()));
#endif
		}



		protected override bool IsAppInstalled ()
		{

#if UNITY_ANDROID
			//return CheckAndroidBundleExist(BUNDLE_ID);
            // Always true, since phone enables web sharing if app does not exist
			return true;
#elif UNITY_IOS
			return true;
			#else
			return true;
			#endif
		}

		#if UNITY_WEBGL
		[DllImport ("__Internal")]
		private static extern void openBrowserWindow (string url);
		#endif
	}
}

