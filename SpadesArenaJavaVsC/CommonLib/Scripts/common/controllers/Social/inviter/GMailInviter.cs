﻿using System;

namespace common.controllers.social.inviter
{



	public class GMailInviter : InviterBase
	{

		public override string ChannelName
        {
            get
            {
                return "gmail";
            }
        }


		public override void OnClick()
		{
			base.OnClick();

			string subject = Uri.EscapeDataString(m_title);
            string body = Uri.EscapeDataString(BuildInviterBody());

			string link = string.Format("https://mail.google.com/mail/u/0/?view=cm&fs=1&su={0}&body={1}&tf=1", subject, body);

#if UNITY_WEBGL && !UNITY_EDITOR
			openBrowserWindow (link);
#else
			Application.OpenURL(link);
#endif

		}

		protected override bool IsAppInstalled()
        {
            return true;
        }

#if UNITY_WEBGL
        [DllImport ("__Internal")]
        private static extern void openBrowserWindow (string url);
#endif
	}

}