﻿using System;

namespace common.controllers.social.inviter
{
	public class WhatsAppInviter : InviterBase
	{
		private const string BUNDLE_ID = "com.whatsapp";
		private const string URL_SCHEME = "whatsapp://";

		public override void OnClick()
		{
			base.OnClick();
			Application.OpenURL(URL_SCHEME + "send?text=" + Uri.EscapeDataString(BuildInviterBody()));
		}

		public override string ChannelName
		{
			get
			{
				return "whatsapp";
			}
		}

		protected override bool IsAppInstalled()
		{
#if UNITY_ANDROID
			return CheckAndroidBundleExist(BUNDLE_ID);
#elif UNITY_IOS
			return CheckIOSCanOpenUrl(URL_SCHEME);
#else
			return false;
#endif
		}
	}
}

