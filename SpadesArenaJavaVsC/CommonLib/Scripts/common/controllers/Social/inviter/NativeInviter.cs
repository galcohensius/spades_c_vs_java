﻿namespace common.controllers.social.inviter
{
    public class NativeInviter : InviterBase
	{
		public override string ChannelName
		{
			get
			{
				return "native";
			}
		}

		public override void OnClick()
		{
			base.OnClick();
#if UNITY_ANDROID

			NativeShareManager.ShowNativeAndroidShare(BuildInviterBody(), m_title);

#elif UNITY_IOS
			NativeShareManager.ShowNativeIosShare(BuildInviterBody(), m_title); // This method has to be statu
#endif
		}


		protected override bool IsAppInstalled()
		{
			return true;
		}

	}
}

