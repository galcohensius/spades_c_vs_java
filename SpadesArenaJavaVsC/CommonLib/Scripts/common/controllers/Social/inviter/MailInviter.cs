﻿using System;

namespace common.controllers.social.inviter
{
	public class MailInviter:InviterBase
	{
		public override string ChannelName {
			get {
				return "mail";
			}
		}

		public override void OnClick()
		{
			base.OnClick();

			string subject = Uri.EscapeDataString(m_title);
			string body = Uri.EscapeDataString(BuildInviterBody());

#if UNITY_WEBGL && !UNITY_EDITOR
            openBrowserWindow ("mailto:?subject=" + subject + "&body=" + body);
#elif UNITY_IOS
			Application.OpenURL("mailto:?subject=" + subject + "&body=" + body);
#elif UNITY_ANDROID

            //Application.OpenURL("mailto:?subject=" + subject + "&body=" + body);
			AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            // Set action for intent
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_VIEW"));

            AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
            AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "mailto:");
            intentObject.Call<AndroidJavaObject>("setData", uriObject);

            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), m_title);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), m_title);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), BuildInviterBody());

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            // Invoke android activity for passing intent to share data
            currentActivity.Call("startActivity", intentObject);

#endif
		}


		protected override bool IsAppInstalled ()
		{
			return true;
		}

		#if UNITY_WEBGL
		[DllImport ("__Internal")]
		private static extern void openBrowserWindow (string url);
		#endif
	}
}

