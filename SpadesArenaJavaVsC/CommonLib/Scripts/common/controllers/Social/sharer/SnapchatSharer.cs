﻿using System.IO;

namespace common.controllers.social.sharer
{


    public class SnapchatSharer : SharerBase
    {
        public override ChannelOptions channel
        {
            get { return ChannelOptions.Snapchat; }
        }

        public override MediaOptions mediaOptions
        {
            get { return MediaOptions.Image | MediaOptions.Video; }
        }

        protected override void Initialize()
        {
            SnapchatKitManager.Initialise((bool success, string error)=> {
                string message = success ? "Snapchat successfully initiated" : "Snapchat failed to initiate " + error;
                Debug.Log(message);

                initialised = success;
            });
        }

        public override bool IsAppInstalled()
        {
            bool isAvailable = SnapchatKitManager.IsAvailable();
            string message = isAvailable ? "Snapchat Kit is available!" : "Snapchat Kit is not available. Instagram app may not be installed.";
            Debug.Log(message);
            return isAvailable;
        }

        public override void Share()
        {
            if (! sharingConfig.isVideo)
                ShareImageInStory();
        }

        private void ShareImageInStory()
        {
            if (! sharingConfig.preferSelfieIfAvailable)
            {

                if (sharingConfig.useScreenshot)
                {
                    sharingConfig.croppedImageHolderForTests.enabled = false;

                    PolaroidShare polaroidShare = new PolaroidShare();
                    polaroidShare.Init(sharingConfig, gameObject, sharingConfig.croppingArea, true, false);
                    StartCoroutine(polaroidShare.CreateImageFileMedia((CropSuccess) =>
                    {
                        if (CropSuccess)
                        {
                            // Set Image on screen
                            sharingConfig.croppedImageHolderForTests.enabled = true;
                            RectTransform croppedImageTransform = (RectTransform)(sharingConfig.croppedImageHolderForTests.transform);
                            Texture2D sharedImage = new Texture2D(2, 2);
                            if (File.Exists(Application.persistentDataPath + "/" + sharingConfig.shareFileName))
                            {

                                // Prepare sticker
                                if (sharingConfig.useSticker)
                                    HandleSticker();

                                OnEnd();

                                // Share Image
                                ShareImage(Application.persistentDataPath + "/" + sharingConfig.shareFileName,
                                        sharingConfig.useSticker,
                                        sharingConfig.useCaption,
                                        sharingConfig.useInvitingURL,
                                        Application.persistentDataPath + "/" + sharingConfig.stickerFileName,
                                        sharingConfig.caption,
                                        sharingConfig.invitingURL);
                                Debug.Log("Snapchat after share image, invitingURL = " + sharingConfig.invitingURL);
                            }
                        }
                    }));
                }
                else
                {
                    // Use a ready made image
                }    
            }
        }

        protected void HandleSticker()
        {
            Debug.Log("SharedMediaHandler sticker = " + sharingConfig.stickerFileName);

            Texture2D sticker = sharingConfig.stickers[UnityEngine.Random.Range(0, sharingConfig.stickers.Length)];
            PolaroidShare.SaveTextureAsPNG(sharingConfig.stickerFileName, sticker);
        }
        private void ShareImage(string filePath, bool withSticker, bool withCaption, bool withURL, string stickerPath = "", string captionText = "", string URL = "")
        {
            SnapchatContent content;
            if (sharingConfig.preferSelfieIfAvailable)
                content = new SnapchatContent(eSnapchatContentType.LiveCamera);
            else
            {
                content = new SnapchatContent(eSnapchatContentType.Photo);
                content.SetContentData(filePath);
            }

            if (withSticker)
            {
                SnapchatSticker sticker = new SnapchatSticker(stickerPath);
                // Set Position in normalised screen coordinates
                sticker.SetPosition(0.5f, 0.5f);

                // Set rotation in degrees
                sticker.SetRotation(-45f);

                // Set size for the sticker in pixels
                sticker.SetSize(Screen.width * 0.3f, Screen.height * 0.3f);

                content.SetSticker(sticker);
                Debug.Log("Snapchat - added Sticker: " + stickerPath);
            }

            if (withCaption)
            {
                content.SetCaptionText(captionText);
                Debug.Log("Snapchat - added Caption: " + captionText);
            }

            if (withURL)
            {
                string inviteURL = URL;
                inviteURL = inviteURL.Replace("{0}",channel.ToString() + "_" + sharingConfig.eventName);
                inviteURL = inviteURL.Replace("{1}", sharingConfig.userID);
                content.SetAttachmentUrl(inviteURL);

                Debug.Log("Snapchat - added URL: " + URL);
            }
            SnapchatKitManager.Share(content, OnShareComplete);
        }

        private void OnShareComplete(bool success, string error)
        {
            string message = success ? "Snapchat successfully Shared" : "Snapchat failed to share " + error;
            Debug.Log(message);
            FirebaseSharingLogging();
        }

    }
}

