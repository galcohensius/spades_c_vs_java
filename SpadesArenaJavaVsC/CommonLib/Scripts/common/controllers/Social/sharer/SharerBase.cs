﻿using System;
using System.Collections.Generic;

namespace common.controllers.social.sharer
{
    public abstract class ShareParameters { }

	public abstract class SharerBase : SocialBase
    {

        [SerializeField]
        public SharingConfig sharingConfig;

        public ChannelOptions socialChannel { get { return channel; } }

        protected bool initialised = false;
        public bool isInitialized { get { return initialised; } }


        public void SetActive (bool isActive)
        {
            gameObject.SetActive(isActive);
        }

        private void SetUserID (string userID)
        {
            sharingConfig.userID = userID;
        }

        public abstract void Share();

        public virtual void Config(string userID, SocialClickDelegate onStartClick, SocialClickDelegate onEndClick)
        {
            SetButtonInteractable(false);
            Initialize();
            if (isInitialized && IsAppInstalled())
            {
                SetButtonInteractable(true);
                Debug.Log("sharer " + socialChannel.ToString() + " initialization success");
                SetUserID(userID);
                GetInvitingURL();
                if (onStartClick != null)
                {
                    OnClickHandler -= onStartClick;
                    OnClickHandler += onStartClick;
                }
                if (onEndClick != null)
                {
                    OnEndHandler -= onEndClick;
                    OnEndHandler += onEndClick;
                }
            }
            else
                Debug.Log("sharer " + socialChannel.ToString() + "  initialization succfailedess");
        }

        private void GetInvitingURL()
        { 
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                sharingConfig.invitingURL = ldc.GetSetting("InviteLink", "");
                String.Format(sharingConfig.invitingURL, socialChannel.ToString(), sharingConfig.userID);
            };
        }

protected virtual void Initialize ()
        {
            initialised = true;
        }

        public override void OnClick()
        {
            base.OnClick();
            Share();
        }

        protected virtual void OnEnd()
        {
            OnEndHandler?.Invoke();
        }

        protected void FirebaseSharingLogging()
        {
            Dictionary<string, string> eventParams = new Dictionary<string, string>();
            eventParams.Add("type", channel.ToString());
            TrackingManager.Instance.LogFirebaseEvent(sharingConfig.eventName, eventParams);
        }

        protected virtual void SetButtonInteractable(bool interactable)
        {
            Button shareButton = GetComponentInChildren<Button>();
            if (shareButton != null)
                shareButton.interactable = interactable;
        }

#if UNITY_ANDROID
        public static ChannelOptions SHARER_OPTIONS_SET = ChannelOptions.Instagram | ChannelOptions.Snapchat;
#elif UNITY_IOS
		public static ChannelOptions SHARER_OPTIONS_SET = ChannelOptions.Instagram | ChannelOptions.Snapchat;
#elif UNITY_WEBGL
		public static ChannelOptions SHARER_OPTIONS_SET = ChannelOptions.None ;
#endif


    }

}