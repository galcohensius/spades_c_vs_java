﻿using System.IO;

namespace common.controllers.social.sharer
{


    public class CropTestSharer : SharerBase
    {
        public override ChannelOptions channel
        {
            get { return ChannelOptions.TestCropping; }
        }

        public override MediaOptions mediaOptions
        {
            get { return MediaOptions.None; }
        }

        public override bool IsAppInstalled()
        {
            return true;
        }

        public override void Share()
        {
            sharingConfig.croppedImageHolderForTests.enabled = false;

            PolaroidShare polaroidShare = new PolaroidShare();
            polaroidShare.Init(sharingConfig, gameObject, sharingConfig.croppingArea, true, false);
            StartCoroutine(polaroidShare.CreateImageFileMedia((CropSuccess) =>
            {
                OnEnd();

                if (CropSuccess)
                {
                    // Set Image on screen
                    sharingConfig.croppedImageHolderForTests.enabled = true;
                    RectTransform croppedImageTransform = (RectTransform)(sharingConfig.croppedImageHolderForTests.transform);
                    Texture2D sharedImage = new Texture2D(2, 2);
                    if (File.Exists(Application.persistentDataPath + "/" + sharingConfig.shareFileName))
                    {
                        byte[] fileData = File.ReadAllBytes(Application.persistentDataPath + "/" + sharingConfig.shareFileName);
                        sharedImage.LoadImage(fileData); //..this will auto-resize the texture dimensions.
                        croppedImageTransform.sizeDelta = new Vector2(sharedImage.width, sharedImage.height);
                        sharingConfig.croppedImageHolderForTests.texture = sharedImage;
                    }
                }
            }));

        }

    }
}

