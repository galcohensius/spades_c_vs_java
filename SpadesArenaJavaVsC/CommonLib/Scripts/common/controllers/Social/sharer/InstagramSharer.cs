﻿using System.IO;

namespace common.controllers.social.sharer
{

    public class InstagramSharer : SharerBase
    {
        public override ChannelOptions channel
        {
            get { return ChannelOptions.Instagram; }
        }

        public override MediaOptions mediaOptions
        {
            get { return MediaOptions.Image | MediaOptions.Video; }
        }

        public override bool IsAppInstalled()
        {
            bool isAvailable = InstagramKitManager.IsAvailable();
            string message = isAvailable ? "Instagram Kit is available!" : "Instagram Kit is not available. Instagram app may not be installed.";
            Debug.Log(message);
            return isAvailable;
        }

        public override void Share()
        {
            if (! sharingConfig.isVideo)
                ShareImageInStory();
        }

        private void ShareImageInStory()
        {
            if (sharingConfig.useScreenshot)
            {
                sharingConfig.croppedImageHolderForTests.enabled = false;

                PolaroidShare polaroidShare = new PolaroidShare();
                polaroidShare.Init(sharingConfig, gameObject, sharingConfig.croppingArea, false, false);
                StartCoroutine(polaroidShare.CreateImageFileMedia((CropSuccess) =>
                {
                    if (CropSuccess)
                    {
                        // Set Image on screen
                        sharingConfig.croppedImageHolderForTests.enabled = true;
                        RectTransform croppedImageTransform = (RectTransform)(sharingConfig.croppedImageHolderForTests.transform);
                        Texture2D sharedImage = new Texture2D(2, 2);
                        if (File.Exists(Application.persistentDataPath + "/" + sharingConfig.shareFileName))
                        {
                            if (sharingConfig.useSticker)
                                HandleSticker();

                            OnEnd();

                            // Share Image
                            ShareImage(Application.persistentDataPath + "/" + sharingConfig.shareFileName,
                                       sharingConfig.useSticker,
                                       Application.persistentDataPath + "/" + sharingConfig.stickerFileName);
                            Debug.Log("Instagram after share image");
                        }
                    }
                }));
            }
            else
            {
                // Use a ready made image
            }
        }

        protected void HandleSticker()
        {
            Debug.Log("SharedMediaHandler sticker = " + sharingConfig.stickerFileName);

            Texture2D sticker = sharingConfig.stickers[UnityEngine.Random.Range(0, sharingConfig.stickers.Length)];
            PolaroidShare.SaveTextureAsPNG(sharingConfig.stickerFileName, sticker);
        }

        private void ShareImage(string filePath, bool withSticker, string stickerPath = "")
        {
            StoryContent content;
            content = new StoryContent(filePath, false);

            if (withSticker)
            {
                Sticker sticker = new Sticker(stickerPath);
                content.SetSticker(sticker);
            }

            InstagramKitManager.Share(content, OnShareComplete);
        }

        private void OnShareComplete(bool success, string error)
        {
            string message = success ? "Instagram successfully Shared" : "Instagram failed to share " + error;
            Debug.Log(message);
        }
    }
}

