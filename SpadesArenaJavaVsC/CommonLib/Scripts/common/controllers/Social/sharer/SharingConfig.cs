﻿namespace common.controllers.social.sharer
{
    public class SharingConfig : MonoBehaviour
    {
        [SerializeField]
        public string eventName;

        [SerializeField]
        public bool preferSelfieIfAvailable;

        [SerializeField]
        public RawImage croppedImageHolderForTests;

        [SerializeField]
        public GameObject croppingArea;

        [SerializeField]
        public bool useBackground;

        [SerializeField]
        public bool useFrame;

        [SerializeField]
        public bool useSticker;

        [SerializeField]
        public bool useCaption;

        [SerializeField]
        public string caption;

        [SerializeField]
        public bool isVideo;

        [SerializeField]
        public string videoFileName;

        [SerializeField]
        public bool useInvitingURL;

        [SerializeField]
        public bool rotateImage;

        [SerializeField]
        public float rotateAngle;

        [HideInInspector]
        public string userID = "";

        [HideInInspector]
        public bool useScreenshot = true;

        [HideInInspector]
        public string shareFileName = "screenshot.png";

        [HideInInspector]
        public string stickerFileName = "sticker.png";

        [HideInInspector]
        public string stickersFolder = "Stickers";

        [HideInInspector]
        public string baseImagesFolder = "Social/Sharing";

        [HideInInspector]
        public string generalFolder = "General";

        [HideInInspector]
        public string gameLogoFolder = "GameLogo";

        [HideInInspector]
        public string bgImagesFolder = "Background";

        [HideInInspector]
        public string frameImagesFolder = "Frame/FrameImage";

        [HideInInspector]
        public string frameTextAssetsFolder = "Frame/FrameTexts";

        [HideInInspector]
        public string topAssetsFolder = "TopAssets";

        [HideInInspector]
        public string bottomAssetsFolder = "BottomAssets";

        [HideInInspector]
        public string bottomSwipeAssetsFolder = "BottomSwipeAssets";

        [HideInInspector]
        public string invitingURL = null;

        [HideInInspector]
        public Texture2D[] stickers = null;
    }
}
