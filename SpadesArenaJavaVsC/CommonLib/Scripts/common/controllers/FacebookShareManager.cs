﻿using System.Collections.Generic;
using common.models;
using System;
using common.facebook;

namespace common.controllers
{

	public class FacebookShareManager : MonoBehaviour
	{

		public TextAsset m_static_fb_share_json_file;

		Dictionary<string,FBShare> m_fb_share_items = new Dictionary<string, FBShare> ();

		public static FacebookShareManager Instance;

		private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }


        //in case this method is changed, update its test at Tests.common.controllers.FacebookShareManagerTests.ReadStaticAchievementsData_Pass()
        //and ReadStaticAchievementsData_Fail() as it code is copy-pasted there and not calling THIS method
        public void ReadStaticAchievementsData ()
		{
			JSONArray root = JSON.Parse (m_static_fb_share_json_file.ToString ()).AsArray;

			foreach (JSONNode node in root)
			{
				string id = node ["id"].Value;
				string title = node ["title"].Value;
				string desc = node ["description"].Value;
				string og = node ["og_action"].Value;

                if (id == "" || title == "" || desc == "" || og == "")
                    LoggerController.Instance.LogWarning("one of the keys has no value. the key is wrong or the data is incomoplete");

                FBShare	fb_share = new FBShare (id, title, desc,og);

				JSONArray arr = node ["images"].AsArray;
						
				foreach (JSONNode arr_node in arr)
				{
					fb_share.Images.Add (arr_node);
				}

				m_fb_share_items.Add (id, fb_share);
						
			}

		}

		void Start ()
		{
			ReadStaticAchievementsData ();

		}

		public void Share (string share_key, Action<bool> ShareDone, string[] title_tokens=null, string[] desc_tokens=null)
		{

			FBShare fb_share = new FBShare();
			if (m_fb_share_items.TryGetValue (share_key, out fb_share))
			{
				string title = fb_share.Title;

				if(title_tokens!=null)
					title = string.Format(fb_share.Title,title_tokens);

				string description = fb_share.Description;

				if(desc_tokens!=null)
					description = string.Format (fb_share.Description,desc_tokens);

				int index = UnityEngine.Random.Range (0, fb_share.Images.Count);
				string image_url = fb_share.Images [index];


				FacebookController.Instance.Share(fb_share.Og_action,title, description,image_url,ShareDone);

			}
			else
			{
				LoggerController.Instance.LogError ("Share key doesnt exist");
			}

		}

	}
}