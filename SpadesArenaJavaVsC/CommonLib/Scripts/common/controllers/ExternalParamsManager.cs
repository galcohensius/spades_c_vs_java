using common.utils;
using System;
using System.Collections.Generic;

namespace common.controllers
{
    public class ExternalParamsManager : MonoBehaviour
    {


        public static string TEMP_STORE = "";
        public const string BONUS_CODE_PARAM = "bc";
        public const string MESSENGER_ID_PARAM = "mesID";

        [SerializeField] string m_QABonusCode = null;

        public static ExternalParamsManager Instance = null;

        private Dictionary<string, string> m_queryParams = null;

        private HashSet<string> m_processedAppUrls = new HashSet<string>();

        public void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public void Init()
        {
            try
            {
                InitParams();

            } catch (Exception e)
            {
                LoggerController.Instance.LogError("Cannot init params from External URL\n" + e);
            }
        }

#if UNITY_WEBGL || UNITY_EDITOR
        //FOR WEB
        private void InitParams()
        {
            if (Application.absoluteURL != "")
            {
                Uri appUrl = new Uri(Application.absoluteURL);
                m_queryParams = HttpUtils.ParseQueryParams(appUrl);
            }

        }

#elif UNITY_ANDROID
		//FOR ANDROID
		private void InitParams()
		{
			AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"); 
			AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

			AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject>("getIntent");
			string launchUrl = intent.Call<string> ("getDataString");
  
		    LoggerController.Instance.Log("ExternalParamsManager: Got Android launch url: " + launchUrl);
          
            if (launchUrl != null && !m_processedAppUrls.Contains(launchUrl))
            {
                Uri appUrl = new Uri(launchUrl);
                m_queryParams = HttpUtils.ParseQueryParams(appUrl);

                // Add the launch url to the set so it will not be processed again on the same session
                // Ios continues to send the URL every time the app is back from background
                m_processedAppUrls.Add(launchUrl);

            }

        }
#else
		//FOR IOS
		private void InitParams()
		{
            // NOTE:
            // In iOS, the launch URL is put into PlayerPrefs in AppsFlyerAppController.mm
            // Found in Plugins/iOS dir

            string launchUrl = PlayerPrefs.GetString("ios_launch_url", "");

		    LoggerController.Instance.Log("ExternalParamsManager: Got ios launch url: " + launchUrl);

            if (launchUrl != "" && !m_processedAppUrls.Contains(launchUrl)) {
                Uri appUrl = new Uri (launchUrl);
                m_queryParams = HttpUtils.ParseQueryParams (appUrl);

                // Add the launch url to the set so it will not be processed again on the same session
                // Ios continues to send the URL every time the app is back from background
                m_processedAppUrls.Add(launchUrl);

                // delete the PlayerPrefs
                PlayerPrefs.DeleteKey("ios_launch_url");

            } 
		}
#endif



        public string GetParam(string name, bool deleteAfterGet = true)
        {
#if QA_MODE && UNITY_EDITOR 
            if (!string.IsNullOrEmpty(m_QABonusCode) && name == BONUS_CODE_PARAM)
                return m_QABonusCode;
#endif

            if (m_queryParams != null && m_queryParams.ContainsKey(name))
            {
                string result = m_queryParams[name];
                if (deleteAfterGet)
                    m_queryParams.Remove(name);

                return result;
            }

            return null;
        }
    }
}

