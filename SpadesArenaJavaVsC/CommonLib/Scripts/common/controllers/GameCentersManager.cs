﻿using System;


namespace common.controllers
{

    public class GameCentersManager : MonoBehaviour
    {
        public static GameCentersManager Instance;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;

#if UNITY_ANDROID
            // Select the Google Play Games platform as our social platform implementation
            //GooglePlayGames.PlayGamesPlatform.Activate();

#endif
        }

        public void Login(Action<bool> login_done)
        {

            LoggerController.Instance.Log("Trying to login to Game Center...");

            Social.localUser.Authenticate((success) =>
            {
                if (success)
                {
                    LoggerController.Instance.Log("Game Center login success. User name: " + Social.localUser.userName);
                }
                else
                {
                    LoggerController.Instance.Log("Game Center login failed");
                }

                login_done(success);

            });

        }


        public bool IsLoggedIn()
        {
#if UNITY_WEBGL
            return false;
#else
            return Social.localUser.authenticated;
#endif
		}


		public void Logout()
        {
#if UNITY_ANDROID
            //GooglePlayGames.PlayGamesPlatform.Instance.SignOut();
#endif
        }

		public void ShowNativeAchievementsScreen() {
			if (IsLoggedIn())
				Social.ShowAchievementsUI();
		}

        public void UpdateAchievementProgress(string achievement_id, double progress)
        {
            if (IsLoggedIn())
                Social.ReportProgress(achievement_id, progress, null);
        }

	}
	
    
}