﻿using common.models;

namespace common.controllers.achievementadaptors
{

	public class AndroidAchievementsAdaptor : AchievementsAdaptor 
	{

		public void UpdateAchievement (Achievement ac)
		{
            double progress = 100* (double)ac.Counter / ac.Total;

            GameCentersManager.Instance.UpdateAchievementProgress(ac.Google_id, progress);
		}

	}
}