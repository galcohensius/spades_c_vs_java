﻿using common.models;

namespace common.controllers.achievementadaptors
{

	public interface AchievementsAdaptor
	{

		void UpdateAchievement (Achievement ac);

	}
}