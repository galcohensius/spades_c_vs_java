﻿using common.models;

namespace common.controllers.achievementadaptors
{

    public class iOSAchievementsAdaptor : AchievementsAdaptor
    {

        public void UpdateAchievement(Achievement ac)
        {

            double progress = 100*(double)ac.Counter / ac.Total;
            GameCentersManager.Instance.UpdateAchievementProgress(ac.Apple_id, progress);

        }

    }
}