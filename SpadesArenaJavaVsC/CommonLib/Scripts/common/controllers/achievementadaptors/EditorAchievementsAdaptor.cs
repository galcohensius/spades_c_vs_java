﻿using common.models;

namespace common.controllers.achievementadaptors
{

	public class EditorAchievementsAdaptor : AchievementsAdaptor 
	{

		public void UpdateAchievement (Achievement ac)
		{
			LoggerController.Instance.Log ("Achievement updated in Editor " + ac.Name); 
		}

	}
}