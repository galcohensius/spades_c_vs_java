﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace common.controllers
{
	/// <summary>
	/// Manages constants that are read from a remote JSON file
	/// </summary>
	public class ConstsManager : MonoBehaviour
	{

		private Dictionary<string,Const> consts;


		public static ConstsManager Instance;

		private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }


		public void LoadConsts(string url,Action<bool> loadCompletedDelegate) {
			StartCoroutine (LoadConstsCoroutine (url,loadCompletedDelegate));
		}

		private IEnumerator LoadConstsCoroutine(string url,Action<bool> loadCompletedDelegate) {

			LoggerController.Instance.LogFormat ("Trying to load consts from {0}",url);

			UnityWebRequest request = new UnityWebRequest (url, "GET");
			request.SetRequestHeader ("Content-Type", "application/json");
			request.SetRequestHeader ("Cache-Control", "no-cache");

			DownloadHandler downloadHandler = new DownloadHandlerBuffer ();

			request.downloadHandler = downloadHandler;

			yield return request.Send();

			consts = new Dictionary<string, Const> ();

			if (request.isNetworkError) {
				LoggerController.Instance.LogError ("Cannot load consts: "+request.error);

				if (loadCompletedDelegate != null)
					loadCompletedDelegate (false);
			} else {

				JSONObject respData = (JSONObject) JSON.Parse (downloadHandler.text);
				LoggerController.Instance.Log ("Const data received: "+respData.ToString());

				// Iterate over the json key value pairs
				foreach (KeyValuePair<string, JSONNode> pair in respData) {
					consts [pair.Key] = new Const (pair.Value);
				}
				if (loadCompletedDelegate != null)
					loadCompletedDelegate (true);

			}

		}


		public Const this[string name]
		{
			get {
				Const result = null;
				if (consts.TryGetValue (name, out result)) {
					return result;
				} else {
					return null;
				}
			}
		}


		public class Const
		{
			private string data;

            //dual constractor with different access levels used for unit testing only. normally should be internal
            //public is needed to be accessed and created during tests.
#if UNITY_EDITOR
            public Const(string data)
            {
                this.data = data;
            }
#else
            internal Const (string data)
			{
				this.data = data;
			}
#endif


            public static implicit operator string(Const c)
			{
				return c.data;
			}

			public static implicit operator int(Const c)
			{
				int result = 0;
				int.TryParse (c.data, out result);
				return result;
			}

			public static implicit operator float(Const c)
			{
				float result = 0;
				float.TryParse (c.data, out result);
				return result;
			}


		}
	}
}
