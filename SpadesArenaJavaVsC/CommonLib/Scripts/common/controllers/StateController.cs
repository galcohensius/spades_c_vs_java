﻿using System;
using System.Linq;

namespace common.controllers
{

    public class StateController : MonoBehaviour
    {

        protected System.Random random = new System.Random();

        protected string m_deviceId;
        protected string user_id = "";

        public Action<int> OnQAEnvChanged;

        public static StateController Instance;

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }

        /// <summary>
        /// Calculates the device identifier or generates a new one.
        /// The device identifier is preferably the advertising id.
        /// </summary>
        public void CalcDeviceIdAsync(Action DeviceIdReady)
        {
            // Try the player prefs first
            m_deviceId = PlayerPrefs.GetString("device_id");

            if (!string.IsNullOrEmpty(m_deviceId))
            {
                LoggerController.Instance.Log("Device id found in player prefs: " + m_deviceId);
                DeviceIdReady?.Invoke();
                return;
            }

            // Try to get the Advertising id
            if (!Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string errorMsg) =>
                {
                    if (trackingEnabled && !string.IsNullOrEmpty(advertisingId) && advertisingId != "00000000-0000-0000-0000-000000000000")
                    {
                        LoggerController.Instance.Log("Found advertising id to use as device id: " + advertisingId);
                        m_deviceId = advertisingId;

                        // Store the device id
                        PlayerPrefs.SetString("device_id", m_deviceId);

                        DeviceIdReady?.Invoke();

                    }
                    else
                    {
                        // Found bad advertising id, generate a device id
                        GenerateDeviceId();
                        DeviceIdReady?.Invoke();

                    }

                }))
            {
                // Cannot use advertising id, generate a device id
                GenerateDeviceId();
                DeviceIdReady?.Invoke();

            }

        }

        private void GenerateDeviceId()
        {
            m_deviceId = "generated-" + RandomString(26);
            LoggerController.Instance.Log("Generated device id: " + m_deviceId);

            // Store the device id
            PlayerPrefs.SetString("device_id", m_deviceId);
        }

        public string GetDeviceId()
        {
            return m_deviceId;
        }

        /// <summary>
        /// Gets the platform code.
        /// </summary>
        /// <returns>1-Android, 2-iOS, 3-Web, 4-Amazon</returns>
        public int GetPlatformCode()
        {
#if AMAZON_APP_STORE
            return 4;
#elif UNITY_IOS
            return 1;
#elif UNITY_ANDROID
            return 2;
#else
            return 3;
#endif
        }

        private string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Saves the user's settings.
        /// </summary>
        public void SaveSettings(int music, int sfx, bool notifications)
        {
            PlayerPrefs.SetInt(user_id + "Music", music);
            PlayerPrefs.SetInt(user_id + "SFX", sfx);
            PlayerPrefs.SetInt(user_id + "Notifications", notifications ? 1 : 0);
        }

       

        public void SaveNotifications(bool notifications)
        {
            PlayerPrefs.SetInt(user_id + "Notifications", notifications ? 1 : 0);
        }

        public void SaveMusic(float music)
        {
            int value = (int)(music * 100);
            LoggerController.Instance.Log("SaveMusic: " + value);
            PlayerPrefs.SetInt(user_id + "Music", value);
        }

        public void SaveSFX(float sfx)
        {
            int value = (int)(sfx * 100);
            LoggerController.Instance.Log("SaveSFX: " + value);
            PlayerPrefs.SetInt(user_id + "SFX", value);
        }

        public float GetMusicSetting()
        {
            float value = PlayerPrefs.GetInt(user_id + "Music", 0) / 100f;
            return value;
        }

        public float GetSFXSetting()
        {
            float value = PlayerPrefs.GetInt(user_id + "SFX", 60) / 100f;
            return value;
        }

        public bool GetFakeMultiSetting()
        {
            bool temp = PlayerPrefs.GetInt("FakeMulti", 1) == 1;

            return temp;
        }

        public bool GetNotificationSetting()
        {
            return PlayerPrefs.GetInt(user_id + "Notifications", 1) == 1;
        }

        public int GetHintClickedCount(string id)
        {
            return PlayerPrefs.GetInt(user_id + "HintClicked" + id);
        }

        public void IncHintClickedCount(string id)
        {
            int temp = PlayerPrefs.GetInt(user_id + "HintClicked" + id);
            PlayerPrefs.SetInt(user_id + "HintClicked" + id, temp + 1);
        }

        public int GetHintCancelledClickedCount(string id)
        {
            return PlayerPrefs.GetInt(user_id + "HintCancelledClicked" + id);
        }


        public void IncHintCancelledClickedCount(string id)
        {
            int temp = PlayerPrefs.GetInt(user_id + "HintCancelledClicked" + id);
            PlayerPrefs.SetInt(user_id + "HintCancelledClicked" + id, temp + 1);
        }

        public void DeleteData()
        {
            PlayerPrefs.DeleteAll();
        }

        public void SetAppleUserValue (string AppleUserKey, string appleUserValue)
        {
            // Apple related prefs are not user based!
            PlayerPrefs.SetString(AppleUserKey, appleUserValue);
        }

        public string GetAppleUserValue(string AppleUserKey)
        {
            // Apple related prefs are not user based!
            return PlayerPrefs.GetString(AppleUserKey,"");
        }

        public void DeleteAppleUserKey(string AppleUserKey)
        {
            PlayerPrefs.DeleteKey(AppleUserKey);
        }

        #region Purchase data restoration
        public void StorePurchaseDataByExtId(string externalId, string internalId)
        {
            PlayerPrefs.SetString(user_id + "PurchaseDataExtId" + externalId, internalId);
        }

        public string GetPurchaseDataByExtId(string externalId)
        {
            return PlayerPrefs.GetString(user_id + "PurchaseDataExtId" + externalId);
        }

        public void DeletePurchaseDataByExtId(string externalId)
        {
            PlayerPrefs.DeleteKey(user_id + "PurchaseDataExtId" + externalId);
        }

        public void StorePurchaseDataByTransId(string transId, string internalId)
        {
            PlayerPrefs.SetString(user_id + "PurchaseDataTransId" + transId, internalId);
        }

        public string GetPurchaseDataByTransId(string transId)
        {
            return PlayerPrefs.GetString(user_id + "PurchaseDataTransId" + transId);
        }

        public void DeletePurchaseDataByTransId(string transId)
        {
            PlayerPrefs.DeleteKey(user_id + "PurchaseDataTransId" + transId);
        }

        #endregion

        #region QA environment

        public void SetQAEnvironment(int index)
        {
            PlayerPrefs.SetInt("QA_Env_index", index); //not per user

            // Fire event
            OnQAEnvChanged?.Invoke(index);
        }

        public int GetQAEnvironment()
        {
            return PlayerPrefs.GetInt("QA_Env_index", 1); //not per user
        }


        #endregion


        #region AppsFlyer data for tracking events

        /// <summary>
        /// Set int value by key to the PlayerPrefs
        /// </summary>
        /// <param name="name">string key to store the value</param>
        /// <param name="value">The new value to be stored</param>
        /// <param name="increment">if increment is true - add the value to the existing stored value</param>
        public void StoreUserIntValue (string key, int value, bool incremental)
        {
            if (incremental)
                value += PlayerPrefs.GetInt(user_id + key, 0);
            PlayerPrefs.SetInt(user_id + key, value);
        }

        /// <summary>
        /// Get int value by key from the PlayerPrefs
        /// </summary>
        /// <param name="name">string key to extract the value</param>
        /// <returns>int value for the key parameter</returns>
        public int ExtractUserIntValue(string key)
        {
            return PlayerPrefs.GetInt(user_id + key, 0);
        }

        #endregion



        /// <summary>
        /// Checks if key exists in the PlayerPrefs, if not, set it to true for future calls.
        /// </summary>
        /// <returns>true if the key exists</returns>
        public bool CheckAndSet(string key)
        {
            bool val = PlayerPrefs.GetInt(user_id + key, 0) == 1;
            if (!val)
                PlayerPrefs.SetInt(user_id + key, 1);

            return val;
        }

        public int User_id
        {
            set
            {
                user_id = value + "_";
            }
        }
    }

}
