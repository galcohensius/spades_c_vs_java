using System;

namespace common.controllers
{
    /// <summary>
    /// Handle application moving to background and back to foreground 
    /// </summary>
    public class BackgroundController : MonoBehaviour
    {
		[SerializeField] double sessionTimeoutSec = 5*60;

        public Action OnGoingToBackground { get; set; }

        public Action<bool,TimeSpan> OnBackFromBackground { get; set; }
        public Action OnSessionTimeout { get; set; }
        public double SessionTimeoutSec { get => sessionTimeoutSec; }

        public static BackgroundController Instance;

        DateTime pauseTime = DateTime.Now;

        [Tooltip("some operations are required to save to playerprefs in real time and not only on app exit. use with caution")]
        [SerializeField]    //performance caution, use is 100% safe. by default this is off and runtime modifible via SavePlayerPrefsOnPause property
        private bool savePlayerPrefsOnPause = false;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }


        void OnApplicationPause(bool isPause)
        {
            if (isPause)
            {
                LoggerController.Instance.LogFormat("Application paused, time: {0}", DateTime.Now);

                // Store the current time
                pauseTime = DateTime.Now;
                if (savePlayerPrefsOnPause)
                    PlayerPrefs.Save();
                OnGoingToBackground?.Invoke();
            }
            else
            {
                TimeSpan pauseInterval = DateTime.Now - pauseTime;
                LoggerController.Instance.LogFormat("Application resumed, time: {0}, interval: {1} sec", DateTime.Now, pauseInterval.TotalSeconds);

                bool sessionTimeout = pauseInterval.TotalSeconds >= sessionTimeoutSec;

                // Fire events
                OnBackFromBackground?.Invoke(sessionTimeout, pauseInterval);

                if (sessionTimeout)
                    OnSessionTimeout?.Invoke();

                pauseTime = DateTime.Now;

            }

        }

        public void ForceSessionTimeout()
        {
            OnSessionTimeout?.Invoke();
        }

        public bool SavePlayerPrefsOnPause { get => savePlayerPrefsOnPause; set => savePlayerPrefsOnPause = value; }
    }
}
