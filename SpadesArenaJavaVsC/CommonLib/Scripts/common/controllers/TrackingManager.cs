using System;
using System.Collections.Generic;
using common.utils;
using common.facebook;
using common.experiments;
using System.Linq;
#if !UNITY_WEBGL

#endif

namespace common.controllers
{
    /// <summary>
    /// Manages tracking information per platform
    /// </summary>
    public class TrackingManager : MonoBehaviour
    {

        [SerializeField] string m_appsflyer_dev_key = null;
        [SerializeField] string m_ios_app_id = null;

        public static TrackingManager Instance;

        private OnboardingFunnelTracker m_onboardingFunnelTracker;
        private string m_experiment_id = "6";
        public string Experiment_Id
        {
            private get { return m_experiment_id; }
            set { m_experiment_id = value; }
        }

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public JSONObject GetTrackingInfo()
        {
#if UNITY_EDITOR
            return GetTrackingInfoEditor();
#elif UNITY_WEBGL
            return GetTrackingInfoWebGL();

#else
            return null;
#endif

        }


        private JSONObject GetTrackingInfoWebGL()
        {
            Uri appUrl = new Uri(Application.absoluteURL);

            Dictionary<string, string> queryParams = HttpUtils.ParseQueryParams(appUrl);

            JSONObject result = null;

            if (queryParams.Count > 0)
            {

                result = new JSONObject();

                string fb_source;
                string bb_aid;
                queryParams.TryGetValue("fb_source", out fb_source);
                queryParams.TryGetValue("bb_aid", out bb_aid);

                result["fbs"] = fb_source;
                result["aid"] = bb_aid;
            }

            return result;
        }

        private JSONObject GetTrackingInfoEditor()
        {
            JSONObject result = new JSONObject();

            result["fbs"] = "editor";
            result["aid"] = "1234";

            return result;
        }


#region AppsFlyer

        /// <summary>
        /// Start AppsFlyer tracking on mobile platforms.
        /// Should be called after server login
        /// </summary>
        /// <param name="userId">Server User identifier.</param>
        public void AppsFlyerStartTracking(int userId, string fcmSenderId)
        {
#if UNITY_IOS
            AppsFlyer.setAppsFlyerKey (m_appsflyer_dev_key);
            AppsFlyer.setAppID(m_ios_app_id);
            AppsFlyer.setCustomerUserID(userId.ToString());
            AppsFlyer.trackAppLaunch();

#elif UNITY_ANDROID
            AppsFlyer.init(m_appsflyer_dev_key);
            AppsFlyer.setAppID(Application.identifier);
            AppsFlyer.setCustomerUserID(userId.ToString());

#endif

#if QA_MODE
            AppsFlyer.setIsDebug (true);
#endif
        }


        public void AppsFlyerTrackPurchase(string priceUsd)
        {
            try
            {

                Dictionary<string, string> purchaseEvent = new Dictionary<string, string>
                {
                    { "af_currency", "USD" },
                    { "af_revenue", priceUsd },
                    { "af_quantity", "1" }
                };
                AppsFlyer.trackRichEvent("af_purchase", purchaseEvent);

                Debug.Log("Appsflyer event 'af_purchase' sent with data: " + priceUsd);
            }
            catch (Exception e)
            {
                Debug.LogError("Cannot send AppsFlyer event: " + e);
            }
        }

        public void AppsFlyerTrackEvent(string name, Dictionary<string, string> eventParams = null)
        {
#if !UNITY_WEBGL
            try
            {
                if (eventParams == null)
                    eventParams = new Dictionary<string, string>();
			           

                AppsFlyer.trackRichEvent(name, eventParams);
            }
            catch (Exception e)
            {
                Debug.Log("Cannot track appsflyer event: " + e);
            }

#endif
        }

        /// <summary>
        /// Interface to send incremental AppsFlyer events
        /// </summary>
        public void AppsFlyerTrackIncrementalEvent (string eventName, int eventValue, int[] eventMilestones)
        {
            string milestonesString = String.Join(",", eventMilestones.Select(i => i.ToString()).ToArray());
            LoggerController.Instance.Log("AppsFlyerTrackIncrementalEvent, eventName = : " + eventName + ", value = " + eventValue.ToString() + ", milestones = " + milestonesString);

            AppsFlyerTrackEvents(new AppsFlyerEvent(eventName).CreateIncrementalEvents(eventMilestones, eventValue));
        }

        /// <summary>
        /// Interface to send one time AppsFlyer event
        /// </summary>
        public void AppsFlyerTrackOneTimeEvent(string eventName)
        {
            LoggerController.Instance.Log("AppsFlyerTrackOneTimeEvent, eventName = : " + eventName);

            AppsFlyerTrackEvents(new AppsFlyerEvent(eventName).CreateOneTimeEvent());
        }

        /// <summary>
        /// Looping event list and sending each event to AppsFlyer
        /// </summary>
        /// <param name="TrackingEventsList">List of events to be set to AppsFlyer</param>
        private void AppsFlyerTrackEvents(List<string> TrackingEventsList)
        {
            foreach (string appsFlyerEvent in TrackingEventsList)
                AppsFlyerTrackEvent(appsFlyerEvent, null);
        }

        /// <summary>
        /// A helper class to decide if AppsFlyer events are needed and create the event string list
        /// Works for both one time and incremental events
        /// </summary>
        public class AppsFlyerEvent
        {
            private int eventValue;
            private int[] eventMilestones;
            private string eventName;
            private List<string> TrackingEventsList = null;

            /// <summary>
            /// Initializing AppsFlyerEvent class with event name
            /// </summary>
            public AppsFlyerEvent(string _eventName)
            {
                eventName = _eventName;
                TrackingEventsList = new List<string>();
            }

            /// <summary>
            /// Checks if an incremental event was fired already or not yet
            /// decision is made using the event name, the new event value and milestones for event sending
            /// If one or multiple events should be fired - it is added to the event string list 
            /// </summary>
            /// <param name="_eventMilestones"></param>
            /// <param name="_eventValue"></param>
            /// <returns>string list - null if no event needed or contains one or multiple events in the list</returns>
            public List<string> CreateIncrementalEvents (int[] _eventMilestones, int _eventValue)
            {
                int AccomulatedEventValue;
                int currentEventValue;
                eventValue = _eventValue;
                eventMilestones = _eventMilestones;

                currentEventValue = ExtractIntState(eventName);
                StoreIntState(eventName, eventValue, true);
                AccomulatedEventValue = ExtractIntState(eventName);
                var TrackingEventsRequired = eventMilestones.Where(milestone => milestone <= AccomulatedEventValue
                                                                && milestone > currentEventValue);
                foreach (int requiredEvent in TrackingEventsRequired)
                    TrackingEventsList.Add( eventName + requiredEvent.ToString() );

                return TrackingEventsList;
            }

            /// <summary>
            /// Checks if the one time event was fired already or not yet - by the event name
            /// If event still not fired - create an event
            /// </summary>
            /// <returns>string list - null if no event needed or containing the one time event string</returns>
            public List<string> CreateOneTimeEvent ()
            {
                int eventFiredStatus = ExtractIntState(eventName);
                if (eventFiredStatus == 0)
                {
                    TrackingEventsList.Add(eventName);
                    StoreIntState(eventName, 1);
                }
                return TrackingEventsList;
            }

            /// <summary>
            /// Set int value by key to the State Controller
            /// </summary>
            /// <param name="name">string key to store the value</param>
            /// <param name="value">The new value to be stored</param>
            /// <param name="increment">if increment is true - add the value to the existing stored value</param>
            private void StoreIntState(string name, int value, bool increment = false)
            {
                StateController.Instance.StoreUserIntValue(name, value, increment);
            }

            /// <summary>
            /// Get int value by key from the State Controller
            /// </summary>
            /// <param name="name">string key to extract the value</param>
            /// <returns>int value for the key parameter</returns>
            private int ExtractIntState(string name)
            {
                return StateController.Instance.ExtractUserIntValue(name);
            }
        }


#endregion

#region GoogleAnalyticsWeb

        public void GoogleAnalyticsTrackLoading(string label)
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            fireEvent(label);
#endif
        }

#if UNITY_WEBGL
        [DllImport("__Internal")]
        private static extern void fireEvent(string label);
#endif

#endregion

        public string Ios_app_id
        {
            get
            {
                return m_ios_app_id;
            }
        }

        public static class EventName
        {
            public const string StartedChallenge = "Started_challenge";
            public const string ReachedLevel = "Reached_level_";
            public const string FirstDeposit = "First_deposit";
            public const string SecondDayUser = "Second_day_user";
        }

        public OnboardingFunnelTracker OnboardingFunnelTracker
        {
            get
            {
                if (m_onboardingFunnelTracker == null)
                {
                    m_onboardingFunnelTracker = new OnboardingFunnelTracker(m_experiment_id);
                }
                return m_onboardingFunnelTracker;
            }
        }

        public void LogFirebaseEvent(string name, Dictionary<string, string> eventParams = null)
        {
#if !UNITY_WEBGL
            try
            {
                List<Parameter> parameters = new List<Parameter>();
                if (eventParams!=null)
                {
                    foreach (var eventParam in eventParams)
                    {
                        parameters.Add(new Parameter(eventParam.Key, eventParam.Value));
                    }
                }

                FirebaseAnalytics.LogEvent(name, parameters.ToArray());
            } catch (Exception e)
            {
                LoggerController.Instance.LogError("Cannot log firebase event. " + e);
            }
#endif
        }
    }


    public class OnboardingFunnelTracker
    {
        bool trackEvents;
        string m_expId;

        public OnboardingFunnelTracker(string expId)
        {
            m_expId = expId;
        }

        public void LogEvent(string eventName)
        {
            if (trackEvents)
            {
                int expId = ExperimentsManager.Instance.GetValue(m_expId);
                FacebookController.Instance.LogAppEvent(string.Format("OF4_{0}_{1}", expId, eventName), new Dictionary<string, object>());
            }
        }

        public bool TrackEvents
        {
            get
            {
                return trackEvents;
            }

            set
            {
                trackEvents = value;
            }
        }
    }
}
