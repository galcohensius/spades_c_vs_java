using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using common.utils;

namespace common.controllers
{
    public class RemoteAssetManager : MonoBehaviour
    {

        private const int MAX_LOAD_TRY_NUM = 3;

        // CDN bases - Spades consts
        //public const string CDN_BASE = "https://d3fzq6r8pai3mx.cloudfront.net/";
        //public const string CDN_BASE_QA = "https://d3q8708ow6bn0t.cloudfront.net/";
        //public const string CDN_BASE_DEV = "https://s3.eu-central-1.amazonaws.com/dev-cdn-spades-dynamic-files/";

        [SerializeField] private string m_cdnBase;
        [SerializeField] private string m_cdnBaseQa;
        [SerializeField] private string m_cdnBaseDev;

        public const int DAYS_TO_KEEP_CACHE = 6;

        public delegate void GraphicsLoadedDelegate(Sprite sprite, Texture2D rawTexture, object userData);

        public static RemoteAssetManager Instance;

        private Dictionary<string, AssetBundle> m_loadedBundles = new Dictionary<string, AssetBundle>();

        private HashSet<string> m_cancelledImageUrls = new HashSet<string>();

        private bool m_useCache = true;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        private void Start()
        {
            // Set the special room title
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                m_useCache = ldc.GetSettingAsBool("UseCacheForRemoteAssets", true);
            };
        }

        /// <summary>
        /// Loads the asset bundle from the web
        /// </summary>
		public void LoadAsset(string base_url, string path, Hash128 version, Action<GameObject> asset_ready)
        {
            if (m_loadedBundles.ContainsKey(path))
            {
                AssetBundle loaded_bundle = null;
                m_loadedBundles.TryGetValue(path, out loaded_bundle);

                string[] assets = loaded_bundle.GetAllAssetNames();

                asset_ready((GameObject)loaded_bundle.LoadAsset(assets[0]));

                LoggerController.Instance.LogFormat("Asset bundle {0} loaded from internal cache", path);
            }
            else
            {
                StartCoroutine(LoadAssetBundle(base_url, path, version, (AssetBundle bundle) =>
                {
                    if (bundle!=null)
                    {
                        m_loadedBundles[path] = bundle;

                        string[] assets = bundle.GetAllAssetNames();

                        if (assets.Length > 0)
                        {
                            // Get only the first GameObject from the bundle
                            GameObject go = (GameObject)bundle.LoadAsset(assets[0]);
                            ParticleUtil.ReapplyShadersOnTextMeshPro(go);
                            ParticleUtil.RefreshShadersandMaterials(go);

                            asset_ready(go);
                        }
                        else
                        {
                            LoggerController.Instance.LogError("Didnt find any asset in asset bundle: " + base_url + path);
                            asset_ready(null);
                        }
                    } else
                    {
                        asset_ready(null);
                    }
                }));
            }

        }

        public void LoadJSON(string path, Action<bool, JSONNode> loadComplete)
        {
            StartCoroutine(LoadJSONCoroutine(CdnBase, path, loadComplete));
        }

        private IEnumerator LoadJSONCoroutine(string base_url, string path, Action<bool, JSONNode> loadComplete)
        {

            LoggerController.Instance.Log("Requesting JSON file from: " + base_url + path);

            using (UnityWebRequest request = UnityWebRequest.Get(base_url + path))
            {
                yield return request.SendWebRequest();

                if (request.error != null)
                {

                    LoggerController.Instance.LogExceptionFormat(new Exception("Cannot load remote config: " + request.error));
                    loadComplete?.Invoke(false, null);
                }
                else
                {
                    try
                    {
                        JSONNode responseJson = JSON.Parse(request.downloadHandler.text) as JSONNode;
                        LoggerController.Instance.Log("Remote JSON file received: " + responseJson.ToString());
                        loadComplete?.Invoke(true, responseJson);
                    }
                    catch (Exception e)
                    {
                        LoggerController.Instance.LogExceptionFormat(e);
                        loadComplete?.Invoke(false, null);
                    }
                }

            }

        }

        public IEnumerator LoadAssetBundle(string base_url, string path, Hash128 version, Action<AssetBundle> asset_loaded)
        {

            string url = base_url + path;
            
            using (UnityWebRequest uwr = UnityWebRequestAssetBundle.GetAssetBundle(url, version, 0))
            {
                uwr.useHttpContinue = false;

                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    LoggerController.Instance.LogErrorFormat("Cannot find asset bundle file: {0}. Error is: {1}", url, uwr.error);
                    asset_loaded(null);
                }
                else
                {
                    // Get downloaded asset bundle
                    AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);

                    if (bundle == null)
                    {
                        LoggerController.Instance.Log("Cannot download asset bundle: " + url);
                        asset_loaded(null);
                    }
                    else
                    {
                        asset_loaded(bundle);

                        LoggerController.Instance.LogFormat("Asset bundle {0} loaded successfully", path);
                    }
                }

            }

        }

        public void UnloadBundle(string path,bool unloadAllLoadedObjects=true)
        {
            if (m_loadedBundles.ContainsKey(path))
            {
                m_loadedBundles[path].Unload(unloadAllLoadedObjects);
                m_loadedBundles.Remove(path);
            }
        }

        public void UnloadAllBundles()
        {
            foreach (AssetBundle bundle in m_loadedBundles.Values)
            {
                bundle.Unload(true);
            }
        }


        public void GetImage(string cdnUrl, string imageUrl, object userData, GraphicsLoadedDelegate image_callback,
             Action<float> load_progress = null, bool createSprite=true)
        {
            //check if image exists on the disk 
            string filePath = GetVersionedPersistentDataPath() + imageUrl;

            if (File.Exists(filePath) && m_useCache)
            {
                (Sprite sprite, Texture2D rawTexture) = LoadImageFromDisk(filePath);

                image_callback(sprite,rawTexture, userData);
            }
            else
            {
                // The file does not exist -> run event
                StartCoroutine(LoadImage(cdnUrl, imageUrl, userData, image_callback, load_progress,createSprite));

                CleanOldFiles("dataFiles", DAYS_TO_KEEP_CACHE); // TODO: The path should be received from an external resource
            }
        }

        public bool IsImageCached(string image_url)
        {
            string filePath = GetVersionedPersistentDataPath() + image_url;

            return File.Exists(filePath) && m_useCache;
        }

        public void CancelGetImage(string imageUrl)
        {
            LoggerController.Instance.Log("Canceling image load : " + imageUrl);
            m_cancelledImageUrls.Add(imageUrl);
        }

        private IEnumerator LoadImage(string cdnUrl, string imgUrl, object userData, GraphicsLoadedDelegate image_callback,
                                             Action<float> load_progress, bool createSprite)
        {
            m_cancelledImageUrls.Remove(imgUrl);


            int tryNum = 1;

            while (tryNum <= MAX_LOAD_TRY_NUM)
            {
                LoggerController.Instance.Log("Requesting image load: " + cdnUrl + imgUrl + ". Try num: " + tryNum);

                using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(cdnUrl + imgUrl))
                {
                    request.SendWebRequest();

                    while (!request.isDone && !m_cancelledImageUrls.Contains(imgUrl))
                    {
                        yield return new WaitForSeconds(0.2f);

                        load_progress?.Invoke(request.downloadProgress);

                    }

                    if (m_cancelledImageUrls.Contains(imgUrl))
                    {
                        // This image loading is cancelled
                        break;
                    }
                    else if (request.isHttpError || request.isNetworkError)
                    {
                        LoggerController.Instance.LogError("Error loading remote image, request error: " + request.error);

                        if (tryNum == MAX_LOAD_TRY_NUM)
                            image_callback(null, null, userData);

                        tryNum++;
                    }
                    else
                    {
                        //success
                        Texture2D rawTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;

                        if (rawTexture.width > 5)
                        {

                            // DO NOT SAVE ON WEBGL
                            // We encountered a WebGL memory error when trying to save and the browser will use its
                            // own cache so most images will load from the web only once.
                            if (Application.platform != RuntimePlatform.WebGLPlayer && m_useCache)
                                SaveImageToDisk(imgUrl, rawTexture);

                            if (load_progress != null)
                                load_progress(1);

                            Sprite image_sprite = null;
                            if (createSprite)
                                 image_sprite = Sprite.Create(rawTexture, new Rect(0, 0, rawTexture.width, rawTexture.height), new Vector2(0.5f, 0.5f));

                            image_callback(image_sprite, rawTexture, userData);

                        }
                        else
                        {
                            // Empty image - used for default images for banners
                            image_callback(null, null, userData);
                        }

                        break; // No retry
                    }
                }

            }
        }

        private (Sprite,Texture2D) LoadImageFromDisk(string imgUrl)
        {
            LoggerController.Instance.Log("Requesting image load from DISK: " + imgUrl);


            byte[] bytes = System.IO.File.ReadAllBytes(imgUrl);

            Texture2D texture = null;
            if (imgUrl.EndsWith("png"))
            {
                texture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            }
            else
            {
                texture = new Texture2D(1, 1, TextureFormat.RGB24, false);
            }


            texture.LoadImage(bytes, true);
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
            return (sprite, texture);
        }

        private void SaveImageToDisk(string imgUrl, Texture2D textImg)
        {
            try
            {
                LoggerController.Instance.Log("Saving image to: " + imgUrl + ", format: " + textImg.format);

                string filePath = GetVersionedPersistentDataPath() + imgUrl;

                string directory = filePath.Substring(0, filePath.LastIndexOf("/"));


                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);


                // Convert the compressed textures if needed, since we cannot save DXT compressed with EncodeTo functions
                // If not compressed save as is
                if (textImg.format == TextureFormat.DXT1)
                {
                    // JPG
                    Texture2D newTexture = new Texture2D(textImg.width, textImg.height, TextureFormat.RGB24, false);
                    newTexture.SetPixels(textImg.GetPixels(0), 0);
                    File.WriteAllBytes(filePath, newTexture.EncodeToJPG());
                    Destroy(newTexture);
                }
                else if (textImg.format == TextureFormat.DXT5)
                {
                    // PNG
                    Texture2D newTexture = new Texture2D(textImg.width, textImg.height, TextureFormat.RGBA32, false);
                    newTexture.SetPixels(textImg.GetPixels(0), 0);
                    File.WriteAllBytes(filePath, newTexture.EncodeToPNG());
                    Destroy(newTexture);
                }
                else if (textImg.format == TextureFormat.RGB24)
                {
                    File.WriteAllBytes(filePath, textImg.EncodeToJPG());
                }
                else if (textImg.format == TextureFormat.ARGB32 || textImg.format == TextureFormat.RGBA32)
                {
                    File.WriteAllBytes(filePath, textImg.EncodeToPNG());
                }
            }
            catch (Exception e)
            {
                LoggerController.Instance.LogFormat("Cannot save image {0} to disk. Exception: {1}", imgUrl, e);

                // Try to delete the cache dir, since it might be full
                try
                {
                    CleanOldFiles("dataFiles", 0);
                }
                catch (Exception e1)
                {

                    LoggerController.Instance.LogError("cannot clean old data files: " + e1);
                }
            }

        }

        private void CleanOldFiles(string folder_uri, int days_to_keep)
        {
            string directory = GetVersionedPersistentDataPath() + folder_uri;

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            string[] dirPaths = Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories);

            DateTime modification;
            TimeSpan delta;

            for (int i = 0; i < dirPaths.Length; i++)
            {
                modification = File.GetLastWriteTime(dirPaths[i]);

                delta = DateTime.Now - modification;

                if (delta.TotalDays > days_to_keep)
                {
                    LoggerController.Instance.Log("Deleting file from DISK: " + dirPaths[i]);
                    File.Delete(dirPaths[i]);
                }

            }
        }



        private string GetVersionedPersistentDataPath()
        {
            string ver = LocalDataController.Instance.GetSetting("files_cache_version", "v2");
            return Application.persistentDataPath + "/" + ver + "/";
        }

        public string CdnBase
        {
            get
            {
#if QA_MODE
                return m_cdnBaseQa;
#elif DEV_MODE
                return m_cdnBaseDev;
#else
                return m_cdnBase;
#endif
            }
        }
    }
}

