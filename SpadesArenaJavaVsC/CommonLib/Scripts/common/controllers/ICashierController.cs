﻿using common.controllers.purchase;
using common.ims.model;
using System;

namespace common.controllers
{
    public interface ICashierController
    {
        event Action<string, IMSGoodsList> PurchaseCompleted;
        event Action<string> PurchaseCanceled;
        void BuyPackage(string packageId, PurchaseTrackingData purchaseTrackingData = default, bool showPurchasePopup = true, PopupManager.AddMode purchasePopupAddMode = PopupManager.AddMode.ShowAndRemove);

        GameObject DefaultCashierPrefab { get; }
    }
}
