﻿using common.ims.model;
using common.models;
using common.utils;
using System;

namespace common.controllers
{
    public class VideoAdController : MonoBehaviour
    {
        [SerializeField] string m_ironSourceAndroidAppKey;
        [SerializeField] string m_ironSourceIOSAppKey;


#if QA_MODE || DEV_MODE
        private const string REWARDED_VIDEO_PLACEMENT = "DefaultPlacementQA";
#else
        private const string REWARDED_VIDEO_PLACEMENT = "DefaultRewardedVideo";
#endif

        private bool m_isIronSourceAgentInitialized = false;
        private Action<Bonus.BonusTypes, IMSInteractionZone> m_onRewardedVideoServed = null;
        private IronSourceSegment m_userIronSourceSegmentData = null;

        private bool m_capReady;
        private Action<bool> m_onCapReady;

        Bonus.BonusTypes m_lastVideoBonusType;
        IMSInteractionZone m_lastOriginIZone;

        private bool m_videosAreAvailable;
        public Action OnVideosNotAvailable;


        public static VideoAdController Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void OnApplicationPause(bool pause)
        {
#if !UNITY_WEBGL
            IronSource.Agent.onApplicationPause(pause);
#endif
        }

        public void InitIronSourceAgentForRewardedVideo(CommonUser user)
        {
#if UNITY_WEBGL
            return;
#endif

            LoggerController.Instance.Log("Initializing IronSource Agent...");


            string appKey = m_ironSourceAndroidAppKey;
#if UNITY_IOS
            appKey = m_ironSourceIOSAppKey;
#endif

            if (!m_isIronSourceAgentInitialized)
            {
                // Initialize The Segment Data Before The Agent.Init
                IronSourceEvents.onSegmentReceivedEvent += OnSegmentReceived;
                InitUserSegmentData(user);

                //For Rewarded Video
                IronSource.Agent.init(appKey, IronSourceAdUnits.REWARDED_VIDEO);

                AddIronSourceRewardedVideoListeners();
                m_isIronSourceAgentInitialized = true;

#if QA_MODE
                IronSource.Agent.validateIntegration(); //Itegration check - for QA
#endif
            }
        }

        private void InitUserSegmentData(CommonUser user)
        {
            IronSource.Agent.setUserId(user.GetID().ToString());

            m_userIronSourceSegmentData = new IronSourceSegment();

            m_userIronSourceSegmentData.level = user.GetLevel();

            if (user.User_type == CommonUser.UserType.Paying)
            {
                m_userIronSourceSegmentData.isPaying = 1;
            }
            else
            {
                m_userIronSourceSegmentData.isPaying = 0;
            }

            m_userIronSourceSegmentData.userCreationDate = DateUtils.GetUserTimeStampViaDaysFromReg(user.Days_from_reg);

            m_userIronSourceSegmentData.setCustom("uvs_level", user.Uvs_level.ToString());
            m_userIronSourceSegmentData.setCustom("activity_level", user.Activity_level.ToString());

            IronSource.Agent.setSegment(m_userIronSourceSegmentData);
        }

        private void AddIronSourceRewardedVideoListeners()
        {
            IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        }

        public void ShowRewardedVideo(Bonus.BonusTypes bonusTypes,IMSInteractionZone originIzone=null)
        {
#if UNITY_EDITOR
            //OnVideosNotAvailable?.Invoke();
            RewardedVideoAdRewardedEvent(new IronSourcePlacement("Test", "", 1));
            return;
#endif


            if (m_isIronSourceAgentInitialized && m_videosAreAvailable)
            {
                if (!IsRewardedVideoCapped())
                {
                    m_lastVideoBonusType = bonusTypes;
                    m_lastOriginIZone = originIzone;

                    LoggerController.Instance.LogFormat("Showing rewarded video on placement: " + REWARDED_VIDEO_PLACEMENT);
                    IronSource.Agent.showRewardedVideo(REWARDED_VIDEO_PLACEMENT);
                }
                else
                {
                    LoggerController.Instance.LogWarningFormat("Rewarded Video Capped For Placement : {0}, Will Not Play Rewarded Video", REWARDED_VIDEO_PLACEMENT);
                    OnVideosNotAvailable?.Invoke();
                }
            }
            else
            {
                LoggerController.Instance.LogWarning("IronSource agent not initialized or videos are not available yet");
                OnVideosNotAvailable?.Invoke();
            }

        }


        //Invoked when there is a change in the ad availability status.
        //@param - available - value will change to true when rewarded videos are available. 
        //You can then show the video by calling showRewardedVideo().
        //Value will change to false when no videos are available.
        private void RewardedVideoAvailabilityChangedEvent(bool available)
        {
            m_videosAreAvailable = available;

            LoggerController.Instance.Log("Rewarded video availability changed to: " + available);
        }

        //Invoked when the RewardedVideo ad view is about to be closed.
        //Your activity will now regain its focus.
        private void RewardedVideoAdClosedEvent()
        {
        }

        //Invoked when the RewardedVideo ad view has opened.
        //Your Activity will lose focus. Please avoid performing heavy 
        //tasks till the video ad will be closed.
        private void RewardedVideoAdOpenedEvent()
        {
        }


        //  Note: the events below are not available for all supported rewarded video 
        //   ad networks. Check which events are available per ad network you choose 
        //   to include in your build.
        //   We recommend only using events which register to ALL ad networks you 
        //   include in your build.

        //Invoked when the Rewarded Video failed to show
        //@param description - string - contains information about the failure.
        private void RewardedVideoAdShowFailedEvent(IronSourceError obj)
        {
            LoggerController.Instance.LogWarningFormat("Cannot play rewarded video. Error: {0} ,Code: {1}", obj.getDescription(), obj.getErrorCode());
        }

        //Invoked when the user completed the video and should be rewarded. 
        //If using server-to-server callbacks you may ignore this events and wait for the callback from the  ironSource server.
        //
        //@param - placement - placement object which contains the reward data
        //
        private void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
        {
            LoggerController.Instance.Log("Rewarded video shown on: " + placement.getPlacementName());

            OnRewardedVideoServed?.Invoke(m_lastVideoBonusType, m_lastOriginIZone);
        }

        //Invoked when the video ad finishes playing.
        private void RewardedVideoAdEndedEvent()
        {
        }

        //Invoked when the video ad starts playing.
        private void RewardedVideoAdStartedEvent()
        {
        }


        /// <summary>
        /// Get The Segment Name That The User Belongs To.
        /// After this event is invoked, the capping can be checked 
        /// </summary>
        /// <param name="segmentName">Segment name.</param>
        private void OnSegmentReceived(string segmentName)
        {
            LoggerController.Instance.Log("IronSource user segment: " + segmentName);

            // Here we know if the user is capped or not.
            m_capReady = true;
            m_onCapReady?.Invoke(IsRewardedVideoCapped());

        }



        public bool IsIronSourceAgentInitialized
        {
            get
            {
                return m_isIronSourceAgentInitialized;
            }
        }

        public bool IsRewardedVideoCapped()
        {
#if UNITY_EDITOR
            return false;
#elif UNITY_WEBGL
            return true;
#else
            return IronSource.Agent.isRewardedVideoPlacementCapped(REWARDED_VIDEO_PLACEMENT);
#endif
        }

        public void WaitOnVideoPlacementCapReady(Action<bool> OnCapReady)
        {
            if (m_capReady)
                OnCapReady(IsRewardedVideoCapped());
            else
            {
                m_onCapReady = OnCapReady;
            }

#if UNITY_EDITOR
            OnCapReady(false); // Never capped
#endif
        }

        public Action<Bonus.BonusTypes, IMSInteractionZone> OnRewardedVideoServed
        {
            get
            {
                return m_onRewardedVideoServed;
            }
            set
            {
                m_onRewardedVideoServed = value;
            }
        }

        public bool VideosAreAvailable { get => m_videosAreAvailable;}
    }
}
