﻿using System;
using System.Collections;

namespace common.controllers
{
	/// <summary>
	/// Used to monitor internet connection by checking against a defined URL every n seconds.
	/// </summary>
	public class ConnectionMonitor: MonoBehaviour
	{

		public Action<bool> ConnectionStateChangedDelegate;

		[SerializeField] int monitorIntervalSec=5;
		[SerializeField] string monitorUrl="www.google.com";


		bool connected = true;

		public static ConnectionMonitor Instance;

		private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }


		public void Start() {
			StartCoroutine (checkInternetConnection ());
		}


		private IEnumerator checkInternetConnection(){
			LoggerController.Instance.Log ("Starting connection check...");
			while (true) {
				WWW www = new WWW (monitorUrl);
				yield return www;
				if (www.error != null && connected) {
					connected = false;
					LoggerController.Instance.Log ("Connection Lost");
					if (ConnectionStateChangedDelegate != null)
						ConnectionStateChangedDelegate (false);
					
				} else if (www.error == null && !connected) {
					connected = true;
					LoggerController.Instance.Log ("Connection resumed");
					if (ConnectionStateChangedDelegate != null)
						ConnectionStateChangedDelegate (true);
				}
				yield return new WaitForSeconds (monitorIntervalSec);
			}	
		} 

	}
}

