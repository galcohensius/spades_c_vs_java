﻿#if !UNITY_WEBGL

#endif

namespace common.controllers
{
    public class FirebaseManager : MonoBehaviour
    {
#if !UNITY_WEBGL
        void Start()
        {

            // Initialize Firebase
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    // Crashlytics will use the DefaultInstance, as well;
                    // this ensures that Crashlytics is initialized.
                    FirebaseApp app = FirebaseApp.DefaultInstance;

                    // Set a flag here for indicating that your project is ready to use Firebase.


                    FirebaseApp.LogLevel = Firebase.LogLevel.Debug;

                    LoggerController.Instance.Log("Firebase is available");
                }
                else
                {
                    LoggerController.Instance.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });

        }
#endif
    }

}

