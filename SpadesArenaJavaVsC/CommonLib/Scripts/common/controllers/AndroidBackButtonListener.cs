﻿using System;


namespace common.controllers
{

    public class AndroidBackButtonListener : MonoBehaviour
    {


        private Action backButtonClicked;

		public static AndroidBackButtonListener Instance;

		private void Awake()
		{
            if (Instance == null)
                Instance = this;
		}


#if UNITY_ANDROID || UNITY_EDITOR
		void Update()
        {
			if (Input.GetKeyDown(KeyCode.Escape)) 
            {
                LoggerController.Instance.Log("Back button click detected");
                if (backButtonClicked != null)
                    backButtonClicked();
            }
        }
#endif


        public Action BackButtonClicked
        {
            get
            {
                return backButtonClicked;
            }

            set
            {
                backButtonClicked = value;
            }
        }
    }

}
