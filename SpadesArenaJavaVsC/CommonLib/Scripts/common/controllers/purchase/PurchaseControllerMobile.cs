﻿using System;
using System.Collections.Generic;
using common.ims.model;
using common.models;
using common.mes;
using common.ims;
using cardGames.models;

namespace common.controllers.purchase
{ 

    public class PurchaseControllerMobile : PurchaseController
    {
        private const int RETRY_DELAY_IF_NOT_INITIALIZED = 5; // Seconds

        private Purchaser m_purchaser;

        private PurchaseControllerCompletedDelegate m_purchaseCallback;

        private Dictionary<string, string> m_externalPackageIds = new Dictionary<string, string>();

        // We must store the package id locally here so it can be passed back to the callbalk
        string m_curr_item_id;

        PurchaseTrackingData m_curr_trackingData;

        /// <summary>
        /// Initialize the purchaser 
        /// </summary>
        public override void InitPurchaser(Cashier cashier, IPurchaseServer purchaseServer, MESBase mes, IMSController ims, PurchaseControllerCompletedDelegate restoredPurchaseCallback)
        {
            base.InitPurchaser(cashier, purchaseServer, mes, ims, restoredPurchaseCallback);

            if (cashier != null)
            {
                m_purchaser = new Purchaser();

                m_purchaser.InitPurchaser(GenerateProductDefinitions(), OnPurchaseComplete);
            }
        }

        public override void UpdatePurchaser()
        {
            try
            {
                m_purchaser.UpdatePurchaser(GenerateProductDefinitions());
            } catch (Exception e)
            {
                LoggerController.Instance.LogError("Cannot update mobile purchaser: " + e);
            }
        }

        private IEnumerable<ProductDefinition> GenerateProductDefinitions()
        {
            Dictionary<string, ProductDefinition> consumableProducts = new Dictionary<string, ProductDefinition>();

            // Add cashier products
            Dictionary<string, CashierItem> m_cashier_packages = new Dictionary<string, CashierItem>();
            foreach (CashierItem item in m_cashier.Cashier_packages)
            {
                string externalId = item.External_id;

                // Since UnityIAP can't have 2 products with the same store id, the external id is used as the key.
                // in order not to lose the product id, it is stored as the data in the m_payout definition object
                // The price is stored as the subtype - used for tracking

                PayoutDefinition payout = new PayoutDefinition(item.Price, 1, item.Id);

                ProductDefinition product = new ProductDefinition(externalId, externalId, ProductType.Consumable, true, payout);

                consumableProducts[externalId] = product;

                m_externalPackageIds[item.Id] = externalId;

                LoggerController.Instance.LogFormat("Purchaser: found cashier item {0} with external id {1}", item.Id, product.storeSpecificId);

            }

            // Add PO products from MES
            if (m_mes != null)
            {
                foreach (KeyValuePair<string, string> ids in m_mes.MESPackageIds)
                {
                    string price = ExtractPriceFromExternalId(ids.Value);

                    PayoutDefinition payout = new PayoutDefinition(price, 1, ids.Key);

                    ProductDefinition product = new ProductDefinition(ids.Value, ids.Value, ProductType.Consumable, true, payout);

                    consumableProducts[ids.Value] = product;

                    m_externalPackageIds[ids.Key] = ids.Value;

                    LoggerController.Instance.LogFormat("Purchaser: found MES item {0} with external id {1}", ids.Key, product.storeSpecificId);

                }
            }

            // Add PO products from IMS
            foreach (KeyValuePair<string, string> ids in m_ims.PackageIds)
            {
                string price = ExtractPriceFromExternalId(ids.Value);
                if (price == null)
                {
                    LoggerController.Instance.LogError($"Cannot extract price from IMS external id: {ids.Value}");
                    continue;
                }

                PayoutDefinition payout = new PayoutDefinition(price, 1, ids.Key);

                ProductDefinition product = new ProductDefinition(ids.Value, ids.Value, ProductType.Consumable, true, payout);

                consumableProducts[ids.Value] = product;

                m_externalPackageIds[ids.Key] = ids.Value;

                LoggerController.Instance.LogFormat("Purchaser: found IMS item {0} with external id {1}", ids.Key, product.storeSpecificId);

            }

            //add additional packages from Java server
            
            foreach (KeyValuePair<string, string> ids in ModelManager.Instance.GetExternalPackages())
            {
                string price = ExtractPriceFromExternalId(ids.Value); //here need external id 

                if (price==null)
                {
                    LoggerController.Instance.LogError($"Cannot extract price from external id: {ids.Value}");
                    continue;
                }

                PayoutDefinition payout = new PayoutDefinition(price, 1, ids.Key);

                ProductDefinition product = new ProductDefinition(ids.Value, ids.Value, ProductType.Consumable, true, payout);

                consumableProducts[ids.Value] = product;

                m_externalPackageIds[ids.Key] = ids.Value;

                LoggerController.Instance.LogFormat("Purchaser: found external item {0} with external id {1}", ids.Key, product.storeSpecificId);
            }
            
            return consumableProducts.Values;
        }



        public override void MakePurchase(string packageId, string loginToken, PurchaseTrackingData purchaseTrackingData, PurchaseControllerCompletedDelegate purchaseCallback)
        {
            m_purchaseCallback = purchaseCallback;

            m_curr_item_id = packageId;

            m_curr_trackingData = purchaseTrackingData;

            string externalPackageId = m_externalPackageIds[packageId];

            // Purchase restoration - Store the package id by external package id first
            StateController.Instance.StorePurchaseDataByExtId(externalPackageId, packageId);

            m_purchaser.PurchaseProduct(externalPackageId, RETRY_DELAY_IF_NOT_INITIALIZED);

        }

        private void OnPurchaseComplete(bool success, Product product, PurchaseFailureReason? fail_reason)
        {
            try
            {
                if (success)
                {
                    LoggerController.Instance.Log("Product receipt: " + product.receipt);


                    // Confirm the purchase with the server
                    JSONNode receiptJSON = JSON.Parse(product.receipt);
#if AMAZON_APP_STORE
                    string signature = "";

                    string payload = receiptJSON["Payload"];
                    JSONNode payloadJSON = JSON.Parse(payload);

                    JSONObject purchase_data = new JSONObject
                    {
                        ["receiptId"] = payloadJSON["receiptId"],
                        ["userId"] = payloadJSON["userId"]
                    };

#elif UNITY_ANDROID
                    string payload = receiptJSON["Payload"];

                    JSONNode payloadJSON = JSON.Parse(payload);

                    string signature = payloadJSON["signature"];
                    JSONNode purchase_data = payloadJSON["json"];
#else
                    string signature = receiptJSON["Payload"];
                    JSONNode purchase_data = receiptJSON;
#endif

                    JSONNode signedRequestJson = new JSONObject
                    {
                        ["signature"] = signature,
                        ["purchase_data"] = purchase_data
                    };

                    // Get the item id.
                    // If it was store before, use it.
                    // If it's null, try to restore it first using the transaction id, than using the external id
                    if (m_curr_item_id == null)
                    {
                        // Try to get the package id by transaction
                        m_curr_item_id = StateController.Instance.GetPurchaseDataByTransId(product.transactionID);
                        if (string.IsNullOrEmpty(m_curr_item_id))
                        {
                            // Try to get the package id by external id
                            m_curr_item_id = StateController.Instance.GetPurchaseDataByExtId(product.definition?.id);
                            LoggerController.Instance.Log("Product internal id restored from external id: " + m_curr_item_id);
                        } else
                        {
                            LoggerController.Instance.Log("Product internal id restored from transaction id: " + m_curr_item_id);
                        }
                    }


                    LoggerController.Instance.Log("Product internal id: " + m_curr_item_id);

                    if (m_curr_item_id==null)
                    {
                        LoggerController.Instance.SendLogDataToServer("Purchase", "Cannot restore purchase with external id " + product.definition?.id);
                        return;
                    }

                    // Store the package id for restoration 
                    StateController.Instance.StorePurchaseDataByTransId(product.transactionID, m_curr_item_id);
                    StateController.Instance.DeletePurchaseDataByExtId(product.definition?.id);


                    m_purchaseServer.ValidatePurchase(m_curr_item_id, product.transactionID, signedRequestJson.ToString(),
                                                  m_curr_trackingData,
                                                  (bool serverSuccess, int newCoinsBalance, IMSGoodsList imsGoodsList, Cashier new_cashier) =>
                    {

                        try
                        {

                            if (serverSuccess)
                            {


                                // Complete the purchase
                                m_purchaser.ConfirmPurchase(product);

                                // Call the delegate now so if the tracking code fails, users will still see their balance change
                                if (m_purchaseCallback != null)
                                    m_purchaseCallback(success, false, false, newCoinsBalance, imsGoodsList, new_cashier);
                                else
                                {
                                    // If there is no callback, it is probably since this method was called due to a non-finished
                                    // transaction that is now consumed. 

                                    //ModelManager.Instance.GetUser().SetCoins(newCoinsBalance);
                                    //LobbyTopController.Instance.UpdateCoinsText();
                                    m_restoredPurchaseCallback?.Invoke(success, false, false, newCoinsBalance, imsGoodsList, new_cashier);
                                }

                                IMSController.Instance.TriggerEvent(IMSController.EVENT_SUCCESS_DEPOSIT);
                                // Trigger login event 
                                MESBase.Instance?.Execute(MESBase.TRIGGER_SUCCESS_DEPOSIT);


                                // Update the purchaser, so if there are changes in the packages, it will be recorded
                                UpdatePurchaser();

                                // AppsFlyer tracking of purchase - subtype is used for price!
                                TrackingManager.Instance.AppsFlyerTrackPurchase(product.definition.payout.subtype);

                                TrackingManager.Instance.AppsFlyerTrackIncrementalEvent("DepositTimes", 1, new int[] { 3, 5, 10 });
                                if (float.TryParse(product.definition.payout.subtype, out float price))
                                    TrackingManager.Instance.AppsFlyerTrackIncrementalEvent("TotalDeposit", (int)Math.Round(price), new int[] { 20, 50, 100 });

                                StateController.Instance.DeletePurchaseDataByTransId(product.transactionID);

                            }
                            else
                            {
                                // Purchase validation failed
                                if (m_purchaseCallback != null)
                                    m_purchaseCallback(false, false, false, 0, null, null);
                            }

                        }
                        catch (Exception e)
                        {
                            LoggerController.Instance.LogError("Cannot complete purchasing.\n" + e);
                        }
                    });
                }
                else
                {
                    // Purchase failed from Unity IAP
                    if (m_purchaseCallback != null)
                        m_purchaseCallback(false, fail_reason == PurchaseFailureReason.UserCancelled, false, 0, null, null);

                }
            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError("Cannot complete purchasing.\n" + e);
            }


        }

        private string ExtractPriceFromExternalId(string externalId)
        {
            // Based on the standard external id pattern:
            // 4.99_usd_package

            string[] tokens = externalId.Split('_');
            if (tokens.Length == 3)
            {
                return tokens[0];
            }
            return null;
        }


    }
}

