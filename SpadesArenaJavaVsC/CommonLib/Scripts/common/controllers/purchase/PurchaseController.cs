﻿using common.models;
using common.ims.model;
using common.mes;
using common.ims;
using System.Collections.Generic;

namespace common.controllers.purchase
{

	public delegate void PurchaseControllerCompletedDelegate(bool success, bool canceled,bool latePayment,
        int newCoinsBalance, IMSGoodsList imsGoodsList, Cashier cashier);

	public abstract class PurchaseController: MonoBehaviour
	{
		private static PurchaseController m_instance;

        protected Cashier m_cashier;
        protected IPurchaseServer m_purchaseServer;
        protected MESBase m_mes;
        protected IMSController m_ims;

        private List<IMSGoodsList> m_lastGoodsList = new List<IMSGoodsList>();

        protected PurchaseControllerCompletedDelegate m_restoredPurchaseCallback;
        public PurchaseControllerCompletedDelegate RestoredPurchaseCallback
        {
            get { return m_restoredPurchaseCallback; }
            set { m_restoredPurchaseCallback = value; }
        }

        public static PurchaseController Instance {
			get {

				if (m_instance == null) {

                    GameObject purchaseControllerRoot = new GameObject("PurchaseControllerRoot");
                    DontDestroyOnLoad(purchaseControllerRoot);

#if UNITY_WEBGL || UNITY_EDITOR
                    purchaseControllerRoot.AddComponent<PurchaseControllerFacebook> ();
					m_instance = purchaseControllerRoot.GetComponent<PurchaseControllerFacebook> ();

#else
					purchaseControllerRoot.AddComponent<PurchaseControllerMobile> ();
					m_instance = purchaseControllerRoot.GetComponent<PurchaseControllerMobile>();
#endif
                }
                return m_instance;
			}
		}

        public List<IMSGoodsList> LastGoodsList { get => m_lastGoodsList; set => m_lastGoodsList = value; }

        public abstract void MakePurchase(string packageId, string loginToken, PurchaseTrackingData purchaseTrackingData, PurchaseControllerCompletedDelegate purchaseCallback);

		public virtual void InitPurchaser (Cashier cashier, IPurchaseServer purchaseServer, MESBase mes, IMSController ims, PurchaseControllerCompletedDelegate restoredPurchaseCallback)
        {
            m_restoredPurchaseCallback = restoredPurchaseCallback;
            m_cashier = cashier;
            m_purchaseServer = purchaseServer;
            m_ims = ims;
            m_mes = mes;
        }

        public virtual void UpdatePurchaser()
        {

        }

    }

   
}

