﻿using common.ims.model;
using common.models;

namespace common.controllers.purchase
{ 
        
    public delegate void PurchaseCompleted(bool success, int newCoinsBalance, IMSGoodsList imsGoodsList, Cashier cashier);

    public interface IPurchaseServer
    {
        void ValidatePurchase(string itemId, string transId, string paymentExtraData,
            PurchaseTrackingData purchaseTrackingData, PurchaseCompleted onComplete);
    }

    public struct PurchaseTrackingData
    {
        public string imsMetaData;
        public string imsOriginMetaData;
        public IMSActionValue imsActionValue;
        public int popupIndex;
        public string clientMetaData;
    }
}