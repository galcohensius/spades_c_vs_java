﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace common.controllers.purchase
{
    public delegate void PurchaseCompleteDelegate(bool success, Product product=null, PurchaseFailureReason? fail_reason = null);


    // Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
    public class Purchaser : IStoreListener
    {
        private IStoreController m_StoreController;          // The Unity Purchasing system.
        private IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

        PurchaseCompleteDelegate m_onPurchaseComplete;

        private HashSet<string> m_initializedProductIds;

        private IEnumerable<ProductDefinition> m_uninitializedProducts;

        public void InitPurchaser(IEnumerable<ProductDefinition> consumableProducts,PurchaseCompleteDelegate onPurchaseComplete)
        {
            // Initialize Unity IAP purchasing
            if (IsInitialized())
                return;

            LoggerController.Instance.LogFormat("Purchaser initializing with {0} products...", consumableProducts.Count());

            m_onPurchaseComplete = onPurchaseComplete;

            m_initializedProductIds = new HashSet<string>();

            // Create a builder, first passing in a suite of Unity provided stores.
            ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            builder.AddProducts(consumableProducts);

            // Init UnityIAP framework, expect a response either in OnInitialized or OnInitializeFailed
            UnityPurchasing.Initialize(this, builder);
        }

        public void UpdatePurchaser(IEnumerable<ProductDefinition> consumableProducts)
        {
            // Initialize Unity IAP purchasing
            if (!IsInitialized())
            {
                LoggerController.Instance.Log("Cannot update purchaser - not initialized, will try after init...");
                m_uninitializedProducts = consumableProducts;
                return;
            }

            HashSet<ProductDefinition> newProducts = new HashSet<ProductDefinition>();

            // Add only new products
            foreach (ProductDefinition productDefinition in consumableProducts)
            {
                if (!m_initializedProductIds.Contains(productDefinition.id))
                    newProducts.Add(productDefinition);
            }

            if (newProducts.Count>0) {
                LoggerController.Instance.LogFormat("Purchaser updating with {0} additional products...", newProducts.Count);
                m_StoreController.FetchAdditionalProducts(newProducts, UpdateInitializedProducts, OnInitializeFailed);
            }
        }


        private bool IsInitialized()
        {
            // Only say we are initialized if both the Purchasing references are set.
            return m_StoreController != null && m_StoreExtensionProvider != null;
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {

            m_StoreController = controller;
            m_StoreExtensionProvider = extensions;


            UpdateInitializedProducts();


            // THIS PROMPTS FOR PASSWORD ON IOS DEVICES, SO IS NOT CALLED FOR NOW
            // IT'S ALSO SEEMS THAT TRANSACTIONS THAT ARE NOT FINISHED ARE RESTORED AUTOMATICALLY, SO MAYBE
            // THIS METHOD IS ONLY FOR NON CONSUMABLES (RAN 15/7/2018)
            //RestorePurchases();

            LoggerController.Instance.Log("Purchaser initialized successfully");


            if (m_uninitializedProducts != null)
            {
                LoggerController.Instance.Log($"Purchaser updating {m_uninitializedProducts.Count()} uninitialized products...");
                UpdatePurchaser(m_uninitializedProducts);
                m_uninitializedProducts = null;
            }
        }


        public void OnInitializeFailed(InitializationFailureReason error)
        {
            LoggerController.Instance.LogError("Purchaser initialization failed. Error: " + error);
        }

        private void UpdateInitializedProducts()
        {
            // Store the initialized products for later updates checks
            foreach (Product product in m_StoreController.products.set)
            {
                m_initializedProductIds.Add(product.definition.id);
            }

        }



        /// <summary>
        /// Purchases a consumable product.
        /// </summary>
        /// <param name="productId">Store product identifier</param>
        public async void PurchaseProduct(string productId, int retryDelayIfNotInitialized)
        {
            if (IsInitialized())
            {

                // Look up the Product reference with the general product id 
                Product product = m_StoreController.products.WithID(productId);


                if (product != null && product.availableToPurchase)
                {
                    LoggerController.Instance.Log(string.Format("Purchasing product {0}", productId));

                    // Buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                    m_StoreController.InitiatePurchase(product);
                }
                else
                {
                    LoggerController.Instance.LogFormat("PurchaseProduct failed. Product {0} is not found or is not available for purchase",productId);

                    if (retryDelayIfNotInitialized > 0)
                    {
                        await Task.Delay(TimeSpan.FromSeconds(retryDelayIfNotInitialized));
                        PurchaseProduct(productId, 0);
                    }
                    else
                    {
                        m_onPurchaseComplete(success: false, fail_reason: PurchaseFailureReason.ProductUnavailable);
                    }

                }
            }
            else
            {
                LoggerController.Instance.Log("PurchaseProduct failed. Not initialized.");

                if (retryDelayIfNotInitialized>0)
                {
                    await Task.Delay(TimeSpan.FromSeconds(retryDelayIfNotInitialized));
                    PurchaseProduct(productId, 0);
                }
                else
                {
                    m_onPurchaseComplete(success: false, fail_reason: PurchaseFailureReason.PurchasingUnavailable);
                }

            }
        }



        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
        {
            LoggerController.Instance.LogFormat("Purchase successuful for product for {0}", purchaseEvent.purchasedProduct.definition.id);

            if (m_onPurchaseComplete != null)
                m_onPurchaseComplete(success: true, 
                                     product: purchaseEvent.purchasedProduct);

            return PurchaseProcessingResult.Pending;
        }


        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            LoggerController.Instance.LogFormat("Purchase failed for {0}. Reason: {1}", product.definition.id, failureReason);

            if (m_onPurchaseComplete != null)
                m_onPurchaseComplete(success: false,product: product, fail_reason: failureReason);

        }


        /// <summary>
        /// Confirms the purchase after server validation
        /// </summary>
        public void ConfirmPurchase(Product product) {
             m_StoreController.ConfirmPendingPurchase(product);
        }


        /// <summary>
        /// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
        /// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
        /// </summary>
        /// NOT NEEDED FOR CONSUMABLE PRODUCTS
        public void RestorePurchases()
        {
            if (!IsInitialized())
                return;

            // If we are running on an Apple device ... 
            if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
            {
                LoggerController.Instance.Log("RestorePurchases started ...");

                // Fetch the Apple store-specific subsystem.
                var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
                // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
                // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
                apple.RestoreTransactions((result) =>
                {
                    // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                    // no purchases are available to be restored.
                    LoggerController.Instance.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                });
            }
            else
            {
                // We are not running on an Apple device. No work is necessary to restore purchases.
                LoggerController.Instance.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            }
        }


    }

}