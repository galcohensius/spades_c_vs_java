﻿using common.models;
using common.facebook;

using common.ims.model;
using common.ims;
using common.mes;


namespace common.controllers.purchase
{
    public class PurchaseControllerFacebook : PurchaseController
    {
        public override void MakePurchase(string packageId, string loginToken, PurchaseTrackingData purchaseTrackingData, PurchaseControllerCompletedDelegate purchaseCallback)
        {
            // Invoke facebook pay API
            FacebookController.Instance.PurchaseItem(packageId, loginToken, m_cashier.CashierVersion, purchaseTrackingData,
                (bool success, bool canceled, bool latePayment, string paymentId, string signedRequest) =>
                {
                    if (success)
                    {
                        // Verify the purchase with the server
                        m_purchaseServer.ValidatePurchase(packageId, paymentId, signedRequest,
                                                       purchaseTrackingData,
                                                       (bool suc, int newCoinsBalance, IMSGoodsList iMSGoodsList, Cashier new_cashier) =>
                                                       {
                                                           purchaseCallback(suc, false, latePayment,newCoinsBalance, iMSGoodsList, new_cashier);

                                                           IMSController.Instance.TriggerEvent(IMSController.EVENT_SUCCESS_DEPOSIT);

                                                       // Trigger login event 
                                                       MESBase.Instance?.Execute(MESBase.TRIGGER_SUCCESS_DEPOSIT);
                                                       });
                    }
                    else
                    {
                        purchaseCallback(success, canceled, latePayment,0, null, null);
                    }
                });
        }
    }
}

