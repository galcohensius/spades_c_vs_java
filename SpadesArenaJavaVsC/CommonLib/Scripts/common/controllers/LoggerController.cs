using System.Collections.Generic;
using System.Text;
using System;


namespace common.controllers
{
    public abstract class LoggerController :MonoBehaviour
    {
        const int MAX_LINES = 1000;

#if UNITY_EDITOR || QA_MODE
        const int MAX_LINE_SIZE = int.MaxValue;
#else
        const int MAX_LINE_SIZE = 1000;
#endif

        const int MAX_CHUNK_LONG_STRING_SPLIT = 8000;

        public enum Module
        {
            General,
            Game,
            Comm,
            AI,
            Contest,
            chattingBrain,
            Anim,
            IMS
        }

        public enum LogLevel
        {
            Log,
            Warning,
            Error
        }

        [SerializeField] protected bool m_SendGeneral;
        [SerializeField] protected bool m_SendGame;
        [SerializeField] protected bool m_SendComm;
        [SerializeField] protected bool m_SendAI;
        [SerializeField] protected bool m_SendAnim;
        [SerializeField] protected bool m_SendContest;

        public static LoggerController Instance;


        LinkedList<string> m_log_lines = new LinkedList<string>();

        protected HashSet<string> m_clientLogTypesToSend = new HashSet<string>();


        private void Awake()
        {
            Instance = this;

        }

        private void Start()
        {
            LocalDataController ldc = LocalDataController.Instance;
            ldc.OnRemoteSettingsLoaded += () =>
            {
                string[] logTypesToSend = ldc.GetSetting("ClientLogTypesToSend", "").Split(',');
                foreach (var type in logTypesToSend)
                {
                    m_clientLogTypesToSend.Add(type);
                }
            };
        }

        private bool IsModuleFiltered(Module module)
        {
            if (module == Module.General)
                return m_SendGeneral;
            else if (module == Module.Game)
                return m_SendGame;
            else if (module == Module.Comm)
                return m_SendComm;
            else if (module == Module.AI)
                return m_SendAI;
            else if (module == Module.Contest)
                return m_SendContest;
            else if (module == Module.Anim)
                return m_SendAnim;
            else
                return false;
        }


        protected void AddToLog(string data,LogLevel logLevel, Module module)
        {
            // dont print empty logs
            if (String.IsNullOrEmpty(data))      
                return;

#if !QA_MODE
            data = data.Substring(0, Math.Min(data.Length, MAX_LINE_SIZE));
#else
            //splits extra long strings for Infra-QA
            if (data.Length > MAX_CHUNK_LONG_STRING_SPLIT)
            {
                BreakAndAddLongStringToLog(data, logLevel, module);
                return;
            }
#endif
            string formattedData = string.Format("{0} - {1} - {2} - {3}\n", DateTime.Now, logLevel, module, data);

            if (IsModuleFiltered(module))
            {
                m_log_lines.AddLast(formattedData);
            }

            if (m_log_lines.Count >= MAX_LINES)
                m_log_lines.RemoveFirst();

            switch (logLevel)
            {
                case LogLevel.Log:
                    Debug.Log(formattedData);
                    break;
                case LogLevel.Warning:
                    Debug.LogWarning(formattedData);
                    break;
                case LogLevel.Error:
                    Debug.LogError(formattedData);
                    break;

            }
        }

        //for really long logs, break'em up and print them all. For QA only
        private void BreakAndAddLongStringToLog(string data, LogLevel logLevel, Module module)
        {
            for (int chunk = 0; chunk < data.Length; chunk += MAX_CHUNK_LONG_STRING_SPLIT)
            {
                AddToLog(data.Substring(chunk, Math.Min(MAX_CHUNK_LONG_STRING_SPLIT, data.Length - chunk)), logLevel, module);
            }
        }

        public void Log(Module module, string message)
        {
            AddToLog(message, LogLevel.Log, module);
        }

        public void Log(string message)
        {
            Log(Module.General, message);
        }

        public void LogFormat(Module module, string format, params object[] args)
        {
            string message = string.Format(format, args); 
            AddToLog(message, LogLevel.Log, module);
        }

        public void LogFormat(string format, params object[] args)
        {
            LogFormat(Module.General, format, args);
        }

        public void LogExceptionFormat(Exception e)
        {
            
            AddToLog(e.ToString(), LogLevel.Error, Module.General);
        }

        public void LogWarning(Module module, string message)
        {
            AddToLog(message, LogLevel.Warning, module);
        }

        public void LogWarning(string message)
        {
            LogWarning(Module.General, message);
        }

        public void LogWarningFormat(Module module, string format, params object[] args)
        {
            string message = string.Format(format, args);
            AddToLog(message, LogLevel.Warning, module);
        }

        public void LogWarningFormat(string format, params object[] args)
        {
            LogWarningFormat(Module.General, format, args);
        }

        public void LogError(Module module, string message)
        {
            AddToLog(message, LogLevel.Error, module);
        }

        public void LogError(string message)
        {
            LogError(Module.General, message);
        }

        public void LogErrorFormat(Module module, string format, params object[] args)
        {
            string message = string.Format(format, args);
            AddToLog(message, LogLevel.Error, module);
        }

        public void LogErrorFormat(string format, params object[] args)
        {
            LogErrorFormat(Module.General, format, args);
        }

        public string GetListLog()
        {
            StringBuilder m_data_string = new StringBuilder();

            foreach (string line in m_log_lines)
            {
                m_data_string.Append(line);
            }

            return m_data_string.ToString();
        }

        public abstract void SendLogDataToServer(string logType, string send_data_reason);


    }



}