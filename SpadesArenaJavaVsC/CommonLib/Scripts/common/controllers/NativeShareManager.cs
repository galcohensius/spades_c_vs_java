﻿namespace common.controllers
{
    public class NativeShareManager 
    {
        public static void ShowNativeAndroidShare(string body, string title)
        {
            // Create Refernece of AndroidJavaClass class for intent
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            // Create Refernece of AndroidJavaObject class intent
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

            // Set action for intent
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            intentObject.Call<AndroidJavaObject>("setType", "text/plain");

            //Set Subject of action
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), title);
            //Set title of action or intent
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TITLE"), title);
            // Set actual data which you want to share
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), body);

            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");
            // Invoke android activity for passing intent to share data
            currentActivity.Call("startActivity", intentObject);
        }


#if UNITY_IOS

        public struct SocialSharingStruct
        {
            public string text;
            public string subject;
            public string filePaths;
        }

        [DllImport("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);


        public static void ShowNativeIosShare(string body, string subject, string url = null, string[] filePaths = null)
        {
            SocialSharingStruct conf = new SocialSharingStruct();
            conf.text = body;

            //string paths = string.Join(";", filePaths);
            //if (string.IsNullOrEmpty(paths))
            //	paths = url;
            //else if (!string.IsNullOrEmpty(url))
            //paths += ";" + url;

            string paths = ""; // Not used for now

            conf.filePaths = paths;
            conf.subject = subject;

            showSocialSharing(ref conf);
        }


#endif

    }

}
