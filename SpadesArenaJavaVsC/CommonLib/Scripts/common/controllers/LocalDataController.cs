using System;
using System.Collections.Generic;
using System.IO;

namespace common.controllers
{

    public abstract class LocalDataController : MonoBehaviour
    {

        public static LocalDataController Instance;

        [SerializeField] TextAsset m_countries_json_file = null;

        protected Dictionary<string, string> m_countries_table = new Dictionary<string, string>();
        protected Dictionary<string, JSONNode> m_settings = new Dictionary<string, JSONNode>();

        [SerializeField] protected bool m_show_message_debug = true;

        protected int m_app_version = -1;
        protected Action mOnRemoteSettingsLoaded = null;
        protected bool mIsSettingsLoaded = false;


        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        protected virtual void Start()
        {
            // NOT USED FOR NOW
            //ReadCountriesData();

        }


        public void LoadRemoteConfig(Action<bool> loadCompleted)
        {
            LoggerController.Instance.Log("LocalDataController loading remote settings");
            RemoteAssetManager.Instance.LoadJSON("client_config.json?c=" + DateTime.Now.Ticks, (bool success, JSONNode jsonData) =>
            {
                if (success)
                {
                    m_settings.Clear();
                    //add all values to colection
                    foreach (KeyValuePair<string, JSONNode> pair in jsonData.AsObject)
                    {
                        //single value
                        m_settings.Add(pair.Key, jsonData[pair.Key]);
                    }
                    mIsSettingsLoaded = true;
                    mOnRemoteSettingsLoaded?.Invoke();
                }
                loadCompleted?.Invoke(success);
            });


        }
        public string GetSetting(string key, string defaultValue = "")
        {
            if (m_app_version == -1)
                throw new Exception("app version not set");

            if (m_settings.ContainsKey(key))
            {
                if (m_settings[key].IsArray)
                {
                    //object value platform and/or version 
                    //find best candidate
                    for (int i = 0; i < m_settings[key].AsArray.Count; i++)
                    {
                        string dataStr = m_settings[key][i][0];

                        //parse data string
                        if (SettingsCheckCondition(dataStr, m_app_version, Application.platform))
                        {
                            //use value if conditions met
                            return m_settings[key][i][1].Value;
                        }
                    }
                }
                else
                {
                    // single value
                    return m_settings[key].Value;
                }
            }

            return defaultValue;
        }

        public int GetSettingAsInt(string key, int defaultValue = 0)
        {
            string strVal = GetSetting(key, defaultValue.ToString());
            return Convert.ToInt32(strVal);
        }

        public bool GetSettingAsBool(string key, bool defaultValue = false)
        {
            string strVal = GetSetting(key, defaultValue.ToString());
            return Convert.ToBoolean(strVal);
        }

        protected bool SettingsCheckCondition(string data, int version, RuntimePlatform platform)
        {//check ver
            bool retVal = true;

            string[] dataArr = data.Split('|');

            //go over all params and check if all params are met conditions
            foreach (string param in dataArr)
            {
                //analyse param
                int parseRes = 0;
                if (int.TryParse(param, out parseRes))
                {
                    retVal = version >= parseRes; //check if version is bigger than min ver
                }

                //check if its platform param and if the platform mets parameter
                if (param == "and" || param == "web" || param == "ios")
                {
                    retVal = ((platform == RuntimePlatform.Android && param == "and") ||
                    (platform == RuntimePlatform.IPhonePlayer && param == "ios") ||
                    (platform == RuntimePlatform.OSXPlayer && param == "ios") ||
                    (platform == RuntimePlatform.WebGLPlayer && param == "web"));
                }

                //is default
                if (param == "default")
                {
                    retVal = true;
                }

                if (!retVal)
                {
                    break; //no need for future check condition failed
                }
            }

            return retVal;
        }

        protected void ReadCountriesData()
        {
            JSONObject countries = (JSONObject)JSON.Parse(m_countries_json_file.ToString());

            foreach (KeyValuePair<string, JSONNode> pair in countries)
            {
                m_countries_table.Add(pair.Key, pair.Value);
            }

        }

        public string GetCountryNameByCode(string code)
        {
            string long_name;

            m_countries_table.TryGetValue(code, out long_name);

            if (long_name != null)
                return long_name;
            else
                return "N/A";
        }

        public Sprite GetCountryFlagByCode(string code)
        {
            Sprite flagSprite = Resources.Load<Sprite>("flags/" + code);
            return flagSprite;
        }

        // Save JSON
        public void SaveJSONFile(JSONNode node, string path)
        {
            string filePath = Application.persistentDataPath + "/" + path;
            string directory = filePath.Substring(0, filePath.LastIndexOf("/"));
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            File.WriteAllText(filePath, node.ToString());
        }

        // Load JSON
        public JSONNode LoadJSONFile(string path)
        {
            string filePath = Application.persistentDataPath + "/" + path;
            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                return JSON.Parse(dataAsJson);
            }
            return null;
        }

        public bool Show_message_debug
        {
            get
            {
                return m_show_message_debug;
            }
        }

        public int AppVersion
        {
            set
            {
                m_app_version = value;
            }

        }

        public Action OnRemoteSettingsLoaded
        {
            get
            {
                return mOnRemoteSettingsLoaded;
            }

            set
            {
                mOnRemoteSettingsLoaded = value;
                if (mIsSettingsLoaded)
                    mOnRemoteSettingsLoaded();
            }
        }
    }
}
