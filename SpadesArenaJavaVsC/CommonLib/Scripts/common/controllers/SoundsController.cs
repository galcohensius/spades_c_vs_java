﻿using System;
using System.Collections.Generic;

namespace common.controllers
{

    public abstract class SoundsController : MonoBehaviour
    {
        public enum MissionSFX
        {
            slider_open,
            slider_barFull,
            show_mission_popup,
            challenge_complete,
            mission_complete
        }

        [Header("Audio sources")]
        //[SerializeField] AudioSource		m_Particles_Source=null;
        [SerializeField] protected AudioSource m_SFX_Source1 = null;
        [SerializeField] protected AudioSource m_SFX_Source2 = null;
        [SerializeField] protected AudioSource m_SFX_Source3 = null;
        [SerializeField] protected AudioSource m_SFX_Source4 = null;
        [SerializeField] protected AudioSource m_SFX_Source5 = null;
        [SerializeField] protected AudioSource m_SFX_Slot1 = null;
        [SerializeField] protected AudioSource m_SFX_Slot2 = null;

        [SerializeField] protected AudioSource m_BG_Music_Source = null;

        protected List<AudioSource> m_aux_audio_sources = new List<AudioSource>();

        public static SoundsController Instance;
        protected bool m_sfx_channels_on = false;

        public Action<float> OnSFXVolumeChanged;

        protected AudioClip m_music;

        private float sfx_volume;
        private float music_volume;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public void CleanAllSFXChannels()
        {
            m_SFX_Source1.clip = null;
            m_SFX_Source2.clip = null;
            m_SFX_Source3.clip = null;
            m_SFX_Source4.clip = null;
            m_SFX_Source5.clip = null;

        }

        protected AudioSource PlayClip(AudioClip clip, bool loop = false)
        {
            AudioSource audio_source = GetFreeAudioSource();
            audio_source.clip = clip;

            if (audio_source.clip != null && audio_source.mute == false)
            {
                audio_source.Play();
                audio_source.loop = loop;
                if (audio_source.outputAudioMixerGroup.name.Contains("SFX"))
                    audio_source.volume = StateController.Instance.GetSFXSetting();
                else
                    audio_source.volume = StateController.Instance.GetMusicSetting();
            }

            return audio_source;
        }

        protected void StopClip(AudioSource audio_source)
        {
            if (audio_source != null)
            {
                audio_source.Stop();
                if (audio_source.clip != null)
                {
                    audio_source.clip.UnloadAudioData();
                    audio_source.clip = null;
                }
            }
        }

        protected AudioSource GetFreeAudioSource()
        {
            if (!m_SFX_Source1.isPlaying)
                return m_SFX_Source1;
            else if (!m_SFX_Source2.isPlaying)
                return m_SFX_Source2;
            else if (!m_SFX_Source3.isPlaying)
                return m_SFX_Source3;
            else if (!m_SFX_Source4.isPlaying)
                return m_SFX_Source4;
            else
                return m_SFX_Source5;
        }

        protected void PlayMusic(AudioSource source, AudioClip clip, bool loop = false)
        {
            AudioSource audio_source = source;
            audio_source.clip = clip;

            if (audio_source.clip != null && audio_source.mute == false)
            {
                audio_source.Play();
                audio_source.loop = loop;
            }

        }

        public void AddIMSBannerAudioSource(AudioSource audioSource)
        {
            m_aux_audio_sources.Add(audioSource);

            audioSource.enabled = m_sfx_channels_on;
        }

        public void RemoveIMSBannerAudioSource(AudioSource audioSource)
        {
            if (m_aux_audio_sources.Contains(audioSource))
            {
                m_aux_audio_sources.Remove(audioSource);
            }
        }

        protected void SetBGMusic(AudioClip bg_music)
        {
            if (bg_music == null)
                StopClip(m_BG_Music_Source);

            m_music = bg_music;
        }

        public void ChannelAdjustment(bool music_channel, float value)
        {
            if (music_channel)
            {
                ChangeVolume(true, value);
            }
            else
            {
                ChangeVolume(false, value);
            }
        }

        // Not in use anymore
        public void ChannelOnOff(bool music_channel, bool channel_on)
        {
            if (music_channel)
            {
                if (m_BG_Music_Source != null)
                {
                    m_BG_Music_Source.mute = !channel_on;

                    if (channel_on)
                    {
                        if (!m_BG_Music_Source.mute) { }
                        PlayBGMusic();
                    }
                    else
                        StopClip(m_BG_Music_Source);
                }
            }
            else
            {

                HandleSFX(channel_on);
            }
        }

        public void PlayBGMusic(bool stop = false)
        {
            if (stop)
            {
                m_BG_Music_Source.Stop();
            }
            else
            {
                if (!m_BG_Music_Source.isPlaying)
                    PlayMusic(m_BG_Music_Source, m_music, true);
            }
        }

        /// <summary>
        /// Sets the correct musicBG and SFX as it has been saved in the playerprefs
        /// </summary>
        public virtual void AdjustMusicAndSFX()
        {
            float sfxValue = StateController.Instance.GetSFXSetting();
            ChangeVolume(false, sfxValue);
            float musicValue = StateController.Instance.GetMusicSetting();
            ChangeVolume(true, musicValue);

            AudioSource[] allAudioSources = GetComponents<AudioSource>();
            foreach (AudioSource item in allAudioSources)
            {
                if (item.outputAudioMixerGroup.name.Contains("SFX"))
                    item.volume = sfxValue;
                else
                    item.volume = musicValue;
            }
        }

        public void ChangeVolume(bool music_channel, float new_volume)
        {
            if (music_channel)
            {
                if (m_BG_Music_Source != null)
                {
                    m_BG_Music_Source.volume = new_volume;
                    m_BG_Music_Source.mute = m_BG_Music_Source.volume == 0 ? m_BG_Music_Source.mute = true : m_BG_Music_Source.mute = false;
                }
                else
                    LoggerController.Instance.LogError(LoggerController.Module.General, "no music sound source assigned");
            }
            else
            {
                if (m_aux_audio_sources != null)
                {
                    foreach (AudioSource sfx_source in m_aux_audio_sources)
                    {
                        if (sfx_source == null)
                            continue;
                        sfx_source.volume = new_volume;
                        sfx_source.mute = sfx_source.volume == 0 ? sfx_source.mute = true : sfx_source.mute = false;
                    }

                    m_SFX_Source1.volume = new_volume;
                    m_SFX_Source2.volume = new_volume;
                    m_SFX_Source3.volume = new_volume;
                    m_SFX_Source4.volume = new_volume;
                    m_SFX_Source5.volume = new_volume;

                    if (m_SFX_Slot1 != null)
                        m_SFX_Slot1.volume = new_volume;
                    if (m_SFX_Slot2 != null)
                        m_SFX_Slot2.volume = new_volume;

                    m_SFX_Source1.mute = m_SFX_Source1.volume == 0 ? m_SFX_Source1.mute = true : m_SFX_Source1.mute = false;
                    m_SFX_Source2.mute = m_SFX_Source2.volume == 0 ? m_SFX_Source2.mute = true : m_SFX_Source2.mute = false;
                    m_SFX_Source3.mute = m_SFX_Source3.volume == 0 ? m_SFX_Source3.mute = true : m_SFX_Source3.mute = false;
                    m_SFX_Source4.mute = m_SFX_Source4.volume == 0 ? m_SFX_Source4.mute = true : m_SFX_Source4.mute = false;
                    m_SFX_Source5.mute = m_SFX_Source5.volume == 0 ? m_SFX_Source5.mute = true : m_SFX_Source5.mute = false;

                    if (m_SFX_Slot1 != null)
                        m_SFX_Slot1.mute = m_SFX_Slot1.volume == 0 ? m_SFX_Slot1.mute = true : m_SFX_Slot1.mute = false;
                    if (m_SFX_Slot2 != null)
                        m_SFX_Slot2.mute = m_SFX_Slot2.volume == 0 ? m_SFX_Slot2.mute = true : m_SFX_Slot2.mute = false;

                    OnSFXVolumeChanged?.Invoke(new_volume);
                }
                else
                    LoggerController.Instance.LogError(LoggerController.Module.General, "no SFX sound source(s) assigned");
            }

        }

        protected virtual void AdjustSFX(float value)
        {
            m_SFX_Source1.volume = value;
            m_SFX_Source2.volume = value;
            m_SFX_Source3.volume = value;
            m_SFX_Source4.volume = value;
            m_SFX_Source5.volume = value;
            if (m_SFX_Slot1 != null)
                m_SFX_Slot1.volume = value;
            if (m_SFX_Slot2 != null)
                m_SFX_Slot2.volume = value;

            OnSFXVolumeChanged?.Invoke(true ? 1 : 0);
        }

        protected virtual void HandleSFX(bool channel_on)
        {
            if (m_aux_audio_sources == null)
                return;

            m_sfx_channels_on = channel_on;

            m_SFX_Source1.enabled = channel_on;
            m_SFX_Source2.enabled = channel_on;
            m_SFX_Source3.enabled = channel_on;
            m_SFX_Source4.enabled = channel_on;
            m_SFX_Source5.enabled = channel_on;
            if (m_SFX_Slot1 != null)
                m_SFX_Slot1.enabled = channel_on;
            if (m_SFX_Slot2 != null)
                m_SFX_Slot2.enabled = channel_on;

            if (m_aux_audio_sources.Count > 0)
            {
                foreach (AudioSource source in m_aux_audio_sources)
                {
                    if (source != null)
                        source.enabled = channel_on;
                }
            }

            OnSFXVolumeChanged?.Invoke(channel_on ? 1 : 0);

        }


        protected float Sfx_volume { get => sfx_volume; set => sfx_volume = value; }
        protected float Music_volume { get => music_volume; set => music_volume = value; }

    }
}
