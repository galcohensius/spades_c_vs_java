﻿namespace common
{
    /// <summary>
    /// changes the curson over a UI object, same as ButtonsOverView without the button part. Used for WebGL only obviously
    /// </summary>
    [RequireComponent(typeof(Image))]
    public class CursorOverView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private static Texture2D m_handCursor;

#if !UNITY_WEBGL
        private void Awake()
        {
            Destroy(this);
        }
#endif
        private void Start()
        {
            // Load the hand cursor texture from Resources
            // This member is static, so will be loaded only once
            if (m_handCursor == null)
                m_handCursor = Resources.Load("Sprites/HandCursor") as Texture2D;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (gameObject.activeInHierarchy)
            {
                Cursor.SetCursor(m_handCursor, Vector2.zero, CursorMode.Auto);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

    }
}