﻿using System.Collections.Generic;
using System;

namespace common.mes
{
	public class MESDecision  
	{
		private int id;
		private String order;
		private List<MESMaterial> marketingMaterials;
		private Boolean oneTimeSession;
		private Boolean fullfilled = false;
		private int weight;

		public MESDecision(int d,String o,List<MESMaterial> m,Boolean s,int w)
		{
			id = d;
			order = o;
			marketingMaterials = m;
			oneTimeSession = s;
			weight = w;
		}


		public int Id 
		{
			get 
			{
				return id;
			}
		}

		public String Order 
		{
			get 
			{
				return order;
			}
		}



		public Boolean OneTimeSession 
		{
			get 
			{
				return oneTimeSession;
			}
		}

		public List<MESMaterial> MarketingMaterials
		{
			get {
				return marketingMaterials;
			}
		}

		public Boolean Fullfilled
		{
			get 
			{
				return fullfilled;
			}
			set {
				fullfilled = value;
			}
		}

		public int Weight
		{
			get 
			{
				return weight;
			}
		}
	}
}