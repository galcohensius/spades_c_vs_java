using System.Collections.Generic;

namespace common.mes
{
	public class MESJsonParser
	{
		static MESBase mMesBase;


		public static void Parse(MESBase mesBase, JSONNode json, bool clearAllKeys)
		{
			mMesBase = mesBase;

			if (json==null || json.Count == 0)
				return;

            // Clear the old events if needed
            if (clearAllKeys)
                mesBase.EventMap.Clear();


			foreach (KeyValuePair<string, JSONNode> keyValue in (JSONObject)json)
			{
                // For login events, clear the active PO
                if (keyValue.Key == MESBase.TRIGGER_LOGIN.ToString())
                {
                    mesBase.PO_material = null;
                }

                mesBase.EventMap[keyValue.Key] = new List<MESSet>();

				JSONArray eventArrJson = (JSONArray) keyValue.Value;	

				foreach (JSONNode setId in eventArrJson)
				{
					List<MESCondition> conditions;
					if ( setId ["cos"] != null )
					{
						conditions = parseConditions ((JSONObject)setId ["cos"]);
					}
					else
					{
						conditions = new List<MESCondition> ();
					}
					List<MESDecision> decisions = parseDecisions((JSONArray)setId["des"]);
					MESSet set = new MESSet(conditions, decisions, setId["iDs"].AsInt);
                    mesBase.EventMap[keyValue.Key].Add (set);
				}
			}
		}

		public static List<MESCondition> parseConditions(JSONObject conditions)
		{
			List<MESCondition> retVal = new List<MESCondition>();

			foreach (KeyValuePair<string, JSONNode> keyValue in conditions)
			{
				JSONObject conditionParams = (JSONObject) keyValue.Value;	
				MESCondition condition = new MESCondition(mMesBase, conditionParams["tye"], conditionParams["id"].AsInt, conditionParams ["vae"].AsArray);
				retVal.Add(condition);
			}

			return retVal;
		}

        static internal List<MESDecision> parseDecisions(JSONArray decisionObj)
		{
			List<MESDecision> decisions = new List<MESDecision>();
			List<MESMaterial> materials;

			for (int i=0  ;i < decisionObj.Count;i++)
			{		
				JSONObject decisionParams = (JSONObject) decisionObj[i];	
				materials = parseMaterials((JSONArray)decisionParams["vae"]);									
				MESDecision decision = new MESDecision(decisionParams["id"].AsInt, decisionParams["orr"], materials, decisionParams["oTS"].AsBool, decisionParams["wet"].AsInt);				
				decisions.Add(decision);
			}			
			return decisions;
		}
			
		static internal List<MESMaterial> parseMaterials(JSONArray materialsObj)
		{			
			List<MESMaterial> materials = new List<MESMaterial>();

			//there is no material
			if (materialsObj == null)
			{
				return materials;
			}

            for (int i = 0; i < materialsObj.Count; i++)
            {
                JSONObject materialParams = (JSONObject)materialsObj[i];

                MESMaterial material = new MESMaterial(materialParams["id"].AsInt,
                                                        materialParams["mTe"],
                                                        materialParams["aId"].AsInt,
                                                        materialParams["loc"],
                                                        materialParams["ste"],
                                                        materialParams["aPm"],
                                                        materialParams["src"],
                                                        materialParams["eDe"].AsDouble,
                                                        materialParams["eIl"].AsDouble,
                                                        materialParams["tPn"],
                                                        materialParams["tVy"].AsInt,
                                                        materialParams["dCt"].AsArray,
                                                        materialParams["cBBr"],
                                                        materialParams["tId"]);


                materials.Add(material);

                // Extract package ids if exists
                if (material.actionParam.IsArray)
                {
                    // Multiple offers
                    foreach (JSONNode offerNode in material.actionParam.AsArray)
                    {
                        if (offerNode["pId"] != null)
                        {
                            mMesBase.MESPackageIds[offerNode["pId"].Value] = offerNode["ePId"].Value;
                            mMesBase.PO_material = material;

                        }
                           
                    }
                }
                else
                {
                    // Single offer
                    if (material.actionParam["pId"] != null)
                    {

                        mMesBase.MESPackageIds[material.actionParam["pId"].Value] = material.actionParam["ePId"].Value;
                        mMesBase.PO_material = material;

                    }
                }

            }
			return materials;
		}
	}
}

