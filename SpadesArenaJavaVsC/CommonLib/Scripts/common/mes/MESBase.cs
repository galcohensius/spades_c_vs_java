using common.controllers;
using System.Collections.Generic;


namespace common.mes
{
	public abstract class MESBase:MonoBehaviour{

        private Dictionary<string, List<MESSet>> mEventMap = new Dictionary<string, List<MESSet>>();
        private Dictionary<string,string> mMESPackageIds = new Dictionary<string, string>(); // A dictionary of all packages that are found in MES events
        private MESMaterial m_PO_material = null;

        //Triggers
        public const int TRIGGER_LOGIN = 1;
        public const int TRIGGER_END_GAME = 2;
        // 3 is not used
        public const int TRIGGER_INSUFFICIAN_BALANCE = 4;
        public const int TRIGGER_SUCCESS_DEPOSIT = 5;
        public const int TRIGGER_CLOSE_CASHIER = 6;


        //Conditions
        public const int CONDITION_OPERAND_BALANCE = 1;
        public const int CONDITION_OPERAND_NUM_GAMES = 2;
        public const int CONDITION_OPERAND_NUM_WIN = 3;
        public const int CONDITION_OPERAND_NUM_LOSE = 4;
        public const int CONDITION_OPERAND_WIN_IN_ROW = 5;
        //session based
        public const int CONDITION_OPERAND_LOSE_IN_ROW = 6;
        //session based
        public const int CONDITION_OPERAND_NUM_SESSION = 7;
        public const int CONDITION_OPERAND_EVERY_NUM_SESSION = 8;
        //i.e every third session
        public const int CONDITION_OPERAND_EVERY_NUM_WIN = 9;
        //i.e every third win
        public const int CONDITION_OPERAND_EVERY_NUM_GAME = 10;
        //i.e every third game
        public const int CONDITION_OPERAND_LEVEL = 11;
        public const int CONDITION_OPERAND_MIN_TABLE = 12;
        public const int CONDITION_OPERAND_TABLE_WIN_IN_ROW = 13;
        public const int CONDITION_OPERAND_TABLE_LOSE_IN_ROW = 14;

        //DECISIONS
        public const int DECISION_SHOW_POPUPS = 1;
        public const int DECISION_SHOW_BANNERS = 2;
        public const int DECISION_OPEN_INVITE = 3;
        public const int DECISION_OPEN_SEND_GIFT = 6;
        public const int DECISION_OPEN_CASHIER = 7;
        public const int DECISION_OPEN_INBOX = 8;
        public const int DECISION_OPEN_DAILY_BONUS = 9;
        public const int DECISION_OPEN_LIKE = 11;

        public const int DECISION_SHOW_PROFILE = 12;
        public const int DECISION_SHOW_AVATAR = 13;
        public const int DECISION_SHOW_ACHHIVMENT = 14;
        public const int DECISION_SHOW_SETTING = 15;
        public const int DECISION_SHOW_TROPHY = 16;
        public const int DECISION_SHOW_CHAT_BOT = 17;
        public const int DECISION_SHOW_REDEEM_BONUS = 21;
        // 18 is not used
        public const int DECISION_SHOW_RATE_US = 19;

        public const int DECISION_PIGGY = 23;
        public const int DECISION_PIGGY_CONDITIONAL = 24;

        //Actions

        public const int ACTION_OPEN_CASHIER = 1;
        public const int ACTION_DIRECT_BUY = 2;
        public const int ACTION_OPEN_FB_INVITE_FRIENDS = 3;
        public const int ACTION_OPEN_POPUP = 4;
        public const int ACTION_GOTO_ROOM = 5;
        public const int ACTION_OPEN_EXTERNAL_URL = 6;
        public const int ACTION_OPEN_SEND_GIFT = 7;
        public const int ACTION_OPEN_GIFTSHOP = 8;
        public const int ACTION_OPEN_FB_SHARE = 10;
        public const int ACTION_OPEN_LEADERBOARD = 11;
        public const int ACTION_CLOSE_CONTINUE = 12;
        public const int ACTION_OPEN_INBOX = 13;
        public const int ACTION_OPEN_DAILYBONUS = 14;
        public const int ACTION_FB_CONNECT = 15;
        public const int ACTION_OPEN_INVITE = 16;
        public const int ACTION_OPEN_TABLE = 17;
        public const int ACTION_CLAIM_BONUS = 18;
        public const int ACTION_SHOW_PROFILE = 19;
        public const int ACTION_SHOW_AVATAR = 20;
        public const int ACTION_SHOW_ACHIEVEMENT = 21;
        public const int ACTION_SHOW_SETTING = 22;
        public const int ACTION_SHOW_TROPHY = 23;
        public const int ACTION_OPEN_CONTEST = 24;
        public const int ACTION_OPEN_RATEUS = 25;
        public const int ACTION_SHOW_REWARDED_VIDEO = 26;
        public const int ACTION_SHOW_MISSION_POPUP = 27;
        public const int ACTION_OPEN_ARENA = 29;
        public const int ACTION_OPEN_REDEEM_BONUS = 33;
        public const int ACTION_CLAIM_AND_OPEN_TABLE = 34;
        public const int ACTION_CLAIM_DYNAMIC_BONUS = 36;

        public const int ACTION_OPEN_SUPPORT_FORM = 41;
        public const int ACTION_OPEN_APP_STORE = 42;
        public const int ACTION_OPEN_SIDE_GAMES = 43;
        public const int ACTION_OPEN_CONTEST_INFO = 44;

        public const int ACTION_GOTO_PROMO_ROOM = 47;

        public const int ACTION_SHOW_DAILY_REWARDED_VIDEO = 48;
        public const int ACTION_SHOW_HOURLY_REWARDED_VIDEO = 49;
        public const int ACTION_SHOW_LEVELUP_REWARDED_VIDEO = 50;
        public const int ACTION_SHOW_LOSEINROW_REWARDED_VIDEO = 51;

        public const int ACTION_OPEN_NATIVE_INVITE_FRIENDS = 55;

        public const int ACTION_PIGGY = 37;
        public const int ACTION_PIGGY_INFO = 38;

        public const string MATERIAL_LOCATION_LOBBY = "lobby";

        //for old versions compatibility
        public const string MATERIAL_LOCATION_LOBBY_OLD = "invitebanner";
        public const string MATERIAL_LOCATION_DEAL_BUTTON = "salebanner";
        public const string MATERIAL_LOCATION_SMALL_BUY_BUTTON = "smBuyBtnbanner";
        public const string MATERIAL_LOCATION_BUY_BUTTON = "buyBtnbanner";
        public const string MATERIAL_LOCATION_PROMO_ROOM = "promoroombanner";
        public const string MATERIAL_LOCATION_SPECIAL_OFFER = "specialOffer";
        public const string MATERIAL_LOCATION_REWARDED_VIDEO_BUTTON = "videoBtnbanner";
        public const string DATA_FILES_DIR = "dataFiles";

        public static MESBase Instance;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }


		public void ParseMESJson(JSONNode json,bool clearAllEvents=false)
		{
            MESJsonParser.Parse (this, json,clearAllEvents);
		}

		public virtual bool Execute(int eventId, bool banners=false)
		{

			LoggerController.Instance.Log ("Executing event: " + eventId);

			//eventId = 1;

			bool validate = false;

			if (!mEventMap.ContainsKey(eventId.ToString())) {
				return validate;
			}

			List<MESSet> _sets = mEventMap[eventId.ToString()];

			for (int i = 0 ; i < _sets.Count ; i++)
			{
				if (_sets[i].validate())
				{					
					validate = DoDecision(_sets[i],eventId, banners);
					break;
				}
			}		
			return validate;
		}

		private bool DoDecision(MESSet mesSet,int eventId, bool banners)
		{					
			List<MESDecision> decisions = mesSet.MDecisions;

			if (decisions.Count > 0)
			{
                if (banners)
                    ExecuteBannerDecision(decisions);
                else
				    ExecuteDecision(decisions,eventId);		
				return true;
			}
			else
			{
				return false;
			}
		}
			
		public abstract int GetOperandValue (int operandId);
		public abstract void ExecuteDecision (List<MESDecision> decisions, int eventId);
        public abstract void ExecuteBannerDecision(List<MESDecision> decisions);

        public Dictionary<string, List<MESSet>> EventMap
        {
            get
            {
                return mEventMap;
            }
        }

        public Dictionary<string,string> MESPackageIds
        {
            get
            {
                return mMESPackageIds;
            }
        }

        public MESMaterial PO_material
        {
            get
            {
                return m_PO_material;
            }

            set
            {
                m_PO_material = value;
            }
        }

        public abstract void HandleInsufficientFunds(int delta, PopupManager.AddMode addMode = PopupManager.AddMode.ShowAndRemove);
    }
}
