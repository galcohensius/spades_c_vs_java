using System.Collections.Generic;

namespace common.mes
{
	public class MESMaterial
	{

		int mId;
		string mMime_type;
		int mActionId;
		string mLocation;
		int mWidth;
		int mHeight;
		JSONNode mActionParam;
		string mSource;
		double mExpired;
		double mInterval;
		string mTimerPos;
		int mTimerVisibilty;
		JSONArray mDynamicContent;
        List<MESMaterial> childMaterials;
        string mCloseButtonMode;
        string mtemplateId=null;

		public MESMaterial (int id, string mime, int aId, string loc, JSONNode sty, JSONNode acPa, string src, double expired, double interval, string timerPos, int timerVisibilty,JSONArray dynamicContent, string closeButtonMode, string templateId=null)
		{
			mId = id;
			mMime_type = mime;
			mActionId = aId;
			mLocation = loc;
			mWidth = sty ["wih"].AsInt;
			mHeight = sty ["het"].AsInt;
			mActionParam = acPa;
			mSource = src;
			mExpired = expired;
			mInterval = interval;
			mTimerPos = timerPos;
			mTimerVisibilty = timerVisibilty;
			mDynamicContent = dynamicContent;
            mCloseButtonMode = closeButtonMode;

            // For open popup actions, action param is always a list of materials
            if (mActionId == MESBase.ACTION_OPEN_POPUP)
            {
                childMaterials = MESJsonParser.parseMaterials((JSONArray)mActionParam);
            }
            else
            {
                childMaterials = new List<MESMaterial>();
            }

            mtemplateId = templateId;
            
        }

		public int id
		{
			get { return mId; }
		}

		public string mime_type
		{
			get { return mMime_type; }
		}

		public int actionId
		{
			get { return mActionId; }
		}

		public JSONNode actionParam
		{
			get { return mActionParam; }
		}

		public string location
		{
			get { return mLocation; }
		}


		public string source
		{
			get { return mSource; }

            
		}

		public double expired
		{
			get { return mExpired; }

            set { mExpired = value; }
        }

		public double interval
		{
			get { return mInterval; }
            set { mInterval = value; }
        }

		public string timerPos
		{
			get { return mTimerPos; }
		}

		public int timerVisibility
		{
			get { return mTimerVisibilty; }
		}

		public JSONArray dynamicContent
		{
			get { return mDynamicContent; }
		}

		public int MWidth
		{
			get {
				return mWidth;
			}
		}

		public int MHeight
		{
			get {
				return mHeight;
			}
		}

        public List<MESMaterial> ChildMaterials
        {
            get
            {
                return childMaterials;
            }

        }

        public string CloseButtonMode
        {
            get
            {
                return mCloseButtonMode;
            }
        }

        public string templateId
        {
            get { return mtemplateId; }
        }
    }
}
