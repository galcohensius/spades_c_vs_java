﻿namespace common.mes
{
	public interface IMESBase
	{
		void ParseMESJson(JSONObject json);
		bool Execute(int eventId);
		int GetOperandValue(int operandId);
	}
}