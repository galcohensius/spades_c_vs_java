﻿using System.Collections.Generic;
using System;

namespace common.mes
{
	public class MESSet  {

		private List<MESDecision> mDecisions;
		private List<MESCondition> mConditions;
		private int mInvokeDecision;

		public MESSet(List<MESCondition> conditions, List<MESDecision> decisions, int invokeDecision)
		{
			mConditions = conditions;
			mDecisions = decisions;
			mInvokeDecision = invokeDecision;
		}

		public bool validate()
		{
			for (int i = 0 ; i < mConditions.Count; i++)
			{
				if (!mConditions[i].validate())
				{
					return false;
				}
			}
			return true;
		}

		public List<MESDecision> getRandomDecisions()
		{
			List<MESDecision> tempDecisions = new List<MESDecision>(mDecisions);


			List<MESDecision> randomDecisions = new List<MESDecision> ();

			int iterationNum = Math.Min (mInvokeDecision, mDecisions.Count);

			if (iterationNum == -1) 
			{
				randomDecisions = tempDecisions;
			}
			else
			{
				for( int i =0;i<iterationNum;i++)					
				{
					int index =UnityEngine.Random.Range(0, tempDecisions.Count);
					randomDecisions.Add(tempDecisions[index]);
					tempDecisions.RemoveAt (index);
				}	
			}

			return randomDecisions;
		}




		public List<MESDecision> MDecisions {
			get {
				return mDecisions;
			}
		}

		public List<MESCondition> MConditions {
			get {
				return mConditions;
			}
		}


		public int MInvokeDecision {
			get {
				return mInvokeDecision;
			}
		}
	}
}