﻿namespace common.mes
{
	public class MESCondition {

	    public const string CONDITION_TYPE_RANGE  = "R";
		public const string CONDITION_TYPE_NUMBER = "N";
		public const string CONDITION_TYPE_CYCLE  = "C";

		MESBase mBase;
		string mType;
		int mOperandId;
		JSONArray mValues;

		public MESCondition(MESBase mesBase, string type,int operandId,JSONArray values)
		{
			mBase = mesBase;
			mType = type;
			mOperandId = operandId;
			mValues = values;
		}

		public bool validate()
		{
			int operand = mBase.GetOperandValue(mOperandId);
			bool result=false;
			string mesOperator = mValues[1];
			switch(mType)
			{
				case CONDITION_TYPE_RANGE:
				if (mesOperator == "-")
					{
						result = (operand >=mValues [0].AsInt && operand <= mValues [2].AsInt);
					}
					else if(mesOperator == ">")
					{
						result = (operand > mValues [2].AsInt);
					}
					else if(mesOperator == "<")
					{
						result = (operand < mValues [2].AsInt);
					}
					else if(mesOperator == ">=")
					{
						result = (operand >= mValues [2].AsInt);
					}
					else if(mesOperator == "<=")
					{
						result = (operand <= mValues [2].AsInt);
					}
					break;
				case CONDITION_TYPE_NUMBER:
					result = (operand == mValues[0].AsInt);
					break;
				case CONDITION_TYPE_CYCLE:
					result = (operand % mValues[0].AsInt == 0);
					break;
			}		

			return result;			
		}
	}
}