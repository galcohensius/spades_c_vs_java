﻿using common.utils;

namespace common.views
{
    //to avoid future confusion, when updating coins for the client, you should never change the
    //TMP.text value yourself. Instead, always use this. Also it needed to be set in the first place
    //like when the lobby is loaded so when collecting rewards it would update through here AFTER
    //'amount' was given its original base value from the model manager or whatever.
	public class BalanceCoinTextbox : MonoBehaviour {

		int amount;
		TMP_Text						m_text;
		[SerializeField]Animator		m_coin_box_animator;
		[SerializeField]GameObject		m_coin_box_target;

		void Start()
		{
			m_text = gameObject.GetComponent<TMP_Text> ();
		}

		public int Amount {
			get {
				return amount;
			}
			set {

				if(m_text==null)
					m_text = gameObject.GetComponent<TMP_Text> ();

				amount = value;
				m_text.text = FormatUtils.FormatBalance (amount);

			}
		}


		public Animator Coin_box_animator {
			get {
				return m_coin_box_animator;
			}
			set {
				m_coin_box_animator = value;
			}
		}

		public GameObject Coin_box_target {
			get {
				return m_coin_box_target;
			}
			set {
				m_coin_box_target = value;
			}
		}
	}
}