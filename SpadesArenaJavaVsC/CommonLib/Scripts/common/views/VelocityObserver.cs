using System;
using System.Collections;

namespace common.views
{
    public class VelocityObserver : MonoBehaviour
    {
        const float INTERVAL = 0.1f;
        public Action<float> OnSpeedUpdated;
        int lastXpos = 0;

        // Use this for initialization
        void Start()
        {
           // lastXpos = (int)gameObject.transform.localPosition.x;
           // StartCoroutine(CalculateVelocity());
        }

        private void OnEnable()
        {
            lastXpos = (int)gameObject.transform.localPosition.x;
            StartCoroutine("CalculateVelocity");
        }

        private void OnDisable()
        {
            StopCoroutine("CalculateVelocity");
        }

        IEnumerator CalculateVelocity()
        {
            while (true)
            {
                int currentXpos = (int)gameObject.transform.localPosition.x;

                float speed = (currentXpos - lastXpos) / INTERVAL;

                if (OnSpeedUpdated != null)
                    OnSpeedUpdated(speed);

                lastXpos = (int)gameObject.transform.localPosition.x;
                yield return new WaitForSecondsRealtime(INTERVAL);
            }
            
        }
    }
}
