﻿using System;

namespace common.views
{
	/// <summary>
	/// Used to add pointer down event to buttons
	/// </summary>
	public class PointerDownHandler : MonoBehaviour, IPointerDownHandler
	{
		[Serializable]
		public class ButtonPressEvent : UnityEvent
		{

		}

		public ButtonPressEvent OnPress = new ButtonPressEvent ();

		public void OnPointerDown (PointerEventData eventData)
		{
			OnPress.Invoke ();
		}
	}
}

