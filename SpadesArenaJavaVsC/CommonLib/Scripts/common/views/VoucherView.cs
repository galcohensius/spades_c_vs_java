﻿using common.models;
using System.Collections;
using common.utils;

namespace common.views
{
    public class VoucherView : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] protected TMP_Text m_quantityText;
        [SerializeField] protected TMP_Text m_timerText;
        [SerializeField] protected TMP_Text m_valueText;
        #endregion

        #region Private Members
        private VouchersGroup m_voucher;
        private Coroutine m_timer_coroutine;
        #endregion

        public void SetData(VouchersGroup voucher, bool fromLobby = true)
        {
            if (this == null)
                return;

            m_voucher = voucher;

            m_quantityText.text = m_voucher.Count.ToString();
            m_valueText.text = FormatUtils.FormatBuyIn(m_voucher.VouchersValue);

            if (gameObject.activeInHierarchy)
            {
                if (m_timer_coroutine != null)
                    StopCoroutine(m_timer_coroutine);
                m_timer_coroutine = StartCoroutine(TimerCoroutine());
            }
        }

        private IEnumerator TimerCoroutine()
        {
            while (true)
            {
                long interval = DateUtils.TotalSeconds(m_voucher.EarliestExpireTime);

                if (interval < 0)
                    break;

                m_timerText.text = DateUtils.ConvertSecondsToDateOrDays(interval);

                if (interval < 24*60*60)
                    yield return new WaitForSecondsRealtime(1);
                else
                    yield return new WaitForSecondsRealtime(60*10); // We can even wait more.. 
            }
        }
    }
}
