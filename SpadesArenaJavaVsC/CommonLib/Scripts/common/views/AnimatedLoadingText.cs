﻿using System.Collections;

namespace common.views
{

    public class AnimatedLoadingText : MonoBehaviour
    {

        string text;
        int numOfDots;

        // Use this for initialization
        void Start()
        {
            text = gameObject.GetComponent<TMP_Text>().text;

            StartCoroutine(UpdateDots());
        }

        private IEnumerator UpdateDots()
        {
            while (true)
            {
                numOfDots++;
                if (numOfDots == 4)
                    numOfDots = 0;

                string dots = "";
                for (int i = 0; i < numOfDots; i++)
                {
                    dots += ".";
                }

                gameObject.GetComponent<TMP_Text>().text = text + dots;

                yield return new WaitForSeconds(0.2f);
            }
        }
    }
}

