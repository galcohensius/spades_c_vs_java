﻿using System;

namespace common.views
{
	/// <summary>
	/// Used to add pointer down event to buttons
	/// </summary>
	public class PointerUpHandler : MonoBehaviour, IPointerUpHandler, IDragHandler
    {
		[Serializable]
		public class ButtonUpEvent : UnityEvent
		{

		}

		public ButtonUpEvent OnUp = new ButtonUpEvent();


        public void OnPointerUp(PointerEventData eventData)
        {
            OnUp.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {

        }

    }
}

