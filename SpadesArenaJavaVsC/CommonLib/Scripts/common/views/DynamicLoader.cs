﻿using System.Collections;
using System;

namespace common.views
{
    public class DynamicLoader : MonoBehaviour {

        [SerializeField] RectTransform m_progress_image = null;

        private float m_width;
        private float m_cur_val = 0;
        private float m_future_val = 0;

        private Coroutine m_updateCoroutine;

        private TimeSpan m_update_time = TimeSpan.FromSeconds(0.5f);

        private void Awake()
        {
            // Set the progress bar image width to the width of the component
            m_width = ((RectTransform)gameObject.transform).rect.width;

            m_progress_image.sizeDelta = new Vector2(m_width,0);
        }

        private void OnDisable()
        {
            if (m_updateCoroutine != null)
                StopCoroutine(m_updateCoroutine);
        }

        public void UpdateImmediate(float val) {
            m_cur_val = val;
            m_future_val = val;

            if (m_updateCoroutine != null)
                StopCoroutine(m_updateCoroutine);
            
            m_progress_image.localPosition = new Vector2(-m_width + val * m_width, 0);
        }

        public void UpdateWithAnim(float val,Action OnUpdateComplete=null) {
            if (val <= m_future_val)
                return;

            m_future_val = val;

            if (m_updateCoroutine != null)
                StopCoroutine(m_updateCoroutine);

            m_updateCoroutine = StartCoroutine(UpdateWithAnimCoroutine(val,OnUpdateComplete));

        }

        private IEnumerator UpdateWithAnimCoroutine(float val,Action OnUpdateComplete) 
        {
            float deltaVal = val - m_cur_val;
            DateTime startTime = DateTime.Now;
            float start_val = m_cur_val;

            while(m_cur_val<val) {
                yield return null;

                float timePercent = (DateTime.Now - startTime).Ticks / (float)m_update_time.Ticks;

                m_cur_val = start_val+ deltaVal * timePercent;

                if (m_cur_val >= val) m_cur_val = val;

                m_progress_image.localPosition = new Vector2(-m_width + m_cur_val * m_width, 0);

                //LoggerController.Instance.Log("Val: " + m_cur_val);

            }

            if (OnUpdateComplete != null)
                OnUpdateComplete();
        }
    }
    
    
}
