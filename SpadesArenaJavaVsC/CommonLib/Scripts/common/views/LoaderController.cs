﻿using System.Collections;

namespace common.views
{
    /// <summary>
    /// Controller used in Loader scene
    /// </summary>
    public class LoaderController : MonoBehaviour
    {

        [SerializeField] CanvasScaler m_canvasScaler = null;

        [SerializeField] DynamicLoader m_loader = null;

        [SerializeField] GameObject m_loader_WEBGL_object;
        [SerializeField] GameObject m_loader_object;

        [SerializeField] string m_lobby_scene_name = "Game";

        private void Awake()
        {
            //Application.backgroundLoadingPriority = ThreadPriority.BelowNormal;

            #if !UNITY_WEBGL
            if (Camera.main.aspect > 16 / 9f)
            {
                m_canvasScaler.matchWidthOrHeight = 1f;
            }
            m_loader_WEBGL_object?.SetActive(false);
            m_loader_object?.SetActive(true);

#else
            //WEBGL
            m_loader_WEBGL_object?.SetActive(true);
            m_loader_object?.SetActive(false);


#endif

        }

        // Use this for initialization
        void Start()
        {

            m_loader.UpdateImmediate(0f);

            // Load the game scene
            StartCoroutine(LoadGameScene());

            //m_loader.UpdateWithAnim(1);

        }
        IEnumerator LoadGameScene()
        {
            

            yield return null;

            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(m_lobby_scene_name);
            // Wait until the asynchronous scene fully loads
            while (asyncLoad.progress<0.9f)
            {
                yield return new WaitForSeconds(0.2f);

                m_loader.UpdateWithAnim(asyncLoad.progress / 2);

            }

            yield return new WaitForSeconds(5f);

        }



    }
}

