﻿using common.mes;
using System;
using common.utils;
using common.ims.model;

namespace common.views
{
    public class BuyItemTemplate : MonoBehaviour
    {
        [SerializeField] Button[] m_buttons = null;
        [SerializeField] TMP_Text m_coins = null;

        public void PopulateTemplate(MESMaterial material, Action button_action)
        {
        
        }

        public void PopulateTemplate(IMSInteractionZone iZone, Action button_action)
        {
            if (iZone.DynamicContent != null && iZone.DynamicContent.Content.Count > 0)
                m_coins.text = "Only <sprite=1> " + FormatUtils.FormatBalance(iZone.DynamicContent.Content[0]["cot"]);

            //buttons part
            if (button_action != null)
            {
                for (int i = 0; i < m_buttons.Length; i++)
                {
                    m_buttons[i].onClick.AddListener(delegate { button_action(); });
                }
            }

            gameObject.SetActive(true);
        }
    }
}
