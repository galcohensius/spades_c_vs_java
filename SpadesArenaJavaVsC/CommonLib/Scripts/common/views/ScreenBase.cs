﻿using common.controllers;

namespace common.views
{

    public class ScreenBase : MonoBehaviour
    {

        protected ScreensManager m_manager;

        ScreenClosedDelegate m_close_delegate;


        public void Start()
        {

            Transform close_button = transform.FindDeepChild("CloseButton");

            if (close_button != null)
                close_button.gameObject.GetComponent<Button>().onClick.AddListener(CloseButtonClicked);
        }

        public virtual void CloseButtonClicked()
        {

        }

        public virtual void Show(ScreensManager manager, ScreenClosedDelegate close_delegate)
        {
            AndroidBackButtonListener.Instance.BackButtonClicked += OnBackButtonClicked;
            m_manager = manager;
            m_close_delegate = close_delegate;
            this.gameObject.SetActive(true);
        }

        private void OnDisable()
        {
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
        }

        public virtual void OnBackButtonClicked()
        {
            // Do nothing by default
        }

        public virtual void Close()
        {
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
            if (m_close_delegate != null)
                m_close_delegate();

            this.gameObject.SetActive(false);
        }

    }
}