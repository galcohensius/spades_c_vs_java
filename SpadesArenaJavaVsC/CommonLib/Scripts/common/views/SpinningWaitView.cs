﻿using System.Collections;

namespace common.views
{
    public class SpinningWaitView : MonoBehaviour
    {

        RectTransform spinner_image;
        [SerializeField] int num_jumps = 8;
        float rotate_amount = 0;
        private void Awake()
        {
            spinner_image = transform.GetChild(0).gameObject.GetComponent<RectTransform>();
            rotate_amount = 360 / num_jumps;
        }

        private void Start()
        {
            StartCoroutine(Spin());
        }

        IEnumerator Spin()
        {
            while (true)
            {
                float curr_z = spinner_image.localEulerAngles.z;
                spinner_image.localEulerAngles = new Vector3(0, 0, curr_z - rotate_amount);
                yield return new WaitForSeconds(0.15f);
            }

        }
    }
}