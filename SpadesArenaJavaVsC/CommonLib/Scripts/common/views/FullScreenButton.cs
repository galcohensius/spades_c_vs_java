﻿using common.controllers;

namespace common.views
{
    public class FullScreenButton : Button
    {
        public FullScreenButton()
        {
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);

#if UNITY_WEBGL
			Screen.fullScreen = !Screen.fullScreen;
#endif
            LoggerController.Instance.Log("Full screen mode: " + Screen.fullScreen);
        }

        public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
#if UNITY_WEBGL
			Screen.fullScreen = !Screen.fullScreen;
#endif
            LoggerController.Instance.Log("Full screen mode: " + Screen.fullScreen);
        }
    }
}

