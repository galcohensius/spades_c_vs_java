﻿using common.ims.model;
using common.models;
using System.Collections.Generic;

namespace common.views

{

    public class GoodsListView : MonoBehaviour
    {

        public enum ToolTipPosition
        {
            Left,
            Right,
            Up,
            Down
        }

        [SerializeField] ToolTipPosition m_toolTipPosition;
        [SerializeField] ToolTipView m_toolTipView;
        [SerializeField] GameObject m_prefab;
        [SerializeField] Transform m_itemsHolder;
        [SerializeField] RectTransform m_mainRect;

        [SerializeField] GameObject m_overFlowHolder;
        [SerializeField] TMP_Text m_overFlowIndication;
        [SerializeField] bool m_forceFillAllItems;

        Vector2 m_bgSize= new Vector2();

        public int m_numItems;
        public int m_numRows;
        public int m_numCols;
        int m_numInventoryItems;
        int m_numVoucherItems;


        IMSGoodsList m_iMSGoodsList;

        public void InitGoodsListView(IMSGoodsList iMSGoodsList, Vector2 bgSize,int col, int row, bool centerMode)
        {
            m_iMSGoodsList = iMSGoodsList;
            m_bgSize = bgSize;
            m_numCols = col;
            m_numRows = row;

            m_numItems = 0;

            m_numInventoryItems= m_iMSGoodsList.InventoryItems.Count;
            m_numVoucherItems= m_iMSGoodsList.Vouchers.Count;

            m_numItems += m_numInventoryItems + m_numVoucherItems;

            BuildMenuItems(m_numItems, centerMode);
        }


        private void Start()
        {
            //BuildFakeData();
        }

        private void BuildFakeData()
        {
            m_iMSGoodsList = new IMSGoodsList();
            List<int> listCoins = new List<int>();
            List<VouchersGroup> vouchersGroups = new List<VouchersGroup>();
            List<string> items = new List<string>();

            listCoins.Add(3);
            listCoins.Add(33);
            listCoins.Add(333);

            List<VouchersGroup> voucherGroupList = new List<VouchersGroup>();
            VouchersGroup vouchersGroup = new VouchersGroup();
           
            Voucher voucher = new Voucher(1000);
            Voucher voucher2 = new Voucher(1000);
           
            vouchersGroup.AddVoucher(voucher);
            vouchersGroup.AddVoucher(voucher2);

            voucherGroupList.Add(vouchersGroup);

            items.Add("Item1");
            items.Add("Item2");
            items.Add("Item3");

            m_iMSGoodsList.Coins = listCoins;
            m_iMSGoodsList.Vouchers = voucherGroupList;
            m_iMSGoodsList.InventoryItems = items;
        }

        public void ShowToolTip()
        {
            m_toolTipView.Init(m_bgSize, m_toolTipPosition, m_iMSGoodsList);
        }

        private void HideToolTip()
        {
            m_toolTipView.HideToolTip();
        }

        private Vector2 GetAdjustedPrefabSize()
        {
            HideToolTip();

            Vector2 cellSize = new Vector2(m_bgSize.x / m_numCols, m_bgSize.y / m_numRows);
            GameObject tempMenuItem = Instantiate(m_prefab);
            Vector2 prefabSize = new Vector2();
            prefabSize = tempMenuItem.GetComponent<RectTransform>().rect.size;
            Destroy(tempMenuItem);

            float scaleFacotr = prefabSize.y / cellSize.y;
            //need to see which part is bigger factor compared with cell size

            if(prefabSize.x/cellSize.x>prefabSize.y/cellSize.y)
                scaleFacotr = prefabSize.x / cellSize.x;

            //if(scaleFacotr>1f)
            prefabSize = prefabSize / scaleFacotr;

            return prefabSize;
        }

        public void BuildMenuItems(int numItems, bool center)
        {
            foreach (Transform item in m_itemsHolder)
                Destroy(item.gameObject);

            Vector2 currItemSize = m_prefab.GetComponent<RectTransform>().rect.size;
            
            m_bgSize = m_mainRect.rect.size;

            currItemSize = GetAdjustedPrefabSize();

            float startOffsetX = currItemSize.x / 2 - m_bgSize.x / 2;
            float startOffsetY = m_bgSize.y / 2 - currItemSize.y / 2;

            

            int overflow = numItems-m_numRows*m_numCols;
            
            m_overFlowHolder.SetActive(overflow > 0);

            if (overflow > 0)
                m_overFlowIndication.text = "+" + overflow;

            //for preventaion of fualty data - 
            numItems = Mathf.Min(numItems, m_numRows * m_numCols);

            //int numItemsInGoodsList =  System.Enum.GetValues(typeof(GoodsListType)).Length;

# if!UNITY_EDITOR
            m_forceFillAllItems = false;
#endif

            //if (m_forceFillAllItems == false)
            //    numItems = numItemsInGoodsList;

            //for less rows then max
            int m_actualNumRows = numItems / m_numCols;

            if (numItems % m_numCols > 0)
                m_actualNumRows++;

            //for last row fixes
            int numItemsInLastRow = numItems % m_numCols;

            if (numItemsInLastRow == 0)
                numItemsInLastRow = m_numCols;

            if (!center)
                m_actualNumRows = m_numRows;

            float spaceX = (m_bgSize.x - (m_numCols * currItemSize.x)) / (m_numCols + 1);

            float spaceY = (m_bgSize.y - (m_actualNumRows * currItemSize.y)) / (m_actualNumRows + 1);
            
            int numItemSlots = m_numCols * m_numRows;

            for (int i = 0; i < m_numRows; i++)
            {
                for (int j = 0; j < m_numCols; j++)
                {
                    int itemIndex = i * m_numCols + j;

                    if (numItems > itemIndex)
                    {
                        GameObject menuItem = Instantiate(m_prefab);
                        menuItem.name = "Item " + itemIndex;
                        //menuItem.transform.GetChild(0).gameObject.GetComponent<TMP_Text>().text = itemIndex.ToString();
                        menuItem.transform.SetParent(m_itemsHolder);
                        RectTransform menuItemTransform = menuItem.GetComponent<RectTransform>();
                        menuItemTransform.localScale = new Vector3(1, 1, 1);
                        menuItemTransform.sizeDelta = currItemSize;

                        //menuItemTransform.sizeDelta = GetAdjustedPrefabSize();


                        GoodsListItemView goodsListItemView = menuItem.GetComponent<GoodsListItemView>();
                        //if(m_forceFillAllItems)
                        //    goodsListItemView.Init(m_iMSGoodsList, (GoodsListType)Random.Range(0, numItemsInGoodsList));
                        //else
                        //goodsListItemView.Init(m_iMSGoodsList, (GoodsListType)itemIndex);

                        goodsListItemView.Init(m_iMSGoodsList, GetTypeByIndex(itemIndex), GetAdjustedIndex(itemIndex));

                        if (center)
                            if(i+1==m_actualNumRows)
                                spaceX=(m_bgSize.x - (numItemsInLastRow * currItemSize.x)) / (numItemsInLastRow + 1);

                        float xPos = spaceX * (j + 1) + startOffsetX + (j * currItemSize.x);
                        
                        menuItemTransform.localPosition = new Vector2(xPos,startOffsetY - (i * currItemSize.y) - spaceY * (i + 1));
                    }
                   
                }
            }
        }

        private GoodsListType GetTypeByIndex(int index)
        {
            if (m_numInventoryItems > index)
                return GoodsListType.Items;
            else
                return GoodsListType.Vouchers;
        }

        private int GetAdjustedIndex(int index)
        {
            if (index < m_numInventoryItems)
                return index;
            else
                return index - m_numInventoryItems;
        }

        public void RebuildClickedCenter()
        {
            BuildMenuItems(m_numItems,true);
        }

        public void RebuildClickedFlow()
        {
            BuildMenuItems(m_numItems, false);
        }

    }
}