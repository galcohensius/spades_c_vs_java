﻿using common.controllers.social.sharer;
using static common.controllers.social.SocialBase;

namespace common.views.social
{
    public class SocialSharerViewBase : MonoBehaviour
    {
        [SerializeField] SharerBase[] sharers = null;

        protected SocialClickDelegate OnStartClick;
        protected SocialClickDelegate OnEndClick;

        public void Init(string userID)
        {
            foreach (SharerBase sharer in sharers)
            {
                sharer.Config(userID, OnStartClick, OnEndClick);
            }
        }

        public void SetClickDelegates(SocialClickDelegate onStartDelegate = null, SocialClickDelegate onEndDelegate = null)
        {
            OnStartClick += onStartDelegate;
            OnEndClick = onEndDelegate;
        }

        public SharerBase GetSharerByChannel(ChannelOptions channel)
        {
            foreach (SharerBase sharer in sharers)
            {
                if (sharer.socialChannel == channel)
                    return sharer;
            }
            return null;
        }

        public bool IsSharingAvailable()
        {
            foreach (SharerBase sharer in sharers)
            {
                if (sharer.GetComponent<Button>().interactable == true)
                    return true;
            }
            return false;
        }

        /*
        public virtual void ShareToChannel(SharerBase sharer)
        {
            if (sharer != null && sharer.isInitialized && sharer.IsAppInstalled())
            {
                sharer.SetActive(true);
                Button shareButton = sharer.GetComponentInChildren<Button>();
                if (shareButton != null)
                    shareButton.interactable = true;

                if (OnStartClick != null)
                {
                    sharer.OnClickHandler -= OnStartClick;
                    sharer.OnClickHandler += OnStartClick;
                }
            }
        }
        */
    }
}
