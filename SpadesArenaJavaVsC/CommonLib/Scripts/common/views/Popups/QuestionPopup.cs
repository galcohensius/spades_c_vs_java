﻿using common.controllers;
using System;

namespace common.views.popups
{

    public class QuestionPopup : PopupBase
    {
        [SerializeField] GameObject m_title = null;
        [SerializeField] TMP_Text m_title_text = null;
        [SerializeField] TMP_Text m_question_text = null;
        [SerializeField] TMP_Text m_yes_text = null;
        [SerializeField] TMP_Text m_no_text = null;

        Action yes_action;
        Action no_action;

        bool isClicking = false;

        PopupManager.AddMode m_addMode;

        private bool m_isOverPopups;



        public void SetTexts(string title, string question_text, string yes_text, string no_text)
        {
            if (title == null)
            {
                // hide the title
                m_title_text.gameObject.SetActive(false);
                m_title.SetActive(false);

                m_question_text.rectTransform.SetTop(60);
            }
            else
            {
                m_title_text.text = title;
            }

            m_question_text.text = question_text;
            m_yes_text.text = yes_text;
            m_no_text.text = no_text;
        }

        public void SetTitle(string title)
        {
            m_title_text.text = title;
        }

        public override void OnBackButtonClicked()
        {
            YesButtonClicked();
        }


        public virtual void YesButtonClicked()
        {
            if (!isClicking)
            {
                isClicking = true;
                yes_action?.Invoke();
                if (!m_isOverPopups)
                    m_manager.HidePopup();
            }
        }

        public void NoButtonClicked()
        {
            if (!isClicking)
            {
                isClicking = true;
                no_action?.Invoke();
                if (!m_isOverPopups)
                    m_manager.HidePopup();
            }
        }

        public Action Yes_action
        {
            get
            {
                return yes_action;
            }

            set
            {
                yes_action = value;
            }
        }

        public Action No_action
        {
            get
            {
                return no_action;
            }

            set
            {
                no_action = value;
            }
        }

        public bool IsOverPopups { get => m_isOverPopups; set => m_isOverPopups = value; }
    }
}
