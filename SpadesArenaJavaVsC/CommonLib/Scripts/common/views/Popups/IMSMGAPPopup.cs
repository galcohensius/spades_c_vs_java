﻿using common.comm;
using common.controllers;
using common.controllers.purchase;
using common.ims;
using common.ims.model;
using common.utils;
using System.Collections.Generic;

namespace common.views.popups
{
    public class IMSMGAPPopup : IMSPopup
    {
        #region Private Members
        Animator m_anim;
        ICashierController m_iCashierController;
        Button m_collectButton;
        bool m_buyWasHit = false;
        float m_cost;
        int m_sumOfRewards; // reward in mgap + cashier 
        #endregion

        #region SerializeField Members
        [SerializeField] int m_baseSortingOrder;
        #endregion

        bool firstShow = true;

        public void InitPopup(IMSInteractionZone iZone, ICashierController iCashierController)
        {
            InitPopup(iZone);
            m_iCashierController = iCashierController;
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);


            m_iCashierController.PurchaseCompleted -= PurchaseCompleted;
            m_iCashierController.PurchaseCanceled -= PurchaseCanceled;

            m_iCashierController.PurchaseCompleted += PurchaseCompleted;
            m_iCashierController.PurchaseCanceled += PurchaseCanceled;

            // If this is the first time this popup is shown, show a short message before the MGAP
            if (firstShow)
            {
                m_manager.ShowAutoMessagePopup("Purchase Completed! Hold on...", 1,PopupManager.AddMode.ShowAndKeep);
                firstShow = false;
            }
        }

        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            m_close_button.interactable = true;

            m_dynamicContentPO = CommManager.Instance.GetModelParser().ParsePODynamicContent(m_iZone.DynamicContent.Content[0]);

            m_anim = m_content.GetChild(0).GetComponent<Animator>();
            m_anim.SetTrigger("animTriggerAppearance");
            m_anim.SetTrigger("animTriggerBGMusic");

            Transform winUpTo = transform.FindDeepChild("winUpTo");
            if (winUpTo != null)
            {
                int lastPurchaseCoins = PurchaseController.Instance.LastGoodsList[PurchaseController.Instance.LastGoodsList.Count - 1].CoinsValue;
                winUpTo.GetComponent<TMP_Text>().text = "<sprite=1>" + FormatUtils.FormatBalance(m_dynamicContentPO.Goods.Coins[m_dynamicContentPO.Goods.Coins.Count - 1] + lastPurchaseCoins);
            }

            Transform spinPriceText = transform.FindDeepChild("Spin_Price_Text");
            if (spinPriceText != null)
                spinPriceText.GetComponent<TMP_Text>().text = "$" + m_dynamicContentPO.Cost;

            Button buy_button = transform.FindDeepChild("buyBtn").GetComponent<Button>();
            buy_button.onClick.AddListener(() => { BuyButtonClicked(); });
            buy_button.gameObject.AddComponent<ButtonsOverView>();

            m_collectButton = transform.FindDeepChild("collectBtn").GetComponent<Button>();
            m_collectButton.onClick.AddListener(() => { CollectButton(); });
            m_collectButton.gameObject.AddComponent<ButtonsOverView>();

            m_cost = m_dynamicContentPO.Cost;

            PrefabUtils.SetSortingOffset(transform, m_baseSortingOrder);
        }

        public void HandleAnimations(float factor)
        {
            m_anim.SetTrigger("Anim_" + factor);
            LoggerController.Instance.Log("MGAP Animation number: " + factor.ToString());
        }

        public void BuyButtonClicked()
        {
            m_buyWasHit = true;
            m_close_button.interactable = false;
            // call the purchase command
            IMSController.Instance.ExecuteDirectBuyAction(m_iZone, 0, false);
        }

        public void CollectButton()
        {
            // this should emit the comets and update the balance...
            m_collectButton.interactable = false;
            // closes the MGAP popup 1 second after the collect was hit
            CloseButtonClicked();
        }

        public override void OnBackButtonClicked()
        {
            if (m_close_button.gameObject.activeSelf && m_close_button.interactable)
                CloseButtonClicked();
        }

        public override void CloseButtonClicked()
        {
            // Show are you sure if not after buy and the origin is a click
            if (!m_buyWasHit)
            {
                // Chnage close button alpha to 0;
                ChangeCloseAlpha(0);
                int lastPurchaseCoins = PurchaseController.Instance.LastGoodsList[PurchaseController.Instance.LastGoodsList.Count - 1].CoinsValue;
                PopupManager.Instance.ShowQuestionOverPopups("Are you sure?",
                    "<size=70>Win up to <sprite=1>" + FormatUtils.FormatBalance(m_dynamicContentPO.Goods.Coins[m_dynamicContentPO.Goods.Coins.Count - 1] + lastPurchaseCoins) +
                    "!</size>\n<size=20>\n</size>For only $" + m_cost, "Stay", "Leave", () =>
                      {
                          // stay
                          ChangeCloseAlpha(1);
                          PopupManager.Instance.HideQuestionOverPopups();

                      }, () =>
                      {
                          // leave

                          PopupManager.Instance.HideQuestionOverPopups();
                          m_manager.HidePopup();

                          // show successful purchase of the previous purchase here:
                          List<IMSGoodsList> lastGoodsList = PurchaseController.Instance.LastGoodsList;
                          IMSGoodsList goodsList = new IMSGoodsList();
                          goodsList.Coins.Add(lastGoodsList[lastGoodsList.Count - 1].CoinsValue);

                          PopupManager.Instance.ShowPurchasePopup(PopupManager.AddMode.ShowAndRemove, 0, goodsList);
                      });
                return;
            }
            else
            {
                m_manager.HidePopup();
                // show successful purchase of the previous purchase here:
                IMSGoodsList goodsList = new IMSGoodsList();
                goodsList.Coins.Add(m_sumOfRewards);
                PopupManager.Instance.ShowPurchasePopup(PopupManager.AddMode.ShowAndRemove, 0, goodsList);
            }
        }

        private void ChangeCloseAlpha(float alpha)
        {
            Image closeImage = m_close_button.gameObject.GetComponent<Image>();
            Color c = closeImage.color;
            c.a = alpha;
            closeImage.color = c;
            m_close_button.interactable = (alpha == 1);
        }

        private void PurchaseCompleted(string packageId, IMSGoodsList goodsList)
        {
            List<IMSGoodsList> lastGoodsList = PurchaseController.Instance.LastGoodsList;
            int rewardAmount = goodsList.CoinsValue;
            float factor = 0;
            for (int i = 0; i < m_dynamicContentPO.Goods.Coins.Count; i++)
            {
                if (rewardAmount == m_dynamicContentPO.Goods.Coins[i])
                {
                    factor = m_dynamicContentPO.Goods.Factors[i];
                    break;
                }
            }

            // It is : lastGoodsList.Count - 2, beacuse there is a purchase also in the MGAP itself
            // So the last purchase we would like to show would be the prior to the MGAP purchase
            m_sumOfRewards = (int)(factor * lastGoodsList[lastGoodsList.Count - 2].CoinsValue);
            // handle the correct animation by the factor
            HandleAnimations(factor);

            Transform last_Purchase = transform.FindDeepChild("Last_Purchase");

            if (last_Purchase != null)
                last_Purchase.GetComponent<TMP_Text>().text = "<sprite=1>" + FormatUtils.FormatBalance(lastGoodsList[lastGoodsList.Count - 2].CoinsValue);

            Transform win = transform.FindDeepChild("Win");

            if (win != null)
                win.GetComponent<TMP_Text>().text = "<sprite=1>" + FormatUtils.FormatBalance(m_sumOfRewards);
        }

        private void PurchaseCanceled(string packageId)
        {
            LoggerController.Instance.Log("Purchase Canceled packageId: " + packageId);
            m_buyWasHit = false;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            m_iCashierController.PurchaseCompleted -= PurchaseCompleted;
            m_iCashierController.PurchaseCanceled -= PurchaseCanceled;
        }

        public override void Close()
        {
            // We want to override Close in order not to call HideAllParticles
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
            PlayCloseAnimation();
        }

        protected override void PlayOpenAnimation()
        {
            // Avoid scale
            UpdatePopupAlpha(0);
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "time", .3f, "onupdate", "UpdatePopupAlpha", "delay", .1f));
        }
    }
}