using common.ims.model;
using common.views;
using common.views.popups;
using System;

namespace common.controllers
{
    public delegate void PopupClosedDelegate();

    public abstract class PopupManager : MonoBehaviour
    {
        public static PopupManager Instance;

        public static Action PopupOpen;
        public static Action PopupClose;

        PopupBase m_popup_base;

        protected CanvasGroup m_canvas_group;

        [SerializeField] protected Image m_dark_bg;
        [SerializeField] protected GameObject m_loading_asset_popup = null;

        [SerializeField] protected GameObject m_message = null;
        [SerializeField] protected GameObject m_auto_message_popup = null;

        // TO REMOVE - OLD CASHIER METHOD, REPLACED BY IMSCashierPopup
        protected GameObject m_cashier_popup = null;
        protected GameObject m_custom_cashier_popup = null;

        [Space]
        [Header("IMS common")]
        [SerializeField] private GameObject ims_loader = null;

        public event Action<bool> OnPopupShown;

        private AddMode? last_add_mode;

        public enum AddMode
        {
            ShowAndRemove,
            ShowAndKeep,
            DontShowIfPopupShown
        }

        protected virtual void Awake()
        {
            //override Instance with each scene load
            Instance = this;

            m_canvas_group = GetComponent<CanvasGroup>();
        }

        protected virtual void Start()
        {
            // Register for bg event to remove all popups
            BackgroundController.Instance.OnSessionTimeout += RemoveAllPopups;
        }

        public void AddPopup(GameObject popup, AddMode? add_mode, PopupClosedDelegate close_delegate = null)
        {
            // If there is no other popup as child of popups - add and show
            if (transform.childCount == 0)
            {
                last_add_mode = null;
                AddPopupToTransform(popup, true);
                ShowPopup();
            }
            else
            {
                if (add_mode == AddMode.DontShowIfPopupShown)   //keeping the open popup if anything is there
                {
                    AddPopupToTransform(popup, true);
                }
                else if (add_mode == AddMode.ShowAndRemove || add_mode == AddMode.ShowAndKeep)
                {
                    last_add_mode = add_mode;
                    HidePopup();
                    AddPopupToTransform(popup, false);
                }
            }
            // Set the close delegate to the popup
            if (close_delegate != null)
            {
                popup.GetComponent<PopupBase>().Close_delegate = close_delegate;
            }

        }

        void HandleCanvasForOverOverlays(GameObject popup)
        {
            popup.GetComponent<RectTransform>().anchoredPosition = new Vector3(1, 1, 1);
            popup.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            popup.gameObject.AddComponent<Canvas>();
            popup.gameObject.GetComponent<CanvasGroup>().alpha = 1;
            popup.gameObject.GetComponent<Canvas>().overrideSorting = true;
            // should be the highest sorting order possible
            popup.gameObject.GetComponent<Canvas>().sortingOrder = 32153;
            popup.gameObject.AddComponent<GraphicRaycaster>();
        }

        private void AddPopupToTransform(GameObject popup_object, bool first)
        {
            popup_object.transform.SetParent(transform);

            if (first)
                popup_object.transform.SetAsFirstSibling();

            //popup_object.SetActive (false);
            popup_object.GetComponent<PopupBase>().UpdatePopupAlpha(0);

            popup_object.GetComponent<RectTransform>().localPosition = new Vector3(0, 10000f, 0);
            popup_object.GetComponent<RectTransform>().localScale = Vector3.one;
        }

        public MessagePopup ShowMessagePopup(string message, AddMode addMode = AddMode.ShowAndRemove, bool show_ok = true, PopupClosedDelegate close_delegate = null, string button_text = null, int ontop = 0)
        {
            GameObject popup = (GameObject)Instantiate(m_message);
            MessagePopup message_popup = popup.GetComponent<MessagePopup>();
            message_popup.Message = message;

            if (button_text != null)
                message_popup.SetButtonCaption(button_text);

            if (!show_ok)
                message_popup.DisableButton();

            if (ontop != 0)
                message_popup.SetDepth(ontop);

            AddPopup(popup, addMode, close_delegate);
            return message_popup;
        }

        public void ShowAutoMessagePopup(string message, float message_show_time, AddMode addMode=AddMode.ShowAndRemove, Action close_done=null, int override_depth = 32001)
        {
            GameObject popup = (GameObject)Instantiate(m_auto_message_popup);

            popup.GetComponent<AutoMessagePopup>().SetAllData(message, message_show_time, close_done, override_depth);

            AddPopup(popup, addMode, null);
        }

        private void ShowPopup()
        {
            GameObject popup = transform.GetChild(transform.childCount - 1).gameObject;

            LoggerController.Instance.Log("POPUP --- Showing Popup: " + popup.name);

            popup.SetActive(true);
            m_popup_base = popup.GetComponent<PopupBase>();

            GraphicRaycaster gr = popup.GetComponent<GraphicRaycaster>();

            if (gr != null)
                gr.enabled = true;

            m_dark_bg.enabled = true;
            m_canvas_group.blocksRaycasts = true;
            if (OnPopupShown != null)
                OnPopupShown(true);


            if (m_dark_bg.sprite == null) //default full black with opacity
                m_dark_bg.color = new Color(0, 0, 0, m_popup_base.GetBGDarkness());
            else //use image instead of black, image should have its original colors with opacity
                m_dark_bg.color = new Color(1, 1, 1, m_popup_base.GetBGDarkness());

            popup.transform.localPosition = m_popup_base.GetPosition();

            if (PopupOpen != null)
                PopupOpen.Invoke();

            m_popup_base.Show(this);

        }

        public void HidePopup()
        {
            if (OnPopupShown != null)
                OnPopupShown(false);
            if (m_popup_base != null)
            {
                LoggerController.Instance.Log("POPUP --- Closing Popup: " + m_popup_base.gameObject.name);
                m_popup_base.Close();
                PopupClose?.Invoke();
            }

            if (transform.childCount == 1)
            {
                m_dark_bg.enabled = false;
            }
        }

        public void RemoveAllPopups()
        {
            if (m_dark_bg == null)
                return;

            m_dark_bg.enabled = false;
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                GameObject popupObject = transform.GetChild(0).gameObject;
                popupObject.GetComponent<PopupBase>()?.Close();
                DestroyImmediate(popupObject);
            }
        }

        /// <summary>
        /// Removes all popups of a specific type.
        /// The type is the type of the associated PopupBase component script.
        /// </summary>
        public void RemovePopups(params Type[] excludeTypes)
        {
            for (int i = transform.childCount - 1; i >= 0; i--)
            {
                GameObject popupObj = transform.GetChild(i).gameObject;
                bool shouldDestroy = true;
                foreach (Type excludeType in excludeTypes)
                {
                    if (popupObj.GetComponent(excludeType) != null)
                        shouldDestroy = false;
                }

                if (shouldDestroy)
                {
                    popupObj.GetComponent<PopupBase>()?.Close();
                    DestroyImmediate(popupObj);
                }
            }
        }

        public void RemoveAllFuturePopups()
        {
            if (transform.childCount > 1)
            {
                for (int i = transform.childCount - 2; i >= 0; i--)
                {
                    DestroyImmediate(transform.GetChild(0).gameObject);
                }
            }
        }

        public abstract void ShowQuestionPopup(AddMode addMode, string title, string question, string yes_title, string no_title, Action yes_action, Action no_action = null);

        public abstract void ShowQuestionOverPopups(string title, string question, string yes_title, string no_title, Action yes_action, Action no_action = null);

        public abstract void HideQuestionOverPopups();

        public abstract void SetCashierPopupPrefab(GameObject cashier_gameobject);


        public bool IsCashierPopupInitialized
        {
            get { return m_cashier_popup != null; }
        }

        public abstract void ShowWaitingIndication(bool show, float autoHideDelay = 0);
        public abstract void ShowPurchasePopup(AddMode add_mode, int new_balance, IMSGoodsList goodsList, PopupClosedDelegate close_delegate = null);
        public abstract IMSCashierPopup ShowCashierIMSPopup(AddMode addMode);

        public GameObject Custom_cashier_popup
        {
            get
            {
                return m_custom_cashier_popup;
            }

            set
            {
                m_custom_cashier_popup = value;
            }
        }

        public void OnPopupClosed(PopupBase popup)
        {
            m_popup_base = null;
           
            if (last_add_mode == AddMode.ShowAndKeep)
            {
                popup.UpdatePopupAlpha(0);

                popup.GetComponent<RectTransform>().localPosition = new Vector3(0, 10000f, 0);

                GraphicRaycaster gr = popup.GetComponent<GraphicRaycaster>();

                if (gr != null)
                    gr.enabled = false;

            }
            else
            {
                if (popup != null)
                    DestroyImmediate(popup.gameObject);
            }

            if (transform.childCount == 0)
            {
                m_canvas_group.blocksRaycasts = false;
            }
            else
            {
                last_add_mode = null;
                ShowPopup();
            }

        }

        public CanvasGroup CanvasGroup
        {
            get
            {
                return m_canvas_group;
            }
        }

        public bool IsPopupShown
        {
            get
            {
                return m_popup_base != null;
            }
        }

        public GameObject Message
        {
            get
            {
                return m_message;
            }
        }

        public GameObject Loader { get => ims_loader; set => ims_loader = value; }

        /// <summary>
        /// emergency reference to open popup. should not be common use. better query with IsPopupShown and/or IsPopupOfTypeShown instead
        /// </summary>
        public PopupBase GetOpenPopup { get => m_popup_base; }

        /// <summary>
        /// Is a specific popup currently shown. doesn't care if said pupon is on the stack
        /// </summary>
        /// <typeparam name="T">a type of popup</typeparam>
        /// <returns></returns>
        public bool IsPopupOfTypeShown<T>() where T: PopupBase
        {
            if (!IsPopupShown)
                return false;

            return m_popup_base is T;
        }

    }
}
