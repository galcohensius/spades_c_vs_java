﻿using common.ims;

namespace common.views.popups
{
    public class IMSBundlePopup : IMSPopup
    {
        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            Transform btn = transform.FindDeepChild("actionBtn");
            if (btn != null)
            {
                btn.GetComponent<Button>().onClick.AddListener(() => { ButtonClicked(); });
                btn.gameObject.AddComponent<ButtonsOverView>();
            }
        }

        private void ButtonClicked()
        {
            IMSController.Instance.ExecuteAction(m_iZone);
        }
    }
}