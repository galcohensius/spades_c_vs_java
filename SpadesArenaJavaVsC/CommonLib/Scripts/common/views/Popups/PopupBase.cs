﻿using common.controllers;

namespace common.views
{
    [RequireComponent(typeof(CanvasGroup))]
    public abstract class PopupBase : MonoBehaviour
    {
        protected PopupManager m_manager;

        protected CanvasGroup m_canvas_group;

        protected PopupClosedDelegate m_close_delegate;

        protected virtual void Awake()
        {
            m_canvas_group = GetComponent<CanvasGroup>(); // Moved here from Show() so we can update the alpha to 0 even before Show() is invoked
        }

        public virtual void Start()
        {
            if (m_canvas_group == null)
                m_canvas_group = GetComponent<CanvasGroup>(); // Moved here from Show() so we can update the alpha to 0 even before Show() is invoked

            Transform close_button = transform.FindDeepChild("CloseButton");

            if (close_button != null)
                close_button.gameObject.GetComponent<Button>().onClick.AddListener(CloseButtonClicked);

        }

        public virtual void Show(PopupManager manager)
        {
            gameObject.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            AndroidBackButtonListener.Instance.BackButtonClicked += OnBackButtonClicked;
            m_manager = manager;

            this.gameObject.SetActive(true);

            PlayOpenAnimation();
        }

        protected virtual void PlayOpenAnimation()
        {
            UpdatePopupAlpha(0);

            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 0, "x", 0, "easeType", "easeOutBack", "time", 0f));
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 1, "x", 1, "delay", 0.05, "easeType", "easeOutBack", "time", .5f, "onComplete", "OnEnterAnimDone", "onCompleteTarget", this.gameObject));
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "time", .3f, "onupdate", "UpdatePopupAlpha", "delay", .1f));
        }

        private void OnDisable()
        {
            // Just to make sure
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
        }

        public void UpdatePopupAlpha(float value)
        {
            if (m_canvas_group != null)
                m_canvas_group.alpha = value;
        }

        public virtual void OnBackButtonClicked()
        {
            // Do nothing on default
        }
        /// <summary>
        /// event from the popup manager to indicate aniamtion done 
        /// </summary>
        protected virtual void OnEnterAnimDone()
        {
        }

        public virtual void Close()
        {
            HideAllParticles();
            AndroidBackButtonListener.Instance.BackButtonClicked -= OnBackButtonClicked;
            PlayCloseAnimation();
        }

        protected virtual void HideAllParticles()
        {
            LoggerController.Instance.Log("HideAllParticles on popup close");
            // disable animator beacuse some animations use particles, so it's not possible to disable them
            Animator[] animators = GetComponentsInChildren<Animator>();
            foreach (var anim in animators)
            {
                anim.enabled = false;
            }

            // find all particles and disable them
            ParticleSystem[] allChildren = GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem child in allChildren)
                child.gameObject.SetActive(false);
        }

        protected virtual void PlayCloseAnimation()
        {
            iTween.MoveBy(this.gameObject, iTween.Hash("y", -10, "easeType", "easeOutCubic", "time", .25f, "onComplete", "OnExitAnimDone", "onCompleteTarget", this.gameObject));
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 1, "to", 0, "time", .25f, "onupdate", "UpdatePopupAlpha", "easeType", "easeOutCubic"));
        }

        /// <summary>
        /// Raises the exit animation done event.
        /// </summary>
        protected virtual void OnExitAnimDone()
        {
            m_close_delegate?.Invoke();

            // Notifty the popup manager that the exit animation is finished
            m_manager?.OnPopupClosed(this);

        }

        public virtual void CloseButtonClicked()
        {

        }

        public virtual Vector3 GetPosition()
        {
            return new Vector3(0, 0, 0);
        }

        public virtual float GetBGDarkness()
        {
            return 0.8f;
        }


        public PopupClosedDelegate Close_delegate
        {
            get
            {
                return m_close_delegate;
            }
            set
            {
                m_close_delegate = value;
            }
        }

        public virtual bool IsOverlayPopup()
        {
            return false;
        }
    }
}