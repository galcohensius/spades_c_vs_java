﻿using System.Collections.Generic;

namespace common.views.popups
{
    public class InfoBubbleContainer : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] List<InfoBubble> m_infoBubbles;
        [SerializeField] GameObject m_blackingOut;
        bool m_isActive = false;
        Placement m_placement;

        public enum Placement
        {
            RightUp = 0,
            RightDown,
            LeftUp,
            LeftDown,
            None
        }
        #endregion

        public void SetData(string description, Placement placement = Placement.None)
        {
            m_placement = placement;
            m_isActive = false;

            foreach (InfoBubble item in m_infoBubbles)
            {
                item.SetData(description);
                item.gameObject.SetActive(false);
            }
        }

        public void SetPlacement(Placement placement, bool saveValue = true)
        {
            m_placement = placement;
            m_isActive = false;

            foreach (InfoBubble item in m_infoBubbles)
                item.gameObject.SetActive(false);

            if (placement != Placement.None)
            {
                m_infoBubbles[(int)m_placement].gameObject.SetActive(true);
                m_isActive = true;
            }
            else
            {
                m_blackingOut.gameObject.SetActive(false);
            }
        }

        public void Hide()
        {
            m_isActive = false;

            foreach (InfoBubble item in m_infoBubbles)
                item.gameObject.SetActive(false);

            m_blackingOut.gameObject.SetActive(false);
        }

        public void Button_clicked()
        {
            m_isActive = !m_isActive;
            if (!m_isActive)
            {
                Hide();
                return;
            }

            if (this.transform.position.y < 0)
            {
                if (this.transform.position.x < 0)
                {
                    SetPlacement(InfoBubbleContainer.Placement.RightUp);
                }
                else
                {
                    SetPlacement(InfoBubbleContainer.Placement.LeftUp);
                }
            }
            else
            {
                if (this.transform.position.x < 0)
                {
                    SetPlacement(InfoBubbleContainer.Placement.RightDown);
                }
                else
                {
                    SetPlacement(InfoBubbleContainer.Placement.LeftDown);
                }
            }

            m_blackingOut.gameObject.SetActive(true);
        }

        public List<InfoBubble> InfoBubbles { get => m_infoBubbles; set => m_infoBubbles = value; }
    }
}
