﻿using common.ims.model;
using common.mes;
using common.utils;
using System;

namespace common.views.popups
{
    public class PromotionTemplate_SingleLine2 : PromotionTemplate
    {

        public override void PopulateFields(MESMaterial material)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                string coins = material.dynamicContent[i]["fQy"];
                string price = material.dynamicContent[i]["cot"];
                string formated_coins = FormatUtils.FormatBalance(Convert.ToInt32(coins));
                m_coins[i].text = string.Format("Get <size=80><sprite=1><b>{0}</b></size>", formated_coins);
                m_price[i].text = string.Format("Only ${0}", price);
            }
        }

        public override void PopulateFields(IMSDynamicContent dynamicContent)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                string coins = dynamicContent.Content[i]["gds"];
                string price = dynamicContent.Content[i]["cot"];
                string formated_coins = FormatUtils.FormatBalance(Convert.ToInt32(coins));
                m_coins[i].text = string.Format("Get <size=80><sprite=1><b>{0}</b></size>", formated_coins);
                m_price[i].text = string.Format("Only ${0}", price);
            }
        }
    }

}

