﻿using common.ims.model;
using common.mes;
using common.utils;
using System;

namespace common.views.popups
{
    /// <summary>
    /// Coins - with sprite
    /// Price - Only $9.99
    /// </summary
    public class PromotionTemplate_TwoLines1 : PromotionTemplate
    {

        public override void PopulateFields(MESMaterial material)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                string coins = material.dynamicContent[i]["fQy"];
                string price = material.dynamicContent[i]["cot"];
                string percent = material.dynamicContent[i]["pMe"];

                string formated_coins = FormatUtils.FormatBalance(Convert.ToInt32(coins));
                m_coins[i].text = string.Format("<sprite=1> <b>{0}</b>", formated_coins);
                m_price[i].text = string.Format("Only ${0}", price);
                m_percent[i].text = percent + "%";
            }
        }

        public override void PopulateFields(IMSDynamicContent dynamicContent)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                string coins = dynamicContent.Content[i]["gds"];
                string price = dynamicContent.Content[i]["cot"];
                string percent = dynamicContent.Content[i]["pMe"];

                string formated_coins = FormatUtils.FormatBalance(Convert.ToInt32(coins));
                m_coins[i].text = string.Format("<sprite=1> <b>{0}</b>", formated_coins);
                m_price[i].text = string.Format("Only ${0}", price);
                m_percent[i].text = percent + "%";

                // Voucher handler
                bool containsVoucher = false;
                JSONArray gltArray = dynamicContent.Content[i]["gLt"].AsArray;

                for (int k = 0; k < gltArray.Count; k++)
                {
                    string type = dynamicContent.Content[i]["gLt"][k]["gTe"];
                    if (type == "V")
                    {
                        m_voucherValue[i].text = dynamicContent.Content[i]["gLt"][k]["vae"];
                        m_voucherQuantity[i].text = dynamicContent.Content[i]["gLt"][k]["quy"];
                        m_voucherContainer[i].SetActive(true);
                        containsVoucher = true;
                    }

                    if (!containsVoucher && i < m_voucherContainer.Count)
                        m_voucherContainer[i].SetActive(false);
                }
            }
        }
    }
}

