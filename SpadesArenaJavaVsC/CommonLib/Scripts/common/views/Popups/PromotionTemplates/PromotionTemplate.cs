﻿using common.mes;
using System;
using common.ims.model;
using System.Collections.Generic;

namespace common.views.popups
{
    public abstract class PromotionTemplate : MonoBehaviour
    {
        [SerializeField] protected TMP_Text[] m_percent = null;
        [SerializeField] protected TMP_Text[] m_coins = null;
        [SerializeField] protected TMP_Text[] m_price = null;

        [SerializeField] Button[] m_buttons = null;

        [SerializeField] TMP_Text m_timer_text = null;

        [SerializeField] protected List<GameObject> m_voucherContainer = null;
        [SerializeField] protected List<TMP_Text> m_voucherValue = null;
        [SerializeField] protected List<TMP_Text> m_voucherQuantity = null;

        public abstract void PopulateFields(MESMaterial material);

        public abstract void PopulateFields(IMSDynamicContent dynamicContent);



        public void PopulateTemplate(MESMaterial material, Action<int> button_action, Action default_button_action)
        {
            // Buttons
            // Use pointer down instead of click to handle external URLs with WebGL builds
            for (int i = 0; i < m_buttons.Length; i++)
            {
                Button btn = m_buttons[i];

                if (!btn.interactable)
                    continue;

                int local_i = i; // needed for the lambda

                EventTrigger triggerDown = btn.gameObject.AddComponent<EventTrigger>();
                var pointerDown = new EventTrigger.Entry();
                pointerDown.eventID = EventTriggerType.PointerDown;
                pointerDown.callback.AddListener((e) =>
                {
                    if (button_action != null)
                    {
                        button_action(local_i);
                    }
                    else
                    {
                        default_button_action();
                    }
                });
                triggerDown.triggers.Add(pointerDown);

            }

            //texts part
            if (material.dynamicContent != null && material.dynamicContent.AsArray.Count > 0)
            {
                PopulateFields(material);
            }
            else
            {
                for (int i = 0; i < m_price.Length; i++)
                    m_price[i].gameObject.SetActive(false);

                for (int i = 0; i < m_percent.Length; i++)
                    m_percent[i].gameObject.SetActive(false);

                for (int i = 0; i < m_coins.Length; i++)
                    m_coins[i].gameObject.SetActive(false);

            }

            gameObject.SetActive(true);

        }

        public void PopulateTemplate(IMSInteractionZone iZone, Action<int> button_action, Action default_button_action)
        {
            // Buttons
           
            for (int i = 0; i < m_buttons.Length; i++)
            {
                Button btn = m_buttons[i];

                int local_i = i; // needed for the lambda

                // TEMPLATES ARE USED ONLY FOR PURCHASES SO NO NEED FOR POINTER DOWN - RAN (3/12/2019)
                // POINTER DOWN DOES NOT PREVENT DOUBLE CLICKS
                //
                // Use pointer down instead of click to handle external URLs with WebGL builds
                //EventTrigger triggerDown = btn.gameObject.AddComponent<EventTrigger>();
                //var pointerDown = new EventTrigger.Entry();
                //pointerDown.eventID = EventTriggerType.PointerDown;
                //pointerDown.callback.AddListener((e) =>
                btn.onClick.AddListener(() =>
                {
                    if (button_action != null)
                    {
                        button_action(local_i);
                    }
                    else
                    {
                        default_button_action();
                    }
                    //pointerDown.callback.RemoveAllListeners();
                });
                //triggerDown.triggers.Add(pointerDown);

            }

            //texts part
            if (iZone.DynamicContent != null && iZone.DynamicContent.Content.Count > 0)
            {
                PopulateFields(iZone.DynamicContent);
            }
            else
            {
                for (int i = 0; i < m_price.Length; i++)
                    m_price[i].gameObject.SetActive(false);

                for (int i = 0; i < m_percent.Length; i++)
                    m_percent[i].gameObject.SetActive(false);

                for (int i = 0; i < m_coins.Length; i++)
                    m_coins[i].gameObject.SetActive(false);

            }

            gameObject.SetActive(true);

        }

        public TMP_Text GetTimer()
        {
            return m_timer_text;
        }
    }
}