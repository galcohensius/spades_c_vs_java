﻿using common.ims.model;
using common.mes;
using common.utils;
using System;
using System.Collections.Generic;

namespace common.views.popups
{
    /// <summary>
    /// Coins - with sprite
    /// Price - Only $9.99
    /// </summary
    public class PromotionTemplate_Offers : PromotionTemplate
    {
        [SerializeField] protected TMP_Text[] m_percent2 = null;
        [SerializeField] protected TMP_Text[] m_coins2 = null;
        [SerializeField] protected TMP_Text[] m_price2 = null;

        [SerializeField] List<GameObject> m_withVouchers;
        [SerializeField] List<GameObject> m_withoutVouchers;

        public override void PopulateFields(MESMaterial material)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                string coins = material.dynamicContent[i]["fQy"];
                string price = material.dynamicContent[i]["cot"];
                string percent = material.dynamicContent[i]["pMe"];

                string formated_coins = FormatUtils.FormatBalance(Convert.ToInt32(coins));
                m_coins[i].text = string.Format("<sprite=1> <b>{0}</b>", formated_coins);
                if (m_price.Length == 1)
                    m_price[i].text = string.Format("Get $ {0}", price);
                else
                    m_price[i].text = string.Format("$ {0}", price);
                m_percent[i].text = percent + "%";
            }
        }

        public override void PopulateFields(IMSDynamicContent dynamicContent)
        {


            for (int i = 0; i < m_price.Length; i++)
            {
                string coins = dynamicContent.Content[i]["gds"];
                string price = dynamicContent.Content[i]["cot"];
                string percent = dynamicContent.Content[i]["pMe"];

                string formated_coins = FormatUtils.FormatBalance(Convert.ToInt32(coins));
                if (m_coins.Length == 1)
                    m_coins[i].text = string.Format("Get <sprite=1> <b>{0}</b>", formated_coins);
                else
                    m_coins[i].text = string.Format("<sprite=1> <b>{0}</b>", formated_coins);

                m_price[i].text = string.Format("$ {0}", price);

                m_percent[i].text = percent + "%";

                m_coins2[i].text = m_coins[i].text;
                m_price2[i].text = m_price[i].text;
                m_percent2[i].text = m_percent[i].text;

                // Voucher handler
                bool containsVoucher = false;
                JSONArray gltArray = dynamicContent.Content[i]["gLt"].AsArray;

                m_withVouchers[i].SetActive(false);
                m_withoutVouchers[i].SetActive(false);

                for (int k = 0; k < gltArray.Count; k++)
                {
                    string type = dynamicContent.Content[i]["gLt"][k]["gTe"];
                    if (type == "V")
                    {
                        m_voucherValue[i].text = FormatUtils.FormatBuyIn(dynamicContent.Content[i]["gLt"][k]["vae"]);
                        m_voucherQuantity[i].text = dynamicContent.Content[i]["gLt"][k]["quy"];
                        m_voucherContainer[i].SetActive(true);
                        containsVoucher = true;
                        //
                        m_withoutVouchers[i].SetActive(false);
                        m_withVouchers[i].SetActive(true);
                    }

                    if (!containsVoucher)
                    {
                        m_voucherContainer[i].SetActive(false);
                        //
                        m_withoutVouchers[i].SetActive(true);
                        m_withVouchers[i].SetActive(false);

                    }
                }
            }
        }
    }
}

