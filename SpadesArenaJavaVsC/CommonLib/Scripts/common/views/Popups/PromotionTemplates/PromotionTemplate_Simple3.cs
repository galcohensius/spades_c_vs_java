﻿using common.ims.model;
using common.mes;
using common.utils;

namespace common.views.popups
{
    /// <summary>
    /// Coins - with sprite
    /// Price - $9.99
    /// </summary
    public class PromotionTemplate_Simple3 : PromotionTemplate
    {

        public override void PopulateFields(MESMaterial material)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                m_percent[i].text = material.dynamicContent[i]["pMe"] + "%";
                m_coins[i].text = "<sprite=1> "+FormatUtils.FormatBalance(material.dynamicContent[i]["fQy"]);
                m_price[i].text = "$" + material.dynamicContent[i]["cot"];
            }
        }

        public override void PopulateFields(IMSDynamicContent dynamicContent)
        {
            for (int i = 0; i < m_price.Length; i++)
            {
                m_percent[i].text = dynamicContent.Content[i]["pMe"] + "%";
                m_coins[i].text = "<sprite=1> " + FormatUtils.FormatBalance(dynamicContent.Content[i]["gds"]);
                m_price[i].text = "$" + dynamicContent.Content[i]["cot"];
            }
        }
    }

}

