﻿using System.Collections;
using System;
using common.views;

namespace common.controllers
{
    /// <summary>
    /// Toasting not necessarily at the bottom of the screen. ToastBase can be extended to have more features. Right now a toast
    /// contains a single text field only. The Toasting mechanism is a light-weight popup system.
    /// </summary>
    public class ToastManager : MonoBehaviour
    {

        public static float OPEN_TIME = 0.35f;
        public static float SHOW_TIME = 1.5f;
        public static float CLOSE_TIME = 0.2f;
        public static float MIN_TIME_BETWEEN_TOASTS = 2f;
        public static float DEFAULT_CHAR_WIDTH = 26.2f;
        public static float DEFAULT_MARGINS_WIDTH = 107f;

        [SerializeField]
        protected GameObject toastPrefab;

        public static ToastManager Instance;

        public static Action ToastOpen;
        public static Action ToastDenied;
        public static Action ToastClose;

        ToastBase activeToast;
        public bool IsToastShown { get => activeToast != null;}
        public bool CanShowToast { get; private set; } = true;
        
        protected CanvasGroup m_canvas_group;
        
        protected virtual void Awake()
        {
            //override Instance with each scene load
            Instance = this;
            
            m_canvas_group = GetComponent<CanvasGroup>();
        }

        protected virtual void Start()
        {
            // Register for bg event to remove all toasts
            BackgroundController.Instance.OnGoingToBackground += RemoveToast;
        }

        //no fade-in animation show it would show instantly also closes other open toast if any
        public void ShowInstantToast(string message, float time = -1)
        {
            if (IsToastShown)
            {
                HideToast();
                StopAllCoroutines();
                CanShowToast = true;
            }

            GameObject toast = (GameObject)Instantiate(toastPrefab, this.transform);

            activeToast = toast.GetComponent<ToastBase>();
            activeToast.AutoSetup(message);

            activeToast.ShowImmediately(this);
            ToastOpen?.Invoke();
            StartCoroutine(ToastActiveTime(time <= 0 ? SHOW_TIME : time));

        }

        public void ShowOneLinerToast(string message, float time = -1)
        {
            if (IsToastShown || !CanShowToast)
            {
                ToastDenied?.Invoke();
                return;
            };

            GameObject toast = (GameObject)Instantiate(toastPrefab, this.transform);

            activeToast = toast.GetComponent<ToastBase>();
            activeToast.AutoSetup(message);

            activeToast.Show(this);
            ToastOpen?.Invoke();
            StartCoroutine(ToastActiveTime(time <= 0 ? SHOW_TIME : time));
        }

        public void ShowMultiLineToast(string message, float width, Vector3? position = null, float heigth = 0, float time = -1)
        {
            if (IsToastShown || !CanShowToast)
            {
                ToastDenied?.Invoke();
                return;
            };

            GameObject toast = (GameObject)Instantiate(toastPrefab, this.transform);

            activeToast = toast.GetComponent<ToastBase>();
            activeToast.Setup(message, width, position, heigth);

            activeToast.Show(this);
            ToastOpen?.Invoke();
            StartCoroutine(ToastActiveTime(time <= 0 ? SHOW_TIME : time));
        }

        IEnumerator ToastActiveTime(float showTime)
        {
            yield return new WaitForSeconds(showTime);
            activeToast.Close();
        }

        public virtual void RemoveToast()
        {
            StopAllCoroutines();
            if (activeToast != null)
                DestroyImmediate(activeToast.gameObject);
        }

        public void HideToast()
        {
            if (activeToast != null)
            {
                LoggerController.Instance.Log("TOAST --- Closing Toast: " + activeToast.Text);
                activeToast.Close();               
            }
        }
        
        public void OnToastClosed()
        {
            DestroyImmediate(activeToast.gameObject);
            ToastClose?.Invoke();
            StartCoroutine(DenyNewToastTime());
        }

        IEnumerator DenyNewToastTime()
        {
            CanShowToast = false;
            yield return new WaitForSeconds(MIN_TIME_BETWEEN_TOASTS);
            CanShowToast = true;
        }

        public CanvasGroup CanvasGroup
        {
            get
            {
                return m_canvas_group;
            }
        }

        private void OnDestroy()
        {
            BackgroundController.Instance.OnGoingToBackground -= RemoveToast;
        }


#if UNITY_EDITOR
        //private void Update()
        //{
        //    if (Input.GetKeyDown(KeyCode.Q))
        //        ShowMultiLineToast("Time's running out! the game will end and you'll lose your bet", 1150, null, 0, 4);
        //}
#endif
    }
}