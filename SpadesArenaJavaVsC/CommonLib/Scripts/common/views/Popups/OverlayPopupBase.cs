﻿using common.controllers;

namespace common.views
{
    /// <summary>
    /// base class for the new overlays. the new overlays are new type of popups without frames possibly different common behaviour.
    /// also comes with auto-setup for click to close
    /// </summary>
    public class OverlayPopupBase : PopupBase, IPointerClickHandler
    {

        public bool clickToClose = true;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);
        }

        protected override void PlayOpenAnimation()
        {
            //no fancy animations, these are handled by the popup's animator
            UpdatePopupAlpha(1);
        }

        public override bool IsOverlayPopup()
        {
            return true;
        }

        public void ButtonClicked()
        {
            m_manager.HidePopup();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (clickToClose)
                ButtonClicked();
        }
    }
}