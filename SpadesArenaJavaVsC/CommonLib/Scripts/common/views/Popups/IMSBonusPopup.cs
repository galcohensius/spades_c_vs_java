﻿using common.ims;
using common.comm;

namespace common.views.popups
{
    public class IMSBonusPopup : IMSPopup
    {
        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            m_dynamicContentPO = CommManager.Instance.GetModelParser().ParsePODynamicContent(m_iZone.DynamicContent.Content[0]);

            Transform btn = transform.FindDeepChild("actionBtn");
            if (btn != null)
            {
                btn.GetComponent<Button>().onClick.AddListener(() => { ButtonClicked(); });
                btn.gameObject.AddComponent<ButtonsOverView>();
            }

            Transform panel = transform.FindDeepChild("Panels");
            HandlePanels(panel.gameObject);
        }

        protected void ButtonClicked()
        {
            IMSController.Instance.ExecuteAction(m_iZone);
        }
    }
}
