﻿namespace common.views.popups
{
    public class InfoBubble : MonoBehaviour
    {
        #region SerializeField Members
        [SerializeField] private TMP_Text m_description;
        [SerializeField] RectTransform m_bg_rect;
        [SerializeField] bool m_strechUp;
        #endregion

        public void SetData(string description)
        {
            m_description.text = description;
            /*
             * Looks better without resisizing. The font is auto size true. (RAN 1/3/2020)
             * 
            float actual_height = m_description.preferredHeight;

            float bottom = -actual_height + 40;
            float top = actual_height - 60;

            if (m_strechUp)
                m_bg_rect.offsetMax = new Vector2(m_bg_rect.offsetMax.x, top);
            else
                m_bg_rect.offsetMin = new Vector2(m_bg_rect.offsetMin.x, bottom);

            LoggerController.Instance.Log("InfoBubble, name: " + this.name + ", actual height: " + actual_height);
            */
        }
    }
}
