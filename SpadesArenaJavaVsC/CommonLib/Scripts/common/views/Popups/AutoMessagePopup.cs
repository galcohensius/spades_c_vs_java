﻿using System;
using System.Collections;
using common.controllers;

namespace common.views.popups
{

    public class AutoMessagePopup : PopupBase
    {


        [SerializeField] TMP_Text m_message_text = null;

        Action m_popup_closed_done;
        float m_time_to_close;

        Canvas m_canvas;

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            StartCoroutine(StartAndClose());
        }

        public void SetAllData(string message, float time_to_close, Action close_done, int depth)
        {
            m_popup_closed_done = close_done;
            m_time_to_close = time_to_close;

            m_canvas = GetComponent<Canvas>();
            m_canvas.sortingOrder = depth;

            m_message_text.text = message;

        }

        IEnumerator StartAndClose()
        {
            yield return new WaitForSeconds(m_time_to_close);

            m_popup_closed_done?.Invoke();
            ClosePopup();
        }


            public void ClosePopup()
        {
            m_manager.HidePopup();
        }

        

    }
}