﻿using common.controllers;
using System;

namespace common.views.popups
{

    public class LoadingAssetPopup : PopupBase
    {
        private float Loader_len = 650f;

        [SerializeField] RectTransform progress_image = null;
        [SerializeField] TMP_Text m_progress_text;
        string m_image_url;

        public Action OnPopupShown;
        private Action cancel_delegate;

        public Action<float> GetProgressFunction()
        {
            return UpdateProgress;
        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);
            UpdateProgress(0);
        }

        protected override void OnEnterAnimDone()
        {
            base.OnEnterAnimDone();
            OnPopupShown?.Invoke();
        }

        public void UpdateProgress(float progress_value)
        {
            if (this == null)
                return;

            progress_image.localPosition = new Vector3(-Loader_len +  progress_value * Loader_len,0,0);
            m_progress_text.text = string.Format("%{0:F2}", progress_value * 100);

        }

        public void Button_Cancel_Clicked()
        {
            RemoteAssetManager.Instance.CancelGetImage(m_image_url);
            Cancel_delegate?.Invoke();
            m_manager.HidePopup();
        }

        public void ClosePopup()
        {
            m_manager.HidePopup();
        }

        public string Image_url
        {
            get
            {
                return m_image_url;
            }

            set
            {
                m_image_url = value;
            }
        }

        public Action Cancel_delegate { get => cancel_delegate; set => cancel_delegate = value; }
    }
}