﻿using common.utils;
using common.controllers;
using common.models;

namespace common.views.popups
{
    public class IMSOpenCashierPopup : IMSPopup
    {
        Cashier m_cashier;

        public void InitPopup(Cashier cashier)
        {
            m_cashier = cashier;
        }

        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            // Should be btnAction
            Transform btn = transform.FindDeepChild("actionBtn");
            if (btn != null)
            {
                btn.GetComponent<Button>().onClick.AddListener(() => { ButtonClicked(); });
                btn.gameObject.AddComponent<ButtonsOverView>();
            }

            int maxValue = 0;

            foreach (CashierItem item in m_cashier.Cashier_packages)
            {
                if (item.Bonus > maxValue)
                    maxValue = item.Bonus;
            }

            Transform percentageMore = transform.FindDeepChild("BonusText");
            if (percentageMore != null)
                percentageMore.GetComponent<TMP_Text>().text = FormatUtils.FormatBalance(maxValue) + "%";
        }

        public void ButtonClicked()
        {
            PopupManager.Instance.ShowCashierIMSPopup(PopupManager.AddMode.ShowAndRemove);
        }
    }
}
