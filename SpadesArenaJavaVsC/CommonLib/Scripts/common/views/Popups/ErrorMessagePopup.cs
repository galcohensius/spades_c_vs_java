﻿namespace common.views.popups
{

    public class ErrorMessagePopup : PopupBase {


		[SerializeField] Text 			m_error_text=null;
		[SerializeField] Text 			m_stack_text=null;

		public string Error_message
		{
			set
			{
				m_error_text.text = value;
			}
		}

		public string Stack_error_message
		{
			set
			{
				m_stack_text.text = value;
			}
		}

		public void OKClicked()
		{
            gameObject.SetActive(false);
		}

	}
}