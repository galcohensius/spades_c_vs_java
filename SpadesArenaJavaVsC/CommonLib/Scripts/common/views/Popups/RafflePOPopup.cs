﻿using common.comm;
using common.controllers;
using common.ims;
using common.ims.model;
using common.utils;
using System;
using System.Collections.Generic;

namespace common.views.popups
{
    public class RafflePOPopup : IMSPopup
    {
        #region Private Members
        float m_cost;
        Transform m_rewardAmountText;
        Image m_close_button_image;
        Animator m_anim;
        Transform m_cometTrigger;
        bool m_buyWasHit = false;
        int m_rewardAmount;
        ICashierController m_iCashierController;
        float m_version = 0f; // 0 is before we started using the version counter
        int m_btnIndex = -1; // selected button
        public Action<int> OnPrizeAwarded;
        #endregion


        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            m_dynamicContentPO = CommManager.Instance.GetModelParser().ParsePODynamicContent(m_iZone.DynamicContent.Content[0]);

            m_anim = m_content.GetChild(0).GetComponent<Animator>();
            m_anim.SetTrigger("animTriggerAppearance");
            m_anim.SetTrigger("animTriggerBGMusic");
            Transform version = transform.FindDeepChild("Version");
            if (version != null)
                m_version = float.Parse(transform.FindDeepChild("Version").GetChild(0).name);

            Transform spinPriceText = transform.FindDeepChild("Spin_Price_Text");
            AssetBundleUtils.SetTextForTransform(spinPriceText, "$" + m_dynamicContentPO.Cost);
            m_cost = m_dynamicContentPO.Cost;
            m_rewardAmountText = transform.FindDeepChild("Reward_Amount_Text");

            List<Button> buy_buttons = new List<Button>();

            if (m_version == 0 || m_version == 1)
            {
                buy_buttons.Add(transform.FindDeepChild("buyBtn").GetComponent<Button>());
                buy_buttons[0].onClick.AddListener(() => { BuyButtonClicked(); });
                buy_buttons[0].gameObject.AddComponent<ButtonsOverView>();
            }
            else if (m_version == 2)
            {
                int index = 0;
                int k = 1;

                // loop while button exist
                while (transform.FindDeepChild("buyBtn_" + k) != null)
                {
                    buy_buttons.Add(transform.FindDeepChild("buyBtn_" + k).GetComponent<Button>());
                    int n = k;
                    buy_buttons[index].onClick.AddListener(() => { BuyButtonClicked(n); });
                    buy_buttons[index].gameObject.AddComponent<ButtonsOverView>();
                    index++;
                    k++;
                }
            }

            m_close_button_image = transform.FindDeepChild("closeBtn").GetComponent<Image>();

            m_cometTrigger = transform.FindDeepChild("cometTrigger");
            if (m_cometTrigger != null)
            {
                m_cometTrigger.gameObject.AddComponent<CometTrigger>();
                m_cometTrigger.gameObject.GetComponent<CometTrigger>().Init(this);
            }

            for (int i = 0; i < m_dynamicContentPO.Goods.Coins.Count; i++)
            {
                string name = null;
                if (m_version == 0 || m_version == 1)
                    name = "stopBox_" + (i + 1);
                else if (m_version == 2)
                    name = "index_" + (i + 1);

                List<Transform> stopBox = transform.FindDeepChildren(name);
                foreach (Transform item in stopBox)
                {
                    AssetBundleUtils.SetTextForTransform(item, FormatUtils.FormatBalance(m_dynamicContentPO.Goods.Coins[i]));
                }
            }
        }

        public void HandleAnimations(int animNumber)
        {
            // call the wanted animations with parameters
            m_anim.SetTrigger("animTrigger_" + animNumber);
            LoggerController.Instance.Log("RafflePO Animation number: " + animNumber);
        }

        public void HandleAnimationsMulti(int index)
        {
            int totalAnims = 1;
            while (m_anim.HasParameterOfType("btn" + m_btnIndex + "_index" + index + "_anim" + totalAnims, AnimatorControllerParameterType.Trigger))
                totalAnims++;

            int animNumber = (int)UnityEngine.Random.Range(1f, totalAnims);
            m_anim.SetTrigger("btn" + m_btnIndex + "_index" + index + "_anim" + animNumber);

            LoggerController.Instance.Log("RafflePO Animation number: " + "btn" + m_btnIndex + "_index" + index + "_anim" + animNumber);
        }

        // version 0/1 solo button
        public void BuyButtonClicked()
        {
            m_iCashierController.PurchaseCompleted += PurchaseCompleted;
            m_buyWasHit = true;
            // call the purchase command
            IMSController.Instance.ExecuteDirectBuyAction(m_iZone, 0, false);
        }

        // version 2 ; n number of buttons
        public void BuyButtonClicked(int index)
        {
            m_btnIndex = index; // save the index of the button that was clicked
            m_iCashierController.PurchaseCompleted += PurchaseCompleted;
            m_buyWasHit = true;
            // call the purchase command
            IMSController.Instance.ExecuteDirectBuyAction(m_iZone, 0, false);
        }

        public override void OnBackButtonClicked()
        {
            if (m_close_button.gameObject.activeSelf)
                CloseButtonClicked();
        }

        public override void CloseButtonClicked()
        {
            // Show are you sure if not after buy and the origin is a click
            if (!m_buyWasHit && !string.IsNullOrEmpty(m_iZone.OriginMetadata))
            {

                // Chnage close button alpha to 0;
                ChangeCloseAlpha(0);

                PopupManager.Instance.ShowQuestionOverPopups("Are you sure?",
                    "<size=70>Win up to <sprite=1>" + FormatUtils.FormatBalance(m_dynamicContentPO.Goods.Coins[m_dynamicContentPO.Goods.Coins.Count - 1]) +
                    "!</size>\n<size=20>\n</size>For only $" + m_cost, "Stay", "Leave", () =>
                      {
                          ChangeCloseAlpha(1);
                          PopupManager.Instance.HideQuestionOverPopups();
                      }, () =>
                      {
                          PopupManager.Instance.HideQuestionOverPopups();
                          m_manager.HidePopup();
                      });
                return;
            }
            else
            {
                m_manager.HidePopup();
            }

        }

        private void ChangeCloseAlpha(float alpha)
        {
            Color c = m_close_button_image.color;
            c.a = alpha;
            m_close_button_image.color = c;
        }

        private void PurchaseCompleted(string packageId, IMSGoodsList goodsList)
        {
            m_rewardAmount = goodsList.CoinsValue;

            for (int i = 0; i < m_dynamicContentPO.Goods.Coins.Count; i++)
            {
                // should read data from the IMS
                if (m_dynamicContentPO.Goods.Coins[i] == m_rewardAmount)
                {
                    if (m_version == 0 || m_version == 1)
                        HandleAnimations(i + 1);
                    else if (m_version == 2)
                        HandleAnimationsMulti(i + 1);
                    break;
                }
            }
            AssetBundleUtils.SetTextForTransform(m_rewardAmountText, FormatUtils.FormatBalance(m_rewardAmount));
        }

        private void PurchaseCanceled(string packageId)
        {
        }

        public void InitPopup(IMSInteractionZone iZone, ICashierController iCashierController)
        {
            InitPopup(iZone);
            m_iCashierController = iCashierController;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            m_iCashierController.PurchaseCompleted -= PurchaseCompleted;
        }

        public GameObject CometTriggerStartPoint { get => m_cometTrigger?.gameObject; }


        private class CometTrigger : MonoBehaviour
        {
            RafflePOPopup m_parent;

            public void Init(RafflePOPopup parent)
            {
                m_parent = parent;
            }

            private void OnEnable()
            {
                m_parent.OnPrizeAwarded?.Invoke(m_parent.m_rewardAmount);
            }
        }
    }

}