using common.ims.model;
using common.mes;
using common.utils;
using System;

namespace common.views.popups
{
    public class BonusTemplate : MonoBehaviour
    {
        [SerializeField] Button[] m_buttons = null;
        [SerializeField] TMP_Text m_coins = null;

        public void PopulateTemplate(MESMaterial material, Action button_action)
        {
            if (material.dynamicContent != null && material.dynamicContent.AsArray.Count > 0)
                m_coins.text = "<sprite=1> " + FormatUtils.FormatBalance(material.dynamicContent[0]["fQy"]);

            //buttons part
            if (button_action != null)
            {
                 for (int i = 0; i < m_buttons.Length; i++)
                 {
                     m_buttons[i].onClick.AddListener(delegate { button_action(); });
                 }
            }

            gameObject.SetActive(true);
        }

        public void PopulateTemplate(IMSInteractionZone material, Action button_action)
        {
            
        }
    }
}
