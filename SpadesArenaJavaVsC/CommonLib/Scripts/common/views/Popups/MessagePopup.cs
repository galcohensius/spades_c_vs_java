﻿namespace common.views.popups
{

    public class MessagePopup : PopupBase {
		

		[SerializeField] TMP_Text 			m_message_text=null;
		[SerializeField]Button				m_button=null;
		bool								m_ok_clicked=false;
		Canvas								m_canvas;



		public string Message
		{
			set
			{
				
				m_message_text.text = value;
			}
		}

		public override void OnBackButtonClicked()
		{
			OKClicked();
		}

		public void SetDepth(int depth)
		{
			m_canvas = GetComponent<Canvas> ();
			m_canvas.sortingOrder = depth;
		}

		public void SetButtonCaption(string new_text)
		{
			if(new_text!=null)
				m_button.transform.GetChild (0).GetComponent<TMP_Text> ().text = new_text;
		}

		public void DisableButton()
		{
			m_button.gameObject.SetActive (false);
		}

		public void OKClicked()
		{
			m_ok_clicked = true;
			m_manager.HidePopup ();
		}

		public bool Ok_clicked {
			get {
				return m_ok_clicked;
			}
		}
	}
}