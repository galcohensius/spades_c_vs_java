using System;
using System.Collections;
using common.mes;
using common.utils;
using System.Collections.Generic;
using common.controllers;
using common.ims.model;
using common.ims;

namespace common.views.popups
{
    public class IMSTemplatePopup : PopupBase
    {
        Dictionary<string, Transform>[] m_all_offer_templates = new Dictionary<string, Transform>[4];

        Dictionary<string, Transform> m_templates1Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_templates2Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_templates3Offer = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_templates4Offer = new Dictionary<string, Transform>();

        Dictionary<string, Transform> m_bonustemplates = new Dictionary<string, Transform>();
        Dictionary<string, Transform> m_buyItemTemplates = new Dictionary<string, Transform>();


        [SerializeField] Transform m_template_1_offer_parent = null;
        [SerializeField] Transform m_template_2_offer_parent = null;
        [SerializeField] Transform m_template_3_offer_parent = null;
        [SerializeField] Transform m_template_4_offer_parent = null;

        [SerializeField] Transform m_bonus_template_parent = null;
        [SerializeField] Transform m_buy_item_template_parent = null;

        [SerializeField] GameObject popupImage = null;
        [SerializeField] GameObject closeImage = null;
        [SerializeField] GameObject noThanksBtn = null;
        [SerializeField] GameObject noTemplateButton = null;

        [SerializeField] Transform m_assetBundleContent;


        Button m_close_button;

        IEnumerator m_timer_routine = null;
        TMP_Text m_timer_text = null;

        string m_template_Id = "";
        Transform m_curTemplate;

        float m_template_scale_factor = 1.42857f; // Scale factor for Spades: 70% -> 100%

        private IMSInteractionZone m_iZone;

        Coroutine m_timeoutCoroutine;

        GameObject m_loaded_asset = null;

        bool m_shown;
        bool m_cantLoad=false;
        Texture2D m_rawTexture;


        public void InitPopup(IMSInteractionZone iZone)
        {

            InitTemplates();

            m_iZone = iZone;

            if (m_iZone.TemplateId == null)
                m_template_Id = "S1";
            else
                m_template_Id = m_iZone.TemplateId;

            if (m_template_Id.StartsWith("B"))
            {
                m_template_scale_factor = 1.5f; // Scale factor for BG
            }

            closeImage.SetActive(false);

            // Start a timeout check
            float popupLoadTimeout = LocalDataController.Instance.GetSettingAsInt("PromoPopupLoadTimeout", 0);
            if (popupLoadTimeout > 0)
                m_timeoutCoroutine = StartCoroutine(TimeoutCheck(popupLoadTimeout));


            if (m_iZone.IsAssetBundle)
                RemoteAssetManager.Instance.LoadAsset(RemoteAssetManager.Instance.CdnBase, m_iZone.Source, Hash128.Compute("1"), OnAssetBundleLoaded);
            else
                RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, m_iZone.Source, null, OnGraphicLoaded, null, false);

            LoggerController.Instance.Log("Loading promo popup with source image: " + m_iZone.Source);


            // Instantiating the loader
            GameObject go = Instantiate(PopupManager.Instance.Loader, transform);
            // making sure the loader is under the content
            go.transform.SetAsFirstSibling();
        }

        private void InitTemplates()
        {
            foreach (Transform template in m_template_1_offer_parent)
            {
                m_templates1Offer.Add(template.name, template);
            }

            foreach (Transform template in m_template_2_offer_parent)
            {
                m_templates2Offer.Add(template.name, template);
            }

            foreach (Transform template in m_template_3_offer_parent)
            {
                m_templates3Offer.Add(template.name, template);
            }

            foreach (Transform template in m_template_4_offer_parent)
            {
                m_templates4Offer.Add(template.name, template);
            }

            m_all_offer_templates[0] = m_templates1Offer;
            m_all_offer_templates[1] = m_templates2Offer;
            m_all_offer_templates[2] = m_templates3Offer;
            m_all_offer_templates[3] = m_templates4Offer;

            foreach (Transform template in m_bonus_template_parent)
            {
                m_bonustemplates.Add(template.name, template);
            }

            foreach (Transform template in m_buy_item_template_parent)
            {
                m_buyItemTemplates.Add(template.name, template);
            }


        }

        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            m_shown = true;

            // Create the sprite here if the texture arrived before showing the popup
            if (m_rawTexture != null)
            {
                Image popup_image = popupImage.GetComponent<Image>();
                popup_image.sprite = Sprite.Create(m_rawTexture, new Rect(0, 0, m_rawTexture.width, m_rawTexture.height), new Vector2(0.5f, 0.5f));
                popup_image.SetNativeSize();

            if (m_iZone.IsAssetBundle)
                RemoteAssetManager.Instance.LoadAsset(RemoteAssetManager.Instance.CdnBase, m_iZone.Source, Hash128.Compute("1"), OnAssetBundleLoaded);
            else
                RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, m_iZone.Source, null, OnGraphicLoaded, null, false);

            } else if (m_cantLoad)
            {
                manager.HidePopup();
            }

        }


        private void FadeInAnimation()
        {
            gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.01f, 0.01f, 0.01f);
            this.gameObject.SetActive(true);
            UpdatePopupAlpha(0);
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 0, "x", 0, "easeType", iTween.EaseType.linear, "time", 0f));
            iTween.ScaleTo(this.gameObject, iTween.Hash("y", 1, "x", 1, "delay", 0.15, "easeType", iTween.EaseType.easeOutBack, "time", .4f, "onComplete", "OnEnterAnimDone", "onCompleteTarget", this.gameObject));
            iTween.ValueTo(this.gameObject, iTween.Hash("from", 0, "to", 1, "time", .4f, "onupdate", "UpdatePopupAlpha", "delay", .15f));
        }

        /*
         * SEEMS UNNEEDED
        public override void Close()
        {
            base.Close();

            m_curTemplate?.gameObject.SetActive(false);
            m_spades_holder.SetActive(true);

        }
        */

        private IEnumerator TimeoutCheck(float popupLoadTimeout)
        {
            yield return new WaitForSecondsRealtime(popupLoadTimeout);

            m_manager.HidePopup();
        }

        public void OnClick()
        {
            // Prevent double click
            IMSController.Instance.ExecuteAction(m_iZone);
        }

        public void OnCloseClick()
        {
            if (closeImage!=null && closeImage.activeSelf ||
                m_close_button!=null && m_close_button.isActiveAndEnabled ||
                noThanksBtn!=null && noThanksBtn.activeSelf)
                m_manager.HidePopup();
        }

        private void OnGraphicLoaded(Sprite popupSprite, Texture2D rawTexture, object userData)
        {

            if (this == null)
                return;


            if (m_timeoutCoroutine != null)
                StopCoroutine(m_timeoutCoroutine);

            if (rawTexture == null)
            {
                m_manager?.HidePopup();
                m_cantLoad = true;
                return;
            }

            m_rawTexture = rawTexture;


            
            Image popup_image = popupImage.GetComponent<Image>();
            popup_image.GetComponent<RectTransform>().localScale = new Vector2(m_template_scale_factor, m_template_scale_factor);

            if (m_shown)
            {
                // Create the sprite here if the texture arrived after showing the popup
                popup_image.sprite = Sprite.Create(m_rawTexture, new Rect(0, 0, m_rawTexture.width, m_rawTexture.height), new Vector2(0.5f, 0.5f));
                popup_image.SetNativeSize();

                IMSController.Instance.FireImpressionTracking(m_iZone);
            }


            closeImage.GetComponent<RectTransform>().localPosition = new Vector3(rawTexture.width * m_template_scale_factor / 2 - 46f,
                                                                                  rawTexture.height * m_template_scale_factor / 2 - 46f);


            if (m_iZone.Action.Id == MESBase.ACTION_CLAIM_DYNAMIC_BONUS)
            {
                PopulateBonusTemplate();

            }
            else if (m_iZone.Action.Id == MESBase.ACTION_DIRECT_BUY)
            {
                PopulatePOTemplate();
            }
            else if (m_iZone.Action.Id == IMSController.ACTION_BUY_ITEM)
            {
                PopulateItemTemplate();
            }
            else
            {
                noTemplateButton.SetActive(true);
            }

            // Close button
            if (m_iZone.CloseBtnType == "X")
            {
                closeImage.SetActive(true);
                noThanksBtn.SetActive(false);
            }
            else if (m_iZone.CloseBtnType == "W" || m_iZone.CloseBtnType == "N")
            {
                closeImage.SetActive(false);
                noThanksBtn.SetActive(false);
            }
            else if (m_iZone.CloseBtnType == "L")
            {
                // link
                closeImage.SetActive(false);
                noThanksBtn.SetActive(true);
            }
            else
            {
                closeImage.SetActive(true);
                noThanksBtn.SetActive(false);
            }


        }

        private void OnAssetBundleLoaded(GameObject loaded_obj)
        {
            if (loaded_obj == null)
                m_manager.HidePopup();

            m_loaded_asset = Instantiate(loaded_obj);
            m_loaded_asset.transform.SetParent(m_assetBundleContent, false);

            m_loaded_asset.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
            m_loaded_asset.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

            // Rescale from 70%->100% since the bundles are created with a reduced size
            m_assetBundleContent.GetComponent<RectTransform>().localScale = new Vector3(m_template_scale_factor, m_template_scale_factor, 1);



            Transform closeButtonTransform = transform.FindDeepChild("closeBtn");

            if (closeButtonTransform != null)
            {
                m_close_button = closeButtonTransform.GetComponent<Button>();
                m_close_button.gameObject.AddComponent<ButtonsOverView>();
                m_close_button.onClick.AddListener(() => {
                    OnCloseClick();
                });
            }


            // FOR NOW - SUPPORT ONLY A SINGLE ACTION IZONES
            Transform actionButtonTransform = transform.FindDeepChild("actionBtn");
            if (actionButtonTransform != null)
            {
                Button actionButton = actionButtonTransform.GetComponent<Button>();
                actionButton.gameObject.AddComponent<ButtonsOverView>();
                actionButton.onClick.AddListener(() => {
                    OnClick();
                });
            }

        }

     


        private void PopulateBonusTemplate()
        {
            if (m_bonustemplates.TryGetValue(m_template_Id, out m_curTemplate))
            {
                BonusTemplate bt = m_curTemplate.GetComponent<BonusTemplate>();
                bt.PopulateTemplate(m_iZone, OnClick);
            }
        }

        private void PopulatePOTemplate()
        {
            List<IMSActionValue> apm = m_iZone.Action.Values;

            int num_offers = 1;

            if (apm != null && apm.Count > 0)
                num_offers = apm.Count;


            Action<int> button_action = null;

            if (m_iZone.Action.Id == MESBase.ACTION_DIRECT_BUY)
                button_action = OnDirectBuy;

            if (!m_all_offer_templates[num_offers - 1].TryGetValue(m_template_Id, out m_curTemplate))
            {
                LoggerController.Instance.Log("Missing data");
            }
            else
            {
                PromotionTemplate pt = m_curTemplate.GetComponent<PromotionTemplate>();
                m_timer_text = pt.GetTimer();

                if (apm == null)
                {
                    //single offer
                    pt.PopulateTemplate(m_iZone, null, OnClick);
                }
                else
                {
                    pt.PopulateTemplate(m_iZone, button_action, OnClick);
                }
            }
            ManageTimer();
        }

        private void PopulateItemTemplate()
        {

            if (m_buyItemTemplates.TryGetValue("S30", out m_curTemplate))
            {
                BuyItemTemplate bt = m_curTemplate.GetComponent<BuyItemTemplate>();
                bt.PopulateTemplate(m_iZone, OnClick);
            }
        }


        private void OnDirectBuy(int index)
        {
            IMSController.Instance.ExecuteAction(m_iZone, index);
        }


        private void ManageTimer()
        {
            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (m_iZone.ViewLimit == null)
            {
                // No view limit - no timer
                m_timer_text?.gameObject.SetActive(false);
                return;
            }

            if (m_iZone.ViewLimit.Expires > 0)
            {

                m_timer_routine = POTimer((int)m_iZone.ViewLimit.Expires);
                StartCoroutine(m_timer_routine);
            }
            else
            {
                m_timer_text?.gameObject.SetActive(false);
            }

            if (!m_iZone.ViewLimit.IsVIsible)
            {
                m_timer_text?.gameObject.SetActive(false);
            }

        }



        IEnumerator POTimer(int expired)
        {
            m_timer_text.gameObject.SetActive(true);
            while (true)
            {
                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);

                m_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            m_manager.HidePopup();
        }

        private void OnDestroy()
        {
            if (popupImage != null)
            {
                if (popupImage.GetComponent<Image>().sprite != null)
                    Destroy(popupImage.GetComponent<Image>().sprite.texture);
            }

            if (m_loaded_asset != null)
            {
                RemoteAssetManager.Instance.UnloadBundle(m_iZone.Source);
            }
        }

        public override void OnBackButtonClicked()
        {
            OnCloseClick();
        }
    }
}

