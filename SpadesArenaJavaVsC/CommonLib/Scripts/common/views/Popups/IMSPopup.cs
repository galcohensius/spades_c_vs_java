﻿using common.controllers;
using common.ims;
using common.ims.model;
using common.models;
using common.utils;

namespace common.views.popups
{
    /// <summary>
    /// Super class for all bundle IMS popups 
    /// </summary>
    public abstract class IMSPopup : PopupBase
    {
        protected IMSPODynamicContent m_dynamicContentPO;
        protected Transform m_selectedPanel = null;

        #region Private Members
        protected Button m_close_button;
        protected IMSInteractionZone m_iZone;

        protected GameObject m_loadedObj = null; // the loaded prefab - comes from the bundle
        protected GameObject m_prefabInstance = null; // The prefab instance
        protected bool m_isShown = false;
        protected bool m_loadingFailed = false;
        #endregion

        #region SerializeField Members
        [SerializeField] protected Transform m_content;
        [SerializeField] protected GameObject m_voucher_prefab;
        GameObject m_loader;
        #endregion


        public override void Show(PopupManager manager)
        {
            base.Show(manager);

            if (m_loadingFailed)
            {

                LoggerController.Instance.Log("Loading failed for IMSPopup " + m_iZone?.Id + ". Hiding popup.");
                // There wad a problem loading
                m_manager.HidePopup();
                return;
            }
            if (m_prefabInstance == null)
            {
                // Prefab is not created yet

                if (m_loadedObj != null)
                {
                    // Create the prefab
                    InstantiatePrefab();
                }
                else
                {
                    // Prefab not loaded yet - Instantiating the loader
                    m_loader = Instantiate(PopupManager.Instance.Loader, transform);
                    // making sure the loader is under the content
                    m_loader.transform.SetAsFirstSibling();
                }
            }

            m_isShown = true;

        }

        public void InitPopup(IMSInteractionZone iZone)
        {
            m_iZone = iZone;
            RemoteAssetManager.Instance.LoadAsset(RemoteAssetManager.Instance.CdnBase, m_iZone.Source, Hash128.Compute("1"), OnAssetBundleLoaded);

            gameObject.name += " - "+iZone.Id;
        }

        protected void OnAssetBundleLoaded(GameObject loaded_obj)
        {
            if (loaded_obj == null)
            {
                // Loading of bundle failed
                m_loadingFailed = true; 
                if (m_isShown)
                    m_manager?.HidePopup();

                if(m_iZone!=null)
                    LoggerController.Instance.SendLogDataToServer("IMSPopup_load", "Cannot load popup for iZone " + m_iZone?.Id);

                return;
            }

            m_loadedObj = loaded_obj;

            if (m_isShown)
                InstantiatePrefab();
        }

        protected virtual void InstantiatePrefab()
        {
            if (m_loadedObj == null)
                return;

            if (m_loader != null)
                m_loader.gameObject.SetActive(false);


            m_prefabInstance = Instantiate(m_loadedObj, m_content);

            // Scale the prefab
            // Get the scale factor from a GameObject named "ScaleFactor" which should be included in the prefab
            float scaleFactor = 1;
            Transform scaleFactorObj = transform.FindDeepChild("ScaleFactor");
            if (scaleFactorObj != null)
            {
                scaleFactor = 1 / scaleFactorObj.localScale.x;
            }

            m_prefabInstance.GetComponent<RectTransform>().localScale = new Vector3(scaleFactor, scaleFactor, 1);

            m_prefabInstance.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);

            Transform close = transform.FindDeepChild("closeBtn");
            if (close != null)
            {
                m_close_button = close.GetComponent<Button>();
                m_close_button.onClick.AddListener(() => { CloseButtonClicked(); });
                m_close_button.gameObject.AddComponent<ButtonsOverView>();
            }

            ChangeMusicOutputToCorrectMixer();

            ParticleUtil.RefreshShadersandMaterials(m_prefabInstance);

            if (m_iZone != null) // In IMSCashierPopup -> no iZone..
                IMSController.Instance.FireImpressionTracking(m_iZone);


        }


        protected void HandlePanels(GameObject parentGO)
        {
            // Hide all children
            foreach (Transform child in parentGO.transform)
                child.gameObject.SetActive(false);

            bool containsCoins = m_dynamicContentPO.Goods.Coins.Count != 0;
            bool containsVouchers = m_dynamicContentPO.Goods.Vouchers.Count != 0;
            bool containsItems = m_dynamicContentPO.Goods.InventoryItems.Count != 0;

            if (containsCoins && containsItems && containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Coins_Voucher_Item");
                m_selectedPanel?.gameObject.SetActive(true);
            }
            else if (containsCoins && containsItems)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Coins_Item");
                m_selectedPanel?.gameObject.SetActive(true);
            }
            else if (containsCoins && containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Coins_Voucher");
                m_selectedPanel?.gameObject.SetActive(true);
            }
            else if (containsItems && containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Voucher_Item");
                m_selectedPanel?.gameObject.SetActive(true);
            }
            else if (containsCoins)
            {
                Transform coins = parentGO.transform.FindDeepChild("CoinsPanel");
                if (coins == null)
                    m_selectedPanel = parentGO.transform.FindDeepChild("Coins");
                else
                    m_selectedPanel = coins;
                m_selectedPanel?.gameObject.SetActive(true);
            }
            else if (containsVouchers)
            {
                Transform voucher = parentGO.transform.FindDeepChild("VoucherPanel");
                if (voucher == null)
                    m_selectedPanel = parentGO.transform.FindDeepChild("Voucher");
                else
                    m_selectedPanel = voucher;
                m_selectedPanel?.gameObject.SetActive(true);
            }
            else if (containsItems)
            {
                Transform item = parentGO.transform.FindDeepChild("ItemPanel");
                if (item == null)
                    m_selectedPanel = parentGO.transform.FindDeepChild("Item");
                else
                    m_selectedPanel = item;
                m_selectedPanel?.gameObject.SetActive(true);
            }

            if (m_selectedPanel == null)
                return;

            HandleCoins(m_selectedPanel);
            HandleVouchers(m_selectedPanel);
        }

        protected void HandleCoins(Transform panel)
        {
            if (m_dynamicContentPO.Goods.Coins.Count == 0)
                return;

            int coinsValue = m_dynamicContentPO.Goods.CoinsValue;

            Transform coinsText = panel.FindDeepChild("CoinsAmount");

            if (coinsText != null)
                coinsText.GetComponent<TMP_Text>().text = "<sprite=1> " + FormatUtils.FormatBalance(coinsValue);
        }

        protected void HandleVouchers(Transform panel)
        {
            if (m_dynamicContentPO.Goods.Vouchers.Count == 0)
                return;

            Transform dummyVoucher = panel.FindDeepChild("DummyVoucher");
            dummyVoucher.gameObject.SetActive(false);

            Transform voucherPlaceholder = panel.FindDeepChild("VoucherPlaceholder");

            GameObject voucherGO = Instantiate(m_voucher_prefab, voucherPlaceholder);
            voucherGO.transform.SetPositionAndRotation(voucherPlaceholder.position, voucherPlaceholder.rotation);

            VouchersGroup voucherGroup = m_dynamicContentPO.Goods.Vouchers[0];
            voucherGO.GetComponent<VoucherView>().SetData(voucherGroup);
        }


        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }

        protected virtual void OnDestroy()
        {
            // first condition is for IMS Cashier (because there's no izone data)
            if (m_iZone != null)
                if (m_iZone.IsAssetBundle)
                {
                    LoggerController.Instance.Log("Unloading bundle for iZone " + m_iZone.Id);
                    RemoteAssetManager.Instance.UnloadBundle(m_iZone.Source);
                }
        }

        public override void OnBackButtonClicked()
        {
            if (m_close_button == null || m_close_button.isActiveAndEnabled)
                CloseButtonClicked();
        }

        private void ChangeMusicOutputToCorrectMixer()
        {
            Transform sounds = transform.FindDeepChild("Sounds");
            if (sounds != null)
            {
                foreach (Transform item in sounds)
                {
                    if (item.name == "BGSound")
                        item.gameObject.GetComponent<AudioSource>().volume = StateController.Instance.GetMusicSetting();

                    else
                        item.gameObject.GetComponent<AudioSource>().volume = StateController.Instance.GetSFXSetting();
                }
            }
        }

        protected override void HideAllParticles()
        {
            // DO NOTHING
        }
            
    }
}