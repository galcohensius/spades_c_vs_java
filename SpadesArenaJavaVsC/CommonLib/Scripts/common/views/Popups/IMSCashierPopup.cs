using common.utils;
using System.Collections.Generic;
using common.models;
using common.controllers.purchase;
using common.controllers;
using common.ims.model;
using System;
using System.Collections;
using common.ims;

namespace common.views.popups
{
    public class IMSCashierPopup : IMSPopup
    {
        ICashierController m_cashierController;
        Cashier m_cashier;
        CommonUser m_user;

        public void InitPopup(ICashierController cashierController, Cashier cashier, CommonUser user)
        {
            m_user = user;
            m_cashierController = cashierController;
            m_cashier = cashier;

            if (string.IsNullOrEmpty(cashier.Custom_bundle_path))
                // No cashier bundle - use default
                OnAssetBundleLoaded(m_cashierController.DefaultCashierPrefab);
            else
                RemoteAssetManager.Instance.LoadAsset(RemoteAssetManager.Instance.CdnBase, cashier.Custom_bundle_path, Hash128.Compute("1"), OnAssetBundleLoaded);
        }

        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            // CloseButton - handeling old version of the cashier close button
            Transform close = transform.FindDeepChild("CloseButton");
            if (close != null)
            {
                m_close_button = close.GetComponent<Button>();
                m_close_button.onClick.AddListener(() => { CloseButtonClicked(); });
                m_close_button.gameObject.AddComponent<ButtonsOverView>();
            }

            List<CashierItem> cashierPackages = m_cashier.Cashier_packages;
            Transform container = transform.FindDeepChild("ItemsHolder");

            int index = 1;

            if (container == null)
                return;

            AssetBundleUtils.SetTextForTransform(transform.FindDeepChild("LevelGroupText"), "LEVELS " + GetLevelText());

            // go through all the cashier packages
            foreach (CashierItem item in cashierPackages)
            {
                // check what panel should be activated
                Transform itemTrns = container.FindDeepChild("CashierItem" + index);

                m_selectedPanel = itemTrns;
                Transform panel = null;

                // if this item contains MGAP, show an indication icon
                Transform mgapIndication = itemTrns.transform.FindDeepChild("MGAPIndicator");
                if (mgapIndication != null)
                    mgapIndication.gameObject.SetActive(item.GoodsList.MGAP);

                HandleCost(itemTrns, item.Price);

                panel = itemTrns.FindDeepChild("Panels");
                // check what panel should be activated
                if (panel != null)
                    HandleCashierPanels(panel.gameObject, item.GoodsList);
                else
                    HandleOldPanels(itemTrns.gameObject, item.GoodsList);

                List<Transform> btn = itemTrns.FindDeepChildren("Button");

                // handeling old versions (<1.26)
                if (btn.Count == 0)
                {
                    btn = itemTrns.FindDeepChildren("BG");
                }

                for (int i = 0; i < btn.Count; i++)
                {
                    btn[i].gameObject.AddComponent<ButtonsOverView>();

                    Button buyButton = btn[i].GetComponent<Button>();
                    buyButton.onClick.AddListener(() =>
                    {
                        if (m_close_button!=null)
                            m_close_button.enabled = false; // Disable closing the cashier while waiting for the purchase
                        StartCoroutine(AutoEnableCloseButton());
                        IMSController.Instance.FireClickTracking(m_cashier,item);

                        m_cashierController.BuyPackage(item.Id, new PurchaseTrackingData
                        {
                            imsMetaData = m_cashier.ImsMetadata,
                            imsActionValue = item.ImsActionValue
                        });

                    });
                }

                HandlePercentageMore(itemTrns, item.Bonus);
                HandleCoins(item.GoodsList.CoinsValue);
                HandleVouchers(item.GoodsList.Vouchers);
                InsertWasValue(item);
                index++;
            }

            // Cashier popup has a special impression firing
            IMSController.Instance.FireImpressionTracking(m_cashier);
        }

       

        void HandleCashierPanels(GameObject parentGO, IMSGoodsList goodsList)
        {
            // Hide all children
            foreach (Transform child in parentGO.transform)
                child.gameObject.SetActive(false);

            bool containsCoins = goodsList.Coins.Count != 0;
            bool containsVouchers = goodsList.Vouchers.Count != 0;
            bool containsItems = goodsList.InventoryItems.Count != 0;

            if (containsCoins && containsItems && containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Coins_Voucher_Item");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsCoins && containsItems)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Coins_Item");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsCoins && containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Coins_Voucher");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsItems && containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Voucher_Item");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsCoins)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("CoinsPanel");
                // handeling old version
                if (m_selectedPanel == null)
                    m_selectedPanel = parentGO.transform.FindDeepChild("Coins");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("VoucherPanel");
                // handeling old version
                if (m_selectedPanel == null)
                    m_selectedPanel = parentGO.transform.FindDeepChild("Voucher");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsItems)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("Item");
                m_selectedPanel.gameObject.SetActive(true);
            }

            if (m_selectedPanel == null)
                return;
        }

        // handeling old versions (=1.26)
        void HandleOldPanels(GameObject parentGO, IMSGoodsList goodsList)
        {
            // Hide all panels
            foreach (Transform child in parentGO.transform)
                if (child.name.Contains("Panel"))
                    child.gameObject.SetActive(false);

            bool containsVouchers = goodsList.Vouchers.Count != 0;
            bool containsCoins = goodsList.Coins.Count != 0;

            if (containsVouchers)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("VoucherPanel");
                m_selectedPanel.gameObject.SetActive(true);
            }
            else if (containsCoins)
            {
                m_selectedPanel = parentGO.transform.FindDeepChild("CoinsPanel");
                m_selectedPanel.gameObject.SetActive(true);
            }
        }

        void HandlePercentageMore(Transform item, int percentage)
        {
            Transform bonusBG = item.FindDeepChild("BonusBG");
            if (percentage == 0)
            {
                bonusBG.gameObject.SetActive(false);
                return;
            }
            List<Transform> bonusText = item.FindDeepChildren("BonusText");
            if (bonusText.Count > 0)
            {
                // this for loop was made to handle new versions of the cashier
                for (int i = 0; i < bonusText.Count; i++)
                {
                    AssetBundleUtils.SetTextForTransform(bonusText[i], FormatUtils.FormatPrice(percentage) + "%");
                }
                return;
            }
            else
            {
                bonusText = m_selectedPanel.FindDeepChildren("PercentageText");
                if (bonusText.Count > 0)
                {
                    // this for loop was made to handle old versions of the cashier (<1.26)
                    for (int i = 0; i < bonusText.Count; i++)
                    {
                        AssetBundleUtils.SetTextForTransform(bonusText[i], FormatUtils.FormatPrice(percentage) + "%");
                    }
                    return;
                }
            }
        }

        void HandleCost(Transform item, string price)
        {
            List<Transform> cost = item.FindDeepChildren("PriceText");
            if (cost.Count > 0)
            {
                // this for loop was made to handle new versions of the cashier(>1.26)
                for (int i = 0; i < cost.Count; i++)
                {
                    AssetBundleUtils.SetTextForTransform(cost[i], "$" + price);
                }
                return;
            }
            else
            {
                cost = m_selectedPanel.FindDeepChildren("CostText");
                if (cost.Count > 0)
                {
                    // this for loop was made to handle old versions of the cashier
                    for (int i = 0; i < cost.Count; i++)
                    {
                        AssetBundleUtils.SetTextForTransform(cost[i], "$" + price);
                    }
                    return;
                }
            }
        }

        void HandleCoins(int coinsAmount)
        {
            if (coinsAmount == 0)
                return;

            List<Transform> coinsText = m_selectedPanel.FindDeepChildren("CoinsAmount");

            // new versions support this (>1.26)
            if (coinsText.Count > 0)
            {
                for (int i = 0; i < coinsText.Count; i++)
                {
                    AssetBundleUtils.SetTextForTransform(coinsText[i], FormatUtils.FormatBalance(coinsAmount));
                }
                return;
            }
            // old
            coinsText = m_selectedPanel.FindDeepChildren("CoinsText");
            if (coinsText.Count > 0)
            {
                // this for loop was made to handle old versions of the cashier
                for (int i = 0; i < coinsText.Count; i++)
                {
                    AssetBundleUtils.SetTextForTransform(coinsText[i], FormatUtils.FormatBalance(coinsAmount));
                }
                return;
            }
        }

        void HandleVouchers(List<VouchersGroup> vouchersGroup)
        {

            if (vouchersGroup.Count == 0)
                return;

            // Method B
            // instantiating the prefab under the place holder
            Transform methodB = transform.FindDeepChild("MethodB");

            if (methodB != null)
            {
                Transform dummyVoucher = m_selectedPanel.FindDeepChild("DummyVoucher");
                dummyVoucher.gameObject.SetActive(false);


                Transform voucherPlaceholder = m_selectedPanel.FindDeepChild("VoucherPlaceholder");

                GameObject voucherGO = Instantiate(m_voucher_prefab, voucherPlaceholder);

                VouchersGroup voucherGroup = vouchersGroup[0];
                voucherGO.GetComponent<VoucherView>().SetData(voucherGroup);
                return;
            }

            if (vouchersGroup.Count == 0)
            {
                Transform voucherItem = m_selectedPanel.FindDeepChild("VoucherItem");
                if (voucherItem != null)
                    voucherItem.gameObject.SetActive(false);

                return;
            }

            // Method A - should be deleted soon
            // using ready prefab (just injecting data)
            Transform value = m_selectedPanel.FindDeepChild("VoucherValueText");
            if (value != null)
                AssetBundleUtils.SetTextForTransform(value, FormatUtils.FormatBuyIn(vouchersGroup[0].VouchersValue));

            Transform quantity = m_selectedPanel.FindDeepChild("VoucherQuantityText");
            if (quantity != null)
                AssetBundleUtils.SetTextForTransform(quantity, FormatUtils.FormatBuyIn(vouchersGroup[0].Count));
        }

        void InsertWasValue(CashierItem cashierItem)
        {
            Transform price_was_text = m_selectedPanel.FindDeepChild("WasText");
            Transform wasLine = m_selectedPanel.FindDeepChild("WasLine");

            if (price_was_text != null && wasLine != null)
            {
                float ratio = (cashierItem.Bonus + 100) / 100f;

                if (ratio <= 1)
                {
                    wasLine.gameObject.SetActive(false);
                    return;
                }
                int was_value = Convert.ToInt32(cashierItem.GoodsList.CoinsValue / ratio);
                AssetBundleUtils.SetTextForTransform(price_was_text, "WAS " + FormatUtils.FormatBalance(was_value));
            }
        }

        private string GetLevelText()
        {
            int curr_level = m_user.GetLevel();

            if (curr_level >= 180)
            {
                return "180+";
            }
            else if (curr_level >= 140)
            {
                return "140-179";
            }
            else if (curr_level >= 100)
            {
                return "100-139";
            }
            else if (curr_level >= 60)
            {
                return "60-99";
            }
            else if (curr_level >= 20)
            {
                return "20-59";
            }
            else
            {
                return "1-19";
            }
        }

        private IEnumerator AutoEnableCloseButton()
        {
            yield return new WaitForSecondsRealtime(15);
            if (this != null)
                m_close_button.enabled = true;
        }

        public override void Close()
        {
            base.Close();

            if (m_close_button != null)
                m_close_button.enabled = true; // Enable the close button, will be invoke here since a popup will be shown

        }
    }
}
