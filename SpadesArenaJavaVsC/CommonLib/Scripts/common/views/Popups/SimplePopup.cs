﻿namespace common.views.popups
{
    /// <summary>
    /// s generic reusable script for a popup that only have a close button interaction. can be used as single screen info popup
    /// </summary>
    public class SimplePopup : PopupBase
    {

        public override void CloseButtonClicked()
        {
            m_manager.HidePopup();
        }
    }
}