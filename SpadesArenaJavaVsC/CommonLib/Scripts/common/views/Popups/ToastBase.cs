﻿using common.controllers;

namespace common.views
{
    public class ToastBase : MonoBehaviour
    {
        
        protected ToastManager m_manager;

        protected CanvasGroup m_canvas_group;
        private RectTransform m_rect_transform;
        private TextMeshProUGUI m_text;
        public string Text { get => m_text.text; }

        private bool undergoneSetup = false;
        
        protected virtual void Awake()
        {
            m_canvas_group = GetComponent<CanvasGroup>(); // Moved here from Show() so we can update the alpha to 0 even before Show() is invoked
            m_rect_transform = GetComponent<RectTransform>();
            m_text = GetComponentInChildren<TextMeshProUGUI>();
        }

        public virtual void Start()
        {
            if (m_canvas_group == null)
                m_canvas_group = GetComponent<CanvasGroup>(); // Moved here from Show() so we can update the alpha to 0 even before Show() is invoked
            if (m_rect_transform == null)
                m_rect_transform = GetComponent<RectTransform>();
            if (m_text == null)
                m_text = GetComponentInChildren<TextMeshProUGUI>();
        }

        //TODO: handle multi line in the same way SpecialSignView handing it in rummy
        public void AutoSetup(string text, Vector3? position = null)
        {
            m_rect_transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, text.Length * ToastManager.DEFAULT_CHAR_WIDTH + ToastManager.DEFAULT_MARGINS_WIDTH);
            m_text.text = text;

            if (position != null)
                m_rect_transform.localPosition = position.Value;
            else
                m_rect_transform.localPosition = GetPosition();

            m_rect_transform.localScale = Vector3.zero;
            undergoneSetup = true;
        }

        public void Setup(string text, float width, Vector3? position = null, float heigth = 0)
        {
            m_rect_transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            if (heigth > 0)
                m_rect_transform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, heigth);

            m_text.text = text;

            if (position != null)
                m_rect_transform.localPosition = position.Value;
            else
                m_rect_transform.localPosition = GetPosition();

            m_rect_transform.localScale = Vector3.zero;
            undergoneSetup = true;
        }

        //public void Setup (string text, Rect rect, Vector3? position = null)
        //{
        //    if (position != null)
        //        m_rect_transform.localPosition = position.Value;
        //    else
        //        m_rect_transform.localPosition = Vector3.zero;

        //    m_rect_transform.localScale = Vector3.zero;
        //    undergoneSetup = true;
        //}

        public virtual void Show(ToastManager manager)
        {
            if (!undergoneSetup)
            {
                LoggerController.Instance.LogError("tried to show a toast without proper setup first");
                return;
            }

            m_manager = manager;

            this.gameObject.SetActive(true);

            PlayOpenAnimation();
        }

        //no opening animation
        public virtual void ShowImmediately(ToastManager manager)
        {
            if (!undergoneSetup)
            {
                LoggerController.Instance.LogError("tried to show a toast without proper setup first");
                return;
            }

            m_manager = manager;

            this.gameObject.SetActive(true);

            UpdateToastAlpha(1);
            m_rect_transform.localScale = Vector3.one;
            OnEnterAnimDone();
        }

        protected virtual void PlayOpenAnimation()
        {
            UpdateToastAlpha(0);

            m_rect_transform.DOScale(1, ToastManager.OPEN_TIME).SetEase(Ease.OutBack);
            m_canvas_group.DOFade(1, ToastManager.OPEN_TIME).OnComplete(OnEnterAnimDone);
        }
        
        public void UpdateToastAlpha(float value)
        {
            if (m_canvas_group != null)
                m_canvas_group.alpha = value;
        }

        /// <summary>
        /// event from the toast manager to indicate aniamtion done 
        /// </summary>
        protected virtual void OnEnterAnimDone()
        {
        }

        public virtual void Close()
        {
            PlayCloseAnimation();
        }
        
        protected virtual void PlayCloseAnimation()
        {
            m_rect_transform.DOScale(0, ToastManager.CLOSE_TIME).SetEase(Ease.InBack);
            m_canvas_group.DOFade(0, ToastManager.CLOSE_TIME).OnComplete(OnExitAnimDone);

        }
        


        /// <summary>
        /// Raises the exit animation done event.
        /// </summary>
        protected virtual void OnExitAnimDone()
        {
            // Notifty the toast manager that the exit animation is finished
            m_manager?.OnToastClosed();

        }
        
        public virtual Vector3 GetPosition()
        {
            return new Vector3(0, 219, 0);
        }
        
    }
}