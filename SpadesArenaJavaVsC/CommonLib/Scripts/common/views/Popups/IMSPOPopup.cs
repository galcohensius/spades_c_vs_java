﻿using common.controllers;
using common.utils;
using common.ims;
using System.Collections.Generic;
using common.comm;

namespace common.views.popups
{
    public class IMSPOPopup : IMSPopup
    {
        protected override void InstantiatePrefab()
        {
            base.InstantiatePrefab();

            // items
            List<Transform> items = new List<Transform>();
            Transform container = transform.FindDeepChild("Container");
            for (int i = 0; i < container.childCount; i++)
            {
                m_dynamicContentPO = CommManager.Instance.GetModelParser().ParsePODynamicContent(m_iZone.DynamicContent.Content[i]);

                int itemNumber = i + 1;
                items.Add(transform.FindDeepChild("Item_" + itemNumber));

                // if this item contains MGAP, show an indication icon
                Transform mgapIndication = items[i].transform.FindDeepChild("MGAPIndicator");
                if (mgapIndication != null)
                    mgapIndication.gameObject.SetActive(m_dynamicContentPO.Goods.MGAP);

                int j = i + 1;
                Transform panels = items[i].FindDeepChild("Panels");
                HandlePanels(panels.gameObject);


                // Coins - support for old way - need to be removed when no prefabs are using this (RAN 19/4/2020)
                Transform oldCoins = m_selectedPanel.FindDeepChild("CoinsText");
                if (oldCoins!=null && m_dynamicContentPO.Goods.CoinsValue > 0)
                    oldCoins.GetComponent<TextMeshProUGUI>().text = "<sprite=1>" + FormatUtils.FormatBalance(m_dynamicContentPO.Goods.CoinsValue);


                // costs
                items[i].FindDeepChild("CostText").GetComponent<TextMeshProUGUI>().text = "$" + m_dynamicContentPO.Cost;
                // Percentage
                items[i].FindDeepChild("BonusText").GetComponent<TextMeshProUGUI>().text = m_dynamicContentPO.PercentageMore + "%";

                Transform btn = items[i].FindDeepChild("Button_" + j);
                if (btn==null)
                    btn = items[i].FindDeepChild("Button"); // This is the correct way to name the button..

                if (btn != null)
                {
                    int closureIndex = i;
                    btn.GetComponent<Button>().onClick.AddListener(() => { ButtonClicked(closureIndex); });
                    btn.gameObject.AddComponent<ButtonsOverView>();
                }
            }
        }

        private void ButtonClicked(int index)
        {
            LoggerController.Instance.Log("IMSPO button clicked: " + index);

            // call the purchase command
            IMSController.Instance.ExecuteDirectBuyAction(m_iZone, index, true);
        }
    }
}
