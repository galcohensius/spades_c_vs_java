﻿using common.controllers;
using System;

namespace common.views
{
	public delegate void ScreenClosedDelegate();

	public class ScreensManager: MonoBehaviour
	{
		private ScreenBase		m_screen_base;

		public event Action<bool> OnScreenShown;

        private bool m_screen_shown;

        private void Start()
        {
            // Register for bg event to hide a screen if shown
            BackgroundController.Instance.OnSessionTimeout += HideScreen;
        }


        protected void ShowScreen(GameObject popup_object, ScreenClosedDelegate close_delegate=null)
		{
			HideScreen ();
			m_screen_base = popup_object.GetComponent<ScreenBase>();

            LoggerController.Instance.Log("SCREEN --- Showing Screen: " + m_screen_base.gameObject.name);

            if (OnScreenShown!=null)
				OnScreenShown (true);

			m_screen_base.Show(this,close_delegate);

            m_screen_shown = true;

		}

		public void HideScreen()
		{
			if (m_screen_base != null)
			{
                LoggerController.Instance.Log("SCREEN --- Hiding Screen: " + m_screen_base.gameObject.name);
                m_screen_base.Close ();
				m_screen_base = null;
			}
			if(OnScreenShown!=null)
				OnScreenShown (false);

            m_screen_shown = false;

		}

        public bool IsScreenShown
        {
            get
            {
                return m_screen_shown;
            }

        }

	}
}

