﻿using System;

namespace common.notification.local
{
	public class NotificationManagerEditor : NotificationManager
	{
		public override void ScheduleNotification (int id, TimeSpan fire_delay, string title_text, string body_text, string custom_sound = null)
		{
			body_text += "\nFire Date: " + fire_delay;

			//PopupManager.Instance.ShowMessagePopup (body_text);
		}

		public override void CancelPendingNotifications ()
		{
			//PopupManager.Instance.ShowMessagePopup ("canceling all notifications");
		}

		public override void CancelPendingNotifications (int id)
		{
			//PopupManager.Instance.ShowMessagePopup ("canceling " + id + " notification");
		}

		public override void RequestNotificationsPermission ()
		{
            
		}

		public override bool IsNotificationsPermissionEnabled ()
		{
			return true;
		}

		public override void CancelProcessedNotifications ()
		{
			
		}

	}
}