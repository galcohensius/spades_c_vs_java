﻿
#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using LocalNotification = UnityEngine.iOS.LocalNotification;
using NotificationServices = UnityEngine.iOS.NotificationServices;
using NotificationType = UnityEngine.iOS.NotificationType;
using common.controllers;

namespace common.notification.local
{
	public class NotificationManagerIOS : NotificationManager 
	{
        public override void ScheduleNotification (int id,TimeSpan fire_delay,string header, string body,string custom_sound=null )
		{
			LocalNotification notification = new LocalNotification ();
			notification.fireDate = DateTime.Now + fire_delay;
			
            IDictionary notifDict = new Dictionary<string,string> ();
            notifDict ["NID"] = id.ToString();
			notification.userInfo = notifDict; 
			
			notification.alertBody = body;
			notification.soundName = custom_sound + ".aiff";
			notification.applicationIconBadgeNumber = 1;
			NotificationServices.ScheduleLocalNotification (notification);

			LoggerController.Instance.LogFormat("Local iOS notification with id {0} scheduled for {1}: {2}",id, notification.fireDate, body);
		}

		public override void CancelPendingNotifications ()
		{
			NotificationServices.CancelAllLocalNotifications ();
		}

		public override void CancelPendingNotifications (int id)
		{
			foreach (LocalNotification notification in NotificationServices.scheduledLocalNotifications) {
				if (notification.userInfo.Contains ("NID") && notification.userInfo ["NID"].ToString() == id.ToString()) {
					NotificationServices.CancelLocalNotification (notification);
					LoggerController.Instance.LogFormat("Local iOS notification with id {0} canceled", id);
					break;
				}
			}

		}
      
        public override void RequestNotificationsPermission()
        {
			NotificationServices.RegisterForNotifications(
                   NotificationType.Alert |
                   NotificationType.Badge |
                   NotificationType.Sound);
        }

        public override bool IsNotificationsPermissionEnabled()
        {
			return NotificationServices.enabledNotificationTypes == (NotificationType.Alert |
                                                                     NotificationType.Badge |
                                                                     NotificationType.Sound);
			
        }

		public override void CancelProcessedNotifications()
		{
            NotificationServices.ClearLocalNotifications();
		}

    }
}

#endif