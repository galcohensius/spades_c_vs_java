﻿using System;

namespace common.notification.local
{

    public class NotificationManagerAndroid : NotificationManager
    {

        public NotificationManagerAndroid()
        {
            #if UNITY_ANDROID
            // This fix was suggested by Pushwoosh in order to stop the OOM crashes
            if (PlayerPrefs.GetInt("drop_pw", 0) == 0)
            {
                PushNotificationsAndroid pushAndroid = (PushNotificationsAndroid)Pushwoosh.Instance; // Casting is safe here
                pushAndroid.ClearLocalNotifications();
                PlayerPrefs.SetInt("drop_pw", 1);
            }
#endif
        }

        public override void ScheduleNotification(int id, TimeSpan fire_delay, string header, string body, string custom_sound = null)
        {
#if UNITY_ANDROID
            PushNotificationsAndroid pushAndroid = (PushNotificationsAndroid) Pushwoosh.Instance; // Casting is safe here
	
            Dictionary<string,string> notifParams = new Dictionary<string, string> {
                {"s",custom_sound}, // Sound
                {"header",header},
                {"pw_badges", "1"},
                //{"ai","http://fb-qa.spadesroyale.com/images/coins.png"}, // Icon
                //{"b","http://fb-qa.spadesroyale.com/images/banner.jpg"} // Banner
            };

			int localNotifId = pushAndroid.ScheduleLocalNotification(body, (int)fire_delay.TotalSeconds, notifParams);

            // Store the local notification id on local PlayerPrefs so we can cancel it later if needed
            PlayerPrefs.SetInt("LocalNotif_" + id, localNotifId);

            LoggerController.Instance.LogFormat("Local Android notification scheduled for {0}: {1}", DateTime.Now + fire_delay, body);

#endif
        }

        public override void CancelPendingNotifications()
        {
#if UNITY_ANDROID
            PushNotificationsAndroid pushAndroid = (PushNotificationsAndroid)Pushwoosh.Instance; 
            pushAndroid.ClearLocalNotifications();
#endif
        }

        public override void CancelPendingNotifications(int id)
        {
#if UNITY_ANDROID
            PushNotificationsAndroid pushAndroid = (PushNotificationsAndroid)Pushwoosh.Instance;

            // Get the store local notification id if exists
            int localNotifId = PlayerPrefs.GetInt("LocalNotif_" + id, 0);

            if (localNotifId>0) {
                pushAndroid.ClearLocalNotification(localNotifId);
                PlayerPrefs.DeleteKey("LocalNotif_" + id);
            }
#endif

		}

        public override void RequestNotificationsPermission()
        {
            
        }

        public override bool IsNotificationsPermissionEnabled()
        {
            return true; // TOOD: CHECK REAL PERMISSION
        }

		public override void CancelProcessedNotifications()
		{
			
		}

    }
}