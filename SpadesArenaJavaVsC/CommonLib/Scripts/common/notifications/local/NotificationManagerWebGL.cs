﻿using System;

namespace common.notification.local
{

	public class NotificationManagerWebGL : NotificationManager
	{
		public override void ScheduleNotification (int id,TimeSpan fire_delay,string title_text, string body_text, string custom_sound=null )
		{
			
		}

		public override void CancelPendingNotifications ()
		{
			
		}

		public override void CancelPendingNotifications (int id)
		{

		}

        public override void RequestNotificationsPermission()
        {
            
        }

        public override bool IsNotificationsPermissionEnabled()
        {
            return false;
        }

		public override void CancelProcessedNotifications()
		{

		}
    }
}