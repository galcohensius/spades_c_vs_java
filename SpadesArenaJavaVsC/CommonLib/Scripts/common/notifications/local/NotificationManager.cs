﻿
using common.controllers;
using System;

namespace common.notification.local
{

    public abstract class NotificationManager
    {
        private static NotificationManager m_instance;

        public static NotificationManager Instance
        {
            get
            {

                if (m_instance == null)
                {
                    LoggerController.Instance.Log("Creating NotificationManager instance");
#if UNITY_EDITOR

                    m_instance = new NotificationManagerEditor();

#elif UNITY_ANDROID

					m_instance= new NotificationManagerAndroid();

#elif UNITY_IOS

					m_instance= new NotificationManagerIOS();

#elif UNITY_WEBGL

					m_instance= new NotificationManagerWebGL();


#endif
                }
                return m_instance;
            }
        }

        protected NotificationManager()
        {
        }

       
        public abstract void CancelPendingNotifications();
		public abstract void CancelPendingNotifications(int id);

		public abstract void ScheduleNotification (int id,TimeSpan fire_delay, string header, string body, string custom_sound = null);

        public abstract void RequestNotificationsPermission();

        public abstract bool IsNotificationsPermissionEnabled();

		public abstract void CancelProcessedNotifications();


    }

}