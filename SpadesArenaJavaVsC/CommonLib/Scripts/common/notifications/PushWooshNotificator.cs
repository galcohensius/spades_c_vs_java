﻿using common.controllers;

namespace common.notification {

    public class PushWooshNotificator : MonoBehaviour {

        [SerializeField]
        string m_pushwooshAppId=null;

        [SerializeField]
        string m_fcm_sender=null;


		// use for initialization
		void Start()
        {
            LoggerController.Instance.Log("PushWooshNotificator initializing...");

            Pushwoosh.ApplicationCode = m_pushwooshAppId;
            Pushwoosh.FcmProjectNumber = m_fcm_sender;
            Pushwoosh.Instance.OnRegisteredForPushNotifications += OnRegisteredForPushNotifications;
            Pushwoosh.Instance.OnFailedToRegisteredForPushNotifications += OnFailedToRegisteredForPushNotifications;
            Pushwoosh.Instance.OnPushNotificationsReceived += OnPushNotificationsReceived;
            Pushwoosh.Instance.RegisterForPushNotifications();

            
        }

        void OnRegisteredForPushNotifications(string token)
        {
            // handle here
            LoggerController.Instance.Log("Received token: \n" + token);
        }

        void OnFailedToRegisteredForPushNotifications(string error)
        {
            // handle here
            LoggerController.Instance.Log("Error ocurred while registering to push notifications: \n" + error);
        }

        void OnPushNotificationsReceived(string payload)
        {
            // handle here
            LoggerController.Instance.Log("Received push notificaiton: \n" + payload);
        }

		public string PushwooshAppId
        {
            get
            {
                return m_pushwooshAppId;
            }
        }

		public string Fcm_sender_id
		{
			get
			{
				return m_fcm_sender;
			}
		}
	}
}
