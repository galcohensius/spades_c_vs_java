namespace common.apple
{

   

    public abstract class AppleController : MonoBehaviour
    {
        public delegate void AppleSignInCallback(AppleData appleData);

        protected AppleData m_AppleData;
        public AppleData AppleData { get { return m_AppleData; } }

        protected AppleSignInCallback AppleSignInFinished = null;
        protected bool signInRequest;
        protected string appleUserID = "";

        public const string AppleUserIdKey = "AppleUserId";
        public const string AppleUserGivenName = "AppleFirstName";
        public const string AppleUserFamilyName = "AppleFamily";
        public const string AppleUserIdEmail = "AppleUserEmail";


        private static AppleController m_instance;

        public static AppleController Instance
        {
            get
            {
                if (m_instance == null)
                {

                    GameObject appleRoot = new GameObject("AppleControllerRoot");
                    DontDestroyOnLoad(appleRoot);

#if UNITY_IOS && !UNITY_EDITOR
                    m_instance = appleRoot.AddComponent<AppleControllerImpl1> ();
#else
                    m_instance = appleRoot.AddComponent<AppleControllerEditor>();
#endif
                }
                return m_instance;
            }
        }
       

        public abstract void SignInWithApple(AppleSignInCallback appleSignInCallback, bool signinRequest = false);

    }

    public class AppleData
    {
        public bool SignedIn { get; set; } = false;
        public string UserId { get; set; } = "";
        public string FirstName { get; set; } = "";
        public string LastName { get; set; } = "";
        public string Email { get; set; } = "";
    }
}