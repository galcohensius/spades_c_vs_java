﻿using common.utils;
using common.controllers;


namespace common.apple.views
{
    public class AppleButton : MonoBehaviour
    {

        public enum ButtonMode
        {
            Connect, Connected, Waiting
        }

        Button button;

        GameObject waitingIndicator = null;

        private void Awake()
        {
            button = GetComponent<Button>();
        }

        private void Start()
        {

            waitingIndicator = Instantiate(PopupManager.Instance.Loader, transform);
            waitingIndicator.SetActive(false);
        }

        public void SetMode(ButtonMode buttonText, bool showGift = false)
        {
            //text.gameObject.SetActive(true);
            if (waitingIndicator != null)
                waitingIndicator.SetActive(false);
            //giftImage.gameObject.SetActive(false);
            button.interactable = true;

            switch (buttonText)
            {
                case ButtonMode.Connect:
                    gameObject.SetActive(true);
                    button.interactable = true;

                    //text.text = "connect";
                    //giftImage.gameObject.SetActive(showGift);
                    break;
                case ButtonMode.Connected:
                    gameObject.SetActive(false);
                    button.interactable = false;
                    break;

                case ButtonMode.Waiting:
                    //text.gameObject.SetActive(false);
                    if (waitingIndicator != null)
                        waitingIndicator.SetActive(true);
                    gameObject.SetActive(true);
                    button.interactable = false;
                    break;
            }
        }

        public bool Interactable
        {
            set
            {
                if(button!=null)
                {
                    button.interactable = value;
                    ViewUtils.EnableDisableButtonText(button);
                }
            }
        }

        public GameObject WaitingIndicator { get => waitingIndicator; set => waitingIndicator = value; }
    }
}