﻿using common.controllers;


namespace common.apple
{
    public class AppleControllerImpl1 : AppleController
    {
        private IAppleAuthManager appleAuthManager;

        public override void SignInWithApple(AppleSignInCallback appleSignInCallback, bool signinRequest = false)
        {
            if (m_AppleData == null)
                m_AppleData = new AppleData();

            AppleSignInFinished = appleSignInCallback;
            signInRequest = signinRequest;
#if UNITY_IOS
            LoggerController.Instance.Log("AppleController: Started Sign in with Apple login, signInRequest = " + signInRequest.ToString());
            if (SetAppleAuthManager())
                ProcessAppleSignIn();
            else
            {
                LoggerController.Instance.Log("AppleController: Cannot set Apple Auth Manager");
                appleSignInCallback?.Invoke(null);
            }

#else
            LoggerController.Instance.Log("Not an Apple device");

            appleSignInCallback(null);
#endif
        }

        private bool SetAppleAuthManager ()
        {
            // If the current platform is supported
            if (AppleAuthManager.IsCurrentPlatformSupported)
            {
                // Creates a default JSON deserializer, to transform JSON Native responses to C# instances
                var deserializer = new PayloadDeserializer();
                // Creates an Apple Authentication manager with the deserializer
                this.appleAuthManager = new AppleAuthManager(deserializer);
                return true;
            }
            return false;
        }

        private void SetCallback(IAppleIDCredential credential)
        {
            LoggerController.Instance.Log("AppleController: SetCallback,  credential not null? " + credential!=null?"true":"false" + ", signInRequest = " + signInRequest.ToString());

            if (credential != null)
            {
                //if (m_AppleData.signedInAuthorized)
                //{
                    StateController.Instance.SetAppleUserValue(AppleUserIdKey, credential.User);
                    m_AppleData.UserId = credential.User;
                    if (signInRequest == true)
                    {
                        LoggerController.Instance.Log("AppleController: Storing apple user details to player pref");

                        StateController.Instance.SetAppleUserValue(AppleUserGivenName, credential.FullName?.GivenName);
                        StateController.Instance.SetAppleUserValue(AppleUserFamilyName, credential.FullName?.FamilyName);
                        StateController.Instance.SetAppleUserValue(AppleUserIdEmail, AppleUserIdEmail);

                        m_AppleData.FirstName = credential.FullName?.GivenName;
                        m_AppleData.LastName = credential.FullName?.FamilyName;
                        m_AppleData.Email = credential.Email;
                    }
                //}
            }

            m_AppleData.SignedIn = credential != null;
            if (m_AppleData.SignedIn)
                m_AppleData.UserId = credential.User;

            AppleSignInFinished?.Invoke(m_AppleData);
        }

       
        private void ProcessAppleSignIn()
        {
            appleUserID = StateController.Instance.GetAppleUserValue(AppleUserIdKey);

            if (appleUserID != "")
                CheckCredentialStatusForUserId(appleUserID);
            else
            {
                if (signInRequest)
                    Login();
                else
                    AttemptQuickLogin();
            }
        }

        private void Login ()
        {
            LoggerController.Instance.Log("AppleController: Starting on-demand login with apple");

            var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

            this.appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                // If a sign in with apple succeeds, we should have obtained the credential with the user id, name, and email, save it
                SetCallback(credential as IAppleIDCredential);
            },
            error =>
            {
                LoggerController.Instance.LogWarningFormat("AppleController: Sign in with Apple failed. Error code: {0}, desc: {1}", error.Code, error.LocalizedDescription);
                SetCallback(null);
            });
        }

        private void AttemptQuickLogin()
        {
            LoggerController.Instance.Log("Starting quick login with apple");

            var quickLoginArgs = new AppleAuthQuickLoginArgs();

            
            // Quick login should succeed if the credential was authorized before and not revoked
            this.appleAuthManager.QuickLogin(
                quickLoginArgs,
                credential =>
                {
                    LoggerController.Instance.Log("AppleController: Success quick login with apple");

                    // If it's an Apple credential, save the user ID, for later logins
                    var appleIdCredential = credential as IAppleIDCredential;
                    if (appleIdCredential != null)
                    {
                        SetCallback(appleIdCredential);
                    } else
                    {
                        SetCallback(null);
                    }
                },
                error =>
                {
                    // If Quick Login fails, we should show the normal sign in with apple menu, to allow for a normal Sign In with apple
                    LoggerController.Instance.LogWarningFormat("AppleController: Quick login with Apple failed. Error code: {0}, desc: {1}", error.Code, error.LocalizedDescription);
                    SetCallback(null);
                });
        }

        private void CheckCredentialStatusForUserId(string appleUserID)
        {
            LoggerController.Instance.Log("Starting Check credencials with apple");

            // If there is an apple ID available, we should check the credential state
            this.appleAuthManager.GetCredentialState(
                appleUserID,
                state =>
                {
                    switch (state)
                    {
                    // If it's authorized, login with that user id
                    case CredentialState.Authorized:
                            var appleIdCredential = new AppleIDCredential();
                            appleIdCredential._user = appleUserID;
                            LoggerController.Instance.Log("Check Credencial authorized");

                            SetCallback(appleIdCredential);
                            return;

                    // If it was revoked, or not found, we need a new sign in with apple attempt
                    // Discard previous apple user id
                    case CredentialState.Revoked:
                        case CredentialState.NotFound:
                            StateController.Instance.DeleteAppleUserKey(AppleUserIdKey);
                            StateController.Instance.DeleteAppleUserKey(AppleUserGivenName);
                            StateController.Instance.DeleteAppleUserKey(AppleUserFamilyName);
                            StateController.Instance.DeleteAppleUserKey(AppleUserIdEmail);
                            LoggerController.Instance.Log("AppleController: Check Credencial is not authorized - Apple User ID deleted");
                            SetCallback(null);
                            return;
                    }
                },
                error =>
                {
                    LoggerController.Instance.LogWarning("AppleController: Error while trying to get credential state " + error.ToString());
                });
        }

        void Update()
        {
            // Updates the AppleAuthManager instance to execute
            // pending callbacks inside Unity's execution loop
            if (this.appleAuthManager != null)
            {
                this.appleAuthManager.Update();
            }
        }
    }
}