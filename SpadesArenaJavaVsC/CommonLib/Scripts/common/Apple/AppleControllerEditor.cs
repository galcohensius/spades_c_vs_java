﻿using common.controllers;

namespace common.apple
{
    public class AppleControllerEditor : AppleController
    {

        public override void SignInWithApple(AppleSignInCallback appleSignInCallback, bool signinRequest = false)
        {
            m_AppleData = null;
            AppleSignInFinished = appleSignInCallback;
            signInRequest = signinRequest;
#if (UNITY_IOS || UNITY_TVOS)
            Debug.Log("Started Sign in with Apple login Editor");

            ProcessAppleSignIn();
#else
            Debug.Log("Not an Apple device");

            appleSignInCallback(m_AppleData);
#endif
        }

        private void ProcessAppleSignIn ()
        {
            Debug.Log("Process Apple Sign In Editor");

            appleUserID = StateController.Instance.GetAppleUserValue(AppleUserIdKey);

            if (signInRequest)
            {
                Debug.Log("Starting on-demand login with apple Editor");
                StateController.Instance.SetAppleUserValue(AppleUserIdKey, "112233445566");
                StateController.Instance.SetAppleUserValue(AppleUserGivenName, "Hozelito");
                StateController.Instance.SetAppleUserValue(AppleUserFamilyName, "Donkey");
                StateController.Instance.SetAppleUserValue(AppleUserIdEmail, "qa.ws@com");
            }
            else
            {
                Debug.Log("Starting quick login with apple Editor");

            }
            SetCallback();
        }

        private void SetCallback()
        {
            m_AppleData = new AppleData();
            m_AppleData.UserId = StateController.Instance.GetAppleUserValue(AppleUserIdKey);
            m_AppleData.SignedIn = m_AppleData.UserId != "";
            Debug.Log("appleUserIDForApp = " + m_AppleData.UserId + ", Authorized = " + m_AppleData.SignedIn.ToString());
            AppleSignInFinished?.Invoke(m_AppleData);
        }


    }
}