﻿using System.IO;

namespace common.comm
{

	public class StateObject  {



		public const int BUFFER_SIZE = 1024;  

		// Receive buffer.  
		private byte[] buffer = new byte[BUFFER_SIZE];  

		private MemoryStream m_data_stream = new MemoryStream(); 

		private ushort	length=0;

		private long lengthFieldEndPos;


		public byte[] Buffer {
			get {
				return buffer;
			}
		}


		public MemoryStream DataStream {
			get {
				return m_data_stream;
			}
		}

		public ushort MsgLength {
			get {
				return length;
			}
			set{ 
				length = value;
			}
		}

		public long LengthFieldEndPos {
			get {
				return lengthFieldEndPos;
			}
			set {
				lengthFieldEndPos = value;
			}
		}
	}
}