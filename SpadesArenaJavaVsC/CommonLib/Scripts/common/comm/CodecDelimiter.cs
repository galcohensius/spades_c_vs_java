﻿namespace common.comm
{

	public class CodecDelimiter {

		public const char BLOCK_START = '{';
		public const char BLOCK_END = '}';
		public const char PARAM = ',';

	}
}