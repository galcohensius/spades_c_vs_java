﻿using System;
using common.commands;

namespace common.comm
{
	public abstract class Connector 
	{
		protected Action	m_client_connected;
		protected Action	m_client_sent_done;
		protected Action<bool> 	m_connect_callback=null;

		protected string					m_host;
		protected int						m_port;
		protected CommandsManager			m_commands_manager;
		protected Encoder					m_encoder;
		protected Decoder					m_decoder;

		string 								m_org_host;

		public Connector (string host, int port, CommandsManager commands_manager)
		{
			this.m_host = host;
			this.m_org_host = host;
			this.m_port = port;
			this.m_commands_manager = commands_manager;

			m_encoder = new Encoder('{','}',',');
			m_decoder = new Decoder ('{', '}', ',');
		}

		public abstract void Connect (Action<bool> ConnectCallback=null);
		public abstract void Disconnect ();
		public abstract void Send (MMessage message);
        public abstract bool IsConnected();

		public virtual void OnUpdate() { }

        public bool showRawMessage;

        public void ModifyHostName(bool set_to_original)
		{
			if (set_to_original)
				m_host = m_org_host;
			else
				m_host = "XXX";
		}

		public Action Client_connected {
			get {
				return m_client_connected;
			}
			set {
				m_client_connected = value;
			}
		}


		public Action Client_sent_done {
			get {
				return m_client_sent_done;
			}
			set {
				m_client_sent_done = value;
			}
		}
	}
}

