﻿namespace common.comm
{

	public class SimpleParam : Param 
	{
		string m_value;

		public SimpleParam (string m_value)
		{
			this.m_value = m_value;
		}

		public SimpleParam (int m_value)
		{
			this.m_value = m_value.ToString ();
		}

        public SimpleParam(long m_value)
        {
            this.m_value = m_value.ToString();
        }
		

		public override string Value {
			get {
				return m_value;
			}
		}

		public override string ToString ()
		{
			return m_value;
		}
		

	}
}