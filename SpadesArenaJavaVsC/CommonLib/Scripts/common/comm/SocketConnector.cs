using System.Net.Sockets;
using System.IO;
using System.Net;
using System;
using common.utils;
using common.commands;
using common.controllers;

namespace common.comm
{
    /// <summary>
    /// Connector for standard socket connection as used on mobile platforms
    /// </summary>
    public class SocketConnector : Connector
    {
        Socket m_client;

        private const int MSG_LEN_LENGTH = 2;

        AddressFamily m_addressFamily = AddressFamily.Unspecified;

        public SocketConnector(string host, int port, CommandsManager commands_manager) : base(host, port, commands_manager)
        {
        }

        public override void Connect(Action<bool> ConnectCallback)
        {
            
            try
            {
                m_connect_callback = ConnectCallback;

                AddressFamily addressFamily = m_addressFamily;

                if (addressFamily == AddressFamily.Unspecified)
                    addressFamily = AddressFamily.InterNetwork;

                m_client = new Socket(addressFamily, SocketType.Stream, ProtocolType.Tcp);

                m_client.SendTimeout = 3000;
                m_client.ReceiveTimeout = 3000;
                m_client.NoDelay = true;

                LoggerController.Instance.Log(LoggerController.Module.Comm, "Connecting to " + m_host + ":" + m_port + "...");

                // Connect to the remote endpoint.  
                m_client.BeginConnect(m_host, m_port, new AsyncCallback(ConnectComplete), null);

            }
            catch (SocketException e)
            {
                //temporary removed the error so i can continue testing disconnection

                LoggerController.Instance.LogError(LoggerController.Module.Comm, "Cannot connect socket: " + e);

                ConnectCallback(false);
            }

        }

        private void ConnectComplete(IAsyncResult ar)
        {

            LoggerController.Instance.Log(LoggerController.Module.Comm, "Connect Complete CallBack");

            if (m_client == null || !m_client.Connected)
            {
                if (m_addressFamily == AddressFamily.Unspecified)
                {
                    m_addressFamily = AddressFamily.InterNetworkV6;
                    Connect(m_connect_callback);

                    LoggerController.Instance.Log(LoggerController.Module.Comm, "Trying to connect to IpV6 network...");

                    return;
                }

                if (m_connect_callback != null)
                {
                    UnityMainThreadDispatcher.Instance.Enqueue(() => {
                        m_connect_callback(false);
                    });
                }
                return;
            }

            try
            {
                // Complete the connection.  
                m_client.EndConnect(ar);

                m_addressFamily = m_client.AddressFamily;

                LoggerController.Instance.Log(LoggerController.Module.Comm, "Socket connected to: " + m_client.RemoteEndPoint);

                if (m_connect_callback != null)
                {
                    UnityMainThreadDispatcher.Instance.Enqueue(() => {
                        m_connect_callback(true);
                    });
                }
                if (m_client_connected != null)
                {
                    UnityMainThreadDispatcher.Instance.Enqueue(() => {
                        m_client_connected();
                    });
                }
                StartReceive();
            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm,"Cannot complete socket connection: " + e);
            }
        }

        private void StartReceive()
        {
            if (m_client == null || !m_client.Connected)
                return;

            try
            {

                // Create the state object.  
                StateObject state = new StateObject();

                // Begin receiving the data from the remote device.  
                m_client.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReceiveComplete), state);
            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm,"Cannot receive from socket: " + e);
            }
        }

        private void ReceiveComplete(IAsyncResult ar)
        {
            if (m_client == null || !m_client.Connected)
                return;

            try
            {

                // Retrieve the state object and the client socket   
                // from the asynchronous state object.  
                StateObject state = (StateObject)ar.AsyncState;

                // Read data from the remote device.  
                int bytesRead = m_client.EndReceive(ar);
                if (bytesRead == 0)
                    return;

                // Write the buffer to the stream and reset the position
                long lastPos = state.DataStream.Position;
                state.DataStream.Write(state.Buffer, 0, bytesRead);

                // Move the stream position back so we can start reading
                state.DataStream.Seek(lastPos, SeekOrigin.Begin);

                // Loop over multiple messages that might arrive in a single read
                do
                {
                    if (state.MsgLength == 0)
                    {

                        // Msg length was not set on previous read
                        state.MsgLength = ByteUtils.ReadLength(state.DataStream);

                        // Mark the position after the length field
                        state.LengthFieldEndPos = state.DataStream.Position;

                        if (state.MsgLength < MSG_LEN_LENGTH)
                        {
                            // not enough bytes to read the length
                            m_client.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReceiveComplete), state);
                            break;
                        }

                        if (state.MsgLength < bytesRead - MSG_LEN_LENGTH)
                        {
                            // Received data is larger than the message length
                            LoggerController.Instance.LogFormat(LoggerController.Module.Comm, "Message length is {0} and data length is {1}", state.MsgLength, bytesRead - MSG_LEN_LENGTH);
                        }

                    }

                    if (state.MsgLength <= state.DataStream.Length - MSG_LEN_LENGTH)
                    {
                        if (showRawMessage)
                            Debug.Log("***** state.MsgLength=" + state.MsgLength + ", state.DataStream.Length - MSG_LEN_LENGTH=" + (state.DataStream.Length - MSG_LEN_LENGTH));

                        // Go back to the position after the length
                        state.DataStream.Seek(state.LengthFieldEndPos, SeekOrigin.Begin);

                        // Process the message
                        byte[] data_no_len = new byte[state.MsgLength];
                        state.DataStream.Read(data_no_len, 0, state.MsgLength);

                        if (showRawMessage)
                            Debug.Log("***** Raw message=" + System.Text.Encoding.UTF8.GetString(data_no_len));


                       MMessage message = m_decoder.Decode(data_no_len);

                        if (LocalDataController.Instance.Show_message_debug && message.Code != CommandsBuilder.ECHO_CODE)
                            LoggerController.Instance.Log(LoggerController.Module.Comm, FormatUtils.GetLogTime() + " Message Received: " + message);


                        UnityMainThreadDispatcher.Instance.Enqueue(() => {
                            m_commands_manager.ExecuteCommand(message);
                        });

                        // Reset the message length
                        state.MsgLength = 0;

                    }
                    else
                    {
                        if (showRawMessage)
                            Debug.Log("***** Not all bytes have been read: state.MsgLength=" + state.MsgLength + ", state.DataStream.Length - MSG_LEN_LENGTH=" + (state.DataStream.Length - MSG_LEN_LENGTH));

                        // Not all bytes have been read
                        // Move the position to the end of the data stream so the next read will continue from there
                        state.DataStream.Seek(state.DataStream.Length, SeekOrigin.Begin);

                        m_client.BeginReceive(state.Buffer, 0, StateObject.BUFFER_SIZE, 0, new AsyncCallback(ReceiveComplete), state);
                        break;
                    }


                } while (state.DataStream.Position < state.DataStream.Length);


                if (state.MsgLength == 0)
                {
                    // All messages have been read completly

                    // Dispose the state object memory stream
                    try
                    {
                        state.DataStream.Dispose();
                    }
                    catch (Exception) { }

                    // Wait for next message
                    StartReceive();

                }

            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm,"Cannot complete socket receive: " + e);
            }
        }

        public override void Send(MMessage message)
        {
            if (m_client == null || !m_client.Connected)
                return;

            try
            {
                byte[] message_bytes = m_encoder.Encode(message);

                byte[] message_with_len = ByteUtils.AddLength(message_bytes);

                // Begin sending the data to the remote device.  
                m_client.BeginSend(message_with_len, 0, message_with_len.Length, 0, new AsyncCallback(SendComplete), null);

                if (LocalDataController.Instance.Show_message_debug && message.Code != CommandsBuilder.ECHO_CODE)
                    LoggerController.Instance.Log(LoggerController.Module.Comm, FormatUtils.GetLogTime() + " Message Sent: " + message);

            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm,"Cannot send to socket: " + e);
            }
        }

        private void SendComplete(IAsyncResult ar)
        {
            if (m_client == null || !m_client.Connected)
                return;

            try
            {

                m_client.EndSend(ar);

                if (m_client_sent_done != null)
                {
                    UnityMainThreadDispatcher.Instance.Enqueue(() => {
                        m_client_sent_done();
                    });
                }

            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm,"Cannot complete send to socket: " + e);
            }
        }


        public override void Disconnect()
        {
            if (m_client == null || !m_client.Connected)
                return;

            m_client.Disconnect(false);

            LoggerController.Instance.Log(LoggerController.Module.Comm, "Socket disconnected");

        }


        private bool IsIpV6Socket()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return false;
                }

            }
            return true;
        }

        public override bool IsConnected()
        {
            if (m_client != null)
                return m_client.IsConnected();
            else
                return false;
        }

    }
}