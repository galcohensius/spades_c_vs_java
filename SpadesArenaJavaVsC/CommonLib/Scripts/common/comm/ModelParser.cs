﻿using common.ims;
using common.ims.model;
using common.models;
using System;
using System.Collections.Generic;

namespace common.comm
{
    public abstract class ModelParser
    {
        public VouchersGroup ParseVouchersGroup(JSONObject voucherJson)
        {
            VouchersGroup result = new VouchersGroup();

            string id = voucherJson["vId"].Value;
            DateTime expiration = DateTime.Now.AddYears(100);
            int expInterval = voucherJson["tLt"].AsInt;
            if (expInterval > 0)
                expiration = DateTime.Now.AddSeconds(expInterval);

            bool isSpecial = voucherJson["iSl"].AsInt == 1;

            int value = voucherJson["vae"].AsInt;

            int quantity = voucherJson["quy"].AsInt;
            for (int i = 0; i < quantity; i++)
            {
                Voucher voucher = new Voucher(id, expiration, value);
                result.AddVoucher(voucher);
            }

            result.TableFilterData = ParseTableFilterData(voucherJson);
            result.TableFilterData.SpecialOnly = isSpecial;
            return result;
        }


        public List<VouchersGroup> ParseVouchersList(JSONArray vouchersListJson)
        {
            List<VouchersGroup> result = new List<VouchersGroup>();

            foreach (JSONObject voucherJson in vouchersListJson)
            {
                VouchersGroup vouchersGroup = ParseVouchersGroup(voucherJson);
                result.Add(vouchersGroup);
            }

            return result;
        }
        /*public List<IMSGoodsList> ParseNewGoodsList(JSONArray goodsJsonArray)
        {
            List<IMSGoodsList> result = new List<IMSGoodsList>();
            foreach (JSONArray array in goodsJsonArray)
                ParseGoodList(array);

            return result;
        }*/

        private void ParseGoods(JSONObject goodsItemJson, IMSGoodsList result)
        {
            string type = goodsItemJson["gTe"];
            switch (type)
            {
                case "C": // Coins
                    result.Coins.Add(goodsItemJson["quy"].AsInt);
                    // this is for the MGAP
                    result.Factors.Add(goodsItemJson["far"].AsFloat);
                    break;
                case "V": // Vouchers
                    result.Vouchers.Add(ParseVouchersGroup(goodsItemJson));
                    break;
                case "I": // InventoryItem
                    result.InventoryItems.Add(goodsItemJson["id"].Value);
                    break;
                case "BM": // MGAP flag
                    result.MGAP = true;
                    break;
            }
        }
        public IMSGoodsList ParseNewGoodsList(JSONArray goodsJsonArray)
        {
            IMSGoodsList result = new IMSGoodsList();
            foreach (JSONArray array in goodsJsonArray)
            {
                foreach (JSONObject goodsItemJson in array)
                {
                    ParseGoods(goodsItemJson, result);
                }
            }

            return result;
        }

        public IMSGoodsList ParseGoodList(JSONArray goodsJsonArray)
        {
            IMSGoodsList result = new IMSGoodsList();

            foreach (JSONObject goodsItemJson in goodsJsonArray)
            {
                ParseGoods(goodsItemJson, result);
            }

            return result;
        }

        public IMSPODynamicContent ParsePODynamicContent(JSONObject json)
        {
            IMSGoodsList goodsList;

            // check if it's using the new parsing field "gSLt"
            if (json["gSLt"].ToString() != string.Empty)
                goodsList = ParseNewGoodsList(json["gSLt"].AsArray);
            else
                goodsList = ParseGoodList(json["gLt"].AsArray);

            float cost = json["cot"].AsFloat;
            int percentageMore = json["pMe"];
            return new IMSPODynamicContent(goodsList, cost, percentageMore);
        }


        #region Cashier

        /// <summary>
        /// Parses an IMS cashier
        /// </summary>
        public Cashier ParseCashier(JSONNode jsonNode)
        {
            if (jsonNode == null) return null;
            Cashier result = new Cashier();

            IMSModelParser modelParser = new IMSModelParser();

            JSONArray offersNode = jsonNode["pks"].AsArray;
            foreach (JSONNode offer in offersNode)
            {
                result.AddPackage(ParseCashierPackage(offer, modelParser));
            }

            result.CashierVersion = jsonNode["ver"].Value;

            result.Custom_bundle_path = jsonNode["src"].Value;

            result.Optimum_group = jsonNode["oPId"].AsInt;

            result.ImsMetadata = jsonNode["mDa"].Value;

            // Parse tracking data
            result.TrackingData = jsonNode["tDa"].AsObject;
            if (jsonNode["trg"] != null)
            {
                result.TrackImpressions = jsonNode["trg"]["iEe"].AsInt == 1;
                result.TrackClicks = jsonNode["trg"]["cEe"].AsInt == 1;
            }


            return result;
        }

        private CashierItem ParseCashierPackage(JSONNode jsonNode, IMSModelParser modelParser)
        {
            CashierItem result = new CashierItem();
            result.Id = jsonNode["pId"];
            result.Price = jsonNode["cot"];
            result.External_id = jsonNode["aSd"];
            //result.External_id_google = jsonNode ["pSd"];
            result.Bonus = jsonNode["dit"].AsInt;
            result.Best_value = jsonNode["tag"].Value == "B";
            result.Most_popular = jsonNode["tag"].Value == "C";
            result.ImsActionValue = modelParser.ParseActionValue(jsonNode["aVe"]);

            // Parse the good list
            IMSGoodsList imsGoodsList = ParseGoodList(jsonNode["gLt"].AsArray);
            result.GoodsList = imsGoodsList;

            return result;
        }

        [Obsolete]
        public Cashier ParseCashierMES(JSONNode jsonNode)
        {
            if (jsonNode == null) return null;

            Cashier result = new Cashier();

            JSONArray offersNode = jsonNode["pks"].AsArray;
            foreach (JSONNode offer in offersNode)
            {
                result.AddPackage(ParseCashierPackageMES(offer));
            }

            result.CashierVersion = jsonNode["ver"].Value;

            result.Custom_bundle_path = jsonNode["aPh"].Value;

            if (jsonNode["oPId"] != null)
                result.Optimum_group = jsonNode["oPId"].AsInt;

            return result;
        }

        private CashierItem ParseCashierPackageMES(JSONNode jsonNode)
        {
            CashierItem result = new CashierItem();
            result.Id = jsonNode["pId"];
            result.Price = jsonNode["cot"];
            result.External_id = jsonNode["aSd"];
            //result.External_id_google = jsonNode ["pSd"];
            result.Bonus = jsonNode["dit"].AsInt;

            if (jsonNode["gTe"].Value == "C")
            {
                result.GoodsList = new IMSGoodsList();
                result.GoodsList.Coins.Add(jsonNode["quy"].AsInt);
            }

            result.Most_popular = jsonNode["tag"].Value == "1";
            result.Best_value = jsonNode["tag"].Value == "2";

            return result;
        }

        #endregion

        public abstract TableFilterData ParseTableFilterData(JSONObject json);
        protected abstract TableFilterData CreateTableFilterdata();

    }

}
