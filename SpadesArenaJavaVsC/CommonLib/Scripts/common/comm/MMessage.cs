﻿using System.Collections.Generic;
using common.utils;

namespace common.comm
{
	
	public class MMessage  
	{

		string 			m_code;
		List<Param> 	m_params = new List<Param> ();


		public string Code {
			get {
				return m_code;
			}
			set {
				m_code = value;
			}
		}

		public List<Param> Params {
			get {
				return m_params;
			}
			set {
				m_params = value;
			}
		}

		public override string ToString ()
		{
			return string.Format ("[Message: code={0}, params={1}]", m_code, FormatUtils.FormatList (m_params));
		}
		

	}


}