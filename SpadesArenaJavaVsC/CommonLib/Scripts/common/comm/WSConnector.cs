﻿using System.Collections;
using System;
using common.commands;
using common.utils;
using common.controllers;

namespace common.comm
{
	/// <summary>
	/// Connector for WebSockets as used with WebGL platform
	/// </summary>
	public class WSConnector : Connector
	{
		
		MonoBehaviour m_wsDelegate;

		bool m_secured = true;
		string m_url;
			
		private WebSocketUnity m_webSocket;


        public WSConnector (string host, int port, CommandsManager commands_manager) : base (host, port, commands_manager)
		{
			if (m_secured)
			{
				m_url = string.Format ("wss://{0}:{1}", m_host, m_port);
			}
			else
			{
				m_url = string.Format ("ws://{0}:{1}", m_host, m_port);
			}

		}
	
	
		public override void Connect (Action<bool> ConnectCallback)
		{
			m_connect_callback = ConnectCallback;

			m_webSocket = new WebSocketUnity (m_url, m_wsDelegate); 

			m_webSocket.Open ();

		}



		public override void Disconnect ()
		{
            m_webSocket.Close();
		}

        public override void Send(MMessage message)
        {
            if (m_webSocket.IsOpened()) { 

                byte[] data = m_encoder.Encode(message);

                m_webSocket.Send(data);

                //using common.controllers;
                if (message.Code != CommandsBuilder.ECHO_CODE)
                    LoggerController.Instance.Log(LoggerController.Module.Comm,FormatUtils.GetLogTime() + " WSConnector message Sent: " + message);
            } else {
                LoggerController.Instance.LogError(LoggerController.Module.Comm, "Trying to send message on a closed web socket. Message: " + message);
            }
		}


		public MonoBehaviour WsDelegate {
			get {
				return this.m_wsDelegate;
			}
			set {
				m_wsDelegate = value;
			}
		}

        public WebSocketUnity WebSocket
        {
            get
            {
                return m_webSocket;
            }
        }

        
        public override bool IsConnected()
        {
            if(m_webSocket!=null)
                return m_webSocket.IsOpened();
            else return false;
        }

        public class WSDelegate:MonoBehaviour,WebSocketUnityDelegate
		{

			private WSConnector m_connector;

			public void OnWebSocketUnityOpen (string sender)
			{
                LoggerController.Instance.LogFormat(LoggerController.Module.Comm, "WebSocket connecting, checking ready state...");

                StartCoroutine(AssureReadyState());

			}

            private IEnumerator AssureReadyState() {
                int tries = 0;
                bool success = true;

                while(!m_connector.WebSocket.IsOpened()) {
                    yield return new WaitForSecondsRealtime(0.2f);
                    tries++;

                    if (tries==10) {
                        success = false;
                        break;
                    }
                        
                }

                if (success) {
                    LoggerController.Instance.LogFormat(LoggerController.Module.Comm, "WebSocket connected to {0}:{1}", m_connector.m_host, m_connector.m_port);

                    UnityMainThreadDispatcher.Instance.Enqueue(() => {
                        if (m_connector.m_client_connected != null)
                            m_connector.m_client_connected();

                        if (m_connector.m_connect_callback != null)
                            m_connector.m_connect_callback(true);
                    });

                } else {
                    LoggerController.Instance.LogError(LoggerController.Module.Comm, "WebSocket was unable to connect!");
                    m_connector.WebSocket.Close();
                    if (m_connector.m_connect_callback != null)
                        m_connector.m_connect_callback(false);
                }
            }

			// This event happens when the websocket is closed
			public void OnWebSocketUnityClose (string reason)
			{

                LoggerController.Instance.Log(LoggerController.Module.Comm, "WebSocket disconnected: " + reason);


            }

			// This event happens when the websocket received a message
			public void OnWebSocketUnityReceiveMessage (string message)
			{
				throw new NotImplementedException ();
			}

			public void OnWebSocketUnityReceiveDataOnMobile (string base64EncodedData)
			{
				throw new NotImplementedException ();
			}

			public void OnWebSocketUnityReceiveData (byte[] data)
			{	
				UnityMainThreadDispatcher.Instance.Enqueue (() => {


					string temp = System.Text.Encoding.UTF8.GetString (data);


					MMessage msg = m_connector.m_decoder.Decode (data);

                    if (msg.Code != CommandsBuilder.ECHO_CODE)
                        LoggerController.Instance.Log(LoggerController.Module.Comm,FormatUtils.GetLogTime() + " Message Received: " + msg);


                    m_connector.m_commands_manager.ExecuteCommand (msg);
				});
			}

			/// <summary>
			/// This method is required, and default value for error param does not solve the problem
			/// </summary>
			public void OnWebSocketUnityError ()
			{

                LoggerController.Instance.LogError(LoggerController.Module.Comm, "WebSocket Error : no error code");

            }

			public void OnWebSocketUnityError (string error)
			{

                LoggerController.Instance.LogError(LoggerController.Module.Comm, "WebSocket Error : " + error);

            }

			public WSConnector Connector {
				get {
					return m_connector;
				}
				set {
					m_connector = value;
					m_connector.WsDelegate = this;
				}
			}


        }


	}
}