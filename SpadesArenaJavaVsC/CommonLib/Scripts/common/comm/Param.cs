﻿namespace common.comm
{

	public abstract class Param  {
		
		public abstract string Value {
			get;
		}

	}
}