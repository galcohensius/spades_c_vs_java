using common.controllers;
using common.controllers.purchase;
using common.ims;
using common.ims.model;
using common.mes;
using common.models;

namespace common.comm
{
    public abstract class CommManager : MonoBehaviour, IPurchaseServer
    {
        public static CommManager Instance;

        protected PurchaseCompleted m_purchase_completed;

        // Password for message signing
        private byte[] Dp = new byte[] { 76, 101, 40, 93, 95, 56, 87, 119, 113, 56, 87, 81, 53, 65, 81, 94 };

        protected RestClient m_client;

        protected virtual void Awake()
        {
            if (Instance == null)
            {
                Instance = this;

                m_client = GetComponent<RestClient>();
                m_client.Dp = Dp;
            }
        }

        public abstract ModelParser GetModelParser();


        public void ValidatePurchase(string itemId, string transId, string paymentExtraData, PurchaseTrackingData purchaseTrackingData, PurchaseCompleted onComplete)
        {
            m_purchase_completed = onComplete;

            JSONNode requestData = new JSONObject();
            requestData["pId"] = itemId;
            requestData["tId"] = transId;
            requestData["exa"] = paymentExtraData;

            // IMS related
            requestData["mDa"] = purchaseTrackingData.imsMetaData;
            requestData["oMd"] = purchaseTrackingData.imsOriginMetaData;
            requestData["cMd"] = purchaseTrackingData.clientMetaData;
            requestData["aVe"] = new IMSModelParser().SerializeActionValue(purchaseTrackingData.imsActionValue);
            requestData["eDa"] = new JSONObject
            {
                ["pIx"] = purchaseTrackingData.popupIndex
            };

            m_client.SendRequest("purchase", requestData, OnPurchaseResponse, true);
        }

        private void OnPurchaseResponse(bool success, JSONNode responseData)
        {
            if (success)
            {

                // Cashier data - IMS (have higher priority)
                Cashier cashier = GetModelParser().ParseCashier(responseData["ims"]["bPe"]);

                // Cashier data - MES
                if (cashier == null)
                    cashier = GetModelParser().ParseCashierMES(responseData["pSr"]);

                JSONNode mesEvents = responseData["evs"];
                LoggerController.Instance.Log(LoggerController.Module.Comm, "MES JSON POST PURCHASE: " + mesEvents.ToString());
                MESBase.Instance?.ParseMESJson(mesEvents);

                JSONObject imsEvents = responseData["ims"]["evs"].AsObject;
                LoggerController.Instance.Log(LoggerController.Module.Comm, "IMS JSON POST PURCHASE: " + imsEvents.ToString());
                IMSController.Instance.ParseEvents(imsEvents);

                // Must update the purchaser since new packages might have been received
                PurchaseController.Instance.UpdatePurchaser();

                // Parse the good list
                IMSGoodsList iMSGoodsList = GetModelParser().ParseGoodList(responseData["rGs"].AsArray);


                int newCoinsBalance = responseData["cBe"].AsInt;

                m_purchase_completed(true, newCoinsBalance, iMSGoodsList, cashier);
            }
            else
            {

                LoggerController.Instance.LogError(LoggerController.Module.Comm, "Purchase error received:\n" + responseData);
                m_purchase_completed(false, 0, null, null);
            }
        }


        public string LoginToken
        {
            get
            {
                return m_client.Login_token;
            }

        }
    }
}
