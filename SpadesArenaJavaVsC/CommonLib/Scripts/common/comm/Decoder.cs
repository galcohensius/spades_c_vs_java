﻿using System;
using System.Text;

namespace common.comm
{

	public class Decoder {


		char	m_startSeperator;
		char	m_endSeperator;
		char	m_paramSeperator;


		public Decoder(char open, char close, char split)
		{
			m_startSeperator = open;
			m_endSeperator = close;
			m_paramSeperator = split;
		}

		UTF8Encoding utf8 = new UTF8Encoding();

		public MMessage Decode(byte[] data)
		{
			
			MMessage msg = new MMessage();

			string message_str = utf8.GetString (data);

			msg.Code = message_str.Substring(0,2);

			CompositeParam cp = new CompositeParam ();

			string message_body = message_str.Substring (2);//removing a leading code

			if (message_body != "")
			{
				decodeBlock (cp, message_body);

				cp.SetParamsInternal (cp.Params [0] as CompositeParam); // this is to extract the internal composite from composite param - as the recursive function works on composite param.

				msg.Params=cp.Params;

			}

			return msg;
		}

		private int decodeBlock(CompositeParam root, string data) {

			bool blockAdded = false;
			int i = 0;

			while(i<data.Length) {
				// Mark the start
				int start = i;

				char next;

				do {
					next = (char) data[i];
					i++;
				} while(next!=m_paramSeperator && next!=m_startSeperator && next!= m_endSeperator && i < data.Length);

				if (next == m_paramSeperator)
				{
					// Add a new param to the message and continue
					if (!blockAdded)
					{
						String value = data.Substring (start, i - 1 - start); 
						root.AddValueParam (value);
					}
					blockAdded = false;
				}
				else if (next == m_endSeperator)
				{
					// Add a new param to the message and return
					if (!blockAdded)
					{
						String value = data.Substring (start, i - 1 - start); 
						root.AddValueParam (value);
					}
					return i;
				}
				else if (next == m_startSeperator)
				{
					CompositeParam childRoot = new CompositeParam ();
					root.AddParam (childRoot);

					string data2 = data.Substring (i);
					int temp = decodeBlock (childRoot, data2);
					i += temp;
					blockAdded = true;
				}
			}
			return i;
			}

	}

}