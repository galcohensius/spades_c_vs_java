﻿using System;
using common.commands;
using common.controllers;
using common.utils;

namespace common.comm
{
    public class SocketConnectorV2 : Connector
    {
        Client m_client;

        public SocketConnectorV2(string host, int port, CommandsManager commands_manager) : base(host, port, commands_manager)
        {
        }

        public override void Connect(Action<bool> ConnectCallback = null)
        {
            m_connect_callback = ConnectCallback;

            Telepathy.Common.headerIO = new UShortHeader();
            m_client = new Client();

            m_client.MaxMessageSize = 65536;
            m_client.NoDelay = true;

            Telepathy.Logger.Log = FireLogOnMainThread;
            Telepathy.Logger.LogWarning = FireLogWarningOnMainThread;
            Telepathy.Logger.LogError = FireLogErrorOnMainThread;


            m_client.Connect(m_host, m_port);

            
            
        }

       
        public override void Disconnect()
        {
            m_client?.Disconnect();
        }

        public override bool IsConnected()
        {
            return m_client.Connected;
        }

        public override void Send(MMessage message)
        {
            try
            {
                byte[] messageBytes = m_encoder.Encode(message);

                m_client.Send(messageBytes);

                if (LocalDataController.Instance.Show_message_debug && message.Code != CommandsBuilder.ECHO_CODE)
                    LoggerController.Instance.Log(LoggerController.Module.Comm, FormatUtils.GetLogTime() + " Message Sent: " + message);

                m_client_sent_done?.Invoke();
            }
            catch (Exception e)
            {
                LoggerController.Instance.LogError(LoggerController.Module.Comm, "Cannot send to socket: " + e);
            }
        }

        public override void OnUpdate()
        {
            // client
            if (m_client!=null)
            {
                // show all new messages
                Telepathy.Message msg;
                while (m_client.GetNextMessage(out msg))
                {
                    switch (msg.eventType)
                    {
                        case Telepathy.EventType.Connected:
                            LoggerController.Instance.Log(LoggerController.Module.Comm, "Socket connected to: " + m_host);
                            m_connect_callback?.Invoke(true);
                            m_client_connected?.Invoke();

                            m_connect_callback = null; // Remove the connect callback if success
                            break;
                        case Telepathy.EventType.Data:

                            MMessage message = m_decoder.Decode(msg.data);

                            if (LocalDataController.Instance.Show_message_debug && message.Code != CommandsBuilder.ECHO_CODE)
                                LoggerController.Instance.Log(LoggerController.Module.Comm, FormatUtils.GetLogTime() + " Message Received: " + message);

                            m_commands_manager.ExecuteCommand(message);

                            break;
                        case Telepathy.EventType.Disconnected:
                            LoggerController.Instance.Log(LoggerController.Module.Comm, "Socket disconnected");

                            // Invoke the connect callback if not null. This happens if the socket cannot connect and the disconnected event is invoked immediatly
                            m_connect_callback?.Invoke(false);
                            break;
                    }
                }
            }
        }

        #region Logging
        private void FireLogOnMainThread(string msg)
        {
            UnityMainThreadDispatcher.Instance.Enqueue(() =>
            {
                LoggerController.Instance.Log(msg);
            });
        }

        private void FireLogWarningOnMainThread(string msg)
        {
            UnityMainThreadDispatcher.Instance.Enqueue(() =>
            {
                LoggerController.Instance.LogWarning(msg);
            });
        }

        private void FireLogErrorOnMainThread(string msg)
        {
            UnityMainThreadDispatcher.Instance.Enqueue(() =>
            {
                LoggerController.Instance.LogError(msg);
            });
        }

        #endregion
    }
}
