﻿using System.Collections.Generic;
using common.utils;

namespace common.comm
{

	public class CompositeParam : Param {

		List<Param>		m_params = new List<Param>();

		public override string Value {
			get {
				return "";
			}

		}

		public void SetParamsInternal(CompositeParam cp)
		{
			m_params = cp.Params;
		}

		public void AddValueParam(string value)
		{
			SimpleParam param = new SimpleParam (value);

			m_params.Add (param);
		}

        public void AddValueParam(int value)
        {
            SimpleParam param = new SimpleParam(value);

            m_params.Add(param);
        }

        public void AddParam(Param param)
		{
			m_params.Add (param);
		}

		public List<Param> Params {
			get {
				return m_params;
			}
			set {
				m_params = value;
			}
		}

		public override string ToString ()
		{
			return string.Format ("{{{0}}}",FormatUtils.FormatList (m_params));
		}
		
	}
}