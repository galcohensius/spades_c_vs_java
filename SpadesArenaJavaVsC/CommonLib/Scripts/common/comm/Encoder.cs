﻿using System.Collections.Generic;
using System.Text;

namespace common.comm
{

	public class Encoder
	{

		char	m_startSeperator;
		char	m_endSeperator;
		char	m_paramSeperator;

		UTF8Encoding utf8 = new UTF8Encoding ();


		public Encoder (char open, char close, char split)
		{
			m_startSeperator = open;
			m_endSeperator = close;
			m_paramSeperator = split;
		}

		public byte[] Encode (MMessage message)
		{
			string code = message.Code;

			string message_string = "";

			if (message.Params.Count > 0)
			{
				message_string = writeComposite (message.Params);
			}

			byte[] msg = utf8.GetBytes (code + message_string); 

			//byte[] result = ByteUtils.AddLength (msg);


			return msg;
		}

		private string writeComposite (List<Param> param)
		{
			// Write the block start delimiter
			string message = "";
			message += m_startSeperator;

			for (int i = 0; i < param.Count; i++)
			{
				Param p = param [i];

				if (p.GetType ().Equals (typeof(SimpleParam)))
				{
					SimpleParam p2 = param [i] as SimpleParam;
					message += p2.Value;
				}
				else
				{
					// Composite
					CompositeParam p3 = param [i] as CompositeParam;
					message += writeComposite (p3.Params);
				}

				if (param.Count - i > 1)//atleast another param afterwards - so add a space
					message += m_paramSeperator;
			}

			// Write the block end delimiter
			message += m_endSeperator;
			return message;
		}


	}
}