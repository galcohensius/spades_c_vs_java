using common.models;

namespace common.comm
{
    public static class BonusModelParser
    {
        public static BonusAwarded ParseBonusAwarded(JSONNode jsonNode)
        {
            BonusAwarded result = new BonusAwarded();

            result.Type = (Bonus.BonusTypes)jsonNode["bTe"].AsInt;

            result.GoodsList.Coins.Add(jsonNode["cGs"].AsInt);

            /*
            JSONArray array = jsonNode["Ava"].AsArray;
            int i = 0;
            foreach (JSONNode item in array)
            {
                result.AvaterItemId[i] = item.AsInt;
                i++;
            }

            array = jsonNode["vae"].AsArray;
            i = 0;
            foreach (JSONNode item in array)
            {
                //result.VoucherGoods[i] = //parse voucher
                i++;
            }
            /*  result.CoinsAwarded = jsonNode["vae"].AsInt;*/

            //
            result.Interval = jsonNode["nSIl"].AsInt;
            result.NextStep = jsonNode["nSp"].AsInt;
            result.GoodsIndex = jsonNode["gIx"].AsInt;
            result.FriendsFactor = jsonNode["fFr"].AsInt;

            return result;
        }
    }
}

