﻿namespace common
{

    public class DevModePanel : MonoBehaviour
    {

        // It calculates frames/second over each updateInterval,
        // so the display does not keep changing wildly.

        public float updateInterval = 0.5F;

        private float accum = 0; // FPS accumulated over the interval
        private int frames = 0; // Frames drawn over the interval
        private float timeleft; // Left time for current interval

        [SerializeField] TMP_Text m_mode_text = null;
        [SerializeField] TMP_Text m_fps_text = null;
        [SerializeField] TMP_Text m_qa_server_text = null;



        void Start()
        {
            timeleft = updateInterval;

            // Show the dev model only if on DEV or QA mode
            gameObject.SetActive(false);

#if QA_MODE 
            gameObject.SetActive(true);

            m_mode_text.text = "QA MODE";
            OnQAEnvChanged(StateController.Instance.GetQAEnvironment());
            StateController.Instance.OnQAEnvChanged += OnQAEnvChanged;

#elif DEV_MODE
            gameObject.SetActive(true);
            m_mode_text.text = "DEV MODE";
#endif

        }

        private void OnQAEnvChanged(int index)
        {
            m_qa_server_text.text = "ENV: QA-" + index;
        }


#if QA_MODE || DEV_MODE
        void Update()
        {
            timeleft -= Time.deltaTime;
            accum += Time.timeScale / Time.deltaTime;
            ++frames;

            // Interval ended - update GUI text and start new interval
            if (timeleft <= 0.0)
            {
                // display two fractional digits (f2 format)
                float fps = accum / frames;
                m_fps_text.text = System.String.Format("FPS: {0:F1}", fps); ;

                //	DebugConsole.Log(format,level);
                timeleft = updateInterval;
                accum = 0.0F;
                frames = 0;

            }
        }
#endif

    }
}
