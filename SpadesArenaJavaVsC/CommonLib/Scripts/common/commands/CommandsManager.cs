using System.Collections.Generic;
using common.comm;
using common.controllers;
using System;

namespace common.commands
{
    public class CommandsManager
    {

        public enum QueueTypes
        {
            None,
            Game,
            Other
        }

        Dictionary<string, Command> m_commands = new Dictionary<string, Command>();

        Dictionary<QueueTypes, Queue<MMessage>> m_commands_queue = new Dictionary<QueueTypes, Queue<MMessage>>()
        {
             { QueueTypes.None, new Queue<MMessage>() },
            { QueueTypes.Game, new Queue<MMessage>() },
            { QueueTypes.Other, new Queue<MMessage>() }
        };

        Dictionary<QueueTypes, bool> m_process_commands = new Dictionary<QueueTypes, bool>()
        {
            { QueueTypes.None,true },
            { QueueTypes.Game,true },
            { QueueTypes.Other,true }

        };


		
        public Dictionary<string, Command> Commands {
			get {
				return m_commands;
			}
		}
       


        public void ExecuteCommand(MMessage message)
        {
			//based on the message code it should be directed at the right command handler

            Command command;

            if (!m_commands.TryGetValue(message.Code, out command))
                LoggerController.Instance.LogError("Unknown command for Code: " + message.Code);
            else
            {

                

                if (command.QueueType == QueueTypes.None || m_process_commands[command.QueueType])
                {
                    m_process_commands[command.QueueType] = false;

                    command.Execute(message);
                }
                else
                    m_commands_queue[command.QueueType].Enqueue(message);
            }


        }


        

        public void RegisterCommand(string code, Command command)
        {
            m_commands.Add(code, command);
            command.CommandsManager = this;
        }

        public void OverrideCommand(string code, Command command)
        {
            if (m_commands.ContainsKey(code))
            {
                m_commands[code] = command;
                command.CommandsManager = this;
            }
            else
            {
                LoggerController.Instance.LogWarning("tried to override unregistered command. registering instead.");
                RegisterCommand(code, command);
            }
        }

        public void ClearCommandsQueues()
        {
            foreach(QueueTypes type in Enum.GetValues(typeof(QueueTypes))) {
                m_commands_queue[type].Clear();
                m_process_commands[type] = true;
            }

        }

        public void EnableCommandsQueue(QueueTypes queueType)
        {
#if UNITY_EDITOR
            LoggerController.Instance.Log("releasing queue: " + queueType);
#endif
            m_process_commands[queueType] = true;

            if (m_commands_queue[queueType].Count > 0)
            {
                ExecuteCommand(m_commands_queue[queueType].Dequeue());
#if UNITY_EDITOR
                if (m_commands_queue[queueType].Count > 0)
                    LoggerController.Instance.Log("there are " + m_commands_queue[queueType].Count + " commands waiting.");
#endif
            }
#if UNITY_EDITOR
            else
                LoggerController.Instance.Log("but queue was empty");
#endif
        }

    }
}