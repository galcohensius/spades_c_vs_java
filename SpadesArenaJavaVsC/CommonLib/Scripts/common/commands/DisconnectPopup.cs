﻿using System;

namespace common.views.popups
{
    public class DisconnectPopup : PopupBase
    {
        [SerializeField] TMP_Text m_title_text;
        [SerializeField] TMP_Text m_body_text;

        [SerializeField] Button m_button;

        public virtual void Init(string title, string message, string button_text, Action close_action)
        {
            m_title_text.text = title;
            m_body_text.text = message;
            m_button.GetComponentInChildren<TextMeshProUGUI>().text = button_text;

            if (close_action != null)    
                m_button.onClick.AddListener(()=> { close_action?.Invoke(); });
        }
    }
}