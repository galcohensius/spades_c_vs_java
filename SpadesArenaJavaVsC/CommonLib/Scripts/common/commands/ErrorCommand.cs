using common.comm;
using common.controllers;
using System;

namespace common.commands
{
    /// <summary>
    /// Errors command handler for all server error
    /// </summary>
    public class ErrorCommand : Command
    {

		Action<string> onErrorReceived;

		public ErrorCommand(Action<string> onErrorReceived) {
			this.onErrorReceived = onErrorReceived;
		}

        public override void Execute(MMessage message)
        {
           
            LoggerController.Instance.LogError("Error received from server: " + message.Code);

            onErrorReceived?.Invoke(message.Code);
        }

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;
    }
}