﻿using common.comm;

namespace common.commands
{

    public abstract class Command
    {

        protected CommandsManager commandsManager;

        public abstract void Execute(MMessage message);

        public CommandsManager CommandsManager { set => commandsManager = value; }

        public abstract CommandsManager.QueueTypes QueueType
        {
            get;
        }
    }

}