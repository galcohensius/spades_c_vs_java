using common.comm;
using common.facebook.models;
using common.models;
using System.Text;

namespace common.commands
{

    public abstract class CommandsBuilder : MonoBehaviour
    {

        public const string SEND_DATA_CODE = "C100"; // FAKEY NEWS
        public const string CONNECT_TO_TABLE_CODE = "C1";
        public const string GAME_CREADTED_CODE = "C2";
        public const string START_ROUND_CODE = "C3";
        public const string PLAY_CODE = "C4";
        public const string RECONNECT_CODE = "C8";
        public const string END_ROUND_CODE = "C5";
        public const string PLAYER_STATUS_CODE = "C6";
        public const string QUIT_CODE = "C7";
        public const string CHAT_CODE = "CA";
        public const string ROUND_HISTORY_CODE = "CB";
        public const string END_ROUND_AUTO_CODE = "CD";
        public const string DISCONNECT_CODE = "CF";
        public const string REMATCH_CODE = "CE";
        public const string SEARCH_AGAIN_CODE = "C9";
        public const string GET_LEADER_BOARD = "L1";
        public const string UPDATE_LEADER_BOARD = "L2";
        public const string UPDATE_USER_FRIENDS = "L3";
        public const string START_ARENA_STAGE = "SA"; // ArenaNew Fake

        public const string DEBUG_END_ROUND_CODE = "D2"; //cheat

        // Error codes
        public const string INTERNAL_SERVER_ERROR = "E1";
        public const string MESSAGE_FORMAT_ERROR = "E2";
        public const string BAD_PARAMS_ERROR = "E3";
        public const string UNKNOWN_COMMAND_ERROR = "E4";
        public const string NOT_LOGGED_IN_ERROR = "E5";
        public const string BAD_FLOW_ERROR = "E6";
        public const string SERVER_SHUTDOWN_ERROR = "E7";
        public const string BANNED_USER_ERROR = "E8";
        public const string S2S_ERROR = "E9";

        public static CommandsBuilder Instance;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public const string ECHO_CODE = "XX";

        protected UTF8Encoding utf8 = new UTF8Encoding();

        public MMessage BuildEchoCommand(string data)
        {

            MMessage m = new MMessage();

            m.Code = ECHO_CODE;

            m.Params.Add(new SimpleParam(data));

            return m;
        }
        /*
        public virtual MMessage BuildConnectTableCommand(int table_id, int max_players)
        {
            CommonUser user = GetUser();

            MMessage m = new MMessage();

            m.Code = CONNECT_TO_TABLE_CODE;

            SimpleParam sp1 = new SimpleParam(table_id.ToString()); // table id
            CompositeParam cp1 = new CompositeParam();
            cp1.AddValueParam(user.GetID().ToString());//user id
            AddAvatarNickname(cp1, user);

            if (user.Facebook_data != null)
                cp1.AddValueParam(user.Facebook_data.User_id);
            else
                cp1.AddValueParam("0");

            //OldAvatarModel av = user.GetAvatar ();

            //string avatar_data = av.GetGenderIndex () + "~" + av.GetHeadIndex () + "~" + av.GetFaceIndex () + "~" + av.GetHairFrontIndex () + "~" + av.GetHairColorIndex () + "~" + av.GetSkinIndex () + "~" + av.Base_Index+"~"+av.GemsCount;

            cp1.AddValueParam("");

            cp1.AddValueParam(user.GetLevel().ToString());

            SimpleParam sp2 = new SimpleParam(max_players.ToString()); // table id

            int win_ratio = 0;

            win_ratio = GetWinRatio(user);

            cp1.AddValueParam((win_ratio * 100).ToString());

            cp1.AddValueParam(user.GetCountry());
            cp1.AddValueParam("1");

            m.Params.Add(sp1);
            m.Params.Add(cp1);
            m.Params.Add(sp2);

            //SimpleParam sp3 = new SimpleParam(seating_mode); // table id
            //m.Params.Add(sp3);

            return m;
        }*/

        // ArenaNew - test
        public virtual MMessage BuildStartArenaMatch(int user_ID, string login_token, int table_id)
        {
            MMessage m = new MMessage();

            m.Code = START_ARENA_STAGE;

            SimpleParam userID = new SimpleParam(user_ID.ToString()); // user id
            SimpleParam loginToken = new SimpleParam(login_token.ToString());
            SimpleParam tableID = new SimpleParam(table_id.ToString()); // table id

            m.Params.Add(userID);
            m.Params.Add(loginToken);
            m.Params.Add(tableID);

            return m;
        }

        //new connect to table command - rummy/multi-spades style
        public virtual MMessage BuildConnectTableCommand(int user_ID, string login_token, int table_id)
        {
            MMessage m = new MMessage();

            m.Code = CONNECT_TO_TABLE_CODE;

            SimpleParam userID = new SimpleParam(user_ID.ToString()); // user id
            SimpleParam loginToken = new SimpleParam(login_token.ToString());
            SimpleParam tableID = new SimpleParam(table_id.ToString()); // table id

            m.Params.Add(userID);
            m.Params.Add(loginToken);
            m.Params.Add(tableID);

            return m;
        }
        protected abstract CommonUser GetUser();
        protected abstract void AddAvatarNickname(CompositeParam cp, CommonUser user);
        protected abstract int GetWinRatio(CommonUser user);


        public virtual MMessage BuildSendDataCommand(string data, string game_id, string code)
        {
            MMessage m = new MMessage();

            m.Code = SEND_DATA_CODE;
            m.Params.Add(new SimpleParam(game_id));
            m.Params.Add(new SimpleParam(code));
            m.Params.Add(new SimpleParam(data));

            return m;
        }


        public MMessage BuildRematchCommand(string game_id, bool rematch)
        {
            MMessage m = new MMessage();

            m.Code = REMATCH_CODE;

            m.Params.Add(new SimpleParam(game_id));
            m.Params.Add(new SimpleParam((rematch ? 1 : 0).ToString()));

            return m;
        }

        public virtual MMessage BuildQuitCommand(string game_id)
        {

            MMessage m = new MMessage();

            m.Code = QUIT_CODE;

            m.Params.Add(new SimpleParam(game_id));

            return m;
        }


        virtual public MMessage BuildReconnectCommand(string game_id, string userID)
        {

            MMessage m = new MMessage();

            m.Code = RECONNECT_CODE;

            m.Params.Add(new SimpleParam(game_id));
            m.Params.Add(new SimpleParam(userID));
            m.Params.Add(new SimpleParam(CommManager.Instance.LoginToken));

            return m;
        }

        #region Leaderboard server commands

        public virtual MMessage BuildGetLeaderboard(common.models.LeaderboardsModel.LeaderboardType type, int id, string user_id, string fb_id, string ip)
        {

            MMessage m = new MMessage();

            m.Code = GET_LEADER_BOARD;

            if (type == LeaderboardsModel.LeaderboardType.GlobalCurrent || type == LeaderboardsModel.LeaderboardType.GlobalPrevious)
            {
                m.Params.Add(new SimpleParam(1));
                m.Params.Add(new SimpleParam(id));
            }
            else if (type == LeaderboardsModel.LeaderboardType.Friends)
            {
                m.Params.Add(new SimpleParam(2));
                m.Params.Add(new SimpleParam("0"));
            }
            else
            {
                m.Params.Add(new SimpleParam(10));
                m.Params.Add(new SimpleParam("0"));
            }
            m.Params.Add(new SimpleParam(user_id));
            m.Params.Add(new SimpleParam(fb_id));
            m.Params.Add(new SimpleParam(ip));

            return m;
        }

        public virtual MMessage BuildUpdateLeaderboard(long user_id, string fb_id, string ip, int delta_score, string user_details, int leaderboardId)
        {

            MMessage m = new MMessage();

            m.Code = UPDATE_LEADER_BOARD;

            m.Params.Add(new SimpleParam(user_id));
            m.Params.Add(new SimpleParam(fb_id));
            m.Params.Add(new SimpleParam(ip));
            m.Params.Add(new SimpleParam(delta_score));
            if (user_details != null)
                m.Params.Add(new SimpleParam(user_details));
            else
                m.Params.Add(new SimpleParam(""));
            m.Params.Add(new SimpleParam(leaderboardId));
            return m;
        }


        public virtual MMessage BuildUpdateUserFriendsForLeaderboard(string fb_id, System.Collections.Generic.List<FacebookData> friends)
        {

            MMessage m = new MMessage();

            m.Code = UPDATE_USER_FRIENDS;
            CompositeParam cp = new CompositeParam();

            foreach (FacebookData friend in friends)
            {
                cp.AddValueParam(friend.User_id);
            }


            m.Params.Add(new SimpleParam(fb_id));
            m.Params.Add(cp);

            return m;
        }

        #endregion
    }
}
