using common.comm;
using System;

namespace common.commands
{

    public class EchoCommand : Command
    {

		Action answerDelegate;
		public EchoCommand(Action answerDelegate) {
			this.answerDelegate = answerDelegate;
		}

        public override void Execute(MMessage message)
        {
            if (answerDelegate != null)
                answerDelegate();

		}

        public override CommandsManager.QueueTypes QueueType => CommandsManager.QueueTypes.None;
    }
}