﻿using common.controllers;

namespace common
{

    public class ButtonsOverViewTabs : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler,IPointerClickHandler
    {

        private static Texture2D m_handCursor;

        [SerializeField] GameObject m_over_object = null;

        bool m_selectedTab;

        void Start()
        {
            // Load the hand cursor texture from Resources
            m_handCursor = Resources.Load("Sprites/HandCursor") as Texture2D;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            Cursor.SetCursor(m_handCursor, Vector2.zero, CursorMode.Auto);
            m_over_object.SetActive(true);
        }


        public void OnPointerExit(PointerEventData eventData)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            m_over_object.SetActive(false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);


            LoggerController.Instance.Log("OnPointerClick tab");
        }

    }
}