﻿namespace common.experiments {

    /// <summary>
    /// A specific experiment with its current user related value
    /// </summary>
	public class Experiment  {

       public Experiment(string id, int value)
        {
            Id = id;
            Value = value;
        }

        public string Id
        {
            get;
            set;
        }

        public int Value
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("[Experiment: id={0}, value={1}]", Id,Value);
        }
	}
	
}