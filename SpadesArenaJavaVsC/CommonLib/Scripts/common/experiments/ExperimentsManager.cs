﻿using System.Collections.Generic;
using System;
using common.controllers;

namespace common.experiments {

    /// <summary>
    /// Manages A/B experiments
    /// </summary>
    public class ExperimentsManager:MonoBehaviour {

        static ExperimentsManager instance;

        Dictionary<string, Experiment> experiments = new Dictionary<string, Experiment>();


        public static ExperimentsManager Instance
        {
            get
            {

                if (instance == null)
                {
                    instance = FindObjectOfType<ExperimentsManager>();

                }
                return instance;
            }
        }

        public void LoadExperiments(JSONNode experimentsJson) {

            foreach (JSONNode experimentJson in experimentsJson.AsArray)
            {
                string id = experimentJson["id"];
                int val = experimentJson["vVe"].AsInt;

				experiments[id] = new Experiment(id, val);

                LoggerController.Instance.Log("Experiment loaded: "+experiments[id]);
            }

        }

        /// <summary>
        /// Gets the value of a specific experiment.
        /// </summary>
        /// <returns>The experiment value</returns>
        /// <param name="expId">Experiment id</param>
        public int GetValue(string expId) {
            Experiment exp;
            if (experiments.TryGetValue(expId,out exp)) {
                return exp.Value;
            }
            return 1;
        }


        /// <summary>
        /// Run a specific action according to the experiment value
        /// </summary>
        /// <param name="expId">Experiment id</param>
        /// <param name="action1">Action to run if experiment value is 1</param>
        /// <param name="actions">More actions to run according to the experiment values</param>
        public void With(string expId,Action action1, params Action[] actions) {
            int val = GetValue(expId);

            if (val==1 || val > actions.Length+1) {
                action1();
            } else {
                actions[val-2]();
            }
        }


        /// <summary>
        /// Runs the action if the experiment value is the expected value
        /// </summary>
        /// <param name="expId">Experiment id</param>
        /// <param name="expectedVal">Expected value.</param>
        /// <param name="action">Action to run if value is as expected</param>
        public void RunIf(string expId, int expectedVal, Action action)
        {
            int val = GetValue(expId);

            if (val == expectedVal)
            {
                action();
            }
        }

		/// <summary>
		/// Overwrites the experiment value localy
		/// </summary>
		/// <param name="expId">Exp id</param>
		/// <param name="val">Value</param>
		public void OverrideValue(string expId,int val) {
			if (experiments.ContainsKey (expId))
				experiments [expId].Value = val;
			else
			{
				experiments.Add(expId,new Experiment(expId, val));
			}
				
		}
	}
}
