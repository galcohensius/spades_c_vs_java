﻿using System.Collections;

namespace common
{
    public class ButtonsOverView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private static Texture2D m_handCursor;

        bool disabled = false;
        RectTransform m_rect;

        [Header("Scaling")]
        [SerializeField] bool m_enable_scaling = false;
        [SerializeField] float m_over_scale_factor = 1.02f;
        [SerializeField] float m_click_scale_factor = 0.98f;

        [Header("Other")]
        [SerializeField] float m_clickProtectionDelay = 1.5f;

        bool m_duringDelay = false;
        [Tooltip("override for selectables that we want to show the curser even if uninteractable")]
        public bool showAlsoOnUninteractables = false;

        enum Click_State
        {
            Enter,
            Exit,
            ClickDown,
            ClickUp
        }

        Selectable m_selectable = null;

        void Start()
        {
            // Load the hand cursor texture from Resources
            // This member is static, so will be loaded only once
            if (m_handCursor == null)
                m_handCursor = Resources.Load("Sprites/HandCursor") as Texture2D;

            // If the game object has a selectable component (such as a button) we also need to check for interactable
            m_selectable = gameObject.GetComponent<Selectable>();

            m_rect = gameObject.GetComponent<RectTransform>();


            if (m_selectable is Button && m_clickProtectionDelay > 0)
            {
                (m_selectable as Button).onClick.AddListener(() =>
                {
                    if(m_selectable.IsActive())
                    {
                        m_selectable.interactable = false;
                        StartCoroutine(CancelDoubleClickProtection());
                    }
                });
            }

        }

        private IEnumerator CancelDoubleClickProtection()
        {
            m_duringDelay = true;

            yield return new WaitForSeconds(m_clickProtectionDelay);

            m_selectable.interactable = true;
            m_duringDelay = false;
        }

        private void OnDisable()
        {
            if (m_selectable != null && m_duringDelay)
            {
                // The protection coroutine will stop, so bring back interactable
                m_selectable.interactable = true;
                m_duringDelay = false;
            }
        }


        public void OnPointerEnter(PointerEventData eventData)
        {
            if (gameObject.activeInHierarchy && !Disabled)
            {
                // Check if the selectable (usually button) is interactable, or there is no selectable
                // component attached to the game object (for example in the lobby banner)
                if (m_selectable != null)
                {
                    if (m_selectable.interactable || showAlsoOnUninteractables)
                        HandlePointer(m_handCursor, Click_State.Enter);

                }
                else
                {
                    HandlePointer(m_handCursor, Click_State.Enter);
                }
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            HandlePointer(null, Click_State.Exit);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            HandlePointer(m_handCursor, Click_State.ClickDown);
        }

        public void OnPointerClickUp()
        {
            HandlePointer(m_handCursor, Click_State.ClickUp);
        }

        private void HandlePointer(Texture2D texture, Click_State click_state)
        {
            if (!disabled || click_state == Click_State.Exit)
            {
                Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);

                if (m_enable_scaling)
                {
                    if (click_state == Click_State.Enter)
                    {
                        m_rect.localScale = new Vector3(m_over_scale_factor, m_over_scale_factor, m_over_scale_factor);
                    }
                    else if (click_state == Click_State.Exit)
                    {
                        m_rect.localScale = new Vector3(1f, 1f, 1f);
                    }
                    else if (click_state == Click_State.ClickDown)
                    {
                        m_rect.localScale = new Vector3(m_click_scale_factor, m_click_scale_factor, m_click_scale_factor);
                    }
                    else if (click_state == Click_State.ClickUp)
                    {
                        m_rect.localScale = new Vector3(1f, 1f, 1f);
                    }
                }
            }

        }

        public static void ForceNormalCursor()
        {
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }

        public bool Disabled
        {
            get
            {
                return disabled;
            }
            set
            {
                disabled = value;
            }
        }
    }
}