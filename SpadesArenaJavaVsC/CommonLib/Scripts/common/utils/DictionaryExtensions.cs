﻿using System.Collections.Generic;

namespace common.utils
{
	public static class DictionaryExtensions
	{
		public static void Merge<TKey, TValue>(this Dictionary<TKey, TValue> me, Dictionary<TKey, TValue> merge)
		{
			foreach (var item in merge)
			{
				me[item.Key] = item.Value;
			}
		}

        //needed to be renamed
		public static void Merge<T>(this List<T> me, List<T> merge)
		{
			for (int i = 0; i < merge.Count; i++)
			{
				for (int j = 0; j < me.Count; j++) 
				{
					if (merge [i].Equals(me [j]))
					{
						me [j] = merge [i];
						break;
					}
				}
			}
		}
	}
}

