﻿using System.Linq;

namespace common.utils
{

    public static class TMPBidiHelper
    {

        public static void MakeRTL(TMP_Text tmpText)
        {

            if (string.IsNullOrEmpty(tmpText.text))
                return;

            int rtlChars = 0;

            foreach (char c in tmpText.text)
            {
                if (c.IsHebrew())
                    rtlChars++;
            }

            tmpText.isRightToLeftText = rtlChars >= 2;//tmpText.text.Length / 2;

        }

        public static void MakeRTL(TMP_InputField tmpInputField) {
            MakeRTL(tmpInputField.textComponent);
        }

		public static string MakeRTL(string text)
		{
			if (string.IsNullOrEmpty(text))
				return "";

			int rtlChars = 0;

			foreach (char c in text)
			{
				if (c.IsHebrew())
					rtlChars++;
			}

            if (rtlChars >= 2)//text.Length/2)
				return new string(text.ToCharArray().Reverse().ToArray());

			return text;
		}
    }

}