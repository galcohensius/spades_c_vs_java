﻿using System.Reflection;
using System;

namespace common.utils
{
    public static class Upcaster
    {
        /// <summary>
        /// copy all the fields and properties from source T to target U. U must be derived from T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public static void UpCast<T, U>(T source, U target) where U : T
        {
            if (!(target is T))
            {
                throw new Exception("target object does not derive from source!");
            }

            var type = typeof(T);
            //foreach (var sourceProperty in type.GetProperties()) //BindingFlags.Instance | BindingFlags.Public  | BindingFlags.NonPublic | BindingFlags.GetProperty
            //{
            //    var targetProperty = type.GetProperty(sourceProperty.Name);
            //    targetProperty.SetValue(target, sourceProperty.GetValue(source, null), null);
            //}
            foreach (var sourceField in type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)) //
            {
                var targetField = type.GetField(sourceField.Name);
                targetField.SetValue(target, sourceField.GetValue(source));
            }
        }
    }
}