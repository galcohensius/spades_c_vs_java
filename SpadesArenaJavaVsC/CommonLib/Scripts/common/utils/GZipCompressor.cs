﻿using System;
using System.IO;
using System.Text;

namespace common.utils
{
    public static class GZipCompressor
    {
        private const ushort GZIP_LEAD_BYTES = 0x8b1f;

        /// <summary>
        /// Compresses the string and adds the length as the first 4 bytes
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>Base64 representation of the compressed string</returns>
        public static string CompressStringWithLen(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            using (var memoryStream = new MemoryStream()) {
                
                using (GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                {
                    gZipStream.Write(buffer, 0, buffer.Length);
                }

                memoryStream.Position = 0;

                var compressedData = new byte[memoryStream.Length];
                memoryStream.Read(compressedData, 0, compressedData.Length);

                var gZipBuffer = new byte[compressedData.Length + 4];
                Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
                Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
                return Convert.ToBase64String(gZipBuffer);
            }


        }

        /// <summary>
        /// Decompresses a base64 string with the length as the first 4 bytes
        /// </summary>
        /// <returns>A decompressed string</returns>
        public static string DecompressStringWithLen(string compressedText)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedText);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);

                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        /// <summary>
        /// Decompresses a byte array (representing a compressed string) to a string
        /// </summary>
        /// <param name="compressedData">The compressed data as a byte array</param>
        /// <returns>a string with the decompressed data</returns>
        public static string DecompressBytes(byte[] compressedData)
        {
            using (var memoryStream = new MemoryStream(compressedData))
            {
                string result = null;

                memoryStream.Position = 0;
                using (GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    using (StreamReader reader = new StreamReader(gZipStream)) {
                        result = reader.ReadToEnd();
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Compresses the string and converts the result to base64
        /// </summary>
        /// <param name="data">The string data</param>
        /// <returns>Base64 representation of the compressed string as byte array</returns>
        public static string CompressString(string data) {
            using (var memoryStream = new MemoryStream())
            {
                using (GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                {
                    using(StreamWriter writer = new StreamWriter(gZipStream)) {
                        writer.Write(data);
                        writer.Flush();
                    }
                    memoryStream.Position = 0;

                    byte[] compressedData = new byte[memoryStream.Length];
                    memoryStream.Read(compressedData, 0, compressedData.Length);

                    return Convert.ToBase64String(compressedData);
                }
            }


        }

        public static bool IsGZipCompressedData(byte[] data)
        {
            if (data != null && data.Length >= 2)
            {
                // if the first 2 bytes of the array are theG ZIP signature then it is compressed data;
                return (BitConverter.ToUInt16(data, 0) == GZIP_LEAD_BYTES);
            }
            return false;
        }
    }
}