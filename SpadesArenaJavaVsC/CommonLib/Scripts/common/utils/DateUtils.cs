﻿using common.controllers;
using System;
using System.Collections;

namespace common.utils
{

    public static class DateUtils
    {

        /// <summary>
        /// Converts DateTime to Unix timestamp
        /// </summary>
        public static int DateToUnixTimestamp(DateTime dateTime)
        {
            TimeSpan t = dateTime - new DateTime(1970, 1, 1);
            return (int)t.TotalSeconds;
        }

        /// <summary>
        /// Calculate the total seconds from now to the unix timestamp
        /// </summary>
        /// <returns>The seconds.</returns>
        /// <param name="unixTimeStamp">Future unix timestamp</param>
        public static long TotalSeconds(int unixTimeStamp)
        {
            return unixTimeStamp - UnixTimestamp();
        }

        /// <summary>
        /// Returns the number of seconds since 1/1/1970
        /// </summary>
        public static int UnixTimestamp()
        {
            return (int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static DateTime UnixTimestampToDate(int unixTimestamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimestamp).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        /// Returns the number of milliseconds since 1/1/1970
        /// </summary>
        public static long JavaTimestamp()
        {
            return (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
        }

        public static string TimespanToDateMission(TimeSpan ts)
        {
            if (ts.Days < 1)
                return string.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
            return string.Format("{0}D {1:00}:{2:00}:{3:00}", ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
        }

        public static string TimespanToDate(TimeSpan ts)
        {
            if (ts.Days < 1)
                return string.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
            else if (ts.Days == 1)
                return string.Format("{0}Day {1:00}:{2:00}:{3:00}", ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
            else
                return string.Format("{0}Days {1:00}:{2:00}:{3:00}", ts.Days, ts.Hours, ts.Minutes, ts.Seconds);
        }

        public static string TimespanTo48HoursDate(TimeSpan ts)
        {
            if (ts.TotalDays <= 2)
                return string.Format("{0:00}:{1:00}:{2:00}", (int)ts.TotalHours, ts.Minutes, ts.Seconds);
            else
                return string.Format("{0} Days", ts.Days);
        }



        public static string FormatDuration(long duration_sec)
        {
            TimeSpan ts = TimeSpan.FromSeconds(duration_sec + 1);
            if (ts.Days > 0)
                return string.Format("{0:0.#}d", Mathf.Round((float)ts.TotalDays));
            if (ts.Hours > 0)
                return string.Format("{0:0.#}h", Mathf.Round((float)ts.TotalHours));

            return string.Format("{0}m", Mathf.Round((float)ts.TotalMinutes));
        }


        public static long GetUserTimeStampViaDaysFromReg(int daysFromReg)
        {
            TimeSpan userTimeSpan = new TimeSpan(daysFromReg, 0, 0, 0);
            int userSecondsFromReg = (int)userTimeSpan.TotalSeconds;

            long userTimeStamp = UnixTimestamp() - userSecondsFromReg;

            LoggerController.Instance.Log("User Time Stamp = " + userTimeStamp);

            return userTimeStamp;
        }

        public static string ConvertSecondsToDate(long seconds)
        {
            TimeSpan t = TimeSpan.FromSeconds(seconds);
            //return t.ToString(@".hh\:mm\:ss");
            if (t.TotalHours <= 9)
                return ($"0{((int)t.TotalHours)}:{t:mm}:{t:ss}");

            return ($"{((int)t.TotalHours)}:{t:mm}:{t:ss}");
        }

        public static string ConvertSecondsToDateOrDays(long seconds)
        {
            TimeSpan t = TimeSpan.FromSeconds(seconds);
            //return t.ToString(@".hh\:mm\:ss");

            if (t.TotalHours <= 9)
                return ($"0{((int)t.TotalHours)}:{t:mm}:{t:ss}");

            if (t.TotalHours >= 24 && t.TotalHours < 48)
                return ($"{((int)t.TotalDays)} Day");

            if (t.TotalHours >= 48)
                return ($"{((int)t.TotalDays)} Days");

            return ($"{((int)t.TotalHours)}:{t:mm}:{t:ss}");
        }

        public static long TotalSeconds(DateTime timeStamp)
        {
            return (long)(timeStamp - DateTime.Now).TotalSeconds;
        }

        //use these two for debugging purposes
        public static string ShowNowLongDate()
        {
            return DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff");
        }

        public static string ShowNowTime()
        {
            return DateTime.UtcNow.ToString("HH:mm:ss.fff");
        }

        public static IEnumerator RunTimer(TimeSpan ts, TMP_Text timer, string prefix, Action timer_done)
        {
            float startTime = Time.realtimeSinceStartup;

            while (ts.TotalSeconds >= 0)
            {

                timer.text = prefix + DateUtils.TimespanToDate(ts);

                yield return new WaitForSecondsRealtime(1f);

                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }

            if (timer_done != null)
                timer_done();

        }

        /// <summary>
        /// like RunTimer but uses the 48 hours format instead
        /// </summary>
        /// <param name="ts">total time span</param>
        /// <param name="timer">text to update</param>
        /// <param name="prefix">set perfix</param>
        /// <param name="timer_done">optional callback when timer runs out</param>
        /// <returns></returns>
        public static IEnumerator RunTimer48Format(TimeSpan ts, TMP_Text timer, string prefix = "", Action timer_done = null)
        {
            float startTime = Time.realtimeSinceStartup;

            if (ts.TotalDays > 2)
            {
                timer.text = prefix + DateUtils.TimespanTo48HoursDate(ts);
                TimeSpan extraSpan = ts.Subtract(new TimeSpan(48, 0, 0));
                ts = new TimeSpan(48, 0, 0);

                yield return new WaitForSecondsRealtime((float)extraSpan.TotalSeconds);
            }

            while (ts.TotalSeconds >= 0)
            {

                timer.text = prefix + DateUtils.TimespanTo48HoursDate(ts);

                yield return new WaitForSecondsRealtime(1f);

                ts = ts.Subtract(TimeSpan.FromSeconds(Time.realtimeSinceStartup - startTime));
                startTime = Time.realtimeSinceStartup;
            }

            if (timer_done != null)
                timer_done();
        }

        public static IEnumerator DelayAction(TimeSpan timeSpan, Action action)
        {
            yield return new WaitForSecondsRealtime((float)timeSpan.TotalSeconds);

            action?.Invoke();
        }

    }
}

