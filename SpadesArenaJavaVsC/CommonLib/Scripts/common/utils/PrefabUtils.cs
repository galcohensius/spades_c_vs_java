﻿using System.Collections.Generic;

namespace common.utils
{
    public static class PrefabUtils
    {

        // Returns the path in the game hierarchy of a game object
        public static string GetGameObjectPath(GameObject obj)
        {
            string path = "/" + obj.name;
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = "/" + obj.name + path;
            }
            return path;
        }

        public static void ResetSortingOrderToOriginal(Transform parentGameObject, string name, int sortOffset)
        {
            // ... Do something with the parent before the children ...
            Canvas tempCanvas = parentGameObject.gameObject.GetComponent<Canvas>();
            ParticleSystemRenderer tempParticle = parentGameObject.gameObject.GetComponent<ParticleSystemRenderer>();
            apPortrait tempApPortrait = parentGameObject.gameObject.GetComponent<apPortrait>();
            if (tempCanvas != null)
            {
                if (tempCanvas.gameObject.name == name)
                    tempCanvas.sortingOrder = sortOffset;
            }

            else if (tempParticle != null)
            {
                if (tempParticle.gameObject.name == name)
                    tempParticle.sortingOrder += sortOffset;
            }

            else if (tempApPortrait != null)
            {
                if (tempApPortrait.gameObject.name == name)
                    tempApPortrait.SetSortingOrder(sortOffset);
            }
            //Debug.Log(parentGameObject.name);

            foreach (Transform childTransform in parentGameObject)
            {
                Transform childGameObject = childTransform;
                ResetSortingOrderToOriginal(childGameObject, name, sortOffset); // Recursion!
            }
        }

        public static void SetExactSortingOffset(Transform parentGameObject, int sortingLayer)
        {
            // ... Do something with the parent before the children ...
            Canvas tempCanvas = parentGameObject.gameObject.GetComponent<Canvas>();
            ParticleSystemRenderer tempParticle = parentGameObject.gameObject.GetComponent<ParticleSystemRenderer>();
            apPortrait tempApPortrait = parentGameObject.gameObject.GetComponent<apPortrait>();
            if (tempCanvas != null)
            {
                tempCanvas.sortingOrder = sortingLayer;
            }

            else if (tempParticle != null)
            {
                tempParticle.sortingOrder = sortingLayer;
            }

            else if (tempApPortrait != null)
            {
                tempApPortrait.SetSortingOrder(sortingLayer);
            }
            //Debug.Log(parentGameObject.name);

            foreach (Transform childTransform in parentGameObject)
            {
                Transform childGameObject = childTransform;
                SetExactSortingOffset(childGameObject, sortingLayer); // Recursion!
            }
        }

        // This goes through all of the hierarchy under the parent gameobject and adds the sorting order to every sorting layer
        public static void SetSortingOffset(Transform parentGameObject, int sortOffset, bool withLimitation = true)
        {
            // ... Do something with the parent before the children ...
            Canvas tempCanvas = parentGameObject.gameObject.GetComponent<Canvas>();
            ParticleSystemRenderer tempParticle = parentGameObject.gameObject.GetComponent<ParticleSystemRenderer>();
            apPortrait tempApPortrait = parentGameObject.gameObject.GetComponent<apPortrait>();
            if (tempCanvas != null)
            {
                if (!withLimitation || (withLimitation && tempCanvas.sortingOrder <= 100))
                    tempCanvas.sortingOrder += sortOffset;
            }

            else if (tempParticle != null)
            {
                if (!withLimitation || (withLimitation && tempParticle.sortingOrder <= 100))
                    tempParticle.sortingOrder += sortOffset;
            }

            else if (tempApPortrait != null)
            {
                int currSort = tempApPortrait.GetSortingOrder();
                if (!withLimitation || (withLimitation && tempApPortrait.GetSortingOrder() <= 100))
                {
                    tempApPortrait.SetSortingOrder(currSort + sortOffset);
                }
            }
            //Debug.Log(parentGameObject.name);

            foreach (Transform childTransform in parentGameObject)
            {
                Transform childGameObject = childTransform;
                SetSortingOffset(childGameObject, sortOffset); // Recursion!
            }
        }

        //the difference between the next two methods is that the first one is set relatively to the current lowers layer while the second is relative
        //to the highest layer. while both can/should flatten down the layes down the same, this is not the same

        // This goes through all of the hierarchy under the parent gameobject and sets the sorting order to every sorting layer to the minimum order while keeping the 
        //relative offsets between the children in the hierarchy
        public static void SetMinimumSortingOrder(Transform parentGameObject, int minimumLayerOrder)
        {
            List<Canvas> childCanvases = new List<Canvas>();
            parentGameObject.GetComponentsInChildren<Canvas>(true, childCanvases);
            List<ParticleSystemRenderer> childParticleSystemRenderers = new List<ParticleSystemRenderer>();
            parentGameObject.GetComponentsInChildren<ParticleSystemRenderer>(true, childParticleSystemRenderers);
            List<apPortrait> childApsPortraits = new List<apPortrait>();
            parentGameObject.GetComponentsInChildren<apPortrait>(true, childApsPortraits);

            int currentMinimumLayer = 70000;
            foreach (Canvas canvas in childCanvases)
            {
                if (canvas.sortingOrder < currentMinimumLayer && canvas.gameObject.activeInHierarchy)
                    currentMinimumLayer = canvas.sortingOrder;
            }
            foreach (ParticleSystemRenderer particleSystemRenderer in childParticleSystemRenderers)
            {
                if (particleSystemRenderer.sortingOrder < currentMinimumLayer && particleSystemRenderer.gameObject.activeInHierarchy)
                    currentMinimumLayer = particleSystemRenderer.sortingOrder;
            }
            foreach (apPortrait apPortrait in childApsPortraits)
            {
                int apLayer = apPortrait.GetSortingOrder();
                if (apLayer < currentMinimumLayer && apPortrait.gameObject.activeInHierarchy)
                    currentMinimumLayer = apLayer;
            }

            int offset = currentMinimumLayer - minimumLayerOrder;

            foreach (Canvas canvas in childCanvases)
            {
                canvas.sortingOrder -= offset;
            }
            foreach (ParticleSystemRenderer particleSystemRenderer in childParticleSystemRenderers)
            {
                particleSystemRenderer.sortingOrder -= offset;
            }
            foreach (apPortrait apPortrait in childApsPortraits)
            {
                apPortrait.SetSortingOrder(apPortrait.GetSortingOrder() - offset);
            }
        }

        // This goes through all of the hierarchy under the parent gameobject and sets the sorting order to every sorting layer under set maximum order while keeping the 
        //relative offsets between the children in the hierarchy
        public static void SetMaximumSortingOrder(Transform parentGameObject, int maximumLayerOrder)
        {
            List<Canvas> childCanvases = new List<Canvas>();
            parentGameObject.GetComponentsInChildren<Canvas>(true, childCanvases);
            List<ParticleSystemRenderer> childParticleSystemRenderers = new List<ParticleSystemRenderer>();
            parentGameObject.GetComponentsInChildren<ParticleSystemRenderer>(true, childParticleSystemRenderers);
            List<apPortrait> childApsPortraits = new List<apPortrait>();
            parentGameObject.GetComponentsInChildren<apPortrait>(true, childApsPortraits);

            int currentMaximumLayer = -70000;
            foreach (Canvas canvas in childCanvases)
            {
                if (canvas.sortingOrder > currentMaximumLayer && canvas.gameObject.activeInHierarchy)
                    currentMaximumLayer = canvas.sortingOrder;
            }
            foreach (ParticleSystemRenderer particleSystemRenderer in childParticleSystemRenderers)
            {
                if (particleSystemRenderer.sortingOrder > currentMaximumLayer && particleSystemRenderer.gameObject.activeInHierarchy)
                    currentMaximumLayer = particleSystemRenderer.sortingOrder;
            }
            foreach (apPortrait apPortrait in childApsPortraits)
            {
                int apLayer = apPortrait.GetSortingOrder();
                if (apLayer > currentMaximumLayer && apPortrait.gameObject.activeInHierarchy)
                    currentMaximumLayer = apLayer;
            }

            int offset = currentMaximumLayer - maximumLayerOrder;

            foreach (Canvas canvas in childCanvases)
            {
                canvas.sortingOrder -= offset;
            }
            foreach (ParticleSystemRenderer particleSystemRenderer in childParticleSystemRenderers)
            {
                particleSystemRenderer.sortingOrder -= offset;
            }
            foreach (apPortrait apPortrait in childApsPortraits)
            {
                apPortrait.SetSortingOrder(apPortrait.GetSortingOrder() - offset);
            }
        }


        // This goes through all of the hierarchy under the parent gameobject and disables the animations
        public static void DisableAnimations(Transform parentGameObject)
        {
            // ... Do something with the parent before the children ...

            ParticleSystemRenderer tempParticle = parentGameObject.gameObject.GetComponent<ParticleSystemRenderer>();
            ParticleSystem particleSystem = parentGameObject.gameObject.GetComponent<ParticleSystem>();
            Animator animator = parentGameObject.gameObject.GetComponent<Animator>();

            // Debug.Log(parentGameObject.name);
            if (animator != null)
                animator.enabled = false;

            if (particleSystem != null)
                particleSystem.gameObject.SetActive(false);

            if (tempParticle != null)
                tempParticle.gameObject.SetActive(false);

            foreach (Transform childTransform in parentGameObject)
            {
                Transform childGameObject = childTransform;
                DisableAnimations(childGameObject); // Recursion!
            }
        }
    }
}


