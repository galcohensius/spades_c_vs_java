using System;
using System.Collections;
using System.Text;

namespace common.utils
{
    public static class FormatUtils
    {
        /// <summary>
        /// Formats the price.
        /// If the number if 100000 or above, the K characater is used (100000=>100K)
        /// If the number if 1000000 or above, the M characater is used (1200000=>1.2M)
        /// With tousands comma separator.
        /// </summary>
        /// <param name="price">Price as a number</param>
        public static string FormatPrice(int? price)
        {
            if (price == null)
                return "";
            if (price == 0)
                return "0";
            if (price >= 1000000)
            {
                return string.Format("{0:#,#,,.##M}", price);
            }
            else if (price >= 100000)
            {
                return string.Format("{0:#,#,K}", price);
            }
            else
            {
                return string.Format("{0:#,#}", price);
            }
        }

        /// <summary>
        /// Formats the price.
        /// If the number if 10000 or above, the K characater is used (10000=>10K)
        /// If the number if 1000000 or above, the M characater is used (1200000=>1.2M)
        /// No tousands comma separator.
        /// </summary>
        public static string FormatBuyIn(int? price)
        {
            if (price == null)
                return "";
            if (price == 0)
                return "0";
            if (price >= 1000000)
            {
                return string.Format("{0:##,,.##M}", price);
            }
            else if (price >= 10000)
            {
                return string.Format("{0:#,.##K}", price);
            }
            else
            {
                return string.Format("{0:##}", price);
            }
        }
        /// <summary>
        /// Add tousands comma separator.
        /// </summary>
        public static string FormatBalance(long price)
        {
            return string.Format("{0:#,##0}", price);
        }

        /// <summary>
        /// Add tousands comma separator and adds TMPro sprite.
        /// </summary>
        public static string FormatBalanceWithCoinSprite(long price, int spriteIndex = 1, bool toTheLeft = true)
        {
            if (toTheLeft)
                return string.Format("<sprite={0}>", spriteIndex) + FormatBalance(price);
            else
                return FormatBalance(price) + string.Format("<sprite={0}>", spriteIndex);
        }

        /// <summary>
        /// Formats a contest jackpot
        /// </summary>
        public static string FormatJackpot(double amount)
        {
            if (amount >= 100000000)
                return string.Format("{0:#,##0}", amount);
            return string.Format("{0:#,##0.00}", amount);
        }

        /// <summary>
        /// Convert the application version of the form major.minor.build to an int 
        /// </summary>
        /// <returns>The to version code.</returns>
        /// <param name="version">Version string</param>
        public static int VersionToVersionCode(string version)
        {
            string[] parts = version.Split('.');
            int result = 0;
            for (int i = 0; i < parts.Length; i++)
            {
                int verPart = int.Parse(parts[i]);
                switch (i)
                {
                    case 0:
                        result = verPart * 1000000;
                        break;
                    case 1:
                        result += verPart * 1000;
                        break;
                    case 2:
                        result += verPart;
                        break;
                }
            }
//            LoggerController.Instance.Log("version result is: " + result);
            return result;
        }

        /// <summary>
        /// Convert the application version code to the form major.minor.build 
        /// </summary>
        /// <returns>The to versio </returns>
        /// <param name="version">Version code</param>
        public static string VersionCodeToVersion(int version_code)
        {

            int major_version = version_code / 1000000;

            version_code -= major_version * 1000000;

            int minor_version = version_code / 1000;

            version_code -= minor_version * 1000;

            int build_number = version_code;

            string result = string.Format("{0}.{1}.{2}", major_version, minor_version, build_number);
//            LoggerController.Instance.Log("version code is: " + result);
            return result;
        }

        /// <summary>
        /// Formats a list of items using the specified seperator.
        /// </summary>
        public static string FormatList(IEnumerable list, string seperator = ",")
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in list)
            {
                sb.Append(item);
                sb.Append(seperator);
            }
            if (sb.Length > 1)
                sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }

        /// <summary>
        /// Formats a dictionary
        /// </summary>
        public static string FormatDictionary(IDictionary dict)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var key in dict.Keys)
            {
                sb.AppendFormat("{0}={1},", key, dict[key]);
            }
            if (sb.Length > 1)
                sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }


//        public static string FormatFacebookName(FacebookData fb_data)
//        {
//            string name = string.Format("{0} {1}", fb_data.First_name, fb_data.Last_name);
//
//            return name;
//        }


        public static string GetLogTime()
        {
            return DateTime.Now.ToString("H:mm:ss.ff");
        }




        public static string ConvertJSONChars(string data)
        {
            string result = "";

            char[] char_arr;

            char_arr = data.ToCharArray();

            for (int i = 0; i < char_arr.Length; i++)
            {
                if (char_arr[i] == Convert.ToChar("("))
                    char_arr[i] = Convert.ToChar("{");

                if (char_arr[i] == Convert.ToChar(")"))
                    char_arr[i] = Convert.ToChar("}");

                if (char_arr[i] == Convert.ToChar("~"))
                    char_arr[i] = Convert.ToChar(",");


                result += char_arr[i].ToString();
            }


            return result;


        }

    }
}

