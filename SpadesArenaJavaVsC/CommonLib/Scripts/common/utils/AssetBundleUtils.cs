﻿namespace common.utils
{
    public static class AssetBundleUtils
    {
        public static void SetTextForTransform(Transform trns, string value)
        {
            if (trns == null)
                return;

            if (trns.GetComponent<Text>() != null)
                trns.GetComponent<Text>().text = value;

            else if (trns.GetComponent<TMP_Text>() != null)
                trns.GetComponent<TMP_Text>().text = value;
        }
    }
}
