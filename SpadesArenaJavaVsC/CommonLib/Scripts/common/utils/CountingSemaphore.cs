﻿using System;


namespace common.utils
{
	public class CountingSemaphore 
	{
		int totalPermits, counter;
		Action onRelease;

		public CountingSemaphore(int totalPermits, Action onRelease)
		{
			this.totalPermits = totalPermits;
			this.onRelease = onRelease;
		}

        public void Post()
		{
			if (++counter == totalPermits)
				onRelease?.Invoke();
		}

	}
}
