﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;


namespace common.utils
{
    public static class CSVReader
    {

        /// Read a CSV file stored in the resource directory.
        /// The first line must contain the headers, and the values should match the generic type T.
        public static List<string[]> ReadFile<T>(string filePath)
        {
            if (!filePath.Contains(".csv"))
            {
                filePath = filePath + ".csv";
            }
            List<string[]> result = new List<string[]>();
            char[] separators = { ',' };
            StreamReader sr = new StreamReader(filePath);
            string line;

            while ((line = sr.ReadLine()) != null)
            {
                string[] read = line.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                result.Add(read);
            }

            // Remove the header line
            result.RemoveAt(0);

            return result;
        }
    }
}
