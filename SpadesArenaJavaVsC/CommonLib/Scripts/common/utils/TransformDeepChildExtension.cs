﻿using System.Collections.Generic;

namespace common.utils
{
	public static class TransformDeepChildExtension
	{
		//Breadth-first search
		public static Transform FindDeepChild (this Transform aParent, string aName)
		{
			var result = aParent.Find (aName);
			if (result != null)
				return result;
			foreach (Transform child in aParent) {
				result = child.FindDeepChild (aName);
				if (result != null)
					return result;
			}
			return null;
		}

        public static bool FindDeepChildsContains(this Transform aParent, string aNameContains, List<Transform> foundChildren)
        {
            if (foundChildren == null)
                return false;
            bool found = false;
            foreach (Transform child in aParent.transform)
            {
                if (child.name.Contains(aNameContains))
                {
                    foundChildren.Add(child);
                    found = true;
                }
            }
            foreach (Transform child in aParent)
            {
                bool curChildFound = child.FindDeepChildsContains(aNameContains, foundChildren);
                if (curChildFound)
                    found = true;
            }
            return found;
        }



        public static List<Transform> FindDeepChildren(this Transform aParent, string aName)
        {
            List<Transform> result = new List<Transform>();

            foreach (Transform child in aParent)
            {
                if (child.name == aName)
                    result.Add(child);

                result.AddRange(child.FindDeepChildren(aName));
            }

            return result;
        }


        public static TMP_Text FindDeepChildTmp(this Transform aParent)
        {
            var result = aParent.GetComponentInChildren<TMP_Text>();
            if (result != null)
                return result;
            foreach (Transform child in aParent)
            {
                result = child.FindDeepChildTmp();
                if (result != null)
                    return result;
            }
            return null;
        }
    }
}