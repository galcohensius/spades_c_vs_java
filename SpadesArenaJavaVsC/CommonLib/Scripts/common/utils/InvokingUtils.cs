﻿using System;
using System.Collections;

namespace common.utils
{
    public static class InvokingUtils
    {
        /// <summary>
        /// invoke an action after a set delay in frames or seconds.
        /// </summary>
        /// <param name="inFrames">true = frame count, false = seconds</param>
        /// <param name="amount">amount of frames (casted to int) or amount of seconds</param>
        /// <param name="action">the action to invoke. can be annonymous function instead of parameterless delegate</param>
        /// <param name="invokingMonoBehaviour">the active MonoBehaviour that do the invokation</param>
        /// /// <param name="realTime">if delaying in seconds use real time or scaled time. default is unscaled time</param>
        public static void InvokeActionWithDelay(bool inFrames, float amount, Action action, MonoBehaviour invokingMonoBehaviour, bool realTime = true)
        {
            if (inFrames)
                invokingMonoBehaviour.StartCoroutine(InvokeActionWithFrameDelay((int)amount, action));
            else
                invokingMonoBehaviour.StartCoroutine(InvokeActionWithTimeDelay(amount, action, realTime));
        }

        private static IEnumerator InvokeActionWithFrameDelay(int numOfFrames, Action action)
        {
            if (numOfFrames < 0)
                throw new Exception("tried to invoke action with negative frame delay");

            for (int i = 0; i < numOfFrames; i++)
                yield return null;
            action?.Invoke();
        }

        private static IEnumerator InvokeActionWithTimeDelay(float timeDelay, Action action, bool realTime)
        {
            if (timeDelay < 0)
                throw new Exception("tried to invoke action with negative time delay");

            if (realTime)
                yield return new WaitForSecondsRealtime(timeDelay);
            else
                yield return new WaitForSeconds(timeDelay);

            action?.Invoke();
        }

        /// <summary>
        /// invoke an action on the next frame
        /// </summary>
        /// <param name="action">the action to invoke. can be annonymous function instead of parameterless delegate</param>
        /// <param name="invokingMonoBehaviour">the active MonoBehaviour that do the invokation. if none supplied will try to use the EventSystem</param>
        public static void InvokeNextFrame(Action action, MonoBehaviour invokingMonoBehaviour = null)
        {
            if (invokingMonoBehaviour == null)
                invokingMonoBehaviour = EventSystem.current;
            invokingMonoBehaviour.StartCoroutine(InvokeActionWithFrameDelay(1, action));
        }

        /// <summary>
        /// invoke an action after set seconds
        /// </summary>
        /// <param name="seconds">time delay</param>
        /// <param name="action">action</param>
        /// <param name="realTime">default = true</param>
        /// <param name="invokingMonoBehaviour">the active MonoBehaviour that do the invokation. if none supplied will try to use the EventSystem</param>
        public static void InvokeInSeconds(float seconds, Action action, bool realTime = true, MonoBehaviour invokingMonoBehaviour = null)
        {
            if (invokingMonoBehaviour == null)
                invokingMonoBehaviour = EventSystem.current;
            invokingMonoBehaviour.StartCoroutine(InvokeActionWithTimeDelay(seconds, action, realTime));
        }
    }
}