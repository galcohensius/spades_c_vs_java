﻿using System;
using System.IO;

namespace common.utils
{
		
	public static class ByteUtils {

		static byte[] EncodeToBytes(string str)
		{
			byte[] bytes = new byte[str.Length * sizeof(char)];
			System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		public static byte[] Combine(byte[] first, byte[] second)
		{
			byte[] ret = new byte[first.Length + second.Length];
			System.Buffer.BlockCopy(first, 0, ret, 0, first.Length);
			System.Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
			return ret;
		}

		public static byte[] SubByteArray(byte[] data, int start, int len)
		{
			byte[] result = new byte[len];
			System.Buffer.BlockCopy(data, start, result, 0, len);
			return result;
		}

		static public string GetString(byte[] bytes)
		{
			char[] chars = new char[bytes.Length / sizeof(char)];
			System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
			return new string(chars);
		}

		public static byte[] AddLength(byte[] data)
		{
			ushort len =(ushort)(data.Length);

			byte[] byte_len = BitConverter.GetBytes (len);

			if(BitConverter.IsLittleEndian)
				Array.Reverse (byte_len);
			
			byte[] result = ByteUtils.Combine (byte_len, data);

			return result;
		}

		public static ushort GetLength(byte[] data)
		{
			if (data.Length < 3)
				return 0;

			byte[] len_data = new byte[2];

			Buffer.BlockCopy (data, 0, len_data, 0, 2);

			if (BitConverter.IsLittleEndian)
				Array.Reverse (len_data);

			ushort len = BitConverter.ToUInt16 (len_data, 0);

			return len;
		}

		/// <summary>
		/// Reads the length represetned by 2 bytes (big endian) 
		/// </summary>
		public static ushort ReadLength(MemoryStream stream)
		{
			if (stream.Length - stream.Position < 3)
				return ushort.MaxValue; // Not enough bytes to read the length

			byte[] len_data = new byte[2];

			stream.Read (len_data, 0, 2);

			if (BitConverter.IsLittleEndian)
				Array.Reverse (len_data);

			ushort len = BitConverter.ToUInt16 (len_data, 0);

			return len;
		}

	}
}