﻿using System.Collections.Generic;
using System;

namespace common.utils
{

    public static class ParticleUtil
    {
        public static void RefreshShadersandMaterials(GameObject loaded_asset)
        {
#if UNITY_EDITOR
            // This is handeling the particles in the unity editor
            Renderer[] renderers = loaded_asset.GetComponentsInChildren<Renderer>(true);
            Material[] materials;
            string[] shaders;
            foreach (var rend in renderers)
            {
                materials = rend.sharedMaterials;
                if (materials == null || materials.Length == 0)
                    continue;

                shaders = new string[materials.Length];

                for (int i = 0; i < materials.Length; i++)
                {
                    if (materials[i] != null)
                        shaders[i] = materials[i].shader.name;
                }

                for (int i = 0; i < materials.Length; i++)
                {
                    if (shaders[i] != null)
                        materials[i].shader = Shader.Find(shaders[i]);
                }
            }
#endif
        }

        public static void ReapplyShadersOnTextMeshPro(GameObject go)
        {
            try
            {

                foreach (TextMeshProUGUI rend in go.GetComponentsInChildren<TextMeshProUGUI>(true))
                {
                    List<Material> refreshedMaterials = new List<Material>();
                    Material[] materials = rend.fontSharedMaterials;

                    for (int i = 0; i < materials.Length; i++)
                    {
                        if (materials[i] == null || materials[i].shader == null)
                            continue;

                        string shaderName = materials[i].shader.name;
                        Shader shader = Shader.Find(shaderName);

                        if (shader != null)
                        {
                            materials[i].shader = shader;
                            refreshedMaterials.Add(materials[i]);
                        }
                        else
                            Debug.LogWarning("Shader " + shaderName + " not found!");
                    }

                    if (refreshedMaterials.Count > 0)
                        rend.fontSharedMaterials = refreshedMaterials.ToArray();
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning("Cannot apply shaders on TMP. Exception: " + e);
            }
        }


    }
}
