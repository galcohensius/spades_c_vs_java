﻿using System.Collections.Generic;

namespace common.utils
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class FPS_Counter : MonoBehaviour
    {
        private TextMeshProUGUI counterText;

        private void Start()
        {
            counterText = GetComponent<TextMeshProUGUI>();
            StartCoroutine(Count());
        }

        private IEnumerator<WaitForEndOfFrame> Count()
        {
            const float smoothness = 0.8f;
            const float sharpness = 1f - smoothness;
            var smoothUnscaledDeltaTime = 1f / 60f;
            while (gameObject)
            {
                for (var i = 0; i < 60; i++)
                {
                    yield return new WaitForEndOfFrame();
                    smoothUnscaledDeltaTime = smoothness * smoothUnscaledDeltaTime + sharpness * Time.deltaTime;
                }

                int fps = Mathf.RoundToInt(1f / smoothUnscaledDeltaTime);
                counterText.text = fps.ToString();
            }
        }
    }
}