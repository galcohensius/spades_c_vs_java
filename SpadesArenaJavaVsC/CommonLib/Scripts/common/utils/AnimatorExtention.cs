﻿namespace common.utils
{
    public static class AnimatorExtension
    {
        // This function checks if the parameter exists in this animator
        public static bool HasParameterOfType(this Animator self, string name, AnimatorControllerParameterType type)
        {
            if (null == self)
                return false;

            AnimatorControllerParameter[] parameters = self.parameters;
            foreach (var currParam in parameters)
            {
                if (currParam.type == type && currParam.name == name)
                    return true;
            }
            return false;
        }
    }
}
