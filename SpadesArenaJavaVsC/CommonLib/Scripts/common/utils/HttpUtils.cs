﻿using System.Collections.Generic;
using System;
namespace common.utils {


    public static class HttpUtils {

        /// <summary>
        /// Parses the query parameters from a given url.
        /// </summary>
        /// <returns>The query parameters as Dictionary.</returns>
        /// <param name="url">the URL to parse</param>
        public static Dictionary<string,string> ParseQueryParams(Uri url) {

            Dictionary<string, string> result = new Dictionary<string, string>();

            string query = url.Query;

            if (!string.IsNullOrEmpty(query)) {
                string[] paramTokens = query.Substring(1).Split('&');

                foreach (var paramToken in paramTokens)
                {
                    string[] nameValuePair = paramToken.Split('=');
                    if (nameValuePair.Length==2)
                        result[nameValuePair[0]] = nameValuePair[1];
                }
            }

            return result;
        }
    }
}