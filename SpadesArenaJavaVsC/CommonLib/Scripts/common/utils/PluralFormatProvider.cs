﻿using System;

namespace common.utils
{
	/// <summary>
	/// Format provider for plural english items, to be used with String.Format method
	/// Eg. Friend / Friends
	/// </summary>
	public class PluralFormatProvider : IFormatProvider, ICustomFormatter
	{

		public object GetFormat (Type formatType)
		{
			return this;
		}


		public string Format (string format, object arg, IFormatProvider formatProvider)
		{
			if (!String.IsNullOrEmpty (format)) {
				string[] forms = format.Split (';');
				int value = (int)arg;
				int form = value == 1 ? 0 : 1;
				return value.ToString () + " " + forms [form];
			} 

			return arg.ToString ();
		}

	}
}