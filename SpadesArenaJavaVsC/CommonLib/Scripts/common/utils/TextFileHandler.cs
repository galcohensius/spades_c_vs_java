﻿using common.controllers;

public class TextFileHandler
{
    public static string ReadStringFromTextFile(string path)
    {
        string fileStringText = string.Empty;

        try
        {
            TextAsset asset = Resources.Load(path) as TextAsset;
            fileStringText = asset.text;
        }
        catch
        {
            LoggerController.Instance.LogError(string.Format("Path Does Not Exist : {0}", path));
        }

        return fileStringText;
    }

    public static void WriteToPathTextFile(string path, string text)
    {
        // Creates File If Does Not Already Exists , Else Replaces Existing Text In The File
        System.IO.File.WriteAllText(path, text);
    }
}