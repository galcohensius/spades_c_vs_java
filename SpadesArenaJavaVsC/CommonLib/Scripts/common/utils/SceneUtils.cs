﻿using System;
using System.Collections.Generic;
using common.controllers;

namespace common.utils
{
    public class SceneUtils : MonoBehaviour
    {
        private HashSet<GameObject> rootGameObjects = new HashSet<GameObject>();
        private Dictionary<Type, Component> cachedComponents = new Dictionary<Type, Component>();

        [Tooltip("object that have the 'don't destroy on load' property move to a special scene outside the active scene and become unreachable to SceneUtils. Add them manually here to protect them.")]
        [SerializeField]
        private List<GameObject> dontDestroyOnLoadObjects;

        public bool LogObjects = true;

        public static SceneUtils Instance;

        private void Awake()
        {
            //FOR THE FUTURE GENERATIONS: the "if (Instance == null)" segment is a must for a MonoBehaviour Singleton to actually behave as intended.
            //DontDestroyOnLoad used to fail and/or crush and/or make compilation errors when it was not the last line of code in the method. it has been
            //fixed in an unknown version of Unity 2018.4. Might still be problematic if not the last line in the block.
            //DestroyImmediate should be used only in extreme cases outside editor code. This is one of them since we want to kill the object as soon as
            //possible before other object can be instantiated. To ensure it will execute correctly as possible SceneUtils has the highest priority in the
            //script execution order (lowest value).
            //what do we accomplish with this? We want the singleton to "travel between scene" and this is accomplished with the DontDestroyOnLoad that transfers
            //the GameObject to the limbo dimension of "Don't Destroy On Load Scene" and as intended, Awake and OnDestory will happen only once in the lifespan of
            //the program. However, when loading the scene where the singleton game object originates from will load another copy of it and will also transfer it
            //to the limbo dimension. To solve and resolve this identity crisis we check on the new object (that calls its Awake) if there is an Instance already.
            //since we actually do we destory the newly loaded one with DestroyImmediate and since we're using DestroyImmediate AND it the first script to execute 
            //this is the most sure and safe to destroy the object before any other script that might be attached to it, run its own Awake and interrupt the correct
            //initializing that was intended.
            //since this is a root object, only a single singleton (and the one that executes first) is necessary.
            //
            //Shay Krainer 11.09.2019 <3
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(this.gameObject);
                //SceneManager.sceneLoaded += SceneManager_sceneLoaded;
            }
            else
            {
                DestroyImmediate(this.gameObject);
                return;
            }
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
            RefreshComponents();
        }

        //TODO: remove me if nothing breaks on scene loading
        private void SceneManager_sceneLoaded(Scene activeScene, LoadSceneMode loadSceneMode)
        {
            //RefreshComponents();
        }

        /// <summary>
        /// there is no quick and reliable way that I can think of to add/remove only components that we need to
        /// so start all over and hope the additive scene loading won't screw us up. therefore: TESTING ARE NEEDED!!!
        /// </summary>
        public void RefreshComponents()
        {
            rootGameObjects.Clear();
            cachedComponents.Clear();

            //refresh active root object
            rootGameObjects.UnionWith(SceneManager.GetActiveScene().GetRootGameObjects());
            //re-add lost undestroyable root game objects
            foreach(GameObject rootObject in dontDestroyOnLoadObjects)
            {
                if (!rootGameObjects.Contains(rootObject))
                    rootGameObjects.Add(rootObject);
            }
            AddComponents();

            if (LogObjects)
            {
                LoggerController.Instance.Log("game objects at root found:");
                foreach (GameObject go in rootGameObjects)
                    LoggerController.Instance.Log(go.name);

                LoggerController.Instance.Log("cached components:");
                foreach (Component c in cachedComponents.Values)
                    LoggerController.Instance.Log(c.GetType().ToString());
            }
        }

        private void AddComponents()
        {
            List<Component> results = new List<Component>();
            foreach(GameObject go in rootGameObjects)
            {
                //unity may have not finish updating and null objects may slip through
                if (go == null)
                    continue;

                go.GetComponentsInChildren(true, results);
                foreach (Component c in results)
                {
                    if (c == null)
                    {
                        //LoggerController is not initialized yet on the first time AddComponents runs. 
                        Debug.LogError("missing component found on game object: " + go.name);
                        continue;
                    }

                    if (!cachedComponents.ContainsKey(c.GetType()))
                    {
                        cachedComponents.Add(c.GetType(), c);
                    }
                }

                results.Clear();
            }
        }

        /// <summary>
        /// to get child object use GameObject.Find
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameObject GetRootGameObject(string name)
        {
            foreach (GameObject go in rootGameObjects)
            {
                if (go.name == name)
                    return go;
            }

            LoggerController.Instance.LogError("no game object called " + name + " found");
            return null;
        }

        /// <summary>
        /// returns the first component of requested type. Assumes usage of this method is for singular instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="componentType"></param>
        /// <returns></returns>
        public T GetStoredComponent<T>() where T : Component
        {
            Component c = null;
            if (cachedComponents.TryGetValue(typeof(T), out c))
                return c as T;

            LoggerController.Instance.LogError("No component of type " + typeof(T).ToString() + " found");
            return null;
        }

        /// <summary>
        /// only game objects at the root of the game scene can have the "don't destroy on load" propery.
        /// objects that are added to this list are not expected to ever leave it.
        /// </summary>
        /// <param name="rootGameObject"></param>
        public void AddUndestroyableRootObject(GameObject rootGameObject)
        {
            if (rootGameObject.transform.root != rootGameObject.transform)
            {
                LoggerController.Instance.LogError(rootGameObject.name + " is not root object and won't be added to the protected list");
                return;
            }
            dontDestroyOnLoadObjects.Add(rootGameObject);
        }
    }
}