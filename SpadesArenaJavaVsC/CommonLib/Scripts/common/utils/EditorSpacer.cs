﻿/// <summary>
/// add string line for notes and can act as divider
/// </summary>
public class EditorSpacer : MonoBehaviour
{
    public string Note = "LOL";

#if !UNITY_EDITOR
    private void Awake()
    {
        DestroyImmediate(this);
    }
#endif
}
