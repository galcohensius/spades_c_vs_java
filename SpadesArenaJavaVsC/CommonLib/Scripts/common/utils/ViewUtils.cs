﻿namespace common.utils
{

    public class ViewUtils
    {
        public static void EnableDisableButtonText(Button button)
        {
            TMP_Text button_text = button.transform.FindDeepChildTmp();

            if (button_text != null)
            {
                Color curr_color = button_text.color;

                if (button.interactable)
                    button_text.color = new Color(curr_color.r, curr_color.g, curr_color.b, 1f);
                else
                    button_text.color = new Color(curr_color.r, curr_color.g, curr_color.b, 0.5f);
            }

        }
    }
}
