﻿using System.Collections.Generic;

namespace common.ims.model
{

    /// <summary>
    /// IMS Action model
    /// </summary>
    public class IMSAction : IMSEventComponent {
        int id;
        List<IMSActionValue> values;

        public IMSAction(int id, List<IMSActionValue> values)
        {
            this.id = id;
            this.values = values;
        }


        public int Id
        {
            get
            {
                return id;
            }
        }

        public List<IMSActionValue> Values
        {
            get
            {
                return values;
            }
        }

        public override void SetImsEvent(IMSEvent imsEvent)
        {
            this.imsEvent = imsEvent;
        }
    }

    /// <summary>
    /// IMS Action value model
    /// </summary>
    public class IMSActionValue
    {
        string value;
        string externalPackageId;
        string extraData;

        public IMSActionValue(string value, string externalPackageId, string extraData)
        {
            this.value = value;
            this.externalPackageId = externalPackageId;
            this.extraData = extraData;
        }

        public string Value
        {
            get
            {
                return value;
            }
        }

        public string ExternalPackageId
        {
            get
            {
                return externalPackageId;
            }
        }

        public string ExtraData { get => extraData; }
    }

}