﻿using common.models;
using System.Collections.Generic;

namespace common.ims.model
{
    public enum GoodsListType
    {
        Coins,
        Vouchers,
        Items
    }


    public class IMSGoodsList
    {
        List<int> m_coins = new List<int>();
        List<VouchersGroup> m_vouchers = new List<VouchersGroup>();
        List<string> m_inventoryItems = new List<string>();
        bool m_mgap = false;

        List<float> m_factors = new List<float>();
        public List<int> Coins { get => m_coins; set => m_coins = value; }
        public int CoinsValue
        {
            get
            {
                if (m_coins.Count > 0)
                    return m_coins[0];
                return 0;
            }
        }
        public List<VouchersGroup> Vouchers { get => m_vouchers; set => m_vouchers = value; }
        public List<string> InventoryItems { get => m_inventoryItems; set => m_inventoryItems = value; }
        public bool MGAP { get => m_mgap; set => m_mgap = value; }
        public List<float> Factors { get => m_factors; set => m_factors = value; }
    }
}
