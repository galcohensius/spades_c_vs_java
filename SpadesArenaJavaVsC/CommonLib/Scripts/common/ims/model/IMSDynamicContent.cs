﻿using System.Collections.Generic;

namespace common.ims.model
{
    /// <summary>
    /// IMS Dynamic content model
    /// </summary>
    public class IMSDynamicContent : IMSEventComponent
    {
        List<JSONObject> content;
        List<JSONObject> style;
        JSONObject placeholders;

        public IMSDynamicContent(List<JSONObject> content, List<JSONObject> style, JSONObject placeholders)
        {
            this.content = content;
            this.style = style;
            this.placeholders = placeholders;
        }

        public List<JSONObject> Content
        {
            get
            {
                return content;
            }

        }

        public List<JSONObject> Style
        {
            get
            {
                return style;
            }
        }

        public JSONObject Placeholders
        {
            get
            {
                return placeholders;
            }
        }

        public override void SetImsEvent(IMSEvent imsEvent)
        {
            this.imsEvent = imsEvent;
        }
    }
}
