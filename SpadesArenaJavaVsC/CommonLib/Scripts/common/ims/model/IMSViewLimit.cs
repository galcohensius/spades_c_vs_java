﻿namespace common.ims.model
{

    /// <summary>
    /// IMS View limit model
    /// </summary>
    public class IMSViewLimit : IMSEventComponent
    {
        long expires;
        string position;
        bool isVIsible;

        public IMSViewLimit(long expires, string position, bool isVIsible)
        {
            this.expires = expires;
            this.position = position;
            this.isVIsible = isVIsible;
        }

        public long Expires
        {
            get
            {
                return expires;
            }
        }

        public string Position
        {
            get
            {
                return position;
            }
        }

        public bool IsVIsible
        {
            get
            {
                return isVIsible;
            }
        }

        public override void SetImsEvent(IMSEvent imsEvent)
        {
            this.imsEvent = imsEvent;
        }
    }
}
