﻿using System.Collections.Generic;

namespace common.ims.model
{
    /// <summary>
    /// IMS Decision model.
    /// </summary>
    public class IMSDecision : IMSEventComponent {
        int id;
        int weight;
        List<string> interactionZoneIds;
        List<string> values;

        public IMSDecision(int id, int weight, List<string> values,List<string> interactionZoneIds)
        {
            this.id = id;
            this.weight = weight;
            this.interactionZoneIds = interactionZoneIds;
            this.values = values;
        }

        public int Id
        {
            get
            {
                return id;
            }

        }

        public int Weight
        {
            get
            {
                return weight;
            }
        }

        public List<string> InteractionZoneIds
        {
            get
            {
                return interactionZoneIds;
            }
        }

        public List<string> Values
        {
            get
            {
                return values;
            }
        }

        public override void SetImsEvent(IMSEvent imsEvent)
        {
            this.imsEvent = imsEvent;
        }
    }
    
}