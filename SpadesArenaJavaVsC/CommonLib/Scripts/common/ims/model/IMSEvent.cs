﻿using System.Collections.Generic;

namespace common.ims.model
{

    /// <summary>
    /// IMS Event model
    /// </summary>
    public class IMSEvent
    {
        string id;
        List<IMSDecision> decisions;

        public IMSEvent(string id, List<IMSDecision> decisions)
        {
            this.id = id;
            this.decisions = decisions;

            // Connect the event to the event model children
            foreach (var dec in decisions)
            {
                dec.SetImsEvent(this);
            }

        }

        public string Id { get => id; }


        public List<IMSDecision> Decisions
        {
            get
            {
                return decisions;
            }
        }

    }
}
