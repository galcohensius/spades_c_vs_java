﻿using common.mes;


namespace common.ims.model
{

    public class IMSInteractionZoneMESWrapper : IMSInteractionZone
    {
        private MESMaterial m_material;

        public IMSInteractionZoneMESWrapper(MESMaterial material, string imsLocation) : base(material.id.ToString(), imsLocation, "", material.source, material.templateId, material.CloseButtonMode, 0, null,
               null, null, null, null, false, false, null, int.MaxValue , false, false, null)
        {
            m_material = material;
        }

        public MESMaterial Material { get => m_material;}
    }
}


