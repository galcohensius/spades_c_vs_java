﻿namespace common.ims.model {
    
    public abstract class IMSEventComponent {

        protected IMSEvent imsEvent;

        public abstract void SetImsEvent(IMSEvent imsEvent);

        public IMSEvent ImsEvent
        {
            get {
                return imsEvent;
            }

        }


    }
    
}