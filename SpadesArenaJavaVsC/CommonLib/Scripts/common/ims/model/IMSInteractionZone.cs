﻿using System.Collections.Generic;

namespace common.ims.model
{

    /// <summary>
    /// IMS Interaction zone model
    /// </summary>
    public class IMSInteractionZone 
    {
        private string id;
        private string location;
        private string metadata;
        private string originMetadata; // Client side assigned
        private string source;
        private string templateId;
        private string closeBtnType;
        private int order;
        private IMSAction action;
        private List<string> childIds;
        private IMSDynamicContent dynamicContent;
        private List<string> languages;
        private IMSViewLimit viewLimit;
        private bool trackImpressions, trackClicks;
        private JSONObject trackingData; // Black box JSON object
        private int popupIndex; 
        private bool isAssetBundle;
        private bool isBanner;
        private IMSEvent openingEvent; // This field is dynamically updated every time the IZone is shown, since IZones are shared by several events
        private Dictionary<string, string> optionalParams;

        public IMSInteractionZone(string id, string location, string metadata, string source, string templateId, string closeBtnType,
                                  int order, IMSAction action,
                                  List<string> childIds, IMSDynamicContent dynamicContent, List<string> languages,
                                  IMSViewLimit viewLimit, bool trackImpressions, bool trackClicks, JSONObject trackingData,
                                  int popupIndex, bool isAssetBundle, bool isBanner,Dictionary<string,string> optionalParams)
        {
            this.id = id;
            this.location = location;
            this.metadata = metadata;
            this.OriginMetadata = ""; // Not sent from the server, client side assigned
            this.source = source;
            this.templateId = templateId;
            this.closeBtnType = closeBtnType;
            this.order = order;
            this.action = action;
            this.childIds = childIds;
            this.dynamicContent = dynamicContent;
            this.languages = languages;
            this.viewLimit = viewLimit;
            this.trackImpressions = trackImpressions;
            this.trackClicks = trackClicks;
            this.trackingData = trackingData;
            this.popupIndex = popupIndex;
            this.isAssetBundle = isAssetBundle;
            this.isBanner = isBanner;
            this.optionalParams = optionalParams;
        }

        public string Id
        {
            get
            {
                return id;
            }
        }

        public string Location
        {
            get
            {
                return location;
            }

        }

        public string Metadata
        {
            get
            {
                return metadata;
            }

        }

        public string Source
        {
            get
            {
                return source;
            }


        }

        public string TemplateId
        {
            get
            {
                return templateId;
            }
        }

        public int Order
        {
            get
            {
                return order;
            }
        }

        public IMSAction Action
        {
            get
            {
                return action;
            }

        }

        public List<string> ChildIds
        {
            get
            {
                return childIds;
            }

        }

        public IMSDynamicContent DynamicContent
        {
            get
            {
                return dynamicContent;
            }

        }

        public List<string> Languages
        {
            get
            {
                return languages;
            }

        }

        public IMSViewLimit ViewLimit
        {
            get
            {
                return viewLimit;
            }

        }

        public string CloseBtnType
        {
            get
            {
                return closeBtnType;
            }
        }

        public JSONObject TrackingData
        {
            get
            {
                return trackingData;
            }
        }

        public bool TrackImpressions
        {
            get
            {
                return trackImpressions;
            }
        }

        public bool TrackClicks
        {
            get
            {
                return trackClicks;
            }

        }

        public int PopupIndex { get => popupIndex; set => popupIndex = value; }

        public bool IsAssetBundle { get => isAssetBundle; set => isAssetBundle = value; }

        public string OriginMetadata { get => originMetadata; set => originMetadata = value; }

        public bool IsBanner { get => isBanner; }

        public IMSEvent OpeningEvent { get => openingEvent; set => openingEvent = value; }

        public string OptionalParam(string key)
        {
            string result=null;
            optionalParams?.TryGetValue(key, out result);
            return result;
        }
        
    }
}
