﻿namespace common.ims.model
{
    public class IMSPODynamicContent
    {
        IMSGoodsList m_imsGoodsList;
        private float m_cost;
        private float m_percentageMore;

        public IMSPODynamicContent(IMSGoodsList imsGoodsList, float cost, float percentageMore)
        {
            m_cost = cost;
            m_percentageMore = percentageMore;
            m_imsGoodsList = imsGoodsList;
        }

        public float Cost { get => m_cost; }
        public IMSGoodsList Goods { get => m_imsGoodsList; }
        public float PercentageMore { get => m_percentageMore; }
    }
}
