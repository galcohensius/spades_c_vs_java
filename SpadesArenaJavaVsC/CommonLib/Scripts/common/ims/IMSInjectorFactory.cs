﻿using common.ims;

namespace common.comm
{
    public class IMSInjectorFactory : MonoBehaviour
    {
        public static IMSInjectorFactory Instance;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }

        public virtual IMSBannerInjector CreateByTemplate(string templateName)
        {
            IMSBannerInjector bannereInjector = null;

            switch (templateName)
            {
                case ("S1"): bannereInjector = new IMSDirectSBuyInjector(); break;
                case ("BBO"): bannereInjector = new IMSBonusBannerInjector(); break;
            }

            return bannereInjector;
        }

        public virtual IMSBannerInjector CreateByLocation(string locationName)
        {
            IMSBannerInjector bannereInjector = null;

            switch (locationName)
            {

            }

            return bannereInjector;
        }
    }
}
