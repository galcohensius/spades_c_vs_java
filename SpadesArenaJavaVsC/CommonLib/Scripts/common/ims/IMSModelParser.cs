﻿using common.controllers;
using common.ims.model;
using System;
using System.Collections.Generic;


namespace common.ims
{

    public class IMSModelParser
    {
        public IMSEvent ParseEvent(string id, JSONNode json, IMSController controller)
        {
            LoggerController.Instance.LogFormat(LoggerController.Module.IMS, "Parsing event {0}:",id);

            // Decisions
            List<IMSDecision> decisions = new List<IMSDecision>();
            JSONArray decArray = json["des"].AsArray;
            foreach (JSONNode decJson in decArray)
            {
                decisions.Add(ParseDecision(decJson));
            }

            IMSEvent result = new IMSEvent(id,decisions);

            // Interaction zones - held globaly even though sent by event
            LoggerController.Instance.Log(LoggerController.Module.IMS, "Interaction zones list:");

            JSONObject iZonesJson = json["iZMp"].AsObject;
            foreach (KeyValuePair<string, JSONNode> iZoneJson in iZonesJson)
            {
                IMSInteractionZone zone = ParseInteractionZone(iZoneJson.Key, iZoneJson.Value);
                controller.AddIZone(iZoneJson.Key, zone);

                if (zone.IsBanner)
                {
                    controller.AddBannerByLocation(zone);
                }

                // Extract package ids
                if (zone.Action.Id == mes.MESBase.ACTION_DIRECT_BUY)
                {
                    List<IMSActionValue> actionVals = zone.Action.Values;
                    foreach (IMSActionValue actionVal in actionVals)
                    {
                        controller.PackageIds[actionVal.Value] = actionVal.ExternalPackageId;
                    }
                }

                LoggerController.Instance.LogFormat(LoggerController.Module.IMS, "iZone: {0}, Location: {1}, Template: {2}", zone.Id, zone.Location, zone.TemplateId);
            }

            return result;
        }

        public IMSInteractionZone ParseInteractionZone(string id, JSONNode json)
        {
            // Parse tracking data
            bool trackImpressions = false, trackClicks = false;
            if (json["trg"] != null)
            {
                trackImpressions = json["trg"]["iEe"].AsInt == 1;
                trackClicks = json["trg"]["cEe"].AsInt == 1;
            }

            string templateId = json["tId"].Value;
            return new IMSInteractionZone(
                id,
                json["lon"].Value,
                json["mDa"].Value,
                json["src"].Value,
                templateId,
                json["cBn"].Value,
                json["orr"].AsInt, // Order: 0 means order was not specified
                ParseAction(json["acn"]),
                JsonArrayToStringList(json["chn"]),
                ParseDynamicContent(json["dCt"]),
                JsonArrayToStringList(json["lSs"]),
                ParseViewLimit(json["vLt"]),
                trackImpressions,
                trackClicks,
                json["tDa"].AsObject,
                json["pIx"].AsInt,
                json["iBe"].AsInt == 1,
                json["mTe"].Value == "B",
                JsonObjectToStringDictionary(json["oPs"].AsObject)
            );
        }

       

        public IMSDecision ParseDecision(JSONNode json)
        {
            IMSDecision dec = new IMSDecision(
                json["id"].AsInt,
                json["wet"].AsInt,
                JsonArrayToStringList(json["vas"]),
                JsonArrayToStringList(json["iZs"])
            );

            LoggerController.Instance.LogFormat(LoggerController.Module.IMS, "Decision: {0}, iZones: {1}", dec.Id, String.Join(",",dec.InteractionZoneIds));

            return dec;
        }

        public IMSAction ParseAction(JSONNode json)
        {
            List<IMSActionValue> values = new List<IMSActionValue>();
            JSONArray valuesArr = json["vas"].AsArray;
            foreach (JSONNode valueJson in valuesArr)
            {
                values.Add(ParseActionValue(valueJson));
            }

            return new IMSAction(
                json["id"].AsInt,
                values
            );
        }

        public IMSActionValue ParseActionValue(JSONNode json)
        {
            return new IMSActionValue(
                    json["vae"].Value,
                    json["ePId"].Value,
                    json["eDa"].Value);
        }

       
        public IMSViewLimit ParseViewLimit(JSONNode json)
        {
            if (json.Count == 0)
                return null;
            return new IMSViewLimit(
                (long)json["exs"].AsDouble,
                json["pon"].Value,
                json["iVe"].AsInt == 1
            );
        }

        public IMSDynamicContent ParseDynamicContent(JSONNode json)
        {
            return new IMSDynamicContent(
                JsonArrayToObjectList(json["cot"]),
                JsonArrayToObjectList(json["ste"]),
                json["pls"].AsObject
            );
        }


        public JSONObject SerializeActionValue(IMSActionValue actionValue)
        {
            if (actionValue == null)
                return null;

            JSONObject result = new JSONObject();
            result["vae"] = actionValue.Value;
            result["eDa"] = actionValue.ExtraData;
            return result;
        }

        private List<JSONObject> JsonArrayToObjectList(JSONNode arr)
        {
            List<JSONObject> result = new List<JSONObject>();
            if (!arr.IsArray)
                return result;

            foreach (JSONNode item in arr as JSONArray)
            {
                result.Add((JSONObject)item);
            }

            return result;
        }

        private List<string> JsonArrayToStringList(JSONNode arr)
        {
            List<string> result = new List<string>();
            if (!arr.IsArray)
                return result;

            foreach (JSONNode item in arr as JSONArray)
            {
                result.Add((string)item);
            }

            return result;
        }

        private Dictionary<string, string> JsonObjectToStringDictionary(JSONObject obj)
        {
            if (obj == null) return null;

            Dictionary<string, string> result = new Dictionary<string, string>();

            foreach (KeyValuePair<string, JSONNode> child in obj)
            {
                result.Add(child.Key, child.Value.Value);
            }

            return result;
        }
    }

}