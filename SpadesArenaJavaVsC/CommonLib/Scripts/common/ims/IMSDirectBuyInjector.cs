﻿using common.utils;
using common.views;

namespace common.ims
{
    public class IMSDirectSBuyInjector : IMSBannerInjector
    {

        protected override void BannerInject()
        {
            InstantiatePrefab();
        }

        private void InstantiatePrefab()
        {
            Transform transform_BG = m_loaded_asset.transform.FindDeepChild("ActionButton");
            
            if (transform_BG != null)
            {
                transform_BG.gameObject.AddComponent<ButtonsOverView>();

                // Use pointer down to support external URL actions
                transform_BG.gameObject.AddComponent<PointerDownHandler>();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                {
                    if (m_iZone.DynamicContent != null && m_iZone.DynamicContent.Content.Count > 0)
                    {
                        Clicked();
                    }
                });
            }
                
            Transform coins_text = m_loaded_asset.transform.FindDeepChild("CoinsText");
            coins_text?.gameObject.SetActive(false);
            Transform price_text = m_loaded_asset.transform.FindDeepChild("PriceText");
            price_text?.gameObject.SetActive(false);
            Transform coins_more_text = m_loaded_asset.transform.FindDeepChild("CoinsMoreText");
            // CoinsMoreText visibility is controlled from the bundle
            Transform timer_bg = m_loaded_asset.transform.FindDeepChild("TimerBG");
            timer_bg?.gameObject.SetActive(false);
            Transform timer_text = m_loaded_asset.transform.FindDeepChild("TimerText");
            timer_text?.gameObject.SetActive(false);


            if (m_iZone.DynamicContent != null && m_iZone.DynamicContent.Content.Count > 0)
            {
                if (coins_text != null && m_iZone.DynamicContent.Content[0]["gds"] != null)
                {
                    AssetBundleUtils.SetTextForTransform(coins_text, FormatUtils.FormatBalance(m_iZone.DynamicContent.Content[0]["gds"]));
                    coins_text.gameObject.SetActive(true);
                }

                if (price_text != null && m_iZone.DynamicContent.Content[0]["cot"] != null)
                {
                    AssetBundleUtils.SetTextForTransform(price_text, "$" + m_iZone.DynamicContent.Content[0]["cot"]);
                    price_text.gameObject.SetActive(true);
                }

                if (coins_more_text != null && m_iZone.DynamicContent.Content[0]["pMe"] != null)
                {
                    AssetBundleUtils.SetTextForTransform(coins_more_text, m_iZone.DynamicContent.Content[0]["pMe"] + "%");
                }
            }
        }

    }
}
