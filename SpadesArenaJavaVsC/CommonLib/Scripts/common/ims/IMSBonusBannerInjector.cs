﻿using common.utils;
using common.views;

namespace common.ims
{
    public class IMSBonusBannerInjector : IMSBannerInjector
    {

        protected override void BannerInject()
        {
            InstantiatePrefab();
        }

        private void InstantiatePrefab()
        {
            Transform transform_BG = m_loaded_asset.transform.FindDeepChild("ActionButton");
            
            if (transform_BG != null)
            {
                transform_BG.gameObject.AddComponent<ButtonsOverView>();

                // Use pointer down to support external URL actions
                transform_BG.gameObject.AddComponent<PointerDownHandler>();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                {
                    if (m_iZone.DynamicContent != null && m_iZone.DynamicContent.Content.Count > 0)
                    {
                        Clicked();
                    }
                });
            }

            Transform CoinsAmount = m_loaded_asset.transform.FindDeepChild("CoinsAmount");

            if (m_iZone.DynamicContent != null && m_iZone.DynamicContent.Content.Count > 0)
            {
                if (CoinsAmount != null && m_iZone.DynamicContent.Content[0]["gds"] != null)
                {
                    TMP_Text tmpCoinsAmount = CoinsAmount.GetComponent<TMP_Text>();
                    if (tmpCoinsAmount != null)         
                        tmpCoinsAmount.text = "<sprite=1>" + FormatUtils.FormatPrice(m_iZone.DynamicContent.Content[0]["gds"]);
                }
            }



        }

    }
}
