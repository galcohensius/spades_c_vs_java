﻿using System.Collections;
using common.ims.model;
using common.utils;
using System;
using common.ims.views;
using common.controllers;
using common.views;
using common.mes;

namespace common.ims
{
    public abstract class IMSBannerInjector
    {
        protected Transform timer_text = null;
        protected Transform timer_bg = null;
        protected Transform sounds_object;
        protected GameObject m_loaded_asset;
        protected IMSInteractionZone m_iZone;
        protected IMSBannerView_v2 m_bannerView;
        protected GameObject m_IMSBannerV2_prefab;
        protected float scaleFactorValue = 1;

        private void SetParameters(IMSBannerView_v2 bannerView, GameObject loaded_asset, IMSInteractionZone iZone, GameObject IMSBannerV2_prefab)
        {
            m_bannerView = bannerView;
            m_loaded_asset = loaded_asset;
            m_iZone = iZone;
            m_IMSBannerV2_prefab = IMSBannerV2_prefab;
        }
        
        protected abstract void BannerInject();

        public virtual void InjectGraphics (IMSBannerView_v2 bannerView, GameObject loaded_asset, IMSInteractionZone iZone)
        {
            m_bannerView = bannerView;
            m_loaded_asset = loaded_asset;
            m_iZone = iZone;

            Transform transform_BG = loaded_asset.transform.FindDeepChild("ActionButton");
            //For promo rooms without Action Button
            if (transform_BG == null)
                transform_BG = m_loaded_asset.transform;

            // For close and continue don't even trigger so if the banner is embedded in a Popup, it will not close it
            if (iZone.Action.Id == MESBase.ACTION_CLOSE_CONTINUE)
                return;

#if UNITY_WEBGL

            // Use pointer down to support external URL actions
            transform_BG.gameObject.AddComponent<PointerDownHandler>();
            transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
            transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
            {
                Clicked();
            });
#else

            transform_BG.gameObject.gameObject.AddComponent<PointerUpHandler>();
            transform_BG.gameObject.gameObject.GetComponent<PointerUpHandler>().OnUp.RemoveAllListeners();
            transform_BG.gameObject.gameObject.GetComponent<PointerUpHandler>().OnUp.AddListener(() =>
            {
                Clicked();
            });
#endif
        }

        public virtual void Inject(IMSBannerView_v2 bannerView, GameObject loaded_asset, IMSInteractionZone iZone, GameObject IMSBannerV2_prefab)
        {
            SetParameters(bannerView, loaded_asset, iZone, IMSBannerV2_prefab);
            BannerInject();
            HandleTimer();
            HandleSound();
            HandleScaling();

            // For close and continue don't even trigger so if the banner is embedded in a Popup, it will not close it
            if (iZone.Action.Id != MESBase.ACTION_CLOSE_CONTINUE)
                HandleClick();
        }

        protected virtual void HandleClick ()
        {
            Transform transform_BG = m_loaded_asset.transform.FindDeepChild("ActionButton");
            //For promo rooms without Action Button
            if (transform_BG == null)
                transform_BG = m_loaded_asset.transform;

            if (transform_BG != null)
            {
                transform_BG.gameObject.AddComponent<ButtonsOverView>();

                // Use pointer down to support external URL actions
#if UNITY_WEBGL
                transform_BG.gameObject.AddComponent<PointerDownHandler>();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                {
                    Clicked();
                });

#else
                // When not WebGL
                transform_BG.gameObject.AddComponent<PointerUpHandler>();
                transform_BG.gameObject.GetComponent<PointerUpHandler>().OnUp.RemoveAllListeners();
                transform_BG.gameObject.GetComponent<PointerUpHandler>().OnUp.AddListener(() =>
                {
                    Clicked();
                });
#endif
            }
        }

        private void HandleScaling ()
        {
            // Scale the prefab
            // Get the scale factor from a GameObject named "ScaleFactor" which should be included in the prefab
            float scaleFactor = 1;
            Transform scaleFactorObj = m_loaded_asset.transform.FindDeepChild("ScaleFactor");
            if (scaleFactorObj != null)
            {
                scaleFactorValue = scaleFactorObj.localScale.x;
                scaleFactor = 1 / scaleFactorValue;
            }

            m_loaded_asset.GetComponent<RectTransform>().localScale = new Vector3(scaleFactor, scaleFactor, 1);

            m_loaded_asset.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
        }

        private void HandleTimer()
        {
            timer_bg = m_loaded_asset.transform.FindDeepChild("TimerBG");
            timer_text = m_loaded_asset.transform.FindDeepChild("TimerText");

            ManageTimer();
        }


        private void HandleSound()
        {
            m_bannerView.sounds_object = m_loaded_asset.transform.FindDeepChild("SFX");

            if (m_bannerView.sounds_object != null)
                SoundsController.Instance.AddIMSBannerAudioSource(m_bannerView.sounds_object.GetComponent<AudioSource>());

        }

        private void ManageTimer()
        {
            if (m_bannerView.timer_routine != null)
            {
                m_bannerView.StopCoroutine(m_bannerView.timer_routine);
                m_bannerView.timer_routine = null;
            }

            if (m_iZone.ViewLimit == null)
            {
                // No view limit - no timer
                return;
            }

            if (m_iZone.ViewLimit.Expires > 0)
            {
                // Start the timer
                if (timer_text.gameObject.GetComponent<Text>() != null)
                    m_bannerView.timer_routine = POTimer((int)m_iZone.ViewLimit.Expires, timer_text.gameObject.GetComponent<Text>());

                if (timer_text.gameObject.GetComponent<TMP_Text>() != null)
                    m_bannerView.timer_routine = POTimer((int)m_iZone.ViewLimit.Expires, timer_text.gameObject.GetComponent<TMP_Text>());

                m_bannerView.StartCoroutine(m_bannerView.timer_routine);


                // Check if we want to actually show the timer
                if (m_iZone.ViewLimit.IsVIsible)
                {
                    timer_bg?.gameObject.SetActive(true);
                    timer_text?.gameObject.SetActive(true);
                }
            }
        }

        IEnumerator POTimer(int expired, TMP_Text m_timer_text)
        {
            m_timer_text.gameObject.SetActive(true);


            while (true)
            {
                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);


                m_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            m_bannerView.OnTimerExpired?.Invoke(m_bannerView.Index);

            if (m_bannerView.DestroyOnTimerExpired)
            {
                //m_bannerView.Destroy(gameObject);
            }
        }

        IEnumerator POTimer(int expired, Text m_timer_text)
        {
            m_timer_text.gameObject.SetActive(true);


            while (true)
            {
                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);


                m_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            m_bannerView.OnTimerExpired?.Invoke(m_bannerView.Index);

            if (m_bannerView.DestroyOnTimerExpired)
            {
                //m_bannerView.Destroy(gameObject);
            }
        }

        protected virtual void Clicked(int index = 0)
        {
            if (!m_bannerView.duringDelay)
            {
                m_bannerView.StartCoroutine(m_bannerView.CancelDoubleClick());

                IMSController.Instance.ExecuteAction(m_iZone, index);
            }
        }

    }
}