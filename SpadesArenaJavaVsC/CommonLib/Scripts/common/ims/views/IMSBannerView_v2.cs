﻿using System;
using System.Collections;
using common.comm;
using common.controllers;
using common.ims.model;

namespace common.ims.views
{
    [System.Serializable]
    public class MyIntEvent_v2 : UnityEvent<int>
    {
    }

    public class IMSBannerView_v2 : MonoBehaviour
    {

        [SerializeField] Button m_linked_button;
        [SerializeField] string m_location;
        [SerializeField] int m_index;
        [SerializeField] MyIntEvent_v2 m_onNoBanner = new MyIntEvent_v2();
        [SerializeField] MyIntEvent_v2 m_onBannerDataArrived = new MyIntEvent_v2();
        [SerializeField] MyIntEvent_v2 m_onBannerLoaded = new MyIntEvent_v2();
        [SerializeField] MyIntEvent_v2 m_onTimerExpired = new MyIntEvent_v2();
        [SerializeField] bool m_destroyOnTimerExpired = false;
        [SerializeField] GameObject m_IMSBannerV2_prefab;

        [SerializeField] float m_clickProtectionDelay = 1.5f;
        bool m_duringDelay = false;

        GameObject m_loader;

        IMSInteractionZone m_model = null;
        Image m_image = null;

        Transform m_sounds_object = null;

        Coroutine m_timeoutCoroutine;

        IEnumerator m_timer_routine = null;

        bool m_isRotatingBanner;

        public Action<IMSBannerInjector> OnInjectionCompleted;

        private void OnEnable()
        {
            // Register to IMSController events - if not a rotating banner - in this case the parent will listen
            IMSController.Instance.IMSEventDataChanged -= UpdateModel;
            if (!m_isRotatingBanner)
            {
                IMSController.Instance.IMSEventDataChanged += UpdateModel;
            }

            if (m_loader==null)
            {
                m_loader = Instantiate(PopupManager.Instance.Loader, transform);
                m_loader.SetActive(false);
            }

            // Update model immediatly - if enabled after the IMS data has arrived
            if (IMSController.Instance.HasEvents)
                UpdateModel();
        }


        public void UpdateModel()
        {
            bool no_data = false;
            if (m_location == null)
                m_location = "Error";

            Debug.Log("Got update event for location " + m_location);
            if (IMSController.Instance.BannersByLocations.ContainsKey(m_location))
            {
                if (IMSController.Instance.BannersByLocations[m_location].Count > m_index)
                {
                    m_model = IMSController.Instance.BannersByLocations[m_location][m_index];

                    if (transform.parent.GetComponent<Button>() != null)
                        transform.parent.GetComponent<Button>().interactable = true;

                    UpdateView();

                    m_onBannerDataArrived?.Invoke(m_index);
                }
                else
                    no_data = true;
            }
            else
                no_data = true;

            //location is still "" (empty) as the item was just created - no reason to trigger the event
            if (no_data && m_location != "")
            {
                //no such banner in the model
                NoModelAction(m_location);
                if (transform.parent != null && transform.parent.GetComponent<Button>() != null)
                    transform.parent.GetComponent<Button>().interactable = false;
            }

        }

        private void UpdateView()
        {

            StartTimeoutCheck();

            if (m_loader!=null)
                m_loader.SetActive(true);

            LoggerController.Instance.Log(LoggerController.Module.IMS, "Loading graphics for banner with location: " + m_location);

            if (m_model.IsAssetBundle)
                RemoteAssetManager.Instance.LoadAsset(RemoteAssetManager.Instance.CdnBase, m_model.Source, Hash128.Compute("1"), OnAssetBundleLoaded);
            else
                RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, m_model.Source, m_model.Id, OnGraphicLoaded, null);
        }

        private void StartTimeoutCheck()
        {
            // Start a timeout check
            float popupLoadTimeout = LocalDataController.Instance.GetSettingAsInt("PromoPopupLoadTimeout", 0);
            if (popupLoadTimeout > 0)
                m_timeoutCoroutine = StartCoroutine(TimeoutCheck(popupLoadTimeout));
        }

        private IEnumerator TimeoutCheck(float popupLoadTimeout)
        {
            yield return new WaitForSecondsRealtime(popupLoadTimeout);

            //decide what to do after couldnt load image
        }

        private void OnGraphicLoaded(Sprite bannerSprite, Texture2D rawTexture, object userData)
        {
            if (m_loader != null)
                m_loader.SetActive(false);

            if (this == null)
                return;
            GameObject image_object = null;

            if (m_timeoutCoroutine != null)
                StopCoroutine(m_timeoutCoroutine);


            if (m_linked_button == null)
            {
                if (bannerSprite == null)
                    return;

                // Delete current children  
                foreach (Transform item in transform)
                {
                    Destroy(item.gameObject);
                }

                image_object = new GameObject("Image");

                image_object.AddComponent<Image>();
                image_object.AddComponent<ButtonsOverView>();
               
                m_image = image_object.GetComponent<Image>();

                image_object.transform.SetParent(transform);

                RectTransform image_bg_rect = m_image.GetComponent<RectTransform>();

                image_bg_rect.localScale = new Vector2(1, 1);
                image_bg_rect.localPosition = new Vector3();
                m_image.sprite = bannerSprite;
                m_image.SetNativeSize();

                Image image = transform.GetChild(0).GetComponent<Image>();
                if (image != null)
                    image.color = new Color(image.color.r, image.color.g, image.color.b, 255);

                //copy alignment from parent and apply to image
                RectTransform parent_rect = gameObject.GetComponent<RectTransform>();

                image_bg_rect.pivot = new Vector2(parent_rect.anchorMin.x, parent_rect.anchorMin.y);
            }
            else
            {
                m_linked_button.onClick.RemoveAllListeners();
                m_linked_button.onClick.AddListener(() =>
                {
                    IMSController.Instance.ExecuteAction(m_model);
                });
            }

            IMSBannerInjector bannerInjector = IMSInjectorFactory.Instance.CreateByLocation(m_model.Location);
            bannerInjector?.InjectGraphics(this, image_object, m_model);
            OnInjectionCompleted?.Invoke(bannerInjector);


            if (LocalDataController.Instance.GetSettingAsBool("BannersImpressionTracking",false))
                IMSController.Instance.FireImpressionTracking(m_model);

            m_onBannerLoaded.Invoke(m_index);
        }


      

        private void OnAssetBundleLoaded(GameObject loaded_obj)
        {
            if (m_loader != null)
                m_loader.SetActive(false);

            GameObject loaded_asset = null;

            if (loaded_obj != null)
            {
                // Delete current children  
                foreach (Transform item in transform)
                {
                     Destroy(item.gameObject);
                }

                loaded_asset = Instantiate(loaded_obj);

                // Unload the bundle after instantiation but keep the objects
                RemoteAssetManager.Instance.UnloadBundle(m_model.Source,false);
            }

            if (loaded_asset != null)
            {
                loaded_asset.transform.SetParent(transform);
                RectTransform loaded_asset_rect = loaded_asset.transform as RectTransform;

                loaded_asset_rect.localScale = Vector3.one;
                loaded_asset_rect.localPosition = Vector3.zero;
                loaded_asset_rect.anchoredPosition = Vector3.zero;
                loaded_asset_rect.eulerAngles = Vector3.zero;

                //copy alignment from parent and apply to image
                RectTransform parent_rect = gameObject.GetComponent<RectTransform>();
               
                if (m_linked_button == null)
                {

                }
                else
                {
                    m_linked_button.onClick.RemoveAllListeners();
                    m_linked_button.onClick.AddListener(() =>
                    {
                        IMSController.Instance.ExecuteAction(m_model);
                    });
                }

                IMSBannerInjector bannerInjector = IMSInjectorFactory.Instance.CreateByTemplate(m_model.TemplateId);
                bannerInjector?.Inject(this, loaded_asset, m_model, m_IMSBannerV2_prefab);
                OnInjectionCompleted?.Invoke(bannerInjector);

                if (LocalDataController.Instance.GetSettingAsBool("BannersImpressionTracking", false))
                    IMSController.Instance.FireImpressionTracking(m_model);

                m_onBannerLoaded.Invoke(m_index);
            }
        }

      

        private void NoModelAction(string model_location)
        {
            LoggerController.Instance.Log("No IMS banner location: " + model_location);
            m_onNoBanner?.Invoke(m_index);

            if (m_loader != null)
                m_loader.SetActive(false);

            //if (m_destroyOnNoBanner)
            //    Destroy(gameObject);
        }

        private void OnDestroy()
        {
            IMSController.Instance.IMSEventDataChanged -= UpdateModel;

            if (m_sounds_object != null)
                SoundsController.Instance.RemoveIMSBannerAudioSource(m_sounds_object.GetComponent<AudioSource>());

            if (m_timer_routine != null)
                StopCoroutine(m_timer_routine);
        }

        public void TimerExpireFunction()
        {
            LoggerController.Instance.Log("Timer Expired Function");
        }

        public IEnumerator CancelDoubleClick()
        {
            m_duringDelay = true;
            yield return new WaitForSeconds(m_clickProtectionDelay);
            m_duringDelay = false;
        }

        public int Index { get => m_index; set => m_index = value; }
        public string Location { get => m_location; set  { m_location = value; UpdateModel(); } }
        public MyIntEvent_v2 OnNoBanner { get => m_onNoBanner; set => m_onNoBanner = value; }
        public MyIntEvent_v2 OnBannerDataArrived { get => m_onBannerDataArrived; set => m_onBannerDataArrived = value; }
        public MyIntEvent_v2 OnTimerExpired { get => m_onTimerExpired; set => m_onTimerExpired = value; }
        public IMSInteractionZone Model { get => m_model; }
        public bool IsRotatingBanner { get => m_isRotatingBanner; set => m_isRotatingBanner = value; }
        public bool DestroyOnTimerExpired { get { return m_destroyOnTimerExpired; } set { m_destroyOnTimerExpired = value; } }
        public Transform sounds_object { get { return m_sounds_object; } set { m_sounds_object = value; } }
        public Coroutine timeoutCoroutine { get { return m_timeoutCoroutine; } set { m_timeoutCoroutine = value; } }
        public IEnumerator timer_routine { get { return m_timer_routine; } set { m_timer_routine = value; } }
        public bool duringDelay { get => m_duringDelay; set => m_duringDelay = value; }

        public GameObject IMSBannerV2_prefab { get { return m_IMSBannerV2_prefab; } set { m_IMSBannerV2_prefab = value; } }
    }
}