using System.Collections;
using System.Collections.Generic;

namespace common.ims.views
{

    public class IMSRotatingBannerView : MonoBehaviour
    {
        const int INDICATION_POINTS_Y_OFFSET = -295;
        const int SHIFT_OFFSET = 400;
        const int INDICATION_POINT_SPACES = 25;
        const int CLICK_TRESHOLD = 20;

        [SerializeField] string m_location_prefix;

        [SerializeField] GameObject m_storageBanners = null;
        [SerializeField] GameObject m_activeBanners = null;
        [SerializeField] GameObject m_indicationPoints_holder = null;
        List<Image> m_indication_points = new List<Image>();

        [SerializeField] GameObject m_prev = null;
        [SerializeField] GameObject m_curr = null;
        [SerializeField] GameObject m_next = null;

        [SerializeField] GameObject m_IMSBannerPrefab = null;
        [SerializeField] GameObject m_indication_point_Prefab = null;
        [SerializeField] GameObject m_default_banner = null;
        [SerializeField] UnityEvent m_default_banner_action = null;


        private List<IMSBannerView> m_banners = new List<IMSBannerView>();

        private float mouse_x = 0;
        private int dir = -1;
        private int m_currentPage = 0;
        private bool mIsDefault = false;
        int m_num_banners = 0;
        bool m_two_banners = false;

        bool m_clicked = false; // To prevent double click

       
        private void Start()
        {
            InitBannersView();
            IMSController.Instance.IMSEventDataChanged += InitBannersView;
        }

        private void OnEnable()
        {
            if (m_num_banners > 0)
                StartAutoScroll(.5f, 1);

            m_clicked = false;
        }

        /// <summary>
        /// instantiate IMSBanners based on the data in the model
        /// </summary>
        public void InitBannersView()
        {
            ResetBannerViews();

            // find out how many items we have in the prefix of this carosel
            if (IMSController.Instance.BannersByLocations.ContainsKey(m_location_prefix))
            {
                m_num_banners = IMSController.Instance.BannersByLocations[m_location_prefix].Count;
            }

            //in this case we need to create 4 banners so we can still drag to both sides
            int do_double = 1;
            if (m_num_banners == 2)
                do_double = 2;

            for (int i = 0; i < m_num_banners; i++)
            {
                for (int j = 0; j < do_double; j++)
                {
                    GameObject ims_banner = Instantiate(m_IMSBannerPrefab);
                    ims_banner.transform.SetParent(m_storageBanners.transform);
                    RectTransform ims_banner_rect = ims_banner.GetComponent<RectTransform>();
                    ims_banner_rect.localPosition = new Vector2();
                    ims_banner_rect.localScale = new Vector3(1f, 1f, 1f);
                    ims_banner_rect.localEulerAngles = new Vector3();
                    ims_banner_rect.sizeDelta = new Vector2(378f, 538f);

                    IMSBannerView iMSBannerView = ims_banner.GetComponent<IMSBannerView>();
                    iMSBannerView.IsRotatingBanner = true;

                    m_banners.Add(iMSBannerView);

                    iMSBannerView.Location = m_location_prefix;
                    iMSBannerView.Index = i;

                    if (do_double == 2)
                    {
                        iMSBannerView.Index = j;
                    }
                    

                    iMSBannerView.OnTimerExpired.AddListener(OnBannerTimerExpired);
                    iMSBannerView.OnNoBanner.AddListener(OnBannerNoData);
                    iMSBannerView.OnBannerDataArrived.AddListener(OnBannerDataArrived);

                    ims_banner.name = "Banner#_" + i;

                    iMSBannerView.UpdateModel();
                }
            }

            if (m_num_banners > 0)
            {
                mIsDefault = false;
                BuildIndicationPoints();

                //made 2 more fake banners for the case of only 2 for easier dragging both directions
                if (m_num_banners == 2)
                {
                    m_num_banners = 4;
                    m_two_banners = true;
                    for (int i = 0; i < m_num_banners; i++)
                    {
                        m_storageBanners.transform.GetChild(i).GetComponent<IMSBannerView>().Index = i;
                        m_storageBanners.transform.GetChild(i).name = "Banner#_" + i;
                    }
                }
                SetBanners();
            }
            else
            {
                mIsDefault = true;
            }

            m_default_banner.SetActive(m_num_banners == 0);


        }

        private void ResetBannerViews()
        {
            iTween.Stop(m_activeBanners);

            
            m_num_banners = 0;
            m_currentPage = 0;
            m_banners.Clear();

            m_two_banners = false;

            foreach (Transform child in m_storageBanners.transform)
            {
                Destroy(child.gameObject);
            }


            m_indication_points.Clear();

            // Destroy the indication points
            foreach (Transform child in m_indicationPoints_holder.transform)
            {
                Destroy(child.gameObject);
            }

            foreach (Transform child in m_prev.transform)
            {
                Destroy(child.gameObject);
            }
            foreach (Transform child in m_next.transform)
            {
                Destroy(child.gameObject);
            }
            foreach (Transform child in m_curr.transform)
            {
                Destroy(child.gameObject);
            }
        }

        private void TurnIndication(int index)
        {


            for (int i = 0; i < m_indication_points.Count; i++)
                m_indication_points[i].enabled = false;

            if (m_two_banners)
                index = index % 2;

            m_indication_points[index].enabled = true;

        }

        private void BuildIndicationPoints()
        {
            for (int i = 0; i < m_num_banners; i++)
            {
                GameObject indication_point = Instantiate(m_indication_point_Prefab);
                indication_point.transform.SetParent(m_indicationPoints_holder.transform);
                RectTransform indication_point_rect = indication_point.GetComponent<RectTransform>();
                indication_point_rect.localPosition = new Vector2(i * INDICATION_POINT_SPACES, 0);
                indication_point_rect.localScale = new Vector3(.5f, .5f, .5f);
                indication_point_rect.localEulerAngles = new Vector3();

                indication_point.name = "Point#_" + i;

                m_indication_points.Add(indication_point.transform.GetChild(0).gameObject.GetComponent<Image>());
            }

            float pos_x = -1 * ((m_num_banners - 1) * INDICATION_POINT_SPACES / 2);

            m_indicationPoints_holder.GetComponent<RectTransform>().localPosition = new Vector3(pos_x, INDICATION_POINTS_Y_OFFSET, 0);

        }

        private void SetBanners()
        {

            TurnIndication(m_currentPage);

            m_activeBanners.GetComponent<RectTransform>().localPosition = new Vector3();

            foreach (IMSBannerView banner in m_banners)
            {
                banner.transform.SetParent(m_storageBanners.transform);
            }

            int curr = m_currentPage;
            int next = (curr + 1 + m_num_banners) % m_num_banners;
            int prev = (curr - 1 + m_num_banners) % m_num_banners;

            SetDragPanel(curr, m_curr.transform);
            if (m_banners.Count > 1)
            {
                SetDragPanel(next, m_next.transform);
                SetDragPanel(prev, m_prev.transform);
                StartAutoScroll(3, 1);
            }

        }

        private void SetDragPanel(int index, Transform target)
        {
            IMSBannerView bannerView = m_banners[index];
            Transform curr_obj_trans = bannerView.gameObject.transform;
            curr_obj_trans.SetParent(target);
            ResetAllPos(curr_obj_trans.gameObject);
        }

        private void ResetAllPos(GameObject obj)
        {
            RectTransform gameObject_transform = obj.GetComponent<RectTransform>();
            gameObject_transform.localPosition = new Vector3();
            gameObject_transform.localScale = new Vector3(1f, 1f, 1f);
            gameObject_transform.localEulerAngles = new Vector3();

        }

        //should never happen - as we only create ones that exist
        private void OnBannerNoData(int index)
        {

        }


        private void OnBannerDataArrived(int index)
        {

        }

        private void OnBannerTimerExpired(int index)
        {

        }


        public void OnPointerDown()
        {
            mouse_x = Input.mousePosition.x;

            if (mIsDefault)
                return;
        }

        public void OnPointerUp()
        {
            //check click

            if (Mathf.Abs(Input.mousePosition.x - mouse_x) < CLICK_TRESHOLD)
            {
                // prevent double click
                if (!m_clicked)
                {
                    m_clicked = true;
                    StartCoroutine(ReleaseClickedFlag());

                    if (mIsDefault)
                    {
                        m_default_banner_action?.Invoke();
                    }
                    else
                    {
                        IMSBannerView page = m_banners[m_currentPage];
                        IMSController.Instance.ExecuteAction(page.Model);
                    }
                }
            }

            //detect drag & drop
            if (Mathf.Abs(Input.mousePosition.x - mouse_x) >= CLICK_TRESHOLD && mIsDefault == false && m_num_banners>1)
            {
                iTween.Stop(m_activeBanners);

                if (Input.mousePosition.x - mouse_x > 0)
                {
                    dir = -1;
                }
                else
                {
                    dir = 1;
                }

                StartAutoScroll(0, dir);
            }
        }

        private IEnumerator ReleaseClickedFlag()
        {
            yield return new WaitForSeconds(1f);
            m_clicked = false;
        }

        private void StartAutoScroll(float delay, int direction)
        {
            iTween.Stop(m_activeBanners);
            iTween.MoveTo(m_activeBanners, iTween.Hash("delay", delay,
                                                    "x", -direction * SHIFT_OFFSET,
                                                    "time", 0.3,
                                                    "isLocal", true,
                                                    "easeType", iTween.EaseType.easeOutCirc,
                                                    "oncomplete", "OnMoveComplete",
                                                    "oncompleteparams", direction,
                                                    "oncompletetarget", this.gameObject));
        }

        private void OnMoveComplete(int direction)
        {
            m_currentPage += direction;
            m_currentPage = (m_currentPage + m_num_banners) % m_num_banners;
            SetBanners();

        }

        private void OnDestroy()
        {
            // Remove the event if another scene is loaded
            IMSController.Instance.IMSEventDataChanged -= InitBannersView;
        }


    }
}
