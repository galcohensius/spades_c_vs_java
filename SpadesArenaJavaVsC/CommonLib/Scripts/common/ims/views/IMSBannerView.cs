﻿using common.controllers;
using common.ims.model;
using common.utils;
using common.views;
using System;
using System.Collections;

namespace common.ims.views
{
    [System.Serializable]
    public class MyIntEvent : UnityEvent<int>
    {
    }

    public class IMSBannerView : MonoBehaviour
    {

        [SerializeField] Button m_linked_button;
        [SerializeField] string m_location;
        [SerializeField] int m_index;
        [SerializeField] MyIntEvent m_onNoBanner = new MyIntEvent();
        [SerializeField] MyIntEvent m_onBannerDataArrived = new MyIntEvent();
        [SerializeField] MyIntEvent m_onTimerExpired = new MyIntEvent();
        [SerializeField] bool m_destroyOnTimerExpired = false;

        GameObject m_loader;

        IMSInteractionZone m_model = null;
        Image m_image = null;

        Transform m_sounds_object = null;

        Coroutine m_timeoutCoroutine;

        IEnumerator m_timer_routine = null;

        bool m_isRotatingBanner;
        bool loaded;

        private void OnEnable()
        {
            // Register to IMSController events - if not a rotating banner - in this case the parent will listen
            if (loaded == false)
            {
                loaded = true;
                IMSController.Instance.IMSEventDataChanged -= UpdateModel;
                if (!m_isRotatingBanner)
                {
                    IMSController.Instance.IMSEventDataChanged += UpdateModel;
                }

                if ( m_loader == null )
                {
                    m_loader = Instantiate(PopupManager.Instance.Loader, transform);
                    m_loader.SetActive(false);
                }

                // Update model immediatly - if enabled after the IMS data has arrived
                if (IMSController.Instance.HasEvents)
                    UpdateModel();
            }
        }


        public void UpdateModel()
        {
            bool no_data = false;

            Debug.Log("Got update event for location " + m_location);

            if (IMSController.Instance.BannersByLocations.ContainsKey(m_location))
            {
                if (IMSController.Instance.BannersByLocations[m_location].Count > m_index)
                {
                    m_model = IMSController.Instance.BannersByLocations[m_location][m_index];
                    if (transform.parent.GetComponent<Button>() != null)
                        transform.parent.GetComponent<Button>().interactable = true;

                    UpdateView();

                    m_onBannerDataArrived?.Invoke(m_index);
                }
                else
                    no_data = true;
            }
            else
                no_data = true;

            //location is still "" (empty) as the item was just created - no reason to trigger the event
            if (no_data && m_location != "")
            {
                //no such banner in the model
                NoModelAction(m_location);
                if (transform.parent != null && transform.parent.GetComponent<Button>() != null)
                    transform.parent.GetComponent<Button>().interactable = false;
            }

        }

        private void UpdateView()
        {

            StartTimeoutCheck();

            if (m_loader!=null)
                m_loader.SetActive(true);

            LoggerController.Instance.Log(LoggerController.Module.IMS, "Loading graphics for banner with location: " + m_location);

            if (m_model.IsAssetBundle)
                RemoteAssetManager.Instance.LoadAsset(RemoteAssetManager.Instance.CdnBase, m_model.Source, Hash128.Compute("1"), OnAssetBundleLoaded);
            else
                RemoteAssetManager.Instance.GetImage(RemoteAssetManager.Instance.CdnBase, m_model.Source, m_model.Id, OnGraphicLoaded, null);
        }

        private void StartTimeoutCheck()
        {
            // Start a timeout check
            float popupLoadTimeout = LocalDataController.Instance.GetSettingAsInt("PromoPopupLoadTimeout", 0);
            if (popupLoadTimeout > 0)
                m_timeoutCoroutine = StartCoroutine(TimeoutCheck(popupLoadTimeout));
        }

        private IEnumerator TimeoutCheck(float popupLoadTimeout)
        {
            yield return new WaitForSecondsRealtime(popupLoadTimeout);

            //decide what to do after couldnt load image
        }

        private void OnGraphicLoaded(Sprite bannerSprite, Texture2D rawTexture, object userData)
        {
            if (m_loader != null)
                m_loader.SetActive(false);

            if (this == null)
                return;

            if (m_timeoutCoroutine != null)
                StopCoroutine(m_timeoutCoroutine);


            if (m_linked_button == null)
            {
                if (bannerSprite == null)
                    return;

                // Delete current children  
                foreach (Transform item in transform)
                {
                    Destroy(item.gameObject);
                }

                GameObject image_object = new GameObject("Image");

                image_object.AddComponent<Image>();
                image_object.AddComponent<ButtonsOverView>();
    

                // Use pointer down to support external URL actions
                image_object.AddComponent<PointerDownHandler>();
                image_object.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                image_object.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                {
                    IMSController.Instance.ExecuteAction(m_model);
                });

                m_image = image_object.GetComponent<Image>();

                image_object.transform.SetParent(transform);

                RectTransform image_bg_rect = m_image.GetComponent<RectTransform>();

                image_bg_rect.localScale = new Vector2(1, 1);
                image_bg_rect.localPosition = new Vector3();
                m_image.sprite = bannerSprite;
                m_image.SetNativeSize();

                Image image = transform.GetChild(0).GetComponent<Image>();
                if (image != null)
                    image.color = new Color(image.color.r, image.color.g, image.color.b, 255);

                //copy alignment from parent and apply to image
                RectTransform parent_rect = gameObject.GetComponent<RectTransform>();

                image_bg_rect.pivot = new Vector2(parent_rect.anchorMin.x, parent_rect.anchorMin.y);
            }
            else
            {
                m_linked_button.onClick.RemoveAllListeners();
                m_linked_button.onClick.AddListener(() =>
                {
                    IMSController.Instance.ExecuteAction(m_model);
                });
            }

            if (LocalDataController.Instance.GetSettingAsBool("BannersImpressionTracking",false))
                IMSController.Instance.FireImpressionTracking(m_model);
        }

        private void OnAssetBundleLoaded(GameObject loaded_obj)
        {
            if (m_loader != null)
                m_loader.SetActive(false);

            GameObject loaded_asset = null;

            if (loaded_obj != null)
            {
                // Delete current children  
                foreach (Transform item in transform)
                {
                     Destroy(item.gameObject);
                }

                loaded_asset = Instantiate(loaded_obj);

                // Unload the bundle after instantiation but keep the objects
                RemoteAssetManager.Instance.UnloadBundle(m_model.Source,false);
            }

            if (loaded_asset != null)
            {
                loaded_asset.transform.SetParent(transform);
                RectTransform loaded_asset_rect = loaded_asset.transform as RectTransform;

                loaded_asset_rect.localScale = Vector3.one;
                loaded_asset_rect.localPosition = Vector3.zero;
                loaded_asset_rect.anchoredPosition = Vector3.zero;
                loaded_asset_rect.eulerAngles = Vector3.zero;



                //RectTransform asset_bundle_bg = loaded_asset.transform.GetChild(0).gameObject.GetComponent<RectTransform>();

                //copy alignment from parent and apply to image
                RectTransform parent_rect = gameObject.GetComponent<RectTransform>();


                //loaded_asset_rect.sizeDelta = new Vector2(asset_bundle_bg.rect.width, asset_bundle_bg.rect.height);
                //loaded_asset_rect.pivot = parent_rect.pivot;
                //loaded_asset_rect.anchorMax = parent_rect.anchorMax;
                //loaded_asset_rect.anchorMin = parent_rect.anchorMin;

                if (m_linked_button == null)
                {
                    Transform transform_BG = loaded_asset.transform.FindDeepChild("ActionButton");

                    if (transform_BG != null)
                    {
                        transform_BG.gameObject.AddComponent<ButtonsOverView>();
                        //transform_BG.gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
                        //transform_BG.gameObject.GetComponent<Button>().onClick.AddListener(() =>
                        //{
                        //    IMSController.Instance.ExecuteAction(m_model);
                        //});

                        // Use pointer down to support external URL actions
                        transform_BG.gameObject.AddComponent<PointerDownHandler>();
                        transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.RemoveAllListeners();
                        transform_BG.gameObject.GetComponent<PointerDownHandler>().OnPress.AddListener(() =>
                        {
                            IMSController.Instance.ExecuteAction(m_model);
                        });

                    }
                }
                else
                {
                    m_linked_button.onClick.RemoveAllListeners();
                    m_linked_button.onClick.AddListener(() =>
                    {
                        IMSController.Instance.ExecuteAction(m_model);
                    });
                }


                Transform coins_text = loaded_asset.transform.FindDeepChild("CoinsText");
                coins_text?.gameObject.SetActive(false);
                Transform price_text = loaded_asset.transform.FindDeepChild("PriceText");
                price_text?.gameObject.SetActive(false);
                Transform coins_more_text = loaded_asset.transform.FindDeepChild("CoinsMoreText");
                // CoinsMoreText visibility is controlled from the bundle
                Transform timer_bg = loaded_asset.transform.FindDeepChild("TimerBG");
                timer_bg?.gameObject.SetActive(false);
                Transform timer_text = loaded_asset.transform.FindDeepChild("TimerText");
                timer_text?.gameObject.SetActive(false);


                if (m_model.DynamicContent != null && m_model.DynamicContent.Content.Count > 0)
                {
                    if (coins_text != null && m_model.DynamicContent.Content[0]["gds"] != null)
                    {

                        AssetBundleUtils.SetTextForTransform(coins_text, FormatUtils.FormatBalance(m_model.DynamicContent.Content[0]["gds"]));
                        coins_text.gameObject.SetActive(true);
                    }

                    if (price_text != null && m_model.DynamicContent.Content[0]["cot"] != null)
                    {
                        AssetBundleUtils.SetTextForTransform(price_text, "$" + m_model.DynamicContent.Content[0]["cot"]);
                        price_text.gameObject.SetActive(true);
                    }

                    if (coins_more_text != null && m_model.DynamicContent.Content[0]["pMe"] != null)
                    {
                        AssetBundleUtils.SetTextForTransform(coins_more_text, m_model.DynamicContent.Content[0]["pMe"] + "%");
                    }

                    if (timer_text != null)
                    {
                        ManageTimer(timer_bg, timer_text);
                    }


                }

                m_sounds_object = loaded_asset.transform.FindDeepChild("SFX");

                if (m_sounds_object != null)
                    SoundsController.Instance.AddIMSBannerAudioSource(m_sounds_object.GetComponent<AudioSource>());

                if (LocalDataController.Instance.GetSettingAsBool("BannersImpressionTracking", false))
                    IMSController.Instance.FireImpressionTracking(m_model);

            }

        }



        private void ManageTimer(Transform timer_bg, Transform timer_text)
        {
            if (m_timer_routine != null)
            {
                StopCoroutine(m_timer_routine);
                m_timer_routine = null;
            }

            if (m_model.ViewLimit == null)
            {
                // No view limit - no timer
                return;
            }

            if (m_model.ViewLimit.Expires > 0)
            {
                // Start the timer
                if (timer_text.gameObject.GetComponent<Text>() != null)
                    m_timer_routine = POTimer((int)m_model.ViewLimit.Expires, timer_text.gameObject.GetComponent<Text>());

                if (timer_text.gameObject.GetComponent<TMP_Text>() != null)
                    m_timer_routine = POTimer((int)m_model.ViewLimit.Expires, timer_text.gameObject.GetComponent<TMP_Text>());

                StartCoroutine(m_timer_routine);


                // Check if we want to actually show the timer
                if (m_model.ViewLimit.IsVIsible)
                {
                    timer_bg?.gameObject.SetActive(true);
                    timer_text?.gameObject.SetActive(true);
                }
            }




        }




        private void NoModelAction(string model_location)
        {
            LoggerController.Instance.Log("No IMS banner location: " + model_location);
            m_onNoBanner?.Invoke(m_index);

            if (m_loader != null)
                m_loader.SetActive(false);

            //if (m_destroyOnNoBanner)
            //    Destroy(gameObject);
        }

        private void OnDestroy()
        {
            IMSController.Instance.IMSEventDataChanged -= UpdateModel;

            if (m_sounds_object != null)
                SoundsController.Instance.RemoveIMSBannerAudioSource(m_sounds_object.GetComponent<AudioSource>());

            if (m_timer_routine != null)
                StopCoroutine(m_timer_routine);
        }

        IEnumerator POTimer(int expired, TMP_Text m_timer_text)
        {
            m_timer_text.gameObject.SetActive(true);


            while (true)
            {
                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);


                m_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            m_onTimerExpired?.Invoke(m_index);

            if (m_destroyOnTimerExpired)
                Destroy(gameObject);
        }

        IEnumerator POTimer(int expired, Text m_timer_text)
        {
            m_timer_text.gameObject.SetActive(true);


            while (true)
            {
                double diff = DateUtils.TotalSeconds(expired);

                if (diff <= 0)
                    break;

                TimeSpan timeRemain = TimeSpan.FromSeconds(diff);


                m_timer_text.text = timeRemain.Hours.ToString("D2") + ":" + timeRemain.Minutes.ToString("D2") + ":" + timeRemain.Seconds.ToString("D2");
                yield return new WaitForSecondsRealtime(1f);

            }

            m_onTimerExpired?.Invoke(m_index);

            if (m_destroyOnTimerExpired)
                Destroy(gameObject);
        }



        public void TimerExpireFunction()
        {
            LoggerController.Instance.Log("Timer Expired Function");
        }

        public int Index { get => m_index; set => m_index = value; }
        public string Location { get => m_location; set => m_location = value; }
        public MyIntEvent OnNoBanner { get => m_onNoBanner; set => m_onNoBanner = value; }
        public MyIntEvent OnBannerDataArrived { get => m_onBannerDataArrived; set => m_onBannerDataArrived = value; }
        public MyIntEvent OnTimerExpired { get => m_onTimerExpired; set => m_onTimerExpired = value; }
        public IMSInteractionZone Model { get => m_model; }
        public bool IsRotatingBanner { get => m_isRotatingBanner; set => m_isRotatingBanner = value; }
    }
}