﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using common.ims.model;
using common.utils;
using System;
using common.controllers;
using common.models;

namespace common.ims {

    /// <summary>
    /// IMS controller
    /// </summary>
    public abstract class IMSController : MonoBehaviour
    {
        public const string INSUFFICIENT_COINS_MSG = "Not enough coins to play";

        // Event ids
        public const string EVENT_LOGIN = "1";
        public const string EVENT_END_GAME = "2";
        public const string EVENT_INSUFFICIANT_BALANCE = "4";
        public const string EVENT_SUCCESS_DEPOSIT = "5";
        public const string EVENT_CLOSE_CASHIER = "6";
        public const string EVENT_BACK_TO_LOBBY = "11";
        public const string EVENT_BONUS_AWARD = "12";
        public const string EVENT_START_GAME = "13";



        // Client event ids
        public const string CLIENT_EVENT_NEED_BOOST = "101";
        public const string CLIENT_EVENT_INSUFFICIENT_FUNDS = "102";

        // Actions
        public const int ACTION_BUY_ITEM = 39;

        // Template IDs
        public const string IMS_RAFFLE_PO_TEMPLATE = "RF";
        public const string IMS_MGAP_TEMPLATE = "MG";
        public const string IMS_BONUS_TEMPLATE = "BO";
        public const string IMS_CHALLENGE_TEMPLATE = "CH";
        public const string IMS_OPEN_TABLE_TEMPLATE = "OT";
        public const string IMS_PO_TEMPLATE = "PO";
        public const string IMS_OPEN_CASHIER_TEMPLATE = "OC";
        public const string IMS_CASHIER_TEMPLATE = "CR";


        // Optional iZone params
        public const string OPTIONAL_PARAM_INITIAL_ROOM = "initialRoom";
        public const string OPTIONAL_PARAM_PROMO_LOCATION = "promoLocation";


        protected Dictionary<string, IMSEvent> events = new Dictionary<string, IMSEvent>();
        protected Dictionary<string, IMSInteractionZone> interactionZones = new Dictionary<string, IMSInteractionZone>();

        Dictionary<string, string> packageIds = new Dictionary<string, string>(); // A dictionary of all packages that are found in IMS events
        Dictionary<string, List<IMSInteractionZone>> bannersByLocations = new Dictionary<string, List<IMSInteractionZone>>();

        private HashSet<string> multiBannerLocations = new HashSet<string> { "L1" };

        public static IMSController Instance;

        public Action IMSEventDataChanged;

        protected virtual void Awake()
        {
            if (Instance == null)
                Instance = this;
        }


        public void ParseEvents(JSONObject eventsJson, bool fireChangeEvent=true) {

            if (eventsJson == null || eventsJson.ToString()=="{}")
                return; // Empty object - do nothing

            // Clear only multi banner locations
            // Exclude IMSInteractionZoneMESWrapper for now since MES cannot add the banners again 
            foreach (string location in multiBannerLocations)
            {
                if (bannersByLocations.ContainsKey(location))
                    bannersByLocations[location].RemoveAll(iz => !(iz is IMSInteractionZoneMESWrapper));
            }

            IMSModelParser parser = new IMSModelParser();

            foreach (KeyValuePair<string, JSONNode> eventKV in eventsJson)
            {
                events[eventKV.Key] = parser.ParseEvent(eventKV.Key,eventKV.Value,this);
            }

            if (fireChangeEvent)
                IMSEventDataChanged?.Invoke();

        }

       
        /// <summary>
        /// Triggers an IMS event.
        /// </summary>
        /// <param name="eventId">Event identifier.</param>
        /// <param name="deleteAfterTriggering">If set to <c>true</c> delete the event after triggering so it wont trigger again
        /// unless received again</param>
        public virtual void TriggerEvent(string eventId, bool deleteAfterTriggering=false)
        {
            if (!events.TryGetValue(eventId, out IMSEvent imsEvent))
            {
                LoggerController.Instance.Log(LoggerController.Module.IMS, "Unknown IMS event: " + eventId);
                return;
            }

            // Execute each decision in the event
            foreach (IMSDecision dec in imsEvent.Decisions)
            {
                ExecuteDecision(dec);
            }

            if (deleteAfterTriggering)
                events.Remove(eventId);

        }

        public bool HasEvent(string eventId)
        {
            return events.ContainsKey(eventId);
        }

        public void ClearEvents()
        {
            events.Clear();
            interactionZones.Clear();
            bannersByLocations.Clear();
        }

        public void AddIZone(string id, IMSInteractionZone zone)
        {
            interactionZones[id] = zone;
        }

        public void FireImpressionTracking(IMSInteractionZone iZone) {
            if (iZone.TrackImpressions) {
                iZone.TrackingData.Add("mDa", iZone.Metadata);
                iZone.TrackingData.Add("eId", iZone.OpeningEvent?.Id ?? "0");
                iZone.TrackingData.Add("ts", DateUtils.UnixTimestamp());

                StartCoroutine(SendTrackingRequestCoroutine(GetImpressionsTrackingURL(), iZone.TrackingData.ToString()));
            }
        }

        public void FireClickTracking(IMSInteractionZone iZone, int actionId)
        {
            if (iZone.TrackClicks)
            {
                //iZone.TrackingData.Add("mDa", iZone.Metadata);
                //iZone.TrackingData.Add("eId", iZone.OpeningEvent.Id);
                iZone.TrackingData.Add("aId", actionId);

                StartCoroutine(SendTrackingRequestCoroutine(GetClicksTrackingURL(), iZone.TrackingData.ToString()));
            }

        }

        /// <summary>
        /// Special variation for casier popup
        /// </summary>
        public void FireImpressionTracking(Cashier cashier)
        {
            if (cashier.TrackImpressions)
            {
                cashier.TrackingData?.Add("mDa", cashier.ImsMetadata);

                StartCoroutine(SendTrackingRequestCoroutine(GetImpressionsTrackingURL(), cashier.TrackingData?.ToString()));
            }
        }

        /// <summary>
        /// Special variation for casier popup
        /// </summary>
        public void FireClickTracking(Cashier cashier, CashierItem item)
        {
            if (cashier.TrackClicks)
            {
                cashier.TrackingData?.Add("pId", item.Id);

                StartCoroutine(SendTrackingRequestCoroutine(GetClicksTrackingURL(), cashier.TrackingData?.ToString()));
            }

        }

        private IEnumerator SendTrackingRequestCoroutine(string url, string trackingData)
        {
            using (UnityWebRequest request = new UnityWebRequest(url, "POST"))
            {
                request.SetRequestHeader("Content-Type", "application/json");
                request.SetRequestHeader("Cache-Control", "no-cache");

                JSONObject requestJson = new JSONObject
                {
                    ["d"] = GZipCompressor.CompressString(trackingData)
                };

                UploadHandler uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(requestJson.ToString()));
                uploadHandler.contentType = "application/json";

                DownloadHandler downloadHandler = new DownloadHandlerBuffer();

                request.uploadHandler = uploadHandler;
                request.downloadHandler = downloadHandler;

                yield return request.SendWebRequest();

                LoggerController.Instance.Log(LoggerController.Module.IMS, "Tracking data sent, Data: " + trackingData +", Response code: " + request.responseCode);
            }

        }

        public bool IsIZoneExpired(IMSInteractionZone iZone)
        {
            if (iZone.ViewLimit == null) return false;

            double diff = DateUtils.TotalSeconds((int)iZone.ViewLimit.Expires);

            return diff <= 0;
        }


        public void AddBannerByLocation(IMSInteractionZone zone)
        {
            if (zone.Location != null)
            {
                // If not multibanner - always create a new list
                if (!multiBannerLocations.Contains(zone.Location))
                    BannersByLocations[zone.Location] = new List<IMSInteractionZone>();
                else if (!BannersByLocations.ContainsKey(zone.Location))
                    BannersByLocations[zone.Location] = new List<IMSInteractionZone>();



                // If the id exists - remove the old one (to prevent duplicates)
                bannersByLocations[zone.Location].RemoveAll(m => m.Id == zone.Id);

                BannersByLocations[zone.Location].Add(zone);
                BannersByLocations[zone.Location].Sort((x, y) => y.Order - x.Order);

            }
        }

        protected abstract void ExecuteDecision(IMSDecision decision);

        public abstract void ExecuteAction(IMSInteractionZone iZone,int actionParamIndex = 0);
        public abstract void ExecuteDirectBuyAction(IMSInteractionZone iZone, int actionParamIndex, bool showPurchasePopup = true);

        protected abstract string GetImpressionsTrackingURL();
        protected abstract string GetClicksTrackingURL();

       
        public bool HandleInsufficientFunds(int delta, PopupManager.AddMode addMode = PopupManager.AddMode.ShowAndRemove)
        {
            if (HasEvent(EVENT_INSUFFICIANT_BALANCE))
            {
                PopupManager.Instance.ShowAutoMessagePopup(INSUFFICIENT_COINS_MSG, 1f,PopupManager.AddMode.ShowAndRemove, () =>
                {
                    TriggerEvent(EVENT_INSUFFICIANT_BALANCE);
                });
                return true;
            }
            else
            {
                // For now - fallback to MES implementation
                //MESBase.Instance?.HandleInsufficientFunds(delta);
                return false;
            }
        }

        public Dictionary<string, string> PackageIds { get => packageIds; }
        public Dictionary<string, List<IMSInteractionZone>> BannersByLocations { get => bannersByLocations; }
        public bool HasEvents { get => events.Count > 0; }
    }

}