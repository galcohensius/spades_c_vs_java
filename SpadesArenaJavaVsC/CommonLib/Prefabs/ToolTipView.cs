﻿using System.Collections.Generic;
using common.ims.model;
using static common.views.GoodsListView;

namespace common.views
{
    public class ToolTipView : MonoBehaviour
    {
        [SerializeField] RectTransform m_toolTipHead;
        [SerializeField] RectTransform m_bg;
        [SerializeField] List<TMP_Text> tmp_Texts = new List<TMP_Text>();

        const float ARROW_OFFSET = 70f;

        public void HideToolTip()
        {
            gameObject.SetActive(false);
        }

        public void Init(Vector2 gridSize, ToolTipPosition toolTipPosition, IMSGoodsList iMSGoodsList)
        {
            FillTexts(iMSGoodsList);
            gameObject.SetActive(true);

            RectTransform mainRect = gameObject.GetComponent<RectTransform>();

            float mainOffset = gridSize.x / 2 + m_bg.sizeDelta.x / 2 + ARROW_OFFSET;
            
            if(toolTipPosition == ToolTipPosition.Down)
            {
                m_toolTipHead.localPosition = new Vector3(0, ARROW_OFFSET, 0);
                m_toolTipHead.localEulerAngles = new Vector3(0, 0, 0);
                mainRect.localPosition = new Vector3(0, -mainOffset, 0);
            }
            else if(toolTipPosition == ToolTipPosition.Left)
            {
                m_toolTipHead.localPosition = new Vector3(ARROW_OFFSET, 0, 0);
                m_toolTipHead.localEulerAngles = new Vector3(0, 0, -90);
                mainRect.localPosition = new Vector3(-mainOffset,0, 0);
            }
            else if (toolTipPosition == ToolTipPosition.Right)
            {
                m_toolTipHead.localPosition = new Vector3(-ARROW_OFFSET, 0, 0);
                m_toolTipHead.localEulerAngles = new Vector3(0, 0, 90);
                mainRect.localPosition = new Vector3(mainOffset, 0, 0);
            }
            else if (toolTipPosition == ToolTipPosition.Up)
            {
                m_toolTipHead.localPosition = new Vector3(0,-ARROW_OFFSET, 0);
                m_toolTipHead.localEulerAngles = new Vector3(0, 0, 180);
                mainRect.localPosition = new Vector3(0, mainOffset, 0);
            }
        }

        private void FillTexts(IMSGoodsList iMSGoodsList)
        {
            for (int i = 0; i < tmp_Texts.Count; i++)
            {
                tmp_Texts[i].text = string.Format("This is an item of type {0}", ((GoodsListType)i).ToString());
            }

            tmp_Texts[0].text += "\nCoins has " + iMSGoodsList.Coins.Count + " items";
            tmp_Texts[1].text += "\nVouchers has " + iMSGoodsList.Vouchers.Count + " items";
            tmp_Texts[2].text += "\nItems has " + iMSGoodsList.InventoryItems.Count + " items";
        }
    }
}
