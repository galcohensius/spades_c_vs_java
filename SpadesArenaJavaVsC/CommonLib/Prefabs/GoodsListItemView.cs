﻿using cardGames.controllers;
using common.ims.model;
using System.Collections.Generic;

namespace common.views

{
    public class GoodsListItemView : MonoBehaviour
    {
        [SerializeField] List<GameObject> m_holders = new List<GameObject>();

        IMSGoodsList m_imsGoodsList;
        GoodsListType m_goodsListType;
        RectTransform m_rect;

        [SerializeField] TMP_Text m_voucherValue;
        [SerializeField] RectTransform m_voucherImage;

        int m_itemIndex;

        public void Init(IMSGoodsList iMSGoodsList, GoodsListType goodsListType,int itemIndex)
        {
            m_rect = GetComponent<RectTransform>();
            m_itemIndex = itemIndex;
            m_imsGoodsList = iMSGoodsList;
            m_goodsListType = goodsListType;
            Visualize();
        }

        public void Visualize()
        {
            if(m_goodsListType == GoodsListType.Vouchers)
            {
                //need to adjust the size of the voucher image based on its parent
                //need to fill in the voucher value

                float voucherImageAspect = m_voucherImage.sizeDelta.x / m_voucherImage.sizeDelta.y;
                float scaleFactor = 0;
                
                if (voucherImageAspect > 1)
                    scaleFactor = m_rect.sizeDelta.x / m_voucherImage.sizeDelta.x;
                else
                    scaleFactor = m_rect.sizeDelta.y / m_voucherImage.sizeDelta.y;       
               
                m_voucherImage.localScale = new Vector2(scaleFactor,scaleFactor);

                m_voucherValue.text = m_imsGoodsList.Vouchers[m_itemIndex].VouchersValue.ToString();
            }

            else if(m_goodsListType == GoodsListType.Items)
            {
                string itemID = "1_hm03";

                itemID = m_imsGoodsList.InventoryItems[m_itemIndex];

                itemID = "1_hm03";

                GameObject itemsObject = MAvatarController.Instance.GetPrefabByID(itemID, m_holders[2].transform);

                //need to change the scale of the prefab to fit in the box
                if(itemsObject!=null)
                {
                    RectTransform itemsObjectRect = itemsObject.GetComponent<RectTransform>();
                    float itemsObjectAspect = itemsObjectRect.sizeDelta.x / itemsObjectRect.sizeDelta.y;
                    float scaleFactor = 0;

                    if (itemsObjectAspect > 1)
                        scaleFactor = m_rect.sizeDelta.x / itemsObjectRect.sizeDelta.x;
                    else
                        scaleFactor = m_rect.sizeDelta.y / itemsObjectRect.sizeDelta.y;

                    itemsObjectRect.localScale = new Vector2(scaleFactor, scaleFactor);
                }
                else
                {
                    //if no item - show the empty container
                    m_holders[2].transform.GetChild(0).gameObject.SetActive(true);
                    m_holders[2].transform.GetChild(1).gameObject.SetActive(true);

                }

            }

            m_holders[(int)m_goodsListType].SetActive(true);
            
        }
    }
}