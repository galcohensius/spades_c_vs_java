﻿using System.Net.Sockets;
using SpadesAI;

namespace SpadesArenaJavaVsC
{
    public class Client
    {
        public Socket workSocket = null;
        public const int BUFFER_SIZE = 1024;
        public byte[] buffer = new byte[BUFFER_SIZE];
        public string lastMsg;
        public SpadesBrainImpl spadesBrain;
    }
}