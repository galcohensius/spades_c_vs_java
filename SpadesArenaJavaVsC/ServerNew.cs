﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SpadesArenaJavaVsC
{
    public delegate void ConnectionEventHandler(Client c);

    public delegate void ConnectionBlockedEventHandler(IPEndPoint endPoint);

    public delegate void MessageReceivedEventHandler(Client c);

    public class ServerNew
    {
        private const int PORT = 1987;
        private const string NEW_LINE = "\n";

        private Socket serverSocket;

        // Thread signal.  
        public static ManualResetEvent allDone = new ManualResetEvent(false);

        public event ConnectionEventHandler ClientConnected;
        public event ConnectionEventHandler ClientDisconnected;
        public event MessageReceivedEventHandler MessageReceived;

        public ServerNew()
        {
            this.serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void start()
        {
            Console.WriteLine("Server starting...");
            // Bind the socket to the local endpoint and listen for incoming connections.  
            try
            {
                serverSocket.Bind(new IPEndPoint(IPAddress.Any, PORT));
                serverSocket.Listen(1000);

                while (true)
                {
                    // Set the event to non-signaled state.  
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.  
                    serverSocket.BeginAccept(new AsyncCallback(handleIncomingConnection), serverSocket);

                    // Wait until a connection is made before continuing.  
                    allDone.WaitOne();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            Console.WriteLine("\nPress ENTER to continue...");
            Console.Read();
        }

        public void stop()
        {
            serverSocket.Close();
        }


        private void handleIncomingConnection(IAsyncResult ar)
        {
            // Signal the main thread to continue.  
            allDone.Set();

            Socket s = (Socket) ar.AsyncState;
            Socket s2 = s.EndAccept(ar);

            Client client = new Client();
            client.workSocket = s2;
            s2.BeginReceive(client.buffer, 0, Client.BUFFER_SIZE, 0,
                new AsyncCallback(receiveData), client);

            Console.WriteLine("Client connected: " + s2.RemoteEndPoint);

            ClientConnected(client);
        }


        /// <summary>
        /// Receives and processes data from a socket.
        /// It triggers the message received event in
        /// case the client pressed the return key.
        /// </summary>
        private void receiveData(IAsyncResult ar)
        {
            try
            {
                Client client = (Client) ar.AsyncState;
                Socket s = client.workSocket;

                int read = s.EndReceive(ar);

                if (read > 0)
                {
                    client.lastMsg = Encoding.ASCII.GetString(client.buffer, 0, read);

                    Console.WriteLine($"Message received: {client.lastMsg}");

                    MessageReceived(client);

                    s.BeginReceive(client.buffer, 0, Client.BUFFER_SIZE, 0, new AsyncCallback(receiveData), client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void sendMessageToClient(Client client, string message)
        {
            byte[] data = Encoding.ASCII.GetBytes(message + NEW_LINE);
            client.workSocket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(sendData), client);

            Console.WriteLine("Message sent: " + message);
        }

        private void sendData(IAsyncResult ar)
        {
            Client client = (Client) ar.AsyncState;
            Socket s = client.workSocket;

            int sent = s.EndSend(ar);
        }
    }
}